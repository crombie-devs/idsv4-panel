import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ICrudWS }  from '../interfaces/ICrudWS.interface';


@Injectable()
export abstract class BaseStore <T> {
  protected data: T[] = [];


  public onChange: Subject<T[]> = new Subject();

  constructor(protected http: ICrudWS<T>) {
    this.load();
  }

  

  public reload() {
    this.load();
  }

  protected load() {
    
  }

  protected save(data: T[]) {
    this.data = data;
  }

  public get(): T[] {
   return this.data;
    
  }

  public set(data: T[]) {
    this.save(data);
    this.onChange.next(data);
  }

  public create(item: T  ) : Observable<T> {
     return  this.http.create(item).pipe(tap(() => this.reload()))
  }

  protected filter(key, value): T[] {
 
   return this.data;
  }
}
