import { Log, LOG_TYPE } from "../models/log";
import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

import { CONFIG } from "../app/app.config";


@Injectable()
export class DebugStore {
	public logs: Log[] = [];
	public onAdd: Subject<Log> = new Subject();

	private addLog(level:string, title:string, description:string) {
		let log: Log = new Log(level, title, description);
		if(!log)
			return;

		this.logs.push(log);
		this.onAdd.next(log);
	}

	addInfo(title:string, description:string) {
		if(!CONFIG.production){ console.info(title, description); }
		this.addLog(LOG_TYPE.INFO, title, description);
	}

	addImportant(title:string, description:string) {
		if(!CONFIG.production){ console.info(title, description); }
		this.addLog(LOG_TYPE.IMPORTANT, title, description);
	}

	addAlert(title:string, description:string) {
		if(!CONFIG.production){ console.warn(title, description); }
		this.addLog(LOG_TYPE.ALERT, title, description);
	}

	addError(title:string, description:string) {
		if(!CONFIG.production){ console.error(title, description); }
		this.addLog(LOG_TYPE.ERROR, title, description);
	}

	count(log:Log): number {
		const filtred = this.logs.filter(l => log.equal(l) );
		return filtred.length;
	}
}