import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService} from 'src/app/auth/auth-guard.service';
import { GuardService } from 'src/app/auth/guard.service';
import { MaintenanceService } from 'src/app/auth/maintenance.service';
import { PrivatePage } from 'src/pages/private/private.page';
import { LoginPage } from 'src/modules/auth/pages/login/login.page';
import { MaintenancePage } from 'src/modules/maintenance/page/maintenance.page';

const routes: Routes = [ 
  { path: 'login', component: LoginPage, canActivate:[GuardService]},
  { path: 'support-contact', canActivate:[GuardService], loadChildren: () => import('src/modules/support-contact/support-contact.module').then(m => m.SupportContactModule),data: { breadcrumb: 'Contacto soporte' } },
  { path: 'private', 
    component: PrivatePage, 
    data: { breadcrumb: {label: 'Inicio', info: 'home'}}, 
    canActivate: [AuthGuardService, GuardService],
    children: [
      { path: '', loadChildren: () => import('src/modules/dashboard/dashboard.module').then(m => m.DashboardModule),data: { breadcrumb: 'Dashboard' }},
      { path: '', loadChildren: () => import('src/modules/content/content.module').then(m => m.ContentModule), data: { breadcrumb: 'Contenidos' }},
      { path: '', loadChildren: () => import('src/modules/customer/customer.module').then(m => m.CustomerModule),data: { breadcrumb: 'Clientes' }},
      { path: '', loadChildren: () => import('src/modules/indoor-location/indoor-location.module').then(m => m.IndoorLocationModule), data: { breadcrumb: 'Localizaciones' }},
      { path: '', loadChildren: () => import('src/modules/format/format.module').then(m => m.FormatModule), data: { breadcrumb: 'Formatos' }},
      { path: '', loadChildren: () => import('src/modules/camera/camera.module').then(m => m.CameraModule),data: { breadcrumb: 'Cámaras' }},
      { path: '', loadChildren: () => import('src/modules/people/people.module').then(m => m.PeopleModule), data: {breadcrumb: 'Peoples'}},
      { path: '', loadChildren: () => import('src/modules/messages/messages.module').then(m => m.MessagesModule), data: { breadcrumb: 'Mensajes' }},
      { path: 'asset', loadChildren: () => import('src/modules/asset/asset.module').then(m => m.AssetModule), data: { breadcrumb: 'Cartelera' }},
      { path: 'audios',  loadChildren: () => import('src/modules/audio/audios.module').then((m) => m.AudiosModule), data: {breadcrumb: 'Audios'}},
      { path: 'dynamic-events',  loadChildren: () => import('src/modules/dynamic-event/dynamic-events.module').then((m) => m.DynamicEventsModule), data: {breadcrumb: 'Eventos dinamicos'}},
      { path: 'dynamic-events/events-preview',  loadChildren: () => import('src/modules/dynamic-event/dynamic-events.module').then((m) => m.DynamicEventsModule), data: {breadcrumb: 'Eventos dinamicos'}},
      { path: 'site',  loadChildren: () => import('src/modules/site/site.module').then((m) => m.SiteModule), data: {breadcrumb: 'Sites'}},
      { path: 'algorithm', loadChildren: () => import('src/modules/algorithm/algorithm.module').then(m => m.AlgorithmModule), data: { breadcrumb: 'Algoritmos' } },
      { path: 'faqs', data: { breadcrumb: "FAQ's" }, loadChildren: () => import('src/modules/algorithm/algorithm.module').then(m => m.AlgorithmModule)}, 
      { path: 'user', loadChildren: () => import('src/modules/user/user.module').then(m => m.UserModule),data: { breadcrumb: 'Usuarios' }},
      { path: 'player', loadChildren: () => import('src/modules/player/player.module').then(m => m.PlayerModule), data: { breadcrumb: 'Visores' } },
      { path: 'template', loadChildren: () => import('src/modules/template/template.module').then(m => m.TemplateModule), data: { breadcrumb: 'Plantillas' } },
      { path: 'products-tags', loadChildren: () => import('src/modules/product/product.module').then(m => m.ProductModule), data: { breadcrumb: 'Tags de productos' }, },
      { path: 'tag', loadChildren: () => import('src/modules/tag/tag.module').then(m => m.TagModule), data: { breadcrumb: 'Tags'}},
      { path: 'languages', loadChildren: () => import('src/modules/lang/lang.module').then(m => m.LangModule), data: { breadcrumb: 'Idiomas'}, },
      { path: 'analisis/emisiones', loadChildren: () => import('src/modules/emission/emission.module').then(m => m.EmissionModule), data: { breadcrumb: 'Analisis' }},
      { path: 'analisis/emisionesxhora', loadChildren: () => import('src/modules/emission-sale/emission-sale.module').then(m => m.EmissionSaleModule), data: { breadcrumb: 'Emisiones & Ventas' }},
      { path: 'analisis/afluencias', loadChildren: () => import('src/modules/flow/flow.module').then(m => m.FlowModule), data: { breadcrumb: 'Afluencias' }},
      { path: 'analisis/socioeconomicos', loadChildren: () => import('src/modules/socio-economic/socio-economic.module').then(m => m.SocioEconomicModule), data: { breadcrumb: 'Socioeconómicos' }},
      { path: 'recomendacion', loadChildren: () => import('src/modules/recomendation/recomendation.module').then(m => m.RecomendationModule), data: { breadcrumb: 'Recomendaciones' }},
      { path: 'playcircuit', loadChildren: () => import('src/modules/playcircuit/playcircuit.module').then(m => m.PlaycircuitModule), data: { breadcrumb: 'Circuitos de reproducción' } },
      { path: 'playareas', loadChildren: () => import('src/modules/playarea/playarea.module').then(m => m.PlayareaModule), data: { breadcrumb: 'Áreas de reproducción' }},
      { path: 'support', data: { breadcrumb: 'Soporte' }, loadChildren: () => import('src/modules/support/support.module').then(m => m.SupportModule)},
      { path: 'ticket', data: { breadcrumb: 'Ticket' }, loadChildren: () => import('src/modules/ticket/ticket.module').then(m => m.TicketModule)},
    ]
  },
  { path: 'private', pathMatch: 'full', redirectTo: 'private', canActivate:[GuardService]},
  { path: 'maintenance', component: MaintenancePage, canActivate: [MaintenanceService] },
  { path: '', pathMatch: 'full', redirectTo: 'private/dashboard', canActivate:[GuardService]}
];
@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload',
    useHash: false
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
