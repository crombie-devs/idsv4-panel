import { environment } from '../environments/environment';
export const CONFIG = environment;
export const VERSION = "4.0.35.b2";
export const APP = "Ladorian IDS";
