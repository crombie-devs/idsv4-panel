import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbModule } from 'xng-breadcrumb';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { IvyCarouselModule } from 'angular-responsive-carousel';

import { StorageServiceModule } from 'ngx-webstorage-service';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { WSService } from 'src/services/ws.service';
import { PostMessageService } from 'src/services/postmessage.service';
import { ExcelService } from 'src/services/excel.service';
import { PdfService } from 'src/services/pdf.service';
import { SocketService } from 'src/services/socket.service';
import { AuthInterceptorService } from 'src/services/interceptor/auth-interceptor.service';
import { AuthService } from 'src/app/auth/auth.service';
import { AuthGuardService } from 'src/app/auth/auth-guard.service';
import { GuardService } from 'src/app/auth/guard.service';
import { MaintenanceService } from 'src/app/auth/maintenance.service';
import { CacheService } from 'src/services/cache.service';
import { NotificationService } from 'src/services/notification.service';

import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MaintenanceStore } from 'src/modules/maintenance/stores/maintenance.store';
import { DebugStore } from 'src/stores/debug.store';
import { CameraStore } from 'src/modules/camera/stores/camera.store';
import { CountryStore } from 'src/modules/country/stores/country.store';
import { ProvinceStore } from 'src/modules/province/stores/province.store';
import { CityStore } from 'src/modules/city/stores/city.store';
import { SiteStore } from 'src/modules/site/stores/site.store';

import { MaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { DialogService } from 'src/modules/dialog/services/dialog.service';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MAT_DATE_LOCALE } from '@angular/material/core';

import { CookieService } from 'ngx-cookie-service';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { SharedModule } from 'src/shared/shared.module';
import { ChartGraphsModule } from 'src/modules/chart/charts.module';

import { CacheInterceptor } from 'src/services/interceptor/HttpCacheInterceptor';

import { DialogModule } from 'src/modules/dialog/dialog.module';
import { PowerModule } from 'src/modules/power/power.module';
import { LandingPageModule } from 'src/modules/landing-page/landingpage.module';
import { MenuModule } from 'src/modules/menu/menu.module';
import { FormFieldModule } from 'src/modules/form-field/form-field.module';
import { PrivatePage } from 'src/pages/private/private.page';
import { LoginPage } from 'src/modules/auth/pages/login/login.page';
import { RecoveryDialog } from 'src/modules/auth/dialogs/recovery/recovery.dialog';
import { ErrorDialog } from 'src/modules/auth/dialogs/error/error.dialog';

@NgModule({
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    declarations: [
        AppComponent,
        PrivatePage,
        LoginPage,
        RecoveryDialog,
        ErrorDialog
    ],
    entryComponents: [RecoveryDialog],
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        MaterialModule,
        FlexLayoutModule,
        StorageServiceModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BreadcrumbModule,
        NgxMatFileInputModule,
        MatAutocompleteModule,
        IvyCarouselModule,
        SharedModule,
        ChartGraphsModule,
        CKEditorModule,
        DialogModule,
        PowerModule,
        LandingPageModule,
        MenuModule,
        FormFieldModule,
    ],
    providers: [
        MYLocalStorage,
        WSService,
        SocketService,
        PostMessageService,
        AuthInterceptorService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptorService,
            multi: true
        },
        CacheInterceptor,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: CacheInterceptor,
            multi: true
        },
        { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
        AuthService,
        AuthGuardService,
        GuardService,
        MaintenanceService,
        DebugStore,
        AuthStore,
        UserStore,
        MaintenanceStore,
        DialogService,
        ExcelService,
        PdfService,
        CameraStore,
        CountryStore,
        ProvinceStore,
        CityStore,
        CookieService,
        CacheService,
        NotificationService,
        SiteStore
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
