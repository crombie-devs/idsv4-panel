import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { WSService } from 'src/services/ws.service';
import { Observable } from 'rxjs';

@Injectable()
export class MaintenanceService implements CanActivate {
  private can:boolean = true;
  constructor(public authService: AuthService, public router: Router, private ws: WSService) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      this.ws.testService().subscribe((res)=>{
        this.router.navigate(['private/dashboard']);
        this.can = false;
      });
      return this.can;
  }
}
