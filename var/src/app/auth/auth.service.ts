import { Injectable } from '@angular/core';
import { Permission } from 'src/modules/user/models/permission';
import { UserStore } from 'src/modules/user/stores/user.store';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { MaintenanceStore } from 'src/modules/maintenance/stores/maintenance.store';

@Injectable()

export class AuthService {

  public maintenance:number = 0;
  constructor(
    private astore: AuthStore,
    private ustore: UserStore, 
    private mstore: MaintenanceStore,
  ) {
    this.mstore.onChange.subscribe(res =>{
      if(res)
        this.maintenance = res;
    })
  }
  public isAuthenticated(): boolean {
    return this.astore.isLogged();   
  }
  public getPermissions(): Permission[] {  
    return this.ustore.getUser().permissions;
  }
}