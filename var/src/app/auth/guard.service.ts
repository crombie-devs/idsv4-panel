import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { WSService } from 'src/services/ws.service';
import { catchError, map} from 'rxjs/operators';
import { FLAG_UNRESTRICTED } from 'html2canvas/dist/types/css/syntax/tokenizer';
@Injectable()
export class GuardService implements CanActivate {

  private can:boolean = true;
  constructor(
    public authService: AuthService, 
    public router: Router,
    private ws: WSService
  ) {}
  
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      this.ws.testService().subscribe((res)=>{
      },
      error => {
        this.router.navigate(['maintenance']);
        this.can = false;
      });
      return this.can;
  }

}
