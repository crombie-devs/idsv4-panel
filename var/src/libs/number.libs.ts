export {}

declare global {
    interface Number {
        isUndefined(): boolean;
    }
}

Number.prototype.isUndefined = function(): boolean {
    return (this === null || this === undefined);
}