import moment from 'moment';


export class MYDate {

	private static timeToArray(time) {
		var time = time.split(":");
        while(time.length < 3)
          time.push("00");
        while(time.length > 3)
          time.pop();
          
        return time;
	}

	public static weekDayByDate(date): number {
		return ((date.getDay() + 6) % 7) + 1;
	}

	public static DatetimeToTimestamp(date, time): number {
		var arrayTime = MYDate.timeToArray(time);
    	var dateTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(), arrayTime[0], arrayTime[1], arrayTime[2], 0);
        
    	return dateTime.getTime();
	}

	public static clearTime(date: Date) {
		return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);
	}

	public static addDays(date: Date, numDays: number): Date {
		let newDate: Date = new Date(date.getTime());
		newDate.setDate(date.getDate() + numDays);
		return newDate;
	}

	public static dateToDiff(diff: number): Date {
		return moment().add(diff, 'ms').toDate();
	}

	public static Diffsecond(init: moment.Moment, end:moment.Moment): number {
		return Math.abs(end.diff(init, 'seconds'));
	}
}