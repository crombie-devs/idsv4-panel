import { ICustomer } from "src/modules/customer/models/customer";

export interface IDashboardMessage {
    id?: number;
    customer_id?: number;
    message?: string;
    date_on?: Date;
    date_off?: Date;
    created_at?: Date;
    updated_at?: Date;
    customer?: ICustomer;
}