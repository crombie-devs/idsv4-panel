import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  ViewEncapsulation,
  ElementRef,
  ViewChild,
} from '@angular/core';
import videojs from 'video.js';

@Component({
  selector: 'video-player-hls',
  templateUrl: './video-player-hls.component.html',
  styleUrls: ['./video-player-hls.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class VideoPlayerHlsComponent implements OnInit, OnDestroy {
  @Input() options: {
    fluid: boolean;
    aspectRatio: string;
    autoplay: boolean;
    controls: boolean;
    responsive: boolean;
    sources: {
      src: string;
      type: string;
    }[];
  };
  player: videojs.Player;

  @ViewChild('target', { static: true }) target: ElementRef;

  constructor() {}

  ngOnInit() {
    // instantiate Video.js
    this.player = videojs(
      this.target.nativeElement,
      this.options,
      function onPlayerReady() {
       }
      
    );
 }


  resetPlayer() {
    
    this.player.on
  } 


  ngOnDestroy() {
    // destroy player
    if (this.player) {
      this.player.dispose();
    }
  }
}
