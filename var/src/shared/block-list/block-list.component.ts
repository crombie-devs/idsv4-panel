import { ElementRef, QueryList, Renderer2, ViewChildren } from "@angular/core";
import { Component, Input, Output, EventEmitter } from "@angular/core";
//import { AnySoaRecord } from "dns";


@Component({
    selector: 'block-list',
    templateUrl: './block-list.component.html',
    styleUrls: ['./block-list.component.scss']
  })
export class BlockListComponent {
    
    
    @Input() items: any[]=[];
    @Input() title: string;
    @Input() editContentcategory: boolean;
	@Input() deletedContentcategory: boolean;
    @Input() selectCategory: boolean;
    @Input() multiple: boolean;
    @Input() btn: string;
 
 	@Output() onDeleted = new EventEmitter<number>();
	@Output() onUpdate = new EventEmitter<number>();
    @Output() onView = new EventEmitter<number>();
    @Output() onCreate = new EventEmitter<number>();
    @Output() onSelect =  new EventEmitter<any>();
    @ViewChildren('box') box : QueryList<ElementRef>; 

   
   

    constructor(private renderer: Renderer2){ 
    }


    select(index: number){
        let action = false;
        if(this.selectCategory) {
            
            this.onSelect.emit(index);
            
            if(!this.multiple) {
                this.deselect();
            }


            let elem = this.box.toArray()[index].nativeElement;

            if(elem.classList.contains('box')) {
                this.renderer.removeClass(elem, 'box');
                this.renderer.addClass(elem, 'box-selected');
                action = true;

            } else {
                this.renderer.removeClass(elem, 'box-selected');
                this.renderer.addClass(elem, 'box');
            }

        
           
        }
  }

    deselect(): void {
     this.box.toArray().forEach( (val, index) => {
     const elem =this.box.toArray()[index].nativeElement;
     this.renderer.removeClass(elem, 'box-selected');
     this.renderer.addClass(elem, 'box');
     });
    }


    delete(index: number): void {
        this.onDeleted.emit(index);
    }
    update(index: number): void {
        this.onUpdate.emit(index);
    }
    view(index: number):void {
        this.onView.emit(index);
    }

    add() : void {
        this.onCreate.emit();
    }

 
}

