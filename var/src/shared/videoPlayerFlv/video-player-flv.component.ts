import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  ViewEncapsulation,
  ViewChild,
  ElementRef
} from '@angular/core';
import FlvJs from 'flv.js';




@Component({
  selector: 'video-player-flv',
  templateUrl: './video-player-flv.component.html',
  styleUrls: ['./video-player-flv.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class VideoPlayerFlvComponent implements OnInit, OnDestroy {
  @Input() streaming_url;

  @ViewChild('videoPlayer', {static: true}) videoplayer: ElementRef;

  flvjs = FlvJs;
  constructor() {}

  ngOnInit() {
      console.log(this.streaming_url)
    if (this.flvjs.isSupported()) {
        
        var flvPlayer = this.flvjs.createPlayer({
            isLive: true,
            hasAudio: false,
            hasVideo: true,            
            type: 'flv',
            url: this.streaming_url
        });
        
        flvPlayer.attachMediaElement(this.videoplayer.nativeElement);
        flvPlayer.on(this.flvjs.Events.ERROR, function() {
            console.log('FlvPlayer error');
          //  location.reload();
        });
        flvPlayer.muted = true;
        flvPlayer.load();
        flvPlayer.play();
    }
    
 }


 

  ngOnDestroy() {
   
  }
}
