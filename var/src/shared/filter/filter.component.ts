import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from "@angular/core";
import { parseMetadata } from "@angular/localize/src/utils";
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


@Component({
    selector: 'app-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.scss']
  })


export class FilterComponent  implements OnInit, OnDestroy{
  param: string;  
  @Input() keyValues: any;
  @Input() arr: any[] = [];
  arrTem = [this.arr];
  @Output() responses: EventEmitter<any> = new EventEmitter(); 
  
   notesModelChanged: Subject<string> = new Subject<string>();
   notesModelChangeSubscription: Subscription;


  ngOnInit() {
    this.notesModelChangeSubscription = this.notesModelChanged
      .pipe(
        debounceTime(300),
        distinctUntilChanged()
      )
      .subscribe(newText => {
    
     });
  }

  ngOnDestroy() {
    this.notesModelChangeSubscription.unsubscribe();
  }


 


  


   

 
}

