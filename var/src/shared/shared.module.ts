import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlockListComponent } from './block-list/block-list.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FilterComponent } from './filter/filter.component';
import { VideoPlayerHlsComponent } from './videoPlayerHls/video-player-hls.component';
import { MaterialModule } from 'src/app/material.module';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CONFIG } from 'src/app/app.config';
import moment from 'moment';
import { VideoPlayerFlvComponent } from './videoPlayerFlv/video-player-flv.component';


export function HttpLoaderFactory(http: HttpClient) {
  let url = CONFIG.lang_url;  
  return new TranslateHttpLoader(http, `${url}/i18n/`, `.json?timestamp=${moment().valueOf()}`);
}
 
@NgModule({
  declarations: [
    BlockListComponent,
    FilterComponent,
    VideoPlayerHlsComponent,
    VideoPlayerFlvComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMatFileInputModule,
    MaterialModule,
    IvyCarouselModule
  ],
  exports: [ 
    FormsModule, 
    MaterialModule,
    TranslateModule,
    ReactiveFormsModule, 
    BlockListComponent,
    FilterComponent,
    VideoPlayerHlsComponent,
    VideoPlayerFlvComponent
  ]

})
export class SharedModule { 
  constructor(private translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('es');
    if(!sessionStorage.getItem('lang')){
      sessionStorage.setItem('lang', 'es');
      translate.use('es');
    }else{
      translate.use(sessionStorage.getItem('lang'));
    }
  }
}


