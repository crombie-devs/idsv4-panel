import { IMenu } from 'src/modules/menu/models/menu';

export const MENU: IMenu[] = [
    {
        title: 'Vision General',
        items: [
            {
                name: 'Dashboard',
                url: 'dashboard',
                icon: 'dashboard',
                permission: ''
            }
        ]
    },
    {
        title: 'Gestión',
        items: [
            {
                name: 'Cartelera',
                url: 'asset/list',
                icon: 'insert_drive_file',
                permission: 'get.assets',
                has_emissions: true,
            },
            {
                name: 'Crear playlist',
                url: 'content/create',
                icon: 'playlist_play',
                permission: 'get.contents',
                has_emissions: true
            },
            {
                name: 'Editar playlist',
                url: 'playlists',
                icon: 'preview',
                permission: 'get.contents.order',
                has_emissions: true
            },
            {
                name: 'Cámaras',
                url: 'camera/list',
                icon: 'videocam',
                permission: 'get.cameras',
                has_influxes: true,
            },
            {
                name: 'Circuitos de reprod.',
                url: 'playcircuit/list',
                icon: 'explore',
                permission: 'get.playcircuits',
                has_emissions: true
            },
            {
                name: 'Áreas de reproducción',
                url: 'playareas',
                icon: 'web',
                permission: 'get.playareas',
                has_emissions: true
            },
            {
                name: 'Eventos dinamicos',
                url: 'dynamic-events',
                icon: 'web',
                permission: 'get.dynamic-events.*',
                has_emissions: true
            },
        ]
    },
    {
        title: 'Mensajes',
        items: [
            {
                name: 'Listado mensajes',
                url: 'mensaje/list',
                icon: 'bubble_chart',
                // permission: 'get.message.*',

                has_emissions: true
            },
            {
                name: 'Enviar mensajes',
                url: 'mensaje/add',
                icon: 'bubble_chart',
                // permission: 'get.message.*',
                has_emissions: true
            },
            {
                name: 'Listado people',
                url: 'peoples/list',
                icon: 'bubble_chart',
                // permission: 'get.people.*',
                has_emissions: true
            },
            {
                name: 'Crear people',
                url: 'peoples/add',
                icon: 'bubble_chart',
                //  permission: 'get.people.*',
                has_emissions: true
            },



        ]
    },
    {
        title: 'Análisis',
        items: [
            {
                name: 'Mensajes',
                url: 'mensaje/stats',
                icon: 'bar_chart',
                permission: '',
                has_emissions: true,
            },
            {
                name: 'Emisiones',
                url: 'analisis/emisiones',
                icon: 'bar_chart',
                permission: '',
                has_emissions: true,
            },
            {
                name: 'Emisiones & Ventas',
                url: 'analisis/emisionesxhora',
                icon: 'multiline_chart',
                permission: '',
                has_emissions: true,
                has_sales: true,
                exclude_customers: [113]
            },
            {
                name: 'Afluencias',
                url: 'analisis/afluencias',
                icon: 'bubble_chart',
                permission: '',
                has_influxes: true,
            },
            {
                name: 'Perfiles Socioeconómicos',
                url: 'analisis/socioeconomicos',
                icon: 'public',
                permission: ''
            },
            {
                name: 'Asociaciones',
                url: 'recomendacion/asociaciones',
                icon: 'shuffle',
                permission: '',
                has_algorithms: true,
            },
            {
                name: 'Recomendaciones',
                url: 'recomendacion/recomendaciones',
                icon: 'insights',
                permission: '',
                has_algorithms: true,
            },
            {
                name: 'Carga autónoma de datos',
                url: 'recomendacion/data',
                icon: 'insights',
                permission: '',
                has_emissions: true,
                has_sales: true
            }
        ]
    },
    {
        title: 'Administración',
        items: [
            {
                name: 'Usuarios',
                url: 'user/list',
                icon: 'person',
                permission: 'get.users',
            },
            {
                name: 'Clientes',
                url: 'customer/list',
                icon: 'business_center',
                permission: 'get.customers',
            },
            {
                name: 'Centros',
                url: 'site/list',
                icon: 'business',
                permission: 'put.site.*',
            },
            {
                name: 'Aplicaciones',
                url: 'apps',
                icon: 'apps',
                permission: 'get.apps',
            },
            {
                name: 'Cámaras',
                url: 'camera/list',
                icon: 'videocam',
                permission: 'get.cameras',
                has_influxes: true
            },
            {
                name: 'Visores',
                url: 'player/list',
                icon: 'tv',
                permission: '',
                has_emissions: true
            },
            {
                name: 'Contenidos',
                url: 'content/list',
                icon: 'playlist_play',
                permission: 'get.contents',
                has_emissions: true
            },
            {
                name: 'Audios',
                url: 'audios',
                icon: 'playlist_play',
                permission: 'get.audios',
                has_emissions: true,
                has_audioasync: true
            },
            {
                name: 'Asociar tags a Productos',
                url: 'products-tags',
                icon: 'local_offer',
                permission: 'delete.tag.*',
                has_sales: true
            },
            {
                name: 'Etiquetas',
                url: 'tag/create',
                icon: 'local_offer',
                permission: 'delete.tag.*',
                has_emissions: true
            },
            {
                name: 'Categorias',
                url: 'contentcategories',
                icon: 'category',
                permission: 'get.content-categories',
                has_emissions: true
            },
            {
                name: 'Ubicaciones',
                url: 'indoorlocations',
                icon: 'location_on',
                permission: 'get.indoorlocations',
                has_emissions: true
            },
            {
                name: 'Preview',
                url: 'dynamic-events/events-preview',
                icon: 'movie',
                permission: 'get.dynamic-events.*',
                has_emissions: true
            },

            {
                name: 'Formatos',
                url: 'format/list',
                icon: 'settings_overscan',
                permission: 'post.formats',
            },
            {
                name: 'Plantillas',
                url: 'template/list',
                icon: 'edit',
                permission: 'post.template.*',
            },
            {
                name: 'Idiomas',
                url: 'languages',
                icon: 'language',
                permission: 'post.languages',
            },
            {
                name: 'Categorías-Productos',
                url: 'products-categories',
                icon: 'inventory',
                permission: 'post.product',
            }
        ]
    },
    {
        title: 'Algoritmos',
        items: [
            {
                name: 'Variables',
                url: 'algorithm/variables',
                icon: 'article',
                permission: '',
                has_emissions: true,
                has_sales: true,
                exclude_customers: [113]
            },
            {
                name: 'Resultados',
                url: 'algorithm/resultados',
                icon: 'article',
                permission: '',
                has_emissions: true,
                has_sales: true,
                exclude_customers: [113]
            },
            {
                name: 'Test Primetime',
                url: 'algorithm/test-primetime',
                icon: 'article',
                permission: '',
                has_emissions: true,
                has_sales: true,
                exclude_customers: [113]
            },
            {
                name: 'Test Asociaciones',
                url: 'algorithm/test-asociaciones',
                icon: 'article',
                permission: '',
                has_emissions: true,
                has_sales: true,
                exclude_customers: [113]
            },
        ]
    },
    {
        title: 'Panel de control',
        items: [
            {
                name: 'Notificationes',
                url: 'support/notifications',
                icon: 'notification_important',
                permission: 'post.notification',
                exclude_customers: [113]
            },
            {
                name: 'Camaras',
                url: 'support/cameras',
                icon: 'videocam',
                permission: 'post.support',
                exclude_customers: [113]
            },
            {
                name: 'Players',
                url: 'support/players',
                icon: 'tv',
                permission: 'post.support',
                exclude_customers: [113]
            }
        ]
    },
    {
        title: 'Tickets',
        items: [
            {
                name: 'Ticket dashboard',
                url: 'ticket/dashboard',
                icon: 'web',
                permission: 'get.ticket.*',
                has_emissions: true
            },
            // {
            //     name: 'Ticket abiertos',
            //     url: 'ticket/list/1',
            //     icon: 'web',
            //     permission: 'get.ticket.*',
            //     has_emissions: true
            // },
            // {
            //     name: 'Ticket cerrados',
            //     url: 'ticket/list/0',
            //     icon: 'web',
            //     permission: 'get.ticket.*',
            //     has_emissions: true
            // },
            {
                name: 'Añadir categorias',
                url: 'ticket/category',
                icon: 'web',
                permission: 'get.ticket.*',
                has_emissions: true
            },
            {
                name: 'Añadir prioridad',
                url: 'ticket/priority',
                icon: 'web',
                permission: 'get.ticket.*',
                has_emissions: true
            },
            {
                name: 'Añadir estado',
                url: 'ticket/status',
                icon: 'web',
                permission: 'get.ticket.*',
                has_emissions: true
            },
            {
                name: 'Añadir departamento',
                url: 'ticket/department',
                icon: 'web',
                permission: 'get.ticket.*',
                has_emissions: true
            },
            {
                name: 'Añadir responsable',
                url: 'ticket/responsible',
                icon: 'web',
                permission: 'get.ticket.*',
                has_emissions: true
            },
        ]
    },
    {
        title: "FAQ's",
        items: [
            {
                name: "FAQ's",
                url: 'faqs/faqs',
                icon: 'article',
                permission: '',
                has_emissions: true,
                has_sales: true,
                exclude_customers: [113]
            }
        ]
    }
];