import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MENU } from './menu.config';
import { Menu } from '../../models/menu';
import { User } from 'src/modules/user/models/user';
import { UserStore } from 'src/modules/user/stores/user.store';


@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})

export class MenuComponent implements OnInit {

  public showSubmenu: boolean = false;
  public showSubSubMenu: boolean = false;
  
  public panelOpenState = false;
  public status: boolean = false;
  public menus: Menu[] = [];
  public me: User;
  public titleBreadcrumbs: string;

  public step = 0;

  constructor(
    private activatedRoute: ActivatedRoute,private ustore: UserStore) 
  {
    this.titleBreadcrumbs = this.activatedRoute.snapshot.routeConfig.children[0].path;
    
    this.menus = MENU.map(m => new Menu(m));
    
    this.ustore.onChange.subscribe(user => {
      this.menus = this.menus.filter(m => 
         m.permissionNames().some(p => { 
          if(!p) return true;
          return this.ustore.hasPermissionByName(p);
        }));

      this.menus = this.menus.map(m => {
        m.items = m.items.filter(i => {
          let permission:boolean = true;
          let has_influxes: boolean = true;
          let has_sales:boolean = true;
          let has_audioasync = true;
          let has_emissions: boolean = true;
          let has_algorithms: boolean = true;
          if(i.permission) 
            permission = this.ustore.hasPermissionByName(i.permission);
          if(i.has_influxes)
            has_influxes = this.ustore.has_influxes();
          if(i.has_sales)
            has_sales = this.ustore.has_sales();
          if(i.has_emissions) {
              has_emissions = this.ustore.has_emissions();
          }
          if(i.has_audioasync) {
             has_audioasync = this.ustore.has_audioasync(); 
          }
          if(i.has_algorithms){
            has_algorithms = this.ustore.has_algorithms();
          }
          if(i.permission) {
            permission = this.ustore.hasPermissionByName(i.permission);
            if (this.ustore.getRoles().every(r => r.id == 5)) {
              if(i.permission == 'get.assets') {
                permission = false;  
              }
            }
          } else if (this.ustore.getRoles().every(r => r.id == 5)) {
            permission = false;
          } 
          return (permission && has_influxes && has_sales && has_emissions && has_audioasync && has_algorithms)
        });
        return m;
      });
    });
  }
  
  public ngOnInit(): void {
      this.me = this.ustore.getUser();
  }
  
  public hasPermissions(elements){
    let permissions:string[] = [];
    elements.map(element => {
      if(element.permissions)
        permissions.push(element.permissions)
      if(element.submenus)
        element.submenus.map(el => {
          if(el.permissions){
            permissions.push(el.permissions);
          }
        })
    });
    if(permissions.length > 0)
      return permissions.some(permission =>{
        return this.hasPermission(permission);
      })
    else
      return true
  }
  public hasPermission(permission){
    return this.me.permissions.some((perm) => perm.name === permission)
  }
}

