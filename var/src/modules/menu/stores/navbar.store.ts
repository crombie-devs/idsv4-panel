import { Injectable } from "@angular/core";
import { loadavg } from "os";
import { BehaviorSubject } from "rxjs";
import { environment } from "src/environments/environment";
import { WSService } from 'src/services/ws.service';
import { CameraService } from 'src/modules/camera/services/camera.service';
import { INavbar, Navbar } from '../models/navbar';

const STORE_KEY: string = 'navbar.store.ids';

@Injectable()
export class NavbarStore {

	private data: Navbar = null;
	public onChange: BehaviorSubject<Navbar> = new BehaviorSubject(null);

	constructor(private ws: WSService, private _camera: CameraService) {
		if(environment.localstorage){
			let store = sessionStorage.getItem(STORE_KEY);
			if(store) {
				const navbar: INavbar = JSON.parse(store); 
				this.set(navbar);
			} else 
				this.load();
        }else
			this.load();
	}

	private load() {
		let navBar: INavbar = {
			sites: 0,
			cameras: 0,
			players: 0
		};
		this.ws.siteToSelect().subscribe(result => {
			navBar.sites = result.data.length;
			this.set(navBar)
		});
		this.ws.playersToSelect().subscribe(result => {
			navBar.players = result.data.length;
			this.set(navBar);
		});
		this._camera.toSelect().subscribe(cameras => {
			navBar.cameras = cameras.length;
			this.set(navBar);
		});
	}

	private save(data: INavbar) {
		this.data = (data) ? new Navbar(data) : null;
		if(environment.localstorage)
			sessionStorage.setItem(STORE_KEY, JSON.stringify(data));
	}

	public get(): Navbar {
		return this.data;
	}

	public set(data: INavbar) {
		this.save(data);
		this.onChange.next(this.data);
	}

	public delete() {
		this.save(null);
		this.onChange.next(this.data);
	}
}
