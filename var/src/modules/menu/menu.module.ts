import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FlexModule } from '@angular/flex-layout';

import { MaterialModule } from 'src/app/material.module';

import { MenuComponent } from './components/menu/menu.component';
import { NavbarStore } from './stores/navbar.store';
import { SharedModule } from 'src/shared/shared.module';
@NgModule({
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [MenuComponent],
  imports: [
    CommonModule,
    RouterModule,
    FlexModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents:[],
  providers: [NavbarStore],
  exports:[MenuComponent]
})
export class MenuModule {}
