import { Submenu } from "./submenu";

export interface IItem {
  name: string;
  url: string;
  icon: string  
  permission?: string;
  has_influxes?: boolean;
  has_emissions?:boolean;
  has_sales?: boolean;
  has_audioasync?: boolean;
  exclude_customers?: number[];
  has_algorithms?: boolean;
}

export class Item implements IItem {
  name: string;
  url: string;
  icon: string  
  permission?: string;
  has_influxes?: boolean;
  has_emissions?: boolean;
  has_sales?: boolean;
  has_audioasync?: boolean;
  exclude_customers?: number[];
  has_algorithms?: boolean;

	constructor(obj?: IItem) {
		if(!obj)
      return;

    this.name = obj.name;
    this.url = obj.url;
    this.icon = obj.icon;
    this.permission = obj.permission;
    this.has_influxes = obj.has_influxes;
    this.has_emissions = obj.has_emissions;
    this.has_sales = obj.has_sales; 
    this.has_audioasync = obj.has_audioasync;
    this.exclude_customers = obj.exclude_customers;
    this.has_algorithms = obj.has_algorithms;
  }
}
