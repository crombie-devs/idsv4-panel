import { IItem, Item } from "./item";

export class IMenuLang {
  lang:string;
  menu:IMenu[];
}

export class IMenu {
  title: string;
  items: IItem [];
}

export class Menu implements IMenu {
  title: string;
  items: Item [];
  

	constructor(obj?: IMenu) {
		if(!obj)
      return;

    this.title = obj.title;
    this.items = obj.items;    
  }

  permissionNames(): string[] {
    return this.items.map(i => i.permission);
  }
}
