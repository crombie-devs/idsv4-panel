export interface INavbar {
  sites: number;
  players: number;
  cameras: number;
}

export class Navbar implements INavbar {
  sites: number;
  players: number;
  cameras: number;

	constructor(obj?: INavbar) {
		if(!obj)
      return;

    this.sites = obj.sites;
    this.players = obj.players;
    this.cameras = obj.cameras;   
  }
}
