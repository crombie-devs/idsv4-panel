export class Submenu {
  name: string;
  url: string;
  permissions?: string; 
 
	constructor(obj?: any) {
		if(!obj)
      return;

    this.name = obj.name;
    this.url = obj.url;
    this.permissions = obj.permissions;
    
  }
}
