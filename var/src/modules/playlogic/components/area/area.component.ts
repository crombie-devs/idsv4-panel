import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { PlayArea } from 'src/modules/playarea/models/playarea';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { Player } from 'src/modules/player/models/player';
import { PlayerStore } from 'src/modules/player/stores/player.store';
import { ContentStore } from 'src/modules/content/stores/content.store';
import { Content } from 'src/modules/content/models/content';
@Component({
    selector: 'playlogics-area',
    templateUrl: './area.component.html',
    styleUrls: ['./area.component.scss']
  })
  
  export class PlaylogicsAreaComponent {

    loading: boolean = true;
    area: PlayArea;
    circuit: PlayCircuit;
    player: Player;
    show: boolean = false;
    @Input() date: string;
    @Output() onChange: EventEmitter<PlayArea> = new EventEmitter<PlayArea>();

    private listener: Subscription;

    public contents:Content[] = [];
    public emittingContent:string;
    constructor(
        private store: PlayerStore,
        private cstore: ContentStore
    ) {
        this.listener = fromEvent(window, "message").subscribe((e: MessageEvent) => {     
            this.emittingContent = null;
            if(e.data.key === "player_text.loaded"){
                this.show = true;
            }
            if(e.data.key === 'is_play'){
                this.changeContent(e.data.value);
            }     
        });   
        this.cstore.onChange.subscribe(contents => {
            this.contents = contents.map(c => new Content(c));
        })
    }

    ngOnDestroy(){
        this.listener.unsubscribe();        
    }

    changeArea(area: PlayArea) {
        this.area = area;
        this.onChange.emit(area);
        this.randomByArea();
    }

    changeCircuit(circuit: PlayCircuit) {
        this.circuit = circuit;
        this.update();
    }

    private randomByArea() {
        if(!this.area){ return; }
        let players = this.store.get().filter(p => this.area.isEquals(p.area));
        
        if(players.length > 0)
            this.player = this.random(players);
        else    
            this.player = this.random(this.store.get());
          
    }
    
    private random(players: Player[]): Player {
        let index = Math.floor(Math.random() * players.length);
        return players[index];
    }

    public update(){
        this.player = null;
        setTimeout(() => {
            this.randomByArea();
        }, 0);
    }

    public changeContent(content:string){
        const con = parseInt(content)
        const c = this.contents.filter(c => c.idcontent === con)[0];
        if(c)
            this.emittingContent = `${c.idcontent} - ${c.name}`;
        else
            this.emittingContent = null;
    }
}
