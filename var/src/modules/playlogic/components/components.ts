
import { PlaylogicsAreaComponent } from './area/area.component';
import { PlaylogicOrderComponent } from './order/playlogic/order.component';
import { PlaylogicsPlayerComponent } from './player/player.component';
import { PlaylogicSidenavComponent } from './sidenav/sidenav.component';
import { ContentOrderComponent } from './order/content/order.component';

export const COMPONENTS = [
    PlaylogicsAreaComponent,
    PlaylogicOrderComponent,
    PlaylogicsPlayerComponent,
    PlaylogicSidenavComponent,
    ContentOrderComponent
];
