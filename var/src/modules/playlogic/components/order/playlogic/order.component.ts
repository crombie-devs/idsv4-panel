import { Component, EventEmitter, Input, Output } from "@angular/core";
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { IPlaylogic, Playlogic } from 'src/modules/playlogic/models/playlogic';
import { WSService } from 'src/services/ws.service';
import { ContentCategory } from 'src/modules/content/models/category';
import { PlaylogicStore } from 'src/modules/playlogic/stores/playlogic.store';
import { PlayArea } from 'src/modules/playarea/models/playarea';
import { Player } from 'src/modules/player/models/player';
import { UserStore } from "src/modules/user/stores/user.store";
import { User } from "src/modules/user/models/user";
import { DebugStore } from "src/stores/debug.store";
import { CacheService, ICacheConfig } from "src/services/cache.service";
import { EventService } from "src/shared/event.service";
import { PlayLogicService } from "src/modules/playlogic/services/playlogic.service";



@Component({
    selector: 'playlogic-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss']
})
export class PlaylogicOrderComponent {

    @Output() onSelect: EventEmitter<ContentCategory> = new EventEmitter();
    @Input() playlogics: Playlogic[] = [];
    @Input() area: PlayArea;
    @Input() player: Player;
    @Input() disabled: boolean = false;

    loading: boolean = false;
    public me: User;

    constructor(private ws: WSService, 
        private store: PlaylogicStore,
        private ustore: UserStore,
        private events: EventService,
		
       ){
            this.ustore.onChange.subscribe(result => { this.me = result; });
         
            
        }

    drop(event: CdkDragDrop<Playlogic[]>) {
        moveItemInArray(this.playlogics, event.previousIndex, event.currentIndex);	
        this.playlogics = event.container.data;
    }
    
    update() {
        if(this.area)
            this.updateToArea();
        else if(this.player)
            this.updateToPlayer();
    }

    private updateToArea() {
     
        this.loading = true;
        let data: IPlaylogic[] = this.playlogics.map((pl, index) => { return {idcustomer: pl.idcustomer, idtype: pl.idtype, idcategory: pl.idcategory, order: index}; }); 
        this.ws.replacePlaylogicToArea(this.area.idarea, data).subscribe(result => {
            console.info('ReplacePlaylogic toArea', result);
            this.loading = false;
            if(this.me && this.me.customer){
                let config: ICacheConfig = {idarea: this.area.idarea, idcustomer: this.me.customer.idcustomer};                           
                setTimeout(() => {
                  console.log('publish', config);
                  this.events.publish('area:change', config);     
                }, 1000);
            }
            this.store.reload();
        });
        
    }

    private updateToPlayer() {
  
        this.loading = true;
        let data: IPlaylogic[] = this.playlogics.map((pl, index) => { return {idcustomer: pl.idcustomer, idtype: pl.idtype, idcategory: pl.idcategory, order: index}; });
        this.ws.replacePlaylogicToPlayer(this.player.idplayer, data).subscribe(result => {
            console.info('ReplacePlaylogic toPlayer', result);
            this.loading = false;
            if(this.me && this.me.customer){
                let config: ICacheConfig = {idcustomer: this.me.customer.idcustomer};
                if(this.me.site)
                  config = {idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer};              
                setTimeout(() => {
                  console.log('publish', config);
                  this.events.publish('content:change', config);     
                }, 1000);
            }
            this.store.reload();
        });

    }
    
    view(category: ContentCategory) {
        this.onSelect.emit(category);
    }
}