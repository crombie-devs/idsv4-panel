import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from "@angular/core";
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { WSService } from 'src/services/ws.service';
import { ContentCategory } from 'src/modules/content/models/category';
import { Content, IContent } from 'src/modules/content/models/content';
import { ContentOrderStore } from 'src/modules/content/stores/order.store';
import { PlayArea } from 'src/modules/playarea/models/playarea';
import { Player } from 'src/modules/player/models/player';
import { ContentStore } from 'src/modules/content/stores/content.store';
import moment from 'moment';
import { PlayCircuit } from "src/modules/playcircuit/models/playcircuit";
import { UserStore } from "src/modules/user/stores/user.store";
import { User } from "src/modules/user/models/user";
import { DebugStore } from "src/stores/debug.store";
import { CacheService, ICacheConfig } from "src/services/cache.service";
import { EventService } from "src/shared/event.service";
import { PlayLogicService } from "src/modules/playlogic/services/playlogic.service";
import { Router } from "@angular/router";

@Component({
    selector: 'content-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss']
})
export class ContentOrderComponent implements OnInit, OnChanges {

    @Output() onChange: EventEmitter<void> = new EventEmitter();
    @Output() onUpdate: EventEmitter<void> = new EventEmitter();
    @Input() category: ContentCategory = null;
    @Input() area: PlayArea;
    @Input() player: Player;
    @Input() circuit: PlayCircuit;
    @Input() disabled: boolean = false;
    @Input() validContents: Content[] = [];
    @Input() date: any;

    all_contents: Content[] = [];
    contents: Content[] = [];
    loading: boolean = false;

    public me: User;

    constructor(
        private ws: WSService,
        private contentStore: ContentStore,
        private orderStore: ContentOrderStore,
        private ustore: UserStore,
        private events: EventService,
        private router: Router) {
        this.ustore.onChange.subscribe(result => { this.me = result; });
    }

    ngOnInit() {
        this.reload();
        this.orderStore.onChange.subscribe(result => {
            if (result)
                this.refresh();
        });
    }

    reload(date?) {
        let d = this.date;
        if (date) {
            d = date;
        }
        this.loading = true;
        let today = moment().format('YYYY-MM-DD');
        if (d) {
            today = moment(d).format('YYYY-MM-DD');
        }
        this.ws.contents(today).subscribe(result => {
            this.loading = false;
            if (result.success) {
                this.all_contents = result.data.map((c: IContent) => new Content(c));
                this.refresh();
            }
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.category)
            this.refresh();
    }

    edit(idcontent: number) {
        this.router.navigate(['/private/content/edit', idcontent, true]);
    }

    checkCircuit(content: Content): boolean {
        if (!this.circuit && content.playcircuits.length === 0) { return true; }
        return content.playcircuits.some(c => c.idcircuit === this.circuit.idcircuit);
    }
    checkHide(content: Content): boolean {
        if (this.validContents.length === 0) { return false; }
        return this.validContents.some(c => c.idcontent === content.idcontent);
    }

    private refresh() {
        this.contents = this.all_contents;
        if (this.category)
            this.contents = this.contents.filter(c => (c.category && c.category.isEquals(this.category)));
        if (this.area) {
            this.contents = this.contents.filter(c => c.playareas.some(pa => pa.isEquals(this.area)));
            this.contents.map((content, i) => {
                content.assets.map(asset => {
                    if (asset.format.idformat === this.area.format.idformat) {
                        this.contents[i].thumbnail_url = asset.cover_url;
                    }
                })
            })
        }
        if (this.player)
            this.contents = this.contents.filter(c => c.playareas.some(pa => pa.isEquals(this.player.area)));

        this.order();
    }

    private order() {
        const orders = this.orderStore.filter(this.area, this.player);
        if (this.area) {
            this.contents.map((content, i) => {
                const c = content.orders.filter(o => o.idarea === this.area.idarea);
                if (c[0])
                    this.contents[i].order = c[0].order;
            })
        }
        if (this.player) {
            this.contents.map((content, i) => {
                const c = content.orders.filter(o => o.idplayer === this.player.idplayer);
                if (c[0])
                    this.contents[i].order = c[0].order
            })
        }

        this.contents = this.contents.sort((a, b) => a.order - b.order);
    }

    drop(event: CdkDragDrop<Content[]>) {
        moveItemInArray(this.contents, event.previousIndex, event.currentIndex);
        this.contents = event.container.data;
    }

    changeStatus(content: Content) {
        this.loading = true;
        const data = { "status": content.status };
        this.ws.updateContent(content.idcontent, data).subscribe(result => {
            this.loading = false;
            if (this.me && this.me.customer) {
                let config: ICacheConfig = { idcustomer: this.me.customer.idcustomer };
                if (this.me.site)
                    config = { idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer };
                setTimeout(() => {
                    this.events.publish('content:change', config);
                }, 1000);
            }
            this.contentStore.reload();
        });
    }

    update() {
        if (this.area)
            this.updateToArea();
        else if (this.player)
            this.updateToPlayer();

        this.onUpdate.emit();
    }

    close() {
        this.onChange.emit();
    }

    private updateToArea() {
        this.loading = true;
        let data: any = this.contents.map((c, index) => { return { idcontent: c.idcontent, order: index }; });
        this.ws.replaceContentOrderToArea(this.area.idarea, this.category.idcategory, data).subscribe(result => {
            this.reload();
            if (this.me && this.me.customer) {
                let config: ICacheConfig = { idarea: this.area.idarea, idcustomer: this.me.customer.idcustomer };
                setTimeout(() => {
                    this.events.publish('area:change', config);
                }, 1000);
            }
            this.close();
        });
    }

    private updateToPlayer() {
        this.loading = true;
        let data: any = this.contents.map((c, index) => { return { idcontent: c.idcontent, order: index }; });
        this.ws.replaceContentOrderToPlayer(this.player.idplayer, this.category.idcategory, data).subscribe(result => {
            this.reload();
            if (this.me && this.me.customer) {
                let config: ICacheConfig = { idcustomer: this.me.customer.idcustomer };
                if (this.me.site)
                    config = { idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer };
                setTimeout(() => {
                    this.events.publish('content:change', config);
                }, 1000);
            }
            this.close();
        });
    }

}