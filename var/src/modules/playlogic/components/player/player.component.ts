import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PlayArea } from 'src/modules/playarea/models/playarea';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { Player } from 'src/modules/player/models/player';
import { User } from 'src/modules/user/models/user';
import { Site } from 'src/modules/site/models/site';
import { PlayerStore } from 'src/modules/player/stores/player.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { fromEvent, Subscription } from 'rxjs';
import { ContentStore } from 'src/modules/content/stores/content.store';
import { Content } from 'src/modules/content/models/content';
@Component({
    selector: 'playlogics-player',
    templateUrl: './player.component.html',
    styleUrls: ['./player.component.scss']
  })
  
  export class PlaylogicsPlayerComponent {

    loading: boolean = false;
    circuit: PlayCircuit = null;
    site: Site = null;
    player: Player = null;
    area: PlayArea = null;
    show: boolean = false;
    me:User;
    private listener: Subscription;
    @Output() onChange: EventEmitter<PlayArea> = new EventEmitter<PlayArea>();
    @Input() date: string;

    public contents:Content[] = [];
    public emittingContent:string;
    constructor(
        private store: PlayerStore,
        private ustore: UserStore,
        private cstore: ContentStore
        ) {
        this.listener = fromEvent(window, "message").subscribe((e: MessageEvent) => {                      
            if(e.data.key === "player_text.loaded"){
                this.show = true;
            }
            if(e.data.key === 'is_play'){
                this.changeContent(e.data.value);
            }      
        });    
        this.ustore.onChange.subscribe(user => {
            if(user){
                this.me = user;
            }
        })
        this.cstore.onChange.subscribe(contents => {
            this.contents = contents.map(c => new Content(c));
        })
    }
    

    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        if(this.me.site){
            this.changeSite(this.me.site);
        }
    }

    ngOnDestroy(){
        this.listener.unsubscribe();        
    }

    changeArea(area: PlayArea) {
        this.area = area;
        this.onChange.emit(area);
        this.randomByArea();
    }

    private randomByArea() {
        if(!this.area){ return; }
        let players = this.store.get().filter(p => this.area.isEquals(p.area));
        if(players.length > 0)
            this.player = this.random(players);
        else    
            this.player = this.random(this.store.get());
    }
    
    private random(players: Player[]): Player {
        let index = Math.floor(Math.random() * players.length);
        return players[index];
    }


    changeSite(site: Site) {
        this.site = site;
        this.loading = false; 
    }
    
    changePlayer(player: Player) {        
        this.player = player;
        this.loading = false; 
    }

    changeCircuit(circuit: PlayCircuit) {
        this.circuit = circuit;
    }
    public changeContent(content:string){
        const con = parseInt(content)
        const c = this.contents.filter(c => c.idcontent === con)[0];
        this.emittingContent = `${c.idcontent} - ${c.name}`;
    }
}