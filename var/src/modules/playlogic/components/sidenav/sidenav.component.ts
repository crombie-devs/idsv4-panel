import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from "@angular/core";
import { PlaylogicStore } from 'src/modules/playlogic/stores/playlogic.store';
import { Playlogic } from 'src/modules/playlogic/models/playlogic';
import { ContentCategory } from 'src/modules/content/models/category';
import { MatDrawer } from '@angular/material/sidenav';
import { PlayArea } from 'src/modules/playarea/models/playarea';
import { Player } from 'src/modules/player/models/player';
import { PlayCircuit } from "src/modules/playcircuit/models/playcircuit";
import { Content } from "src/modules/content/models/content";
import { PlayLogicService } from "../../services/playlogic.service";
import { ContentOrder } from "src/modules/content/models/order";
import { ContentOrderComponent } from "../order/content/order.component";



@Component({
    selector: 'playlogic-sidenav',
    templateUrl: './sidenav.component.html',
    styleUrls: ['./sidenav.component.scss']
  })
export class PlaylogicSidenavComponent implements OnInit, OnChanges {

    @ViewChild('sidenav') sidenav: MatDrawer;
    @ViewChild(ContentOrderComponent) order: ContentOrderComponent;
    @Output() onUpdate: EventEmitter<void> = new EventEmitter();
    @Input() area: PlayArea = null;
    @Input() player: Player = null;
    @Input() circuit: PlayCircuit = null;
    @Input() disabled: boolean = false;
    @Input() date: any;

    category: ContentCategory = null;
    playlogics: Playlogic[] = [];

    validContents: Content[] = [];

    constructor(private store: PlaylogicStore, private _playLogic: PlayLogicService) {
        if (window.addEventListener) { 
            window.addEventListener("message", (e) => {    
                if(e.data.key === "contents"){
                    this.validContents = e.data.value.map(c => new Content(c)); 
                }                 
            }, false); 
        }
    }

    ngOnInit() {
        this._playLogic.$reload.subscribe( date => {
            this.date = date;
            if(this.order) {
                this.order.reload(date);
            }
        });
        this.store.onChange.subscribe(result => {
            this.refresh();
            this.update();
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        this.refresh();
    }

    private refresh() {
        try{ this.sidenav.close(); }catch(ex){}
        this.playlogics = this.store.filter(this.area, this.player);
    }

    selectCategory(category: ContentCategory) {
        this.category = category;
        try{ this.sidenav.open(); }catch(ex){}
    }

    public update(){
        this.onUpdate.emit();
    }
}