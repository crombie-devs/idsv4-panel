import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { IPlaylogic, Playlogic } from 'src/modules/playlogic/models/playlogic';
import { IPlayArea, PlayArea } from 'src/modules/playarea/models/playarea';
import { IPlayer, Player } from 'src/modules/player/models/player';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'playlogics.store.ids';

@Injectable()
export class PlaylogicStore {

    public onChange: BehaviorSubject<Playlogic[]> = new BehaviorSubject([]);
    private data: Playlogic[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const items: IPlaylogic[] = JSON.parse(store); 
                this.set(items);
            } else 
                this.load();
        } else
            this.load();
    }

    public get(): Playlogic[] {
        return this.data;
    }

    public set(items: IPlaylogic[]) {
        this.save(items);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    public filter(area: IPlayArea, player: IPlayer): Playlogic[] {
        let playlogics = this.data;
        if(player) {
            playlogics = this.data.filter(logic => logic.idplayer === player.idplayer);
        }
        else if(area) {   
            playlogics = this.data.filter(logic => logic.idarea === area.idarea);
        }
    
        return playlogics;
    }

    private load() {
        this.ws.playlogicToSelect().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }
        });
    }

    private save(items: IPlaylogic[]) {
        this.data = items.map(a => new Playlogic(a));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(items));
    }
}