import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class PlayLogicService  {

  reload: Subject<string> = new Subject();
  $reload = this.reload.asObservable();


  setDate(date: string) {
    this.reload.next(date);
  }

}


