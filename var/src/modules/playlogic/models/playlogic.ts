import { ContentCategory, IContentCategory } from "src/modules/content/models/category";
import { IPlayArea, PlayArea } from "src/modules/playarea/models/playarea";
import { Customer, ICustomer } from "src/modules/customer/models/customer";
import { ContentType, IContentType } from "src/modules/content/models/type";

export interface IPlaylogic {
    idlogic?: number;
    order: number;

    idplayer?: number;
    idarea?: number;
    area?: IPlayArea;

    idcustomer: number;
    customer?: ICustomer;

    idtype: number;
    type?: IContentType;

    idcategory: number;
    content_category?: IContentCategory;
}

export class Playlogic implements IPlaylogic {
    idlogic: number;
    order: number;

    idplayer?: number;
    idarea?: number;
    area?: PlayArea;

    idcustomer: number;
    customer?: Customer;

    idtype: number;
    type?: ContentType;

    idcategory: number;
    content_category?: ContentCategory;
    
    constructor(obj?: IPlaylogic) {
        if(!obj)
            return;
            
        this.idlogic = obj.idlogic;
        this.order = obj.order;

        this.idplayer = obj.idplayer;
        this.idarea = obj.idarea;
        if(obj.area)
            this.area = new PlayArea(obj.area);

        this.idcategory = obj.idcategory;
        if(obj.content_category)
            this.content_category = new ContentCategory(obj.content_category);

        this.idcustomer = obj.idcustomer;
        if(obj.customer)
            this.customer = new Customer(obj.customer);

        this.idtype = obj.idtype;
        if(obj.type)
            this.type = new ContentType(obj.type);
    }
}