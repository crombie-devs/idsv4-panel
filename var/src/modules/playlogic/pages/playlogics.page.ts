import { Component, ViewChild } from '@angular/core';
import { PlaylogicStore } from '../stores/playlogic.store';
import { ContentStore } from 'src/modules/content/stores/content.store';
import { ContentOrderStore } from 'src/modules/content/stores/order.store';
import { PlayerStore } from 'src/modules/player/stores/player.store';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { UserStore } from 'src/modules/user/stores/user.store';
import { User } from 'src/modules/user/models/user';
import { PlayArea } from 'src/modules/playarea/models/playarea';
import moment, { Moment } from "moment";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormField } from 'src/modules/form-field/models/form-field';
import { PlayLogicService } from '../services/playlogic.service';
import { Content } from 'src/modules/content/models/content';

@Component({
    providers: [
        PlaylogicStore,
        ContentStore,
        ContentOrderStore,
        PlayerStore
    ],
    templateUrl: './playlogics.page.html',
    styleUrls: ['./playlogics.page.scss']
})

export class PlaylogicsPage {

    private me: User = null;
    private area: PlayArea;
    public date;
    public d: string = '';
    public isUser: boolean = false;
    public isRandom: boolean;

    public formGroup: FormGroup = new FormGroup({
        date: new FormControl(new Date())
    });

    public fieldDataPrevious: FormField = {
        label: "",
        name: 'date',
        placeholder: 'Fecha',
        required: false
    };
    public contents: Content[] = [];
    public emittingContent: Content;
    constructor(private ws: WSService,
        private debug: DebugStore,
        private ustore: UserStore,
        private cstore: ContentStore,
        private _playlogic: PlayLogicService
    ) {
        this.ustore.onChange.subscribe(user => {
            this.me = user;
            this.isUser = user.isUser();
        });
        this.cstore.onChange.subscribe(contents => {
            this.contents = contents.map(c => new Content(c));

        })
    }

    public onChangeArea(area: PlayArea) {
        this.area = area;
    }

    public onChangeDate(date: any) {
        this.d = moment(this.formGroup.get('date').value).format('YYYY-MM-DD HH:mm');
        this._playlogic.setDate(this.d);
    }
}