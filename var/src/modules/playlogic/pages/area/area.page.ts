import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { Subscription, fromEvent } from 'rxjs';
import { PlayArea } from 'src/modules/playarea/models/playarea';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { Player } from 'src/modules/player/models/player';
import { PlayerStore } from 'src/modules/player/stores/player.store';
@Component({
    selector: 'playlogics-area',
    templateUrl: './area.component.html',
    styleUrls: ['./area.component.scss']
  })
  
  export class PlaylogicsAreaComponent {
    loading: boolean = true;
    area: PlayArea;
    circuit: PlayCircuit;
    player: Player;
    show: boolean = false;
    @Input() date: string;
    @Output() onChange: EventEmitter<PlayArea> = new EventEmitter<PlayArea>();

    private listener: Subscription;

    constructor(private store: PlayerStore) {
        this.listener = fromEvent(window, "message").subscribe((e: MessageEvent) => {                      
            if(e.data.key === "player_text.loaded"){
                this.show = true;
            }               
        });   
    }

    ngOnDestroy(){
        this.listener.unsubscribe();        
    }

    changeArea(area: PlayArea) {
        this.area = area;
        this.onChange.emit(area);
        this.randomByArea();
    }

    changeCircuit(circuit: PlayCircuit) {
        this.circuit = circuit;
        this.update();
    }

    private randomByArea() {
        if(!this.area){ return; }
        let players = this.store.get().filter(p => this.area.isEquals(p.area));
        if(players.length > 0)
            this.player = this.random(players);
        else    
            this.player = this.random(this.store.get());
    }
    
    private random(players: Player[]): Player {
        let index = Math.floor(Math.random() * players.length);
        return players[index];
    }

    public update(){
        this.player = null;
        setTimeout(() => {
            this.randomByArea();
        });
    }
}
