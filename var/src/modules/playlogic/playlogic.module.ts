import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexModule } from '@angular/flex-layout';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DirectivesModule } from 'src/directive/directives.module';
import { MaterialModule } from 'src/app/material.module';

import { COMPONENTS } from './components/components';

import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CONFIG } from 'src/app/app.config';
import moment from 'moment';
import { PipesModule } from 'src/pipes/pipes.module';
import { PlaylogicRoutingModule } from './playlogic-routing.module';
import { PlayareaModule } from 'src/modules/playarea/playarea.module';
import { PlaycircuitModule } from 'src/modules/playcircuit/playcircuit.module';
import { PlayerModule } from 'src/modules/player/player.module';
import { SiteModule } from 'src/modules/site/site.module';
import { MatChipsModule } from '@angular/material/chips';

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, `${CONFIG.ws_url}/i18n/`, `.json?timestamp=${moment().valueOf()}`);
}

@NgModule({
    declarations: [COMPONENTS],
    imports: [
        CommonModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient],
            },
        }),
        FlexModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        DirectivesModule,
        PipesModule,
        PlaycircuitModule,
        PlayareaModule,
        PlayerModule,
        PlaylogicRoutingModule,
        SiteModule,
        MatChipsModule
    ],
    providers: [],
    exports: [COMPONENTS]
})
export class PlaylogicModule { }