export interface IAudioModel {
  idaudio?: string;
  idcategory?: string;
  name: string;
  audio_url: string;
  status: boolean;
  duration: number;
  isrc: string;
  play?: boolean;
}



export class AudioModel implements IAudioModel {
  idaudio?: string;
  idcategory?: string;
  name: string;
  audio_url: string;
  status: boolean;
  duration: number;
  isrc: string;
  play?: boolean;

  constructor(obj?:  IAudioModel) {
    if (!obj) {
      return;
    }
    this.idaudio = obj.idaudio ? obj.idaudio : null;
    this.idcategory = obj.idcategory ? obj.idcategory : null;
    this.name = obj.name ? obj.name : null;
    this.audio_url = obj.audio_url ? obj.audio_url : null;
    this.status = obj.status ? obj.status : null;
    this.duration = obj.duration ? obj.duration : null;
    this.isrc = obj.isrc ? obj.isrc : null;
    this.play = obj.play ? obj.play : false;
  }
}
