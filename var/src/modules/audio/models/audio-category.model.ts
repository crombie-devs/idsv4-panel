export interface IAudioCategoryModel {
  idcategory: string;
  name: string;
  image_url: string;
  status: boolean;
  idcustomer?: string;
}

export interface AudioCategoryModel {
  idcategory: string;
  name: string;
  image_url: string;
  status: boolean;
  idcustomer?: string;
}

export class AudioCategory implements AudioCategoryModel {
  idcategory: string;
  name: string;
  image_url: string;
  status: boolean;
  idcustomer: string;

  constructor(obj?: AudioCategoryModel) {
    if (!obj) {
      return;
    }

    this.idcategory = obj.idcategory ? obj.idcategory : null;
    this.name = obj.name ? obj.name : null;
    this.image_url = obj.image_url ? obj.image_url : null;
    this.status = obj.status ? obj.status : null;
    this.idcustomer = obj.idcustomer ? obj.idcustomer : null;
  }
}
