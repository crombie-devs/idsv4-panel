import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AudiosComponent } from './audios.component';
import { AudioCategoryComponent } from './categories/audio-category.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
import { AudioListComponent } from './audios/audio-list.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { AudioDialog } from './dialogs/audio/audio-dialog';
import { AudioCategoryDialog }from './dialogs/audio-category/audio-category-dialog';
import { MatDialogModule } from '@angular/material/dialog';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { AudioCategoryService } from './services/audio-category.service';
import { AudioCategoryStore } from './stores/audiocategory.store';
import { AudioStore } from './stores/audio.store';
import { DirectivesModule } from 'src/directive/directives.module';



const routes: Routes = [
  {
    path: '',
    component: AudiosComponent,

    children: [
      { path: 'category', component: AudioCategoryComponent },
      { path: 'audio/:id', component: AudioListComponent },
      { path: '', redirectTo: 'category', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  declarations: [AudiosComponent, AudioCategoryComponent, AudioListComponent,AudioDialog, AudioCategoryDialog],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    MatPaginatorModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    MatProgressSpinnerModule,
    FlexModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatCheckboxModule,
    MatIconModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MatSliderModule,
    DirectivesModule
  ],
  providers: [ AudioStore,AudioCategoryStore,AudioCategoryService ]

})
export class AudiosModule {}
