import { Injectable } from '@angular/core';
import { BaseStore } from 'src/stores/base.store';
import {
    AudioCategoryModel,
    IAudioCategoryModel,
    AudioCategory,
} from 'src/modules/audio/models/audio-category.model';
import { AudioCategoryService } from 'src/modules/audio/services/audio-category.service';

@Injectable({
    providedIn: 'root',
})
export class AudioCategoryStore extends BaseStore<AudioCategoryModel> {
    constructor(protected http: AudioCategoryService) {
        super(http);
    }

    protected load() {
        this.http.list().subscribe((item) => this.set(item));
    }

    protected save(data: IAudioCategoryModel[]) {
        this.data = data.map((d) => new AudioCategory(d));

    }
}
