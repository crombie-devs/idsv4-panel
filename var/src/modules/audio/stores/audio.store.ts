import { Injectable } from '@angular/core';
import { BaseStore } from 'src/stores/base.store';
import {
    AudioModel,
    IAudioModel,
} from 'src/modules/audio/models/audio.model';
import { AudiosService } from 'src/modules/audio/services/audios.service';

@Injectable({
    providedIn: 'root',
})
export class AudioStore extends BaseStore<AudioModel> {
    constructor(protected http: AudiosService) {
        super(http);
    }

    protected load() {

        this.http.list().subscribe((item) => this.set(item));
    }

    protected save(data: IAudioModel[]) {
        this.data = data.map((d) => new AudioModel(d));

    }

    filter(key, value) {
        return this.data.filter(d => d[key] == value)
    }
}
