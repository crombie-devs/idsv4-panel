import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/modules/user/models/user';
import { DebugStore } from 'src/stores/debug.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { AudioModel } from 'src/modules/audio/models/audio.model';
import { TranslateService } from '@ngx-translate/core';
import { AudioDialog } from 'src/modules/audio/dialogs/audio/audio-dialog';
import { AudiosService } from 'src/modules/audio/services/audios.service';
import { AudioStore } from 'src/modules/audio/stores/audio.store';
import { MatSort, Sort } from '@angular/material/sort';
import { MYString } from 'src/libs/string.libs';
import { DialogService } from 'src/modules/dialog/services/dialog.service';


@Component({
  selector: 'app-audio-list',
  templateUrl: './audio-list.component.html',
  styleUrls: ['./audio-list.componet.scss'],
})
export class AudioListComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public displayedColumns: string[] = [
    'select',
    'idaudio',
    'name',
    'audio_url',
  ];

  public selection = new SelectionModel<any>(true, []);
  public dataSource = new MatTableDataSource<any>();
  public data: any[];
  public filterData: any[];
  public new: boolean = false;
  public edit: boolean = false;
  public deleted: boolean = false;
  public permission: boolean = false;
  public load: boolean = true;
  public me: User;
  public idcategory: string;
  public isUser: boolean = false;
  sortedData: any[];
  contents: any[];
  audios: AudioModel[] = [];
  dataTemp: any[] = [];
  audioHTML: any;
  hide: boolean

  constructor(
    private debug: DebugStore,
    private ustore: UserStore,
    private _snackBar: MatSnackBar,
    private http: AudiosService,
    private matDialog: MatDialog,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private audioStore: AudioStore,
    private dialog: DialogService,
  ) {
    this.ustore.onChange.subscribe(user => { 
      this.me = user; 
    });	
  }

  public ngOnDestroy(){
    this.pauseAudio();
  }

  public ngOnInit() {
    this.debug.addInfo('ContentListComponent', 'OnInit');
    this.verifyPermissions();
 
       
    
    

    this.audioStore.reload();

    this.audioStore.onChange.subscribe((items) => {
      this.route.params.subscribe((param) => {
        if (param.id) {
          this.idcategory = param.id;
           let val = [];
           this.audios = this.audioStore.filter('idcategory', param.id);
           this.audios .forEach((element) => {
            element.play = false;
            val.push(element);
          });
          this.renderDataTable(val);
         }
      });
    });
  }

  createAudio(): void {
    
      let dialogRef;
      this.pauseAudio();
      dialogRef = this.matDialog.open(AudioDialog, {});
      dialogRef.afterClosed().subscribe((result) => {
      this.audioHTML = null;
      if (result) {
          result.idcategory = this.idcategory;
          this.audioStore.create(result).subscribe((item) => {
           
            if(!item) {
              this.openSnackBar(
                  this.translate.instant(
                    'audio_error'
                  )
                );
              }
            const arr = this.dataSource.data;
            arr.push(item);
            this.dataSource.data = arr;
            this.dataTemp = arr;

            this.openSnackBar(
              this.translate.instant(
                'audio_success'
              )
            );
          } , err => {
            
          });
        } 
      });
    
  }


  openDialog(id): void {
    let dialogRef;
    const datas : AudioModel = this.audios.filter(item => item.idaudio === id)[0];

    dialogRef = this.matDialog.open(AudioDialog, {
      data: datas,
    });

    

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        result.idcategory = this.idcategory;
        result.idaudio = result.id;
        delete result.id;
        
        this.http.update(result).subscribe((item: AudioModel) => {
         if(!item) {
        this.openSnackBar(
            this.translate.instant(
              'audio_error'
            )
          );
         }

        const arr = [...this.dataSource.data];
        this.dataSource.data.forEach((val, index) => {
            if (val.idaudio === result.idaudio) {
              arr[index] = item;
              let a = item;
            }
          });

        

          this.renderDataTable(arr);
          this.audios = arr;
          this.dataTemp = arr;
          this.selection.clear();

          this.openSnackBar(
            this.translate.instant(
              'audio_update'
            )
          );
        });
      }
    });
  }



  updateAudio(id?: any): void {
 
    if (this.selection.selected.length>1){
			this.dialog.warning('Aviso', 'Sólo puede editar un audio').subscribe();
		}else if(this.selection.selected.length == 0){
			this.dialog.warning('Aviso', 'Debe seleccionar un audio').subscribe();
		}else{
    
      this.pauseAudio();     

      let dialogRef;
            let datas: any = this.selection.selected[0];
       if(id) {
            datas = this.dataSource.data.filter(item => item.id = id);
       }

        
      
      dialogRef = this.matDialog.open(AudioDialog, {
        data: datas,
      });

      dialogRef.afterClosed().subscribe((result) => {
        
        if (result) {
          result.idcategory = this.idcategory;
          result.idaudio = result.id;
          delete result.id;
          
          this.http.update(result).subscribe((item: AudioModel) => {
            
            if(!item) {
              this.openSnackBar(
                this.translate.instant(
                  'audio_error'
                )
              );
            }

            const arr = [...this.dataSource.data];
            
            this.dataSource.data.forEach((val, index) => {
              if (val.idaudio === result.idaudio) {
                arr[index] = item;
              }
            });

           
            this.renderDataTable(arr)
            this.audios = arr;
            this.dataTemp = arr;
            this.selection.clear();

            this.openSnackBar(
              this.translate.instant(
                'audio_update'
              )
            );
          });
        } 
      });
    }
  }

  public deleteAudio() {
    if(this.selection.selected.length == 0){
			this.dialog.warning('Aviso', 'Debe seleccionar al menos un audio').subscribe();
		}else{
			this.dialog.confirm('Confirmar Borrado', '¿Desea borrar los elementos seleccionados?').subscribe(confirm => {
				if (confirm){
          if (this.selection.selected.length > 0) {
            let data = [];
            this.selection.selected.forEach((item) => {
              this.http.delete(item.idaudio).subscribe((_) => {
                data = this.dataSource.data.filter((v) => v.idaudio != item.idaudio);
                this.dataSource.data = data;
                this.openSnackBar(
                  this.translate.instant(
                    'audio_update'
                  )
                );
              });
            });
          }
        }
      }); 
    }
  }


  sortData(sort: Sort) {
		const data = this.audios.slice();
		if (!sort.active || sort.direction === '') {
		  this.sortedData = data;
		  return;
		}

   
   
		
		this.sortedData = data.sort((a, b) => {
		  const isAsc = sort.direction === 'asc';
		  switch (sort.active) {
			case 'name': return this.compare(a.name? a.name:'', b.name? b.name:'', isAsc);
			case 'idaudio': return this.compare(a.idaudio, b.idaudio, isAsc);
			case 'audio_url': return this.compare(a.audio_url? a.audio_url:'' , b.audio_url? b.audio_url:'', isAsc);
		
			default: return 0;
		  }
		});
		this.dataSource.data = this.sortedData;
	}

  compare(a: number | string, b: number | string, isAsc: boolean) {
	  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
	}

  
 public renderDataTable(data: any[]) {
    this.dataSource = new MatTableDataSource();
    this.dataSource.filterPredicate = (data: AudioModel, filter: string): boolean => {
			const dataStr = MYString.removeAccents(Object.keys(data).reduce((currentTerm: string, key: string) => {
			  return (currentTerm + (data as { [key: string]: any })[key] + '◬');
			}, '')).trim().toLowerCase();	  
			return dataStr.indexOf(MYString.removeAccents(filter.trim().toLowerCase())) != -1;
		};
    this.dataSource.data = data;
    this.dataTemp = [...this.dataSource.data];
    this.load = false;
    this.sortedData = data.slice();
    this.contents = data;
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  public masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  public checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.idcontent + 1
    }`;
  }

  public openSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 1500,
    });
  } 

  public applyFilter(event: string) {
		if(typeof event === 'string'){
			this.dataSource.filter = event;
		
			if (this.dataSource.paginator) {
				this.dataSource.paginator.firstPage();
			}
		}
	}

  async playAudio(url) {
    if (typeof this.audioHTML != 'object' || this.audioHTML === null) {
      try {
        this.onPlay(url.idaudio);
        this.audioHTML = new Audio(url.audio_url);
        await this.audioHTML.play();
      } catch (err) {
        this.resetPlay();
        this.audioHTML = null;
        this.openSnackBar(
          this.translate.instant(
            'COMPONENTS.contents_category.element.update.error.text2'
          )
        );
      }
    } else {
      this.resetPlay();
      try {
        await this.audioHTML.pause();
        this.audioHTML = null;
      } catch (err) {
        this.audioHTML = null;
        this.openSnackBar(
          this.translate.instant(
            'COMPONENTS.contents_category.element.update.error.text2'
          )
        );
      }
    }
  }

  private resetPlay(): void {
    const data = [...this.dataSource.data];
    data.forEach((item, index) => {
      data[index].play = false;
    });
    this.dataSource.data = data;
  }

  private onPlay(id): void {
    const data = [...this.dataSource.data];
    data.forEach((item, index) => {
      if (item.idaudio == id) {
        data[index].play = true;
      }
    });
    this.dataSource.data = data;
  }

  private pauseAudio(){

    try {
      this.audioHTML.pause();
      this.audioHTML = null;
    } catch (err) {
        this.audioHTML = null;
     
    }

  }

  private verifyPermissions() {
    this.new = this.ustore.hasPermissionByName('post.audio.*');
    this.edit = this.ustore.hasPermissionByName('put.audio.*');
    this.deleted = this.ustore.hasPermissionByName('delete.audio.*');
	}	


}
