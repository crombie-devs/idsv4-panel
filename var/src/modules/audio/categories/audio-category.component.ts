import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { AudioCategoryModel,} from 'src/modules/audio/models/audio-category.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AudioCategoryDialog } from 'src/modules/audio/dialogs/audio-category/audio-category-dialog';
import { AudioCategoryService } from 'src/modules/audio/services/audio-category.service';
import { AudioCategoryStore } from 'src/modules/audio/stores/audiocategory.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import {  User } from 'src/modules/user/models/user';
@Component({
  selector: 'app-audio-category',
  templateUrl: './audio-category.component.html'


})
export class AudioCategoryComponent implements OnInit {

  categories: AudioCategoryModel[] = [];
  idUser: any;
  editContentcategory: boolean;
  deleteContentcategory: boolean;

  constructor(
    private http: AudioCategoryService,
    private matDialog: MatDialog,
    private _snackBar: MatSnackBar,
    private translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private  store: AudioCategoryStore,
    private ustore: UserStore
  ) {
    
  }
 
  ngOnInit(): void {
    this.store.reload();
    this.ustore.onChange.subscribe((user: User) => {
		 if(user.roles[0].name == 'user') {
      this.editContentcategory = true;
      this.deleteContentcategory = true;
     }
		});


    this.store.onChange.subscribe(items => {
     
      this.categories = items;
      });



  }

  deleteCategory(event: any): void {
    const id = this.categories[event].idcategory;
    this.http.delete(id).subscribe((status) => {
      this.categories = this.categories.filter((item) => item.idcategory != id);
    });
  }

  updateCategory(event: any): any {
    let dialogRef;
    dialogRef = this.matDialog.open(AudioCategoryDialog, {
      data: this.categories[event],
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (result.hasOwnProperty('id')) {
          result.idcategory = result.id;
          result.idcustomer = result.customer;
          this.categories[event] = result;
          delete result.id;
          delete result.customer;

          this.http.update(result).subscribe((item: AudioCategoryModel) => {
            this.categories[event] = item;
            this.openSnackBar(
              this.translate.instant(
                'COMPONENTS.contents_category.element.update.success'
              )
            );
          });
        }
      } else {
        this.openSnackBar(
          this.translate.instant(
            'COMPONENTS.contents_category.element.update.error.text2'
          )
        );
      }
    });
  }

  viewCategory(event: any): void {
    this.router.navigate(['../audio', this.categories[event].idcategory], {
      relativeTo: this.route,
    });
  }

  createCategory(): void {
    let dialogRef;
      dialogRef = this.matDialog.open(AudioCategoryDialog, {});
      dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.http.create(result).subscribe((item) => {
           this.categories.push(item);
        });
      }
     });
  }

  public openSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 1500,
    });
  }
}
