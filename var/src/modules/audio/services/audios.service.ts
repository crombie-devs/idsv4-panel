import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AudioModel } from 'src/modules/audio/models/audio.model';
import { HttpService } from 'src/services/http.service';
import { ICrudWS } from 'src/interfaces/ICrudWS.interface';
import { AudiosModule } from 'src/modules/audio/audios.module';

@Injectable({
    providedIn: 'root',
})
export class AudiosService implements ICrudWS<AudioModel>{
    constructor(private http: HttpService) { }

    public create(obj: AudioModel): Observable<AudioModel> {
        return this.http.createWithFiles('private/audio', obj, 'WS.audio_add');
    }

    public update(obj: AudioModel): Observable<AudioModel> {
        return this.http.updateWithFiles('private/audio', obj, 'WS.audio_add');
    }

    public delete(id: number): Observable<boolean> {
        return this.http.delete('private/audio', [id], 'WS.audio_delete');
    }

    public get(id: number): Observable<AudioModel> {
        return this.http.get('private/audio', [id], 'WS.audio');
    }

    public toSelect(args?: any[]): Observable<AudioModel[]> {
        return this.http.get('private/audio/toselect', args, 'WS.audio');
    }


    public list(): Observable<AudioModel[]> {
        return this.http.get('private/audio/list', [], 'WS.audio');
    }


}


