import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AudioCategoryModel } from 'src/modules/audio/models/audio-category.model';
import { HttpService } from 'src/services/http.service';
import { ICrudWS } from 'src/interfaces/ICrudWS.interface';
import { tap } from 'rxjs/operators';
import { UserStore } from 'src/modules/user/stores/user.store';
import { User } from 'src/modules/user/models/user';

@Injectable({
    providedIn: 'root',
})
export class AudioCategoryService implements ICrudWS<AudioCategoryModel> {
    constructor(private http: HttpService, ustore: UserStore) {
        this.user = ustore.getUser();
    }
    user: User;
    public create(obj: AudioCategoryModel): Observable<AudioCategoryModel> {
        return this.http.createWithFiles(
            'private/audiocategory',
            obj,
            'audiocategory_create'
        );
    }

    public update(obj: AudioCategoryModel): Observable<AudioCategoryModel> {
        if (typeof obj.image_url === 'string') {
            return this.http.update(
                'private/audiocategory',
                obj,
                'audiocategory_update'
            );
        } else {
            return this.http.updateWithFiles(
                'private/audiocategory',
                obj,
                'audiocategory_update'
            );
        }
    }

    public delete(id): Observable<boolean> {
        return this.http
            .delete('private/audiocategory', [id], 'WS.audiocaregoty_delete')
            .pipe(tap(() => id));
    }

    public get(): Observable<AudioCategoryModel> {
        return of();
    }

    public toSelect(args?: any[]): Observable<AudioCategoryModel[]> {
        return this.http.get('private/audiocategory/toselect', [], 'WS.audiocategory');
    }

    public list(args?: any[]): Observable<AudioCategoryModel[]> {

        if (!args) {
            args = [this.user.customer.idcustomer];
        }

        return this.http.get('private/audiocategory/list', args, 'WS.audiocategory');
    }
}
