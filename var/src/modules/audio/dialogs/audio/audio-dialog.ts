import {
  Component,
  Inject,

} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { AssetTypeStore } from 'src/modules/asset/stores/assettype.store';
import { MaxSizeValidator } from '@angular-material-components/file-input';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { AudioModel } from 'src/modules/audio/models/audio.model';


@Component({
  providers: [FormatStore, LangStore, AssetTypeStore],
  selector: 'audio-dialog',
  templateUrl: './audio-dialog.html',
  styleUrls: ['./audio-dialog.scss'],
})
export class AudioDialog {
  public id: number | null;
  public idcustomer: number | null;
  public me: any;
  public form: FormGroup;

  public durationStatus;

  public loading: boolean = false;
  public contentcategory: any;
  public files;
  public maxSize = 10000;
  public url: SafeHtml;
  public name: FormControl = new FormControl('', [Validators.required]);
  public duration: FormControl = new FormControl('');
  public customer: FormControl = new FormControl('');
  public isrc: FormControl = new FormControl('', [Validators.required]);
  public audio_url: FormControl;
  public status: boolean = true;
  public btn = 'Grabar';
  public title = "Audio";
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<any>,

    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private sanitizer: DomSanitizer
  ) {
    this.audio_url = new FormControl('', [
      MaxSizeValidator(this.maxSize * 1024),
    ]);
  }

  public ngOnInit() {
    this.audio_url.valueChanges.subscribe((file: File) => {
      this.files = file;
      this.calculateDuration(this.files);
      const duration: any = document.getElementById('duration');

      setTimeout(() => {
        this.duration.patchValue(duration.value);
      }, 500);
      this.url = this.sanitizer.bypassSecurityTrustResourceUrl(
        URL.createObjectURL(this.files)
      );
    });

    if (this.data) {
      this.initForm(this.data);
    } else {
      this.initForm(null);
    }
  }

  calculateDuration(file: File): any {
    const audio: any = document.createElement('audio');
    var reader = new FileReader();

    if (file) {
      var reader = new FileReader();
      reader.onload = function (e) {
        audio.src = e.target.result;

        audio.addEventListener(
          'loadedmetadata',
          function (event) {
            const i: any = document.getElementById('duration');
            i.value = audio.duration;
          },
          false
        );
      };

      reader.readAsDataURL(file);
    }
  }

  onDisabledChanged(value: boolean) {
    if (!value) {
      this.audio_url.enable();
    } else {
      this.audio_url.disable();
    }
  }

  private initForm(data: AudioModel | null) {
    if (!data) {
      this.form = this.fb.group({
        name: this.name,
        audio_url: this.audio_url,
        duration: this.duration,
        isrc: this.isrc,
        status: this.status,
      });
    } else {
      this.status = data.status;
	  this.btn = 'Editar';
      this.form = this.fb.group({
        id: data.idaudio,
        name: data.name,
        audio_url: data.audio_url,
        duration: data.duration ? data.duration : false,
        isrc: data.isrc,
        status: data.status,
      });
    }
  }

  public close() {
    this.dialogRef.close();
  }

  public save() {
    
    if(this.form.valid) {
      this.form.value.audio_url = this.files ? this.files : this.data.audio_url;
      this.dialogRef.close(this.form.value);
    } else {
         
      this.form.get('isrc').markAsTouched();
      this.form.get('name').markAsTouched();
    }

  
  }

  changeStatus(): void {
    this.status = !this.status;
    this.form.patchValue({ status: this.status });
  }

  public openSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 1500,
    });
  }
}
