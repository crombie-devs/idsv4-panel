import { Component, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


import { FormatStore } from 'src/modules/format/stores/format.store';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { AssetTypeStore } from 'src/modules/asset/stores/assettype.store';



import { MaxSizeValidator } from '@angular-material-components/file-input';


import { MatSnackBar } from '@angular/material/snack-bar';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
    providers: [FormatStore, LangStore, AssetTypeStore],
	selector: 'audio-category-dialog',
  	templateUrl: './audio-category-dialog.html',
  	styleUrls: ['./audio-category-dialog.scss']
})

export class AudioCategoryDialog {    
    public id: number|null;
	public idcustomer: number|null;
	public me: any;
	public form: FormGroup;

    public durationStatus;

	public loading: boolean = false;
	public contentcategory: any;
	public files;
	public maxSize= 500;
	public url: SafeHtml;
	public name: FormControl = new FormControl('', [Validators.required]);
    public duration: FormControl = new FormControl('');
    public customer: FormControl = new FormControl('');
	public status: boolean;
	public image_url: FormControl ;
	constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<any>, 
      
        private fb: FormBuilder,
		private _snackBar: MatSnackBar,
		private sanitizer: DomSanitizer)
    {

		
		

        this.image_url= new FormControl('', [
            MaxSizeValidator(this.maxSize * 1024)
            ]);
             
    }

    public ngOnInit() {		
		this.image_url.valueChanges.subscribe((file: File) => {
            this.files = file;	
			this.url = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(this.files));
			

		});	

		if(this.data) {
			this.initForm(this.data);
		} else {
			this.initForm(null);
		}
		

			
	} 

    onDisabledChanged(value: boolean) {
		if (!value) {
		  this.image_url.enable();
		} else {
		  this.image_url.disable();
		}
	  }

	private initForm(data: any|null){	
		if (!data){

			this.form = this.fb.group({
				name: this.name,
				image_url: this.image_url,
                customer: this.customer,
				status: this.status

			});

		  }else{	
	
			this.durationStatus = data.duration ? data.duration : false;
			this.status = data.status;	 
            
			this.form = this.fb.group({
				id: data.idcategory,
				name: data.name,
				image_url: data.image_url,
                customer: data.idcustomer,
				status: data.status == 0 ? false : true
			});
			
		   
		  } 
	}	

   public close() {
        this.dialogRef.close();
    }


    changeStatus(): void {
		this.status = !this.status;
		this.form.patchValue( {status: this.status} );
	}


    public save(){

		this.form.value.image_url  =  this.files?this.files: this.data.image_url;
       
       	this.dialogRef.close(this.form.value);
    }

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
  
}