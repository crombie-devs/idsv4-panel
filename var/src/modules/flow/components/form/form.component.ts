import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { SiteService } from 'src/modules/site/services/site.service';
import { DebugStore } from 'src/stores/debug.store';

import { User } from 'src/modules/user/models/user';
import { Site } from 'src/modules/site/models/site';
import { FormField } from 'src/modules/form-field/models/form-field';
import { UserStore } from 'src/modules/user/stores/user.store';
import { IResult } from 'src/models/iresult';
import {TranslateService} from '@ngx-translate/core';
import moment from 'moment';

@Component({
  selector: 'flow-form',
  templateUrl: './view.component.html',
  styleUrls: ['./styles.component.scss']
})

export class FlowFormComponent {
	@Output() onSubmittedForm: EventEmitter<any> = new EventEmitter<any>();
	public meses:string[] = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
	public minDate:string = '01/01/2018 - 00:00';
	public dateDisabled:boolean = false;

	public fieldDataCustomer: FormField = {
		label: "Cliente",
		name: 'customer',
		placeholder: '',
		options: [],
		multiple:false,
		required: true
	};
	public fieldDataSite: FormField = {
		label: "Centros/Áreas",
		name: 'site',
		placeholder: '',
		options: [],
		multiple:false,
		required: false
	};
	public fieldDataStart: FormField = {
		label: "Fecha Inicio",
		name: 'startDate',
		placeholder: '',
		multiple:false,
		required: true,
		minDate: this.minDate,
		defaultTime:['00','00','00'],
		disabled:this.dateDisabled,
		maxDate: moment().format('YYYY-MM-DD HH:mm:ss')
	};
	public fieldDataEnd: FormField = {
		label: "Fecha Fin",
		name: 'endDate',
		placeholder: '',
		multiple:false,
		required: true,
		minDate: moment().format('YYYY-MM-DD HH:mm:ss'),
		defaultTime:['23','59','59'],
		disabled:this.dateDisabled,
		maxDate: moment().format('YYYY-MM-DD HH:mm:ss')
	};
	public me: any;
	public formGroup:FormGroup = new FormGroup({
		customer: new FormControl(null, Validators.required),
		site: new FormControl(null),
		camera: new FormControl(null),
		startDate: new FormControl(null,Validators.required),
		endDate: new FormControl(null,Validators.required),
	});
	public loadingCustomers: boolean = false;
	public loadingSites: boolean = false;
	public loadingCameras: boolean = false;

  	constructor(
		private ws: WSService,
		private ss: SiteService,
		private debug: DebugStore,
		private ustore: UserStore) {
  	}
  
  	public ngOnInit() {
		this.me = this.ustore.getUser();
		if(this.me && this.me.customer){
			//this.getAfluencias();
			let max = moment()
			this.formGroup.patchValue({ startDate: max.toDate(), endDate:max.toDate()})
			const customer = this.me.customer;
			this.fieldDataCustomer.options = [{name: customer.name, value: customer.idcustomer }]
			this.formGroup.patchValue({customer: customer.idcustomer});
			this.loadingCustomers = false;
			if(this.me.site){
				const site = this.me.site;
				this.fieldDataSite.options = [{name: site.name, value: site.idsite }]
				this.formGroup.patchValue({site: site.idsite});
				this.loadingSites = false;
			}else{
				this.getSites();
			}
		}else{
			this.getCustomers();
		}
	}
	public getAfluencias(){
		let params: any = {
				"filter": {
					"customer_id": this.me.customer.idcustomer,
				},
				"group": {
					"_id":"$date",
				}
			}
		return this.ws.filterAndGroupPeople(params).subscribe((result: IResult) => {
		  if(result.success) { 
			this.getMinDate(result.data);
		  }
		});
	}

	getMinDate(data){
		if(data.length > 0){
			data.sort((a,b) => a._id > b._id ? 1 : -1);
			this.fieldDataStart.minDate = data[0]._id;
			this.fieldDataStart.disabled = false;
			this.fieldDataEnd.minDate = data[0]._id;
			this.fieldDataEnd.disabled = false;
			let date = moment(data[0]._id)
			let max = moment()
			this.formGroup.patchValue({ startDate: max.toDate(), endDate:max.toDate()})
		}else{
			alert("Este usuario todavía no tiene datos que analizar.");
		}
	}
	getSites(){
		this.fieldDataSite.options = [];
		this.formGroup.patchValue({site: null});
		this.loadingSites = true;
		this.ss.list().subscribe((result: Site[]) => {
			if(result.length > 0){
				this.fieldDataSite.options.push({name: "Todo", value: 0});
				this.formGroup.patchValue({site: 0});
				result.map(res => {
					this.fieldDataSite.options.push({name: res.name, value: res.idsite });
				});	
			}
			this.loadingSites = false;
		}, error => {
			this.loadingSites = false;
		});
	}

	getCustomers(){
		this.loadingCustomers = true;
		this.ws.customers().subscribe((result: IResult) => {
			if(result.success) {
				if(result.data.length > 0){
					this.fieldDataCustomer.options.push({name: "Todo", value: 0 });
					this.formGroup.patchValue({ customer: 0});
					result.data.map(res => {
						this.fieldDataCustomer.options.push({name: res.name, value: res.idcustomer });
					});
				}
				this.loadingCustomers = false;
			}
			else {      
				this.loadingCustomers = false;
				return null;
			}
		}, error => {
			this.loadingCustomers = false;
			return null;          
		});	
	}
	

	onChangeCustomer(){
		this.getSites();
	}

	public submitForm(){
		this.onSubmittedForm.emit(this.formGroup.getRawValue());
	}

	public onChangeStartDate(data){
		this.fieldDataEnd.minDate = this.formGroup.get('startDate').value;
	}
	 
	public onChangeEndDate(data){
		this.fieldDataStart.maxDate = this.formGroup.get('endDate').value;
	}
	public cameraSelected(data){
		this.formGroup.get('camera').setValue(data);
	}
}
