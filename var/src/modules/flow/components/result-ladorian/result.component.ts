import { Component, Input, SimpleChange, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { IResult } from 'src/models/iresult';
import {MatGridListModule} from '@angular/material/grid-list';

import moment from 'moment';
import { User } from 'src/modules/user/models/user';
import { Site } from "src/modules/site/models/site";
import { FormField } from 'src/modules/form-field/models/form-field';
import { UserStore } from 'src/modules/user/stores/user.store';

// For exports
import { ExcelService } from 'src/services/excel.service';
import { PdfService } from 'src/services/pdf.service';

// For graphics
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import {TranslateService} from '@ngx-translate/core';
import { Afluence } from 'src/modules/flow/models/afluence';
import { Camera } from 'src/modules/camera/models/camera';
import { CONFIG } from 'src/app/app.config';
import { windowCount } from 'rxjs/operators';
import { SiteService } from 'src/modules/site/services/site.service';

@Component({
  selector: 'flow-result-ladorian',
  templateUrl: './view.component.html',
  styleUrls: ['./styles.component.scss']
})

export class FlowResultLadorianComponent {
	@ViewChild('htmlData') htmlData:ElementRef;
	@Input() data:any;
	@Input() cameras: any[] = [];
	@Output() onBackToForm:EventEmitter<any> = new EventEmitter(null);
	
	public loadingpdf: boolean = false;
	public afluencias: any[] = [];
	public me: User;
	public store: string = null;
	public site: any = {};
	public sites: Site[] = [];
	public camera: any = {};
	public allCameras: Camera[] = [];
	public customer: any = {};
	public totalAfluencias: string = '0';
	public isLoading: boolean = false;
	public horaPunta: string = '00:00';
	public bestWeekday:string = 'lunes';
	public bestDay: string;
	public aspectRatio: boolean = false;
	public chartHeightNumber: Number = window.innerHeight * 0.4;
	public chartHeihgt: string = this.chartHeightNumber.toString();
	public lastAfluences: Afluence[] = [];
	public loading: boolean = false;
	public groupedSitesCameras:any[] = [];
	public allAfluences:any[] = [];
	public ranking:any[] = [];
	public filteredByAgeRange:any[] = [];
	public hombres = 0;
	public mujeres = 0;
	public totalVisits:any[] = [];
	public totalAfluencesArray:any[] = [];
	public selectedGender:string = '';
	public factorCorrector:number = 1;
	public ageRanges:any = {
		young: [0,20],
		adult: [21-50],
		senior: [51-200]
	}

	public totalMediumTime:number = 0;

	public fieldDataTotal: FormField = {
		label: "Elegir:",
		name: 'total',
		placeholder: '',
		options: [
			{name: 'Horas', value: 1 },
			{name: 'Días', value: 2 },
			{name: 'Días de la semana', value: 3 }
		],
		multiple:false,
		required: false
	};
	public fieldDataGenderRange: FormField = {
		label: "Elegir:",
		name: 'genderRange',
		placeholder: '',
		options: [
			{name: 'Horas', value: 1 },
			{name: 'Días', value: 2 },
			{name: 'Días de la semana', value: 3 }
		],
		multiple:false,
		required: false
	};
	public fieldDataCamera: FormField = {
		label: "Elegir:",
		name: 'camera',
		placeholder: '',
		options: [
			{name: 'Horas', value: 1 },
			{name: 'Días', value: 2 },
			{name: 'Días de la semana', value: 3 }
		],
		multiple:false,
		required: false
	};
	public fieldDataTimes: FormField = {
		label: "Elegir:",
		name: 'times',
		placeholder: '',
		options: [
			{name: 'Horas', value: 1 },
			{name: 'Días', value: 2 },
			{name: 'Días de la semana', value: 3 }
		],
		multiple:false,
		required: false
	};
	public formGroup:FormGroup = new FormGroup({
		total: new FormControl(1),
		camera: new FormControl(1),
		genderRange: new FormControl(1),
		times: new FormControl(1)
	});

	afluenciasHoraLineChartData: ChartDataSets[] = [
		{label : this.translate.instant('Horas'), data : [0]}];
	afluenciasHoraLineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];
	afluenciasDiaLineChartData: ChartDataSets[] = [
		{label : this.translate.instant('Días'), data : [0]}];
	afluenciasDiaLineChartLabels: Label[] = [];
	afluenciasDiaSemanaLineChartData: ChartDataSets[] = [
		{label : this.translate.instant('Horas'), data : [0]}];
	afluenciasDiaSemanaLineChartLabels: Label[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

	genderHoraLineChartData: ChartDataSets[] = [
		{label : this.translate.instant('COMMON.men'), data : [0]},
		{label : this.translate.instant('COMMON.women'), data : [0]},];
	genderDiaLineChartData: ChartDataSets[] = [
		{label : this.translate.instant('COMMON.men'), data : [0]},
		{label : this.translate.instant('COMMON.women'), data : [0]},];
	genderDiaSemanaLineChartData: ChartDataSets[] = [
		{label : this.translate.instant('COMMON.men'), data : [0]},
		{label : this.translate.instant('COMMON.women'), data : [0]},];
		
	genderHoraLineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];
	genderDiaLineChartLabels: Label[] = [];
	genderDiaSemanaLineChartLabels: Label[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
	
	ageRangeHoraLineChartData: ChartDataSets[] = [];
	ageRangeHoraLineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];
	ageRangeDiaLineChartData: ChartDataSets[] = [];
	ageRangeDiaLineChartLabels: Label[] = [];
	ageRangeDiaSemanaLineChartData: ChartDataSets[] = [];
	ageRangeDiaSemanaLineChartLabels: Label[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
	
	camarasHoraLineChartData: ChartDataSets[] = [];
	camarasDiaLineChartData: ChartDataSets[] = [];
	camarasDiaSemanaLineChartData: ChartDataSets[] = [];
	allCamerasData: ChartDataSets[] = [];
	
	camarasHoraLineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];
	camarasDiaLineChartLabels: Label[] = [];
	camarasDiaSemanaLineChartLabels: Label[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
	
	timesHoraLineChartData: ChartDataSets[] = [];
	timesDiaLineChartData: ChartDataSets[] = [];
	timesDiaSemanaLineChartData: ChartDataSets[] = [];
	
	timesHoraLineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];
	timesDiaLineChartLabels: Label[] = [];
	timesDiaSemanaLineChartLabels: Label[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

	genderPieChartData: number[] = [0,0];
	
	public gender:string[] = ['COMMON.man', 'COMMON.woman'];
	genderPieChartLabels: Label[] =[];
	typePieChartData: number[] = [0,0,0,0];
	public type:string[] = [
		'COMMON.type_age.adult',
		'COMMON.type_age.young',
		'COMMON.type_age.kid',
		'COMMON.type_age.senior'
	]
	typePieChartLabels: Label[] = [];

	constructor(
		private router: Router,
		private ws: WSService,
		private wsSite: SiteService,
		private debug: DebugStore,	
		private dialog: DialogService,
    	private astore: AuthStore,
		private localStorage: MYLocalStorage,
		private ustore: UserStore,
		private excelService:ExcelService,
		private pdfService:PdfService,
		private translate: TranslateService) {
			this.ustore.onChange.subscribe(user => { 
				this.me = user; 
			});
  	}
  
  	public ngOnInit() {
		this.syncViewer();
		this.translate.get(this.gender).subscribe(translations => {
			this.genderPieChartLabels = Object.values(translations);
		})
	}
	private resetData(){
		this.afluencias = [];
		this.sites = [];
		this.camera = {};
		this.allCameras = [];
		this.totalAfluencias = '0';
		this.lastAfluences = [];
		this.groupedSitesCameras = [];
		this.allAfluences = [];
		this.ranking = [];
		this.filteredByAgeRange = [];
		this.hombres = 0;
		this.mujeres = 0;
		this.ageRangeDiaLineChartLabels = [];
		this.typePieChartLabels = [];
		this.totalVisits = [];
		this.totalAfluencesArray = [];
	}

	public ngOnChanges(changes: SimpleChange){
		this.syncViewer();
	}

	private syncViewer() {
		if(!this.loading){
			this.resetData();
			this.getSites();
			this.getCameras();
			this.loading = true;
			this.isLoading = true;
			this.cameras = [];
			this.cameras.map(camera => {
				this.fieldDataCamera.options.push(camera);
			})
			this.resetData();
			this.getCustomer();
			this.getSite();
			this.getCamera();
			this.getAfluencias();
			this.getLastAfluences();
			this.cameras = [];
		}
	}
	public getSites(){
		this.wsSite.list().subscribe((sites: Site[]) => { 
			this.sites =  sites;
		});
	}
	public getCameras(){
		this.ws.cameras().subscribe((result: IResult) => {
			if(result.success) { 
				this.allCameras =  result.data;
				this.groupCamerasBySite();
			}else {
				this.debug.addError('WS.findPeople', result.data);
				this.dialog.warning('COMPONENTS.flow.result.dialogs.warning.title', result.data); 
			}
		  });
	}
	public groupCamerasBySite(){
		let group: any[] = [];
		this.allCameras.map(camera => {
			if(this.data.site && this.data.site !== 0){
				if(!group.some(g => g.site.idsite === camera.site.idsite)){
					if(camera.site.idsite === this.data.site)
						group.push({site: camera.site, cameras: []})
				}
			}else{
				if(!group.some(g => g.site.idsite === camera.site.idsite))
					group.push({site: camera.site, cameras: []})
			}
		})
		this.allCameras.map(camera => {
			group.map((g,i) => {
				if(g.site.idsite === camera.site.idsite)
					if(this.data.camera){
						if(camera.idcamera === this.data.camera)
							g.cameras.push(camera)
					}
					else
						g.cameras.push(camera)
			})
		})
		this.groupedSitesCameras = group;
	}
	public getCustomer(){
		this.ws.getCustomer(this.data.customer).subscribe((result: IResult) => {
			if(result.success) { 
				this.customer =  result.data;
			  }else {
				this.debug.addError('WS.findPeople', result.data);
				this.dialog.warning('COMPONENTS.flow.result.dialogs.warning.title', result.data); 
			  }
		  });
	}
	public getSite(){
		if(this.data.site !== 0){
			if(!this.sites || this.sites.length === 0){
				this.wsSite.get(this.data.site).subscribe((site: Site) => {
					this.site =  site;					
				});
			}else{
				this.sites.map(site => {
					if(site.idsite === this.data.site)
						this.site = site;
				})
			}
		}else{
			this.site.name = this.translate.instant('Todo');
		}
	}
	public getCamera(){
		if(this.data.camera && this.data.camera !== 0){
			this.ws.getCamera(this.data.camera).subscribe((result: IResult) => {
				if(result.success) { 
					this.camera =  result.data;
				  }else {
					this.debug.addError('WS.findPeople', result.data);
					this.dialog.warning('COMPONENTS.flow.result.dialogs.warning.title', result.data); 
				  }
			  });
		}else{
			this.camera.name = this.translate.instant('Todo');
		}
	}
	public getAfluencias(){
		let startDate = moment.utc(this.data.startDate).format('YYYY-MM-DD HH:mm')
		let endDate = moment.utc(this.data.endDate).format('YYYY-MM-DD HH:mm')
		let params: any = {
			datetime: {'$gte': startDate, '$lte': endDate},
			customer_id: this.data.customer,
		}

		if(this.data.site && this.data.site !== 0)
			params.site_id = this.data.site;
		
		if(this.data.camera && this.data.camera !== 0)
			params.camera_id = this.data.camera;

		return this.ws.analyticsAfluenciasSubmit(params).subscribe((result: IResult) => {
		  if(result.success) { 
			  let elements:any[] = [];
				elements = result.data;
				this.totalVisits = result.data
			
			  this.totalAfluencias = new Intl.NumberFormat("de-DE").format(Math.round(elements.length/this.factorCorrector));
			  this.allAfluences = elements;
			  this.totalAfluencesArray = result.data;
			  this.totalMediumTime = this.calculateMediumTimes(this.totalAfluencesArray);
			  if(this.data.camera === null || this.data.camera === 0)
			  	this.getRanking(elements);
			  this.processAllAfluencias(elements).then(() => {
				this.isLoading = false;
			  });
			  this.getHoraPunta(elements);
			  this.getBestDay(elements);
			  this.getBestWeekDay(elements);
			  this.afluencias = elements;

			}else {
			  this.debug.addError('WS.findPeople', result.data);
			  this.dialog.warning('COMPONENTS.flow.result.dialogs.warning.title', result.data); 
			}
		});
	}

	private calculateMediumTimes(data){
		let total = 0;
		data.map(afluence => {
			total += afluence.duration;
		});
		return total / data.length;
	}
	private getLastAfluences() {
		const today = moment().format('YYYY-MM-DD');
		const filter: any = {customer_id: this.me.customer.idcustomer, date: today};
		if(this.data.site !== 0){
			filter.site_id = this.data.site
		}
		if(this.data.camera )
			filter.camera_id = this.data.camera;
		this.ws.detailFacesByCamera({filter:filter}).subscribe(result => {
			let afluence:Afluence[] = []
			if(result.success){
				result.data.map(af => {
					if(af.gender !== 'unknown')
						afluence.push(af);
				})
				this.lastAfluences = afluence.slice(-4).map(p => new Afluence(p));
				this.loading = false;
			}
		})
   }
	private getHoraPunta(elements){
		  let array = []
		  elements.map(element => {
			if(!array[element.hour]){
				array[element.hour] = 1;
			}else{
				array[element.hour]++;
			}
		  });
		  let mayor = 0;
		  let hour = 1;
		  array.map((dat, index) => {
			if(dat > mayor){
				mayor = dat;
				hour = index;
			}
		  })
		  let hourString:string;
		  if(hour < 10){
			  hourString = `0${hour}:00`;
		  }else{
			  hourString = `${hour}:00`;
		  }
		  this.horaPunta = hourString;
	}
	private getBestDay(elements){
		let array = []
		let arrayDays = [];
		elements.map(element => {
		  if(!array[element.date]){
			arrayDays.push(element.date);
			array[element.date] = 1;
		  }else{
			array[element.date]++;
		  }
		});
		let mayor = 0;
		let day:string;;
		for(let i in array){
			if(array[i] > mayor){
				mayor = array[i];
				day = i;
			}
		}

		this.bestDay = day;  
	}
	
	private getBestWeekDay(elements){
		let array = []
		elements.map(element => {
		  if(!array[element.weekday]){
			  array[element.weekday] = 1;
		  }else{
			  array[element.weekday]++;
		  }
		});
		let mayor = 0;
		let weekday = 1;
		array.map((dat, index) => {
		  if(dat > mayor){
			  mayor = dat;
			  weekday = index;
		  }
		})
		const arrayDays = [
			'',
			'WEEKDAYS.mon', 
			'WEEKDAYS.tue', 
			'WEEKDAYS.wed', 
			'WEEKDAYS.thu', 
			'WEEKDAYS.fri', 
			'WEEKDAYS.sat',
			'WEEKDAYS.sun'];
		this.bestWeekday = arrayDays[weekday];  
	}

	async processAllAfluencias(elements: any [], day:string = 'today'){
		this.camarasDiaLineChartLabels = this.getAfluenciasDiaChartLabels(this.totalAfluencesArray);
		this.timesDiaLineChartLabels = this.getAfluenciasDiaChartLabels(this.totalAfluencesArray);
		let afluencias = [];
		afluencias['genderPie'] = [];
		afluencias['genderLine'] = [];
		afluencias['typePie'] = [];
		afluencias['ageRangeLine'] = [];
		afluencias['dayLine'] = [];
		afluencias['cameras'] = [];
		afluencias['times'] = [];
		afluencias['horas'] = this.groupBy(this.totalVisits, 'hour');
		afluencias['dia'] = this.groupBy(this.totalVisits, 'date');
		afluencias['diaSemana'] = this.groupBy(this.totalVisits, 'weekday');
	
		afluencias['genderPie']["hombres"] = Math.round(elements.filter(emision  => emision.gender === 'male').length / this.factorCorrector);
		afluencias['genderPie']["mujeres"] = Math.round(elements.filter(emision => emision.gender === 'female').length / this.factorCorrector);
		this.hombres = (afluencias['genderPie']['hombres']*100) / (afluencias['genderPie']['hombres'] +afluencias['genderPie']['mujeres']);
		this.mujeres = (afluencias['genderPie']['mujeres']*100) / (afluencias['genderPie']['hombres'] +afluencias['genderPie']['mujeres']);

		this.genderPieChartLabels.map((label,index) => {
			let str = new String(label);
			let splitted = str.split('/', 1);
			if(index === 0){
				this.genderPieChartLabels[index] = `${splitted[0]} / ${this.hombres.toFixed(2)}%`;
			}else{
				this.genderPieChartLabels[index] = `${splitted[0]} / ${this.mujeres.toFixed(2)}%`;
			}
		})

		afluencias['genderPie']["unknown"] = elements.filter(emision => emision.gender === 'unknown').length > 0 ? elements.filter(emision => emision.gender === 'unknown').length - (afluencias['genderPie']["mujeres"] + afluencias['genderPie']["hombres"]) : 0;
		afluencias['genderLine']["mujeres-hora"] = this.groupBy(elements.filter(emision => emision.gender === 'female'), 'hour');
		afluencias['genderLine']["hombres-hora"] = this.groupBy(elements.filter(emision => emision.gender === 'male'), 'hour');
		afluencias['genderLine']["mujeres-dia"] = this.groupBy(elements.filter(emision => emision.gender === 'female'), 'date');
		afluencias['genderLine']["hombres-dia"] = this.groupBy(elements.filter(emision => emision.gender === 'male'), 'date');
		afluencias['genderLine']["mujeres-weekday"] = this.groupBy(elements.filter(emision => emision.gender === 'female'), 'weekday');
		afluencias['genderLine']["hombres-weekday"] = this.groupBy(elements.filter(emision => emision.gender === 'male'), 'weekday');
		
		const ageRanges:string[] = this.getAgesRanges(elements);
		this.typePieChartLabels = [];
		this.typePieChartData = [];
		this.ageRangeHoraLineChartData = [];
		ageRanges.map(range => {
			afluencias['ageRangeLine'][`${range}-hora`] = this.groupBy(elements.filter(emision => this.filterAgeRange(emision, range)), 'hour');
			this.ageRangeHoraLineChartData.push({data: afluencias['ageRangeLine'][`${range}-hora`], label: range, fill:false});
			afluencias['ageRangeLine'][`${range}-dia`] = this.groupBy(elements.filter(emision => this.filterAgeRange(emision, range)), 'date');
			this.ageRangeDiaLineChartData.push({data: afluencias['ageRangeLine'][`${range}-dia`], label: range, fill:false});
			afluencias['ageRangeLine'][`${range}-weekday`] = this.groupBy(elements.filter(emision => this.filterAgeRange(emision, range)), 'weekday');
			afluencias['typePie'][`${range}`] = Math.round(elements.filter(emision => this.filterAgeRange(emision, range)).length / this.factorCorrector);
			this.ageRangeDiaSemanaLineChartData.push({data: afluencias['ageRangeLine'][`${range}-weekday`], label: range, fill:false});
			let percentageTypePieChart =  (afluencias['typePie'][`${range}`]*100) / (elements.length / this.factorCorrector);
			this.typePieChartLabels.push(`${range} / ${percentageTypePieChart.toFixed(2)}%`);
			this.typePieChartData.push(afluencias['typePie'][`${range}`]);
			this.filteredByAgeRange[`${range}`] = [];
			this.filteredByAgeRange[`${range}`] = elements.filter(emision => this.filterAgeRange(emision, range));
		});
		this.ageRangeDiaLineChartLabels = [];
		this.ageRangeDiaLineChartLabels = this.getAfluenciasDiaChartLabels(elements);

		this.afluenciasHoraLineChartData = [
			{data: afluencias['horas'], label: this.translate.instant('Horas'), fill:false}
		];
		this.afluenciasDiaLineChartLabels = this.getAfluenciasDiaChartLabels(elements);
		this.afluenciasDiaLineChartData = [
			{data: afluencias['dia'], label: this.translate.instant('Dia'), fill:false}
		];
		this.afluenciasDiaSemanaLineChartData = [
			{data: afluencias['diaSemana'], label: this.translate.instant('Día de la semana'), fill:false}
		];
	
		this.genderHoraLineChartData = [
		  { data: afluencias['genderLine']["hombres-hora"], label: this.translate.instant('COMMON.men') },
		  { data: afluencias['genderLine']["mujeres-hora"], label: this.translate.instant('COMMON.women') },
		];
		this.genderDiaLineChartLabels = this.getAfluenciasDiaChartLabels(elements);
		this.genderDiaLineChartData = [
			{ data: afluencias['genderLine']["hombres-dia"], label: this.translate.instant('COMMON.men') },
			{ data: afluencias['genderLine']["mujeres-dia"], label: this.translate.instant('COMMON.women') },
		];
		this.genderDiaSemanaLineChartData = [
			{ data: afluencias['genderLine']["hombres-weekday"], label: this.translate.instant('COMMON.men') },
			{ data: afluencias['genderLine']["mujeres-weekday"], label: this.translate.instant('COMMON.women') },
		];

		this.genderPieChartData = [afluencias["genderPie"]['hombres'],afluencias["genderPie"]['mujeres']];
		elements.sort(this.compareEmail);
		const emails = this.getEmails(this.totalAfluencesArray);
		this.camarasHoraLineChartData = [];
		emails.map(email => {
			afluencias['cameras'][`${email}-hora`] = this.groupBy(this.totalAfluencesArray.filter(emission => emission.email === email), 'hour');
			afluencias['cameras'][`${email}-dia`] = this.groupBy(this.totalAfluencesArray.filter(emission => emission.email === email), 'date');
			afluencias['cameras'][`${email}-weekday`] = this.groupBy(this.totalAfluencesArray.filter(emission => emission.email === email), 'weekday');
		});
		afluencias['times'][`hora`] = this.calculateTimes(this.totalAfluencesArray, 'hour');
		afluencias['times'][`dia`] = this.calculateTimes(this.totalAfluencesArray, 'date');
		afluencias['times'][`weekday`] = this.calculateTimes(this.totalAfluencesArray, 'weekday');

		emails.map(email => {
			this.camarasHoraLineChartData.push({data: afluencias['cameras'][`${email}-hora`], label: email, fill:false});
			this.camarasDiaLineChartData.push({data: afluencias['cameras'][`${email}-dia`], label: email, fill:false});
			this.camarasDiaSemanaLineChartData.push({data: afluencias['cameras'][`${email}-weekday`], label: email, fill:false});
			this.allCamerasData.push({data: afluencias['cameras'][email], label: email});
		});
		this.timesHoraLineChartData.push({data: afluencias['times'][`hora`], label: 'Tiempo medio de estancia', fill:false});
		this.timesDiaLineChartData.push({data: afluencias['times'][`dia`], label: 'Tiempo medio de estancia', fill:false});
		this.timesDiaSemanaLineChartData.push({data: afluencias['times'][`weekday`], label: 'Tiempo medio de estancia', fill:false});
	}

	private calculateTimes(elements: any [], field: string){
		let result = [];
		if(field === 'hour')
			result = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		let fields:number[] = [];
		elements.map(element => {
		  if(result[element[field]]){
			const val = result[element[field]];
			fields[element[field]]++;
			result[element[field]] = (val + element.duration);
		  }else{
			fields[element[field]] = 1;
			result[element[field]] = element.duration;
		  }
		});
		let keys = Object.keys(result);
		if(result.length > 0)
			result.map((res, i) =>{
				result[i] = result[i] / fields[i];
			});
		else
			for (const property in result) {
				result[property] = result[property] / fields[property];
				console.log(`${property}: ${result[property]}`);
			}
		if(field === 'date'){
			let resp = [];
			this.timesDiaLineChartLabels.map((date, index) =>{
				resp[index] = 0;
				for(let i in result){
					if(i === date)
						resp[index] = result[i];
				}
			});
			result = resp;
		}
		if(field === 'weekday'){
			let resp = [0,0,0,0,0,0,0]
			for(let i in result){
				resp[parseInt(i) - 1] = result[i];
			}
			result = resp;
		}
		return result;
	}

	private getAgesRanges(data){
		let ranges: string[] = [];
		data.map(elem => {
			let range:string = '';
			if(elem.age >= 0 && elem.age < 10){
				range = '0-9';
			}else if(elem.age >= 10 && elem.age < 20){
				range = '10-19';
			}else if(elem.age >= 20 && elem.age < 30){
				range = '20-29';
			}else if(elem.age >= 30 && elem.age < 40){
				range = '30-39';
			}else if(elem.age >= 40 && elem.age < 50){
				range = '40-49';
			}else if(elem.age >= 50 && elem.age < 60){
				range = '50-59';
			}else if(elem.age >= 60 && elem.age < 70){
				range = '60-69';
			}else if(elem.age >= 70 && elem.age < 80){
				range = '70-79';
			}else if(elem.age >= 80 && elem.age < 90){
				range = '80-89';
			}else if(elem.age >= 90 && elem.age < 100){
				range = '90-99';
			}else if(elem.age >= 100 && elem.age < 110){
				range = '100-110';
			}
			if(!ranges.some(r => r === range))
				ranges.push(range)
			
			ranges.sort((a,b) => a > b ? 1 : -1)
		})
		return ranges;
	}
	private filterAgeRange(emission, range){
		if(range === '0-9'){
			return (emission.age <= 9);
		}else if(range === '10-19'){
			return (emission.age >= 10 && emission.age <= 19);
		}else if(range === '20-29'){
			return (emission.age >= 20 && emission.age <= 29);
		}else if(range === '30-39'){
			return (emission.age >= 30 && emission.age <= 39);
		}else if(range === '40-49'){
			return (emission.age >= 40 && emission.age <= 49);
		}else if(range === '50-59'){
			return (emission.age >= 50 && emission.age <= 59);
		}else if(range === '60-69'){
			return (emission.age >= 60 && emission.age <= 69);
		}else if(range === '70-79'){
			return (emission.age >= 70 && emission.age <= 79);
		}else if(range === '80-89'){
			return (emission.age >= 80 && emission.age <= 89);
		}else if(range === '90-99'){
			return (emission.age >= 90 && emission.age <= 99);
		}else if(range === '100-109'){
			return (emission.age >= 100 && emission.age <= 109);
		}
	}
	private getAfluenciasDiaChartLabels(data){
		let array = [];
		data.sort((a,b) => a.date > b.date ? 1 : -1);
		data.map(d => {
			if(!array.some(a => a === d.date))
				array.push(d.date);
		})
		return array;
	}
	
	private getEmails(data){
	  let array = [];
	  data.map(dat => {
		if(!array.some(e => e === dat.email)){
			array.push(dat.email);
		}
	  });
	  return array;
	}

	private compareEmail(a, b) {
		// Use toUpperCase() to ignore character casing
		const emailA = a.email.toUpperCase();
		const emailB = b.email.toUpperCase();
	  
		let comparison = 0;
		if (emailA > emailB) {
		  comparison = 1;
		} else if (emailA < emailB) {
		  comparison = -1;
		}
		return comparison;
	}
	  
	private groupBy(elements: any [], field: string){
		let result = [];
		if(field === 'hour')
			result = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		elements.map(element => {
		  if(result[element[field]]){
			result[element[field]]++;
		  }else{
			result[element[field]] = 1
		  }
		});
		if(field === 'date'){
			let resp = [];
			this.camarasDiaLineChartLabels.map((date, index) =>{
				resp[index] = 0;
				for(let i in result){
					if(i === date)
						resp[index] = result[i];
				}
			});
			result = resp;
		}
		if(field === 'weekday'){
			let resp = [0,0,0,0,0,0,0]
			for(let i in result){
				resp[parseInt(i) - 1] = result[i];
			}
			result = resp;
		}
		let res:number[] = [];
		result.map(r => res.push(Math.round(r / this.factorCorrector)));
		return res;
	}
	public excelExport(){
		let startDate = moment(this.data.startDate).format('YYY-MM-DD');
		let endDate = moment(this.data.endDate).format('YYY-MM-DD');
		let dataExcel:any[] = [];
		const genero = {
			male: this.translate.instant('COMMON.man'),
			female:this.translate.instant('COMMON.woman')
		}
		const rango = this.getAgesRanges(this.afluencias);
		this.afluencias.map(afluencia => {
			const site = this.sites.filter(site => site.idsite === afluencia.site_id);
			const data:Object = {
				'Centro': site[0].name,
				'Cámara': afluencia.email,
				'Fecha': afluencia.date,
				'Hora': afluencia.time,
				'Género': genero[afluencia.gender],
				'Rango de edad': this.getAgeRange(afluencia),
				//'Edad': afluencia.age
			}
			dataExcel.push(data);
		});
		this.excelService.exportAsExcelFile(dataExcel, `${this.customer.name}-${startDate}-${endDate}`);
	}

	private getAgeRange(afluencia){
		let range = '';
		if (afluencia.age <= 20){
			range = 'Joven';
		}else if(afluencia.age > 20 && afluencia.age <= 50){
			range = 'Adulto';
		}else{
			range = 'Senior';
		}
		return range;
	}
	public pdfExport(){
		this.loadingpdf = true;
		setTimeout(() => {this.loadingpdf = false;}, 1000);
		let startDate = moment(this.data.startDate).format('YYYY-MM-DD');
		let endDate = moment(this.data.endDate).format('YYYY-MM-DD');
		const DATA = this.htmlData.nativeElement;
		const name = `${this.customer.name}-${startDate}-${endDate}`;
		this.pdfService.generatePdf(DATA, name);
	}
	public filterPageData($event){
	  this.store = $event;
	  this.syncViewer();
	}

	volverForm($event){
		this.onBackToForm.emit($event);
	}

	public chartClicked(data){
		this.recalculateTypeChart(data)
		if(data === 0){
			this.selectedGender = "Hombres";
		}else{
			this.selectedGender = "Mujeres";
		}
	}

	private recalculateTypeChart(data){
		const genres = ['male', 'female'];
		const elements = this.allAfluences;
		const ageRanges:string[] = this.getAgesRanges(elements);
		let afluencias = [];
		afluencias['typePie'] = [];
		this.typePieChartData = [];
		this.typePieChartLabels = [];
		ageRanges.map(range => {
			afluencias['typePie'][`${range}`] = Math.round(elements.filter(emision => this.filterAgeRange(emision, range) && emision.gender === genres[data]).length / this.factorCorrector);
			let percentage = (afluencias['typePie'][`${range}`] * 100) / elements.length;
			this.typePieChartLabels.push(`${range} / ${percentage.toFixed(2)}%`);
			this.typePieChartData.push(afluencias['typePie'][`${range}`]);
		})
		console.log(this.typePieChartData);
	}

	private getRanking(elements){
		let ranking:any[] = [];
		const ageRanges:string[] = this.getAgesRanges(elements);
		ageRanges.map(range => {
			ranking.push(
				{
					range: range, 
					genre: 'male', 
					percentage: this.calculatePercentage(range, 'male')
				}, 
				{
					range: range, 
					genre: 'female', 
					percentage: this.calculatePercentage(range, 'female') 
				});
		})
		ranking.sort((a,b) => a.percentage > b.percentage ? -1 : 1)
		this.ranking = [];
		for(let i = 0; i < 5; i++){
			if(ranking[i])
				this.ranking.push(ranking[i])
		}
	}
	private calculatePercentage(range, genre){
		const total = this.allAfluences.length;
		let count = this.allAfluences.filter(afluence => this.filterAgeRange(afluence, range) && afluence.gender === genre).length;
		return (count*100) / total;
	}
}