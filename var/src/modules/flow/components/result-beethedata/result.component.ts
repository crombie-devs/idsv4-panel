import { Site } from "src/modules/site/models/site";
import { Component, Input, SimpleChange, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { Auth } from 'src/modules/auth/models/auth';
import { IResult } from 'src/models/iresult';
import {MatGridListModule} from '@angular/material/grid-list';

import moment from 'moment';
import { User } from 'src/modules/user/models/user';
import { FormField } from 'src/modules/form-field/models/form-field';
import { UserStore } from 'src/modules/user/stores/user.store';

// For exports
import { ExcelService } from 'src/services/excel.service';
import { PdfService } from 'src/services/pdf.service';

// For graphics
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

import {TranslateService} from '@ngx-translate/core';
import { Afluence } from 'src/modules/flow/models/afluence';
import { Camera } from 'src/modules/camera/models/camera';
import { SiteService } from "src/modules/site/services/site.service";

@Component({
  selector: 'flow-result-beethedata',
  templateUrl: './view.component.html',
  styleUrls: ['./styles.component.scss']
})

export class FlowResultBeethedataComponent {
	@ViewChild('htmlData') htmlData:ElementRef;
	@Input() data:any;
	@Input() cameras: any[] = [];
	@Output() onBackToForm:EventEmitter<any> = new EventEmitter(null);

	public afluencias: any[] = [];
	public me: any;
	public store: string = null;
	public site: any = {};
	public sites: Site[] = [];
	public camera: any = {};
	public allCameras: Camera[] = [];
	public customer: any = {};
	public totalAfluencias: string = '0';
	public isLoading: boolean = true;
	public horaPunta: string = '00:00';
	public bestWeekday:string = 'lunes';
	public bestDay: string;
	public aspectRatio: boolean = false;
	public chartHeihgt: string = '500';
	public lastAfluences: Afluence[] = [];
	public loading: boolean = true;
	public groupedSitesCameras:any[] = [];
	public allAfluences:any[] = [];
	public ranking:any[] = [];
	public recurrenceRanking: any[] = [];
	public filteredByAgeRange:any[] = [];
	public hombres = 0;
	public mujeres = 0;
	public hombresRecurrentes = 0;
	public mujeresRecurrentes = 0;
	public numMovements: number = 0;
	public totalVisits:any[] = [];
	public totalAfluencesArray:any[] = [];
	public selectedGender:string = '';
	public selectedRecurrenceGender:string = '';
	public allRecurrencers:any[] = [];
	public allRecurrencersCounter = 0;
	public totalRecurrentsVisits = 0;
	public uniqueRecurrentVisits = 0;
	public filterRecurrencers:string;
	public filterAllVisitors:string;

	public fieldDataTotal: FormField = {
		label: "Elegir:",
		name: 'total',
		placeholder: '',
		options: [
			{name: 'Horas', value: 1 },
			{name: 'Días', value: 2 },
			{name: 'Días de la semana', value: 3 }
		],
		multiple:false,
		required: false
	};
	public fieldDataGenderRange: FormField = {
		label: "Elegir:",
		name: 'genderRange',
		placeholder: '',
		options: [
			{name: 'Horas', value: 1 },
			{name: 'Días', value: 2 },
			{name: 'Días de la semana', value: 3 }
		],
		multiple:false,
		required: false
	};
	public fieldDataCamera: FormField = {
		label: "Elegir:",
		name: 'camera',
		placeholder: '',
		options: [
			{name: 'Horas', value: 1 },
			{name: 'Días', value: 2 },
			{name: 'Días de la semana', value: 3 }
		],
		multiple:false,
		required: false
	};
	public formGroup:FormGroup = new FormGroup({
		total: new FormControl(1),
		camera: new FormControl(1),
		genderRange: new FormControl(1),
	});

	afluenciasHoraLineChartData: ChartDataSets[] = [
		{label : this.translate.instant('Horas'), data : [0]}];
	afluenciasHoraLineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];
	afluenciasDiaLineChartData: ChartDataSets[] = [
		{label : this.translate.instant('Días'), data : [0]}];
	afluenciasDiaLineChartLabels: Label[] = [];
	afluenciasDiaSemanaLineChartData: ChartDataSets[] = [
		{label : this.translate.instant('Horas'), data : [0]}];
	afluenciasDiaSemanaLineChartLabels: Label[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

	genderHoraLineChartData: ChartDataSets[] = [
		{label : this.translate.instant('COMMON.men'), data : [0]},
		{label : this.translate.instant('COMMON.women'), data : [0]},];
	genderDiaLineChartData: ChartDataSets[] = [
		{label : this.translate.instant('COMMON.men'), data : [0]},
		{label : this.translate.instant('COMMON.women'), data : [0]},];
	genderDiaSemanaLineChartData: ChartDataSets[] = [
		{label : this.translate.instant('COMMON.men'), data : [0]},
		{label : this.translate.instant('COMMON.women'), data : [0]},];
		
	genderHoraLineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];
	genderDiaLineChartLabels: Label[] = [];
	genderDiaSemanaLineChartLabels: Label[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
	
	ageRangeHoraLineChartData: ChartDataSets[] = [];
	ageRangeHoraLineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];
	ageRangeDiaLineChartData: ChartDataSets[] = [];
	ageRangeDiaLineChartLabels: Label[] = [];
	ageRangeDiaSemanaLineChartData: ChartDataSets[] = [];
	ageRangeDiaSemanaLineChartLabels: Label[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
	
	camarasHoraLineChartData: ChartDataSets[] = [];
	camarasDiaLineChartData: ChartDataSets[] = [];
	camarasDiaSemanaLineChartData: ChartDataSets[] = [];
	allCamerasData: ChartDataSets[] = [];
	
	camarasHoraLineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];
	camarasDiaLineChartLabels: Label[] = [];
	camarasDiaSemanaLineChartLabels: Label[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
	
	genderPieChartData: number[] = [0,0];
	genderRecurrencePieChartData: number[] = [0,0];
	
	public gender:string[] = ['COMMON.man', 'COMMON.woman'];
	genderPieChartLabels: Label[] =[];
	genderRecurrencePieChartLabels: Label[] =[];
	typePieChartData: number[] = [];
	typePieChartLabels: Label[] = [];
	typeRecurrencePieChartData: number[] = [];
	typeRecurrencePieChartLabels: Label[] = [];

	constructor(
		private router: Router,
		private ws: WSService,
		private wsSite: SiteService,
		private debug: DebugStore,	
		private dialog: DialogService,
    	private astore: AuthStore,
		private localStorage: MYLocalStorage,
		private ustore: UserStore,
		private excelService:ExcelService,
		private pdfService:PdfService,
		private translate: TranslateService) {	
  	}
  
  	public ngOnInit() {
		this.debug.addInfo('ResulFlowPage', 'OnInit');
		this.me = this.ustore.getUser();
		this.syncViewer();
		this.translate.get(this.gender).subscribe(translations => {
			this.genderPieChartLabels = Object.values(translations);
			this.genderRecurrencePieChartLabels = Object.values(translations);
		})
	}
	private resetData(){
		this.totalRecurrentsVisits = 0;
		this.uniqueRecurrentVisits = 0;
		this.afluencias = [];
		this.sites = [];
		this.camera = {};
		this.allCameras = [];
		this.totalAfluencias = '0';
		this.lastAfluences = [];
		this.groupedSitesCameras = [];
		this.allAfluences = [];
		this.allRecurrencers = [];
		this.ranking = [];
		this.filteredByAgeRange = [];
		this.hombres = 0;
		this.hombresRecurrentes = 0;
		this.mujeres = 0;
		this.mujeresRecurrentes = 0;
		this.ageRangeDiaLineChartLabels = [];
		this.typePieChartLabels = [];
		this.numMovements = 0;
		this.totalVisits = [];
		this.totalAfluencesArray = [];
	}

	public ngOnChanges(changes: SimpleChange){
	  if(!changes['data'].isFirstChange())
		this.syncViewer();
	}
	private syncViewer() {
		this.resetData();
		this.getSites();
		this.getCameras();
		this.loading = true;
		this.isLoading = true;
		this.cameras = [];
		this.getCustomer();
		this.getSite();
		this.getCamera();
		this.getAfluencias();
		this.getLastAfluences();
	}
	public getSites(){
		this.wsSite.list().subscribe((sites: Site[]) => {
			this.sites =  sites;			 
		});
	}
	public getCameras(){
		this.ws.cameras().subscribe((result: IResult) => {
			if(result.success) { 
				this.allCameras =  result.data;
				this.groupCamerasBySite();
			}else {
				this.debug.addError('WS.findPeople', result.data);
				this.dialog.warning('COMPONENTS.flow.result.dialogs.warning.title', result.data); 
			}
		  });
	}
	public groupCamerasBySite(){
		let group: any[] = [];
		this.allCameras.map(camera => {
			if(this.data.site && this.data.site !== 0){
				if(!group.some(g => g.site.idsite === camera.site.idsite)){
					if(camera.site.idsite === this.data.site)
						group.push({site: camera.site, cameras: []})
				}
			}else{
				if(!group.some(g => g.site.idsite === camera.site.idsite))
					group.push({site: camera.site, cameras: []})
			}
		})
		this.allCameras.map(camera => {
			group.map((g,i) => {
				if(g.site.idsite === camera.site.idsite)
					if(this.data.camera){
						if(camera.idcamera === this.data.camera)
							g.cameras.push(camera)
					}
					else
						g.cameras.push(camera)
			})
		})
		this.groupedSitesCameras = group;
	}
	public getCustomer(){
		this.ws.getCustomer(this.data.customer).subscribe((result: IResult) => {
			if(result.success) { 
				this.customer =  result.data;
			  }else {
				this.debug.addError('WS.findPeople', result.data);
				this.dialog.warning('COMPONENTS.flow.result.dialogs.warning.title', result.data); 
			  }
		  });
	}
	public getSite(){
		if(this.data.site !== 0){
			if(!this.sites){
				this.wsSite.get(this.data.site).subscribe((site: Site) => {
					this.site =  site;					  
				});
			}else{
				this.sites.map(site => {
					if(site.idsite === this.data.site)
						this.site = site;
				})
			}
		}else{
			this.site.name = this.translate.instant('Todo');
		}
	}
	public getCamera(){
		if(this.data.camera && this.data.camera !== 0){
			this.ws.getCamera(this.data.camera).subscribe((result: IResult) => {
				if(result.success) { 
					this.camera =  result.data;
				  }else {
					this.debug.addError('WS.findPeople', result.data);
					this.dialog.warning('COMPONENTS.flow.result.dialogs.warning.title', result.data); 
				  }
			  });
		}else{
			this.camera.name = this.translate.instant('Todo');
		}
	}
	public getAfluencias(){
		let startDate = moment(this.data.startDate).format('YYYY-MM-DD HH:mm')
		let endDate = moment(this.data.endDate).format('YYYY-MM-DD HH:mm')
		let params: any = {
			datetime: {'$gte': startDate, '$lte': endDate},
			customer_id: this.data.customer,
		}

		if(this.data.site && this.data.site !== 0)
			params.site_id = this.data.site;
		
		if(this.data.camera && this.data.camera !== 0)
			params.camera_id = this.data.camera;

		return this.ws.analyticsAfluenciasSubmit(params).subscribe((result: IResult) => {
		  if(result.success) { 
			  this.numMovements = 0;
			  let elements:any[] = [];
			  if(this.data.camera === null || this.data.camera === 0 || this.data.camera === 120 || this.data.camera === 121) {
			  	result.data.reverse().map(element => {
					if(element.target_id !== null){
						if(!elements.some(el => el.target_id === element.target_id)) {	
							elements.push(element);
						}
					} else {
						if(element.camera_id === 120 || element.camera_id === 121)
							this.numMovements++;
					}
				});
			  } else{
				elements = result.data;
				this.numMovements = elements.length;
			  }
			  this.totalVisits = elements;

			  this.totalAfluencias = new Intl.NumberFormat("de-DE").format(elements.length);
			  this.allAfluences = elements;
			  this.totalAfluencesArray = result.data;
			  if(this.data.camera === null || this.data.camera === 0 || this.data.camera === 120 || this.data.camera === 121){
				this.getRanking(elements);
			  }
			  this.processAllAfluencias(elements);
			  this.getRecurrenceData(elements);
			  this.getHoraPunta(this.totalVisits);
			  this.getBestDay(elements);
			  this.getBestWeekDay(elements);
			  this.addToGroupedData(result.data, elements);
			  this.afluencias = elements;
			  this.isLoading = false;
			} else {
			  this.debug.addError('WS.findPeople', result.data);
			  this.dialog.warning('COMPONENTS.flow.result.dialogs.warning.title', result.data); 
			}
		});
	}
	private getRecurrenceData(data){
		let unique:any[] = [];
		let recurrence:any[] = [];
		let uniqueRecurrencers:any[] = [];
		data.map(d => {
			if(!unique.some(e => e.recurrence_id === d.recurrence_id))
				unique.push(d)
		})
		unique.map(u => {
			data.map(d => {
				if(u.recurrence_id === d.recurrence_id){
					if(!recurrence.some(r => r.recurrence_id === d.recurrence_id))
						recurrence.push({recurrence_id: d.recurrence_id, counter: 1})
					else
						recurrence.map(r => {
							if(r.recurrence_id === d.recurrence_id)
								r.counter++;
						})
				}
			})
		})
		this.allRecurrencersCounter = recurrence.length;
		recurrence.map(r => {
			if(r.counter > 1)
				unique.map(u => {
					if(u.recurrence_id === r.recurrence_id){
						u.counter = r.counter;
						uniqueRecurrencers.push(u);
					}
				})
		})
		this.allRecurrencers = uniqueRecurrencers;
		this.processAllRecurrences(uniqueRecurrencers);
		this.getRecurrenceRanking(uniqueRecurrencers)

	}
	private getAverageAge(total, filter = null){
		let data = []
		if(filter === null)
			data = total.filter(emision => emision.gender !== 'unknown')
		else
			data = total.filter(emision  => emision.gender === filter);
		let counter = 0;
		let totalAge = 0;
		data.map(d => {
			if(d.age > -1){
				counter++;
				totalAge += d.age;
			}
		})
		return totalAge/counter;
	}
	private getLastAfluences() {
		if(!this.data.camera || this.data.camera === 120 || this.data.camera === 121){
			let filter: any = {
				customer_id: this.me.customer.idcustomer,
				gender: { "$ne": "unknown"},
			};
			if(this.data.site !== 0){
				filter.site_id = this.data.site
			}
			if(this.data.camera)
				filter.camera_id = this.data.camera;

			let params = {
				filter: filter,
				sort: {
					datetime: -1
				},
				limit: 4
			}
			this.ws.detailFacesByCamera(params).subscribe(result => {
				if(result.success)
					result.data.map(af => {
						this.lastAfluences.push(af)
					})
				this.loading = false;
			})
		}
    }
	private getHoraPunta(elements){
		  let array = [];
		  elements.forEach(element => {
			if(!array[element.hour]){
				array[element.hour] = 1;
			}else{
				array[element.hour]++;
			}
		  });
		  let mayor = 0;
		  let hour = 0;
		  array.forEach((dat, index) => {
			if(dat > mayor){
				mayor = dat;
				hour = index;
			}
		  })
		  let hourString:string;
		  if(hour < 10){
			  hourString = `0${hour}:00`;
		  }else{
			  hourString = `${hour}:00`;
		  }
		  this.horaPunta = hourString;
	}
	private getBestDay(elements){
		let array = []
		let arrayDays = [];
		elements.map(element => {
		  if(!array[element.date]){
			arrayDays.push(element.date);
			array[element.date] = 1;
		  }else{
			array[element.date]++;
		  }
		});
		let mayor = 0;
		let day:string;;
		for(let i in array){
			if(array[i] > mayor){
				mayor = array[i];
				day = i;
			}
		}

		this.bestDay = day;  
	}
	
	private getBestWeekDay(elements){
		let array = []
		elements.map(element => {
		  if(!array[element.weekday]){
			  array[element.weekday] = 1;
		  }else{
			  array[element.weekday]++;
		  }
		});
		let mayor = 0;
		let weekday = 1;
		array.map((dat, index) => {
		  if(dat > mayor){
			  mayor = dat;
			  weekday = index;
		  }
		})
		const arrayDays = [
			'',
			'WEEKDAYS.mon', 
			'WEEKDAYS.tue', 
			'WEEKDAYS.wed', 
			'WEEKDAYS.thu', 
			'WEEKDAYS.fri', 
			'WEEKDAYS.sat',
			'WEEKDAYS.sun'];
		this.bestWeekday = arrayDays[weekday];  
	}

	private processAllAfluencias(elements: any [], day:string = 'today'){
		let afluencias = [];
		afluencias['genderPie'] = [];
		afluencias['genderLine'] = [];
		afluencias['typePie'] = [];
		afluencias['ageRangeLine'] = [];
		afluencias['dayLine'] = [];
		afluencias['cameras'] = [];
		afluencias['horas'] = this.groupBy(this.totalVisits, 'hour');
		afluencias['dia'] = this.groupBy(this.totalVisits, 'date');
		afluencias['diaSemana'] = this.groupBy(this.totalVisits, 'weekday');
	
		afluencias['genderPie']["hombres"] = elements.filter(emision  => emision.gender === 'male').length;
		afluencias['genderPie']["mujeres"] = elements.filter(emision => emision.gender === 'female').length;
		this.hombres = Math.round((afluencias['genderPie']['hombres']*100) /(afluencias['genderPie']['hombres'] +afluencias['genderPie']['mujeres']));
		this.mujeres = Math.round((afluencias['genderPie']['mujeres']*100) /(afluencias['genderPie']['hombres'] +afluencias['genderPie']['mujeres']));
		afluencias['genderPie']["unknown"] = elements.filter(emision => emision.gender === 'unknown').length > 0 ? elements.filter(emision => emision.gender === 'unknown').length - (afluencias['genderPie']["mujeres"] + afluencias['genderPie']["hombres"]) : 0;
		afluencias['genderLine']["mujeres-hora"] = this.groupBy(elements.filter(emision => emision.gender === 'female'), 'hour');
		afluencias['genderLine']["hombres-hora"] = this.groupBy(elements.filter(emision => emision.gender === 'male'), 'hour');
		afluencias['genderLine']["mujeres-dia"] = this.groupBy(elements.filter(emision => emision.gender === 'female'), 'date');
		afluencias['genderLine']["hombres-dia"] = this.groupBy(elements.filter(emision => emision.gender === 'male'), 'date');
		afluencias['genderLine']["mujeres-weekday"] = this.groupBy(elements.filter(emision => emision.gender === 'female'), 'weekday');
		afluencias['genderLine']["hombres-weekday"] = this.groupBy(elements.filter(emision => emision.gender === 'male'), 'weekday');
		
		const ageRanges:string[] = this.getAgesRanges(elements);
		this.typePieChartLabels = [];
		this.typePieChartData = [];
		this.ageRangeHoraLineChartData = [];
		ageRanges.map(range => {
			afluencias['ageRangeLine'][`${range}-hora`] = this.groupBy(elements.filter(emision => this.filterAgeRange(emision, range)), 'hour');
			this.ageRangeHoraLineChartData.push({data: afluencias['ageRangeLine'][`${range}-hora`], label: range, fill:false});
			afluencias['ageRangeLine'][`${range}-dia`] = this.groupBy(elements.filter(emision => this.filterAgeRange(emision, range)), 'date');
			this.ageRangeDiaLineChartData.push({data: afluencias['ageRangeLine'][`${range}-dia`], label: range, fill:false});
			afluencias['ageRangeLine'][`${range}-weekday`] = this.groupBy(elements.filter(emision => this.filterAgeRange(emision, range)), 'weekday');
			afluencias['typePie'][`${range}`] = elements.filter(emision => this.filterAgeRange(emision, range)).length;
			this.ageRangeDiaSemanaLineChartData.push({data: afluencias['ageRangeLine'][`${range}-weekday`], label: range, fill:false});
			this.typePieChartLabels.push(range);
			this.typePieChartData.push(afluencias['typePie'][`${range}`]);
			this.filteredByAgeRange[`${range}`] = [];
			this.filteredByAgeRange[`${range}`] = elements.filter(emision => this.filterAgeRange(emision, range));
		})
		this.ageRangeDiaLineChartLabels = [];
		this.ageRangeDiaLineChartLabels = this.getAfluenciasDiaChartLabels(elements);

		this.afluenciasHoraLineChartData = [
			{data: afluencias['horas'], label: this.translate.instant('Horas'), fill:false}
		];
		this.afluenciasDiaLineChartLabels = this.getAfluenciasDiaChartLabels(elements);
		this.afluenciasDiaLineChartData = [
			{data: afluencias['dia'], label: this.translate.instant('Dia'), fill:false}
		];
		this.afluenciasDiaSemanaLineChartData = [
			{data: afluencias['diaSemana'], label: this.translate.instant('Día de la semana'), fill:false}
		];
	
		this.genderHoraLineChartData = [
		  { data: afluencias['genderLine']["hombres-hora"], label: this.translate.instant('COMMON.men') },
		  { data: afluencias['genderLine']["mujeres-hora"], label: this.translate.instant('COMMON.women') },
		];
		this.genderDiaLineChartLabels = this.getAfluenciasDiaChartLabels(elements);
		this.genderDiaLineChartData = [
			{ data: afluencias['genderLine']["hombres-dia"], label: this.translate.instant('COMMON.men') },
			{ data: afluencias['genderLine']["mujeres-dia"], label: this.translate.instant('COMMON.women') },
		];
		this.genderDiaSemanaLineChartData = [
			{ data: afluencias['genderLine']["hombres-weekday"], label: this.translate.instant('COMMON.men') },
			{ data: afluencias['genderLine']["mujeres-weekday"], label: this.translate.instant('COMMON.women') },
		];

		this.genderPieChartData = [afluencias["genderPie"]['hombres'],afluencias["genderPie"]['mujeres']];
		elements.sort(this.compareEmail);
		const emails = this.getEmails(this.totalAfluencesArray);
		this.camarasHoraLineChartData = [];
		emails.map(email => {
			afluencias['cameras'][`${email}-hora`] = this.groupBy(this.totalAfluencesArray.filter(emission => emission.email === email), 'hour');
			afluencias['cameras'][`${email}-dia`] = this.groupBy(this.totalAfluencesArray.filter(emission => emission.email === email), 'date');
			afluencias['cameras'][`${email}-weekday`] = this.groupBy(this.totalAfluencesArray.filter(emission => emission.email === email), 'weekday');
		});
		emails.map(email =>{
			this.camarasHoraLineChartData.push({data: afluencias['cameras'][`${email}-hora`], label: email, fill:false});
			this.camarasDiaLineChartData.push({data: afluencias['cameras'][`${email}-dia`], label: email, fill:false});
			this.camarasDiaSemanaLineChartData.push({data: afluencias['cameras'][`${email}-weekday`], label: email, fill:false});
			this.allCamerasData.push({data: afluencias['cameras'][email], label: email});
		});
		this.camarasDiaLineChartLabels = this.getAfluenciasDiaChartLabels(this.totalAfluencesArray);
	}

	private processAllRecurrences(elements: any [], day:string = 'today'){
		this.uniqueRecurrentVisits = elements.length;
		elements.map(e => {
			this.totalRecurrentsVisits += e.counter;
		}) 
		let afluencias = [];
		afluencias['genderRecurrencePie'] = [];
		afluencias['typeRecurrencePie'] = [];
	
		afluencias['genderRecurrencePie']["hombres"] = elements.filter(emision  => emision.gender === 'male').length;
		afluencias['genderRecurrencePie']["mujeres"] = elements.filter(emision => emision.gender === 'female').length;
		this.hombresRecurrentes = Math.round((afluencias['genderRecurrencePie']['hombres']*100) /(afluencias['genderRecurrencePie']['hombres'] +afluencias['genderRecurrencePie']['mujeres']));
		this.mujeresRecurrentes = Math.round((afluencias['genderRecurrencePie']['mujeres']*100) /(afluencias['genderRecurrencePie']['hombres'] +afluencias['genderRecurrencePie']['mujeres']));
		
		const ageRanges:string[] = this.getAgesRanges(elements);
		this.typeRecurrencePieChartLabels = [];
		this.typeRecurrencePieChartData = [];
		ageRanges.map(range => {
			afluencias['typeRecurrencePie'][`${range}`] = elements.filter(emision => this.filterAgeRange(emision, range)).length;
			this.typeRecurrencePieChartLabels.push(range);
			this.typeRecurrencePieChartData.push(afluencias['typeRecurrencePie'][`${range}`]);
			this.filteredByAgeRange[`${range}`] = [];
			this.filteredByAgeRange[`${range}`] = elements.filter(emision => this.filterAgeRange(emision, range));
		})
		this.genderRecurrencePieChartData = [afluencias["genderRecurrencePie"]['hombres'],afluencias["genderRecurrencePie"]['mujeres']];
	}

	private getAgesRanges(data){
		let ranges: string[] = [];
		data.map(elem => {
			let range:string = '';
			if(elem.age <= 10){
				range = '01-10'
			}else{
				let age = elem.age.toString()
				range = `${age[0]}1-${parseInt(age[0]) + 1}0`
			}
			if(!ranges.some(r => r === range))
				ranges.push(range)
			
			ranges.sort((a,b) => a > b ? 1 : -1)
		})
		return ranges;
	}
	private filterAgeRange(emission, range){
		const rangeArray = range.split('-');
		return (emission.age >= parseInt(rangeArray[0]) && emission.age < rangeArray[1])
	}
	private getAfluenciasDiaChartLabels(data){
		let array = [];
		data.sort((a,b) => a.date > b.date ? 1 : -1);
		data.map(d => {
			if(!array.some(a => a === d.date))
				array.push(d.date);
		})
		return array;
	}
	private addToGroupedData(totalElements, elements){
		let carasUnicas:any[] = [];
		this.groupedSitesCameras.map(group => {
			
			group.total = 0;
			group.cameras.map(camera => {
				carasUnicas = [];
				camera.male = 0;
				camera.female = 0;
				camera.unknown = 0;
				camera.total = 0;
				camera.totalVisits = 0;
				if(camera.idcamera === 120 || camera.idcamera === 121){
					elements.map(element => {
						if(camera.idcamera === element.camera_id){
							if(!carasUnicas.some(c => c.recurrence_id === element.recurrence_id))
								carasUnicas.push(element);
							if(element.gender === 'male' || element.gender === 'female'){
								camera.total++;
								if(element.gender === 'male')
									camera.male++;
								else if(element.gender === 'female')
									camera.female++
							}
						}
					})
					camera.malepercentage = Math.round((camera.male * 100) / (camera.total));
					camera.femalepercentage = Math.round((camera.female * 100) / (camera.total));
					camera.uniqueVisits = carasUnicas.length;
					this.totalVisits.map(element => {
						if(camera.idcamera === element.camera_id){
							if(element.gender !== 'male' || element.gender !== 'female'){
								camera.totalVisits++;
							}
						}
					})
				}else{
					totalElements.map(element => {
						if(camera.idcamera === element.camera_id){
							camera.unknown++;
							camera.total++;
							group.total++;
						}
					})
				}
			})
			this.groupedSitesCameras.map(group => {
				group.cameras.map(camera => {
					if(camera.idcamera !== 120 && camera.idcamera !== 121){
						camera.visitpercentage = Math.round((camera.total*100) / group.total);
					}
				})
			})
		});
	}
	
	private getEmails(data){
	  let array = [];
	  data.map(dat => {
		if(!array.some(e => e === dat.email)){
			array.push(dat.email);
		}
	  });
	  return array;
	}

	private compareEmail(a, b) {
		// Use toUpperCase() to ignore character casing
		const emailA = a.email.toUpperCase();
		const emailB = b.email.toUpperCase();
	  
		let comparison = 0;
		if (emailA > emailB) {
		  comparison = 1;
		} else if (emailA < emailB) {
		  comparison = -1;
		}
		return comparison;
	}
	  
	private groupBy(elements: any [], field: string){
		let result = [];
		if(field === 'hour')
			result = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		elements.map(element => {
		  if(result[element[field]]){
			result[element[field]]++;
		  }else{
			result[element[field]] = 1
		  }
		});
		if(field === 'date'){
			let resp = []
			for(let i in result){
				resp.push(result[i]);
			}
			result = resp;
		}
		if(field === 'weekday'){
			let resp = [0,0,0,0,0,0,0]
			for(let i in result){
				resp[parseInt(i) - 1] = result[i];
			}
			result = resp;
		}
		return result;
	}
	public excelExport(){
		let startDate = moment(this.data.startDate).format('YYY-MM-DD');
		let endDate = moment(this.data.endDate).format('YYY-MM-DD');
		let dataExcel:any[] = [];
		const genero = {
			male: this.translate.instant('COMMON.man'),
			female:this.translate.instant('COMMON.woman')
		}
		const rango = this.getAgesRanges(this.afluencias);
		this.afluencias.map(afluencia => {
			const site = this.sites.filter(site => site.idsite === afluencia.site_id);
			const data:Object = {
				'Centro': site[0].name,
				'Cámara': afluencia.email,
				'Fecha': afluencia.date,
				'Hora': afluencia.time,
				'Género': genero[afluencia.gender],
				'Rango de edad': this.getAgeRange(afluencia, rango),
				'Edad': afluencia.age,
				'Recurrente': this.isRecurrent(afluencia)
			}
			dataExcel.push(data);
		});
		this.excelService.exportAsExcelFile(dataExcel, `${this.customer.name}-${this.site.name}-${startDate}-${endDate}`);
	}

	private isRecurrent(afluencia){
		let isRecurrent:string = 'No';
		this.allRecurrencers.map(r =>{
			if(r.recurrence_id === afluencia.recurrence_id)
				if(r.counter > 1)
					isRecurrent = "Sí";
		})
		return isRecurrent;
	}

	private getAgeRange(afluencia, rangos){
		let range = ''
		rangos.map(rango => {
			const r:string[] = rango.split('-');
			if(afluencia.age >= parseInt(r[0]) && afluencia.age <= parseInt(r[1]))
				range = rango;
		})
		return range;
	}
	public pdfExport(){
		let startDate = moment(this.data.startDate).format('YYY-MM-DD');
		let endDate = moment(this.data.endDate).format('YYY-MM-DD');
		const DATA = this.htmlData.nativeElement;
		const name = `${this.customer.name}-${this.site.name}${startDate}-${endDate}`;
		this.pdfService.generatePdf(DATA, name);
	}
	public filterPageData($event){
		  this.store = $event;
		  this.syncViewer();
	}

	volverForm($event){
		this.onBackToForm.emit($event);
	}

	public chartClicked(data){
		this.recalculateTypeChart(data)
		if(data === 0){
			this.selectedGender = "Hombres";
			this.filterAllVisitors = 'male';
		}else{
			this.selectedGender = "Mujeres";
			this.filterAllVisitors = 'female';
		}
	}
	public chartRecurrenceClicked(data){
		this.recalculateRecurrenceTypeChart(data)
		if(data === 0){
			this.selectedRecurrenceGender = "Hombres";
			this.filterRecurrencers = 'male';
		}else{
			this.selectedRecurrenceGender = "Mujeres";
			this.filterRecurrencers = 'female';
		}
	}

	private recalculateTypeChart(data){
		const genres = ['male', 'female'];
		const elements = this.allAfluences;
		const ageRanges:string[] = this.getAgesRanges(elements);
		let afluencias = [];
		afluencias['typePie'] = [];
		this.typePieChartData = [];
		this.typePieChartLabels = [];
		ageRanges.map(range => {
			afluencias['typePie'][`${range}`] = elements.filter(emision => this.filterAgeRange(emision, range) && emision.gender === genres[data]).length;
			this.typePieChartLabels.push(range);
			this.typePieChartData.push(afluencias['typePie'][`${range}`]);
		})
	}

	private recalculateRecurrenceTypeChart(data){
		const genres = ['male', 'female'];
		const elements = this.allRecurrencers;
		const ageRanges:string[] = this.getAgesRanges(elements);
		let afluencias = [];
		afluencias['typeRecurrencePie'] = [];
		this.typeRecurrencePieChartData = [];
		this.typeRecurrencePieChartLabels = [];
		ageRanges.map(range => {
			afluencias['typeRecurrencePie'][`${range}`] = elements.filter(emision => this.filterAgeRange(emision, range) && emision.gender === genres[data]).length;
			this.typeRecurrencePieChartLabels.push(range);
			this.typeRecurrencePieChartData.push(afluencias['typeRecurrencePie'][`${range}`]);
		})
	}

	private getRanking(elements){
		let ranking:any[] = [];
		const ageRanges:string[] = this.getAgesRanges(elements);
		ageRanges.map(range => {
			ranking.push(
				{
					range: range, 
					genre: 'male', 
					percentage: this.calculatePercentage(range, 'male')
				}, 
				{
					range: range, 
					genre: 'female', 
					percentage: this.calculatePercentage(range, 'female') 
				});
		})
		ranking.sort((a,b) => a.percentage > b.percentage ? -1 : 1)
		this.ranking = [];
		for(let i = 0; i < 5; i++){
			if(ranking[i])
				this.ranking.push(ranking[i])
		}
	}
	private getRecurrenceRanking(elements){
		let ranking:any[] = [];
		const ageRanges:string[] = this.getAgesRanges(elements);
		ageRanges.map(range => {
			ranking.push(
				{
					range: range, 
					genre: 'male', 
					percentage: this.calculateRecurrencePercentage(range, 'male')
				}, 
				{
					range: range, 
					genre: 'female', 
					percentage: this.calculateRecurrencePercentage(range, 'female') 
				});
		})
		ranking.sort((a,b) => a.percentage > b.percentage ? -1 : 1)
		this.recurrenceRanking = [];
		for(let i = 0; i < 5; i++){
			if(ranking[i])
				this.recurrenceRanking.push(ranking[i])
		}
	}
	private calculatePercentage(range, genre){
		const total = this.allAfluences.length;
		let count = this.allAfluences.filter(afluence => this.filterAgeRange(afluence, range) && afluence.gender === genre).length;
		return (count*100) / total;
	}
	private calculateRecurrencePercentage(range,genre){
		const total = this.allRecurrencers.length;
		let count = this.allRecurrencers.filter(r => this.filterAgeRange(r,range) && r.gender === genre).length;
		return (count*100) / total;
	}
}
