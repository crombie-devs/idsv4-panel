import { FlowFormComponent } from './form/form.component';
import { FlowResultLadorianComponent } from './result-ladorian/result.component';
import { FlowResultBeethedataComponent } from './result-beethedata/result.component';

export const FLOW_COMPONENTS = [
    FlowFormComponent,
    FlowResultLadorianComponent,
    FlowResultBeethedataComponent
];
