import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { MatGridListModule } from '@angular/material/grid-list';

import { UserStore } from 'src/modules/user/stores/user.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';

@Component({
  selector: 'flow-page',
  templateUrl: './view.page.html',
  styleUrls: ['./styles.page.scss']
})

export class FlowPage {
	
	public me: any;
	public store: string = null;
	public submitted: boolean = false;
	public data: any;
	public cameras: any[] = [];

  	constructor(
		private router: Router,
		private ws: WSService,
		private debug: DebugStore,	
		private dialog: DialogService,
    	private astore: AuthStore,
		private localStorage: MYLocalStorage,
		private ustore: UserStore) {
			
  	}
  
  	public ngOnInit() {
		this.me = this.ustore.getUser();
		this.debug.addInfo('FlowPage', 'OnInit');
	}

	public submitForm(data: any){
		this.data = data;
		this.submitted = true;
	}
	backToForm(){
		this.data = [];
		this.submitted = false;
	}
}
