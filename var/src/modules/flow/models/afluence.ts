export interface IAfluence {
    datetime: string,
    date: string,
    time: string,
    year: number,
    month: number,
    day: number,
    hour: number,
    weekday: number,
    gender: string,
    type: string,
    age: number,
    duration: number
}

export class Afluence implements IAfluence {
    datetime: string;
    date: string;
    time: string;
    year: number;
    month: number;
    day: number;
    hour: number;
    weekday: number;
    gender: string;
    type: string;
    age: number;
    duration: number;

    constructor(obj?: IAfluence){
        if(!obj)
            return;

        this.datetime = obj.datetime.replace('.000Z', '');
        this.date = obj.date;
        this.time = obj.date;
        this.year = obj.year;
        this.month = obj.month;
        this.day = obj.day;
        this.hour = obj.hour;
        this.weekday = obj.weekday;
        this.gender = obj.gender;
        this.type = obj.type;
        this.age = obj.age;
        this.duration = obj.duration;
    }
}