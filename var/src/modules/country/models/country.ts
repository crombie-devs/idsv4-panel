export interface ICountry {
    idcountry: number;
    name: string;
}
  
export class Country implements ICountry {
  
  idcountry: number;
    name: string;
  
    constructor(obj?: ICountry) {
      if(!obj)
        return;
  
      this.idcountry = obj.idcountry;
      this.name = obj.name;   
  
    }
  
}