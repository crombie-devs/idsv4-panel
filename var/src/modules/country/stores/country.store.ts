import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { environment } from "src/environments/environment";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { ICountry, Country } from '../models/country';

const STORE_KEY: string = 'countries.store.ids';

@Injectable()
export class CountryStore {

    public onChange: BehaviorSubject<Country[]> = new BehaviorSubject([]);
    private data: Country[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = localStorage.getItem(STORE_KEY);
            if(store) {
                const formats: ICountry[] = JSON.parse(store); 
                this.set(formats);
            } else 
                this.load();
        }
        else    
            this.load();
    }

    public get(): Country[] {
        return this.data;
    }

    public set(formats: ICountry[]) {
        this.save(formats);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.countriesToSelect().subscribe((result: IResult) => {
            if(result.success)
              this.set(result.data);
        });
    }

    private save(formats: ICountry[]) {
        this.data = formats.map(s => new Country(s));
        if(environment.localstorage)
            localStorage.setItem(STORE_KEY, JSON.stringify(formats));
    }
}