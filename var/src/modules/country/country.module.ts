import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { COUNTRY_COMPONENTS } from './components/components';
import { MaterialModule } from 'src/app/material.module';
import { CountryStore } from './stores/country.store';
@NgModule({
  declarations: [COUNTRY_COMPONENTS],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  providers: [CountryStore],
  exports: [COUNTRY_COMPONENTS]
})
export class CountryModule {}