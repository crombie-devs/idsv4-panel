import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Router } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { IResult } from 'src/models/iresult';
import { DebugStore } from 'src/stores/debug.store';
import { FormControl, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'country-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})

export class SelectCountryComponent implements OnInit {

	@Output() countriesSelected = new EventEmitter<number>();
	@Input() dataCountries:any;

	public countries:any[] = [];
	public paises = new FormControl();
	public filterForm:FormGroup;

	constructor(
		private dialog: DialogService,
		private router: Router,
		private ws: WSService,
		private debug: DebugStore,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private formBuilder:FormBuilder,
	) { }


	ngOnInit(): void {
		this.getCountries();
	}
	
	bindData(){
		if(this.dataCountries){
			const anotherList:any[]= this.dataCountries['countries'];
			this.paises.setValue(anotherList);
		}
	}
	
	private getCountries(){
		this.ws.countriesToSelect().subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.countries', 'successful');				
				this.countries = result.data;
				this.bindData()			
			} else {            
				this.debug.addError('WS.countries', result.data);
				this.openSnackBar(result.data);
			}
		}, error => {
			this.debug.addError('WS.countries', error.message);
		}); 
	}
	
	public captureValue() {
		let countries = this.paises.value;
		this.countriesSelected.emit(countries);		
	}
		
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
			duration: 1500,
		});
	}
}
