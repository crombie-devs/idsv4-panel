import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatIconModule } from "@angular/material/icon";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTooltipModule } from "@angular/material/tooltip";
import { SupportContactRoutingModule } from "./support-contact.routing.module";
import { SupportContactFormPage } from "./pages/form/form.page";
import { TicketModule } from "../ticket/ticket.module";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { MatButtonModule } from "@angular/material/button";

@NgModule({
  declarations: [
    SupportContactFormPage
     
  ],
    
  imports: [
    CommonModule,
    MatDialogModule,
    MatCheckboxModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    MatToolbarModule,
    MatTooltipModule,
    MatFormFieldModule,
    SupportContactRoutingModule,
    TicketModule,
    CKEditorModule,
    
MatButtonModule
   
],
  exports: [
  
  ],
  providers: [],
  entryComponents: []
 
})
export class SupportContactModule {}
