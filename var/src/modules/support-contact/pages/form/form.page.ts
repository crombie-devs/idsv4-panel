import { AfterViewInit, Component, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent, CKEditorComponent } from '@ckeditor/ckeditor5-angular';
import { TicketService } from '../../../ticket/services/ticket.service';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
    selector: 'support-contact-form',
    templateUrl: './form.page.html',
    styleUrls: ['./form.page.scss']
})

export class SupportContactFormPage implements AfterViewInit {
    public form: FormGroup;
    public id:  number;
    public categories: any[] = [];
    public priorities: any[] = [];
    public status: any[] = [];
    public sHtml: string;
    public users: any[] = [];
    @ViewChild('v') v: ElementRef<HTMLVideoElement>;
    
    aperance: string = 'outline';
    userSelect: number;
    usersToSelect: boolean = true;
    fecha_alerta = [new Date()];
    files: File[]=[]; 

    id_cat: number;
    id_sta: number;
    id_pri: number;
    id_site: number;
    iddepartment: number;
    idresponsible: number;

	public Editor = ClassicEditor;
	@ViewChild(CKEditorComponent) ckeditor : CKEditorComponent;


    constructor(private _ticket: TicketService, private _snackBar: MatSnackBar) { 
        this.form = new FormGroup({
            titulo: new FormControl('', [Validators.required]),
            descripcion: new FormControl('', [Validators.required]),
            files: new FormControl([]),
            phone: new FormControl(''),
            point_address: new FormControl(),
            email: new FormControl('', [Validators.required, Validators.email]),
            accept: new FormControl(false, [])
        });
   }
 
   
	ngAfterViewInit(): void {
		this.v.nativeElement.muted = true;
		this.v.nativeElement.play();
	}

     public onChange( { editor }: ChangeEvent ) {
        const data = editor.getData();
        this.form.patchValue({descripcion : data});      
    }

    getFiles(events): void {
        this.files = events;
       

     }


    
    create(): void {
        if(this.form.valid) {
            if(this.files.length > 0) {
                this.form.value.files = this.files;
            }
            this._ticket.createContactSupport(this.form.value).subscribe( item => {
                this.form.reset();
                this.files = [];
                this.sHtml = '';
                 this.openSnackBar("Formulario enviado con exito, en breve recibira un email de confirmacion");
            });
            
        }
     }

     public openSnackBar(message: string) {
        this._snackBar.open(message,'', {
          duration: 1500,
          panelClass: ['green-snackbar']
        });
      }

}
