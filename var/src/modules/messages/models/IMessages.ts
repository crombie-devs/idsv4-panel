export interface IMessages {
    id: number;
    subject: string;
    body: { email: [], sms: [], whatsapp: [] } | string;
    datetime: Date;
    idsite: number;
    idcustomer: number;
    idformat: number;
    num_sends: number;
    num_received: number;
    num_opened: number;
    status: number;
    is_sync: number;
    created_at: Date;
    updated_at: Date;
    tags: [],
    typeMessage: [],
    formats: []
}