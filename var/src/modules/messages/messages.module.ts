import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatIconModule } from "@angular/material/icon";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTooltipModule } from "@angular/material/tooltip";
import { ColorPickerModule } from 'ngx-color-picker';
import { UserModule } from "../user/user.module";
import { SharedModule } from "../../shared/shared.module";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { DirectivesModule } from "src/directive/directives.module";
import { AssetModule } from "../asset/asset.module";
import { SiteModule } from "../site/site.module";
import { MatButtonModule } from "@angular/material/button";
import { PipesModule } from "src/pipes/pipes.module";
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MessagesRoutingModule } from "./messages-routing.module";
import { MessageFormPage } from "./pages/form/form.page";
import { MessagesListPage } from "./pages/list/list.page";
import { FormFieldModule } from "../form-field/form-field.module";
import { TagModule } from "../tag/tag.module";
import { CustomerModule } from "../customer/customer.module";
import { FormatTemplateDialog } from './dialogs/form-template/format-template.dialog';
import { ListStatsPage } from "./pages/stats/list-stats.page";


@NgModule({
    declarations: [
        MessageFormPage,
        MessagesListPage,
        FormatTemplateDialog,
        ListStatsPage
    ],

    imports: [
        CommonModule,
        MatDialogModule,
        MatCheckboxModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatIconModule,
        MatToolbarModule,
        MatTooltipModule,
        SharedModule,
        ColorPickerModule,
        UserModule,
        NgxMatFileInputModule,
        MatDatepickerModule,
        MatFormFieldModule,
        DirectivesModule,
        AssetModule,
        SiteModule,
        MatButtonModule,
        PipesModule,
        MatProgressSpinnerModule,
        MessagesRoutingModule,
        SiteModule,
        FormFieldModule,
        TagModule,
        CustomerModule,
        PipesModule
    ],
    exports: [
        FormatTemplateDialog
    ],
    providers: [],
    entryComponents: []

})
export class MessagesModule { }
