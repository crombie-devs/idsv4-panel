import { Component, Input, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatInput } from '@angular/material/input';
import { MessagesService } from '../../services/messages.service';
import { SelectionModel } from '@angular/cdk/collections';
import { UserStore } from 'src/modules/user/stores/user.store';
import { User } from 'src/modules/user/models/user';
import moment, { Moment } from 'moment';


@Component({
    selector: 'list-stats',
    templateUrl: './list-stats.page.html',
    styleUrls: ['./list-stats.page.scss']
})

export class ListStatsPage {

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChildren('table') table: QueryList<MatCheckbox>
    @ViewChild(MatInput) input: MatInput;

    displayedColumns: string[] = ['idcontent', 'name', 'datetime', 'hours', 'format', 'lang', 'num_sends', 'num_received', 'num_opened'];
    new: boolean = false;
    edit: boolean = false;
    deleted: boolean = false;
    permission: boolean = false;
    load: boolean = false;
    dataSource: any;
    dataSourceTemp: any[] = [];
    me: User;
    init: Moment;
    end: Moment;
    public selection = new SelectionModel<any>(true, []);


    data: any[] = [];

    constructor(private _message: MessagesService, private ustore: UserStore,) {

        this.ustore.onChange.subscribe(user => {
            this.me = user;
            this._message.messageByCustomer(this.me.customer.idcustomer).subscribe(contents => {
                let arr = [];
                contents.forEach(content => {
                    content.message.forEach(message => {
                        arr.push({
                            idcontent: content.idcontent,
                            name: content.name,
                            datetime: message.datetime,
                            hours: message.datetime,
                            format: message.formats[0].name,
                            lang: message.lang.name,
                            num_sends: message.num_sends,
                            num_received: message.num_received,
                            num_opened: message.num_opened
                        })
                    });
                });
                this.data = [...arr];
                this.dataSourceTemp = [...arr];
                this.renderDataTable(this.data)
            });
        });
    }

    public ngOnInit() { }

    public applyFilter(event: string) {
        this.selection.clear();
        if (!event) {
            this.renderDataTable(this.dataSourceTemp);
        } else {
            if (typeof event == 'string') {
                this.dataSource.data = this.dataSourceTemp.filter((message: any) =>
                    message.subject.toLowerCase().includes(event.trim().toLowerCase()) ||
                    message.id.toString().includes(event.trim().toLowerCase())
                );
            }
        }
    }

    public selectDate() {
        if (this.init && this.end) {
            let filter = [];
            this.dataSourceTemp.forEach(message => {
                if (moment(message.datetime).isBetween(this.init, this.end)) {
                    filter.push(message);
                }
            });
            this.renderDataTable(filter);
        } else {
            alert("Tienes que seleccionar fecha de inicio y fecha de fin");
        }
    }








    public renderDataTable(data: any[]) {
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = data.reverse();
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.load = false;
    }







}
