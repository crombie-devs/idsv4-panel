import { Component, Input, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatInput } from '@angular/material/input';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IMessages } from '../../models/IMessages';
import { MessagesService } from '../../services/messages.service';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
    selector: 'messages-list',
    templateUrl: './list.page.html',
    styleUrls: ['./list.page.scss']
})

export class MessagesListPage {

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChildren('table') table: QueryList<MatCheckbox>
    @ViewChild(MatInput) input: MatInput;

    displayedColumns: string[] = ['select', 'id', 'subject', 'datetime', 'formats','status', 'num_sends', 'num_received', 'num_opened'];
    new: boolean = false;
    edit: boolean = false;
    deleted: boolean = false;
    permission: boolean = false;
    load: boolean = false;
    dataSource: any;
    dataSourceTemp: any[] = [];
    public selection = new SelectionModel<IMessages>(true, []);


    data:  IMessages  [];

    constructor(private router: Router,
        private dialog: DialogService,
        private translate: TranslateService, private _snackBar: MatSnackBar, private _message: MessagesService) {

        this._message.toSelect().subscribe(messages => {
            this.dataSourceTemp = [...messages]; 
        
            this.renderDataTable(messages);
        });
    }

    public ngOnInit() { }

    public applyFilter(event: string) {
        this.selection.clear();
        if (!event) { 
            this.renderDataTable(this.dataSourceTemp);
          } else {
            if (typeof event == 'string') {
                this.dataSource.data = this.dataSourceTemp.filter((message: any) =>
                    message.subject.toLowerCase().includes(event.trim().toLowerCase()) ||
                    message.id.toString().includes(event.trim().toLowerCase())
                );
            }
        }
    }


    public update() {
        if (this.selection.selected.length > 1) {
            this.openSnackBar("para editar solo puedes seleccioar un mensaje");
            return;
        }
        if(this.selection.selected[0].status != 0){
            this.openSnackBar("Solo se pueden editar mensajes en estado pendiente");
            return;
        }
        this.router.navigate(['/private/mensaje/add', this.selection.selected[0].id])
    }


    checkTable(row, value, index) {

        if (row.hasOwnProperty([value])) {
            if (row[value] != null) {
                return true;
            }
        }
        return false;

    }

    public delete() {
        let contentsByDelete = [];
        this.table.toArray().forEach(item => {
            if (item.checked)
                contentsByDelete.push(item.value);
        });

        if (contentsByDelete.length == 0)
            this.dialog.warning(this.translate.instant('Aviso'), this.translate.instant('Debe seleccionar al menos un contenido')).subscribe();
        else
            this.dialog.confirm(this.translate.instant('Confirmar Archivado'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
                if (confirm) {
                    this._message.deleteArray(contentsByDelete).subscribe(result => {
                        if (result) {
                            this.dataSourceTemp = this.dataSourceTemp.filter(ds => !contentsByDelete.includes(ds.id));
                            this.dataSource.data = this.dataSource.data.filter(ds => !contentsByDelete.includes(ds.id));
                            this.renderDataTable(this.dataSource.data);
                            this.dialog.warning('Aviso', 'Los contenidos han sido eliminados').subscribe();
                        }
                    })
                }
            });
    }

    public renderDataTable(data:  IMessages  []) {
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = data.reverse();
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.load = false;
    }

    isAllSelected() {
        if (this.dataSource.paginator) {
            const numSelected = this.selection.selected.length;
            const page = this.dataSource.paginator.pageSize;
            let endIndex: number;
            if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
                endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
            } else {
                endIndex = this.dataSource.data.length - (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize);
            }
            return numSelected === endIndex;
        }
    }

    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() : this.selectRows();
    }

    selectRows() {
        let endIndex: number;
        if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
            endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
        } else {
            endIndex = this.dataSource.data.length;
        }
        for (let index = (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize); index < endIndex; index++) {
            this.selection.select(this.dataSource.data[index]);
        }
    }


    public checkboxLabel(row?:  IMessages  ): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
            panelClass: ['orange-snackbar']
        });
    }
}
