import { Component, OnInit, QueryList, Renderer2, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import moment from 'moment';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent, CKEditorComponent } from '@ckeditor/ckeditor5-angular';
import { UserStore } from '../../../user/stores/user.store';
import { MessagesService } from '../../services/messages.service';
import { IMessages } from '../../models/IMessages';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { User } from 'src/modules/user/models/user';
import { TypeMessageComponent } from 'src/modules/customer/components/type-message/type-message.component';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { TAGTYPE } from 'src/modules/tag/models/tagcategory';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { FormatTemplateDialog } from '../../dialogs/form-template/format-template.dialog';
import { MatListItem } from '@angular/material/list';



@Component({
    selector: 'message-form',
    templateUrl: './form.page.html',
    styleUrls: ['./form.page.scss']

})

export class MessageFormPage implements OnInit {
    public form: FormGroup;
    loading: boolean;
    id: number;
    today = new Date();
    sHtml: any;
    types: any[] = [];
    pass: boolean;
    site: number = 0;
    idsite: number;
    idcustomer: number;
    tags: any = {
        email: [],
        sms: [],
        whatsapp: []
    };
    email: boolean;
    sms: boolean;
    whatsapp: boolean;
    format: number;
    body: any;
    templates: any[] = [];

    public em: User;
    public typeSelected: Set<any> = new Set();
    public Editor = ClassicEditor;

    @ViewChild(CKEditorComponent) ckeditor: CKEditorComponent;
    @ViewChild(TypeMessageComponent) typeMessage: TypeMessageComponent;
    @ViewChildren('template') template: QueryList<MatListItem>;


    constructor(private route: ActivatedRoute, private _message: MessagesService,
        private dialog: DialogService, private _snackBar: MatSnackBar,
        private user: UserStore, private router: Router, private matDialog: MatDialog, private renderer: Renderer2) {
        this.em = this.user.getUser();

        this._message.getTemplate().subscribe(templates => {
            this.templates = templates;

        });

        if (this.user.getRoles().some(r => r.name == 'user')) {
            this.site = this.user.getUser().site.idsite;

        }

        if (this.user.getRoles().some(r => r.name != 'superadmin')) {
            this.idcustomer = this.user.getUser().customer.idcustomer;
        }



        this.form = new FormGroup({
            id: new FormControl(),
            subject: new FormControl('', Validators.required),
            body: new FormControl({ email: [], sms: [], whatsapp: [] }),
            nameTemplate: new FormControl(''),
            datetime: new FormControl(new Date(), Validators.required),
            idcustomer: new FormControl(this.idcustomer),
            format: new FormControl(),
            tags: new FormControl({ email: [], sms: [], whatsapp: [] }),
            hours: new FormControl(0),
        });
    }




    public ngOnInit(): void {

        if (this.em.hasOwnProperty('config_customer')) {
            const types = this.em.config_customer;
            const keys = Object.keys(types);

            keys.forEach(key => {
                if (key != 'id' && key != 'idcustomer') {
                    if (types[key] != null) {
                        this.types.push(types[key]);
                    }
                }
            });
            if (this.types.length > 0) {
                this.pass = true;
            }
        }

        this.route.params.subscribe(param => {
            if (param.id) {
                this._message.get(param.id).subscribe((message: any) => {
                    if (message) {
                        this.format = message.idformat;
                        this.initForm(message);
                    }
                });
            }
        });

    }


    public setTemplate(index): void {
        const elem = this.template.toArray()[index];
        this.template.toArray().forEach(el => {
            this.renderer.setStyle(el._getHostElement(), 'background-color', 'white')
        });
        this.renderer.setStyle(elem._getHostElement(), 'background-color', 'gray');
        this.form.value.body['whatsapp'] = this.templates[index].name_template;
    }


    public removeTag(tag: any, format): void {
        this.tags[format] = this.tags[format].filter(t => t.idtag != tag.idtag);
        if (this.id) {

            this.form.value.tags = [{ format: format, tags: [... this.tags[format]] }];
        } else {

            this.form.value.tags[format] = [...this.tags[format]];
        }
    }

    public dateChange(event: MatDatepickerInputEvent<Date>) {
        const holiday = moment(event.value).format('YYYY-MM-DD')
    }


    public onChange({ editor }: ChangeEvent) {
        const data = editor.getData();
        if (this.id) {
            this.form.value.body = data;
        } else {
            this.form.value.body.email = data;
        }

    }

    public setBody(event: any, format: string): void {
        this.form.value.body[format] = event.target.value;
    }

    changeType(events): void {
        this.types = [...events];
        this.email = false;
        this.sms = false;
        this.whatsapp = false;
        this.types.forEach(item => {
            let v = item.name.toLowerCase();
            this[v] = true;
        });
    }

    changeSite(event): void {
        this.idsite = event.idsite;
    }

    getTags(tags): void {
        this.form.value.tags = JSON.stringify(tags);
    }


    extracHours(datetime): string {
        const hours = moment(datetime).format('HH:mm');
        return hours;
    }


    convertToFormat(id: number): string {
        const formats = ['email', 'sms', 'whatsapp'];
        return formats[id];
    }



    initForm(data: IMessages) {
        this.id = data.id;
        this.body = data.body;
        this.form.setValue({
            id: data.id,
            subject: data.subject,
            body: data.body,
            tags: data.tags,
            //idsite: data.idsite,
            idcustomer: data.idcustomer,
            datetime: data.datetime,
            hours: this.extracHours(data.datetime),
            format: data.idformat,
            nameTemplate: 'dsdsd'
        });

        let tags = [];
        data.tags.forEach((t: any) => {
            tags.push({ idtag: t.idtag, name: t.name, categoryName: t.tag_category.name });
        });


        this.tags[this.convertToFormat(data.idformat - 1)] = tags;
        setTimeout(() => {
            this.sHtml = data.body;
        }, 200);
    }

    public openDialogTemplate() {
        const dialogRef = this.matDialog.open(FormatTemplateDialog, {

        });

        dialogRef.afterClosed().subscribe((result: any) => {
            this.templates.unshift(result);
        });
    }

    public removeTemplate(id: number): void {
        this._message.deleteTemplates(id).subscribe(response => {
            if (response) {
                this.templates = this.templates.filter(t => t.idtemplate != id);
            }
        });
    }


    public openDialogTags(format: string) {
        let values = this.form.value.tags[format]
        if (this.id) {
            values = this.form.value.tags;
        }
        this.dialog.tagcategoryselect('Tags', values, this.form.get('idcustomer').value, TAGTYPE.difusion)
            .afterClosed().subscribe((result) => {
                if (!result.cancel) {
                    this.tags[format] = result.tags;
                    if (!this.id) {
                        this.form.value.tags[format].push({ format: format, tags: [...this.tags[format]] });
                    } else {
                        this.form.value.tags.push({ format: format, tags: [...this.tags[format]] });
                    }
                }
            });
    }

    create(): void {


        if (this.form.valid) {

            const keys = Object.keys(this.form.value.body);
            let pass = false;
            keys.forEach(key => {

                if (this.form.value.body[key].length > 0) {
                    pass = true;
                }
            });


            if (!pass) {
                this.openSnackBar("Tiene que tener un body");
                return;

            }


            const arr = Object.values(this.form.value.tags);
            const matconv = {
                email: 1,
                sms: 2,
                whatsapp: 3
            }

            let request = [];
            arr.forEach((format: any) => {
                if (format.length > 0) {
                    request.push({
                        body: this.form.value.body[format[0].format],
                        format: matconv[format[0].format],
                        tags: format[0].tags
                    });
                }
            });

            const date = moment(this.form.value.datetime).format("DD/MM/YYYY");
            const datetime = moment(date + ' ' + this.form.value.hours, 'DD/MM/YYYY HH:mm:ss');

            this.form.value.datetime = moment(datetime).format('MM/DD/YYYY HH:mm:ss');

            //   this.form.value.idsite = this.idsite;
            this.form.value.messages = JSON.stringify(request);

            this._message.save(this.form.value).subscribe(item => {
                if (item) {
                    let route = '../list';
                    if (this.id) {
                        route = '../' + route;
                    }
                    this.router.navigate([route], { relativeTo: this.route });
                }
            });
        } else {
            this.openSnackBar("Revisa el formulario, faltan campos");
            return;
        }
    }


    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
            panelClass: ['orange-snackbar']
        });
    }
}
