import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MaxSizeValidator } from '@angular-material-components/file-input';
import { Format } from 'src/modules/format/models/format';
import { WSService } from 'src/services/ws.service';
import { IResult } from 'src/models/iresult';
import { DebugStore } from 'src/stores/debug.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MessagesService } from '../../services/messages.service';

@Component({
    selector: 'format-template-dialog',
    templateUrl: './format-template.dialog.html',
    styleUrls: ['./format-template.dialog.scss']
})

export class FormatTemplateDialog {

    public title: string;
    public message: string;
    public button: string;
    public form: FormGroup;
    public id: number | null;
    public maxSize = 16;
    public files;
    public frame_files;
    public loading: boolean = false;
    public templates: any[] = [];

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: Format,
        public dialogRef: MatDialogRef<FormatTemplateDialog>,
        private _message: MessagesService,

        private debug: DebugStore,
        private _snackBar: MatSnackBar
    ) {



        this.form = new FormGroup({
            nameTemplate: new FormControl('', Validators.required)
        })
        if (this.data)
            this.id = Number(this.data.idformat);




    }

    public ngOnInit() {
        if (this.id) {
            this.initForm(this.data);
        }
    }

    private initForm(data) {
        this.form.setValue({
            templateName: data.nameTemplate,
            id: data.id
        });
    }


    public save() {
        if (this.form.invalid)
            return null;
        this.loading = true;
        this._message.saveTemplate(this.form.value).subscribe((result: any) => {

            if (result.id) {

                this.debug.addInfo('WS.createFormat', 'successful');
                this.dialogRef.close(result);
                this.loading = false;
            } else {
                this.dialogRef.close(false);
            }

        }, error => {
            let msj = JSON.stringify(error.error.data);
            this.debug.addError('WS.createFormat', error.message);
            this.openSnackBar(msj.substr(2, msj.length - 3));
            this.loading = false;
        });
    }



    public close() {
        this.dialogRef.close(false);
    }



    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }

}
