import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { HttpService } from '../../../services/http.service';
import { ICrudWS } from '../../../interfaces/ICrudWS.interface';
import { IMessages } from '../models/IMessages';


@Injectable({
    providedIn: 'root',
})
export class MessagesService implements ICrudWS<IMessages> {

    idsite: Subject<number> = new Subject();
    $idsite = this.idsite.asObservable();

    constructor(private http: HttpService) { }

    public save(obj: any): Observable<IMessages> {
        return this.create(obj);
    }

    public saveTemplate(obj: any): Observable<any> {

        if (obj.hasOwnProperty('id') && obj.id) {

            return this.updateTemplate(obj);
        } else {

            return this.createTemplate(obj);
        }
    }





    public delete(id): Observable<IMessages> {
        return this.http.delete('private/messages', [id], 'WS.message');
    }


    public create(obj: IMessages): Observable<IMessages> {
        return this.http.create('private/messages', obj, 'WS.message_add');
    }

    public update(obj: IMessages): Observable<IMessages> {

        return this.http.update('private/messages', obj, 'WS.message_update');
    }

    public deleteArray(ids: number[]): Observable<IMessages> {
        return this.http.update('private/messages/delete', { ids: ids }, 'WS.message_delete');
    }

    public get(id: number): Observable<IMessages> {
        return this.http.get('private/messages', [id], 'WS.message');
    }

    public toSelect(args?: any[]): Observable<IMessages[]> {
        return this.http.get('private/messages', args, 'WS.message');
    }

    public list(args?: any[]): Observable<IMessages[]> {
        return this.http.get('private/messages', args, 'WS.message');
    }

    public listBytype(args?: any[]): Observable<IMessages[]> {
        return this.http.get('private/messages/type', args, 'WS.message');
    }


    public createTemplate(obj: any): Observable<any> {
        return this.http.createWithFiles('private/messages/templates', obj, 'WS.message_add_template');
    }

    public updateTemplate(obj: any): Observable<any> {

        return this.http.update('private/messages/templates', obj, 'WS.message_update_template');
    }

    public getTemplate(): Observable<any> {
        return this.http.get('private/messages/templates/list', [], 'WS.message_template_list');
    }

    public deleteTemplates(id): Observable<any> {
        return this.http.delete('private/messages/templates/delete', [id], 'WS.message_template_delete');
    }

    public deleteMsg(id): Observable<IMessages> {
        return this.http.delete('private/messages/msg', [id], 'WS.message');
    }

    public messageByCustomer(id): Observable<any> {
        return this.http.get('private/messages/bycustomer', [id], 'WS.message');
    }










}
