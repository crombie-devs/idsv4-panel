import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessageFormPage } from './pages/form/form.page';
import { MessagesListPage } from './pages/list/list.page';
import { ListStatsPage } from './pages/stats/list-stats.page';




const routes: Routes = [
    { path: 'mensaje/list', component: MessagesListPage, data: { breadcrumb: 'Listado de mensajes' } },
    { path: 'mensaje/add', component: MessageFormPage, data: { breadcrumb: 'Listado de mensajes' } },
    { path: 'mensaje/add/:id', component: MessageFormPage, data: { breadcrumb: 'Listado de mensajes' } },
    { path: 'mensaje/stats', component: ListStatsPage, data: { breadcrumb: 'Estadisticas de mensajes' } },





]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class MessagesRoutingModule { }
