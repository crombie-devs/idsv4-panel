import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Asset } from 'src/modules/asset/models/asset';
import { IResult } from 'src/models/iresult';
import { Landingpage } from 'src/modules/landing-page/models/landingpage';
import { User } from 'src/modules/user/models/user';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { CacheService, ICacheConfig } from 'src/services/cache.service';
import { EventService } from 'src/shared/event.service';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { Format } from 'src/modules/format/models/format';
import { Lang } from 'src/modules/lang/models/lang';

@Component({
  providers: [FormatStore, LangStore],
	selector: 'landingpage-dialog',
  	templateUrl: './landingpage.dialog.html',
  	styleUrls: ['./landingpage.dialog.css']
})

export class LandingpageDialog {

  public title: string;
  public message: string;
  public landingpage: Landingpage = null;
  public asset: Asset = null;  
  public url: SafeResourceUrl; 
  public formParam: FormGroup = new FormGroup({});
  public form: FormGroup = new FormGroup({});
  private me: User = null;
  public  name: FormControl = new FormControl(null, [Validators.required]);
  public idformat: FormControl = new FormControl(null,[Validators.required]);
  public idlang: FormControl = new FormControl(null, [Validators.required]);
  public idtype: FormControl = new FormControl(3, [Validators.required]);
  public asset_url: FormControl = new FormControl(null, [Validators.required]);
  public duration: FormControl = new FormControl(20, [Validators.required]);
  public idlanding: FormControl = new FormControl(null, [Validators.required]);
  public delay:FormControl = new FormControl(2, [Validators.required, Validators.min(0), Validators.max(30)])

  public formats: Format[] = [];
  public langs: Lang[] = [];

  public is_multiple = false;

  public results: Asset[] = [];

	constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
	  public dialogRef: MatDialogRef<LandingpageDialog>,
    private sanitizer: DomSanitizer,
    private ws: WSService,
    private debug: DebugStore,
    private ustore: UserStore,
    private dialog: DialogService,
    private events: EventService,
		private eventc: CacheService,
    private fstore: FormatStore,
    private lstore: LangStore) {
      this.ustore.onChange.subscribe(user => { 
        this.me = user; 
      });
      this.fstore.onChange.subscribe(formats => { 
        if(this.data.formats){
            formats.map(f => {
                if(this.data.formats.some(fc => fc === f.idformat))
                    this.formats.push(f);
            }) 
        }else
            this.formats = formats; 
      });
      this.lstore.onChange.subscribe(langs => { 
        if(this.data.langs){
            langs.map(l => {
                if(this.data.langs.some(lc => lc === l.idlang))
                    this.langs.push(l);
            }) 
        }else{
            this.langs = langs;
        }
      });
    }

  public ngOnInit() { 
    this.landingpage = this.data.landingpage;
    this.asset = this.data.asset; 
    if(this.asset)
      this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.asset.asset_url); 
    else
      this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.landingpage.base_url);  
    this.initForm(this.asset);
  }

  private initForm(asset:Asset|null) {
    this.duration.patchValue(this.landingpage.duration);
    if(!asset){
      this.is_multiple = true;
      this.landingpage.config.map(c => {
        this.formParam.addControl(c.key, new FormControl(c.default));
      });
    }
    else{
      let params = this.asset.asset_url.split('?')[1].split('&');
      let arr: any[] = [];
      for (let i=0; i<params.length; i++){
        let key = params[i].split("=")[0];
        let value = params[i].split("=")[1];
        if(key.includes("[]")){
          key = key.substr(0, key.length -2 );
          arr.push(value);
        }else{
          arr = [];
        }
        if(arr.length>0)
          if(this.formParam.contains(key))
            this.formParam.get(key).patchValue(arr);
          else
            this.formParam.addControl(key, new FormControl(arr));
        else 
          this.formParam.addControl(key, new FormControl(value));
      }      
      this.name.patchValue(this.asset.name);
      this.idlang.patchValue(this.asset.lang.idlang);
      this.idformat.patchValue(this.asset.format.idformat);
    }
    this.form.addControl("name", this.name);
    this.form.addControl("idcustomer", new FormControl((this.me.customer)? this.me.customer.idcustomer : ''));
    this.form.addControl("idsite", new FormControl(''));
    this.form.addControl("idformat", this.idformat);
    this.form.addControl("idlang", this.idlang); 
    this.form.addControl("idtype", this.idtype); 
    this.form.addControl("asset_url", this.asset_url); 
    this.form.addControl("duration", this.duration);
    this.form.addControl("idlandingpage", this.idlanding); 
    this.form.addControl("delay", this.delay); 
   
  }

  public preview(){
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.setUrl().substr(0,this.setUrl().length -1));
  }

  private setUrl(){
    let url = this.landingpage.base_url+"?";
    for (let control in this.formParam.getRawValue()){
      if (Array.isArray(this.formParam.getRawValue()[control]))
        for (let v in this.formParam.getRawValue()[control]){
          url +=control+"[]="+this.formParam.getRawValue()[control][v]+"&";
        }
      else     
        url +=control+"="+this.formParam.getRawValue()[control]+"&";  
    };

    return url;
    
  }
  
  public save() {
    const formats = this.form.get('idformat').value;
    const langs = this.form.get('idlang').value;

    this.asset_url.patchValue(this.setUrl().substr(0,this.setUrl().length -1));
    this.idlanding.patchValue(this.landingpage.id);

    if (this.form.invalid) return null;
    if(!this.asset){
      let assets: Asset[] = [];
      let counter = 0;
      formats.map(format => {
        langs.map(lang => {
          this.ws.createAsset(this.getFormData(format, lang)).subscribe((result: IResult) => {
            if(result.success) {
              const asset: Asset = new Asset(result.data);
              if(this.me && this.me.customer){
                let config: ICacheConfig = {idcustomer: this.me.customer.idcustomer};
                if(this.me.site)
                  config = {idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer};              
                setTimeout(() => {
                  this.events.publish('asset:change', config);     
                }, 1000);
                assets.push(asset);
                counter++;
                if(counter === formats.length*langs.length){
                  this.dialogRef.close(assets);
                }
              }
            }else{
              this.dialog.warning("Error", "No se ha podido guardar el asset");
            }
          },
          error => {
              this.dialog.warning("Error", "No se ha podido guardar el asset");
          }); 
        }); 
      });
    }
    else{
      this.ws.updateAsset(this.asset.idasset, this.getFormData()).subscribe((result: IResult) => {
        if(result.success) {
          const asset: Asset = new Asset(result.data);
          if(this.me && this.me.customer){
            let config: ICacheConfig = {idcustomer: this.me.customer.idcustomer};
            if(this.me.site)
              config = {idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer};              
            setTimeout(() => {
              this.events.publish('asset:change', config);     
            }, 1000);
          }
          this.dialogRef.close(asset); 
        }else{
          this.dialog.warning("Error", "No se ha podido guardar el asset");
        }
      },
      error => {
        this.dialog.warning("Error", "No se ha podido guardar el asset");
      });
    }  
  }

  public close(){
    this.dialogRef.close(false);
  }

  private getFormData(format:string = null, lang: string = null): FormData{
    let formData = new FormData();
    for (const property in this.form.getRawValue()) { 
      if(property === "idformat" && format){
        formData.set(property,format);
      } 
      else if(property === "idlang" && lang){
        formData.set(property,lang);
      } 
      else  
        formData.set(property,this.form.get(property).value);
    } 
    return formData;
  }

}
