export interface ILandingpage {
  id: number;
  name: string;
  thumbnail_url: string; 
  base_url: string;
  config: any;
  duration: number;
}

export class Landingpage implements ILandingpage {
  id: number;
  name: string;
  thumbnail_url: string; 
  base_url: string;
  config: any;
  duration: number;

	constructor(obj?: Landingpage) {
		if(!obj)
      return;

    this.id = obj.id;
    this.name = obj.name;
    this.thumbnail_url = obj.thumbnail_url; 
    this.base_url = obj.base_url;
    this.duration = obj.duration;
    try{ 
      let json = JSON.parse(obj.config);
      this.config = json.map(c => new Config(c));
    }
    catch (error){
      this.config = [];
    }
   /*  if(obj.config)    
      this.config = obj.config.map(c => new Config(c)); */  
  }
}

export class Data{
  key: number;
  value: string;

  constructor(obj?: Data) {
		if(!obj)
      return;

    this.key = obj.key;
    this.value = obj.value;
  }


}

export class Config {
  key: string;
  name: string;
  type: string;
  data: Data[];
  placeholder: string;
  requiered:boolean;
  multiple: boolean;
  default: string;

  constructor(obj?: Config) {
		if(!obj)
      return;

    this.key = obj.key;
    this.name = obj.name;
    this.type = obj.type; 
    if(obj.data)    
      this.data = obj.data.map(d => new Data(d));
    this.placeholder = obj.placeholder;
    this.requiered = obj.requiered;
    this.multiple = obj.multiple;
    this.default = obj.default;
    
  }

}


