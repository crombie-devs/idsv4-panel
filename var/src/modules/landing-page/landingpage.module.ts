import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/shared/shared.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatPaginatorModule } from '@angular/material/paginator';
import { PlayerModule } from 'src/modules/player/player.module';
import { LandingpageDialog } from './dialogs/landingpage/landingpage.dialog';
@NgModule({
  declarations: [LandingpageDialog],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatPaginatorModule,
    MaterialModule,
    SharedModule,
    PlayerModule,
  ],
  entryComponents:[LandingpageDialog],
  providers: [{ provide: 'Window',  useValue: window }],
  exports: []
})

export class LandingPageModule {}
