import { Component, ViewChild, ElementRef } from '@angular/core';
import { APP, VERSION } from 'src/app/app.config';
import { DebugStore } from 'src/stores/debug.store';

@Component({
  selector: 'maintenance-page',
  templateUrl: './maintenance.page.html',
  styleUrls: ['./maintenance.page.scss']
})

export class MaintenancePage {

	@ViewChild('v') v: ElementRef<HTMLVideoElement>;
	
	public app = APP;
	public version = VERSION;

  	constructor(private debug: DebugStore) {
  	}
  
  	public ngOnInit() {
		this.debug.addInfo('MaintenancePage', 'OnInit');
	}
	ngAfterViewInit(): void {
		this.v.nativeElement.muted = true;
		this.v.nativeElement.play();
	}
}
