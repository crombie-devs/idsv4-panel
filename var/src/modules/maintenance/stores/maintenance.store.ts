import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

import { WSService } from 'src/services/ws.service';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'maintenance.store.ids';
@Injectable()
export class MaintenanceStore {

    public onChange: BehaviorSubject<number> = new BehaviorSubject(null);
    private data: number = 0;

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const maintenance: number = JSON.parse(store); 
                this.set(maintenance);
            } else 
                this.load();
        }else
            this.load();
    }

    public get(): number {
        return this.data;
    }


    public set(maintenance: number) {
        this.save(maintenance);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.testService().subscribe((result: boolean) => {
            if(result) {
              this.set(1);
            }
        },
		error => {
			console.log('error', error);
			this.set(0);
		});
    }

    private save(maintenance: number) {
        this.data = maintenance;
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(maintenance));
    }
}