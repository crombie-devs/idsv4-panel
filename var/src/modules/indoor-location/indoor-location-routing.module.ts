import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndoorLocationListPage } from './pages/list/list.page';

const routes: Routes = [
  { path: 'indoorlocations', component: IndoorLocationListPage, data: { breadcrumb: 'Ubicaciones' }}          

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class IndoorLocationRoutingModule { }
