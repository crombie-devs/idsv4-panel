import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from 'src/pipes/pipes.module';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexModule } from '@angular/flex-layout';

import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from 'src/shared/shared.module';
import { IndoorLocationRoutingModule } from './indoor-location-routing.module';
import { IndoorlocationDialog } from './dialogs/indoorlocation/indoorlocation.dialog'; 

import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { IndoorLocationListPage } from './pages/list/list.page';
import { LocationModule } from 'src/modules/location/location.module';

@NgModule({
  declarations: [IndoorlocationDialog, IndoorLocationListPage],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    IndoorLocationRoutingModule,
    PipesModule,
    LocationModule
  ],
  providers: [DialogService],
  entryComponents: [IndoorlocationDialog],
  exports:[]
})
export class IndoorLocationModule {}