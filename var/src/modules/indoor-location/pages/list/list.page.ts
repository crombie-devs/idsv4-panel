import { Component, OnInit } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { IResult } from 'src/models/iresult';
import { DebugStore } from 'src/stores/debug.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Location } from 'src/modules/location/models/location';
import { MatDialog } from '@angular/material/dialog';
import { IndoorlocationDialog } from 'src/modules/indoor-location/dialogs/indoorlocation/indoorlocation.dialog';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'indoorlocation.list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss']
})

export class IndoorLocationListPage implements OnInit {
  
	public newIndoorLocation: boolean = false;
	public editIndoorLocation: boolean = false;
	public deletedIndoorLocation: boolean = false;
	public customersToSelect: boolean = false;
	public permission: boolean = false;
	public load: boolean = false;
	public me: any;
	public indoorlocations: Location[] = [];	
	public indoorlocationFull: Location[] = [];
	public filterVal:string='';
	public customerId: number = null;
	
  constructor(
    private ws: WSService,
		private debug: DebugStore,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private dialog: DialogService,
		public matDialog: MatDialog,
		private translate: TranslateService
    ) {
		this.ustore.onChange.subscribe(user => { 
            this.me = user; 
        }); 
	}

  	ngOnInit(): void {
	  	this.verifyPermissions();	  
		this.me = this.ustore.getUser();
		this.getIndoorlocation();
  	} 
  
	private getIndoorlocation(){
    	this.ws.locations().subscribe((result: IResult) => {
			
  			if(result.success) {
  				this.debug.addInfo('WS.locations', 'successful');
				this.permission = true;
  				let results = result.data;
				this.indoorlocationFull = results;
				this.indoorlocations = results;
				this.load = false;
				let customer = !this.customerId?this.me.customer:this.customerId;
				this.customerSelected(customer);
  			}
  			else {
  				this.dialog.warning('COMPONENTS.indoor_location.list.warning.error.title', result.data);
  				return null;
  			}
  		}, error => {
  			this.debug.addError('Error en locations', error.message);
  			this.openSnackBar(error.message);
  			return null;          
  		});	
  	}

	private verifyPermissions() {
		this.newIndoorLocation = this.ustore.hasPermissionByName("post.indoorlocation");
		this.editIndoorLocation = this.ustore.hasPermissionByName("put.indoorlocation.*");
		this.deletedIndoorLocation = this.ustore.hasPermissionByName("delete.indoorlocation.*");
		this.customersToSelect = this.ustore.hasPermissionByName("get.customers.toselect");
	}
  
  	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

  	public customerSelected(idcustomer:number) {
		  
		this.customerId = idcustomer;
		this.indoorlocations = this.indoorlocationFull;
		if(idcustomer > 0){
			let filterArray = Object.values(this.indoorlocations).filter(val => val.idcustomer == idcustomer);				
			this.indoorlocations = filterArray;
		}else
			this.indoorlocations = this.indoorlocationFull	
  	}
  
  	public change(){  
		this.getIndoorlocation();
	} 

  	public openDialog(){
		let dialogRef;
		if(!this.me.customer){  
			if(this.customerId && this.customerId>0){
				dialogRef = this.matDialog.open(IndoorlocationDialog, {
					data: {"idcustomer": this.customerId }
				});
				dialogRef.afterClosed().subscribe(result => {
					this.openSnackBar(result);
					this.getIndoorlocation();
				});
			}else{
			this.dialog.warning('Aviso', 'Debe seleccionar un cliente').subscribe();
			}
		}else{
			dialogRef = this.matDialog.open(IndoorlocationDialog, {
				data: {}
			});
			dialogRef.afterClosed().subscribe(result => {
				this.openSnackBar(result);
				this.getIndoorlocation();
			});
		}
	}
}
