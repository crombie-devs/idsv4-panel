import { Component, Inject, resolveForwardRef, Type, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { AssetTypeStore } from 'src/modules/asset/stores/assettype.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { Location } from 'src/modules/location/models/location';
import { Customer } from 'src/modules/customer/models/customer';
import { DebugStore } from 'src/stores/debug.store';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    providers: [FormatStore, LangStore, AssetTypeStore],
	selector: 'indoorlocation-dialog',
  	templateUrl: './indoorlocation.dialog.html',
  	styleUrls: ['./indoorlocation.dialog.scss']
})

export class IndoorlocationDialog {    
    public id: number|null;
	public idcust: number|null;
	public me: any;
	public form: FormGroup;
	
	public name: FormControl = new FormControl('', [Validators.required]);    
    public idcustomer: FormControl = new FormControl('');

	public customers: Customer[];
	
	public loading: boolean = false;
	public indoorlocation: Location = new Location(null);

	constructor(
        @Inject(MAT_DIALOG_DATA) public data: Location,
        public dialogRef: MatDialogRef<IndoorlocationDialog>, 
        private ws: WSService,
        private ustore: UserStore,
        private debug: DebugStore,
        private fb: FormBuilder,
		private _snackBar: MatSnackBar)
    {
        this.id = Number(this.data.idlocation);        
        this.ustore.onChange.subscribe(user => { 
            this.me = user; 
        });       
    }

    public ngOnInit() {			
		this.initForm(null);
		if (this.id){
			this.ws.location(this.id).subscribe((result: IResult) => {
				if(result.success) {
					this.debug.addInfo('WS.location', 'successful');
					this.indoorlocation = new Location(result.data);
					this.idcust= this.indoorlocation.idcustomer;						
					this.initForm(this.indoorlocation);
				}
				else {            
					this.debug.addError('WS.location', result.data);
				}
			}, error => {
				this.debug.addError('WS.location', error.message);   
			}); 
		}else{
			this.idcust = (this.me.customer)?this.me.customer.idcustomer:this.data.idcustomer;
			this.initForm(null);
		}	
	}     

	private initForm(data: Location|null){	
		if (!data){
			this.form = this.fb.group({
				name: this.name,
                idcustomer: this.idcust
			});
		  }else{
			this.form = this.fb.group({
				name: data.name,
                idcustomer: data.idcustomer
			});
		  } 
	}	

	public create(){
		if (this.form.invalid) return null;
		this.loading = true;
		this.ws.createIndoorLocation(this.form.value).subscribe((result: IResult) => {
			if(result.success) {
				const id = result.data.idcircuit;
				this.debug.addInfo('WS.createIndoorLocation', 'successful');
                this.dialogRef.close("Localización creada correctamente");
				this.loading = false;
			}
			else {            
				this.debug.addError('WS.createIndoorLocation', result.data);
				this.dialogRef.close("La Localización no se ha creado correctamente" + result.data);
				this.loading = false;			
			}
		}, error => {
			let msj = JSON.stringify(error.error.data);
			this.openSnackBar(msj.substr(2,msj.length-3));
			this.debug.addError('WS.createIndoorLocation', error.message); 
			this.loading = false;          
		});
	}

	public update(){
		if (this.form.invalid) return null;
		this.loading = true;
		this.ws.updateIndoorLocation(this.id, this.form.value).subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.updateIndoorLocation', 'successful');
                this.dialogRef.close(result);	
				this.loading = false;
			}
			else { 				         
				this.debug.addError('WS.updateIndoorLocation', result.data); 
				this.dialogRef.close(result); 
				this.loading = false;
			
			}
		}, error => {
			let msj = JSON.stringify(error.error.data);
			this.openSnackBar(msj.substr(2,msj.length-3)); 
			this.debug.addError('WS.updateIndoorLocation', error.message);
			this.loading = false;          
		}); 
	}

    public close() {
        this.dialogRef.close(null);
    }

    public save(){
        if(!this.id)
            this.create();
        else
            this.update();    

    }

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
  
  
}