import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { ContentPower } from 'src/modules/content/models/power';
import { Primetime } from 'src/modules/primetime/models/primetime';
import { PlayerPower } from 'src/modules/player/models/player-power';

import {TranslateService} from '@ngx-translate/core';
import { Power } from 'src/modules/power/models/power';
import moment from 'moment';
import { throwToolbarMixedModesError } from '@angular/material/toolbar';
import { UserStore } from "src/modules/user/stores/user.store";
import { WSService } from 'src/services/ws.service';
import { ElementSchemaRegistry } from '@angular/compiler';

@Component({
  selector: 'primetime-power',
  templateUrl: './power.component.html',
  styleUrls: ['./power.component.scss']
})
export class PowerComponent {
  @Output() modalSave = new EventEmitter<any[]>();
  @Input() selectedContentPowers:Power[] = [];
  @Input() selectedContentData:any = {};
  @Input() powers: PlayerPower[] = [];
  @Input() primetime: boolean = true;
  @Input() loadingPrimetime: boolean = false;
  @Input() exclusive: boolean = false;

	public weekdays = ['', 'WEEKDAYS.mon', 'WEEKDAYS.tue', 'WEEKDAYS.wed', 'WEEKDAYS.thu', 'WEEKDAYS.fri', 'WEEKDAYS.sat', 'WEEKDAYS.sun'];
  public contentPowers:any[] = [];
  public me: any;
  public loading:boolean = false;
  public has_primetime:boolean = false;

  constructor(
    private debug: DebugStore,
    private ustore: UserStore,
    private ws: WSService,
		private dialog: DialogService
  ) {
      this.ustore.onChange.subscribe(user => { 
        this.me = user; 
      });
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.selectedContentPowers && changes.selectedContentPowers.currentValue){
      let powers = changes.selectedContentPowers.currentValue;
      this.contentPowers = this.groupByDay(powers)
    }
    if(changes.exclusive && changes.exclusive.currentValue == true && this.selectedContentPowers.length == 0){
      this.openDialog(true);
    }
  }

  private groupByDay(powers){
    let array = [];
    powers.map(p => {
      if(!array.some(a => a.weekday === p.weekday))
        array.push({weekday: p.weekday, times:[{time_on: p.time_on, time_off: p.time_off, new: p.new}]})
      else
        array.map(a => {
          if(a.weekday === p.weekday)
            a.times.push({time_on: p.time_on, time_off: p.time_off, new: p.new})
        })
    })
    return array;
  }
  
  public openDialog(is_exclusive: boolean = false) {
    this.has_primetime = (this.me.customer && this.me.customer.has_sales && this.selectedContentData.products && this.selectedContentData.products.length > 0 && this.primetime);
  	this.dialog.contentpower('DIALOGS.content_power.title', {selections:this.selectedContentPowers, data: this.selectedContentData, has_primetime: this.has_primetime, is_exclusive: is_exclusive}).afterClosed().subscribe((result) => {
      if(result)
  	    this.modalSave.emit(result)
  	});
  }
}
