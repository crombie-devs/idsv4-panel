import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FlexLayoutModule} from "@angular/flex-layout";
import {FormsModule} from "@angular/forms";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatDialogModule} from "@angular/material/dialog";
import {MatSelectModule} from "@angular/material/select";
import {MatCheckboxModule} from "@angular/material/checkbox";

import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { SharedModule } from "src/shared/shared.module";
import { PrimetimeDialog } from "./dialogs/primetime/primetime.dialog";

import { PowerComponent } from './components/power/power.component';

@NgModule({
  declarations: [PowerComponent, PrimetimeDialog],
  imports: [
    CommonModule,
    MatDialogModule,
    MatCheckboxModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    SharedModule
  ],
  entryComponents:[PrimetimeDialog],
  exports: [PowerComponent],
  providers: [DialogService ]
})
export class PrimetimeModule {}