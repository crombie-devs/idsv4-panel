export class Primetime {
    weekday: number;
    hour: number;
    
    constructor(obj?: any) {
        this.weekday = obj.weekday;
        this.hour = obj.hour;
    }
}