import { Component, Inject, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DebugStore } from 'src/stores/debug.store';
import { WEEKDAYS, TIMES } from 'src/modules/content/models/powerdata';


@Component({
	selector: 'primetime-dialog',
  	templateUrl: './primetime.dialog.html',
  	styleUrls: ['./primetime.dialog.scss']
})

export class PrimetimeDialog {
    
    @ViewChild('powersContainer', {read: ViewContainerRef}) powersContainer: ViewContainerRef;
    
    components = [];

    public title: string;
    public load = false;
    public selectedPowers:any[] =  [];
    public dataBefore: {};
    public weekdays = WEEKDAYS
    public times = TIMES;
    public isMouseDown = false
    public powerCount: number = 1
    public newSelection: any = {};
    public selecting:boolean = true;
    public selectedData:any = {};
    
    public dayFrom: string = "";
    public daysTo: string[] = [];
    public me: any;

	constructor(public dialogRef: MatDialogRef<PrimetimeDialog>,
	    @Inject(MAT_DIALOG_DATA) public data,
	    private debug: DebugStore,
	    private componentFactoryResolver: ComponentFactoryResolver) { }
  
    public ngOnInit() {
        if(this.data.data){
            this.selectedData = this.data.data;
        }
    }

    public close() {
        this.dialogRef.close(false);
    }
}
