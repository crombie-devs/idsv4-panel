import { Component, Input, Output, EventEmitter, SimpleChange } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormField } from 'src/modules/form-field/models/form-field';
import { CustomNgxDatetimeAdapter} from './CustomNgxDatetimeAdapter';
import {
	MAT_MOMENT_DATE_FORMATS,
	MomentDateAdapter,
	MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  } from '@angular/material-moment-adapter';
import {DateAdapter, MatDateFormats, MAT_DATE_FORMATS, MAT_DATE_LOCALE, NativeDateAdapter} from '@angular/material/core';
import { NgxMatDateAdapter, NgxMatDateFormats, NGX_MAT_DATE_FORMATS } from '@angular-material-components/datetime-picker';

import {TranslateService} from '@ngx-translate/core';

  const CUSTOM_DATE_FORMATS: NgxMatDateFormats = {
    parse: {
      dateInput: 'l, LTS'
    },
    display: {
      dateInput: 'DD/MM/YYYY HH:mm',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
    }
  };
@Component({
  selector: 'form-field-date-time-hour-picker',
  templateUrl: './view.component.html',
  styleUrls: ['./styles.component.css'],
  providers: [
    // The locale would typically be provided on the root module of your application. We do it at
    // the component level here, due to limitations of our example generation script.
    {provide: MAT_DATE_LOCALE, useValue: 'es'},

    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: 'es'},
    {
      provide: NgxMatDateAdapter,
      useClass: CustomNgxDatetimeAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: NGX_MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS }
  ],
})

export class DateTimeHourPickerformFieldComponent {
	@Input() fieldData: FormField;
	@Input() formGroup: FormGroup;
  @Output() onChangeField: EventEmitter<any> = new EventEmitter<any>();

  public hideTime:boolean = false;
  public touchUI: boolean = false;
	  
	constructor(private dateAdapter: DateAdapter<any>, private translate: TranslateService) {
    this.dateAdapter.setLocale('es');	
  	}
  
  public ngOnInit() {
    if(this.fieldData){
      if(this.fieldData.name==="startDate")
        this.fieldData.label=this.translate.instant('Fecha Inicio');
      if(this.fieldData.name==="endDate")
        this.fieldData.label=this.translate.instant('Fecha Fin');
      if(this.fieldData.hideTime){
        this.hideTime = this.fieldData.hideTime
      }
    }
	}
  public changedField(data){
    this.onChangeField.emit();
  }
	public ngOnChanges(changes: SimpleChange){
	}
}
