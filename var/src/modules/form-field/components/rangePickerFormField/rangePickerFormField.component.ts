import { Component, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormField } from 'src/modules/form-field/models/form-field';

@Component({
  selector: 'form-field-rangepicker',
  templateUrl: './view.component.html',
  styleUrls: ['./styles.component.scss']
})

export class RangePickerformFieldComponent {
	@Input() fieldData: FormField;
	@Input() formGroup: FormGroup;
	@Output() onModelChange: EventEmitter<any> = new EventEmitter<any>();
  
	constructor() {	
  	}
  
  	public ngOnInit() {
	}
}
