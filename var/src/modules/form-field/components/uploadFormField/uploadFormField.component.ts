import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators, FormGroup, NgForm, FormGroupDirective } from '@angular/forms';
import { FormField } from 'src/modules/form-field/models/form-field';
import {ErrorStateMatcher} from '@angular/material/core';
import { FileHandle } from 'src/directive/drag-drop.directive';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';

export class SelectErrorStateMatcher implements ErrorStateMatcher {
	isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
	  const isSubmitted = form && form.submitted;
	  return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
	}
}

@Component({
  selector: 'form-field-upload',
  templateUrl: './view.component.html',
  styleUrls: ['./styles.component.scss']
})

export class UploadformFieldComponent {
	@Input() fieldData: FormField;
	@Input() formGroup: FormGroup;
	@Output() onFileDropped: EventEmitter<any> = new EventEmitter(null);
	@Output() onChangeField: EventEmitter<any> = new EventEmitter<any>();

	public files:File[] = [];

  	constructor(		
		private _snackBar: MatSnackBar,
		private translate: TranslateService) {
  	}
  
  	public ngOnInit() {
	}

	public onChange(ev){
		if(this.fieldData.multiple){
			const file = this.formGroup.get(this.fieldData.name).value;
			this.files.push(file)
			this.formGroup.get(`${this.fieldData.name}`).setValue(this.files);
		}
		this.onChangeField.emit(this.formGroup);
	}

	filesDropped(file: FileHandle[]): void {
		let validated: boolean = false;
		if(this.fieldData.accept){
			let acceptted = this.fieldData.accept.split(',');
			let extArray:string[] = file[0].file.name.split('.')
			let ext:string = extArray[extArray.length -1];
			if(acceptted.includes(`.${ext}`))
				validated = true;
		}
		if(validated){
			if(!this.fieldData.multiple)
				this.formGroup.patchValue({file: file[0].file})
			else{
				file.map(f => this.files.push(f.file))
				this.formGroup.get(`${this.fieldData.name}`).setValue(this.files);
			}
			this.onChangeField.emit(this.formGroup);
		}else{
			this.openSnackBar(this.translate.instant("El archivo seleccionado no es válido"))
		}
	}

	public delete(file){
		this.files.map((f,i) => {
			if(f.name === file.name)
				this.files.splice(i,1);
		})
		this.formGroup.get(`${this.fieldData.name}`).setValue(this.files);
	}

	public openSnackBar(message: string) {
		const mess:string = message.toString();
		this._snackBar.open(mess,'', {
		  duration: 1500,
		});
	}
	matcher = new SelectErrorStateMatcher();
}
