import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators, FormGroup, NgForm, FormGroupDirective } from '@angular/forms';
import { FormField } from 'src/modules/form-field/models/form-field';
import {ErrorStateMatcher} from '@angular/material/core';
import { Content } from 'src/modules/content/models/content'

@Component({
  selector: 'form-field-select-content',
  templateUrl: './view.component.html',
  styleUrls: ['./styles.component.scss']
})

export class SelectContentformFieldComponent {
	@Input() fieldData: FormField;
	@Input() formGroup: FormGroup;
	@Output() onChangeField: EventEmitter<any> = new EventEmitter<any>();
	public selected:number = null;

  	constructor() {	
  	}
  
  	public ngOnInit() {
		  
	}

	public contentSelected(evt, content){
		this.selected = content.idcontent;
		const field = this.fieldData.name;
		this.formGroup.patchValue({[field]: content.idcontent});
		this.onChangeField.emit(this.formGroup);
	}
}
