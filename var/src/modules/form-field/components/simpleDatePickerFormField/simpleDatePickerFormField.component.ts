import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormField } from 'src/modules/form-field/models/form-field';
import moment from 'moment';

@Component({
  selector: 'form-field-date-picker',
  templateUrl: './view.component.html',
  styleUrls: ['./styles.component.scss']
})

export class SimpleDatePickerformFieldComponent {
	@Input() fieldData: FormField;
	@Input() formGroup: FormGroup;
	@Output() onModelChange: EventEmitter<any> = new EventEmitter<any>();
	  
	constructor() {	
  	}
  
  	public ngOnInit() {
	}

	public changedField(data){
       
		this.onModelChange.emit(data);
	}
}
