import { RangePickerformFieldComponent } from './rangePickerFormField/rangePickerFormField.component';
import { SimpleDatePickerformFieldComponent } from './simpleDatePickerFormField/simpleDatePickerFormField.component';
import { SelectformFieldComponent } from './selectFormField/selectFormField.component';
import { MultiSelectformFieldAutocompleteComponent } from './multiSelectFormFieldAutocomplete/multiSelectFormFieldAutocomplete.component';
import { SelectformFieldMonospaceComponent } from './selectFormFieldMonospace/selectFormFieldMonospace.component';
import { MultiSelectformFieldComponent } from './multiSelectFormField/multiSelectFormField.component';
import { TextformFieldComponent } from './textFormField/textFormField.component';
import { NumberformFieldComponent } from './numberFormField/numberFormField.component';
import { AutocompleteformFieldComponent } from './autocompleteFormField/autocompleteFormField.component';
import { UploadformFieldComponent } from './uploadFormField/uploadFormField.component';
import { MonthPickerformFieldComponent } from './monthPickerFormField/monthPickerFormField.component';
import { SelectContentformFieldComponent } from './SelectContentFormField/selectContentField.component';
import { DateTimePickerformFieldComponent } from './dateTimePickerFormField/dateTimePickerFormField.component';
import { DateTimeHourPickerformFieldComponent } from './dateTimeHourPickerFormField/dateTimeHourPickerFormField.component';
export const FORM_FIELD_COMPONENTS = [
    SelectformFieldComponent,
    SelectformFieldMonospaceComponent,
    MultiSelectformFieldAutocompleteComponent,
    MultiSelectformFieldComponent,
    RangePickerformFieldComponent,
    SimpleDatePickerformFieldComponent,
    MonthPickerformFieldComponent,
    TextformFieldComponent,
    NumberformFieldComponent,
    AutocompleteformFieldComponent,
    UploadformFieldComponent,
    SelectContentformFieldComponent,
    DateTimePickerformFieldComponent,
    DateTimeHourPickerformFieldComponent
];