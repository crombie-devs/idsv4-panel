import { Component, Input, Output, EventEmitter, SimpleChanges, ViewChild, ElementRef, AfterViewInit  } from '@angular/core';
import { FormControl, Validators, FormGroup, NgForm, FormGroupDirective } from '@angular/forms';
import { FormField } from 'src/modules/form-field/models/form-field';
import {ErrorStateMatcher} from '@angular/material/core';
import { DialogService } from 'src/modules/dialog/services/dialog.service';

import {TranslateService} from '@ngx-translate/core';

export class SelectErrorStateMatcher implements ErrorStateMatcher {
	isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
	  const isSubmitted = form && form.submitted;
	  return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
	}
}

@Component({
  selector: 'form-field-select-monospace',
  templateUrl: './view.component.html',
  styleUrls: ['./styles.component.scss']
})

export class SelectformFieldMonospaceComponent implements AfterViewInit {
	@Input() fieldData: FormField;
	@Input() formGroup: FormGroup;
	@Input() isLoading: boolean = false;
	@Input() isRecomendation: boolean = false;
	@Input() panelOpen: boolean = false;
	@Input() color: string;
	@Output() onChangeField: EventEmitter<any> = new EventEmitter<any>();

	@ViewChild('select') select;

  	constructor(
		private dialog: DialogService,
		private translate: TranslateService
	  ) {		  
  	}
  
  	public ngOnInit() {
	}
	ngAfterViewInit(){
		if(this.isRecomendation || this.panelOpen)
			setTimeout(() => {
				this.select.open();
			}, 500)

	}
	public ngOnChanges(changes: SimpleChanges): void {
		//Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
		//Add '${implements OnChanges}' to the class.
		//if(changes.isLoading !== undefined && !changes.isLoading.firstChange && !changes.isLoading.currentValue && this.fieldData.options.length === 0)
		//	this.dialog.warning('FORMS.fields.select.warning.title', 'FORMS.fields.select.warning.text');
	}
	public onChange(){
		this.onChangeField.emit(this.formGroup);
	}

	matcher = new SelectErrorStateMatcher();
}
