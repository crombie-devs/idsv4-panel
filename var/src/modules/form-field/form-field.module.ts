import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import { FORM_FIELD_COMPONENTS } from './components/components';

import { CONFIG } from 'src/app/app.config';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import moment from 'moment';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';


export function HttpLoaderFactory(http: HttpClient) {
  let url = CONFIG.lang_url;  
  return new TranslateHttpLoader(http, `${url}/i18n/`, `.json?timestamp=${moment().valueOf()}`);
}

@NgModule({
  declarations: [FORM_FIELD_COMPONENTS],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MatProgressSpinnerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [],
  exports:[FORM_FIELD_COMPONENTS]
})
export class FormFieldModule {
  constructor(private translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('es');
    if(!sessionStorage.getItem('lang')){
      sessionStorage.setItem('lang', 'es');
      translate.use('es');
    }else{
      translate.use(sessionStorage.getItem('lang'));
    }
  }
}