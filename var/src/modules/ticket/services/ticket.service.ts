import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { HttpService } from '../../../services/http.service';
import { ICrudWS } from '../../../interfaces/ICrudWS.interface';
import { Ticket, ITicket, ITicketMisc, TicketComment, ITicketComment, Department, IDepartment, IResponsible } from '../models/ticket';


@Injectable({
    providedIn: 'root',
})
export class TicketService implements ICrudWS<Ticket> {

    idsite: Subject<number> = new Subject();
    $idsite = this.idsite.asObservable();

    constructor(private http: HttpService) { }

    public save(obj: any): Observable<ITicket> {

        if (obj.hasOwnProperty('id') && obj.id) {

            return this.update(obj);
        } else {

            return this.create(obj);
        }
    }

    public delete(id): Observable<ITicket> {
        return this.http.delete('private/ticket', [id], 'WS.ticket');
    }


    public create(obj: Ticket): Observable<ITicket> {
        return this.http.createWithFiles('private/ticket', obj, 'WS.ticket_add');
    }

    public update(obj: Ticket): Observable<ITicket> {

        return this.http.updateWithFiles('private/ticket', obj, 'WS.ticket_update');
    }

    public deleteArray(ids: number[]): Observable<ITicket> {
        return this.http.update('private/ticket/delete', { ids: ids }, 'WS.ticket_delete');
    }

    public get(id: number): Observable<ITicket> {
        return this.http.get('private/ticket', [id], 'WS.ticket');
    }

    public toSelect(args?: any[]): Observable<ITicket[]> {
        return this.http.get('private/tickets/toselect', args, 'WS.ticket');
    }

    public list(args?: any[]): Observable<any[]> {
        return this.http.get('private/tickets', args, 'WS.ticket');
    }

    public listBytype(args?: any[]): Observable<ITicket[]> {
        return this.http.get('private/tickets/type', args, 'WS.ticket');
    }



    public getCategories(): Observable<any[]> {
        return this.http.get('private/tickets/category', [], 'WS.get_ticket_category');
    }

    public getClients(args?: any[]): Observable<any[]> {
        return this.http.get('private/customers/toselect', [], 'WS.customer_toselect');
    }


    public saveCategories(obj: ITicketMisc): Observable<any> {
        if (obj.hasOwnProperty('id') && obj.id != 0) {
            return this.updateCategory(obj);
        } else {
            return this.createCategory(obj);
        }
    }


    public createCategory(obj: ITicketMisc): Observable<ITicketMisc> {
        return this.http.create('private/ticket/category', obj, 'WS.ticket_category_add');
    }

    public updateCategory(obj: ITicketMisc): Observable<ITicketMisc> {
        return this.http.update('private/ticket/category', obj, 'WS.ticket_category_update');
    }

    public deleteCategory(ids: number[]): Observable<ITicketMisc> {
        return this.http.update('private/ticket/category/delete', { ids: ids }, 'WS.ticket_category_delete');
    }

    public getPriorities(): Observable<any[]> {
        return this.http.get('private/tickets/priority', [], 'WS.get_ticket_priority');
    }

    public savePriority(obj: ITicketMisc): Observable<any> {
        if (obj.hasOwnProperty('id') && obj.id != 0) {
            return this.updatePriority(obj);
        } else {
            return this.createPriority(obj);
        }
    }


    public createPriority(obj: ITicketMisc): Observable<ITicketMisc> {
        return this.http.create('private/ticket/priority', obj, 'WS.ticket_category_add');
    }

    public updatePriority(obj: ITicketMisc): Observable<ITicketMisc> {
        return this.http.update('private/ticket/priority', obj, 'WS.ticket_category_update');
    }

    public deletePriority(ids: number[]): Observable<ITicketMisc> {
        return this.http.update('private/ticket/priority/delete', { ids: ids }, 'WS.ticket_category_delete');
    }

    public getPrStatus(): Observable<any[]> {
        return this.http.get('private/tickets/status', [], 'WS.get_ticket_status');
    }

    public saveStatus(obj: ITicketMisc): Observable<any> {
        if (obj.hasOwnProperty('id') && obj.id != 0) {
            return this.updateStatus(obj);
        } else {
            return this.createStatus(obj);
        }
    }


    public createStatus(obj: ITicketMisc): Observable<ITicketMisc> {
        return this.http.create('private/ticket/status', obj, 'WS.ticket_status_add');
    }

    public updateStatus(obj: ITicketMisc): Observable<ITicketMisc> {
        return this.http.update('private/ticket/status', obj, 'WS.ticket_status_update');
    }

    public deleteStatus(ids: number[]): Observable<ITicketMisc> {
        return this.http.update('private/ticket/status/delete', { ids: ids }, 'WS.ticket_status_delete');
    }




    public saveComment(obj: TicketComment): Observable<any> {
        if (obj.hasOwnProperty('id') && obj.id != null && obj.id != 0) {
            return this.updateComment(obj);
        } else {
            return this.createComment(obj);
        }
    }


    public createComment(obj: ITicketComment): Observable<ITicketComment> {
        return this.http.createWithFiles('private/ticket/comment', obj, 'WS.ticket_comment_add');
    }

    public updateComment(obj: ITicketComment): Observable<ITicketComment> {
        return this.http.updateWithFiles('private/ticket/comment', obj, 'WS.ticket_comment_update');
    }

    public getComments(id: number): Observable<any[]> {
        return this.http.get('private/ticket/comments', [id], 'WS.get_ticket_comments');
    }

    public deleteComment(id: number[]): Observable<any> {
        return this.http.delete('private/ticket/comment', id, 'WS.ticket_comment_delete');
    }


    public getByCustomer(id: number): Observable<any> {
        return this.http.get('private/ticket/customer', [id], 'WS.ticket_comment_delete');
    }


    public saveDepartment(obj: IDepartment): Observable<any> {
        if (obj.hasOwnProperty('id') && obj.id != null && obj.id != 0) {
            return this.updateDepartment(obj);
        } else {
            return this.createDepartment(obj);
        }
    }


    public createDepartment(obj: IDepartment): Observable<IDepartment> {
        return this.http.create('private/department', obj, 'WS.ticket_department_add');
    }

    public updateDepartment(obj: IDepartment): Observable<IDepartment> {
        return this.http.update('private/department', obj, 'WS.ticket_department_update');
    }

    public getDepartments(): Observable<any[]> {
        return this.http.get('private/departments', [], 'WS.get_ticket_departments');
    }

    public deleteDepartment(ids: number[]): Observable<any> {
        return this.http.update('private/department/delete', { ids: ids }, 'WS.ticket_department_delete');
    }


    public saveResponsible(obj: IResponsible): Observable<any> {
        if (obj.hasOwnProperty('id') && obj.id != null && obj.id != 0) {
            return this.updateResponsible(obj);
        } else {
            return this.createResponsible(obj);
        }
    }


    public createResponsible(obj: IDepartment): Observable<IDepartment> {
        return this.http.create('private/responsible', obj, 'WS.ticket_responsible_add');
    }

    public updateResponsible(obj: IDepartment): Observable<IDepartment> {
        return this.http.update('private/responsible', obj, 'WS.ticket_responsible_update');
    }

    public getResponsibles(): Observable<any[]> {
        return this.http.get('private/responsibles', [], 'WS.get_ticket_responsibles');
    }

    public deleteResponsible(ids: number[]): Observable<any> {
        return this.http.update('private/responsible/delete', { ids: ids }, 'WS.ticket_responsible_delete');
    }

    public closeTicket(id: number): Observable<any> {
        return this.http.get('private/ticket/close', [id], 'WS.ticket_ticket_close');
    }


    public createContactSupport(obj: any): Observable<any> {
        return this.http.createWithFiles('public/ticket/support', obj, 'WS.ticket_contact_support_add');
    }








}


