import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";
// import { TicketFormPage } from './pages/form/form.page';
import { TicketListPage } from "./pages/list/list.page";
import { MatIconModule } from "@angular/material/icon";
import { MatToolbarModule } from "@angular/material/toolbar";
import { TicketRoutingModule } from './ticket-routing.module';
import { MatTooltipModule } from "@angular/material/tooltip";
import { StatusListPage} from './pages/status/list-status.page';
import { PriorityListPage } from './pages/priority/list-priority.page';
import { CategoryListPage } from "./pages/category/list-category.page";
import { TicketDialog } from "./dialogs/ticket-dialog/ticket.dialog";
import { PriorityDialog } from "./dialogs/priority-dialog/priority.dialog";
import { ColorPickerModule } from 'ngx-color-picker';
import { UserModule } from "../user/user.module";
import { TicketSelectComponent } from "./components/select-categories/select.component";
import { SharedModule } from "../../shared/shared.module";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { DirectivesModule } from "src/directive/directives.module";
import { CommentComponent }  from './components/comments/comment.component';
import { ViewPage } from './pages/view/view.page';
import { AssetModule } from "../asset/asset.module";
import { DragDropFileComponent } from './components/file-manager/drag-drop-files.component';
import { DndDirective} from './components/file-manager/drag-drop-file.directive';
import { ProgressComponent} from './components/progress/progress.component'; 
import { CommentsViewComponent  } from './components/comments-view/comments-view.component';
import { DashboardPage } from './pages/dashboard/dashboard.page';
import { ListTicketComponent } from './components/list-ticket/list-ticket.component';
import { StatsTicketComponent } from './components/stats-ticket/stats-ticket.component'; 
import { ChartsModule } from 'ng2-charts';
import { StatsTypeComponent } from './components/stats-type/stats-type.component';
import { CustomerModule } from "../customer/customer.module";
import { StatsToolbarComponent } from './components/stats-toolbar/stats-toolbar.component';
import { DepartmentsListPage } from './pages/departments/list-departments.page';
import { DepartmentDialog } from './dialogs/department-dialog/department.dialog';
import { ResponsibleListPage } from './pages/responsible/list-responsible.page';
import { ResponsibleDialog } from './dialogs/responsible-dialog/responsible.dialog';
import { SelectDepartmentsComponent } from './components/select-departments/select-departments.component';
import { SelectResponsiblesComponent } from './components/select-responsibles/select-responsibles.component';
import { SiteModule } from "../site/site.module";
import { MatButtonModule } from "@angular/material/button";
import { ImageDialog } from './dialogs/image-dialog/image.dialog';
import { PipesModule } from "src/pipes/pipes.module";
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { SelectComponent } from "./components/select-component/select.component";

import { TicketDashboardPage } from "./pages/ticket-dashboard/ticket-dashboard.page";

import { TicketTableComponent } from "./components/ticket-table/ticket-table.component";
import { TicketListComponent } from "./components/ticket-list/ticket-list.component";
import { TicketChartBarComponent } from "./components/ticket-charts/ticket-chart-bar/ticket-chart-bar.component";
import { TicketChartBarVerticalComponent } from "./components/ticket-charts/ticket-chart-bar-vertical/ticket-chart-bar-vertical.component";
import { TicketChartPieComponent } from "./components/ticket-charts/ticket-chart-pie/ticket-chart-pie.component";

import { TicketNewPage } from "./pages/tickets/ticket-new/ticket-new.page";
import { TicketEditPage } from "./pages/tickets/ticket-edit/ticket-edit.page";

@NgModule({
  declarations: [
      TicketListPage,
      StatusListPage,
      PriorityListPage,
      CategoryListPage,
      TicketDialog,
      PriorityDialog,
      TicketSelectComponent,
      CommentComponent,
      ViewPage,
      DragDropFileComponent,
      DndDirective,
      ProgressComponent,
      CommentsViewComponent,
      DashboardPage,
      ListTicketComponent,
      StatsTicketComponent,
      StatsTypeComponent,
      StatsToolbarComponent,
      DepartmentsListPage,
      DepartmentDialog,
      ResponsibleListPage,
      ResponsibleDialog,
      SelectDepartmentsComponent,
      SelectResponsiblesComponent,
      ImageDialog,

      SelectComponent,

      TicketDashboardPage,
      TicketNewPage,
      TicketEditPage,
      TicketTableComponent,
      TicketListComponent,
      TicketChartBarComponent,
      TicketChartBarVerticalComponent,
      TicketChartPieComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatCheckboxModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    MatToolbarModule,
    TicketRoutingModule,
    MatTooltipModule,
    SharedModule,
    ColorPickerModule,
    UserModule,
    NgxMatFileInputModule,
    MatDatepickerModule,
    MatFormFieldModule,
    DirectivesModule,
    AssetModule,
    ChartsModule,
    CustomerModule,
    SiteModule,
    MatButtonModule,
    CustomerModule,
    PipesModule,
    MatProgressSpinnerModule
  ],

  exports: [
    TicketSelectComponent,
    CommentComponent,
    DragDropFileComponent,
  ],
  providers: [],
  entryComponents: []
 
})
export class TicketModule {}
