import { Component, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Ticket } from '../../models/ticket';
import { TicketService } from '../../services/ticket.service';
import { ChartOptions, ChartType, ChartDataSets, ChartTooltipOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import { FormControl } from '@angular/forms';
import moment from 'moment';
import { Moment } from 'moment';
import { MatDatepicker } from '@angular/material/datepicker';




@Component({
    selector: 'stats-toolbar',
    templateUrl: './stats-toolbar.component.html',
    styleUrls: ['./stats-toolbar.component.scss']
})

export class StatsToolbarComponent {
    init = new FormControl(moment());
    end = new FormControl(moment());
    date = new FormControl(moment());
    tickets: Ticket[] = [];
    ticketsTemp: Ticket[] = [];


    @Output() sendDate: EventEmitter<any> = new EventEmitter();
    @Output() sendCustomer: EventEmitter<any> = new EventEmitter();

    public barChartData: ChartDataSets[] = [];

    constructor(private route: ActivatedRoute, private _ticket: TicketService) { }

    ngOnInit() {
        this._ticket.list([]).subscribe((tickets: Ticket[]) => {
            if (tickets) {
                this.tickets = tickets;
                this.ticketsTemp = tickets;
            }
        });
    }



    dateChange(datepicker) {
        datepicker.close();
        this.filterByDate();

    }



    filterByDate() {
        const tickets = this.ticketsTemp.filter(tk => moment(tk.created_at).isSameOrAfter(this.init.value)
        && moment(tk.created_at).isSameOrBefore(this.end.value));
        this.filterByType(tickets);
      
      
    }


    filterByType(tickets): void {
        const abiertos = tickets.filter(t => t.finalizado == 1).length;
        const cerrados = tickets.filter(t => t.finalizado == 0).length;
        this.barChartData = [
            { data: [tickets.length], label: 'Total' },
            { data: [abiertos], label: 'Abiertos' },
            { data: [cerrados], label: 'Cerrados' }
        ];
        this.sendDate.emit(this.barChartData);
    }

    customerSelected(event): void {
       
  
        this._ticket.getByCustomer(event).subscribe(item => {     
              console.log(this.tickets);
              console.log(item); 
              let users = this.tickets.filter(tk => item.some(i => i.iduser == tk.id_user));
           
            this.filterByType(item);
            this.sendCustomer.emit(item);
        });
    }





}
