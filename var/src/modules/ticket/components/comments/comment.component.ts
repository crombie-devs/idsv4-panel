import { Component, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { TicketComment } from '../../models/ticket';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent, CKEditorComponent } from '@ckeditor/ckeditor5-angular';
import { TicketService } from '../../services/ticket.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import { SelectResponsiblesComponent } from '../select-responsibles/select-responsibles.component';
import { MatSnackBar } from '@angular/material/snack-bar';






@Component({
    selector: 'ticket-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.scss']
})

export class CommentComponent {
    public form: FormGroup;
    public sHtml: string;
    public Editor = ClassicEditor;
    public id;
    public files: File[] = [];
    public em: UserStore;
    public create_by: number;
    public id_responsible: number;
    public id_department: number;
    @ViewChild(CKEditorComponent) ckeditor: CKEditorComponent;
    @ViewChild(SelectResponsiblesComponent) select: SelectResponsiblesComponent;
    @Output() onSend: EventEmitter<any> = new EventEmitter();
    @Input() idTicket: number;


    constructor(private _ticket: TicketService, private _snackBar: MatSnackBar) {

        this.em = new UserStore();
        this.create_by = this.em.getUser().iduser;

        this.form = new FormGroup({
            id: new FormControl(0),
            descripcion: new FormControl('', [Validators.required]),
            files: new FormControl([]),
            id_status: new FormControl(0),
            notificar_usuario: new FormControl(1),
            create_by: new FormControl(this.create_by),
            id_responsible: new FormControl()
        });
    }




    getFiles(events): void {
        this.files = events
    }

    public onChange({ editor }: ChangeEvent) {
        const data = editor.getData();
        this.form.patchValue({ descripcion: data });
    }

    setValues(event, type): void {
        if (type == 'id_department') {
            this.select.filterResponsibles(event);
        }
        this.form.patchValue({ [type]: event });
    }

    initForm(data: TicketComment) {
        this.form.setValue({
            descripcion: data.mensaje,
            files: data.files,
            id: data.id,
            create_by: data.create_by,
            notificar_usuario: data.notificar_usuario,
            id_status: data.id_status,
            id_responsible: data.responsible.id
        });
        this.sHtml = data.mensaje;
    }


    create(): void {
        if (this.form.valid) {
            if (this.files.length > 0) {
                this.form.value.files = this.files;
            }
            this.form.value.create_by = this.create_by;
            this.form.value.id_ticket = this.idTicket;

            this._ticket.saveComment(this.form.value).subscribe(item => {
                if (item) {
                    this.onSend.emit(item[0]);
                }
            });
        } else {
            this.openSnackBar("Debes añadir una descripcion");
        }
    }


    clear(): void {
        this.form.reset();
        this.files = [];
        this.sHtml = '';
    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
            panelClass: ['orange-snackbar']
        });
    }


}
