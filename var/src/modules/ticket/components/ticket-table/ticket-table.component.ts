import { Component, Input, OnChanges, OnInit, QueryList, SimpleChange, SimpleChanges, ViewChild, ViewChildren }    from '@angular/core';
import { SelectionModel }                                   from '@angular/cdk/collections';
import { MatCheckbox }                                      from '@angular/material/checkbox';
import { MatInput }                                         from '@angular/material/input';
import { MatPaginator }                                     from '@angular/material/paginator';
import { MatSnackBar }                                      from '@angular/material/snack-bar';
import { MatSort, MatSortable }                             from '@angular/material/sort';
import { MatTableDataSource }                               from '@angular/material/table';
import { ActivatedRoute, Router }                           from '@angular/router';

import { TranslateService }                                 from '@ngx-translate/core';

import { Ticket }                                           from 'src/modules/ticket/models/ticket';

import { TicketService }                                    from 'src/services/ticket.service';
import { DialogService }                                    from 'src/modules/dialog/services/dialog.service';

@Component({
    selector: 'ticket-table',
    templateUrl: './ticket-table.component.html',
    styleUrls: ['./ticket-table.component.scss']
})
export class TicketTableComponent implements OnInit {

    constructor(
        private router: Router,
        private translate: TranslateService,
        private dialog: DialogService,
        private _ticket: TicketService,
        private _snackBar: MatSnackBar
    ) { }

    @ViewChildren('table') table: QueryList<MatCheckbox>
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild(MatInput) input: MatInput;

    @Input() dataTickets: any[];

    public displayedColumns: string[] = ['select', 'id', 'title', 'status', 'responsible', 'site', 'priority', 'category', 'created_at', 'updated_at'];
    public dataSource = new MatTableDataSource<any>();
    public selection = new SelectionModel<any>(true, []);

    public dataSourceTemp: any[] = [];
    
    public new: boolean = false;
    public edit: boolean = false;
    public deleted: boolean = false;
    public permission: boolean = false;
    public load: boolean = true;

    public title: string = "Abiertas";

    public filter: any[];
    public all: number = 0;
    public open: number = 0;;
    public close: number = 0;
    public commemts_not_read: number = 0;

    ngOnInit() { 

        this._ticket.getTickets().subscribe(response => {
            
            let res: any = response['data'].filter(tk => tk.deleted != 1);

            this.all = res.length;
            this.open = res.filter(tk => tk.id_status != 5).length;
            this.close = res.filter(tk => tk.id_status == 5).length;
            res.forEach(tk => {
                    this.commemts_not_read = this.commemts_not_read + tk.comments.filter(t => t.leido == 0).length;
            });

            
            this.title = "Todas";
            this.filter = res;
        
            this.format(this.filter);

            this.load = false;

            // this.sort.sort(({ id: 'id', start: 'desc'}) as MatSortable);
            this.dataSource.sort = this.sort;
        }); 
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        // this.sort.sort(({ id: 'id', start: 'desc'}) as MatSortable);
        this.dataSource.sort = this.sort;
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    updateFilterStatus(filterValue: number) {
        this.applyFilter(filterValue.toString());
    }

    public update() {        
        if (this.selection.selected.length > 1) {
            this.openSnackBar("para editar solo puedes seleccioar un ticket");
                return;
        }
        this.router.navigate(['/private/ticket/edit', this.selection.selected[0].id])        
     }


    checkTable(row, value) {
        if (row.hasOwnProperty([value])) {  
            if (row[value] != null) {
                return true;
            }
        }
        return false;
    } 

    public delete() {
        let contentsByDelete = [];
		this.table.toArray().forEach(item => {
			if (item.checked) 
				contentsByDelete.push(item.value);			
	    });	
       
        if (contentsByDelete.length == 0)
        this.dialog.warning(this.translate.instant('Aviso'), this.translate.instant('Debe seleccionar al menos un contenido')).subscribe();
    else
        this.dialog.confirm(this.translate.instant('Confirmar Archivado'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
            if (confirm){
                this._ticket.deleteArray(contentsByDelete).subscribe(result => {
                    if (result){
                        this.dataSourceTemp =   this.dataSourceTemp.filter(ds => !contentsByDelete.includes(ds.id));
                        this.dataSource.data = this.dataSource.data.filter(ds =>!contentsByDelete.includes(ds.id));
                        this.renderDataTable(this.dataSource.data);
                        this.dialog.warning('Aviso', 'Los contenidos han sido eliminados').subscribe();
                    }
                })
            }
        });
    }

    public renderDataTable(data: Ticket[]) {
        this.dataSource = new MatTableDataSource();
        // this.dataSource.data = data.reverse();
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.load = false;
    }

    isAllSelected() {
		if (this.dataSource.paginator){
			const numSelected = this.selection.selected.length;
			const page = this.dataSource.paginator.pageSize;
			let endIndex: number;
			if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
			endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
			} else {
			endIndex = this.dataSource.data.length - (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize);
			}
			return numSelected === endIndex;
		}
    }

    masterToggle() {
        this.isAllSelected() ? this.selection.clear() : this.selectRows();
    }

    selectRows() {
        let endIndex: number;
        if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
          endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
        } else {
          endIndex = this.dataSource.data.length;
        }        
        for (let index = (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize); index < endIndex; index++) {
          this.selection.select(this.dataSource.data[index]);
        }
    }

    public checkboxLabel(row?: Ticket): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
    }

    public openSnackBar(message: string) {
        this._snackBar.open(message,'', {
            duration: 1500,
            panelClass: ['orange-snackbar']
        });
    }

    updateAll() {
        this.title = "Todas";

        this._ticket.getTickets().subscribe(response => {

            let res: any = response['data'];

            this.format(res);
        });
    }

    updateOpen() {
        this.title = "Abiertas";

        this._ticket.getTickets().subscribe(response => {

            let res: any = response['data'];

            this.filter = res.filter(
                t => t.id_status != 5
            );
    
            this.format(this.filter);
        });
    }

    updateClosed() {
        this.title = "Cerradas";

        this._ticket.getTickets().subscribe(response => {

            let res: any = response['data'];

            this.filter = res.filter(
                t => t.id_status == 5
            );
    
            this.format(this.filter);
        });

    }

    updateNotRead() {
        this.title = "con comentarios no leidos";

        this._ticket.getTickets().subscribe(response => {

            let res: any = response['data'];

            this.filter = res.filter(
                t => { 
                    const x = t.comments.filter(c => c.leido == 0).length;
                    if (x > 1) {
                        return t;
                    }
                }
            );
    
            this.format(this.filter);
        });
    }

    format(filter) {
        const dateMaping: any = filter;

        dateMaping.forEach(m => {
            m.id = m.id,
            m.title = m.title,
            m.status = m.status.name,
            m.responsible = (m.responsible) ? m.responsible.name : "Todos",
            m.site = (m.site) ? m.site.name : "Todos",
            m.priority_color = (m.priority) ? m.priority.color : "#EDEDED",
            m.priority = (m.priority) ? m.priority.name : "No Asignado",
            m.category = (m.category) ? m.category.name : "No Asignado",
            m.updated_at = (m.id_status == 5) ? m.updated_at : "-"
        });

        this.dataSource.data = dateMaping;
    }
}
