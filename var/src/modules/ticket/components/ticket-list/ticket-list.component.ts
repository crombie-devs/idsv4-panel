import { Component, Input, OnChanges, OnInit, SimpleChanges }   from '@angular/core';
import { MatTableDataSource }                                   from '@angular/material/table';

export interface Element {
    quantity: number;
    name: string;
    tickets_open: number;
    tickets_close: number;
}

@Component({
    selector: 'ticket-list',
    templateUrl: './ticket-list.component.html',
    styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit, OnChanges {

    public displayedColumns: string[] = ['quantity', 'name', 'tickets'];
    public dataSource = new MatTableDataSource<any>();
    
    @Input() columnKey: string;
    @Input() dataList: Element[];
    
    constructor() { }

    ngOnInit() { }

    ngOnChanges(changes: SimpleChanges) {

        if (!this.columnKey || !this.dataList) {
            return;    
        }

        this.dataSource.data = this.dataList.sort((a, b) => { return b.quantity - a.quantity; });;
    }
}
