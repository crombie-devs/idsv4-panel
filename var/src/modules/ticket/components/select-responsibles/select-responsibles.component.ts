import { Component, OnInit, Input,Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormControl,  FormGroup} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IResponsible } from '../../models/ticket';
import { TicketService } from '../../services/ticket.service';


@Component({
  selector: 'select-responsibles',
  templateUrl: './select-responsibles.component.html',
  styleUrls: ['./select-responsibles.component.scss']
})
export class SelectResponsiblesComponent implements OnInit {

	@Output() onSelected = new EventEmitter<number>();
	@Input() usersToSelect:boolean;
	@Input() selected: number = null;
	@Input() apparence: boolean = true;
	@Input() id: number;


	public responsibles :IResponsible[] = [];
    public responsiblesTemp :IResponsible[] = [];
    public showList: boolean;
	public filterForm:FormGroup;


  	constructor(
      	private _snackBar: MatSnackBar,
		private _ticket: TicketService
	) {	
        this.filterForm = new FormGroup({
			id: new FormControl('')
		})

    	
    }

	ngOnInit(): void {
		setTimeout(() =>{
			this.getResponsibles();
		}, 500)
	
    }
  
	private getResponsibles(){
			this._ticket.getResponsibles().subscribe((result: any) => {
				if(result) {					
					this.responsibles = result;
                    this.responsiblesTemp = [...this.responsibles];
                    
                    if(this.id) {
                        this.showList = true;
                        this.filterForm.get('id').setValue(this.id);
                    }
		        	
				} else {            
					this.openSnackBar(result);
				}
			}, error => {
				
			}); 		
	}


    

    
    public filterResponsibles(id): void {
        this.showList = true;
        this.responsibles = this.responsiblesTemp.filter(rt => rt.department.id == id);

    }

  
  	public captureValue() {
		let id = this.filterForm.value.id;
		this.onSelected.emit(id);
	}
	
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
	

	public ngOnChanges(changes: SimpleChanges): void {
		if(changes.selected)
			this.filterForm.get('id').setValue(changes.selected.currentValue);
	}

}
