import { Component, OnInit, Input,Output, EventEmitter, SimpleChanges } 	from '@angular/core';
import { FormControl,  FormGroup } 											from '@angular/forms';
import { MatSnackBar } 														from '@angular/material/snack-bar';
import { ITicketMisc } 														from '../../models/ticket';
import { TicketService } 													from '../../services/ticket.service';

@Component({
  selector: 'select-component',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {

	@Input() ticketToSelect:boolean;
	@Input() selected: number = null;
	@Input() apparence: boolean = true;
	@Input() id: number = 0;
    @Input() type: string;
    @Input() label: string;
	@Input() placeholder: string;

	@Output() onSelected = new EventEmitter<number>();

	public ticketMisc: ITicketMisc[] = [];
	public filterForm:FormGroup;
	public superAdmin:boolean = false;

  	constructor(
		private _snackBar: MatSnackBar,
		private _ticket: TicketService
	) {	
        this.filterForm = new FormGroup({
			id: new FormControl(this.selected)
		});
    }

	ngOnInit(): void {
		switch(this.type) {
			case 'category':
				this.getCategory();
				break;
			case 'priority':
				this.getPriority();
				break;
			case 'status':
				this.getStatus();
				break;
			case 'site':
				this.getSites();
				break;
		}  	
    }

    setValue(id: number): void {
        this.filterForm.value.id = id;
    }
    
	private getCategory(){	
		this._ticket.getCategories().subscribe((result: any) => {
			if(result) {					
				this.ticketMisc = result;                   
				if(this.id){						
					this.filterForm.patchValue({id: this.id+''})
				}
			} else {            
				this.openSnackBar(result);
			}
		}, error => {
			
		}); 		
	}

    private getPriority(){	
		this._ticket.getPriorities().subscribe((result: any) => {         
			if(result) {					
				this.ticketMisc = result;                    
				if(this.id){						
					this.filterForm.patchValue({id: this.id+''})
				}
			} else {            
				this.openSnackBar(result);
			}
		}, error => {
			
		}); 	
	}

    private getStatus(){	
		this._ticket.getPrStatus().subscribe((result: any) => {
			if(result) {					
				this.ticketMisc = result;                     
				if(this.id){						
					this.filterForm.patchValue({id: this.id+''})
				}

			} else {            
				this.openSnackBar(result);
			}
		}, error => {
			
		}); 
	}

	private getSites(){	
		this._ticket.getPrStatus().subscribe((result: any) => {
			if(result) {					
				this.ticketMisc = result;                     
				if(this.id){						
					this.filterForm.patchValue({id: this.id+''})
				}

			} else {            
				this.openSnackBar(result);
			}
		}, error => {
			
		}); 
	}

    public captureValue() {
		let id = this.filterForm.value.id;
		this.onSelected.emit(id);
	}
	
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
	
    public ngOnChanges(changes: SimpleChanges): void {
		if(changes.selected)
			this.filterForm.get('id').setValue(changes.selected.currentValue);
	}

}
