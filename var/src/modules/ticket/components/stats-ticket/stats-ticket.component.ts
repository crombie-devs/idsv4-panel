import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Ticket } from '../../models/ticket';
import { TicketService } from '../../services/ticket.service';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';



@Component({
    selector: 'stats-ticket',
    templateUrl: './stats-ticket.component.html',
    styleUrls: ['./stats-ticket.component.scss']
})

export class StatsTicketComponent {
    @Input() tickets:  Ticket[] = [];
    @Input() set: boolean;

    doughnutChartLabels: Label[] = ['BMW', 'Ford', 'Tesla'];
    doughnutChartData: MultiDataSet = [
        [55, 25, 20]
    ];
    doughnutChartType: ChartType = 'doughnut';

    public barChartOptions: ChartOptions = {
        responsive: true,
        scales: {
            yAxes: [
                {
                    display: true,
                    ticks: {
                      min: 0,
                    } 
                }
            ]
        }
       
    };

    public barChartLabels: Label[] = ['Tickets'];
    public barChartType: ChartType = 'bar';
    public barChartLegend = true;
    public barChartPlugins = [];
    public barChartData: ChartDataSets[] = [];
    constructor(private _ticket: TicketService) { }
   
    ngOnInit() {
        this._ticket.list([]).subscribe((tickets: Ticket[]) => {
            if (tickets) {
                this.tickets = tickets;
                this.filterByType(tickets); 
            }
        });
    }


    setValues(tickets): void {
        this.barChartData = tickets;
    }


    filterByType(tickets): void {
            const abiertos = tickets.filter(t => t.finalizado == 1).length;
            const cerrados = tickets.filter(t => t.finalizado == 0).length;
            this.barChartData = [
                { data: [tickets.length], label: 'Total' },
                { data: [abiertos], label: 'Abiertos' },
                { data: [cerrados], label: 'Cerrados' }
            ]
    }

  

}
