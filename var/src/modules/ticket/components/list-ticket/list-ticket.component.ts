import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatInput } from '@angular/material/input';
import { Ticket } from '../../models/ticket';
import { TicketService } from '../../services/ticket.service';






@Component({
    selector: 'list-ticket',
    templateUrl: './list-ticket.component.html',
    styleUrls: ['./list-ticket.component.scss']
})

export class ListTicketComponent {

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChildren('table') table: QueryList<MatCheckbox>
    @ViewChild(MatInput) input: MatInput;

    public displayedColumns: string[] = ['id', 'titulo', 'status', 'responsable', 'ref', 'prioridad', 'categoria'];
    public selection = new SelectionModel<Ticket>(true, []);
    public dataSource = new MatTableDataSource<Ticket>([]);
    public dataSourceTemp: Ticket[] = []
    public new: boolean = false;
    public edit: boolean = false;
    public deleted: boolean = false;
    public permission: boolean = false;
    public load: boolean = false;



    private data: Ticket[];

    constructor(private router: Router, private _ticket: TicketService, private route: ActivatedRoute) {

        this.route.params.subscribe(param => {

            this._ticket.listBytype([1]).subscribe((tickets: Ticket[]) => {
                if (tickets) {
                    this.dataSourceTemp = [...tickets];
                    this.renderDataTable(tickets);
                }
            });

        });
    }

    public ngOnInit() { }


    checkTable(row, value, index) {

        if (row.hasOwnProperty([value])) {

            if (row[value] != null) {
                return true;
            }
        }

        return false;

    }

    public applyFilter(event: string) {
        this.selection.clear();
        if (!event)
            this.dataSource.data = this.dataSourceTemp;
        else {
            if (typeof event == 'string') {
                this.dataSource.data = this.dataSourceTemp.filter(ticket =>
                    ticket.titulo.toLowerCase().includes(event.trim().toLowerCase()));
            }
        }
    }





    public renderDataTable(data: Ticket[]) {
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = data.reverse();
        console.log(this.dataSource.data)
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.load = false;
    }

    public isAllSelected() {
        const numSelected = this.selection.selected.length;
        let numRows = 0;
        if (Array.isArray(this.dataSource.data)) {
            numRows = this.dataSource.data.length;
        }

        return numSelected === numRows;
    }

    public masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    public checkboxLabel(row?: Ticket): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
    }


}
