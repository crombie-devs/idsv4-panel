import { Component, Input } from '@angular/core';
import { Ticket } from '../../models/ticket';





@Component({
    selector: 'stats-type',
    templateUrl: './stats-type.component.html',
    styleUrls: ['../../pages/dashboard/dashboard.page.scss', './stats-type.component.scss']
})

export class StatsTypeComponent {

    @Input() users: any[] = [];
    @Input() categories: Ticket[] = [];



    constructor() { }

    ngOnInit() {}



    setValues(tickets): void {
        console.log(tickets);
        this.users = [];
        this.categories = [];
        this.filterByUser(tickets); 
        this.filterByCategory(tickets);
    }


 
   filterByUser(tickets): void {
    let response = {};
    tickets.forEach(tk => {
      
        if (!response.hasOwnProperty(tk.user.name)) {
            let name = tk.user.name;
            response[name] = {
                 
                    open: 0,
                    close: 0,
                    name                
            }
        }
        if (tk.finalizado == 1) {
            response[tk.user.name].open = response[tk.user.name].open + 1 ;
        } else {
            response[tk.user.name].close = response[tk.user.name].close + 1 ;
        }
        
    });
   
    let r = Object.keys(response)
      r.forEach(key => {
     this.users.push(response[key]);
     });
    
   }



   filterByCategory(tickets): void {
    let response = {};
    tickets.forEach(tk => {
        if (!response.hasOwnProperty(tk.category.name)) {
            let name = tk.category.name;
            response[name] = {
                    open: 0,
                    close: 0,
                    name
                
            }
        }
       
        if (tk.finalizado == 1) {
            response[tk.category.name].open = response[tk.category.name].open + 1 ;
        } else {
            response[tk.category.name].close = response[tk.category.name].close + 1 ;
        }

    });
   
    let r = Object.keys(response)

     r.forEach(key => {
     this.categories.push(response[key]);
     });

   }



}
