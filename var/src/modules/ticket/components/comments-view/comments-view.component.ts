import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ImageDialog } from '../../dialogs/image-dialog/image.dialog';
import { ITicketComment } from '../../models/ticket';
import { TicketService } from '../../services/ticket.service';





@Component({
    selector: 'ticket-comments-view',
    templateUrl: './comments-view.component.html',
    styleUrls: ['../../pages/view/view.page.scss']
})

export class CommentsViewComponent {

    public id: number;
    @Input() comment: ITicketComment;
    @Input() close: number;
    @Output() onClick: EventEmitter<number> = new EventEmitter();


    constructor(private _ticket: TicketService,  private matDialog: MatDialog) { }
    public ngOnInit() {

    }

    delete(id: number): void {
        this._ticket.deleteComment([id]).subscribe(item => {
            if (item) {
                this.onClick.emit(id);
            }
        });
    }


  openImage(url: string, type: string) {
    
    const dialogRef = this.matDialog.open(ImageDialog, {
        
        height: '100%',
        width: '100%',
        
        data: {url, type}
    });
    dialogRef.afterClosed().subscribe((result: any) => {});
  }





}
