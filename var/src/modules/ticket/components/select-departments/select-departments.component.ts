import { Component, OnInit, Input,Output, EventEmitter, SimpleChanges, ViewChild } from '@angular/core';
import { FormControl,  FormGroup} from '@angular/forms';
import { ID_SEPARATOR } from '@angular/localize/src/utils';
import { MatSnackBar } from '@angular/material/snack-bar';

import { TicketService } from '../../services/ticket.service';
import { IDepartment } from '../../models/ticket';
import { SelectResponsiblesComponent } from '../select-responsibles/select-responsibles.component';
@Component({
  selector: 'select-departments',
  templateUrl: './select-departments.component.html',
  styleUrls: ['./select-departments.component.scss']
})
export class SelectDepartmentsComponent implements OnInit {

    @Output() onSelected = new EventEmitter<number>();
	@Input() usersToSelect:boolean;
	@Input() selected: number = null;
	@Input() apparence: boolean = true;
	@Input() id: number;

    @ViewChild(SelectResponsiblesComponent) select: SelectResponsiblesComponent;


	public departments :IDepartment[] = [];
    public departmentsTemp :IDepartment[] = [];
    public showList: boolean;
	public filterForm:FormGroup;


  	constructor(
      	private _snackBar: MatSnackBar,
		private _ticket: TicketService
	) {	
        this.filterForm = new FormGroup({
			id: new FormControl('')
		})

    	
    }

	ngOnInit(): void {
		setTimeout(() =>{
			this.getDepartments();
		}, 500)
	
    }
  
	private getDepartments(){
			this._ticket.getDepartments().subscribe((result: any) => {
                console.log(result)
				if(result) {					
					this.departments = result;
                    this.departmentsTemp = [...this.departments];
                    
                    if(this.id) {
                        this.showList = true;
                        this.filterForm.get('id').setValue(this.id);
                    }
		        	
				} else {            
					this.openSnackBar(result);
				}
			}, error => {
				
			}); 		
	}


    

    
    public filterdepartments(id): void {
        this.showList = true;
        this.departments = this.departmentsTemp.filter(rt => rt.id == id);
    }

  
  	public captureValue() {
        
		let id = this.filterForm.value.id;
		this.onSelected.emit(id);
	}
	
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
	

	public ngOnChanges(changes: SimpleChanges): void {
		if(changes.selected)
			this.filterForm.get('id').setValue(changes.selected.currentValue);
	}

}
