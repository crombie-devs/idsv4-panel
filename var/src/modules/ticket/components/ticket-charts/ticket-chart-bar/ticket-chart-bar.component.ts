import { Component, Input, OnChanges, OnInit, SimpleChanges }   from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType }               from 'chart.js';
import { Label }                                                from 'ng2-charts';

import { Ticket }                                               from 'src/modules/ticket/models/ticket';

@Component({
    selector: 'ticket-chart-bar',
    templateUrl: './ticket-chart-bar.component.html'
})
export class TicketChartBarComponent implements OnChanges, OnInit {

    constructor() { }

    @Input() tickets: Ticket[] = [];
    @Input() set: boolean;

    public barChartOptions: ChartOptions = {
        responsive: true,
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    min: 0,
                } 
            }]
        }
    };

    public barChartLabels: Label[] = ['Tickets'];
    public barChartType: ChartType = 'bar';
    public barChartLegend = true;
    public barChartPlugins = [];
    public barChartData: ChartDataSets[] = [];
   
    ngOnInit() { }

    ngOnChanges(changes: SimpleChanges) {

        if (!this.tickets) {
            return;    
        }

        this.filterByType(this.tickets);
    }

    setValues(tickets): void {
        this.barChartData = tickets;
    }

    filterByType(tickets): void {

        const abiertos = tickets.filter(t => t.id_status != 5).length;
        const cerrados = tickets.filter(t => t.id_status == 5).length;

        this.barChartData = [
            { data: [tickets.length], label: 'Total' },
            { data: [abiertos], label: 'Abiertos' },
            { data: [cerrados], label: 'Cerrados' }
        ];
    }
}
