import { Component, Input, OnChanges, OnInit, SimpleChanges }                           from '@angular/core';
import { ChartType, ChartOptions }                                                      from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip }    from 'ng2-charts';
import { ITicketPie }                                                                   from 'src/modules/ticket/models/ticket';

@Component({
    selector: 'ticket-chart-pie',
    templateUrl: './ticket-chart-pie.component.html'
})
export class TicketChartPieComponent implements OnInit, OnChanges {

    public pieChartOptions: ChartOptions = {
        responsive: true,
    };
    public pieChartLabels: Label[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
    public pieChartData: SingleDataSet = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10];
    public pieChartType: ChartType = 'pie';
    public pieChartLegend = true;
    public pieChartPlugins = [];

    @Input() title: string;
    @Input() dataPie: ITicketPie[];

    constructor() {
        monkeyPatchChartJsTooltip();
        monkeyPatchChartJsLegend();
    }

    ngOnInit() { }

    ngOnChanges(changes: SimpleChanges) {

        if (!this.title || !this.dataPie) {
            return;    
        }

        var dataLabels: string[] = [];
        var dataSet: number[] = [];

        this.dataPie.forEach(t => {
            dataLabels.push(t.name);
            dataSet.push(t.value);
        });
        
        this.pieChartLabels = dataLabels;
        this.pieChartData = dataSet;
    }
}
