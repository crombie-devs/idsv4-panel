import { Component, Input, OnChanges, OnInit, SimpleChanges }   from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType }               from 'chart.js';
import { Label }                                                from 'ng2-charts';

@Component({
    selector: 'ticket-chart-bar-vertical',
    templateUrl: './ticket-chart-bar-vertical.component.html'
})
export class TicketChartBarVerticalComponent implements OnChanges, OnInit {

    constructor() { }

    @Input() tickets: any[] = [];
    @Input() set: boolean;

    public barChartOptions: ChartOptions = {
        responsive: true,
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    max: 100,
                    min: -100
                } 
            }]
        }
    };

    public barChartLabels: Label[] = ['Prioridades'];
    public barChartType: ChartType = 'bar';
    public barChartLegend = true;
    public barChartPlugins = [];
    public barChartData: ChartDataSets[] = [];
   
    ngOnInit() { }

    ngOnChanges(changes: SimpleChanges) {

        if (!this.tickets) {
            return;    
        }

        this.filterByType(this.tickets);
    }

    filterByType(tickets): void {

        this.barChartData = [];

        tickets.forEach(t => {
            this.barChartData.push(
                { data: [t.data], label: t.label }
            );
            // this.barChartData = [
            //     { data: [t.data], label: t.label }, 
            // ];    
        });

    }
}
