export interface ITicketDepartment {
    id: number;
    name: string;
    deleted: number;
}