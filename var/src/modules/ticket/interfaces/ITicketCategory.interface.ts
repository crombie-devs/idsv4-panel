export interface ITicketCategory {
    id: number;
    name: string;
    color: string;
}