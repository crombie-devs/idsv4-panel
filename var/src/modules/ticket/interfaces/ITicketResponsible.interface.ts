export interface ITicketResponsible {
    id: number;
    name: string;
    email: string;
    id_department: number;
    deleted: number;
}