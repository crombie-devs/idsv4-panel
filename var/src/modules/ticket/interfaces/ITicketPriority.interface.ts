export interface ITicketPriority {
    id: number;
    name: string;
    color: string;
    hours_resolution_max: number;
}