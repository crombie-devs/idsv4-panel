export interface ITicketComment {
    id: number;
    message: string;
    readed: number;
}