export interface ITicketStatus {
    id: number;
    name: string;
    color: string;
}