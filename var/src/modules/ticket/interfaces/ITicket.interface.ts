import { ITicketComment } from "./ITicketComment.interface";

export interface ITicket {
    id?: number;
    email?: string;
    phone?: number;
    point_address?: string;
    title: string;
    description?: string;
    created_at?: Date;
    updated_at?: Date;
    id_customer?: number;
    id_category: number;
    id_site?: number;
    id_status?: number;
    id_priority: number;
    id_department: number;
    expected_resolution_date?: number;
    id_responsible?: number;
    notify_user?: boolean;
    date_alert?: Date;
    comments?: ITicketComment[];
    files: File[];
    images: string[];
}