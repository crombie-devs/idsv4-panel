export interface ITicketPie {
    name: string;
    value: number;
}

export interface ITicketList {
    quantity: number;
    name: string;
    tickets_open: number;
    tickets_close: number;
}

export interface IResponsible {
    id?: number;
    name: string;
    email: string;
    department: IDepartment;
    deleted: boolean;
}

export class Responsible {
    id?: number;
    name: string;
    email: string;
    department: IDepartment;
    deleted: boolean;

    constructor(obj?: IResponsible) {
        this.id = obj.id ? obj.id : null;
        this.name = obj.name ? obj.name : null;
        this.email = obj.email ? obj.email : null;
        this.department = obj.department ? obj.department : null;
        this.deleted = obj.deleted ? obj.deleted : false;
    }
}

export interface IDepartment {
    id?: number;
    name: string;
    deleted: boolean;
}

export class Department implements IDepartment {
    id?: number;
    name: string;
    deleted: boolean; 

    constructor(obj?: IDepartment) {
        if(!obj)
        return;

        this.id = obj.id ? obj.id : null;
        this.name = obj.name ? obj.name : null;
        this.deleted = obj.deleted ? obj.deleted: false;
    }
}

export interface ITicketMisc {
    id: number;
    name: string;
    color: string;
} 

export interface ITicketComment {
    id: number;
    mensaje: string;
    files: File[];
    id_status: number;
    notificar_usuario: boolean;
    leido: number;
    images: string[];
    created_at: Date;
    deleted: number;
    create_by: number;
    responsible: Responsible;
}

export class TicketComment implements ITicketComment {
    id: number;
    mensaje: string;
    files: File[];
    id_status: number;
    notificar_usuario: boolean;
    leido: number;
    images: string[];
    created_at: Date;
    deleted: number;
    create_by: number;
    responsible: Responsible;

    constructor(obj?: ITicketComment) {
        if (!obj)
            return;

        this.id = obj.id ? obj.id : 0;
        this.mensaje = obj.mensaje ? obj.mensaje : '';
        this.files = obj.files ? obj.files : [];
        this.id_status = obj.id_status ? obj.id_status : 0;
        this.notificar_usuario = obj.notificar_usuario ? obj.notificar_usuario : false;
        this.leido = obj.leido ? obj.leido : 0;
        this.images = obj.images ? obj.images : [];
        this.created_at = obj.created_at ? obj.created_at : null;
        this.deleted = obj.deleted ? obj.deleted : 1;
        this.create_by = obj.create_by ? this.create_by : null;
        this.responsible = obj.responsible;
    }
}

export interface ITicket {
    id: number;
    titulo: string;
    descripcion: string;
    id_priority: number;
    id_category: number;
    id_site: number;
    id_status: number;
    id_admin: number;
    id_user: number;
    notificar_usuario: boolean;
    finalizado: number;
    fecha_alerta: Date;
    alerta_fin: boolean;
    tiempo_estimado: number;
    create_by_user: number;
    created_at: Date;
    updated_at: Date;
    comments: TicketComment[];
    department: Department;
    responsible: Responsible;
    email: string;
    phone: number;
    deleted: number;
    point_address: string;
    files: File[];
    images: string[];
}

export class Ticket implements ITicket {
    id: number;
    titulo: string;
    descripcion: string;
    id_priority: number;
    id_category: number;
    id_site: number;
    id_status: number;
    id_admin: number;
    id_user: number;
    notificar_usuario: boolean;
    finalizado: number;
    fecha_alerta: Date;
    alerta_fin: boolean;
    tiempo_estimado: number;
    create_by_user: number;
    created_at: Date;
    updated_at: Date;
    comments: TicketComment[];
    department: Department;
    responsible: Responsible;
    email: string;
    phone: number;
    deleted: number;
    point_address: string;
    files: File[];
    images: string[];

    constructor(obj?: ITicket) {
        if (!obj)
            return;

        this.id = obj.id ? obj.id : 0;
        this.titulo = obj.titulo ? obj.titulo : '';
        this.descripcion = obj.descripcion ? obj.descripcion : '';
        this.id_priority = obj.id_priority ? obj.id_priority : 0;
        this.id_category = obj.id_category ? obj.id_category : 0;
        this.id_site = obj.id_site ? obj.id_site : 0;
        this.id_status = obj.id_status ? obj.id_status : 0;
        this.id_admin = obj.id_admin ? obj.id_admin : 0;
        this.id_user = obj.id_user ? obj.id_user : 0;
        this.notificar_usuario = obj.notificar_usuario ? obj.notificar_usuario : false;
        this.finalizado =obj.finalizado ? obj.finalizado : 0;
        this.fecha_alerta = obj.fecha_alerta ? obj.fecha_alerta : null;
        this.alerta_fin = obj.alerta_fin ? obj.alerta_fin : false;
        this.tiempo_estimado = obj.tiempo_estimado ? obj.tiempo_estimado : 0;
        this.create_by_user = obj.create_by_user ? obj.create_by_user : 0;
        this.created_at = obj.created_at ? obj.created_at : null;
        this.updated_at =  obj.updated_at ? obj.updated_at : null;
        this.comments = obj.comments ? obj.comments : [];
        this.department = obj.department ? obj.department : null;
        this.responsible = obj.responsible ? obj.responsible : null;
        this.email = obj.email ? obj.email : null;
        this.phone = obj.phone ? obj.phone : 0;
        this.deleted = obj.deleted ? obj.deleted : 0;
        this.point_address = obj.point_address ? obj.point_address : null;
        this.images = obj.images ? obj.images : null;   
    }
}
