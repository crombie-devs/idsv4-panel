import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import moment from 'moment';
import { ITicketPriority } from '../../interfaces/ITicketPriority.interface';
import { ITicketPie, ITicketList } from '../../models/ticket';
import { TicketService } from '../../services/ticket.service';

const groupBy = function (xs, key) {
    return xs.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
};

@Component({
    selector: 'ticket-dashboard',
    templateUrl: './ticket-dashboard.page.html',
    styleUrls: ['./ticket-dashboard.page.scss']
})
export class TicketDashboardPage implements OnInit, AfterViewInit {

    filterForm = this.fb.group({
        start: [''],
        end: [''],
        customer: ['']
    });

    public dataTickets: any[];
    public dataPriorities: ITicketPriority[];
    
    public dataMapingRes: any[];
    public dataMaping: any[];

    public dataByCustomers: any[];

    public dataPieResponsible: ITicketPie[];
    public dataPieCountry: ITicketPie[];
    public dataPieCustomer: ITicketPie[];

    public dataChartPriorities: any[];

    public dataListCategories: ITicketList[];
    public dataListSites: ITicketList[];

    constructor(
        private _ticket: TicketService,
        private fb: FormBuilder
    ) { }

    ngAfterViewInit() {
        this._ticket.getPriorities().subscribe(
            response => {
                this.dataPriorities = response;
            },
            error => {
                console.error(error);
            }
        );
    }
    
    ngOnInit() { 

        this.initChart();

        this._ticket.getPriorities().subscribe(
            response => {
                this.dataPriorities = response;
            },
            error => {
                console.error(error);
            }
        );

        this._ticket.list().subscribe(tickets => {

            this.dataTickets = tickets;
            
            this.dataMaping = tickets;
            this.dataMapingRes = tickets;
                        
            this.dataMaping.forEach(m => {
                m.id = m.id,
                m.title = m.title,
                m.status = m.status.name,
                m.responsible = (m.responsible) ? m.responsible.name : "No Asignado",
                m.priority_color = (m.priority) ? m.priority.color : "#EDEDED",
                m.priority = (m.priority) ? m.priority.name : "No Asignado",
                m.category = (m.category) ? m.category.name : "No Asignado",
                m.customer = (m.customer) ? m.customer.name : "No Asignado",
                m.country = (m.site) ? m.site.city.provinces.countries.name : "No Asignado",
                m.site = (m.site) ? m.site.name : "No Asignado"
            });

            this.getCustomersSelect(this.dataMaping);
            this.createListChart(this.dataMaping);
            this.createPieCharts(this.dataMaping);
        });
    }

    getCustomersSelect(dataMaping: any[]) {

        var dataCus = [];

        dataMaping.forEach(t => {

            var entryFoundCus = false;

            var tempObjCus = {
                name: t.customer,
                quantity: 1
            };

            for(var item of dataCus) {
                if(item.name === tempObjCus.name) {
                    item.quantity++;
                    entryFoundCus = true;
                    break;
                }
            }

            if(!entryFoundCus){
                dataCus.push(tempObjCus);
            }
        });

        this.dataByCustomers = dataCus;
    }

    createPieCharts(dataMaping: any[]) {

        const responsibles = groupBy(dataMaping, 'responsible');
        const countries = groupBy(dataMaping, 'country');
        const customers = groupBy(dataMaping, 'customer');
        const priorities = groupBy(dataMaping, 'priority');

        const dataMapingRes: ITicketPie[] = [];
        const dataMapingCou: ITicketPie[] = [];
        const dataMapingCus: ITicketPie[] = [];
        const dataMapingPri: any[] = [];

        Object.keys(responsibles).forEach( key => {
            
            let tempObj: ITicketPie = {
                name: key,
                value: responsibles[key].length
            };

            dataMapingRes.push(tempObj);
        });

        this.dataPieResponsible = dataMapingRes;

        Object.keys(countries).forEach( key => {
            
            let tempObj: ITicketPie = {
                name: key,
                value: countries[key].length
            };

            dataMapingCou.push(tempObj);
        });

        this.dataPieCountry = dataMapingCou;

        Object.keys(customers).forEach( key => {
            
            let tempObj: ITicketPie = {
                name: key,
                value: customers[key].length
            };

            dataMapingCus.push(tempObj);
        });

        this.dataPieCustomer = dataMapingCus;

        Object.keys(priorities).forEach( key => {

            let total_hours_resolution: number = 0;

            priorities[key].forEach( t => {
                
                let now = moment().subtract(1, 'years');
                let end = moment();

                if (t.created_at) {
                    now = moment(t.created_at);
                }
                
                if (t.updated_at != '0000-00-00 00:00:00') {
                    end = moment(t.updated_at);
                }

                let duration = moment.duration(end.diff(now));

                let hours = duration.asHours();

                total_hours_resolution += hours;
            });

            let priority = this.dataPriorities.find(p => { return p.name.toLowerCase() == key.toLowerCase(); });
            
            // let tempObj: any = {
            //     label: key,
            //     quantity: priorities[key].length,
            //     hours_resolution: total_hours_resolution,
            //     hours_resolution_max: priority.hours_resolution_max,
            //     percentage: ( ( ( total_hours_resolution / priorities[key].length ) / priority.hours_resolution_max ) * 100 ) - 100,
            // };

            let tempObjChart: any = {
                label: key,
                data: ( ( ( total_hours_resolution / priorities[key].length ) / priority.hours_resolution_max ) * 100 ) - 100,
            };

            dataMapingPri.push(tempObjChart);
        });

        this.dataChartPriorities = dataMapingPri;
    }

    workingDays(dateFrom, dateTo) {
        var from = moment(dateFrom, 'DD/MM/YYY'),
        to = moment(dateTo, 'DD/MM/YYY'),
        days = 0;
          
        while (!from.isAfter(to)) {
          if (from.isoWeekday() !== 6 && from.isoWeekday() !== 7) {
            days++;
          }
          from.add(1, 'days');
        }

        return days;
    }

    createListChart(dataMaping: any[]) {

        var dataCat = [];
        var dataSit = [];
        
        dataMaping.forEach(t => {

            var entryFoundCat = false;
            var entryFoundSit = false;

            var tempObjCat: ITicketList = {
                quantity: 1,
                name: t.category,
                tickets_open: 1,
                tickets_close: 0,
            };

            var tempObjSit: ITicketList = {
                quantity: 1,
                name: t.site,
                tickets_open: 1,
                tickets_close: 0,
            };

            for(var item of dataCat) {
                if(item.name === tempObjCat.name) {
                    item.quantity++;
                    (t.id_status == 5) ? item.tickets_open++ : item.tickets_close++;
                    entryFoundCat = true;
                    break;
                }
            }

            for(var item of dataSit) {
                if(item.name === tempObjSit.name) {
                    item.quantity++;
                    (t.id_status == 5) ? item.tickets_open++ : item.tickets_close++;
                    entryFoundSit = true;
                    break;
                }
            }

            if(!entryFoundCat){
                dataCat.push(tempObjCat);
            }

            if(!entryFoundSit){
                dataSit.push(tempObjSit);
            }
        });

        this.dataListCategories = dataCat;
        this.dataListSites = dataSit;
    }

    onChange(selectValue) {
        this.dataMaping = this.dataMapingRes;
        let filteredData = this.dataMaping.filter(item => { return item.customer.toLowerCase() == selectValue.toLowerCase(); });
        this.dataMaping = filteredData;

        if (this.filterForm.controls.start.value && this.filterForm.controls.end.value) {
            let filteredByDate = this.dataMaping.filter(item => { return ( moment(item.created_at) >= moment(this.filterForm.controls.start.value) && moment(item.created_at) <= moment(this.filterForm.controls.end.value) ) });
            this.dataMaping = filteredByDate;
        }
        
        this.createListChart(this.dataMaping);
        this.createPieCharts(this.dataMaping);
    }

    onSubmit() {

        this.dataMaping = this.dataMapingRes;
        
        if (this.filterForm.controls.customer.value && this.filterForm.controls.customer.value.toLowerCase() != 'todos') {

            let filteredData = this.dataMaping.filter(item => { 
                return item.customer.toLowerCase() == this.filterForm.controls.customer.value.toLowerCase(); 
            });
    
            this.dataMaping = filteredData;
        } else {
            this.initChart();
        }

        if (this.filterForm.controls.start.value && this.filterForm.controls.end.value) {

            let filteredByDate = this.dataMaping.filter(item => { return ( moment(item.created_at) >= moment(this.filterForm.controls.start.value) && moment(item.created_at) <= moment(this.filterForm.controls.end.value) ) });
            
            this.dataMaping = filteredByDate;
        }

        this.createListChart(this.dataMaping);
        this.createPieCharts(this.dataMaping);
    }

    onCleanFilter() {
        this.filterForm.reset();
    }

    initChart() {
        let tempObjChartPrototipe: any[] = [
            { label: 'Critica', data: 80 },
            { label: 'Alta', data: -30 },
            { label: 'Media', data: 25 },
            { label: 'Baja', data: 98 }
        ];

        this.dataChartPriorities = tempObjChartPrototipe;
    }
}
