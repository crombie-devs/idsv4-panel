import { Component, OnInit } from '@angular/core';
import { Location, PopStateEvent } from "@angular/common";
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, NavigationStart, NavigationEnd } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ImageDialog } from 'src/modules/ticket/dialogs/image-dialog/image.dialog';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular';

import moment from 'moment';

import { ITicket } from 'src/modules/ticket/interfaces/ITicket.interface';
import { ITicketCategory } from 'src/modules/ticket/interfaces/ITicketCategory.interface';
import { ITicketComment } from 'src/modules/ticket/interfaces/ITicketComment.interface';
import { ITicketDepartment } from 'src/modules/ticket/interfaces/ITicketDepartment.interface';
import { ITicketPriority } from 'src/modules/ticket/interfaces/ITicketPriority.interface';
import { ITicketResponsible } from 'src/modules/ticket/interfaces/ITicketResponsible.interface';
import { ITicketStatus } from 'src/modules/ticket/interfaces/ITicketStatus.interface';
import { TicketService } from 'src/services/ticket.service';

@Component({
    selector: 'ticket-edit',
    templateUrl: './ticket-edit.page.html',
    styleUrls: ['./ticket-edit.page.scss']
})
export class TicketEditPage implements OnInit {

    private lastPoppedUrl: string;
    private yScrollStack: number[] = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private fb: FormBuilder,
        private _ticketService: TicketService,
        private _snackBar: MatSnackBar,
        private matDialog: MatDialog
    ) { }

    ticketId: number;
    statusTicket: string = 'open';

    now = Date.now();

    // description text area.
    sHtml: string;
    cHtml: string;
    editor = ClassicEditor;

    // foreach selects lists.
    customers: any[];
    categories: ITicketCategory[];
    sites: any[];
    sites_resg: any[];
    status: ITicketStatus[];
    priorities: ITicketPriority[];
    departments: ITicketDepartment[];
    responsibles: ITicketResponsible[];
    responsibles_resg: ITicketResponsible[];
    comments: ITicketComment[] = [];
    images: any[] = [];
    files: File[] = [];

    isHiddenUpdatedAt: boolean = true;

    ticketForm = this.fb.group({
        id: [null],
        email: [null, [Validators.email]],
        phone: [null],
        point_address: [null],
        title: [null, Validators.required],
        description: [null],
        created_at: [{ value: null, readonly: true }],
        updated_at: [{ value: null, readonly: true }],
        id_customer: [null],
        id_category: [null, Validators.required],
        id_site: [null],
        id_status: [null],
        id_priority: [null, Validators.required],
        id_department: [null, Validators.required],
        expected_resolution_date: [null],
        id_responsible: [null, Validators.required],
        notify_user: [null],
        date_alert: [null],
        comments: this.fb.array([]),
        images: this.fb.array([]),
    });
    commentForm = this.fb.group({
        message: [null, Validators.required],
        readed: [null],
    });

    ngOnInit() {
        this.location.subscribe((ev: PopStateEvent) => {
            this.lastPoppedUrl = ev.url;
        });
        this.router.events.subscribe((ev: any) => {
            if (ev instanceof NavigationStart) {
                if (ev.url != this.lastPoppedUrl)
                    this.yScrollStack.push(window.scrollY);
            } else if (ev instanceof NavigationEnd) {
                if (ev.url == this.lastPoppedUrl) {
                    this.lastPoppedUrl = undefined;
                    window.scrollTo(0, this.yScrollStack.pop());
                } else
                    window.scrollTo(0, 0);
            }
        });
        this._ticketService.getCustomers().subscribe(
            response => {
                this.customers = response;
            },
            error => {
                console.error(error);
            }
        );
        this._ticketService.getCategories().subscribe(
            response => {
                this.categories = response;
            },
            error => {
                console.error(error);
            }
        );
        this._ticketService.getSites().subscribe(
            response => {
                this.sites = response;
                this.sites_resg = response;
            },
            error => {
                console.error(error);
            }
        );
        this._ticketService.getStatus().subscribe(
            response => {
                this.status = response;
            },
            error => {
                console.error(error);
            }
        );
        this._ticketService.getPriorities().subscribe(
            response => {
                this.priorities = response;
            },
            error => {
                console.error(error);
            }
        );
        this._ticketService.getDepartments().subscribe(
            response => {
                this.departments = response;
            },
            error => {
                console.error(error);
            }
        );
        this._ticketService.getResponsibles().subscribe(
            response => {
                this.responsibles = response;
                this.responsibles_resg = response;
            },
            error => {
                console.error(error);
            }
        );
        this.route.params.subscribe(
            params => {
                this.ticketId = +params['id'];
                let args: any = {
                    column: 'id',
                    condition: this.ticketId
                };
                this._ticketService.getTickets(args).subscribe(
                    response => {
                        if (response['data']) {
                            let ticket: ITicket = response['data'][0];
                            this.ticketForm.patchValue({
                                id: ticket.id,
                                email: ticket.email,
                                phone: ticket.phone,
                                point_address: ticket.point_address,
                                title: ticket.title,
                                description: ticket.description,
                                created_at: ticket.created_at,
                                updated_at: ticket.updated_at,
                                id_customer: ticket.id_customer,
                                id_category: ticket.id_category,
                                id_site: ticket.id_site,
                                id_status: ticket.id_status,
                                id_priority: ticket.id_priority,
                                id_department: ticket.id_department,
                                expected_resolution_date: ticket.expected_resolution_date,
                                id_responsible: ticket.id_responsible,
                                notify_user: ticket.notify_user,
                                date_alert: moment(ticket.date_alert).format("DD-MM-YYYY hh:mm A"),
                                comments: ticket.comments,
                                images: ticket.images
                            });
                            if (ticket.id_site == 0) {
                                this.ticketForm.patchValue({id_site: '0'});
                            }
                            if (ticket.id_customer == 0) {
                                this.ticketForm.patchValue({id_customer: '0'});
                            }
                            this.isHiddenUpdatedAt = (ticket.id_status == 5) ? true : false;
                            this.statusTicket = (ticket.id_status == 5) ? "close" : "open";
                            if (ticket.id_status == 5) {
                                this.ticketForm.controls['email'].disable();
                                this.ticketForm.controls['phone'].disable();
                                this.ticketForm.controls['point_address'].disable();
                                this.ticketForm.controls['title'].disable();
                                this.ticketForm.controls['id_customer'].disable();
                                this.ticketForm.controls['id_category'].disable();
                                this.ticketForm.controls['id_site'].disable();
                                this.ticketForm.controls['id_status'].disable();
                                this.ticketForm.controls['id_priority'].disable();
                                this.ticketForm.controls['id_department'].disable();
                                this.ticketForm.controls['expected_resolution_date'].disable();
                                this.ticketForm.controls['id_responsible'].disable();
                                this.ticketForm.controls['notify_user'].disable();
                                this.ticketForm.controls['date_alert'].disable();
                                this.ticketForm.controls['date_alert'].disable();
                            }
                            if (ticket.hasOwnProperty('images')) {
                                if (ticket.images.length > 0) {
                                    this.images = ticket.images;
                                }
                            }
                            this.sHtml = ticket.description;
                            this.comments = ticket.comments;
                        }
                    },
                    error => {
                        console.error(error);
                    }
                );
            }
        );
    }

    onSubmit() {
        if (this.ticketForm.valid) {
            this.ticketForm.value.comments = this.comments;
            if (this.ticketForm.value.id_status == 5) {
                this.ticketForm.value.updated_at = moment(this.now).format("DD-MM-YYYY hh:mm A");
            }
            if (this.files.length > 0) {
                this.ticketForm.value.files = this.files;
            }
            if (this.ticketForm.value.date_alert) {
                this.ticketForm.value.date_alert = this.ticketForm.value.date_alert;
            } else {
                this.ticketForm.value.date_alert = moment(this.now).format("DD-MM-YYYY hh:mm A");
            }
            Object.keys(this.ticketForm.value).forEach((key) => (this.ticketForm.value[key] == null) && delete this.ticketForm.value[key]);
            this._ticketService.update(this.ticketForm.value).subscribe(
                response => {
                    this.router.navigate(['/private/ticket/dashboard']);
                    this.openSnackBar("Se guardaron los cambios correctamente!");
                },
                error => {
                    console.error(error);
                }
            );
        }
    }

    getFiles(events): void {
        this.files = events
    }

    // description text area function.
    onChange({ editor }: ChangeEvent) {
        const data = editor.getData();
        this.ticketForm.patchValue({ description: data });
    }

    onChangePriority(priority_id: number) {

        let asignPriority: ITicketPriority = this.priorities.find(p => { return p.id == priority_id; });

        this.ticketForm.patchValue({ expected_resolution_date: asignPriority.hours_resolution_max });
    }

    addNewComment(commentForm: any) {

        let comment: any = {
            deleted: 0,
            id_admin: 0,
            id_created_by: 1,
            id_ticket: this.ticketId,
            images: [],
            leido: 0,
            mensaje: commentForm.message,
            notificar_usuario: 1,
            created_at: moment(this.now),
            updated_at: moment(this.now),
            user: []
        }

        this.comments.push(comment);

        this.commentForm.reset();
    }

    onDeletedComment(comment_index: any) {

        this.comments.splice(comment_index, 1);
    }

    openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
            panelClass: ['green-snackbar']
        });
    }

    onCloseTicket() {
        if (this.ticketForm.value.id_status == 5) {
            this.openSnackBar("Este ticket ya se encuentra cerrado!");
        } else {
            this.ticketForm.value.id_status = 5;
            this.onSubmit();
        }
    }

    onChangeDepartment(department_id: number) {

        this.responsibles = this.responsibles_resg;

        this.responsibles = this.responsibles.filter(r => r.id_department == department_id);
    }

    onChangeCustomer(customer_id: number) {

        this.sites = this.sites_resg;

        this.sites = this.sites.filter(s => s.idcustomer == customer_id);
    }

    openImage(url: string, type: string) {

        const dialogRef = this.matDialog.open(ImageDialog, {
            height: '100%',
            width: '100%',
            data: { url, type }
        });
        dialogRef.afterClosed().subscribe((result: any) => { });
    }

    onDeletedImages() { 
        this._ticketService.deletedImages(this.ticketId).subscribe(
            response => {
                this.router.navigate(['/private/ticket/dashboard']);
                this.openSnackBar("Se eliminaron las imagenes correctamente!");
            },
            error => {
                console.error(error);
            }
        );
    }

}