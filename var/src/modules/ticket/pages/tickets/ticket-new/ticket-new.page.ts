import { Component, OnInit } from '@angular/core';
import { Location, PopStateEvent } from "@angular/common";
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, NavigationStart, NavigationEnd  } from '@angular/router';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular';

import moment from 'moment';

import { ITicket } from 'src/modules/ticket/interfaces/ITicket.interface';
import { ITicketCategory } from 'src/modules/ticket/interfaces/ITicketCategory.interface';
import { ITicketComment } from 'src/modules/ticket/interfaces/ITicketComment.interface';
import { ITicketDepartment } from 'src/modules/ticket/interfaces/ITicketDepartment.interface';
import { ITicketPriority } from 'src/modules/ticket/interfaces/ITicketPriority.interface';
import { ITicketResponsible } from 'src/modules/ticket/interfaces/ITicketResponsible.interface';
import { ITicketStatus } from 'src/modules/ticket/interfaces/ITicketStatus.interface';
import { TicketService } from 'src/services/ticket.service';

@Component({
    selector: 'ticket-new',
    templateUrl: './ticket-new.page.html',
    styleUrls: ['./ticket-new.page.scss']
})
export class TicketNewPage implements OnInit {

    private lastPoppedUrl: string;
    private yScrollStack: number[] = [];

    constructor(
        private router: Router,
        private location: Location,
        private fb: FormBuilder,
        private _ticketService: TicketService,
        private _snackBar: MatSnackBar
    ) { }

    ticketId: number;

    now = Date.now();

    // description text area.
    sHtml: string;
    editor = ClassicEditor;

    // foreach selects lists.
    customers: any[];
    categories: ITicketCategory[];
    sites: any[];
    sites_resg: any[];
    status: ITicketStatus[];
    priorities: ITicketPriority[];
    departments: ITicketDepartment[];
    responsibles: ITicketResponsible[];
    responsibles_resg: ITicketResponsible[];
    comments: ITicketComment[] = [];
    images: any[] = [];
    files: File[] = [];
    public today = moment().format('YYYY-MM-DD');
    ticketForm = this.fb.group({
        email: [null, [Validators.email]],
        phone: [null],
        point_address: [null],
        title: [null, Validators.required],
        description: [null],
        created_at: [{value: null, disabled: true}],
        id_customer: [null],
        id_category: [null, Validators.required],
        id_site: [null],
        id_status: [null],
        id_priority: [null, Validators.required],
        id_department: [null, Validators.required],
        expected_resolution_date: [null],
        id_responsible: [null, Validators.required],
        notify_user: [null],
        date_alert: [null],
        comments: this.fb.array([]),
        images: this.fb.array([]),
    });

    ngOnInit() {
        this.location.subscribe((ev:PopStateEvent) => {
            this.lastPoppedUrl = ev.url;
        });
        this.router.events.subscribe((ev:any) => {
            if (ev instanceof NavigationStart) {
                if (ev.url != this.lastPoppedUrl)
                    this.yScrollStack.push(window.scrollY);
            } else if (ev instanceof NavigationEnd) {
                if (ev.url == this.lastPoppedUrl) {
                    this.lastPoppedUrl = undefined;
                    window.scrollTo(0, this.yScrollStack.pop());
                } else
                    window.scrollTo(0, 0);
            }
        });
        this._ticketService.getCustomers().subscribe(
            response => {
                this.customers = response;
            },
            error => {
                console.error(error);
            }
        );
        this._ticketService.getCategories().subscribe(
            response => {
                this.categories = response;
            },
            error => {
                console.error(error);
            }
        );
        this._ticketService.getSites().subscribe(
            response => {
                this.sites = response;
                this.sites_resg = response;
            },
            error => {
                console.error(error);
            }
        );
        this._ticketService.getStatus().subscribe(
            response => {
                this.status = response;
            },
            error => {
                console.error(error);
            }
        );
        this._ticketService.getPriorities().subscribe(
            response => {
                this.priorities = response;
            },
            error => {
                console.error(error);
            }
        );
        this._ticketService.getDepartments().subscribe(
            response => {
                this.departments = response;
            },
            error => {
                console.error(error);
            }
        );
        this._ticketService.getResponsibles().subscribe(
            response => {
                this.responsibles = response;
                this.responsibles_resg = response;
            },
            error => {
                console.error(error);
            }
        );
        this.ticketForm.patchValue({
            id_customer: '0',
            id_site: '0',
            id_status: 1,
            created_at: moment(this.now).format("DD-MM-YYYY hh:mm A")
        });
    }
    onSubmit() {
        if (this.ticketForm.valid) {
            this.ticketForm.value.comments = this.comments;
            this.ticketForm.value.created_at = moment(this.now).format("DD-MM-YYYY hh:mm A");
            if (this.ticketForm.value.id_status == 5) {
                this.ticketForm.value.updated_at = moment(this.now).format("DD-MM-YYYY hh:mm A");
            }
            if(this.files.length > 0) {
                this.ticketForm.value.files = this.files;
            }
            if(this.ticketForm.value.date_alert) {
                this.ticketForm.value.date_alert = this.ticketForm.value.date_alert.format("DD-MM-YYYY hh:mm A");
            }
            Object.keys(this.ticketForm.value).forEach((key) => (this.ticketForm.value[key] == null) && delete this.ticketForm.value[key]);
            this._ticketService.create(this.ticketForm.value).subscribe(
                response => {
                    this.router.navigate(['/private/ticket/dashboard']);
                    this.openSnackBar("Se guardaron los cambios correctamente!");
                },
                error => {
                    console.error(error);
                }
            );
        }
    }

    getFiles(events): void {
        this.files = events       
    }

    // description text area function.
    onChange({ editor }: ChangeEvent) {
        const data = editor.getData();
        this.ticketForm.patchValue({ description: data });
    }

    onChangePriority(priority_id: number) {
        let asignPriority: ITicketPriority = this.priorities.find(p => { return p.id == priority_id; });
        this.ticketForm.patchValue({ expected_resolution_date: asignPriority.hours_resolution_max });
    }

    onChangeCustomer(customer_id: number) {
        this.sites = this.sites_resg;
        this.sites = this.sites.filter(s => s.idcustomer == customer_id);
    }

    openSnackBar(message: string) {
        this._snackBar.open(message,'', {
          duration: 1500,
          panelClass: ['green-snackbar']
        });
    }

    onChangeDepartment(department_id: number) {
        this.responsibles = this.responsibles_resg;
        this.responsibles = this.responsibles.filter(r => r.id_department == department_id);
    }

}

