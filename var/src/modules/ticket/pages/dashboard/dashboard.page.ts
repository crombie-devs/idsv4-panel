import { Component, ViewChild} from '@angular/core';
import { StatsTicketComponent } from '../../components/stats-ticket/stats-ticket.component';
import { StatsTypeComponent } from '../../components/stats-type/stats-type.component';
import {  Ticket } from '../../models/ticket';
import { TicketService } from '../../services/ticket.service';

@Component({
    selector: 'ticket-dashboard',
    templateUrl: './dashboard.page.html',
    styleUrls: ['./dashboard.page.scss']
})

export class DashboardPage {
   
   all: number = 0;
   open: number = 0;;
   close: number = 0;
   commemts_not_read: number = 0;
   tickets: Ticket[]= [];
   set: boolean = true;
   
   @ViewChild(StatsTypeComponent) type: StatsTypeComponent; 
   @ViewChild('date') date: StatsTicketComponent;

   constructor( private _ticket: TicketService) { }
    public ngOnInit() {                        
        this._ticket.list([]).subscribe((tickets: Ticket[]) => {
            if (tickets) {
                   this.tickets = tickets;
                   this.all = tickets.length;
                   this.open = tickets.filter(tk => tk.finalizado == 1).length;
                   this.close = tickets.filter(tk => tk.finalizado == 0).length;
                    tickets.forEach(tk => {
                        this.commemts_not_read = this.commemts_not_read + tk.comments.filter(t => t.leido == 0).length;
                   }); 

                }
        });
    }



    getDate(event): void {
        this.date.setValues(event);
    }


    getCustomer(event): void {
       
       let users = this.tickets.filter(tk => event.some(ev => ev.iduser == tk.id_user)
       ).map((tk: any) =>  { tk.user = event.filter(e => e.iduser == tk.id_user)[0]; return tk } ) 
       this.type.setValues(users);
    }

     
}
