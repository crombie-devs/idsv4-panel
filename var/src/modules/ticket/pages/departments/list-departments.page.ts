import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Sort, MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SelectionModel } from '@angular/cdk/collections';
import { MYString } from 'src/libs/string.libs';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { DebugStore } from 'src/stores/debug.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatCheckbox } from '@angular/material/checkbox';
import { User } from 'src/modules/user/models/user';
import { MatInput } from '@angular/material/input';
import { IDepartment } from '../../models/ticket';
import { TicketService } from '../../services/ticket.service';
import { MatDialog } from '@angular/material/dialog';
import { DepartmentDialog } from '../../dialogs/department-dialog/department.dialog';



@Component({
    selector: 'list-departments',
    templateUrl: './list-departments.page.html',
    styleUrls: ['../list/list.page.scss']
})

export class DepartmentsListPage {

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChildren('table') table: QueryList<MatCheckbox>
    @ViewChild(MatInput) input: MatInput;


    public displayedColumns: string[] = ['select', 'id', 'name'];
    public selection = new SelectionModel<IDepartment>(true, []);
    public dataSource = new MatTableDataSource<IDepartment>([]);
    public new: boolean = false;
    public edit: boolean = false;
    public deleted: boolean = false;
    public permission: boolean = false;
    public load: boolean = true;

    private sortedData: IDepartment[] = [];
    private misc: IDepartment[] = [];
    public me: User;
    private data: IDepartment[] = [];

    constructor(
        private _ticket: TicketService,       
        private dialog: DialogService,
        private debug: DebugStore,
        private ustore: UserStore,
        private snackBar: MatSnackBar,
        private matDialog: MatDialog) { }

    public ngOnInit() {
        this.debug.addInfo('DepartmentListComponent', 'OnInit');
        this.verifyPermissions();
        this.getDepartments();
    }


    private create(data?:  IDepartment): void {
        const val = data ? data : [];
        const dialogRef = this.matDialog.open(DepartmentDialog, { data: { data: val, type: 'department' }, });
        dialogRef.afterClosed().subscribe((result: IDepartment | null) => {
            if (result) {
                //console.log(result);
                this._ticket.saveDepartment(result).subscribe(item => {
                    if (!result.id) {
                        this.data.unshift(item)
                        this.dataSource.data = this.data;                  
                    } else {
                        this.selection.clear();
                        const objIndex = this.dataSource.data.findIndex((obj => obj.id == item.id));
                        this.dataSource.data[objIndex] = item;
                        this.dataSource.data = this.dataSource.data;
                    }
                });
            }
        });
    }


    private getDepartments() {
        this._ticket.getDepartments().subscribe((departments: IDepartment[]) => {
            this.permission = true;
            this.data = departments;
            this.renderDataTable(departments);
            this.load = false;
        }, error => {
            this.load = false;
            return null;
        });
    }

    public renderDataTable(data:  IDepartment[]) {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.filterPredicate = (data:  IDepartment, filter: string): boolean => {
            const dataStr = MYString.removeAccents(Object.keys(data).reduce((currentTerm: string, key: string) => {
                return (currentTerm + (data as { [key: string]: any })[key] + '◬');
            }, '')).trim().toLowerCase();
            return dataStr.indexOf(MYString.removeAccents(filter.trim().toLowerCase())) != -1;
        };

        setTimeout(() => {
            this.dataSource.paginator = this.paginator;
        }, 500);

      
          

        this.sortedData = data.slice();
        this.misc = data;
        this.dataSource.sort = this.sort;
        this.load = false;
    }

    isAllSelected() {
        if (this.dataSource.paginator) {
            const numSelected = this.selection.selected.length;
            const page = this.dataSource.paginator.pageSize;
            let endIndex: number;
            if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
                endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
            } else {
                endIndex = this.dataSource.data.length - (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize);
            }
            return numSelected === endIndex;
        }
    }

    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() : this.selectRows();
    }

    selectRows() {
        let endIndex: number;
        if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
            endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
        } else {
            endIndex = this.dataSource.data.length;
        }
        for (let index = (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize); index < endIndex; index++) {
            this.selection.select(this.dataSource.data[index]);
        }
    }

    public checkboxLabel(row?: IDepartment): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
    }

    public delete() {
        let departmentByDelete: number[] = [];
        this.table.toArray().forEach(item => {
            console.log(item)
            if (item.checked)
                departmentByDelete.push(parseInt(item.value));
        });
        if (departmentByDelete.length == 0) {
            this.dialog.warning('Aviso', 'Debe seleccionar al menos un centro').subscribe();
        } else {
            this.dialog.confirm('Confirmar Borrado', '¿Desea borrar los elementos seleccionados?').subscribe(confirm => {
                if (confirm) {
                     console.log(departmentByDelete)
                    this._ticket.deleteDepartment(departmentByDelete).subscribe(categories => {
                        const results =  this.dataSource.data.filter(({ id: id1 }) => !departmentByDelete.some(( value ) =>value === id1));
                        this.dataSource.data = results;                       
                   })
                }
            });
        }
    }

    public update() {
        if (this.selection.selected.length > 1) {
            this.dialog.warning('Aviso', 'Sólo puede editar un centro').subscribe();
        } else if (this.selection.selected.length == 0) {
            this.dialog.warning('Aviso', 'Debe seleccionar un centro').subscribe();
        } else {
            this.create(this.selection.selected[0]);
        }
    }

    public openSnackBar(message: string) {
        this.snackBar.open(message, '', {
            duration: 1500,
        });
    }

 

    private verifyPermissions() {
        this.debug.addInfo('SiteListComponent', 'verifyPermissions');
        this.new = this.ustore.hasPermissionByName("post.site");
        this.edit = this.ustore.hasPermissionByName("put.site.*");
        this.deleted = this.ustore.hasPermissionByName("delete.site.*");
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.dataSource.paginator = this.paginator;
        }, 500)

        this.dataSource.sort = this.sort;

    }

    sortData(sort: Sort) {
        const data = this.misc.slice();
        if (!sort.active || sort.direction === '') {
            this.sortedData = data;
            return;
        }

        this.sortedData = data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'id': return this.compare(a.id, b.id, isAsc);
                case 'name': return this.compare(a.name ? a.name : '', b.name ? b.name : '', isAsc);
                default: return 0;
            }
        });
        this.dataSource.data = this.sortedData;
    }

    compare(a: number | string, b: number | string, isAsc: boolean) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }


}
