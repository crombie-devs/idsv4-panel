import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Ticket } from '../../models/ticket';
import { TicketService } from '../../services/ticket.service';
import moment from 'moment';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent, CKEditorComponent } from '@ckeditor/ckeditor5-angular';
import { UserStore } from '../../../user/stores/user.store';
import { TicketSelectComponent } from '../../components/select-categories/select.component';
import { SelectResponsiblesComponent } from '../../components/select-responsibles/select-responsibles.component';
import { SiteSelectComponent } from 'src/modules/site/components/select/select.component';
import { MatDialog } from '@angular/material/dialog';
import { ImageDialog } from '../../dialogs/image-dialog/image.dialog';
import { CustomerSelectComponent } from 'src/modules/customer/components/select/select.component';





@Component({
    selector: 'ticket-form',
    templateUrl: './form.page.html',
    styleUrls: ['./form.page.scss', '../../pages/view/view.page.scss']
})

export class TicketFormPage {
    public form: FormGroup;
    public id: number;
    public categories: any[] = [];
    public priorities: any[] = [];
    public status: any[] = [];
    public sHtml: string;
    public users: any[] = [];
    loading: boolean;


    aperance: string = 'outline';
    userSelect: number;
    usersToSelect: boolean = true;
    fecha_alerta = [new Date()];
    @ViewChild(TicketSelectComponent) select: TicketSelectComponent;
    @ViewChild(SelectResponsiblesComponent) selectResponsible: SelectResponsiblesComponent;
    @ViewChild(SiteSelectComponent) selectSite: SiteSelectComponent;
    @ViewChild(CustomerSelectComponent) selectCustomer: CustomerSelectComponent;

    id_cat: number;
    id_sta: number;
    id_pri: number;
    id_site: number;
    iddepartment: number;
    idresponsible: number;
    id_user: number;
    idcustomer: number;
    images: any[] = [];
    files: File[] = [];

    public Editor = ClassicEditor;
    @ViewChild(CKEditorComponent) ckeditor: CKEditorComponent;


    constructor(private route: ActivatedRoute, private _ticket: TicketService, private matDialog: MatDialog,
        private user: UserStore, private router: Router) {
        this.id_user = this.user.getUser().iduser;
        this.form = new FormGroup({
            id: new FormControl(null),
            titulo: new FormControl('', [Validators.required]),
            descripcion: new FormControl('', [Validators.required]),
            id_priority: new FormControl(null, [Validators.required]),
            id_category: new FormControl(null, [Validators.required]),
            id_site: new FormControl(null, [Validators.required]),
            id_status: new FormControl(null, [Validators.required]),
            id_admin: new FormControl(null),
            id_user: new FormControl(null, [Validators.required]),
            notificar_usuario: new FormControl(0),
            finalizado: new FormControl(1),
            fecha_alerta: new FormControl(new Date()),
            alerta_fin: new FormControl(null),
            tiempo_estimado: new FormControl(0),
            create_by_user: new FormControl(this.id_user),
            iddepartment: new FormControl(null, [Validators.required]),
            idresponsible: new FormControl(null, [Validators.required]),
            email: new FormControl('', [Validators.required, Validators.email]),
            phone: new FormControl(0),
            point_address: new FormControl(''),

        });
    }



    public ngOnInit() {
        this.route.params.subscribe(param => {
            if (param.id) {
                this.id = param.id;
                this._ticket.get(param.id).subscribe(tickets => {
                    this.selectSite.setValue(tickets.id_site);
                    this.selectCustomer.setValue(tickets.id_user);
                    if (tickets.hasOwnProperty('images')) {

                        if (tickets.images.length > 0) {
                            this.images = tickets.images;
                        }

                    }

                    this.initForm(tickets);
                });
            }
        });
    }

    openImage(url: string, type: string) {

        const dialogRef = this.matDialog.open(ImageDialog, {
            height: '100%',
            width: '100%',
            data: { url, type }
        });
        dialogRef.afterClosed().subscribe((result: any) => { });
    }




    public onChange({ editor }: ChangeEvent) {
        const data = editor.getData();
        this.form.patchValue({ descripcion: data });
    }

    userSelected(event) {
        this.selectSite.filterSite(event);
        this.form.patchValue({ id_user: event });
        this.form.patchValue({ id_admin: event });
    }



    getFiles(events): void {
        this.files = events
    }

    setValues(event, type) {
        if (typeof event == 'object') {
            event = event.idsite;
        }


        if (type == 'iddepartment') {
            this.selectResponsible.filterResponsibles(event);
        }
        this.form.patchValue({ [type]: event });
    }

    initForm(data: Ticket) {
        this.form.setValue({
            titulo: data.titulo,
            descripcion: data.descripcion,
            id_priority: data.id_priority,
            id_category: data.id_category,
            id_site: data.id_site,
            id_status: data.id_status,
            id_admin: data.id_admin,
            id_user: data.id_user,
            notificar_usuario: data.notificar_usuario,
            finalizado: data.finalizado,
            fecha_alerta: data.fecha_alerta ? data.fecha_alerta : new Date(),
            tiempo_estimado: data.tiempo_estimado,
            create_by_user: data.create_by_user ? data.create_by_user : this.id_user,
            alerta_fin: data.alerta_fin,
            id: data.id,
            iddepartment: data.department ? data.department.iddepartment : 0,
            idresponsible: data.responsible ? data.responsible.idresponsible : 0,
            email: data.email ? data.email : '',
            phone: data.phone ? data.phone : 0,
            point_address: data.point_address ? data.point_address : '',

        });

        this.id_cat = data.id_category;
        this.id_pri = data.id_priority;
        this.id_sta = data.id_status;
        this.iddepartment = data.department ? data.department.iddepartment : 0;
        this.idresponsible = data.responsible ? data.responsible.idresponsible : 0;
        this.userSelect = data.id_user;
        this.sHtml = data.descripcion;
    }


    create(): void {
        this.loading = true;

        const date = moment(this.form.value.fecha_alerta).format('YYYY-MM-DD') ? moment(this.form.value.fecha_alerta).format('YYYY-MM-DD') : new Date();
        this.form.patchValue({ fecha_alerta: date });
        if (this.form.valid) {

            if (this.files.length > 0) {
                this.form.value.files = this.files;
            }

            this._ticket.save(this.form.value).subscribe(item => {
                this.loading = false;
                if (item) {
                    this.router.navigate(['/private/ticket/list/1']);
                }
            });
        }
    }

}
