import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ITicketComment, Ticket } from '../../models/ticket';
import { TicketService } from '../../services/ticket.service';
import { UserStore } from '../../../user/stores/user.store';
import { CommentComponent } from '../../components/comments/comment.component';
import { MatSnackBar } from '@angular/material/snack-bar';




@Component({
    selector: 'ticket-view',
    templateUrl: './view.page.html',
    styleUrls: ['./view.page.scss']
})

export class ViewPage {

    public id: number;
    public ticket: Ticket;
    public comments: ITicketComment[] = [];
    public images: any[]=[];

    @ViewChild(CommentComponent) comment: CommentComponent;

    constructor(private route: ActivatedRoute, private _ticket: TicketService,private _snackBar: MatSnackBar) { }
    public ngOnInit() {
        this.route.params.subscribe(param => {
            if (param.id) {
                this.id = param.id;
                this._ticket.get(param.id).subscribe(tickets => {
                    this.ticket = tickets;

                    if(this.ticket.hasOwnProperty('images')) {
                        if(this.ticket.images.length > 0) {
                            this.images = this.ticket.images;
                        }
                    }


                    if (this.ticket.hasOwnProperty('comments')) {
                        if (this.ticket.comments.length > 0) {
                            this.comments = this.ticket.comments;

                        }
                    }

                });
            }
        });
    }


    closeTicket(): void {

        this.ticket.finalizado = this.ticket.finalizado == 1 ? 0 : 1;

        this._ticket.closeTicket(this.ticket.id).subscribe(item => {
        
            this.openSnackBar("El ticket se cerro con existo");
        });
    }

    setComment(event): void {
        this.comment.clear();
        let c = [...this.comments];
        c.push(event);
        this.comments = c;
    }


    deleteComment(id: number) {
        this.comments = this.comments.filter(co => co.id != id);
    }

    public openSnackBar(message: string) {
        this._snackBar.open(message,'', {
          duration: 1500,
          panelClass: ['green-snackbar']
        });
      }

}
