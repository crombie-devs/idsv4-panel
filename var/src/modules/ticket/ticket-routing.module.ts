import { NgModule } from '@angular/core';
import { _MatTabLinkBase } from '@angular/material/tabs';
import { Routes, RouterModule } from '@angular/router';

import { CategoryListPage } from './pages/category/list-category.page';
import { PriorityListPage } from './pages/priority/list-priority.page';
import { StatusListPage } from './pages/status/list-status.page';

import { DepartmentsListPage } from './pages/departments/list-departments.page';
import { ResponsibleListPage } from './pages/responsible/list-responsible.page';

import { TicketDashboardPage } from './pages/ticket-dashboard/ticket-dashboard.page';

import { TicketEditPage } from './pages/tickets/ticket-edit/ticket-edit.page';
import { TicketNewPage } from './pages/tickets/ticket-new/ticket-new.page';

const routes: Routes = [
  { path: '', component: TicketDashboardPage, data: { breadcrumb: 'Listado de tickets' }},

  { path: 'dashboard', component: TicketDashboardPage, data: {breadcrumb: 'Dashboard tickets'}},

  { path: 'create', component:  TicketNewPage, data: { breadcrumb: 'Crear Ticket' }},
  
  { path: 'edit/:id', component:  TicketEditPage, data: { breadcrumb: 'Editar Ticket' }},

  { path: 'category', component:  CategoryListPage, data: { breadcrumb: 'Crear categoria' }},
  { path: 'department', component: DepartmentsListPage, data: { breadcrumb: 'Crear departamento' }},
  { path: 'responsible', component: ResponsibleListPage, data: { breadcrumb: 'Crear responsble' }},
  { path: 'priority', component:  PriorityListPage, data: { breadcrumb: 'Crear prioridad' }},  
  { path: 'status', component:  StatusListPage, data: { breadcrumb: 'Crear Status' }},
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TicketRoutingModule { }
