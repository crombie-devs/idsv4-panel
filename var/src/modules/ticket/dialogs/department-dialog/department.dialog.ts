import { Component, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
    providers: [],
    selector: 'department-dialog',
    templateUrl: './department.dialog.html',
    styleUrls: ['./department.dialog.css']
})

export class DepartmentDialog {

    public title: string;
    public button: string;
    public form: FormGroup;
    public loading: boolean = false;
  

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<DepartmentDialog>, private _snackBar: MatSnackBar, private translate: TranslateService) {
        this.form = new FormGroup({
            name: new FormControl('', Validators.required),
            id: new FormControl(null),
            deleted: new FormControl(false)
           
        });
    
    }


    ngOnInit() {
           
        if (!this.data.data.hasOwnProperty('id'))
            this.title = this.translate.instant(`Nueva ${this.data.type}`);
        else {
            this.title = this.translate.instant(`Editar ${this.data.type}`);
            this.initForm();
        }
    }

    private initForm() {    
        this.form.setValue({
            name: this.data.data.name,
            id: this.data.data.id, 
            deleted: this.data.data.deleted         
        });
       
    }

    public save() {              
        if (this.form.valid) {
            let result = this.form.value ;
            this.dialogRef.close(
                result
            );
        } else {
            this.openSnackBar('Complete los campos requeridos');
        }
    }

    public close() {
        this.dialogRef.close(false);
    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }



}
