import { Component, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';




@Component({
    providers: [],
    selector: 'image-dialog',
    templateUrl: './image-dialog.html',
    styleUrls: ['./image-dialog.scss']
})

export class ImageDialog {

   img: string;
   type: string;
  

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<ImageDialog>) {
        this.img = this.data.url;
        this.type = this.data.type;

       
    
    }


    ngOnInit() {}

    

 

    public close() {
        this.dialogRef.close(false);
    }

    



}
