import { Component, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
    providers: [],
    selector: 'ticket-dialog',
    templateUrl: './ticket.dialog.html',
    styleUrls: ['./ticket.dialog.css']
})

export class TicketDialog {

    public title: string;
    public button: string;
    public form: FormGroup;
    public loading: boolean = false;
    public color: string;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<TicketDialog>, private _snackBar: MatSnackBar, private translate: TranslateService) {
        this.form = new FormGroup({
            name: new FormControl('', Validators.required),
            id: new FormControl(0),
            color: new FormControl('')
        })
    }


    ngOnInit() {
           
        if (!this.data.data.hasOwnProperty('id'))
            this.title = this.translate.instant(`Nueva ${this.data.type}`);
        else {
            this.title = this.translate.instant(`Editar ${this.data.type}`);
            this.initForm();
        }
    }

    private initForm() {       
        this.form.setValue({
            name: this.data.data.name,
            id: this.data.data.id,
            color: this.data.data.color
        });
        this.color =  this.data.data.color;
    }

    public save() {      
        if(!this.color){
            return;
        }
        this.form.value.color = this.color;
        if (this.form.valid) {
            let result = this.form.value ;
            this.dialogRef.close(
                result
            );
        } else {
            this.openSnackBar('Complete los campos requeridos');
        }
    }

    public close() {
        this.dialogRef.close(false);
    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }



}
