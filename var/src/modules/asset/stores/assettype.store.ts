import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { AssetType, IAssetType } from "src/modules/asset/models/asset-type";
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'asset-type.store.ids';

@Injectable()
export class AssetTypeStore {
    public onChange: BehaviorSubject<AssetType[]> = new BehaviorSubject([]);
    private data: AssetType[] = [];
    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const items: IAssetType[] = JSON.parse(store); 
                this.update(items);
            } else {
                this.load();
            }
        }else
            this.load();
    }
    public get(): AssetType[] {
        return this.data;
    }
    public set(items: IAssetType[]) {
        this.update(items);
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(items));
    }
    public reload() {
        this.load();
    }
    private load() {
        this.ws.assetTypesToSelect().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }
        });
    }
    private update(items: IAssetType[]) {
        this.data = items.map(s => new AssetType(s));
        this.onChange.next(this.data);
    }
}