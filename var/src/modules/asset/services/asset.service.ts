import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Asset } from 'src/modules/asset/models/asset';
import { HttpService } from 'src/services/http.service';
import { ICrudWS } from 'src/interfaces/ICrudWS.interface';

@Injectable({
	providedIn: 'root',
})
export class AssetService implements ICrudWS<Asset>{
  constructor(private http: HttpService) {}

	public list(args?:any[]): Observable<Asset[]> {
		return this.http.get(`/private/assets`, args, 'WS.asset');
	}

	public toSelect(args?: any[]): Observable<Asset[]> {
		return this.http.get(`/private/assets/toselect`, args, 'WS.asset');
	}

	public delete(id: number): Observable<boolean> {
		return this.http.delete('private/asset', [id], 'WS.asset_delete');
	}
	public deleteForce(id: number, args?:any[]): Observable<Asset> {
		return this.http.delete(`/private/asset/${id}/force`, args, 'WS.asset_delete');
	}

	public update(obj: Asset): Observable<Asset> {
		return this.http.update(`/private/asset`,obj, 'WS.asset_add');
	}

	public create(data: any): Observable<Asset> {
		return this.http.create(`/private/asset`, data, 'WS.asset_add');
	}

	public assetTypesToSelect(args?: any[]): Observable<Asset[]> {
		return this.http.get(`/private/asset-types/toselect`, args, 'WS.asset');
	}

	public get(id: number): Observable<Asset> {
		return this.http.get('private/asset', [id], 'WS.asset');
	}
}


