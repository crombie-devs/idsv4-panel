import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Asset } from 'src/modules/asset/models/asset';
import { PlayArea } from 'src/modules/playarea/models/playarea';

@Component({
  selector: 'asset-assetmanager',
  templateUrl: './assetmanager.component.html',
  styleUrls: ['./assetmanager.component.scss']
})

export class AssetManagerComponent implements OnInit {
  
  @Input() items;
  @Input() selectedAreas: PlayArea[];
  @Output() onSelectAsset = new EventEmitter<Asset>()

  constructor() { }

  ngOnInit(): void {
  }
  
  public selectAsset(asset: Asset) {
    this.onSelectAsset.emit(asset)
  }

}
