import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, ElementRef, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { Asset } from 'src/modules/asset/models/asset';
import { AssetCarouselComponent } from 'src/modules/asset/components/assetcarousel/assetcarousel.component'
import { CarouselItemDirective } from 'src/modules/asset/directives/carousel-item.directive';
import { MatDialog } from '@angular/material/dialog';
import { PreviewDialog } from 'src/modules/asset/dialogs/preview/preview.dialog';
import { TranslateService } from '@ngx-translate/core';
import { FileHandle } from 'src/directive/drag-drop.directive';
import { Format } from "src/modules/format/models/format";
import { FormatStore } from 'src/modules/format/stores/format.store';
import { Lang } from 'src/modules/lang/models/lang';
import { LangStore } from 'src/modules/lang/stores/lang.store';

@Component({
  selector: 'asset-assetmanagerpreview',
  templateUrl: './assetmanagerpreview.component.html',
  styleUrls: ['./assetmanagerpreview.component.scss']
})

export class AssetManagerPreviewComponent implements OnInit {
  
  @ViewChildren('appCarouselItem') items: QueryList<any>;
  @ViewChild('carouselItems') carouselItemsContainer: ElementRef<HTMLDivElement>;
  @Input() assetHist:Asset[];
  @Input() reload:boolean;
  @Input() dataFormat;
  @Input() dataLangid;
  @Output() onSelectAsset = new EventEmitter<Asset>();
  @Output() onDeletedAsset = new EventEmitter<any>();
  @Output() onFileDropped = new EventEmitter<any>();
  
  private slidesIndex = 0;
  public finalAssets = 0;
  public showCarusel:boolean = true;
  public format: Format;
  public lang: Lang;

  constructor(
    private matDialog: MatDialog, 
    private translate: TranslateService,
    private fstore: FormatStore,
    private lstore: LangStore
  ) { }

  ngOnInit(): void {
  }
  
  ngOnChanges(changes: SimpleChanges) {
    this.showCarusel = false;
      setTimeout(() => {
        this.showCarusel = true;        
      }, 0);
      if(changes.dataFormat && changes.dataFormat.currentValue){
        this.fstore.onChange.subscribe(formats => { 
          this.format = formats.filter(f => f.idformat === this.dataFormat)[0];          
        }); 
      }
      if(changes.dataLangid && changes.dataLangid.currentValue){
        this.lstore.onChange.subscribe(langs => { 
          this.lang = langs.filter(l => l.idlang === this.dataLangid)[0];
        }); 
      }
  }
  
  onClickLeft() {
    this.carouselItemsContainer.nativeElement.scrollLeft -= this.currentItem.nativeElement.offsetWidth;
    if (this.slidesIndex > 0) {
      this.slidesIndex--;
    } 
  }

  onClickRight() {
    this.carouselItemsContainer.nativeElement.scrollLeft += this.currentItem.nativeElement.offsetWidth;
    if (this.slidesIndex < this.items.length - 1) {
      this.slidesIndex++
    }
  }
  
  get currentItem(): ElementRef<HTMLDivElement> {
    return this.items.find((item, index) => index === this.slidesIndex);
  }
  
  public selectAsset(asset: Asset) {
    this.onSelectAsset.emit(asset)
  }
  
  public deleteAsset(idx:number,idasset:number) {
    this.onDeletedAsset.emit({idx,idasset})
  }
  
  public removeSelectedAsset(index, idasset) {
    this.onDeletedAsset.emit({index,idasset})
	}

  preview(asset: Asset){
    this.openDialogPreview(asset);
  }
	
	public openDialogPreview(asset: Asset) {
      const dialogRef = this.matDialog.open(PreviewDialog, {data:asset});
      dialogRef.afterClosed().subscribe((result: Asset) => {}); 
  }

  filesDropped(file: FileHandle[], position:number): void {
    const data = {
      file:file[0],
      lang:this.lang,
      format: this.format,
      position:position
    }
    this.onFileDropped.emit(data);
  }
}
