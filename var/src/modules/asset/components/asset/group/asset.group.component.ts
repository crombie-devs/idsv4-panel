import { Component, ElementRef, EventEmitter, HostListener, Input, Output, SimpleChanges, ViewChild } from "@angular/core";

import { Asset } from "src/modules/asset/models/asset";
import { Format } from "src/modules/format/models/format";
import { Lang } from "src/modules/lang/models/lang";
import { FileHandle } from 'src/directive/drag-drop.directive';

@Component({
    selector: 'asset-group',
    templateUrl: './asset.group.component.html',
    styleUrls: ['./asset.group.component.scss']
  })

export class AssetGroupComponent {

  @Output() onChange: EventEmitter<Asset[]> = new EventEmitter(null);
  @Output() onFileDropped: EventEmitter<any> = new EventEmitter(null);
  @Output() onClickAsset: EventEmitter<Asset[]> = new EventEmitter(null);
  @Input() canSelect: boolean = false;
  @Input() lang: Lang;
  @Input() format: Format;
  @Input() name: string;
  @Input() assets: Asset[];
  @Input() slidesToShow: number;
  @Input() sliderWith: number;
  @Input() cellsLimit:number = 3;
  @Input() lightDOM:boolean = true;

  public assetsToShow:Asset[] = [];
  public slidesToScroll:number = 1;

  @ViewChild('carouselWrapper', {static: true}) carouselWrapper: ElementRef;
  @ViewChild('carouselContainer', {static: true}) carouselContainer: ElementRef;
  @ViewChild('carouselCell', {static: true}) carouselCell: ElementRef;
  
  
  @HostListener('window:resize', ['$event.target']) 
	onResize(event) {
	  this.resizeWorks(event.innerWidth);
	}

  constructor(){
    this.resizeWorks(window.innerWidth);
  }

  resizeWorks(width){
    this.slidesToScroll = Math.floor((width * 7) / 1920);
  }
  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    if(changes.assets){
      this.assignAssetsToShow()
    }
  }

  public assignAssetsToShow(){
    this.assetsToShow = this.assets.filter((a,i) => i <= this.slidesToShow);
  }
  onUpdated(asset: Asset) {
    if(asset) {
      this.assets = this.assets.map(a => (a.isEquals(asset))? asset : a );
      this.onChange.emit(this.assets);
    }
  }

  onDeleted(asset: Asset) {
    if(asset) {
      this.assets = this.assets.filter(a => !a.isEquals(asset));
      this.onChange.emit(this.assets);
    }
  }
    
  filesDropped(file: FileHandle[], lang:Lang, format: Format): void {
    const data = {
      file:file[0],
      lang:lang,
      format:format
    }
    this.onFileDropped.emit(data);
  }
    
  onClick(e) {
    this.onClickAsset.emit(e)
  }

  public prev(data){
    const posInit = this.carouselWrapper.nativeElement.offsetLeft;
		let posFinal = posInit + this.carouselContainer.nativeElement.clientWidth;
  		let i = posInit;
		if(posFinal  > -this.carouselContainer.nativeElement.clientWidth)
			posFinal = 0;
		let anim = setInterval(() => {
			if(i <= posFinal){
				this.carouselWrapper.nativeElement.style.left = `${i}px`;
				i += 10;
			}else{
				clearInterval(anim);
			}
		}, 0.2);
  }
  public next(data){
    const posInit = this.carouselWrapper.nativeElement.offsetLeft;
    const elems = Math.floor(this.carouselContainer.nativeElement.clientWidth / 210);
    let posFinal = posInit - (elems * 210);
    let maxLeft = 0;
    if(this.assets.length > elems){
      if(this.assets.length === this.assetsToShow.length){
        maxLeft = -(this.carouselWrapper.nativeElement.clientWidth - this.carouselContainer.nativeElement.clientWidth);
      }
      let i = posInit;
      if(maxLeft < 0 && maxLeft > posFinal)
        posFinal = maxLeft;
      let anim = setInterval(() => {
        if(i >= posFinal){
          this.carouselWrapper.nativeElement.style.left = `${i}px`;
          i -= 10;
        }else{
          clearInterval(anim);
        }
      }, 0.2);
      if(this.assetsToShow < this.assets){
        this.slidesToShow = Math.floor(this.carouselContainer.nativeElement.clientWidth / 200);
        let slidePosition = Math.floor(this.assetsToShow.length / this.slidesToShow);
        const assetsToAdd = this.assets.filter((a,i) => i >= this.assetsToShow.length && i <= (this.assetsToShow.length + this.slidesToShow));
        assetsToAdd.map(asset => {
          if(!this.assetsToShow.some(a => a.idasset === asset.idasset))
            this.assetsToShow = [...this.assetsToShow, asset];
        })
      }
    }
  }
}