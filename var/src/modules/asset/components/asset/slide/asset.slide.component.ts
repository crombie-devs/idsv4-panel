import { Component, EventEmitter, Input, Output, SimpleChanges } from "@angular/core";
import { MatDialog } from '@angular/material/dialog';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { LandingpageDialog } from "src/modules/landing-page/dialogs/landingpage/landingpage.dialog";
import { PreviewDialog } from 'src/modules/asset/dialogs/preview/preview.dialog';
import { TemplateDialog } from "src/modules/template/dialogs/template/template.dialog";
import { IResult } from "src/models/iresult";
import { Landingpage } from "src/modules/landing-page/models/landingpage";
import { WSService } from 'src/services/ws.service';
import { DebugStore } from "src/stores/debug.store";
import { UserStore } from "src/modules/user/stores/user.store";
import { Asset, ASSET_TYPES, IAsset } from "src/modules/asset/models/asset";
import { AssetDialog } from 'src/modules/asset/dialogs/asset/asset.dialog';
import { CacheService, ICacheConfig } from "src/services/cache.service";
import { EventService } from "src/shared/event.service";

@Component({
    selector: 'asset-slide',
    templateUrl: './asset.slide.component.html',
    styleUrls: ['./asset.slide.component.scss']
  })
export class AssetSlideComponent {
    @Output() onUpdated: EventEmitter<Asset> = new EventEmitter(null);
    @Output() onDeleted: EventEmitter<Asset> = new EventEmitter(null);
    @Output() onClick: EventEmitter<Asset> = new EventEmitter(null);
    @Input() canSelect: boolean = false;
    @Input() asset: Asset;

    loading: boolean = false;
    asset_types: any = ASSET_TYPES;
    me: any;	
    landingpage: Landingpage;
    
    constructor(private ws: WSService,
      private events: EventService,
		  private eventc: CacheService, 
      private dialog: DialogService, 
      private matDialog: MatDialog,
      private ustore: UserStore,
      private debug: DebugStore){
        this.ustore.onChange.subscribe(user => { 
          this.me = user; 
        });
    }
    openPreview() {
      const dialogRef = this.matDialog.open(PreviewDialog, {data: this.asset});
      dialogRef.afterClosed().subscribe((result: Asset) => {}); 
    }
    
    ngOnChanges(changes: SimpleChanges): void {
      //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
      //Add '${implements OnChanges}' to the class.
    }
    openEditForm() {
      if(this.asset.idtype === this.asset_types.template){
        const dialogRef = this.matDialog.open(TemplateDialog, {data: this.asset});
        dialogRef.afterClosed().subscribe((result: any) => {
          let id;
          if (result){
            let asset: IAsset;
            let urlParts = result.asset.asset_url.split('/');
            id = urlParts[urlParts.length - 1];
            id = id.substring(0,id.length-5);
            if(result.asset){            
              asset = new Asset(result.asset);
              asset.asset_url = result.url;            
            }
            else{
              asset = new Asset();
              asset.asset_url = result.url;
              asset.idtype = ASSET_TYPES.url;
            }
          const dialogRef = this.matDialog.open(AssetDialog, {data: asset});
          dialogRef.afterClosed().subscribe((result: Asset) => {
            if(result){
              if(id)
                this.ws.deleteHtmlTemplate(id).subscribe();
              this.onUpdated.emit(result);
              if(this.me && this.me.customer){
                let config: ICacheConfig = {idcustomer: this.me.customer.idcustomer};
                if(this.me.site)
                  config = {idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer};              
                setTimeout(() => {
                  this.events.publish('asset:change', config);     
                }, 1000);
              }
            }else{
              if(id)
                this.ws.deleteHtmlTemplate(id).subscribe();
              this.ws.deleteHtmlTemplate(id);
            }
          });
          }
        }); 
      } else if (!this.asset.idlandingpage){
        const dialogRef = this.matDialog.open(AssetDialog, {data: this.asset});
        dialogRef.afterClosed().subscribe((result: Asset) => {
          if(result){
            this.onUpdated.emit(result);
            if(this.me && this.me.customer){
              let config: ICacheConfig = {idcustomer: this.me.customer.idcustomer};
              if(this.me.site)
                config = {idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer};              
              setTimeout(() => {
                this.events.publish('asset:change', config);     
              }, 1000);
            }
          }
        });
      } else{
        this.ws.landingpage(this.asset.idlandingpage).subscribe((result: IResult) => {
          if(result.success) 
            this.landingpage = new Landingpage(result.data);      
            const dialogRef =  this.matDialog.open(
              LandingpageDialog, 
              {
                data: { "landingpage":this.landingpage, asset: this.asset}, 
                width: 'auto',
                height: 'auto',
                minHeight: '600px',
                minWidth:'800px'
              }
            );
            dialogRef.afterClosed().subscribe((result: Asset) => {
              if(result){
                this.onUpdated.emit(result);
                if(this.me && this.me.customer){
                  let config: ICacheConfig = {idcustomer: this.me.customer.idcustomer};
                  if(this.me.site)
                    config = {idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer};              
                  setTimeout(() => {
                    this.events.publish('asset:change', config);     
                  }, 1000);
                }
              }
            }); 
          });
      }
    }
    confirmDelete() {
      this.dialog.confirm('ASSETS.slide.confirm.title', 'ASSETS.slide.confirm.text').subscribe(confirm => {
        if(confirm)
          this.delete();
      });
    }
    confirmDeleteFoce() {
      this.dialog.confirm('ASSETS.slide.confirm.title', 'ASSETS.slide.confirm.text2').subscribe(confirm => {
        if(confirm)
          this.deleteForce();
      });
    }
    onSelect(){
      if(this.canSelect) {
        this.asset.selected = !this.asset.selected;
        this.onUpdated.emit(this.asset);
      } else {
        this.openPreview();
      }      
    }
    private delete() {
      this.loading = true;
      if(this.asset.contents.length>0)
        this.confirmDeleteFoce();
      else{  
        this.ws.deleteAsset(this.asset.idasset).subscribe(result => {
          this.loading = false;
          if(result.success){
            if(this.me && this.me.customer){
              let config: ICacheConfig = {idcustomer: this.me.customer.idcustomer};
              if(this.me.site)
                config = {idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer};              
              setTimeout(() => {
                this.events.publish('asset:change', config);     
              }, 1000);
            }
            let urlParts = this.asset.asset_url.split('/');
            let id;
            id = urlParts[urlParts.length - 1];
            id = id.substring(0,id.length-5);
            this.ws.deleteHtmlTemplate(id).subscribe();
            this.onDeleted.emit(this.asset);
          }
          else{
            this.dialog.warning('ASSETS.slide.delete.title', result.data);
          }
        }, error => {
          this.dialog.warning('ASSETS.slide.delete.title', error.message); 
          this.loading = false;
        });
      }
    }
    private deleteForce(){
      this.ws.deleteAssetForce(this.asset.idasset).subscribe(result => {
        if(result.success){
          if(this.me && this.me.customer){
            let config: ICacheConfig = {idcustomer: this.me.customer.idcustomer};
            if(this.me.site)
              config = {idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer};              
            setTimeout(() => {
              this.events.publish('asset:change', config);     
            }, 1000);
          }
          this.onDeleted.emit(this.asset);
        }
        else{
          this.dialog.warning('ASSETS.slide.delete.title', result.data);
        }
      }, error => {
        this.dialog.warning('ASSETS.slide.delete.title', error.message);
      });
    }

    public getContentsTooltip(contents){
      if(contents){
        let conts:string = '';
        contents.map(c => {
          conts += c.name + '\n';
        })
        return conts;
      }
    }
}