import { Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { FormControl } from '@angular/forms';
import { ASSET_TYPES } from 'src/modules/asset/models/asset';
import { InputfileComponent } from "src/modules/asset/components/input-file/input-file.component";

@Component({
    selector: 'input-media',
    templateUrl: './input-media.component.html',
    styleUrls: ['./input-media.component.scss']
  })
export class InputMediaComponent {
    
    @Input() asset_type: number;
    @Input() asset_url: string
    
    @Output() onChangePreview = new EventEmitter<number>();
    @Output() onChangeInputfile = new EventEmitter<File>();
    @Output() onDeleted = new EventEmitter<string>();
    @ViewChild(InputfileComponent) file: InputfileComponent;

    public asset_types: any = ASSET_TYPES;
    
    constructor(){}

    public changeInputfile(file: File){
        this.onChangeInputfile.emit(file);
    } 

    public changePreview(duration: number){
        this.onChangePreview.emit(duration);
    }

    public delete(){
        
        this.onDeleted.emit('delete');
    }

    public removeUrl() {
        this.file.removeFile();
    }

}