import { MaxSizeValidator } from '@angular-material-components/file-input';
import { Component, EventEmitter, Input, Output } from "@angular/core";
import { FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'input-file',
    templateUrl: './input-file.component.html',
    styleUrls: ['./input-file.component.scss']
  })
export class InputfileComponent {
    
    @Output() onChange = new EventEmitter<File>();

    public file_input: FormControl ; 
    public maxSize= 300;
    public file: File;

    constructor(){
        this.file_input= new FormControl('', [
            Validators.required,
            MaxSizeValidator(this.maxSize * 1024)
        ]);
    }

    ngOnInit() {
        this.file_input.valueChanges.subscribe((files: any) => {
            if (!Array.isArray(files)) 
                this.file = files;                    
            else 
                this.file = files[0];
            this.change(this.file);
        });	        
    }

    public change (file: File) {
        this.onChange.emit(file);
    }

    public removeFile() : void {
        this.file_input.reset();
    }

}