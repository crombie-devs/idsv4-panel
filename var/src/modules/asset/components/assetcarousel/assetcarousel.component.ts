import { Component, AfterContentInit, ContentChildren, ViewChild, ViewChildren, QueryList, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { Asset } from 'src/modules/asset/models/asset';
import { CarouselItemDirective } from 'src/modules/asset/directives/carousel-item.directive';
import { MatDialog } from '@angular/material/dialog';
import { PreviewDialog } from 'src/modules/asset/dialogs/preview/preview.dialog';

@Component({
  selector: 'asset-assetcarousel',
  templateUrl: './assetcarousel.component.html',
  styleUrls: ['./assetcarousel.component.scss']
})
export class AssetCarouselComponent implements AfterContentInit {
  
  @ViewChildren('appCarouselItem') items: QueryList<any>;
  @ViewChild('carouselItems') carouselItemsContainer: ElementRef<HTMLDivElement>;
  
  @Input() lang;
  @Input() format;
  @Input() elements;
  @Output() onSelect = new EventEmitter<Asset>() 
  
  private slidesIndex = 0;
  public elemsToShow:Asset[] = [];

  constructor(private matDialog: MatDialog){}
  
  public ngOnInit() {
    this.elemsToShow = this.elements.filter((e,i) => i < 8);
  }

  get currentItem(): ElementRef<HTMLDivElement> {
    return this.items.find((item, index) => index === this.slidesIndex);
  }

  ngAfterContentInit() {
  }

  ngAfterViewInit() {
  }

  onClickLeft() {
    console.log(this.slidesIndex);
    this.carouselItemsContainer.nativeElement.scrollLeft -= this.currentItem.nativeElement.offsetWidth;
    if (this.slidesIndex > 0) {
      this.slidesIndex--;
    } 
  }

  onClickRight() {
    this.carouselItemsContainer.nativeElement.scrollLeft += this.currentItem.nativeElement.offsetWidth;
    if (this.slidesIndex < this.items.length - 1) {
      this.slidesIndex++
    }
  }
  
  public onSelectAsset(event) {
    let target = event.target;
    if(!target.classList.contains('clicked')) {
			target.classList.add('clicked');
		}
  }
  
   preview(asset: Asset){
      this.openDialogPreview(asset);
    }
	
	  public openDialogPreview(asset: Asset) {
      const dialogRef = this.matDialog.open(PreviewDialog, {data:asset});
      dialogRef.afterClosed().subscribe((result: Asset) => { }); 
  }

}
