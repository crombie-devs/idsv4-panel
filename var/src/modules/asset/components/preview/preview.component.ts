import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ASSET_TYPES } from 'src/modules/asset/models/asset';

@Component({
    selector: 'preview',
    templateUrl: './preview.component.html',
    styleUrls: ['./preview.component.scss']
  })
export class PreviewComponent {
    
    @Input() url: string;
    @Input() type: string;
    @Input() delete: boolean = true;

    @Output() onDeleted = new EventEmitter<string>();
    @Output() onChange = new EventEmitter<number>();

    public asset_types: any = ASSET_TYPES;

    public deleted() {
        this.onDeleted.emit('delete');
    } 

    public change(duration: number) {
        this.onChange.emit(duration);
    }

    public onloadVideo(){
        let video = document.getElementById('video') as HTMLVideoElement;
        try{ video.play(); }catch(ex){}
        this.change(Math.trunc(video.duration)); 
    }

    public onloadAudio(){
        let audio = document.getElementById('audio') as HTMLAudioElement;
        try{ audio.play(); }catch(ex){}
        this.change(Math.trunc(audio.duration)); 
    }
    
}