import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssetListPage } from './pages/list/list.page';
import { FormatLangAssetListPage } from './pages/format-lang-list/format-lang-list.page';

const routes: Routes = [
  { path: 'list/:idformat/:idlang', component: FormatLangAssetListPage, data: { breadcrumb: 'Cartelera' } },
  { path: 'list', component: AssetListPage, data: { breadcrumb: 'Cartelera' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AssetRoutingModule { }
