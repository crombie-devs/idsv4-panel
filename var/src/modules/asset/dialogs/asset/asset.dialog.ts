import { Component, Inject, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Asset, ASSET_TYPES } from 'src/modules/asset/models/asset';
import { AssetFromDrop } from 'src/modules/asset/models/asset-from-drop';
import { Format } from 'src/modules/format/models/format';
import { IResult } from 'src/models/iresult';
import {  Lang } from 'src/modules/lang/models/lang';
import { User } from 'src/modules/user/models/user';
import { WSService } from 'src/services/ws.service';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { AssetTypeStore } from 'src/modules/asset/stores/assettype.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { AssetType } from 'src/modules/asset/models/asset-type';

import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { DebugStore } from 'src/stores/debug.store';
import { InputMediaComponent } from 'src/modules/asset/components/input-media/input-media.component';
import { CacheService, ICacheConfig } from 'src/services/cache.service';
import { EventService } from 'src/shared/event.service';
import { FormField } from 'src/modules/form-field/models/form-field';
import moment from 'moment';

@Component({
    providers: [FormatStore, LangStore, AssetTypeStore],
	selector: 'asset-dialog',
  	templateUrl: './asset.dialog.html',
  	styleUrls: ['./asset.dialog.scss']
})

export class AssetDialog {
    formats: Format[] = [];
    langs: Lang[] = [];
    types: AssetType[] = [];
    me: User = null;
    asset: Asset;
    file: File;
    url: string;
    u: any;
    isSaving: boolean = false;
    public asset_url;
    public asset_type;
    public action: string = 'new';
    public hiddenInputmedia: boolean = false;
    public disableType: boolean = false;
    public disableFormat: boolean = false;
    public disableLang: boolean = false;
    public disableWidget: boolean = false;
    public color = 'primary';
    public disabledURL:boolean = false;
    id: number;
    @ViewChild(InputMediaComponent) media: InputMediaComponent;
    public exceded: boolean; 

    public asset_types: any = ASSET_TYPES;

    public minDate:string = moment().format('YYYY-MM-DD');
    public fieldDataExpiredDate: FormField = {
		label: "Fecha de expiración",
		name: 'expired_date',
		placeholder: '',
		multiple:false,
		required: false,
		minDate: moment(this.minDate).format('YYYY-MM-DD'),
		hideTime: true
	};
    public fieldThumbMoment: FormField = {
		label: "Captura de miniatura",
		name: 'delay',
		placeholder: '',
		required: true,
        min:0,
        max:30,
        medida:'seg.'
	};
    form: FormGroup = new FormGroup({
        idcustomer: new FormControl(null),
        idsite: new FormControl(null),
        name: new FormControl(null, [Validators.required]),
        idformat: new FormControl(null, [Validators.required]),
        idlang: new FormControl(null, [Validators.required]),
        idtype: new FormControl(null, [Validators.required]),
        asset_url:  new FormControl(null),
        duration:  new FormControl(10),
        size:  new FormControl({ value: null, disabled: true }),
        asset: new FormControl(null),
        is_widget: new FormControl(false),
        expired_date: new FormControl(null),
        delay: new FormControl(5, [Validators.required, Validators.min(0), Validators.max(30)])
    });

	constructor(
        @Inject(MAT_DIALOG_DATA) public data: Asset | AssetFromDrop | File | any,
        public dialogRef: MatDialogRef<AssetDialog>, 
        private ws: WSService,
        private ustore: UserStore,
        private fstore: FormatStore,
        private lstore: LangStore,
        private atstore: AssetTypeStore,
        private dialog: DialogService) {
            this.fstore.onChange.subscribe(formats => { 
                if(this.data.formats){
                    formats.map(f => {
                        if(this.data.formats.some(fc => fc === f.idformat))
                            this.formats.push(f);
                    }) 
                }else
                    this.formats = formats; 
            });
            this.lstore.onChange.subscribe(langs => { 
                if(this.data.langs){
                    langs.map(l => {
                        if(this.data.langs.some(lc => lc === l.idlang))
                            this.langs.push(l);
                    }) 
                }else{
                    this.langs = langs;
                }
            });
            this.atstore.onChange.subscribe(types => { 
                this.types = types;
            });
            this.ustore.onChange.subscribe(user => { 
                this.me = user; 
                this.setValues();
            });
        }

    private setValues() {
        if (this.data instanceof Asset) {
            this.id = this.data.idasset;
            if(this.id)
                this.action = 'edit';
            this.asset = new Asset(this.data);
            this.form.patchValue({
                idcustomer: (this.me && this.me.customer)? this.me.customer.idcustomer : '',
                idsite: (this.me && this.me.site)? this.me.site.idsite : '',
                name: this.data.name,
                idformat: (this.data.format)? this.data.format.idformat : '',
                idlang: (this.data.lang)? this.data.lang.idlang : '',
                idtype: this.data.idtype,
                asset_url: this.data.asset_url,
                duration: this.data.duration ? this.data.duration : 10,
                size: this.data.size,
                is_widget: this.data.is_widget,
                asset: this.data.asset?this.data.asset:'',
                expired_date: (this.data.expired_date && this.data.expired_date !== "0000-00-00")  ? moment(this.data.expired_date) : null,
                delay: this.data.delay
            })
            if(this.asset.idtype){
                this.changeType(this.asset.idtype);
                this.disableType = false;
                this.disableWidget = true;
            }
        }else if (this.data instanceof AssetFromDrop) {
            this.form.patchValue({
                idcustomer: (this.me && this.me.customer)? this.me.customer.idcustomer : '',
                idsite: (this.me && this.me.site)? this.me.site.idsite : '',
                name: this.data.name,
                idformat: (this.data.format)? this.data.format.idformat : '',
                idlang: (this.data.lang)? this.data.lang.idlang : '',
                duration: this.data.duration ? this.data.duration : 10,
                size: this.data.size,
                is_widget: this.data.is_widget,
                asset: this.data.asset?this.data.asset:''
            })
            this.setFile(this.data.asset);
            this.changeType(this.asset.idtype);
        } else if(this.data instanceof File){
            this.form.patchValue({
                idcustomer: (this.me && this.me.customer)? this.me.customer.idcustomer : '',
                idsite: (this.me && this.me.site)? this.me.site.idsite : ''
            })
            this.setFile(this.data);
        }else{
            this.asset = new Asset(this.data);
            this.form.patchValue({
                idcustomer: (this.me && this.me.customer)? this.me.customer.idcustomer : '',
                idsite: (this.me && this.me.site)? this.me.site.idsite : '',
                idtype: this.data.idtype ? this.data.idtype : '',
                idformat: (this.data.format)? [this.data.format.idformat] : '',
                idlang: (this.data.lang)? this.data.lang.idlang : '',
            });
            this.changeType(this.data.idtype);
            this.disableType = true;
            if(this.data.format)
                this.disableFormat = true;
            if(this.data.lang)
                this.disableLang = true;
            this.disableWidget = true;
        }
    }     

    private findInvalidControls() {
        const invalid = [];
        const controls = this.form.controls;
        for (const name in controls) {
            if (controls[name].invalid) {
                invalid.push(controls[name]);
            }
        }
        return invalid;
    }
    public save() {
        this.isSaving = true;
        if(this.form.invalid){ 
            let invalid: any [] = [];
            const controls = this.form.controls;
            for (const name in controls) {
                if (controls[name].invalid) {
                    invalid.push(controls[name]);
                }
            }
            this.isSaving = false; 
            this.dialog.warning("Error", "No está completo el formulario");
            return; 
        }
        if(this.action === 'edit'){
            this.ws.updateAsset(this.id, this.getFormData()).subscribe((result: IResult) => {
                if(result.success) {                    
                    const asset: Asset = new Asset(result.data);
                    this.isSaving = false;
                    this.dialogRef.close(asset);
                }else{
                    this.dialog.warning("Error", "No se ha podido guardar el asset");
                    this.isSaving = false;
                }
            },
            error => {
                this.dialog.warning("Error", "No se ha podido guardar el asset");
                this.isSaving = false;
            }); 
        }else{
            const formats = this.form.get('idformat').value;
            if(!Array.isArray(formats)){
                this.ws.createAsset(this.getFormData()).subscribe((result: IResult) => {
                    if(result.success) {
                        const asset: Asset = new Asset(result.data);
                        this.isSaving = false;
                        this.dialogRef.close(asset);
                    }else{
                        this.dialog.warning("Error", "No se ha podido guardar el asset");
                        this.isSaving = false;
                    }
                },
                error => {
                    this.dialog.warning("Error", "No se ha podido guardar el asset");
                    this.isSaving = false;
                });    
            }else{
                let assets: Asset[] = [];
                let counter = 0;
                formats.map(format => {
                    this.ws.createAsset(this.getFormData(format)).subscribe((result: IResult) => {
                        if(result.success) {
                            assets.push(new Asset(result.data));
                            counter++;
                            if(counter === formats.length){
                                this.isSaving = false;
                                this.dialogRef.close(assets);
                            }
                        }else{
                            this.isSaving = false;
                        }
                    },
                    error => {
                        this.isSaving = false;
                    });    
                })
            }
        
        }       
    } 
    
    private getFormData(format:string = null): FormData{
        let formData = new FormData();
        Object.keys(this.form.value).forEach((key) => (this.form.value[key] === null) && delete this.form.value[key]);
        if(!format){
            for(const property in this.form.value) {
                if(property === "asset_url"){
                    this.form.get('asset_url').patchValue(encodeURI(this.form.value[property]));
                    formData.set(property,this.form.value[property]);
                } else if(property === 'expired_date'){
                    this.form.get('expired_date').patchValue(moment(this.form.value[property]).format('YYYY-MM-DD'));
                    formData.set(property,this.form.value[property]);
                }else if(property === 'is_widget'){
                } else if(this.form.value[property] !== null)
                    formData.set(property,this.form.value[property]);
            }
        }else{
            for(const property in this.form.value) {
                if(property === "idformat"){
                    formData.set(property,format);
                }else if(property === "asset_url"){
                    this.form.get('asset_url').patchValue(encodeURI(this.form.value[property]));  
                    formData.set(property,this.form.value[property]);
                } else if(property === 'expired_date'){
                    this.form.get('expired_date').patchValue(moment(this.form.value[property]).format('YYYY-MM-DD'));
                    formData.set(property,this.form.value[property]);
                }else if(property === 'is_widget'){
                }else if(this.form.value[property] !== null){
                    formData.set(property,this.form.value[property]);
                }
            }
        }
        if(this.form.value.duration == null)
            formData.append('duration', '0')
        if(!formData.get('is_widget'))
            this.form.value.is_widget ? formData.append('is_widget', '1') : formData.append('is_widget', '0');
        return formData;
    }

    public setFile(file: File){ 
        this.asset = new Asset();
        const maxsize = this.me.customer.max_file_size;
        if(!file) {
            return;
        } 
        if((file.size / 1000000) > maxsize)  {
            this.media.removeUrl();
            this.exceded = true;
            return;
        }
        this.exceded = false;
        if (file){
            if(file instanceof AssetFromDrop){
                if (file.asset.type.substr(0,5) === 'image') {
                    this.setType(1); 
                } else if(file.asset.type.substr(0,5) === 'audio')   {  
                    this.setType(5);
                } else if(file.asset.type.substr(0,5) === 'text/') {
                    this.setType(4);
                } else {
                    this.setType(2);
                    this.setSize(file.asset.size);
                    this.setAsseturl(URL.createObjectURL(file.asset), false);  
                    this.form.get('asset').patchValue(file.asset);
                }
            }else{
                if (file.type.substr(0,5) === 'image')
                    this.setType(1);
                else if(file.type.substr(0,5) === 'audio')    
                    this.setType(5);
                else if(file.type.substr(0,5) === 'text/')   
                     this.setType(4);
                else 
                this.setType(2);
                this.setSize(file.size);
                this.setAsseturl(URL.createObjectURL(file), false);  
                this.form.get('asset').patchValue(file);
            }  
        }               
    }
    public setType(idtype: number){
        this.asset.idtype = idtype; 
        this.form.get('idtype').patchValue(this.asset.idtype); 
        this.hiddenInputmedia = (idtype == ASSET_TYPES.url || idtype == ASSET_TYPES.template);   
    }
    public setDuration(duration: number){
       this.asset.duration = duration;            
       this.form.get('duration').patchValue(this.asset.duration);    
    }
    public setSize(size: number){
        this.asset.size = size;            
        this.form.get('size').patchValue(this.asset.size);    
    }
    public setAsseturl(asset_url: string, form:boolean = true){
        this.asset.asset_url = asset_url;
        if (form)            
            this.form.get('asset_url').patchValue(this.asset.asset_url);    
    }
    public changePreview(duration: number){
        this.setDuration(duration);
    }
    public changeInputfile(file: File){
        this.setFile(file);
    }
    public deleted(){
        this.setAsseturl('');
        this.setType(null); 
        this.setSize(null); 
        this.setDuration(null);   
    }
    public close() {
        this.dialogRef.close(null);
    }
    changeType(idtype: number){
        this.asset.idtype = idtype;
        this.hiddenInputmedia = (idtype == ASSET_TYPES.url || idtype == ASSET_TYPES.template);
        console.log(idtype, ASSET_TYPES.template)
        if(idtype == ASSET_TYPES.template){
            this.form.controls['asset_url'].disable()
            this.disabledURL = true;
        }else{
            this.form.controls['asset_url'].enable()
        }
        
        console.log(this.disabledURL);
    }
    onBlurUrl() {
        this.asset.asset_url = this.form.get('asset_url').value;
    }
}