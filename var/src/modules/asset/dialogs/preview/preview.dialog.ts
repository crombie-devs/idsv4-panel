import { Component, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Asset } from 'src/modules/asset/models/asset';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { AssetTypeStore } from 'src/modules/asset/stores/assettype.store';
import { MaxSizeValidator } from '@angular-material-components/file-input';


@Component({
    providers: [FormatStore, LangStore, AssetTypeStore],
	  selector: 'preview-dialog',
  	templateUrl: './preview.dialog.html',
  	styleUrls: ['./preview.dialog.scss']
})

export class PreviewDialog {   

    url: string ; 
    type: number;

	constructor(
        @Inject(MAT_DIALOG_DATA) public data: Asset,
        public dialogRef: MatDialogRef<PreviewDialog>) 
    {  }

    ngOnInit(){
    }
    

}