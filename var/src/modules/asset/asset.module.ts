import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/shared/shared.module';

import { FlexModule } from '@angular/flex-layout';

import { IvyCarouselModule } from 'angular-responsive-carousel';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from 'src/directive/directives.module';
import { MaterialModule } from 'src/app/material.module';
import { AssetRoutingModule } from './asset-routing.module';
import { PipesModule } from 'src/pipes/pipes.module';
import { TemplateModule } from 'src/modules/template/template.module';

import { AssetManagerPreviewComponent } from './components/assetmanagerpreview/assetmanagerpreview.component';
import { AssetManagerComponent } from './components/assetmanager/assetmanager.component';
import { AssetCarouselComponent } from './components/assetcarousel/assetcarousel.component';
import { AssetSlideComponent } from './components/asset/slide/asset.slide.component';
import { AssetGroupComponent } from './components/asset/group/asset.group.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { InputMediaComponent } from './components/input-media/input-media.component';
import { InputfileComponent } from './components/input-file/input-file.component';
import { PreviewComponent } from './components/preview/preview.component';

import { AssetDialog } from './dialogs/asset/asset.dialog';
import { PreviewDialog } from './dialogs/preview/preview.dialog';
import { AssetTypeStore } from './stores/assettype.store';

import { CONFIG } from 'src/app/app.config';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import moment from 'moment';
import { FormFieldModule } from 'src/modules/form-field/form-field.module';


import { AssetListPage } from './pages/list/list.page';
import { FormatLangAssetListPage } from './pages/format-lang-list/format-lang-list.page';

export function HttpLoaderFactory(http: HttpClient) {
  let url = CONFIG.lang_url; 
  return new TranslateHttpLoader(http, `${url}/i18n/`, `.json?timestamp=${moment().valueOf()}`);
}
@NgModule({
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    AssetManagerPreviewComponent,
    AssetManagerComponent,
    AssetCarouselComponent,
    AssetSlideComponent,
    AssetGroupComponent,
    CarouselComponent,
    InputfileComponent,
    InputMediaComponent,
    PreviewComponent,
    PreviewDialog, 
    AssetDialog,
    AssetListPage,
    FormatLangAssetListPage
  ],
  imports: [
    CommonModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    IvyCarouselModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    NgxMatFileInputModule,
    FlexModule,
    SharedModule,
    DirectivesModule,
    AssetRoutingModule,
    PipesModule,
    FormFieldModule,
    TemplateModule
  ],
  exports:[
    AssetManagerPreviewComponent,
    AssetManagerComponent,
    AssetCarouselComponent,
    AssetSlideComponent,
    AssetGroupComponent,
    CarouselComponent,
    InputfileComponent,
    InputMediaComponent,
    PreviewComponent,
    PipesModule
  ],
  entryComponents: [PreviewDialog, AssetDialog],
  providers: [AssetTypeStore],
})
export class AssetModule {
  constructor(private translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('es');
    if(!sessionStorage.getItem('lang')){
      sessionStorage.setItem('lang', 'es');
      translate.use('es');
    }else{
      translate.use(sessionStorage.getItem('lang'));
    }
  }
}