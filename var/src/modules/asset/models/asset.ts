import { Format, IFormat } from 'src/modules/format/models/format';
import { ILang, Lang } from 'src/modules/lang/models/lang';

export const ASSET_TYPES = {image: 1, video: 2, url: 3, template: 4, audio:5};

export interface IAsset {
    idasset: number;
    idcustomer?: number;
    idsite?: number;
    idtype: number;
    name: string;
    asset_url?: string;
    cover_url?: string;
    duration: number;
    format: IFormat;
    size: number;
    lang: ILang;
    default: boolean;
    has_audio: boolean;
    is_widget: number;
    idlandingpage: number;
    asset: File;
    contents?: any[];
    expired_date: string;
    delay:number;
}

export class Asset implements IAsset {
    idasset: number;
    idcustomer?: number;
    idsite?: number;
    idtype: number;
    name: string;
    asset_url?: string;
    cover_url?: string;
    duration: number = 0;
    format: Format;
    size: number;
    lang: Lang;
    default: boolean;
    has_audio: boolean;
    is_widget: number;
    idlandingpage: number;
    asset: File = null;
    contents?: any[];
    expired_date: string;

    selected: boolean = false;
    delay: number;
    
    constructor(obj?: IAsset) {
        if(!obj)
            return;
            
        this.idasset = obj.idasset;
        this.idcustomer = obj.idcustomer;
        this.idsite = obj.idsite;
        this.idtype = obj.idtype;
        this.name = obj.name;
        this.asset_url = obj.asset_url;
        this.cover_url = obj.cover_url;
        this.duration = obj.duration?obj.duration:0;
        this.format = new Format(obj.format);
        this.size = obj.size;
        this.lang = new Lang(obj.lang)
        this.default = obj.default;
        this.has_audio = obj.has_audio;
        this.is_widget = obj.is_widget;
        this.idlandingpage = obj.idlandingpage;
        if(obj.asset)
            this.asset = obj.asset;
        if(obj.contents)
            this.contents = obj.contents;
        this.expired_date = obj.expired_date;
        this.delay = obj.delay;
    }

    isEquals(asset: Asset): boolean {
        return (this.idasset === asset.idasset);
    }

    check(lang: Lang, format: Format, is_widget:number): boolean {
        return (this.lang.isEquals(lang) && this.format.isEquals(format) && this.is_widget == is_widget);
    }

    typeIconName(): string {
        let icon_name: string = 'link';
        if (this.idtype == ASSET_TYPES.image)
            icon_name='photo_camera';
        else if(this.idtype == ASSET_TYPES.video)
            icon_name='play_arrow';
        else if(this.idtype == ASSET_TYPES.audio)
            icon_name='music_video';
        else if(this.idtype == ASSET_TYPES.url)
            icon_name='public';
        return icon_name;
    }
}