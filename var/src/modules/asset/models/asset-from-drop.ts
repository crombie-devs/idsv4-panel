import { Format, IFormat } from 'src/modules/format/models/format';
import { ILang, Lang } from 'src/modules/lang/models/lang';

export const ASSET_TYPES = {image: 1, video: 2, url: 3, template: 4};

export interface IAssetFromDrop {
    name: string;
    //duration?: number | null;
    duration?: number | null;
    format: IFormat;
    size: number;
    lang: ILang;
    asset: File;
    is_widget: boolean;
    position?:number;
    delay:number;
}

export class AssetFromDrop implements IAssetFromDrop {
    name: string;
    //duration?: number = 0;
    duration?: number | null = null;
    format: Format;
    size: number;
    lang: Lang;
    asset: File = null;
    is_widget:boolean;
    position?:number;
    delay:number;
    constructor(obj?: IAssetFromDrop) {
        if(!obj)
            return;
            
        this.name = obj.name;
        this.duration = obj.duration?obj.duration:null;
        this.format = new Format(obj.format);
        this.size = obj.size;
        this.lang = new Lang(obj.lang);
        this.is_widget = obj.is_widget;
        this.position = obj.position;
        this.delay = obj.delay;
        if(obj.asset)
            this.asset = obj.asset;
    }
}