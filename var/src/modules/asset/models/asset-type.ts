export interface IAssetType {
    idtype: number;
    name: string;
}

export class AssetType implements IAssetType {
    idtype: number;
    name: string;
    
    constructor(obj?: IAssetType) {
        if(!obj)
            return;
            
        this.idtype = obj.idtype;
        this.name = obj.name;
    }
}