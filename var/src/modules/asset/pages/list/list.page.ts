import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Asset, ASSET_TYPES } from 'src/modules/asset/models/asset';
import { AssetFromDrop } from 'src/modules/asset/models/asset-from-drop';
import { Format } from 'src/modules/format/models/format';
import { Lang } from 'src/modules/lang/models/lang';

import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { MatDialog } from '@angular/material/dialog';
import { TemplateDialog } from 'src/modules/template/dialogs/template/template.dialog';
import { Landingpage } from 'src/modules/landing-page/models/landingpage';
import { MatMenuTrigger } from '@angular/material/menu';
import { LandingpageDialog } from 'src/modules/landing-page/dialogs/landingpage/landingpage.dialog';
import { MYString } from 'src/libs/string.libs';
import { User } from 'src/modules/user/models/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AssetDialog } from 'src/modules/asset/dialogs/asset/asset.dialog';

interface IGroup {
  lang?: Lang,
  format?: Format,
  assets: Asset[],
  name?: string
}

@Component({
  providers: [FormatStore, LangStore],
  selector: 'asset-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss']
})
export class AssetListPage implements OnInit {

  @ViewChild('menuTrigger' , {static: true}) menuTrigger: MatMenuTrigger;
  
  public asset: AssetFromDrop = new AssetFromDrop();
  public loading: boolean = true;
  public permission: boolean = false;

  public groups: IGroup[] = [];
  public assets: Asset[] = [];
  public assetsTemp: Asset[] = [];
  public formats: Format[] = [];
  public langs: Lang[] = [];
  public filterName: string = '';
  
  public slidesToShow: number = 7;

  public landingpages: Landingpage[] = [];
  public me: User;
  public is_words: string = "";
  
  constructor(
    private ws: WSService, 
    private ustore: UserStore,
    private fstore: FormatStore,
    private lstore: LangStore,
    private _snackBar: MatSnackBar,
    private translate:TranslateService,
    private matDialog: MatDialog) 
  {
    this.ustore.onChange.subscribe(user => { 
      this.me = user; 
      this.permission = ustore.hasPermissionByName('get.assets');
    });
  }

  ngOnInit() { 
   this.run();
  }

  run(){
    const that = this;
    let count: number = 3;
    this.fstore.onChange.subscribe(formats => {
      this.formats = formats;
      decrement();
    });

    this.lstore.onChange.subscribe(langs => {
      this.langs = langs;
      decrement();
    });

    this.ws.assets().subscribe((result: IResult) => {
	    if(result.success) {
        this.permission = true;
        this.assets = result.data.map(d => new Asset(d));
        this.assetsTemp = [...this.assets];
        if(this.is_words) {
          if( this.is_words.length > 0 ) {
            this.proccessData(this.is_words)
          }
        } else {
          decrement();
        }
      }
    }); 

    this.ws.landingpages().subscribe((result: IResult) => {
	    if(result.success) 
        this.landingpages = result.data.map(lp => new Landingpage(lp));      
    });

    function decrement() {
      count--; that.loading = (count > 0);
      if(!that.loading){ that.proccessData(); }
    }
  }
  
  private proccessData(filter?: string, assets?: Asset[]) {
    this.groups = [];
    const is_widget = 0;
    this.is_words = filter;
    this.langs.forEach((lang: Lang) => {
      this.formats.forEach((format: Format) => {
        let assets =  this.assetsTemp.filter(a => a.check(lang, format,is_widget));
        if(filter && filter.length > 0){     
          assets = assets.filter(a =>  {          
            return  MYString.removeAccents(a.name).toLowerCase().indexOf(MYString.removeAccents(filter).toLowerCase()) > -1
          }); 
        }           
        const group: IGroup = {'lang': lang, 'format': format, 'assets': assets};
        this.groups.push(group);
      });
    });
    this.groups.sort(this.assetsGroupOrder)
    let widgets:Asset[] = this.assets.filter(asset => asset.is_widget === 1);
    if(widgets.length > 0)
      this.groups.push({ name: 'Widgets', assets:widgets});
    if(this.is_words) {
     if(this.is_words.length > 0) {
      this.loading = false
     }
    }
  } 
  
  assetsGroupOrder(a,b){
    if (a.format.name === b.format.name) {
      return b.lang.name - a.lang.name;
    }
    return a.format.name > b.format.name ? 1 : -1;
  }
  
  onChange(assets: Asset[]) {
    this.run();
  }

  onKeyup(event: KeyboardEvent) {
    this.proccessData(this.filterName);
  }

  clearFilter() {
    this.filterName = '';
    this.proccessData(this.filterName);
  }

  openNewForm(type = null) {
    let dialogRef;
    if(type)
      dialogRef = this.matDialog.open(AssetDialog, {data: {idtype: type}});
    else  
      dialogRef = this.matDialog.open(AssetDialog);
    dialogRef.afterClosed().subscribe((result: Asset) => {
      this.updateGroups(result);
    }); 
  }
  openNewFormFromDrop(asset) {
    const dialogRef = this.matDialog.open(AssetDialog, {data: asset});
    dialogRef.afterClosed().subscribe((result: Asset) => {
      this.run();
      this.updateGroups(result);
    });
  }
  private updateGroups(asset:Asset){
    console.log(asset);
    if(asset){
      let found:boolean = false;
    
      this.groups.map((group,i) => {
        if(group.format.idformat === asset[0].format.idformat && group.lang.idlang === asset[0].lang.idlang){
          group.assets.map((a,j) => {
            if(a.idasset === asset[0].idasset){
                this.groups[i].assets[j] = asset[0];
                found = true;
            }
          })
          if(!found)
            this.groups[i].assets = [...this.groups[i].assets, new Asset(asset[0])];
          this.groups[i].assets = this.groups[i].assets.sort((n1,n2)=> n2.idasset - n1.idasset);
        }
      })
    }
  }
  openNewTemplate() {
    const dialogRef = this.matDialog.open(TemplateDialog);
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result){
        let asset: Asset = new Asset();
        asset.asset_url = result.url;
        asset.idtype = ASSET_TYPES.template;
        asset.delay = 5;
        const dialogRef = this.matDialog.open(AssetDialog, {data: asset});
        dialogRef.afterClosed().subscribe((result: Asset) => {
          this.run();
        });
      }
    }); 
  }
  OnFileDropped(data:any){
    const size = this.toMb(data.file.file.size);
    if(size > this.me.customer.max_file_size){
      this.openSnackBar(`${this.translate.instant('El archivo no puede exceder de ')} ${this.me.customer.max_file_size}mb`);
      return;
    }
    let is_widget = false;
    if(!data.lang && !data.format)
      is_widget = true
    const asset = new AssetFromDrop({
      name: data.file.file.name,
      duration: 0,
      format: data.format,
      size: data.file.file.size,
      lang: data.lang,
      asset: data.file.file,
      is_widget: is_widget,
      delay:data.delay
    })
    this.openNewFormFromDrop(asset)
  }
  toMb(size){
    return size / (1024*1024);
  }
  public openDialogLandingpage(landingpage: Landingpage){
    const dialogRef =  this.matDialog.open(
      LandingpageDialog, 
      {
        data: { "landingpage":landingpage, asset: null}, 
        width: 'auto',
        height: 'auto',
        minHeight: '600px',
        minWidth:'800px'
      }
    );
    dialogRef.afterClosed().subscribe((result: any) => {  
      this.run();
    }); 
  }
  public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
}