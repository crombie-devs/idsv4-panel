import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Asset, ASSET_TYPES } from 'src/modules/asset/models/asset';
import { AssetFromDrop } from 'src/modules/asset/models/asset-from-drop';
import { Format } from 'src/modules/format/models/format';
import { Lang } from 'src/modules/lang/models/lang';
import { ActivatedRoute, Router } from '@angular/router';

import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { MatDialog } from '@angular/material/dialog';
import { TemplateDialog } from 'src/modules/template/dialogs/template/template.dialog';
import { Landingpage } from 'src/modules/landing-page/models/landingpage';
import { MatMenuTrigger } from '@angular/material/menu';
import { LandingpageDialog } from 'src/modules/landing-page/dialogs/landingpage/landingpage.dialog';
import { MYString } from 'src/libs/string.libs';
import { User } from 'src/modules/user/models/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AssetDialog } from 'src/modules/asset/dialogs/asset/asset.dialog';
import { FileHandle } from 'src/directive/drag-drop.directive';

interface IGroup {
  lang?: Lang,
  format?: Format,
  assets: Asset[],
  name?: string
}

@Component({
  providers: [FormatStore, LangStore],
  selector: 'format-lang-asset-list',
  templateUrl: './format-lang-list.page.html',
  styleUrls: ['./format-lang-list.page.scss']
})
export class FormatLangAssetListPage implements OnInit {

  @ViewChild('menuTrigger' , {static: true}) menuTrigger: MatMenuTrigger;
  @ViewChild('gridContainer', {static: true}) carouselContainer: ElementRef;
  
  @HostListener('window:resize', ['$event.target']) 
	onResize(event) {
	  this.resizeWorks(event.innerWidth);
	}

  public asset: AssetFromDrop = new AssetFromDrop();
  public loading: boolean = true;
  public permission: boolean = false;

  public idformat:number;
  public idlang:number;
  public groups: IGroup[] = [];
  public assets: Asset[] = [];
  public assetsTemp: Asset[] = [];
  public format: Format;
  public lang: Lang;
  public filterName: string = '';
  
  public slidesToShow: number = 7;

  public landingpages: Landingpage[] = [];
  public me: User;
  public is_words: string = "";
  
  start: number = 0;
  limit: number = 30;
  end: number = this.limit + this.start;

  public assetData: Asset[];
  public loadedAssets: Asset[] = [];


  constructor(
    private ws: WSService, 
    private ustore: UserStore,
    private fstore: FormatStore,
    private lstore: LangStore,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private translate:TranslateService,
    private matDialog: MatDialog) 
  {
    this.idformat = Number(this.route.snapshot.paramMap.get('idformat'));
    this.idlang = Number(this.route.snapshot.paramMap.get('idlang'));

    this.ustore.onChange.subscribe(user => { 
      this.me = user; 
      this.permission = ustore.hasPermissionByName('get.assets');
    });
    this.resizeWorks(window.innerWidth);
  }
  resizeWorks(width){
    this.slidesToShow = Math.floor((width * 7) / 1920);
  }
  ngOnInit() { 
   this.run();
  }

  run(){
    const that = this;
    let count: number = 3;
    this.fstore.onChange.subscribe(formats => {
      this.format = formats.filter(f => f.idformat === this.idformat )[0];
      decrement();
    });

    this.lstore.onChange.subscribe(langs => {
      this.lang = langs.filter(l => l.idlang === this.idlang)[0];
      decrement();
    });

    this.ws.assets().subscribe((result: IResult) => {
	    if(result.success) {
        this.permission = true;
        this.assets = result.data.map(d => new Asset(d)).filter(a => a.format.idformat === this.idformat && a.lang.idlang === this.idlang);
        this.assetsTemp = [...this.assets];
        this.assetData = this.assets;
        this.loadedAssets = this.getGridListData(this.start, this.end);
        if(this.is_words) {
          if( this.is_words.length > 0 ) {
            this.proccessData(this.is_words)
          }
        } else {
          decrement();
        }
      }
    }); 

    this.ws.landingpages().subscribe((result: IResult) => {
	    if(result.success) 
        this.landingpages = result.data.map(lp => new Landingpage(lp));      
    });

    function decrement() {
      count--; that.loading = (count > 0);
      if(!that.loading){ that.proccessData(); }
    }
  }
  
  private proccessData(filter?: string) {
    this.groups = [];
    const is_widget = 0;
    this.is_words = filter;
 
    let assets =  this.assetsTemp.filter(a => a.check(this.lang, this.format,is_widget));
    if(filter && filter.length > 0){     
      assets = assets.filter(a =>  {          
        return  MYString.removeAccents(a.name).toLowerCase().indexOf(MYString.removeAccents(filter).toLowerCase()) > -1
      }); 
    }           
    const group: IGroup = {'lang': this.lang, 'format': this.format, 'assets': assets};
        this.groups.push(group);

      this.groups.sort(this.assetsGroupOrder)
    let widgets:Asset[] = this.assets.filter(asset => asset.is_widget === 1);
    if(widgets.length > 0)
      this.groups.push({ name: 'Widgets', assets:widgets});
    if(this.is_words) {
     if(this.is_words.length > 0) {
      this.loading = false
     }
   }

  } 
  
  assetsGroupOrder(a,b){
    if (a.format.name === b.format.name) {
      return b.lang.name - a.lang.name;
    }
    return a.format.name > b.format.name ? 1 : -1;
  }
  
  onChange(assets: Asset[]) {
    this.run();
  }

  onKeyup(event: KeyboardEvent) {
    this.proccessData(this.filterName);
  }

  clearFilter() {
    this.filterName = '';
    this.proccessData(this.filterName);
  }

  openNewForm(type = null) {
    let dialogRef;
    if(type)
      dialogRef = this.matDialog.open(AssetDialog, {data: {idtype: type, format: this.format, lang: this.lang}});
    else  
      dialogRef = this.matDialog.open(AssetDialog);
    dialogRef.afterClosed().subscribe((asset: Asset) => {
      if(asset)
        this.loadedAssets.unshift(asset);
    }); 
  }
  filesDropped(file: FileHandle[]): void {
    const data = {
      file:file[0],
      lang:this.lang,
      format:this.format
    }
    this.OnFileDropped(data);
  }

  openNewFormFromDrop(asset) {
    const dialogRef = this.matDialog.open(AssetDialog, {data: asset});
    dialogRef.afterClosed().subscribe((asset: Asset) => {
      if(asset){
        this.loadedAssets.unshift(asset);
      }
    });
  }
  openNewTemplate() {
    const dialogRef = this.matDialog.open(TemplateDialog);
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result){
        let asset: Asset = new Asset();
        asset.asset_url = result.url;
        asset.idtype = ASSET_TYPES.template;
        const dialogRef = this.matDialog.open(AssetDialog, {data: asset});
        dialogRef.afterClosed().subscribe((result: Asset) => {
          this.run();
        });
      }
    }); 
  }
  OnFileDropped(data:any){
    const size = this.toMb(data.file.file.size);
    if(size > this.me.customer.max_file_size){
      this.openSnackBar(`${this.translate.instant('El archivo no puede exceder de ')} ${this.me.customer.max_file_size}mb`);
      return;
    }
    let is_widget = false;
    if(!data.lang && !data.format)
      is_widget = true
    const asset = new AssetFromDrop({
      name: data.file.file.name,
      duration: 0,
      format: this.format,
      size: data.file.file.size,
      lang: this.lang,
      asset: data.file.file,
      is_widget: is_widget,
      delay: data.delay
    })
    this.openNewFormFromDrop(asset)
  }
  toMb(size){
    return size / (1024*1024);
  }
  public openDialogLandingpage(landingpage: Landingpage){
    const dialogRef =  this.matDialog.open(
      LandingpageDialog, 
      {
        data: { "landingpage":landingpage, asset: null}, 
        width: 'auto',
        height: 'auto',
        minHeight: '600px',
        minWidth:'800px'
      }
    );
    dialogRef.afterClosed().subscribe((result: any) => {  
      this.run();
    }); 
  }
  public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
  public onGridListScroll(e) {
    const tableViewHeight = e.target.offsetHeight; // viewport
    const tableScrollHeight = e.target.scrollHeight; // length of all table
    const scrollLocation = e.target.scrollTop; // how far user scrolled
    // If the user has scrolled within 200px of the bottom, add more data
    const buffer = 200;
    const limit = tableScrollHeight - tableViewHeight - buffer;
    if (scrollLocation !== 0 && limit > -200) {
      if (scrollLocation > limit) {
        let data = this.getGridListData(this.start, this.end);
        this.loadedAssets = this.loadedAssets.concat(data);
        const loadSet: any = new Set();
        this.loadedAssets.forEach(ass => {
          loadSet.add(JSON.stringify(ass));
        });
        let ass = [];
        loadSet.forEach(element => {
          ass.push(new Asset(JSON.parse(element)));   
        });
        this.loadedAssets = ass;
        this.updateIndex();
      }
    }
  }
  updateIndex() {
    this.start = this.end;
    this.end = this.limit + this.start;
  }
  getGridListData(start, end) {
    return this.assets.filter((value, index) => index >= start && index < end);
  }
  onDeleted(asset: Asset) {
    console.log(asset);
    if(asset) {
      this.loadedAssets = this.loadedAssets.filter(a => !a.isEquals(asset));
    }
  }

  onUpdated(asset: Asset) {
    if(asset) {
      this.loadedAssets = this.loadedAssets.map(a => (a.isEquals(asset))? asset : a );
    }
  }
}