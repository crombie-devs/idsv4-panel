import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListPage } from './pages/list/list.page';
import { ProductsCategoriesPage } from './pages/products-categories/products-categories.page';
const routes: Routes = [
  { path: '', component: ProductListPage},
  { path: 'products-categories', component: ProductsCategoriesPage }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ProductRoutingModule { }
