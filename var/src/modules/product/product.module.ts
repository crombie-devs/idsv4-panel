import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FlexLayoutModule} from "@angular/flex-layout";
import {FormsModule} from "@angular/forms";
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import { DirectivesModule } from 'src/directive/directives.module';
import { ProductSelectComponent } from './components/productselect/productselect.component';
import { ProductRoutingModule } from './product-routing.module';
import { ProductSelectDialog } from './dialogs/productselect/productselect.dialog'; 
import { ProductCategoryFormDialog } from "./dialogs/product-category-form/product-category-form.dialog";
import { ProductFormDialog } from "./dialogs/product-form/product-form.dialog";
import { MaterialModule } from 'src/app/material.module';
import { ProductListPage } from './pages/list/list.page';
import { ProductsCategoriesPage } from './pages/products-categories/products-categories.page';
import { SharedModule } from 'src/shared/shared.module';
import { FormFieldModule } from "src/modules/form-field/form-field.module";

@NgModule({
  declarations: [ProductSelectComponent, ProductSelectDialog,ProductFormDialog,ProductCategoryFormDialog, ProductListPage, ProductsCategoriesPage],
  imports: [
    CommonModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    DirectivesModule,
    MaterialModule,
    ProductRoutingModule,
    SharedModule,
    FormFieldModule
  ],
  entryComponents:[ProductSelectDialog, ProductCategoryFormDialog, ProductFormDialog],
  exports: [ProductSelectComponent],
})
export class ProductModule {}