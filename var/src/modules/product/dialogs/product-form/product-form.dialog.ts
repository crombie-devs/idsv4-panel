import { Component, Inject } from "@angular/core";
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {WSService} from "src/services/ws.service";
import {ProductCategory} from "src/modules/product/models/product-category";
import {Product} from "src/modules/product/models/product";
import {DebugStore} from "src/stores/debug.store";

import { FormControl, Validators, FormGroup } from '@angular/forms';
import { FormField } from 'src/modules/form-field/models/form-field';

@Component({
  selector: "product-form-dialog", 
  templateUrl: "./product-form.dialog.html", 
  styleUrls: ["./product-form.dialog.scss"]
})

export class ProductFormDialog {
  public title: string;
  public load = false;
  public category: ProductCategory;

  public fieldName: FormField = {
		label: "Nombre",
    placeholder: 'Nombre de categoría',
		name: 'name',
		required: true
  };
  public fieldCode: FormField = {
		label: "Código",
    placeholder: 'Código de categoría',
		name: 'code',
		required: true
  };
  public formGroup:FormGroup = new FormGroup({
    name: new FormControl(null, [Validators.required]),
    code: new FormControl(null, [Validators.required]),
    idcategory: new FormControl(null, [Validators.required])
  });

  constructor(
    public dialogRef : MatDialogRef<ProductFormDialog>,  
    @Inject(MAT_DIALOG_DATA) public data : Product, 
    private ws : WSService) {}

  public ngOnInit() {
    if(this.data)
      this.initForm();
  }

  public initForm(){
    this.formGroup.patchValue({name: this.data.name, code: this.data.code, idcategory: this.data.idcategory});
  }

  public save() {
    const values = this.formGroup.getRawValue();
    if(this.data.idproduct)
      this.ws.editProduct(this.data.idproduct, values).subscribe(result => {
        if(result.success){
          this.dialogRef.close(result.data);
        }
      })
    else
      this.ws.createProduct(values).subscribe(result => {
        if(result.success){
          this.dialogRef.close(result.data);
        }
      })
  }

  public close() {
    this.dialogRef.close(false);
  }
}
