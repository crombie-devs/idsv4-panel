import {
  Component,
  Inject,
  QueryList,
  ViewChildren,
  ElementRef,
  Renderer2,
  ViewChild
} from "@angular/core";
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {WSService} from "src/services/ws.service";
import {Product} from "src/modules/product/models/product";
import {DebugStore} from "src/stores/debug.store";
import {MatTableDataSource} from "@angular/material/table";
import {SelectionModel} from "@angular/cdk/collections";
import { User } from 'src/modules/user/models/user';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: "productselect-dialog", 
  templateUrl: "./productselect.dialog.html", 
  styleUrls: ["./productselect.dialog.scss"]
})

export class ProductSelectDialog {
  public title: string;
  public load = true;
  public products: Product[] = [];
  public categories: any[] = [];
  public categoriesData: any[] = [];
  public selectedproducts: Product[] = [];
  public displayedColumns: string[] = ["select", "code", "name"];
  public dataSource = new MatTableDataSource<Product>();
 
  public productData: Product[];
  public selection = new SelectionModel(true, []);
  public me: User;
  public loadedProducts: Product[] = [];
  public dataSourceTemp: any[] = [];
  public dataSourceFilterTemp: any[] = [];
  @ViewChildren("categoryElem")categoryElem: QueryList<ElementRef>;
  @ViewChild("productFilter") productFilter: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  start: number = 0;
  limit: number = 15;
  end: number = this.limit + this.start;
  oldcategory: number = null;
  categorySelected: number;

  constructor(
    public dialogRef : MatDialogRef<ProductSelectDialog>,
    @Inject(MAT_DIALOG_DATA) public data : Product[], 
    private ws : WSService, 
    private debug : DebugStore, 
    private ustore : UserStore, 
    private renderer : Renderer2) {}

  public ngOnInit() {
    this.me = this.ustore.getUser();
    this.getProducts();
    this.updateIndex();
  }

  public save() {
    this.selectedproducts.map((element, index) => {
      if (!element.hasOwnProperty("idproduct")) {
        const product = this.products.filter((item) => item.name == element.name)[0];
        this.selectedproducts[index] = product;
      }
    });
    this.dialogRef.close(this.selectedproducts);
  }

  public close() {
    this.dialogRef.close(this.data);
  }
  private getProducts() {
    this.ws.getProductsFromCustomer().subscribe((result) => {
      if (result.success) {
        this.products = result.data.sort((a,b) => {
          if (a.name < b.name) {
            return -1;
          }
          if (a.name > b.name) {
            return 1;
          }
          return 0;
        });
        this.dataSourceFilterTemp = [...this.products];
        this.products.map((product) => {
          product["selected"] = false;
          if (product.idcategory && !this.categories.some((c) => c.idcategory === product.idcategory))
            if (product.category) 
              this.categories.push(product.category);
        }); 
        this.categoriesData = this.categories.sort((a,b) => {
          if (a.name < b.name) {
            return -1;
          }
          if (a.name > b.name) {
            return 1;
          }
          return 0;
        });
      
        if (this.data) {
          this.products = this.setSelecteds(this.data);
        }
        this.load = false;
        this.productData = this.products;
        this.loadedProducts = this.getTableData(this.start, this.end);
       
        this.updateIndex();
      }
    });
  }
  public filterCategory(q) {
    this.categoriesData = this.categories.filter((c) => c.name.toLowerCase().includes(q.trim().toLowerCase()) || c.code.includes(q.trim()));
  }

  public filterProducts(category, index) {
    this.productData = [];
    this.productFilter.nativeElement.value = '';
    if(this.oldcategory == index) {
      let e = this.categoryElem.toArray()[index].nativeElement;
      this.renderer.setStyle(e, "background-color", "white");
      this.renderer.setStyle(e, "color", "black");
      this.oldcategory = null;
      this.categorySelected = null;
      this.loadedProducts = [];
    } else{
      this.oldcategory = index;
      this.categoryElem.toArray().forEach((elem, i) => {
        let e = elem.nativeElement;
        this.renderer.setStyle(e, "background-color", "white");
        this.renderer.setStyle(e, "color", "black");
        if (i === index) {
          this.renderer.setStyle(e, "background-color", "#64ca26");
          this.renderer.setStyle(e, "color", "white");
        }
      });
      this.resetStartEnd();
      this.loadedProducts = [];
      this.categorySelected = category;
      this.productData = this.products.filter((p) => p.idcategory === category);
      this.loadedProducts = this.getTableData(this.start, this.end);
      if(!this.productData.some(p => this.selectedproducts.some(s => s.idproduct === p.idproduct)))
        this.selection.clear();
      else{
        this.productData.map((row) => {
          if(this.selectedproducts.some(s => s.idproduct === row.idproduct))
            this.selection.select(row)
        });
        this.setSelecteds(this.selection.selected);
      }
    }
    this.renderDataTable(this.loadedProducts);
  }

  public renderDataTable(data : Product[]) {
    this.dataSource = new MatTableDataSource<Product>();
    this.dataSource.data = data;
    this.dataSource.sort = this.sort;
    this.dataSourceTemp = [...this.dataSource.data];
    this.load = false;
  }

  resetStartEnd() {
    this.start = 0;
    this.end = this.limit + this.start;
  }

  public isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.productData.length;
    return numSelected >= numRows;
  }

  public masterToggle() {
    if(this.isAllSelected()){
      this.productData.map(p => {
        p['selected'] = false;
        this.selectedproducts = this.selectedproducts.filter(sp => sp.code !== p.code);
        this.selection.deselect(p);
      });
      this.loadedProducts.map(p => p['selected'] = false);
    }else{
      this.productData.map((row) => this.selection.select(row));
      this.loadedProducts.map(p => p['selected'] = true);
      this.setSelecteds(this.selection.selected, false);
    } 
    this.renderDataTable(this.loadedProducts);
  }

  public selectRow($event, dataSource) {
    if ($event.checked || dataSource === false) {
      dataSource.selected = true;
      this.selectedproducts.push(dataSource);
    } else {
      dataSource.selected = false;
      this.selectedproducts.forEach((selectedProduct, i) => {
        if (selectedProduct.code == dataSource.code) 
          this.selectedproducts.splice(i, 1);
        }
      );
    }
  }

  private setSelecteds(selectedProducts:Product[], res:boolean = true) {
    this.products.map((product) => {
      product["selected"] = false;
      selectedProducts.map((selectedProduct) => {
        if (product.code == selectedProduct.code) 
          product["selected"] = true;
        }
      );
    });
    if(res)
      this.selectedproducts = [];
    selectedProducts.map((selectedProduct) => {
      this.selectedproducts.push(selectedProduct);
    });
    if (res)
      return this.products;
  }

  public filterProduct(q: string) {
    this.resetStartEnd();
    this.loadedProducts = [];
    if(!q) {
      this.dataSource.data = this.dataSourceFilterTemp.filter(item => item.idcategory == this.categorySelected)
    } else {   
      if(this.categorySelected) {
        this.productData =  this.dataSourceFilterTemp.filter(item => item.idcategory == this.categorySelected).filter(product => 
        product.name.toLowerCase().includes(q.trim().toLowerCase()))
      } else {
        this.productData =  this.dataSourceFilterTemp.filter(product => 
        product.name.toLowerCase().includes(q.trim().toLowerCase()))
      }   
      if(this.productData.length == 0)
        this.productData = this.products.filter(product => String(product.code).includes(q.trim()));  
    }
    this.loadedProducts = this.getTableData(this.start, this.end);
    this.renderDataTable(this.loadedProducts);
  }

  public onTableScroll(e) {
    const tableViewHeight = e.target.offsetHeight; // viewport
    const tableScrollHeight = e.target.scrollHeight; // length of all table
    const scrollLocation = e.target.scrollTop; // how far user scrolled
    // If the user has scrolled within 200px of the bottom, add more data
    const buffer = 200;
    const limit = tableScrollHeight - tableViewHeight - buffer;
    if (scrollLocation !== 0 && limit > -200) {
      if (scrollLocation > limit) {
        let data = this.getTableData(this.start, this.end);
        this.loadedProducts = this.loadedProducts.concat(data);
        const loadSet: any = new Set();
        this.loadedProducts.forEach(pr => {
            loadSet.add(JSON.stringify(pr));
        });
        let prod = [];
        loadSet.forEach(element => {
          prod.push(JSON.parse(element));   
        });
        this.loadedProducts = prod;
        this.updateIndex();
        this.renderDataTable(this.loadedProducts);
      }
    }
  }
  
  updateIndex() {
    this.start = this.end;
    this.end = this.limit + this.start;
  }

  getTableData(start, end) {
    return this.productData.filter((value, index) => index >= start && index < end);
  }
}
