import { Component, Inject } from "@angular/core";
import { FormControl, Validators, FormGroup } from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {WSService} from "src/services/ws.service";
import {ProductCategory} from "src/modules/product/models/product-category";
import { FormField } from 'src/modules/form-field/models/form-field';
import {DebugStore} from "src/stores/debug.store";

@Component({
  selector: "product-category-form-dialog", 
  templateUrl: "./product-category-form.dialog.html", 
  styleUrls: ["./product-category-form.dialog.scss"]
})

export class ProductCategoryFormDialog {
  public title: string;
  public load = false;
  public category: ProductCategory;

  public fieldName: FormField = {
		label: "Nombre",
    placeholder: 'Nombre de categoría',
		name: 'name',
		required: true
  };
  public fieldCode: FormField = {
		label: "Código",
    placeholder: 'Código de categoría',
		name: 'code',
		required: true
  };
  public formGroup:FormGroup = new FormGroup({
    name: new FormControl(null, {validators: [Validators.required]}),
    code: new FormControl(null, {validators: [Validators.required]})
  });

  constructor(
    public dialogRef : MatDialogRef<ProductCategoryFormDialog>,  
    @Inject(MAT_DIALOG_DATA) public data : ProductCategory, 
    private ws : WSService) {}

  public ngOnInit() {
    if(this.data)
      this.initForm();
  }

  public initForm(){
    this.formGroup.patchValue({name: this.data.name, code: this.data.code});
  }

  public save() {
    const values = this.formGroup.getRawValue();
    if(this.data)
      this.ws.editProductCategory(this.data.idcategory, values).subscribe(result => {
        if(result.success){
          this.dialogRef.close(result.data);
        }
      })
    else
      this.ws.createProductCategory(values).subscribe(result => {
        if(result.success){
          this.dialogRef.close(result.data);
        }
      })
  }

  public close() {
    this.dialogRef.close(false);
  }
}
