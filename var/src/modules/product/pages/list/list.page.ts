import { Component, ViewChild } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Product } from 'src/modules/product/models/product';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Router } from '@angular/router';
import { Sort } from '@angular/material/sort';
import { MYString } from 'src/libs/string.libs';

@Component({
  selector: 'product-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss']
})

export class ProductListPage {

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
  
	public displayedColumns: string[] = ['code', 'name', 'category', 'tags', 'link'];
	public selection = new SelectionModel<Product>(true, []);
	public dataSource = new MatTableDataSource<Product>();
	public permission: boolean = false;
	public load: boolean = true;
	public new: boolean = false;
	public edit: boolean = false;
	public deleted: boolean = false;
	public customersToSelect: boolean = false;
	public me: any;	
	sortedData: Product[];
	products: any[];
	categories: any[]; 

  	constructor(
		private ws: WSService,
		private debug: DebugStore,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private dialog: DialogService,
		private router: Router) {
			this.ustore.onChange.subscribe(user => { 
				this.me = user; 
			});
			
  	}
  
  	public ngOnInit() {
		this.verifyPermissions();
		this.getProducts();
	}

	public getProducts(){
		this.ws.getProductsFromCustomer().subscribe((result: IResult) => {
			if(result.success) {
				this.products = result.data;
				this.permission = true;
				this.renderDataTable(this.products);
			} else {            
				this.load = false;
				return null;
			}
		}, error => {
			this.load = false;
			return null;          
		}); 		

	}
	public openDialog(product) {
		this.dialog.tagcategoryselect("Tags", product.tags,this.me.customer.idcustomer, 'P').afterClosed().subscribe((result) => {
			if(!result.cancel){
				let params:any = {
					idproduct: product.idproduct,
					idtags: []
				}
				result.tags.map(tag => {
					params.idtags.push(tag.idtag)
				})
				this.ws.updateProductTags(params).subscribe(result => {
					if(result.success){
						let product = result.data;
						this.products.map(prod => {
							if(prod.idproduct === product.idproduct){
								prod.tags = [];
								product.tags.map(tag => {
									prod.tags.push({name: tag.name, idtag: tag.idtag});
								})
							}
						})
						this.openSnackBar("El producto se ha actualizado correctamente.")
					}
				})
			}
		})
	}
	public renderDataTable(data: Product []) {
		this.dataSource = new MatTableDataSource(data);
		this.dataSource.filterPredicate = (data: Product, filter: string): boolean => {
			const dataStr = MYString.removeAccents(Object.keys(data).reduce((currentTerm: string, key: string) => {
			  return (currentTerm + (data as { [key: string]: any })[key] + '◬');
			}, '')).trim().toLowerCase();	  
			return dataStr.indexOf(MYString.removeAccents(filter.trim().toLowerCase())) != -1;
		};
		

		setTimeout(() => {
			this.dataSource.paginator = this.paginator;
  	  	}, 1000) 
		this.sortedData = data.slice();
		this.products = data;
		this.dataSource.sort = this.sort;
		this.load = false;
	}

	public isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	public masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource.data.forEach(row => this.selection.select(row));
	}

	public checkboxLabel(row?: Product): string {
		if (!row) {
		  return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
		}
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.code + 1}`;
	}	

	public update(){
		if (this.selection.selected.length > 1){
			this.dialog.warning('Aviso', 'Sólo puede editar un visor').subscribe();
		}else if(this.selection.selected.length == 0){
			this.dialog.warning('Aviso', 'Debe seleccionar un visor').subscribe();
		}else{
			this.router.navigateByUrl('/private/products-tags/edit/'+this.selection.selected[0].idproduct);
		}
	}

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

	public applyFilter(event: string) {
		if(typeof event === 'string'){
			this.dataSource.filter = event;
		
			if (this.dataSource.paginator) {
				this.dataSource.paginator.firstPage();
			}
		}
	}

	private verifyPermissions() {
		this.edit = true;
		this.customersToSelect = this.ustore.hasPermissionByName("get.customers.toselect");
	}

	ngAfterViewInit (){
		setTimeout(() => {
			this.dataSource.paginator = this.paginator;
		  }, 500) 
		this.dataSource.sort = this.sort;
	}

	sortData(sort: Sort) {
		const data = this.products.slice();
		if (!sort.active || sort.direction === '') {
		  this.sortedData = data;
		  return;
		}
	
		this.sortedData = data.sort((a, b) => {
		  const isAsc = sort.direction === 'asc';
		  switch (sort.active) {
			case 'code': return this.compare(a.code, b.code, isAsc);
			case 'name': return this.compare(a.name, b.name, isAsc);
			default: return 0;
		  }
		});
		this.dataSource.data = this.sortedData;
	}
	compare(a: number | string, b: number | string, isAsc: boolean) {
		return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
	}
	
}
