import { Component,   QueryList, ViewChildren, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { IResult } from 'src/models/iresult';
import { Product } from 'src/modules/product/models/product';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Router } from '@angular/router';
import { ProductCategory } from 'src/modules/product/models/product-category';
import { Content } from 'src/modules/content/models/content';
import { User } from 'src/modules/user/models/user';

@Component({ 
  selector: 'products-categories',
  templateUrl: './products-categories.page.html',
  styleUrls: ['./products-categories.page.scss']
})

export class ProductsCategoriesPage {  

	@ViewChildren("categoryElem")categoryElem: QueryList<ElementRef>;
	@ViewChildren("productElem")productElem: QueryList<ElementRef>;

	public permission: boolean = false;
	public load: boolean = true;
	public categories: ProductCategory[] = [];
	public filteredCategories: ProductCategory[] = [];
	public products: Product[] = [];
	public filteredProducts: Product[] = [];
	public contents: Content[] = []; 
	public filteredContents: Content[] = [];
	public catSelected: ProductCategory;
	public prodSelected: Product;
	public me:User;

  	constructor(
		private ws: WSService,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private dialog: DialogService,
		private router: Router,
		private renderer : Renderer2) {
			this.ustore.onChange.subscribe(user => { 
				this.me = user; 
			});
  	}
  
  	public ngOnInit() {
		this.verifyPermissions();
		this.getCategoriesProductsContents();
	}

	public getCategoriesProductsContents(){
		this.ws.getProductsCategoriesContentsFromUser().subscribe((result: IResult) => {
			if(result.success) {
				if(result.data.length > 0){
					this.filteredCategories = this.categories = result.data.map(cat => new ProductCategory(cat));
					this.catSelected = this.filteredCategories[0];
					this.selectCategory(0);
				}
				if(this.categories.length > 0){
					this.filteredProducts = this.products = this.categories[0].products;
					this.prodSelected = this.products[0];
					this.selectProduct(0);
				}
				if(this.products.length > 0)
					this.filteredContents = this.contents = this.products[0].contents;
				this.load = false;
			} else {            
				this.load = false;
			}
		}, error => {
			this.load = false;
		});
	}

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

	private verifyPermissions() {
		this.permission = this.ustore.hasPermissionByName("post.product");
	}	
	public selectCategory(index){
		if(index > -1){
			this.filteredProducts = this.products = [];
			this.catSelected = this.filteredCategories[index];
			if(this.catSelected.products){
				this.filteredProducts = this.products = this.catSelected.products;
				this.selectProduct(0);
			}
			this.contents = [];
			if(this.products.length > 0)
				this.filteredContents = this.contents = this.products[0].contents;
		}
	}
	public filterCategory(q: string) {
		if(!q) {
		  this.filteredCategories = this.categories;
		} else {   
			this.filteredCategories =  this.categories.filter(category => category.name.toLowerCase().includes(q.trim().toLowerCase()) || category.idcategory.toString().includes(q) ||category.code.includes(q))  
		}
		if(this.filteredCategories.length > 0)
			this.filteredProducts = this.products = this.filteredCategories[0].products;
		else
			this.filteredProducts = this.products = [];
		if(this.products.length > 0)
			this.filteredContents = this.contents = this.products[0].contents;
		else
			this.filteredContents = this.contents = [];
	}
	public selectProduct(index:number){
		if(index > -1){
			this.prodSelected = this.filteredProducts[index];
			if(this.products.length > 0)
				this.filteredContents = this.contents = this.products[index].contents;
		}
	}
	public filterProduct(q: string) {
		if(!q) {
		  this.filteredProducts = this.products;
		} else {   
			this.filteredProducts =  this.products.filter(product => product.name.toLowerCase().includes(q.trim().toLowerCase()) || product.idproduct.toString().includes(q) || product.code.includes(q))  
		}
		if(this.filteredProducts.length > 0)
			this.filteredContents = this.contents = this.products[0].contents;
		else
			this.filteredContents = this.contents = [];
	}
	public createCategory(){
		const title = "Crear categoría";
		this.dialog.productcategoryform(title,null).afterClosed().subscribe(result => {
			if(result){
				if(!result.products)
					result.products = [];
				if((this.categories[0] && this.categories[0].idcategory !== result.idcategory) || !this.categories[0])
					this.categories.unshift(result);
				if((this.filteredCategories[0] && this.filteredCategories[0].idcategory !== result.idcategory) || !this.filteredCategories[0])
					this.filteredCategories.unshift(result);
			}
		})
	}
	public editCategory(category){
		const title = "Crear categoría";
		this.dialog.productcategoryform(title,category).afterClosed().subscribe(result => {
			if(result){
				if(!result.products)
					result.products = [];
				this.categories.map((c,i) => {
					if(c.idcategory === category.idcategory)
						this.categories[i] = result;
				})
				this.filteredCategories.map((c,i) => {
					if(c.idcategory === category.idcategory)
						this.filteredCategories[i] = result;
				})
			}
		});
	}
	public deleteCategory(category){
		const title="Eliminar categoría";
		const message= `¿Realmente desea borrar la categoría ${category.name} y TODOS sus productos asociados?`;
		this.dialog.confirm(title, message, null).subscribe(res => {
			if(res)
				this.ws.deleteproductCategory(category.idcategory).subscribe(result => {
					if(result.success && result.data)
						this.filteredCategories.map((c,i) => {
							if(c.idcategory === category.idcategory)
								this.filteredCategories.splice(i,1);
						})
						this.categories.map((c,i) => {
							if(c.idcategory === category.idcategory)
								this.categories.splice(i,1);
						})
				})
		});
	}
	public createProduct(){
		const title = "Crear producto";
		let product:Product = new Product();
		product.idcategory = this.catSelected.idcategory;
		this.dialog.productform(title,product).afterClosed().subscribe(result => {
			if(result){
				if((this.products[0] && this.products[0].idproduct !== result.idproduct) || !this.products[0])
					this.products.unshift(result);
				this.categories.map((cat, i) => {
					if(cat.idcategory === this.catSelected.idcategory){
						if((this.categories[i].products[0] && this.categories[i].products[0].idproduct !== result.idproduct) || !this.categories[i].products[0])
							this.categories[i].products.unshift(result);
					}
				})
				if((this.filteredProducts[0] && this.filteredProducts[0].idproduct !== result.idproduct) || !this.filteredProducts[0])
					this.filteredProducts.unshift(result);
			}
		})
	}
	public editProduct(product){
		const title = "Editar producto";
		this.dialog.productform(title,product).afterClosed().subscribe(result => {
			if(result){
				this.products.map((p,i) => {
					if(p.idproduct === product.idproduct)
						this.products[i] = result;
				})
				this.categories.map((cat, i) => {
					if(cat.idcategory === this.catSelected.idcategory){
						cat.products.map((p,j) => {
							if(p.idproduct === product.idproduct)
								this.categories[i].products[j] = result;
						})
					}
				})
				this.filteredProducts.map((p,i) => {
					if(p.idproduct === product.idproduct)
						this.filteredProducts[i] = result;
				})
			}
		});
	}
	public deleteProduct(product){
		const title="Eliminar producto";
		const message= `¿Realmente desea borrar el producto ${product.name} y desvincularlo de todos los contenidos a los que esté vinculado?`;
		this.dialog.confirm(title, message,null).subscribe(res => {
			if(res)
				this.ws.deleteproduct(product.idproduct).subscribe(result => {
					if(result.success && result.data)
						this.filteredProducts.map((p,i) => {
							if(p.idproduct === product.idproduct)
								this.filteredProducts.splice(i,1);
						})
						this.products.map((p,i) => {
							if(p.idproduct === product.idproduct)
								this.products.splice(i,1);
						})
				})
		});
	}
	public detachProduct(content){
		const title="Eliminar producto";
		const message= `¿Realmente desea eliminar el producto ${this.prodSelected.name} y desvincularlo del contenido ${content.name}?`;
		this.dialog.confirm(title, message,null).subscribe(res => {
			if(res)
				this.ws.detachproductFromContent(this.prodSelected.idproduct, content.idcontent).subscribe(result => {
					if(result){
						this.filteredContents.map((c,i) => {
							if(c.idcontent === content.idcontent)
								this.filteredContents.splice(i,1);
						})
						this.contents.map((c,i) => {
							if(c.idcontent === content.idcontent)
								this.contents.splice(i,1);
						})
					}
				})
		})
	}
	public filterContent(q: string) {
		if(!q) {
		  this.filteredContents = this.contents;
		} else {   
			this.filteredContents =  this.contents.filter(content => content.name.toLowerCase().includes(q.trim().toLowerCase()) || content.idcontent.toString().includes(q))  
		}
	}

}
