import { Component, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { Product } from 'src/modules/product/models/product';
import { DialogService } from 'src/modules/dialog/services/dialog.service';

@Component({
  selector: 'product-select',
  templateUrl: './productselect.component.html',
  styleUrls: ['./productselect.component.scss']
})
export class ProductSelectComponent {
  
  @Output() modalSave = new EventEmitter<Product[]>();
  @Output() removeProduct = new EventEmitter<number>();
  @Input() selectedProducts: Product[] = [];
  @Input() statusCtProd: number;
  
  public prods:any = [
      {
        idcategory:55,
        name:"Producto",
        color:"#64ca26",
        image_url:"https://ladorianids.com/images/v4/tagproducto.png",
        estado:false
      }
    ]

  constructor(private dialog: DialogService) { }

  ngOnInit(): void {
  }
  
  ngOnChanges(changes: SimpleChanges) {
    if(changes && changes.statusCtProd && changes.statusCtProd.currentValue > 0){
      this.prods.forEach(val=>{
        if(changes.statusCtProd.currentValue > 0) val.estado = false;
      },this)
    }
    
    if(changes && changes.selectedProducts && changes.selectedProducts.currentValue){
    	this.productsInCategory();
    }
  }
  
  public productsInCategory(){
    if(this.selectedProducts){
      let productCat = this.prods;
     
      this.selectedProducts.forEach(function(prod) {
        productCat.forEach((cat)=>{
          cat.estado = true;            
          prod['color']=cat.color;
        })
  		}, this)
    }
  }
  
  public openDialog() {

  	this.dialog.productselect('DIALOGS.product_select.title', this.selectedProducts).afterClosed().subscribe((result: Product[]) => {
  	  this.modalSave.emit(result)
  	})
  }
  
  changeVal(idx:number) {
    if(this.prods[idx].estado){
      this.prods[idx].estado = false
    }else{
      this.prods[idx].estado = true 
      
    }
  }
}
