import { Tag } from 'src/modules/tag/models/tag';
import { ProductCategory } from './product-category';
import { Content } from 'src/modules/content/models/content';
export class Product {
    idproduct: number;    
    code: string;
    name: string; 
    idcategory:number;
    category:ProductCategory;
    tags:Tag[];
    contents?:Content[];
    
    constructor(obj?: any) {
        if(!obj)
            return;
        this.idproduct = obj.idproduct;
        this.code = obj.code;
        this.idcategory = obj.idcategory;
        this.category = obj.category;
        this.name = obj.name;
        this.tags = obj.tags;
        this.contents = obj.contents;
    }
}