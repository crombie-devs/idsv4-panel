
import { Product } from './product';

export class ProductCategory {
    code: string;
    name: string;
    idcategory:number;
    products?:Product[];
    
    constructor(obj?: any) {
        if(!obj)
            return;
        this.code = obj.code;
        this.idcategory = obj.idcategory;
        this.name = obj.name;
        this.products = obj.products;
    }
}