import { Component, ViewChild, ElementRef, EventEmitter, Output, Input } from '@angular/core';
import { People } from '../../models/IPeople';
import { PeopleService } from '../../services/people.service';



@Component({
    selector: 'create-csv-people',
    templateUrl: './create-csv-people.component.html',
    styleUrls: ['./create-csv-people.component.scss']
})

export class CreateCsvPeople {

    peoples: People[] = [];
    @Input() users: [] = [];
    @ViewChild('elem') elem: ElementRef;
    constructor(private _people: PeopleService) {
        this.getPeoples();
    }

    generateCSV() {
        let filtered = this.peoples;
        if (this.users.length > 0) {
            filtered = this.peoples.filter(p => this.users.some(u => u == p.id));
        }
        let csvContent = "data:text/csv;charset=utf-8,"
            + filtered.map(people =>
                people.email + ','
                + people.phone + '\n');
        var encodedUri = encodeURI(csvContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "my_data.csv");
        document.body.appendChild(link);
        link.click();
        console.log(csvContent)
    }


    public uploadCSV(): void {
        this.elem.nativeElement.click()
    }

    getPeoples(): void {
        this._people.toSelect().subscribe(peoples => {
            this.peoples = peoples;
        });
    }





}
