import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PeopleService } from '../../services/people.service';
import { IPeople, People } from '../../models/IPeople';
import { SiteSelectComponent } from 'src/modules/site/components/select/select.component';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { TAGTYPE } from 'src/modules/tag/models/tagcategory';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomerSelectComponent } from 'src/modules/customer/components/select/select.component';
import { UserStore } from 'src/modules/user/stores/user.store';
@Component({
    selector: 'people-form',
    templateUrl: './form.page.html',
})

export class PeopleFormPage {
    public form: FormGroup;
    @ViewChild(SiteSelectComponent) site: SiteSelectComponent;
    @ViewChild(CustomerSelectComponent) customer: CustomerSelectComponent;
    tags: any[] = [];
    id: number;
    idcustomer: number;
    idsite: number;
    loading: boolean;
    people: People;
    me: UserStore;
    show: boolean;

    constructor(private route: ActivatedRoute, private _people: PeopleService, private dialog: DialogService, private _snackBar: MatSnackBar,
        private router: Router) {

        this.me = new UserStore();
        if (this.me.getRoles().some(r => r.name != 'superadmin')) {
            this.show = true;
            this.idcustomer = this.me.getUser().customer.idcustomer;
            if (this.me.getUser().site) {
                this.idsite = this.me.getUser().site.idsite;
            }

        }

        this.form = new FormGroup({
            id: new FormControl(),
            email: new FormControl('', [Validators.email]),
            idsite: new FormControl(),
            phone: new FormControl(''),
            tags: new FormControl([]),
            idcustomer: new FormControl(this.idcustomer),

        });
    }

    public ngOnInit() {
        this.route.params.subscribe(param => {
            if (param.id) {
                this.id = param.id;
                this._people.get(this.id).subscribe(people => {
                    if (people) {
                        this.initForm(people[0]);
                    }
                });
            }
        });
    }



    initForm(data: IPeople) {
        this.form.setValue({
            id: data.id,
            email: data.email,
            phone: data.phone,
            tags: data.tags,
            idsite: data.idsite,
            idcustomer: data.idcustomer
        });

        data.tags.forEach(element => {
<<<<<<< Updated upstream
            this.tags.push({ name: element.name, idtag: element.idtag, categoryName: element.tag_category.name });
=======
            this.tags.push({ name: element.tags[0].name, idtag: element.idtag, categoryName: element.tags[0].tag_category.name });
>>>>>>> Stashed changes
        });

        if (!this.show) {
            this.site.setValue(data.idsite);
            this.customer.setValue(data.idcustomer);
        }
    }



    create(): void {
        if (this.form.valid) {
            this.form.value.idsite = this.idsite ? this.idsite : null;
            this.form.value.idcustomer = this.idcustomer;
            this.form.value.tags = JSON.stringify(this.form.value.tags);
            if (this.id) {
                this.form.value.tags = this.tags;
            }

            this._people.save(this.form.value).subscribe(item => {
                if (item) {
                    let url = '../list'
                    if (this.id) {
                        url = '../' + url;
                    }
                    this.router.navigate([url], { relativeTo: this.route })
                }
            });
        }
    }

    formatTag(tags): any {
        let tag: any = {};
        tag.idtag = tags.idtag;
        tags.tags.forEach(element => {
            tag.name = element.name;
            tag.categoryName = element.tag_category.name;
        });
        return tag;
    }

    getSites(event): void {
        this.idcustomer = event;
        this.site.filterSite(event);
    }

    setSites(event): void {
        this.idsite = event.idsite;
    }

    public openDialogTags() {
        if (this.idcustomer) {
            this.dialog.tagcategoryselect('Tags', this.form.value.tags, this.idcustomer, TAGTYPE.mensaje)
                .afterClosed().subscribe((result) => {
                    if (!result.cancel) {
                        this.tags = result.tags;
                        this.form.value.tags = [...this.tags];

                    }
                });
        } else {
            this.openSnackBar("Debes seleccionar un customer")
        }
    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
            panelClass: ['orange-snackbar']
        });
    }

}
