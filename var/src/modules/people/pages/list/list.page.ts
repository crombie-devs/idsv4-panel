import { Component, Input, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatInput } from '@angular/material/input';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { IPeople, People } from '../../models/IPeople';
import { PeopleService } from '../../services/people.service';
import { SelectionModel } from '@angular/cdk/collections';


@Component({
    selector: 'people-list',
    templateUrl: './list.page.html',
    styleUrls: ['./list.page.scss']
})

export class PeopleListPage {

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChildren('table') table: QueryList<MatCheckbox>
    @ViewChild(MatInput) input: MatInput;

    displayedColumns: string[] = ['select', 'id', 'email', 'phone'];
    new: boolean = false;
    edit: boolean = false;
    deleted: boolean = false;
    permission: boolean = false;
    load: boolean = false;
    dataSource = new MatTableDataSource<People>([]);;
    dataSourceTemp: any[] = [];
    selection = new SelectionModel<People>(true, []);;


    data:  IPeople  [];

    constructor(private router: Router,
        private route: ActivatedRoute, private dialog: DialogService,
        private translate: TranslateService, private _snackBar: MatSnackBar, private _people: PeopleService) {

        this.route.params.subscribe(param => {
            if (param.id) {
               
            }
        });
    }

    public ngOnInit() {
        this._people.toSelect().subscribe(peoples => {
          if(peoples) {
              this.dataSourceTemp = [...peoples].reverse();
              this.renderDataTable(peoples.reverse());
          }
        });
     }

    public applyFilter(event: string) {
        this.selection.clear();
        if (!event)
            this.dataSource.data = this.dataSourceTemp;
        else {
            if (typeof event == 'string') {
                this.dataSource.data = this.dataSourceTemp.filter((ticket: any) =>
                    ticket.titulo.toLowerCase().includes(event.trim().toLowerCase()) ||
                    ticket.id.toString().includes(event.trim().toLowerCase()) ||
                    (ticket.priority && ticket.priority.name.toLowerCase().includes(event.trim().toLowerCase())) ||
                    (ticket.category && ticket.category.name.toLowerCase().includes(event.trim().toLowerCase())) ||
                    (ticket.status && ticket.status.name.toLowerCase().includes(event.trim().toLowerCase())) ||
                    (ticket.site && ticket.site.name.toLowerCase().includes(event.trim().toLowerCase())) ||
                    (ticket.responsible && ticket.responsible.name.toLowerCase().includes(event.trim().toLowerCase()))
                );

            }
        }
    }

    public update() {
     
        if (this.selection.selected.length > 1) {
            this.openSnackBar("para editar solo puedes seleccioar un ticket");
            return;
        }
        this.router.navigate(['/private/peoples/add', this.selection.selected[0].id])
    }


    checkTable(row, value, index) {

        if (row.hasOwnProperty([value])) {
            if (row[value] != null) {
                return true;
            }
        }
        return false;

    }

    public delete() {
        let peoplesByDelete = [];
        this.table.toArray().forEach(item => {
            if (item.checked)
                peoplesByDelete.push(item.value);
        });

        if (peoplesByDelete.length == 0)
            this.dialog.warning(this.translate.instant('Aviso'), this.translate.instant('Debe seleccionar al menos un contenido')).subscribe();
        else
            this.dialog.confirm(this.translate.instant('Confirmar Archivado'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
                if (confirm) {
                    this._people.deleteArray(peoplesByDelete).subscribe(result => {
                        if (result) {
                            this.dataSourceTemp = this.dataSourceTemp.filter(ds => !peoplesByDelete.includes(ds.id));
                            this.dataSource.data = this.dataSource.data.filter(ds => !peoplesByDelete.includes(ds.id));
                            this.renderDataTable(this.dataSource.data);
                            this.dialog.warning('Aviso', 'Los peoples han sido eliminados').subscribe();
                        }
                    })
                }
            });
    }

    public renderDataTable(data:  IPeople  []) {
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.load = false;
    }

    isAllSelected() {
        if (this.dataSource.paginator) {
            const numSelected = this.selection.selected.length;
            const page = this.dataSource.paginator.pageSize;
            let endIndex: number;
            if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
                endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
            } else {
                endIndex = this.dataSource.data.length - (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize);
            }
            return numSelected === endIndex;
        }
    }

    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() : this.selectRows();
    }

    selectRows() {
        let endIndex: number;
        if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
            endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
        } else {
            endIndex = this.dataSource.data.length;
        }
        for (let index = (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize); index < endIndex; index++) {
            this.selection.select(this.dataSource.data[index]);
        }
    }


    public checkboxLabel(row?:  IPeople  ): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
            panelClass: ['orange-snackbar']
        });
    }


}
