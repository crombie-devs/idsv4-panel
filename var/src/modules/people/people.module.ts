import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatIconModule } from "@angular/material/icon";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTooltipModule } from "@angular/material/tooltip";
import { ColorPickerModule } from 'ngx-color-picker';
import { UserModule } from "../user/user.module";
import { SharedModule } from "../../shared/shared.module";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { DirectivesModule } from "src/directive/directives.module";
import { AssetModule } from "../asset/asset.module";
import { SiteModule } from "../site/site.module";
import { MatButtonModule } from "@angular/material/button";
import { PipesModule } from "src/pipes/pipes.module";
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { PeopleRoutingModule } from "./people-routing.module";
import { PeopleFormPage } from "./pages/form/form.page";
import { PeopleListPage } from "./pages/list/list.page";
import { CustomerModule } from "../customer/customer.module";
import { TagModule } from "../tag/tag.module";
import { CreateCsvPeople } from "./components/create-csv-people/create-csv-people.component";


@NgModule({
    declarations: [
        PeopleListPage,
        PeopleFormPage,
        CreateCsvPeople
    ],

    imports: [
        CommonModule,
        MatDialogModule,
        MatCheckboxModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatIconModule,
        MatToolbarModule,
        MatTooltipModule,
        SharedModule,
        ColorPickerModule,
        UserModule,
        NgxMatFileInputModule,
        MatDatepickerModule,
        MatFormFieldModule,
        DirectivesModule,
        AssetModule,
        SiteModule,
        MatButtonModule,
        PipesModule,
        MatProgressSpinnerModule,
        PeopleRoutingModule,
        CustomerModule,
        SiteModule,
        TagModule
    ],
    exports: [
        CreateCsvPeople
    ],
    providers: [],
    entryComponents: []

})
export class PeopleModule { }
