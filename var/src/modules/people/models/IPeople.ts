import { ReturnStatement } from "@angular/compiler";
import { Tag } from "src/modules/tag/models/tag";

export  interface IPeople {
id: number;
email: string;
phone: string;
tags: any[];
idsite: number;
idcustomer: number;
}

export class People implements IPeople {
    id: number;
    email: string;
    phone: string;
    tags: any[];
    idsite: number;
    idcustomer: number;

    constructor(data?: IPeople) {
        if(!data)
        return;

        this.id = data.id ? data.id : 0;
        this.email = data.email ? data.email : '';
        this.phone = data.phone ? data.phone : '';
        this.tags =  data.tags.length > 0 ? data.tags.map(t => new Tag(t)) : [];
        this.idsite = data.idsite ? this.idsite : 0;
        this.idcustomer = data.idcustomer ? data.idcustomer : 0;

    }
}