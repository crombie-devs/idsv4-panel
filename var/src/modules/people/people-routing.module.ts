import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PeopleFormPage } from './pages/form/form.page';
import { PeopleListPage } from './pages/list/list.page';





const routes: Routes = [
  { path: 'peoples/list', component: PeopleListPage, data: { breadcrumb: 'Listado de peoples' }},
  { path: 'peoples/add', component: PeopleFormPage, data: { breadcrumb: 'Crear people' }},
  { path: 'peoples/add/:id', component: PeopleFormPage, data: { breadcrumb: 'Editar people' }},

  




]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PeopleRoutingModule { }
