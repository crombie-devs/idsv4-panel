import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { HttpService}  from '../../../services/http.service';
import { ICrudWS } from '../../../interfaces/ICrudWS.interface';
import { IPeople } from '../models/IPeople';


@Injectable({
  providedIn: 'root',
})
export class PeopleService implements ICrudWS<IPeople> {

  idsite: Subject<number> = new Subject();
  $idsite = this.idsite.asObservable();

  constructor(private http: HttpService) {}

  public save(obj: any): Observable<IPeople> {
    
    if(obj.hasOwnProperty('id') && obj.id) {
       
      return this.update(obj);
    } else {
   
      return this.create(obj);
    }
  } 

  public delete(id): Observable<IPeople>{
    return this.http.delete('private/peoples', [id], 'WS.people');
  }
 

  public create(obj: IPeople): Observable<IPeople> {
    return this.http.createWithFiles('private/peoples', obj, 'WS.people_add');
  }

  public update(obj: IPeople): Observable<IPeople> {

    return this.http.update('private/peoples', obj, 'WS.people_update');
  }

  public deleteArray(ids: number[]): Observable<IPeople> {
    return this.http.update('private/peoples/delete', {ids:ids}, 'WS.people_delete');
  }

  public get(id: number): Observable<IPeople> {
    return this.http.get('private/people', [id], 'WS.people');
  }

  public toSelect(args?: any[]): Observable<IPeople[]> {
    return  this.http.get('private/peoples/toselect', args, 'WS.people');
  }

  public list(args?: any[]): Observable<IPeople[]> {
    return  this.http.get('private/peoples', args, 'WS.people');
  }

  public listBytype(args?: any[]): Observable<IPeople[]> {
    return  this.http.get('private/peoples/type', args, 'WS.people');
  }


}
  