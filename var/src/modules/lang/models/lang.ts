export interface ILang {
  idlang: number;
  name: string;
  image_url?: string
}

export class Lang implements ILang {

  idlang: number;
  name: string;
  image_url?: string

  constructor(obj?: ILang) {
    if(!obj)
      return;

    this.idlang = obj.idlang;
    this.name = obj.name;   
    this.image_url = obj.image_url;

  }

  isEquals(lang: ILang) {
    return (this.idlang === lang.idlang);
  }

}