import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { ILang, Lang } from 'src/modules/lang/models/lang';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'langs.store.ids';

@Injectable()
export class LangStore {

    public onChange: BehaviorSubject<Lang[]> = new BehaviorSubject([]);
    private data: Lang[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const langs: ILang[] = JSON.parse(store); 
                this.set(langs);
            } else 
                this.load();
        }else   
            this.load();
    }

    public get(): Lang[] {
        return this.data;
    }

    public set(langs: ILang[]) {
        this.save(langs);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.langsToSelect().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }
        });
    }

    private save(langs: ILang[]) {
        this.data = langs.map(s => new Lang(s));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(langs));        
    }
}