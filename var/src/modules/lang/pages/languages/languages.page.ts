import { WSService } from 'src/services/ws.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { FormField } from '../../../form-field/models/form-field';

@Component({
  selector: 'app-languages',
  templateUrl: './view.page.html',
  styleUrls: ['./styles.page.scss']
})
export class LanguagesPage implements OnInit {
  public languages:string[] = ['es','en','fr','it'];
  public plang:string = 'es';
  public translations:any[] = [];
  public tran:any[] = [];
  public keys:any[] = [];
  public result: any;
  public key:string;
  public title:string;
  public tree:any[] = [];
  public navigation: any[] = [];
  public newKey: boolean = true;

  public fieldDataKey: FormField = {
		label: this.translate.instant("Clave de traduccion"),
		name: 'key',
		placeholder: '',
		required: true
  };
  public fieldDataEs: FormField = {
		label: this.translate.instant("Español"),
		name: 'es',
		placeholder: '',
		required: true
  };
  public fieldDataEn: FormField = {
		label: this.translate.instant("Inglés"),
		name: 'en',
		placeholder: '',
		required: false
  };
  public fieldDataFr: FormField = {
		label: this.translate.instant("Francés"),
		name: 'fr',
		placeholder: '',
		required: false
  };
  public fieldDataIt: FormField = {
		label: this.translate.instant("Italiano"),
		name: 'it',
		placeholder: '',
		required: false
	};
  public formGroup:FormGroup = new FormGroup({
    key: new FormControl(null, Validators.required),
    es: new FormControl(null, Validators.required),
		en: new FormControl(null),
		fr: new FormControl(null),
		it: new FormControl(null),
	});
  constructor(
    private _snackBar: MatSnackBar,
    private translate: TranslateService,
    private ws: WSService,
    private dialog: DialogService
  ) {
  }

  ngOnInit(): void {
    this.getPLang()
    this.getTranslations();
  }

  private getPLang(){
    this.plang = sessionStorage.getItem('lang');
  }

  private getTranslations(){
    this.translate.reloadLang('es').subscribe(result => {
      this.keys = Object.keys(result);
      this.keys.sort(this.compare);
      this.translations[0] = result;
      this.translate.reloadLang('en').subscribe(result => {
        this.translations[1] = result;
        this.translate.reloadLang('fr').subscribe(result => {
          this.translations[2] = result;
          this.translate.reloadLang('it').subscribe(result => {
            this.translations[3] = result;
            this.translate.use(this.plang);
          })
        })
      })
    })
  }
  private compare(a,b){
    if(a.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "") < b.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, ""))
      return -1
    else
      return 1
  }

  showTranslations(key){
    this.newKey = false;
    this.tran = [];
    if(this.tree.length > 0)
      this.tree[this.tree.length - 1].map(translation => {
        this.tran.push(translation[key]);
      });
    else
      this.translations.map(translation => {
        this.tran.push(translation[key]);
      })

    if(typeof this.tran[0] === 'object'){
      this.navigation.push(this.keys);
      this.tree.push(this.tran);
      if(!this.title)
        this.title = key;
      else
        this.title += `/${key}`;
      this.keys.map((k,i) => {
        if(k === key){
          this.keys[i]
        }
      })
      this.keys = Object.keys(this.tran[0]);
      this.keys.sort(this.compare);
    }else{
      this.key = key;
      this.formGroup.patchValue({es: this.tran[0], en:this.tran[1], fr: this.tran[2], it: this.tran[3]})
    }
  }
  public back(){
    this.keys = this.navigation[this.navigation.length - 1];
    this.navigation.splice(this.navigation.length - 1, 1)
    let title = this.title.split('/');
    if(title.length > 1){
      title.map((t,i) => {
        if(i === 0)
          this.title = t;
        else if(i < title.length - 1)
          this.title += `/${t}`;
      })
    }else{
      this.title = null;
    }
    this.tree.splice(this.tree.length - 1, 1);
  }

  public saveTranslation(){
    let key = this.formGroup.get('key').value;
    if(!key)
      key = this.key;
    if(this.title){
      key = `${this.title.split('/').join('.')}.${this.key}`;
      key = key.split('.');
    }
    
    this.languages.map((lang,index) =>{
      let translation = this.formGroup.get(lang).value;
      if(this.title){
        let text = `this.translations[${index}]`;
        key.map(k => {
          text += `['${k}']`;
        })
        text += `= '${translation}'`;
        eval(text);
      }else
        this.translations[index][key] = translation;
      this.ws.saveTranslation(lang, this.translations[index]).subscribe(result => {
        if(result.success)
          this.openSnackBar(this.translate.instant("La traducción se ha guardado correctamente"))
        else
          this.openSnackBar(this.translate.instant("La traducción No se ha podido guardar correctamente"))
        this.getTranslations();
        this.newKey = false;
      });
    })
  }
  public newTranslation(){
    this.newKey = true;
    this.key = null;
    this.formGroup.patchValue({key:null,es:null, en: null, fr: null, it: null})
  }
  public openSnackBar(message: string) {
    this._snackBar.open(message,'', {
      duration: 1500,
    });
  }
  public delete(key){
    this.dialog.confirm(this.translate.instant('Borrar traducción'), `${this.translate.instant('¿Seguro que quiere eliminar este elemento de la traducción?')} ${key}`).subscribe(confirm => {
      if(confirm)
        this.deleteKey(key);
    });
  }
  private deleteKey(key){
    if(this.title){
      key = `${this.title.replace('.','/')}.${key}`;
      key = key.split('.');
    }
    
    this.languages.map((lang,index) =>{
      if(this.title){
        let text = `delete this.translations[${index}]`
        key.map(k => {
          text += `['${k}']`;
        })
        eval(text);
      }
      else
        delete this.translations[index][key];
      this.ws.saveTranslation(lang, this.translations[index]).subscribe(result => {
        if(result.success)
          this.openSnackBar(this.translate.instant("La traducción se ha eliminado correctamente"))
        else
          this.openSnackBar(this.translate.instant("La traducción No se ha podido eliminar correctamente"))
        this.getTranslations();
      });
    })
  }

}
