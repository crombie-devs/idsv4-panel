import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LanguagesPage } from './pages/languages/languages.page';


const routes: Routes = [
  { 
    path: '', 
    component: LanguagesPage, 
    data: { breadcrumb: 'Variables' }
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class LangRoutingModule { }
