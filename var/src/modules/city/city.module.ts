import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CITY_COMPONENTS } from './components/components';
import { CityStore } from './stores/city.store';

import { MaterialModule } from '../../app/material.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [CITY_COMPONENTS],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule
  ],
  providers: [CityStore],
  exports: [CITY_COMPONENTS]
})
export class CityModule {}