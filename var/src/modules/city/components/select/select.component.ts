import { Component, OnInit, Input,Output, EventEmitter, SimpleChanges } from '@angular/core';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Router } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { IResult } from 'src/models/iresult';
import { DebugStore } from 'src/stores/debug.store';
import { FormControl, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'city-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectCityComponent implements OnInit {

  
@Output() citiesSelected = new EventEmitter<number>();
@Input() localityAvailable:boolean;
@Input() idCountry:number;
@Input() idProvince:number;

public cities:any[] = [];
public filterForm:FormGroup;
public ciudades = new FormControl();

  constructor(
    private dialog: DialogService,
		private router: Router,
		private ws: WSService,
		private debug: DebugStore,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private formBuilder:FormBuilder,
    ) {
    }

  ngOnInit(): void {
  }
  
  ngOnChanges(changes: SimpleChanges) {
    //this.getProvinces();
    if(changes.localityAvailable){
	    if(changes.localityAvailable.currentValue){
	    	this.getCities()
	    }
    }
  }
  
  private getCities(){
    this.ws.citiesToSelectById(this.idCountry,this.idProvince).subscribe((result: IResult) => {
      if(result.success) {
				this.debug.addInfo('WS.cities', 'successful');				
				this.cities = result.data;
			}
			else {            
				this.debug.addError('WS.cities', result.data);
				this.openSnackBar(result.data);
			}
		}, error => {
			this.debug.addError('WS.cities', error.message);
			//this.openSnackBar(error.message);
		}); 
		
		
  }
  
  public captureValue() {
		let ciudades = this.ciudades.value;
		this.citiesSelected.emit(ciudades);
		
  }
	
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
	
	
	
	

}
