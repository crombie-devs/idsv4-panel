export interface ICity {
    idcity: number;
    idprovince:number;
    name: string;
}
  
export class City implements ICity {
  
    idcity: number;
    idprovince: number;
    name: string;
  
    constructor(obj?: ICity) {
      if(!obj)
        return;
  
      this.idcity = obj.idcity;
      this.idprovince = obj.idprovince;
      this.name = obj.name;   
  
    }
  
}