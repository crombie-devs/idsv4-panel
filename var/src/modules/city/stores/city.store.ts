import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { ICity, City } from 'src/modules/city/models/city';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'cities.store.ids';

@Injectable()
export class CityStore {

    public onChange: BehaviorSubject<City[]> = new BehaviorSubject([]);
    private data: City[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = localStorage.getItem(STORE_KEY);
            if(store) {
                const cities: ICity[] = JSON.parse(store); 
                this.set(cities);
            } else 
                this.load();            
        }else
            this.load();
    }

    public get(): City[] {
        return this.data;
    }

    public set(cities: ICity[]) {
        this.save(cities);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.citiesToSelect().subscribe((result: IResult) => {
            if(result.success)
              this.set(result.data);
        });
    }

    private save(cities: ICity[]) {
        this.data = cities.map(s => new City(s));
        if(environment.localstorage)
            localStorage.setItem(STORE_KEY, JSON.stringify(cities));
    }

    private filterByProvince (idprovince: number){        
        return this.data.filter(p=> p.idprovince === idprovince);
    }

}