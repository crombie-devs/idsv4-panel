import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { WSService } from 'src/services/ws.service';
import { IRecomendation, Recomendation } from 'src/modules/recomendation/models/recomendation';

const STORE_KEY: string = 'recomendation.store.ids';

@Injectable()
export class RecomendationStore {

    public onChange: BehaviorSubject<Recomendation> = new BehaviorSubject(null);
    private data: Recomendation;

    constructor() {
        let store = sessionStorage.getItem(STORE_KEY);
        if(store) {
            const recomendation: IRecomendation = JSON.parse(store); 
            this.set(recomendation);
        }
    }

    public get(): Recomendation {
        return this.data;
    }

    public set(recomendation: IRecomendation) {
        this.save(recomendation);
        this.onChange.next(this.data);
    }

    private save(recomendation: IRecomendation) {
        this.data = new Recomendation(recomendation);
        sessionStorage.setItem(STORE_KEY, JSON.stringify(recomendation));
    }

    public delete(){
        sessionStorage.removeItem(STORE_KEY);
    }
}