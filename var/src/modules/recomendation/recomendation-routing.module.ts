import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssociationsPage } from './pages/asociaciones/asociaciones.page';
import { RecomendationsPage } from './pages/recomendaciones/recomendaciones.page';
import { RecomendationDataPage } from './pages/recomendationdata/recomendation-data.page';

const routes: Routes = [
  { path: 'asociaciones', component: AssociationsPage, data: { breadcrumb: 'Asociaciones' }},
  { path: 'recomendaciones', component: RecomendationsPage, data: { breadcrumb: 'Recomendaciones' }},
  { path: 'data', component: RecomendationDataPage, data: { breadcrumb: 'datos para recomendaciones'}},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class RecomendationRoutingModule { }
