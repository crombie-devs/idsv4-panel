import { Component, Input, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { FormField } from 'src/modules/form-field/models/form-field';


@Component({
    selector: 'recomendaciones-asociaciones',
    templateUrl: './view.page.html',
    styleUrls: ['./styles.page.scss']
  })

export class AssociationsPage {
    public me: any;
	public store: string = null;
    public loadingCategories: boolean = true;
    public loadingAssociated: boolean = false;
    public data: any[];
    public fieldDataCategory: FormField = {
		label: "Categoría",
		name: 'category',
		placeholder: 'Selecciona una categoría',
		options: [],
		multiple:false,
		required: true
    };
    public formGroup:FormGroup = new FormGroup({
		category: new FormControl(null, Validators.required),
    });

    public associations: any[] = [];
    
    constructor(
		private ws: WSService,
		private debug: DebugStore,	
		private dialog: DialogService,
    	private astore: AuthStore,
		private localStorage: MYLocalStorage,
		private ustore: UserStore) {}
    
    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
		this.getCategories();
    }

    public getCategories(){
        this.ws.getAssociationsCategories().subscribe(result => {
            if(result.success){
                if(result.data.length === 0){
                    this.dialog.warning('Categorías', "No se han encontrado categorías");
                    this.loadingCategories = false;
                    return;
                }
                let categories = result.data;
                categories.sort((a,b) => a.name > b.name ? 1 : -1);
                categories.map(category => {
                    this.fieldDataCategory.options.push({ name: category.name, value: category.idcategory });
                    this.loadingCategories = false;
                });
            }else{
                this.dialog.warning('Categorías', "No se han encontrado categorías");
                this.loadingCategories = false;
                return;
            }
        },
        error => {
            this.dialog.warning('Categorías', "Ha habido un error");
            this.loadingCategories = false;
            return;
        });
    }

    public changeCategory(data){
        this.getProbabilidadCondicionada(this.formGroup.get('category').value);
        this.loadingAssociated = true;
    }

    public getProbabilidadCondicionada(category){
        this.ws.getAssociationsConditionalProbability(category).subscribe(result => {
            if(result.success){
                if(result.data.length === 0){
                    this.dialog.warning('Datos', "No se han encontrado Datos");
                    this.loadingCategories = false;
                    return;
                }
                this.associations = [];
                result.data.map(res => {
                    this.associations.push(res);
                })
            }else{
                this.dialog.warning('Datos', "No se han encontrado Datos");
                this.loadingCategories = false;
                return;
            }
            this.loadingAssociated = false;
        },
        error => {
            this.dialog.warning('Datos', "Ha habido un error");
            this.loadingCategories = false;
            return;
        });
    }
  }