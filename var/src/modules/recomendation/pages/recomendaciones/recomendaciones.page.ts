import { FormField } from 'src/modules/form-field/models/form-field';
import { Component, Input, ViewChild } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';

import { FormControl, Validators, FormGroup } from '@angular/forms';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { Product } from 'src/modules/product/models/product';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { RecomendationStore } from 'src/modules/recomendation/stores/recomendations.store';
import { Router } from '@angular/router';

import { ChartDataSets } from 'chart.js';
import { Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

import {TranslateService} from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { find } from 'rxjs/operators';

@Component({
    providers: [RecomendationStore],
    selector: 'recomendaciones-recomendaciones',
    templateUrl: './view.page.html',
    styleUrls: ['./styles.page.scss']
  })

export class RecomendationsPage {
    public me: any;
	public store: string = null;
    //Loadings
    public loadingMonths: boolean = false;
    public loadingProvinces: boolean = false;
    public loadingCategories: boolean = false;
    public loadingTrendCategories:boolean = false;
    public loadingWeightCategories:boolean = false;
    public loadingAssociated: boolean = true;
    public loadingProducts: boolean = true;
    public isLoadingPatronVentas: boolean = true;
    public isLoadingSalesEvolution: boolean = true;
    public isLoadingDataTable: boolean = true;
    public showThirdBlock: boolean = false;
    public selectedCategory:string = '';
    public selectedAssociatedCategory: string = '';
    public combinemejores: any = {};
    public showProducts: boolean = false;
    public panelOpen: boolean = true;
    public recomendationSelect: boolean = true;
    public beginAtZero:boolean = false;
    public trendsData: any[] = [];
    public selectedTrendCategories:any[] = [];
    public provinces:any[] = [];

    public data: any[];
    public fieldDataMonth: FormField = {
		label: 'RECOMENDACIONES.recomendaciones.field_month.label',
		name: 'month',
		placeholder: 'RECOMENDACIONES.recomendaciones.field_month.placeholder',
		options: [
            {name:'MONTHS.jan', value: 1},
            {name:'MONTHS.feb', value: 2},
            {name:'MONTHS.mar', value: 3},
            {name:'MONTHS.apr', value: 4},
            {name:'MONTHS.may', value: 5},
            {name:'MONTHS.jun', value: 6},
            {name:'MONTHS.jul', value: 7},
            {name:'MONTHS.aug', value: 8},
            {name:'MONTHS.sep', value: 9},
            {name:'MONTHS.oct', value: 10},
            {name:'MONTHS.nov', value: 11},
            {name:'MONTHS.dec', value: 12},
        ],
		multiple:false,
		required: true
    };
    public fieldDataCategories: FormField = {
		label: 'RECOMENDACIONES.recomendaciones.field_categories.label',
		name: 'categories',
		placeholder: '',
		options: [],
		multiple: true,
		required: true
    };
    public fieldDataCategoriesTrends: FormField = {
		label: 'Ranking de categorías ordenadas según algoritmo basado en tendencias',
		name: 'categoriesTrends',
		placeholder: '',
		options: [],
		multiple: true,
		required: false
    };
    public fieldDataCategoriesWeights: FormField = {
		label: 'Categorías ordenadas según volumen de ventas',
		name: 'categoriesWeights',
		placeholder: '',
		options: [],
		multiple: true,
		required: false
    };
    public fieldDataProvince: FormField = {
		label: 'Área geográfica',
		name: 'province',
		placeholder: '',
		options: [],
		multiple: false,
		required: true
    };
    public fieldDataProductAssociated: FormField = {
		label: 'RECOMENDACIONES.recomendaciones.field_product_assoc.label',
		name: 'associated',
		placeholder: '',
		options: [],
		multiple: false,
		required: false
    };
    public fieldDataProducts: FormField = {
		label: 'RECOMENDACIONES.recomendaciones.field_products.label',
		name: 'products',
		placeholder: '',
		options: [],
		multiple: false,
		required: false
    };
    public fieldDataProductsPrincipal: FormField = {
		label: 'RECOMENDACIONES.recomendaciones.field_products.label',
		name: 'productsPrin',
		placeholder: '',
		options: [],
		multiple: false,
		required: false
    };
    public fieldDataProductsAsociados: FormField = {
		label: 'RECOMENDACIONES.recomendaciones.field_products.label',
		name: 'productsAssoc',
		placeholder: '',
		options: [],
		multiple: false,
		required: false
    };
    public formGroup:FormGroup = new FormGroup({
        month: new FormControl(null, Validators.required),
        associated: new FormControl(null),
        products: new FormControl(null),
        productsPrin: new FormControl(null),
        productsAssoc: new FormControl(null)
    });

    public promoControl = new FormControl(null);

    public associations: any[] = [];
    public categories: any[] = [];
    public diasseleccionados = []; 
    public horasseleccionados = [];
    public diashorasmax = [];
    public profileGender = [];
    public profileType = [];
    public temperature: string = '';
    public incremento:string = '';
    public productos:Product[] = [];
    public productosAsociados: Product[] = [];

    public weekdays: string[] = [
        'WEEKDAYS.mon',
        'WEEKDAYS.tue',
        'WEEKDAYS.wed', 
        'WEEKDAYS.thu',
        'WEEKDAYS.fri',
        'WEEKDAYS.sat',
        'WEEKDAYS.sun',
        'WEEKDAYS.bank'
    ];
    public gender:string[] = [
        'COMMON.men', 
        'COMMON.women'
    ];
    public type:string[] = [
        'COMMON.type_age.kid',
        'COMMON.type_age.young',
        'COMMON.type_age.adult',
        'COMMON.type_age.senior'
    ];
    
    public patronVentasLineChartLabels: Label[] = [];
    public salesEvolutionLineChartLabels: Label[] = ['-7', '-6', '-5', '-4', '-3', '-2', '-1'];
    public totalChars = 40;
    public patronVentasLineChartData: ChartDataSets[] = [];
    public salesEvolutionChartData: ChartDataSets[] = [];
    public salesEvolution:any[] = [];
    genderPieChartData: number[] = [0,0];

	genderPieChartLabels: Label[] = [];
	typePieChartData: number[] = [0,0,0,0];
    typePieChartLabels: Label[] = [];

    public afluencias: any[] = [];
    public tableData: any[] = [];

    constructor(
		private ws: WSService,
		private debug: DebugStore,	
		private dialog: DialogService,
    	private astore: AuthStore,
        private localStorage: MYLocalStorage,
        private recomendationStore: RecomendationStore,
        private router: Router,
        private ustore: UserStore,
        private translate: TranslateService,
        private cdr: ChangeDetectorRef) {
    }
    ngAfterContentChecked() {
        this.cdr.detectChanges();
    }
    ngOnInit(): void {
        this.me = this.ustore.getUser();
        this.translate.get(this.weekdays).subscribe(translations => {
            this.patronVentasLineChartLabels = Object.values(translations)
        });
        this.translate.get(this.gender).subscribe(translations => {
            this.genderPieChartLabels = Object.values(translations)
        });
        this.translate.get(this.type).subscribe(translations => {
            this.typePieChartLabels = Object.values(translations)
        });
        if(this.me.customer.has_trends){
            this.getTrendsCategories(0);
            this.formGroup.addControl('province', new FormControl(null, Validators.required))
            this.formGroup.addControl('categoriesTrends', new FormControl(null));
            this.formGroup.addControl('categoriesWeights', new FormControl(null));
        }else{
            this.formGroup.addControl('categories', new FormControl(null, Validators.required));
        }
    }

    public getTrendsCategories(province, searchProvinces = true){
        this.fieldDataCategoriesTrends.options = [];
        this.fieldDataCategoriesWeights.options = [];
        this.formGroup.patchValue({categories: null});
        this.loadingTrendCategories = true;
        this.loadingWeightCategories = true;
        this.ws.getRecomendationsTrendsCategories().subscribe(result => {
            if(result.success){
                if(result.data.length > 0){
                    this.trendsData = result.data;
                    let trends:any[] = [];
                    let weights:any[] = [];
                    this.provinces = [];
                    result.data.map(res =>{
                        if(!this.provinces.some(p => p.idlocalarea === res.idlocalarea))
                            this.provinces.push(res);
                        if(res.idlocalarea === province){
                            trends.push(res);
                            weights.push(res);
                        }
                    });
                    weights.sort((a,b) => (a.weight < b.weight) ? 1 : (a.weight > b.weight) ? -1 : 0);
                    trends.sort((a,b) => (a.trend < b.trend) ? -1 : (a.trend > b.trend) ? 1 : 0);
                    trends.map(trend => {
                        const chars = trend.trend.toString().length + trend.category.name.length;
                        const dashesCount = this.totalChars - chars;
                        let dashes = '';
                        for(let i = 0; i < dashesCount; i++){
                            dashes +='-';
                        }
                        this.fieldDataCategoriesTrends.options.push({name: `${trend.category.name} ${dashes} ${trend.trend}`, value: trend.idcategory});
                    })
                    weights.map(weight => {
                        const chars = weight.weight.toString().length + weight.category.name.length;
                        const dashesCount = this.totalChars - chars;
                        let dashes = '';
                        for(let i = 0; i < dashesCount; i++){
                            dashes +='-';
                        }
                        this.fieldDataCategoriesWeights.options.push({name: `${weight.category.name} ${dashes} ${weight.weight}%`, value: weight.idcategory});
                    });
                    this.loadingTrendCategories = false;
                    this.loadingWeightCategories = false;
                    if(searchProvinces)
                        this.getProvinces();
                }else{
                    this.dialog.warning('RECOMENDACIONES.recomendaciones.categories.error.title', 'RECOMENDACIONES.recomendaciones.categories.error.text');
                    this.loadingTrendCategories = false;
                    this.loadingWeightCategories = false;
                    return;                   
                }
            }else{
                this.dialog.warning('RECOMENDACIONES.recomendaciones.categories.error.title', 'RECOMENDACIONES.recomendaciones.categories.error.text');
                this.loadingTrendCategories = false;
                this.loadingWeightCategories = false;
                return;
            }
        },
        error => {
            this.dialog.warning('RECOMENDACIONES.recomendaciones.categories.error.title', 'RECOMENDACIONES.recomendaciones.categories.error.text');
            this.loadingTrendCategories = false;
            this.loadingWeightCategories = false;
            return;
        })
    }

    public getProvinces(){
        this.fieldDataProvince.options = [];
        this.formGroup.patchValue({province: null});
        this.loadingProvinces = true;
        this.ws.getRecomendationsTrendsProvinces().subscribe(result => {
            if(result.success){
                if(result.data.length > 0){
                    result.data.map(res => {
                        if(this.provinces.some(p => p.idlocalarea === res.idlocalarea))
                            this.fieldDataProvince.options.push({name: res.name, value: res.idlocalarea});
                    });
                    this.formGroup.patchValue({province: 0});
                    this.loadingProvinces = false;
                }else{
                    this.fieldDataProvince.options.push({name: 'Nacional', value: 0});
                    //this.dialog.warning('Provincias', 'No se han encontrado provincias.');
                    this.loadingProvinces = false;
                    return;                   
                }
            }else{
                this.dialog.warning('Provincias', 'Ha habido un problema al cargar las provincias');
                this.loadingProvinces = false;
                return;
            }
        },
        error => {
            this.dialog.warning('Provincias', 'Ha habido un problema al cargar las provincias');
            this.loadingProvinces = false;
            return;
        })
    }

    public getCategories(){
        const month = this.formGroup.get('month').value;        
        this.fieldDataCategories.options = [];
        this.formGroup.patchValue({categories: null});

        this.categories = [];
        this.loadingCategories = true;
        this.ws.getRecomendationsCategories(month).subscribe(result => {
            if(result.success){
                if(result.data.length > 0){
                    result.data.map(res => {
                        if(!this.me.customer.has_trends){
                            const percentage = (Math.round(res.value * 10) / 10).toString();
                            const chars = percentage.length + res.name.length;
                            const dashesCount = this.totalChars - chars;
                            let dashes = '';
                            for(let i = 0; i < dashesCount; i++){
                                dashes +='-';
                            }
                            this.fieldDataCategories.options.push({name: `${res.name} ${dashes} ${percentage}%`, value: res.code});
                        }
                        this.categories.push(res);
                    });
                    this.loadingCategories = false;
                }else{
                    this.dialog.warning('RECOMENDACIONES.recomendaciones.categories.error.title', 'RECOMENDACIONES.recomendaciones.categories.error.text');
                    this.loadingCategories = false;
                    return;                   
                }
            }else{
                this.dialog.warning('RECOMENDACIONES.recomendaciones.categories.error.title', 'RECOMENDACIONES.recomendaciones.categories.error.text');
                this.loadingCategories = false;
                return;
            }
        },
        error => {
            this.dialog.warning('RECOMENDACIONES.recomendaciones.categories.error.title', 'RECOMENDACIONES.recomendaciones.categories.error.text');
            this.loadingCategories = false;
            return;
        })
    }

    public changeMonth(data){
        this.resetBooleans();
        this.getCategories();
    }

    public changeProvince(data){
        this.getTrendsCategories(this.formGroup.get('province').value,false);
    }

    public changeCategories(e){
        this.resetBooleans();
    }

    public submitMonthCategories(){
        this.resetBooleans();
        this.getPatronVentas();
        this.tableFilasColores();
    }
    public submitCategoriesTrends(){
        this.resetBooleans();
        this.getTrendsData();
        this.getPatronVentas();
        this.tableFilasColores();
    }
    public getTrendsData(){
        this.selectedTrendCategories = [];
        this.salesEvolution = [];
        const categoriesTrends = this.formGroup.get('categoriesTrends').value;
        const categoriesWeight = this.formGroup.get('categoriesWeights').value;
        const province = this.formGroup.get('province').value;
        let categories = [];
        if(categoriesTrends && categoriesTrends.length > 0)
            categoriesTrends.map(c => {
                if(!categories.some(ca => ca === c)){
                    categories.push(c);
                }
            });
        if(categoriesWeight && categoriesWeight.length > 0)
            categoriesWeight.map(c => {
                if(!categories.some(ca => ca === c)){
                    categories.push(c);
                }
            })
        this.trendsData.map(td => {
            if(categories.some(c => c === td.idcategory && province === td.idlocalarea)){
                if(categoriesWeight && categoriesWeight.some(c => c === td.idcategory))
                    td.color = 'green';
                else
                    td.color = 'black';
                this.selectedTrendCategories.push(td);
            }
        })

        this.selectedTrendCategories.map(sel => {
            this.salesEvolutionChartData = [];
            let tmp7 = {label: sel.category.name, data:sel.previous_day_menos7, fill:false};
            this.salesEvolutionChartData.push(tmp7);
            let tmp6 = {label: sel.category.name, data:sel.previous_day_menos6, fill:false};
            this.salesEvolutionChartData.push(tmp6);
            let tmp5 = {label: sel.category.name, data:sel.previous_day_menos5, fill:false};
            this.salesEvolutionChartData.push(tmp5);
            let tmp4 = {label: sel.category.name, data:sel.previous_day_menos4, fill:false};
            this.salesEvolutionChartData.push(tmp4);
            let tmp3 = {label: sel.category.name, data:sel.previous_day_menos3, fill:false};
            this.salesEvolutionChartData.push(tmp3);
            let tmp2 = {label: sel.category.name, data:sel.previous_day_menos2, fill:false};
            this.salesEvolutionChartData.push(tmp2);
            let tmp1 = {label: sel.category.name, data:sel.previous_day_menos1, fill:false};
            this.salesEvolutionChartData.push(tmp1);
            this.salesEvolution.push(this.salesEvolutionChartData);
        })
        this.isLoadingSalesEvolution = false;
    }
    private getCagetoriesSelected(){
        let categories = [];
        let categoriesId = [];
        if(!this.me.customer.has_trends)
            categoriesId = this.formGroup.get('categories').value
        else{
            const categoriesTrends = this.formGroup.get('categoriesTrends').value;
            const categoriesWeight = this.formGroup.get('categoriesWeights').value;
            if(categoriesTrends && categoriesTrends.length > 0)
                categoriesTrends.map(c => {
                    if(!categories.some(ca => ca === c))
                        categories.push(c);
                });
            if(categoriesWeight && categoriesWeight.length > 0)
                categoriesWeight.map(c => {
                    if(!categories.some(ca => ca === c))
                        categories.push(c);
                })
            this.trendsData.map(t => {
                if(categories.some(c => c === t.idcategory) && !categoriesId.some(c => c === t.idcategory)){
                    categoriesId.push(t.idcategory)
                }
            })
        }
        return categoriesId;
    }
    public getPatronVentas(){
        let categories = this.getCagetoriesSelected()
        
        const params = {
            from: this.formGroup.get('month').value,
            categoria: categories
        }
        this.ws.getPatronVentas(params).subscribe(result => {
            if(result.success){
                let datpatro = [];  
                let patvent = [];
                let a_ventfin = [];
                this.patronVentasLineChartData = [];
                result.data.map(val => {
                    if(!patvent[val.idcategory]){
                        patvent[val.idcategory] = [];
                    }
                    patvent[val.idcategory].push([val.weekday, val.value]);
                });        
                for(const vent in patvent){
                    let tmp = {label: vent, data:patvent[vent]};
                    a_ventfin.push(tmp);
                }
                var j_categories = this.categories;
                var a_categories = Object.keys(j_categories).map(i => j_categories[i]);
                a_ventfin.map(val => {
                    let namecat = a_categories.find(o => o.code === val.label);
                    let tmodat = [];
					for (let index = 1; index < 9; index++) {
                        let tmppat = val.data.find(o => parseInt(o[0]) === index);
                        if(tmppat){
                            tmodat[tmppat[0] - 1] = tmppat[1]
                            this.diasseleccionados.push({label: namecat.name, data:tmppat[1], weekday: index});
                        }
                        else{
                            tmodat[index - 1] = 0;
                            this.diasseleccionados.push({label: namecat.name, data:0, weekday: index});
                        }
                    }
                    let tmp = {label: namecat.name, data:tmodat, fill:false};
                    this.patronVentasLineChartData.push(tmp);
                });
                this.isLoadingPatronVentas = false;
            }
        })
    }

    public tableFilasColores(){
        let categories = this.getCagetoriesSelected();
        const params = {
            from: this.formGroup.get('month').value,
            categoria: categories
        }
        this.tableData = [];
        this.isLoadingDataTable = true;
        this.ws.getfilasdiassemana(params).subscribe(result => {
            if(result.success){
                this.tableData = [];
                result.data.data.map(dat => {
                    dat.map(d => {
                        categories.map((cat,i) => {
                            if(d.idcategory && d.idcategory == cat){
                                this.tableData[i] = dat;
                            }
                        })
                    })
                });
                this.horasseleccionados = result.data.data;
                this.processAllAfluencias(result.data.afinidades.data);
                this.isLoadingDataTable = false;
            }
        },
        error =>{
            this.dialog.warning('RECOMENDACIONES.recomendaciones.table_filas_colores.title', error);
        });
    }

    private processAllAfluencias(elements: any [], day:string = 'today'){
		this.afluencias['table'] = [];
	
		this.afluencias['table']["mujeres"] = this.groupBy(elements.filter(emision => emision._id.gender === 'female'), 'hour');
		this.afluencias['table']["hombres"] = this.groupBy(elements.filter(emision => emision._id.gender === 'male'), 'hour');
	
		this.afluencias['table']['adultos'] = this.groupBy(elements.filter(emision  => emision._id.type === 'adult'), 'hour');
		this.afluencias['table']['joven'] = this.groupBy(elements.filter(emision  => emision._id.type === 'young'), 'hour');
		this.afluencias['table']['senior'] = this.groupBy(elements.filter(emision  => emision._id.type === 'senior'), 'hour');
		this.afluencias['table']['ninios'] = this.groupBy(elements.filter(emision  => emision._id.type === 'child'), 'hour');
	}
  
	private groupBy(elements: any [], field: string){
		let result = [];
		if(field === 'hour')
			result = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		elements.map(element => {
		  if(result[element._id[field]]){
			result[element._id[field]] += element.count;
		  }else{
			result[element._id[field]] = element.count;
		  }
		});
		return result;
    }
    
    getDataTooltip(index){
        const mujeres = this.calculatePercentages(index, 'mujeres');
        const hombres = this.calculatePercentages(index, 'hombres');
        const adultos = this.calculatePercentages(index, 'adultos');
        const jovenes = this.calculatePercentages(index, 'joven');
        const senior = this.calculatePercentages(index, 'senior');
        const ninios = this.calculatePercentages(index, 'ninios');
        return `
        ${this.translate.instant('COMMON.men')}: ${hombres}% \n 
        ${this.translate.instant('COMMON.women')}: ${mujeres}% \n 
        ------- \n 
        ${this.translate.instant('COMMON.kids')}: ${ninios}% \n 
        ${this.translate.instant('COMMON.young')}: ${jovenes}% \n 
        ${this.translate.instant('COMMON.adults')}: ${adultos}% \n 
        ${this.translate.instant('COMMON.senior')}: ${senior}%`;
    }

    calculatePercentages(index, field){
        let total = 0;
        if(field === 'hombres' || field === 'mujeres'){
            total = this.afluencias['table']['hombres'][index] + this.afluencias['table']['mujeres'][index];
        }else{
            total = this.afluencias['table']['adultos'][index] + this.afluencias['table']['joven'][index] + this.afluencias['table']['senior'][index] + this.afluencias['table']['ninios'][index];
        }
        let percentage:number = 0;
        if(this.afluencias['table'][field][index] !== 0 && total !== 0)
            percentage = (this.afluencias['table'][field][index] * 100) / total;

        return Math.round((percentage + Number.EPSILON) * 100) / 100;
    }

    showChartsBlock(category, index){
        this.selectedCategory = category.name;
        if(!this.promoControl.value){
            this.dialog.warning('RECOMENDACIONES.recomendaciones.warning.promo.title', 'RECOMENDACIONES.recomendaciones.warning.promo.text');
        }
        const combinemejores:any = this.calcularMaxdiaYhora(category.name, index);
        this.combinemejores = combinemejores;
        this.genderPieChartData = [];
        this.typePieChartData = [];
        this.genderPieChartData = this.getPieData('gender', combinemejores, index);
        this.typePieChartData = this.getPieData('type', combinemejores,index);
        this.getTemperature(category);
        if(this.promoControl.value === '0'){
            this.getProductos(category);
        }else{
            this.getAsociados(category);
        }
        this.showThirdBlock = true;
    }

    getPieData(field, combinemejores, index){
        const hour = parseInt(combinemejores[0].hour[0].hour);
        let response = [];
        if(field === 'gender'){
            response = [this.calculatePercentages(hour,'hombres'),this.calculatePercentages(hour,'mujeres')];
            this.profileGender = response;
        }else{
            response = [this.calculatePercentages(hour,'ninios'),this.calculatePercentages(hour,'joven'),this.calculatePercentages(hour,'adultos'),this.calculatePercentages(hour,'senior')];
            this.profileType = response;
        }
        return response;
    }

    calcularMaxdiaYhora(tituperficat, index){
        let a_horas = [];
        let a_dias = [];
        let ncasos = 3;
        let dias = this.diasseleccionados.filter(selec => selec.label === tituperficat);
        let horas = this.horasseleccionados[index];
        horas.map(hora => {
            a_horas.push({value: hora.value, hour: hora.hour});
        });
        a_horas.pop();
        a_horas = a_horas.sort(function(a, b) {
            return a.value - b.value;
        });
        a_horas.reverse()
        dias.map(dia => {
            a_dias.push({value: dia.data, weekday: dia.weekday});
        })
        a_dias.pop();
        a_dias = a_dias.sort(function(a, b) {
            return a.value-b.value;
        });
        a_dias.reverse();
        let mejoreshoras = a_horas.slice(0, ncasos);
        let mejoresdias = a_dias.slice(0, ncasos);
        let combinemejores = [];
        combinemejores.push({'hour':mejoreshoras,'weekday':mejoresdias });
        return combinemejores;
    }

    getTemperature(category){
        this.ws.getTemperature({idcategory: category}).subscribe(result => {
            if(result.success){
                const temperature = result.data;
                if(!temperature)
                    this.temperature = 'templado';
                else{
                    if(temperature.estado === 'C')
                        this.temperature = 'calor';
                    if(temperature.estado === 'F')
                        this.temperature = 'frio';
                
                    this.incremento = result.inc
                }
            }
        })
    }

    getProductos(category){
        const params = {
            category: category.idcategoria,
            mes: this.formGroup.get('month').value
        }
        this.fieldDataProducts.options = [];
        this.ws.getProductsFromCategory(params).subscribe(result => {
            if(result.success){
                if(result.data.length === 0){
                    this.dialog.warning('RECOMENDACIONES.recomendaciones.warning.products.title', 'RECOMENDACIONES.recomendaciones.warning.products.text')
                }
                result.data.map(product => {
                    this.productos.push(product);
                    const percentage = (Math.round(product.value * 10) / 10).toString();
                    const chars = percentage.length + product.name.length;
                    const dashesCount = this.totalChars - chars;
                    let dashes = '';
                    for(let i = 0; i < dashesCount; i++){
                        dashes +='-';
                    }
                    this.fieldDataProducts.options.push({name: `${product.name} ${dashes} ${percentage}%`, value: product.code});
                })
                this.loadingProducts = false;
            }
        },
        error => {
            console.log(error);
        })
    }

    getAsociados(category){
        this.ws.getAssociationsConditionalProbability(category.idcategoria).subscribe(result => {
            if(result.success){
                if(result.data.length === 0){
                    this.dialog.warning('RECOMENDACIONES.recomendaciones.warning.data.title', 'RECOMENDACIONES.recomendaciones.warning.data.text');
                    this.loadingAssociated = false;
                    return;
                }
                this.associations = [];
                result.data.map(res => {
                    this.associations.push(res);
                });
                this.getProductosAsociados(category, true);
                //this.getProductosAsociados(this.associations[0],false);
                this.loadingAssociated = false;
            }else{
                this.dialog.warning('RECOMENDACIONES.recomendaciones.warning.data.title', 'RECOMENDACIONES.recomendaciones.warning.data.text');
                this.loadingAssociated = false;
                return;
            }
        },
        error => {
            this.dialog.warning('RECOMENDACIONES.recomendaciones.warning.data.title', 'RECOMENDACIONES.recomendaciones.warning.data.text2');
            this.loadingAssociated = false;
            return;
        });
    }

    getProductosAsociados(category, principal){
        let params:any = {
            mes: this.formGroup.get('month').value
        }
        if(principal){
            this.fieldDataProductsPrincipal.options = [];
            params.category = category.idcategoria
        }
        else{
            this.fieldDataProductsAsociados.options = [];
            let cat = this.categories.filter(c => c.code === category.category_code);
            params.category = cat[0].idcategoria;
        }

        this.ws.getProductsFromCategory(params).subscribe(result => {
            if(result.success){
                if(result.data.length === 0){
                    this.dialog.warning('RECOMENDACIONES.recomendaciones.warning.products.title', 'RECOMENDACIONES.recomendaciones.warning.products.text')
                }
                result.data.map(product => {
                    if(principal){
                        this.productos.push(product);
                        const percentage = (Math.round(product.value * 10) / 10).toString();
                        const chars = percentage.length + product.name.length;
                        const dashesCount = this.totalChars - chars;
                        let dashes = '';
                        for(let i = 0; i < dashesCount; i++){
                            dashes +='-';
                        }
                        this.fieldDataProductsPrincipal.options.push({name: `${product.name} ${dashes} ${percentage}%`, value: product.code});
                    }else{
                        this.productosAsociados.push(product);
                        this.selectedAssociatedCategory = category.name;
                        const percentage = (Math.round(product.value * 10) / 10).toString();
                        const chars = percentage.length + product.name.length;
                        const dashesCount = this.totalChars - chars;
                        let dashes = '';
                        for(let i = 0; i < dashesCount; i++){
                            dashes +='-';
                        }
                        this.fieldDataProductsAsociados.options.push({name: `${product.name} ${dashes} ${percentage}%`, value: product.code});
                    }
                })
            }
        },
        error => {
            console.log(error);
        })
    }
    changeAssociated(category){
        this.showProducts = true;
        this.getProductosAsociados(category, false);
    }
    changePromoControl(promo){
        this.showThirdBlock = false;
        this.loadingProducts = true;
        this.loadingAssociated = true;
    }

    resetBooleans(){
        this.loadingMonths = false;
        this.loadingProvinces = false;
        this.loadingCategories = false;
        this.loadingTrendCategories = false;
        this.loadingWeightCategories = false;
        this.loadingAssociated = true;
        this.loadingProducts = true;
        this.isLoadingPatronVentas = true;
        this.isLoadingDataTable = true;    
        this.showThirdBlock = false;
        this.showProducts = false;
    }
    goToNewPlayslist(){
        const temperatura = {
            calor: [309,339],
            templado: [308,337],
            frio: [307,336]
        }
        let params:any = {
            horas: this.combinemejores[0].hour,
            genre: this.profileGender[0] > this.profileGender[1] ? [131,325] : [132,326],
            type: this.mayor(this.profileType),
            temperatura: temperatura[`${this.temperature}`]
        }
        if(this.formGroup.get('products').value){
            params.product = this.productos.filter(prod => prod.code === this.formGroup.get('products').value)[0];
        }else{
            params.product = this.productos.filter(prod => prod.code === this.formGroup.get('productsPrin').value)[0];
            params.assocProd = this.productosAsociados.filter(prod => prod.code === this.formGroup.get('productsAssoc').value)[0];
        }
        if(this.formGroup.get('month').value){
            params.month = this.formGroup.get('month').value;
        }
        console.log(params);
        this.recomendationStore.set(params);
        this.router.navigate(['/private/content/create']);
    }
    mayor(array:any[]){
        let mayor;
        const tipos = [[312,333],[313,334],[314,335],[315,338]];
        array.map((arr, index) => {
            if(index === 0)
                mayor = index;
            if(arr > array[index - 1])
                mayor = index;
        })
        return tipos[mayor];
    }
  }