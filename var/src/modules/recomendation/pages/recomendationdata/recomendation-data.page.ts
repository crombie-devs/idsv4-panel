import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormField } from 'src/modules/form-field/models/form-field';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { UserStore } from "src/modules/user/stores/user.store";
import { User } from "src/modules/user/models/user";
import { WSService } from 'src/services/ws.service';
import { Customer } from 'src/modules/customer/models/customer';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as XLSX from 'xlsx';
import { CONFIG } from 'src/app/app.config';
import { DialogService } from 'src/modules/dialog/services/dialog.service';

@Component({
    templateUrl: './recomendation-data.page.html',
    selector: 'recomendation-data-page',
    styleUrls: ['./recomendation-data.page.scss'],
  })
  
  export class RecomendationDataPage {
    public me: User;
    public customers:Customer[] = [];
    public files:File[] = [];
    public upload:boolean = false;
    public checked:boolean = false;
    public fieldDataCustomer: FormField = {
		label: "Cliente",
		name: 'customer',
		placeholder: 'Selecciona un cliente',
		required: false,
        multiple:true,
        options:[]
    };
    public fieldDataFiles: FormField[] = [];
    public loading:boolean = false;

    public formGroup:FormGroup = new FormGroup({
        customer: new FormControl(null)
    });
    constructor(
        private translate: TranslateService,
        private ustore: UserStore,
        private ws: WSService,
        private _snackBar: MatSnackBar,
        private dialog: DialogService, 
    ) {
        this.ustore.onChange.subscribe(result => { this.me = result; this.setCustomerData() });
    }

    ngOnInit(): void {

    }

    private setCustomerData(){
        if(this.me && this.me.customer){
            this.fieldDataCustomer.options.push({ value: this.me.customer.idcustomer, name: this.me.customer.name});
            this.formGroup.get('customer').setValue([this.me.customer.idcustomer]);

            this.fieldDataCustomer.disabled = true;
            this.upload = true;
            this.checked = true;
            if(!this.customers.some(c => c.idcustomer === this.me.customer.idcustomer)){
                this.customers.push(this.me.customer);
            }
            this.changedCustomers(null);
        }else{
            this.getCustomers();
        }
    }

    private getCustomers(){
        this.ws.customersToSelect().subscribe((result) => {
            if(result.success && result.data.length > 0) {
                this.fieldDataCustomer.options = [];
                result.data.map(c => c.has_sales && c.has_influxes ? this.fieldDataCustomer.options.push({ value: c.idcustomer, name: c.name, disabled: false}) : null)
                this.customers = result.data;
            }
        }, error => {
            this.openSnackBar(error.message);
        });
    }
    public checkUploadFiles(data, fromChangedCustomers = false){
        if(!fromChangedCustomers)
            this.checked = !this.checked;
        let customers = this.formGroup.get('customer').value;
        let customs:Customer[] = [];
        this.fieldDataCustomer.options.map(o => o.disabled = false);
        if((!customers || customers.length === 0) && this.checked){
            this.openSnackBar("Selecciona primero al menos un cliente.");
        }else{
            if(this.checked){
                this.customers.map(c => {
                    if(customers.includes(c.idcustomer)){
                        customs.push(c);
                    }
                })
                this.fieldDataFiles = [];
                customs.map(c => {
                    this.fieldDataFiles.push({
                        label: `Datos de tickets`,
                        name: `file-${c.name}`,
                        placeholder: 'Selecciona uno varios archivos excel',
                        accept:'.xlsx,.xls',
                        required: true,
                        multiple: true
                    });
                    this.formGroup.addControl(`file-${c.name}`, new FormControl(null, Validators.required))
                })
            }else{
                this.fieldDataFiles.map(df => {
                    this.formGroup.removeControl(df.name);
                });
                this.fieldDataFiles = [];
            }
        }
    }
    public changedCustomers(data){
        if(this.upload){
            this.checkUploadFiles(data, true)
        }
    }
    public submitForm(data){
        this.loading = true;
        let formdata = this.formGroup.getRawValue();

        if(this.upload)
            formdata.customer.map(c => {
                const customer = this.customers.filter(cus => cus.idcustomer === c)[0];
                const files = formdata[`file-${customer.name}`];
                files.map(file => {
                    this.getData(customer, file);
                })
            })
        else
            this.updateDataCustomer(formdata.customer);
    }

    private uploadCustomerFiles(customer, file, csvData){
        let formData: any = new FormData();
        formData.append("tickets", file);
        formData.append("customer", JSON.stringify(customer));
        formData.append("email", this.me.email);
        if(!CONFIG.demo && CONFIG.production)
            this.ws.uploadTicketsFiles(customer.idcustomer, formData).subscribe(result => {
                if(result.success){
                    this.ws.uploadTicketsFilesToCore(customer.idcustomer, formData).subscribe(result => {
                        if(result.success){
                            this.loading = false;
                            this.dialog.warning('Actualización de datos', "El proceso de actualización se ha iniciado. Recibirá un correo electrónico cuando termine.");
                        }
                    },
                    error => {
                        this.dialog.warning('Actualización de datos', "Ha habido un problema al guardar los datos.");
                        this.loading = false;          
                    })
                }
            });
        else{
            this.loading = false;
            this.dialog.warning('Actualización de datos', "El proceso de actualización se ha iniciado. Recibirá un correo electrónico cuando termine.");
        }
    }

    private updateDataCustomer(customers){
        let customs:Customer[] = [];
        customers.map(c => {
            const customer = this.customers.filter(cus => cus.idcustomer === c)[0];
            customs.push(customer);
        })
        if(!CONFIG.demo && CONFIG.production)
            this.ws.updateRecomedationsData({email: this.me.email, customers: customs}).subscribe(result => {
                if(result.success){
                    this.formGroup.patchValue({customer: null});
                    customers.map(c => {
                        this.fieldDataCustomer.options.map((co,i) => {
                            if(co.value === c)
                                co.disabled = true;
                        })
                    })
                    this.loading = false;
                    this.dialog.warning('Actualización de datos', "El proceso de actualización se ha iniciado. Recibirá un correo electrónico cuando termine.");
                }
            });
        else{
            this.loading = false;
            this.dialog.warning('Actualización de datos', "El proceso de actualización se ha iniciado. Recibirá un correo electrónico cuando termine.");
        }
    }

    getData(customer:Customer, file:File){
        const indexes = ['site_id', 
                          'site_name', 
                          'date', 
                          'time', 
                          'ticket_id',
                          'line_number',
                          'category1_id ', 
                          'category1_name',  
                          'product_id', 
                          'product_name', 
                          'units',
                          'total_amount',
                          'currency'];
        let workBook = null;
        let jsonData = null;
        const reader = new FileReader();
        reader.onload = (event) => {
          const data = reader.result;
          workBook = XLSX.read(data, { type: 'binary' });
          jsonData = workBook.SheetNames.reduce((initial, name) => {
            const sheet = workBook.Sheets[name];
            initial[name] = XLSX.utils.sheet_to_json(sheet);
            return initial;
          }, {});
          const csvData = workBook.SheetNames.reduce((initial, name) => {
            const sheet = workBook.Sheets[name];
            initial[name] = XLSX.utils.sheet_to_csv(sheet);
            return initial;
          }, {});
          let filteredData:any[] = [];
          let sheetkeys = Object.keys(jsonData);
          const keys = Object.keys(jsonData[sheetkeys[0]][0]);
          let valids:boolean = keys.some(key => indexes.some(k => k === key));
          if(valids)
            this.uploadCustomerFiles(customer, file, csvData);
          else
            this.dialog.warning('Actualización de datos', `El archivo ${file.name} no contiene la estructura necesaria para ser subido.`);
        }
        reader.readAsBinaryString(file);
    }
    public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
}