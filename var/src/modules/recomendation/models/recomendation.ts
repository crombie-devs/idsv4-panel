import { Product } from 'src/modules/product/models/product';
import { Hour } from 'src/models/hour';

export interface IRecomendation {
  horas: Hour[];
  product: Product;
  month:number;
  temperatura: number[];
  genre: number[]; 
  type: number[];
  assocProd?: Product;
}

export class Recomendation implements IRecomendation {
  horas: Hour[];
  product: Product;
  month:number;
  temperatura: number[];
  genre: number[]; 
  type: number[];
  assocProd?: Product;

	constructor(obj?: IRecomendation) {
		if(!obj)
      return;

    this.horas = obj.horas;
    this.product = obj.product;
    this.month = obj.month;
    this.temperatura = obj.temperatura;
    this.genre = obj.genre;
    this.type = obj.type; 
    this.assocProd = obj.assocProd;
  }
}
