import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import { RecomendationRoutingModule } from './recomendation-routing.module';
import { RecomendationStore } from './stores/recomendations.store';
import { ChartGraphsModule } from 'src/modules/chart/charts.module';
import { FormFieldModule } from 'src/modules/form-field/form-field.module';

import { AssociationsPage } from './pages/asociaciones/asociaciones.page';
import { RecomendationsPage } from './pages/recomendaciones/recomendaciones.page';
import { RecomendationDataPage } from './pages/recomendationdata/recomendation-data.page';
import { SharedModule } from 'src/shared/shared.module';
import { PipesModule } from 'src/pipes/pipes.module';

@NgModule({
  declarations: [AssociationsPage, RecomendationsPage, RecomendationDataPage],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FormFieldModule,
    ChartGraphsModule,
    RecomendationRoutingModule,
    SharedModule,
    PipesModule
  ],
  providers: [RecomendationStore],
  exports:[]
})
export class RecomendationModule {}