import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/shared/shared.module';
import { FlexModule } from '@angular/flex-layout';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DirectivesModule } from 'src/directive/directives.module';
import { MaterialModule } from 'src/app/material.module';

import { PLAYCIRCUITS_COMPONENTS } from './components/components';
import { PipesModule } from 'src/pipes/pipes.module';
import { PlaycircuitRoutingModule } from './playcircuit-routing.module';
import { PlaycircuitStore } from './stores/playcircuit.store';
import { LocationModule } from 'src/modules/location/location.module';

import { IndoorLocationModule } from 'src/modules/indoor-location/indoor-location.module';

import { PlaycircuitListPage } from './pages/list/list.page';
import { PlaycircuitFormPage } from './pages/form/form.page';
import { CustomerModule } from 'src/modules/customer/customer.module';

@NgModule({
  declarations: [PLAYCIRCUITS_COMPONENTS, PlaycircuitListPage, PlaycircuitFormPage],
  imports: [
    CommonModule,   
    FlexModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    DirectivesModule,
    PipesModule,
    LocationModule,
    IndoorLocationModule,
    PlaycircuitRoutingModule,
    CustomerModule,
    SharedModule
  ],
  providers: [PlaycircuitStore],
  exports: [PLAYCIRCUITS_COMPONENTS]
})
export class PlaycircuitModule {}