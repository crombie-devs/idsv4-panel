import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { PlaycircuitOptionComponent } from './option.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';

import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

import { MaterialModule } from 'src/app/material.module';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatDialog } from '@angular/material/dialog';

import { WSService } from 'src/services/ws.service';
import { HttpClient } from "@angular/common/http";
import { HttpLoaderFactory } from "src/shared/shared.module";
import { DebugStore } from 'src/stores/debug.store';
import { UserModule} from 'src/modules/user/user.module';
import moment from 'moment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogModule } from 'src/modules/dialog/dialog.module';

import { PlayCircuitMock } from 'src/modules/playcircuit/models/playcircuit.mock';
import { of } from "rxjs";

const MatDialogMock = {
    open(){
        return {
            afterClosed: () => of([2954])
        }
    }
}

describe('Componente: PlaycircuitOptionComponent', () => {
    let component: PlaycircuitOptionComponent;
    let fixture: ComponentFixture<PlaycircuitOptionComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports:[ 
                UserModule,
                MaterialModule,
                OverlayModule,
                RouterTestingModule,
                BrowserAnimationsModule,
                DialogModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                }),
                HttpClientTestingModule],
            declarations: [
                PlaycircuitOptionComponent
            ],
            providers:[
                WSService, 
                {
                    provide: MatDialog, useValue: MatDialogMock
                }
            ],
            schemas:[CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        }).compileComponents();
    });

    beforeEach(async(() => {
        fixture = TestBed.createComponent(PlaycircuitOptionComponent);
        component = fixture.componentInstance;
        component.circuit = PlayCircuitMock;
        fixture.whenStable().then(() => {
            fixture.detectChanges();
        });
    }));

    it("Creando componente", () => {
        expect(component).toBeTruthy();
    });

    it("Función changeStatus: true", () => {
        const spy1 = spyOn(component.selectedChange, 'emit');
        const spy2 = spyOn((component as any),'render').and.callThrough();
        component.changeStatus(true);
        expect(component.selected).toEqual(true);
        expect(spy1).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
    })
    it("Función changeStatus: false", () => {
        const spy1 = spyOn(component.selectedChange, 'emit');
        const spy2 = spyOn((component as any),'render').and.callThrough();
        component.changeStatus(false);
        expect(component.selected).toEqual(false);
        expect(spy1).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
    })
    it("Función changeSites", () => {
        const spy = spyOn((component as any), 'changeSites').and.callThrough();
        const spy1 = spyOn(component.sitesChange, 'emit');
        const spy2 = spyOn((component as any),'render').and.callThrough();
        component.openDialog();
        expect(spy).toHaveBeenCalled();
        expect(spy1).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
    });
});