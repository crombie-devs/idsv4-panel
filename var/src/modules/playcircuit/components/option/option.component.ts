import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { ContentSitesDialog } from 'src/modules/content/dialogs/sites/sites.dialog';
import { MatDialog } from '@angular/material/dialog';


@Component({
    selector: 'playcircuit-option',
    templateUrl: './option.component.html',
    styleUrls: ['./option.component.scss']
})
export class PlaycircuitOptionComponent implements OnInit, OnChanges {

    @Input() disabled: boolean = false;
    @Input() selected: boolean;
    @Input() circuit: PlayCircuit;
    @Input() sites: { idsite: number, idcircuit: number }[] = [];

    @Output() selectedChange = new EventEmitter<boolean>();
    @Output() sitesChange = new EventEmitter<{ idsite: number, idcircuit: number }[]>();

    public is_degradado: boolean = false;
    public totalSites: number;
    constructor(private matDialog: MatDialog) { }

    ngOnInit() {
        this.render();

        if (this.selected) {
            this.totalSites = this.circuit.sites.length;
        } else {

            this.totalSites = this.sites.map(si => si.idsite).length;
        }
    }


    setCountSites(totalSites): void {
        this.totalSites = totalSites;
    }



    ngOnChanges(changes: SimpleChanges) {
        this.render();
    }

    public changeStatus(value: boolean): void {

        if (this.disabled) { return; }
        this.selected = value;
        if (this.selected) {
            this.totalSites = this.circuit.sites.length;
        } else {
            this.totalSites = 0;
        }

        this.selectedChange.emit(this.selected);
        this.render();
    }

    private changeSites(idsites: number[]) {
        if (this.disabled) { return; }

        this.sites = idsites.map(idsite => { return { idsite: idsite, idcircuit: this.circuit.idcircuit }; });
        this.sitesChange.emit(this.sites);
        this.render();
    }

    private render() {
        this.is_degradado = (this.sites && this.sites.length > 0 && this.sites.length != this.circuit.sites.length);
    }

    public openDialog() {
        if (this.disabled) { return; }

        let ids = { selected: this.sites.map(si => si.idsite), filterByCircuit: this.circuit, invert: this.selected };

        const dialogRef = this.matDialog.open(
            ContentSitesDialog,
            { data: ids, width: 'auto', height: 'auto', minHeight: '600px', minWidth: '800px' }
        );

        dialogRef.afterClosed().subscribe((idsites: number[] | null) => {
            if (idsites && idsites.length > 0) {
                this.changeStatus(true);
                this.changeSites(idsites);
                this.totalSites = idsites.length;
                console.log("Sites al actualizar", this.totalSites);
            }
            else if (idsites && idsites.length == 0) {
                this.changeStatus(true);
                this.changeSites([]);
            }
            else if (idsites == null) {
                this.changeStatus(false);
                this.changeSites([]);
            }
        });
    }
}
