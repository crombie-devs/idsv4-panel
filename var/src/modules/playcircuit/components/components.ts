import { PlaycircuitSelectComponent } from './select/select.component';
import { PlaycircuitElementComponent } from './element/element.component';
import { PlaycircuitSelectorComponent } from './selector/selector.component';
import { PlaycircuitOptionComponent } from './option/option.component';

export const PLAYCIRCUITS_COMPONENTS = [
    PlaycircuitSelectorComponent,
    PlaycircuitElementComponent,
    PlaycircuitSelectComponent,
    PlaycircuitOptionComponent
];
