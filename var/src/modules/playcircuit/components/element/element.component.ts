import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { MatSnackBar } from '@angular/material/snack-bar';
import { WSService } from 'src/services/ws.service';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'playcircuit-element',
  templateUrl: './element.component.html',
  styleUrls: ['./element.component.scss']
})

export class PlaycircuitElementComponent implements OnInit {

  
@Input() playcircuit:PlayCircuit;
@Input() editPlaycircuit: boolean;
@Input() deletedPlaycircuit: boolean;
@Output() playcircuitDeleted = new EventEmitter<number>();

  constructor(
  	private ws: WSService,
		private router: Router,
		private debug: DebugStore,
		private dialog: DialogService,
		private _snackBar: MatSnackBar,
        private translate: TranslateService
    ) { }

	ngOnInit(){}
	 
   public update(idcircuit:number){
		if (idcircuit==0){
			this.dialog.warning('DIALOGS.warning.title', 'COMPONENTS.product_select.warning.text1').subscribe();
		}else{
			this.router.navigateByUrl('/private/playcircuit/edit/'+idcircuit);
		}
	}
	
	public delete(idcircuit:number){
		if(idcircuit == 0){
			this.dialog.warning('DIALOGS.warning.title', 'COMPONENTS.product_select.warning.text2').subscribe();
		}else{
			this.dialog.confirm(this.translate.instant('Confirmar Archivado'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
				if (confirm){
					this.ws.deletePlaycircuit(idcircuit).subscribe((result: IResult) => {
						if(result.success) {
							this.playcircuitDeleted.emit(idcircuit)
							this.debug.addInfo('WS.deletePlaycircuit', 'successful');
							this.openSnackBar('Circuito borrado correctamente');
						}
						else {            
							this.debug.addError('WS.deletePlaycircuit', result.data);	
							this.openSnackBar(result.data);			
						}
					}, error => {
						this.debug.addError('WS.deletePlaycircuit', error.message);
						this.openSnackBar(error.message);	           
					});
				}
			});
		}		
	}
	
	public openSnackBar(message: string) {
		const mess:string = message.toString();
		this._snackBar.open(mess,'', {
		  duration: 1500,
		});
	}
	

}
