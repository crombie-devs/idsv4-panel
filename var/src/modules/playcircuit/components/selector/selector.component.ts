import { Component, OnInit, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { PlaycircuitStore } from 'src/modules/playcircuit/stores/playcircuit.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { User } from 'src/modules/user/models/user';
import { PlaycircuitOptionComponent } from '../option/option.component';
import { DialogService } from 'src/modules/dialog/services/dialog.service';

@Component({
    providers: [PlaycircuitStore],
    selector: 'playcircuit-selector',
    templateUrl: './selector.component.html',
    styleUrls: ['./selector.component.scss']
})
export class PlaycircuitSelectorComponent {
    @Input() disabled: boolean = false;
    @Input() playcircuitsSelected: PlayCircuit[] = [];
    @Input() sitesSelected: { idsite: number, idcircuit: number }[] = [];

    @Output() playcircuitsSelectedChange: EventEmitter<PlayCircuit[]> = new EventEmitter();
    @Output() sitesSelectedChange: EventEmitter<{ idsite: number, idcircuit: number }[]> = new EventEmitter();
    @Output() idcircuit: EventEmitter<PlayCircuit[]> = new EventEmitter();
    @ViewChildren(PlaycircuitOptionComponent) option: QueryList<PlaycircuitOptionComponent>;
    private all_playcircuits: PlayCircuit[] = [];
    public playcircuits: PlayCircuit[] = [];

    public searchText: string = '';
    public isUser: boolean = true;
	public setCircuits:boolean = false;
    constructor(
        private ustore: UserStore,
        private cstore: PlaycircuitStore,
        private dialog: DialogService
    ) { 
        this.ustore.onChange.subscribe((user: User) => {
			if(user){
				this.isUser = user.isUser();
				this.cstore.onChange.subscribe(circuits => {
					if(circuits && this.setCircuits){
						this.all_playcircuits = circuits.filter(ci => this.ustore.hasCircuit(ci)); // filtrar circuitos por usuario
						this.playcircuits = this.all_playcircuits;
						if(this.playcircuits.length > 0) {
							if(!this.isUser)
								this.idcircuit.emit(this.playcircuits);
							else
								if(this.playcircuits.length > 1)
									this.showError(2)
								else
									this.idcircuit.emit(this.playcircuits);
						}else{
							this.showError(1);
						}
					}else{
						this.setCircuits = true;
					}
				});				
			}
		});
    }
    private showError(error){
		let message = "No se han encontrado circuitos. Si no eres el administrador del cliente, ponte en contacto con el administrador de tu cliente para que te genere un circuito.";
		if(error === 2)
			message = "Hay más de un circuito asociado a tu tienda. Ponte en contacto con el administrador de tu cliente para que solucione el problema.";
		this.dialog.warning('error', message);
	}
    public checkboxToggle(event: MatCheckboxChange) {
        if (event.source.checked) {
            this.playcircuits.forEach((element, index) => {
                this.option.toArray()[index].setCountSites(element.sites.length);
            });
        } else {
            this.playcircuits.forEach((element, index) => {
                this.option.toArray()[index].setCountSites(0);
            });
        }
        if (this.hasSelected() && !this.isAllSelected()) {
            event.source.checked = false;
        }
        this.playcircuitsSelected = (event.source.checked) ? this.playcircuits : [];
        this.playcircuitsSelectedChange.emit(this.playcircuitsSelected);
        if (!this.hasSelected()) {
            this.sitesSelected = [];
            this.sitesSelectedChange.emit(this.sitesSelected);
        }
    }
    public hasSelected(): boolean {
        return this.playcircuitsSelected.length > 0;
    }
    public isAllSelected(): boolean {
        return (this.playcircuitsSelected.length === this.all_playcircuits.length);
    }
    public filterByText(value: string) {
        this.playcircuits = (!value) ? this.all_playcircuits : this.all_playcircuits.filter(ci => ci.name.toLowerCase().includes(value.trim().toLowerCase()));
    }
    public circuitIsSelected(circuit: PlayCircuit): boolean {
        return this.playcircuitsSelected.some(ci => ci.idcircuit === circuit.idcircuit);
    }
    public sitesByCircuit(circuit: PlayCircuit) {
        return this.sitesSelected.filter(si => si.idcircuit == circuit.idcircuit);
    }
    public selectedByCircuit(circuit: PlayCircuit, selected: boolean) {
        if (selected) {
            if (!this.circuitIsSelected(circuit))
                this.playcircuitsSelected.push(circuit);
        }
        else {
            this.changeSitesByCircuit(circuit, []);
            this.playcircuitsSelected = this.playcircuitsSelected.filter(ci => ci.idcircuit != circuit.idcircuit);
        }
        this.playcircuitsSelectedChange.emit(this.playcircuitsSelected);
    }
    public changeSitesByCircuit(circuit: PlayCircuit, sites: { idsite: number, idcircuit: number }[]) {
        this.sitesSelected = this.sitesSelected.filter(si => si.idcircuit != circuit.idcircuit).concat(sites);
        this.sitesSelectedChange.emit(this.sitesSelected);
    }
}
