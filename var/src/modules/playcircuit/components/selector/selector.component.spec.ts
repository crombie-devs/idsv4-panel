import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { PlaycircuitSelectorComponent } from './selector.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';

import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

import { MaterialModule } from 'src/app/material.module';
import { OverlayModule } from '@angular/cdk/overlay';

import { WSService } from 'src/services/ws.service';
import { HttpClient } from "@angular/common/http";
import { HttpLoaderFactory } from "src/shared/shared.module";
import { DebugStore } from 'src/stores/debug.store';
import { UserModule} from 'src/modules/user/user.module';
import moment from 'moment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogModule } from 'src/modules/dialog/dialog.module';
import { MatCheckbox } from "@angular/material/checkbox";

describe('Componente: PlaycircuitSelectorComponent', () => {
    let component: PlaycircuitSelectorComponent;
    let fixture: ComponentFixture<PlaycircuitSelectorComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports:[ 
                UserModule,
                MaterialModule,
                OverlayModule,
                RouterTestingModule,
                BrowserAnimationsModule,
                DialogModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                }),
                HttpClientTestingModule],
            declarations: [
                PlaycircuitSelectorComponent
            ],
            providers:[WSService],
            schemas:[CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        }).compileComponents();
    });

    beforeEach(async(() => {
        fixture = TestBed.createComponent(PlaycircuitSelectorComponent);
        component = fixture.componentInstance;
        fixture.whenStable().then(() => {
            fixture.detectChanges();
        });
    }));

    it("Creando componente", () => {
        expect(component).toBeTruthy();
    });
});