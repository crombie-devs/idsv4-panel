import { Component, ComponentFactoryResolver, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { PlaycircuitStore } from 'src/modules/playcircuit/stores/playcircuit.store';

@Component({
    providers: [PlaycircuitStore],
    selector: 'playcircuit-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.scss']
})
export class PlaycircuitSelectComponent implements OnInit {

    @Output() onChange: EventEmitter<PlayCircuit> = new EventEmitter<PlayCircuit>();
    @Input() selected: PlayCircuit = null;
    @Input() selectedIndex: number;
    loading: boolean = true;

    constructor(public circuits: PlaycircuitStore){}

    ngOnInit() {

        this.circuits.reload(); 
        this.circuits.onChange.subscribe(result => {
            if(result)
                this.loading = false;
                if(!this.selected && this.selectedIndex >= 0)
                    this.onSelectIndex(this.selectedIndex);
                //this.refresh();
        });
    }

    private refresh() {
        if(this.selected)
            this.onSelect(this.selected);
        else if(this.selectedIndex >= 0)
            this.onSelectIndex(this.selectedIndex);
    }

    onSelectIndex(index: number) {
        if(!this.circuits.hasIndex(index)){ return; }
        const circuit = this.circuits.getByIndex(index);
        this.onSelect(circuit);
        
    }

    onSelect(circuit: PlayCircuit) {
        this.selected = (this.selected != null && this.selected.isEquals(circuit))? null : circuit;
        this.onChange.emit(this.selected);
    }

    checkSelect(circuit: PlayCircuit){
   
        if(this.selected)
            return this.selected.isEquals(circuit);

        return false;
    }

}