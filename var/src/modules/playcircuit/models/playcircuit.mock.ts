import { PlayCircuit } from './playcircuit';

export const PlayCircuitMock: any = {
    tags: [],
    cities: [],
    provinces: [],
    countries: [],
    langs: [{
            idlang: 1,
            name: "Español"
        },
        {
            idlang: 5,
            name: "Catalán"
        }
    ],
    sites: [
        {
            holidays: [],
            tags: [],
            idsite: 2705,
            name: "BEGUES"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2691,
            name: "EL TEJAR"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2695,
            name: "FUENGIROLA - CENTRO"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2711,
            name: "GETAFE - M406"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2710,
            name: "GUADARRAMA"
          },
          {
            holidays: [],
            tags: [],
            idsite: 1928,
            name: "JUNCAL"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2703,
            name: "LA CREUETA"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2713,
            name: "LA NUCIA"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2712,
            name: "MARIA"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2693,
            name: "MONDA"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2704,
            name: "MONTIGALA"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2689,
            name: "OJEN"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2716,
            name: "POZUELO"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2690,
            name: "RINCON DE LA VICTORIA"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2694,
            name: "RIO PADRON"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2715,
            name: "RONDA SUR DE CASTELLON"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2709,
            name: "SAN FERNANDO - LA PRESA"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2692,
            name: "TORROX"
          },
          {
            holidays: [],
            tags: [],
            idsite: 2707,
            name: "TRES CANTOS"
          }
        ],
    powers: [],
    is_valid: false,
    idcircuit: 103,
    customer: {},
    name: "Lavado Promo 5+1"
}

export const mockPlaycircuits: PlayCircuit[] = [PlayCircuitMock];