import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaycircuitListPage } from './pages/list/list.page';
import { PlaycircuitFormPage } from './pages/form/form.page';

const routes: Routes = [
  { path: 'list', component: PlaycircuitListPage},
  { path: 'create', component: PlaycircuitFormPage, data: { breadcrumb: 'Crear circuito' }},
  { path: 'edit/:idplaycircuit', component: PlaycircuitFormPage, data: { breadcrumb: 'Editar circuito' }}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PlaycircuitRoutingModule { }
