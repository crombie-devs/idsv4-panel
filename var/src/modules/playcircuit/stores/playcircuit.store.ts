import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { environment } from "src/environments/environment";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { IPlayCircuit, PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';

const STORE_KEY: string = 'playcircuits.store.ids';

@Injectable()
export class PlaycircuitStore {

    public onChange: BehaviorSubject<PlayCircuit[]> = new BehaviorSubject([]);
    private data: PlayCircuit[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const items: IPlayCircuit[] = JSON.parse(store); 
                this.set(items);
            } else 
                this.load();
        } else
            this.load();
    }

    public get(): PlayCircuit[] {
        return this.data;
    }

    public hasIndex(index: number): boolean {
        return (this.data.length > 0 && this.data.length > index)
    }

    public getByIndex(index: number): PlayCircuit {
        return (this.hasIndex(index))? this.data[index] : null;
    }

    public set(items: IPlayCircuit[]) {
        this.save(items);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.playcircuitsToSelect().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }else{
                this.set([]);
            }
        });
    }

    private save(items: IPlayCircuit[]) {
        this.data = items.map(a => new PlayCircuit(a));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(items));
    }
}