import { Component } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { Customer } from 'src/modules/customer/models/customer';
import { Tag } from 'src/modules/tag/models/tag';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserStore } from 'src/modules/user/stores/user.store';
import { Lang } from 'src/modules/lang/models/lang';
import { SegmentationStore } from 'src/modules/socio-economic/stores/segmentation.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { ICacheConfig } from 'src/services/cache.service';
import { EventService } from 'src/shared/event.service';

@Component({
    providers: [SegmentationStore],
    selector: 'playcircuit-form',
    templateUrl: './form.page.html',
    styleUrls: ['./form.page.scss']
})

export class PlaycircuitFormPage {

    public id: number | null;
    public idcustomer: number | null;
    public me: any;
    public form: FormGroup;

    public name: FormControl = new FormControl('', [Validators.required]);
    public lang = new FormControl();

    public customers: Customer[];
    public selectedTags: Tag[];
    public selectedContentPowers: any[];
    public selectedLocations: any;
    public loading: boolean = false;
    public playcircuit: PlayCircuit = new PlayCircuit(null);
    public files;
    public maxSize = 64;
    public load: boolean = true;

    public langs: any[] = [];
    public langsSelected: any[] = [];

    public customersToSelect: boolean = false;
    public customerSelect: number = null;


    constructor(
        private ws: WSService,
        private debug: DebugStore,
        private router: Router,
        private ustore: UserStore,
        private dialog: DialogService,
        private route: ActivatedRoute,
        private fb: FormBuilder,
        private _snackBar: MatSnackBar,
        private segStore: SegmentationStore,
        private events: EventService,
    ) {

        this.id = Number(this.route.snapshot.paramMap.get('idplaycircuit'));
        this.ustore.onChange.subscribe(user => {
            this.me = user;
        });

        this.segStore.onChange.subscribe(seg => {
            if (seg && seg.length > 0) {
                let playcircuit = new PlayCircuit(null);
                playcircuit.sites = seg;
                this.onSaveLocationsDialog(playcircuit);
            }
        })
    }

    public ngOnInit() {
        document.getElementById("name").focus();
        this.verifyPermissions();
        this.initForm(null);
        if (this.id) {

            this.ws.playcircuit(this.id).subscribe((result: IResult) => {
                if (result.success) {
                    this.debug.addInfo('WS.Playcircuit', 'successful');
                    this.playcircuit = new PlayCircuit(result.data);
                    this.idcustomer = this.playcircuit.customer.idcustomer;
                    this.getLangByCustomer();
                    this.lang.setValue(this.playcircuit.langs);
                    this.initForm(this.playcircuit);
                }
                else {
                    this.load = false;
                    this.debug.addError('WS.Playcircuit', result.data);
                }
            }, error => {
                this.load = false;
                this.debug.addError('WS.Playcircuit', error.message);
            });
        } else {
            this.idcustomer = (this.me.customer) ? this.me.customer.idcustomer : null;
            this.getLangByCustomer(true);
            this.initForm(null);
        }
    }

    public getLang() {
        this.ws.langsToSelect().subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.langsToSelect', 'successful');
                this.langs = result.data;
            }
            else {
                this.debug.addError('WS.langsToSelect', result.data);

            }
        }, error => {
            this.debug.addError('WS.langsToSelect', error.message);

        });

    }

    public getLangByCustomer(newp: boolean = false) {
        if (this.idcustomer == 0)
            this.getLang();
        else
            this.ws.getCustomer(this.idcustomer).subscribe((result: IResult) => {
                if (result.success) {
                    this.debug.addInfo('WS.getCustomer', 'successful');
                    this.langs = result.data.langs;
                    if (newp) {
                        let langValue: string[] = [];
                        this.langs.map(l => langValue.push(l));
                        this.lang.setValue(langValue);
                        this.playcircuit.langs = this.langs;
                    }
                } else {
                    this.debug.addError('WS.getCustomer', result.data);
                }
            }, error => {
                this.debug.addError('WS.getCustomer', error.message);
            });
    }

    private initForm(data: PlayCircuit | null) {
        if (!data) {
            this.form = this.fb.group({
                name: this.name,
                lang: this.langs
            });
            this.load = false;
        } else {
            let c = data.customer.idcustomer;
            this.customerSelect = c;
            this.form = this.fb.group({
                name: data.name
            });
            this.load = false;
        }
    }

    public create() {
        if (this.form.invalid) return null;
        this.loading = true;

        let formData: any = new FormData();
        formData.append("name", this.form.get('name').value);
        formData.append("idcustomer", this.idcustomer);

        if (this.playcircuit.langs && this.playcircuit.langs.length > 0 && this.checkLocation)
            this.ws.createPlaycircuit(formData).subscribe((result: IResult) => {
                if (result.success) {
                    const id = result.data.idcircuit;
                    this.debug.addInfo('WS.createPlaycircuit', 'successful');
                    this.updatePlaycircuitSites(id);
                    this.updatePlaycircuitTags(id);
                    this.updatePlaycircuitCountries(id);
                    this.updatePlaycircuitProvinces(id);
                    this.updatePlaycircuitCities(id);
                    this.updatePlaycircuitLangs(id);
                    if (this.me && this.me.customer) {
                        let config: ICacheConfig = { idcustomer: this.me.customer.idcustomer };
                        if (this.me.site)
                            config = { idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer };
                        setTimeout(() => {

                            this.events.publish('circuit:change', config);
                        }, 1000);
                    }
                    this.openSnackBar("Cicuito creado correctamente");
                    this.router.navigateByUrl('/private/playcircuit/list');
                    this.loading = false;
                } else {
                    this.debug.addError('WS.createPlaycircuit', result.data);
                    this.openSnackBar(result.data);
                    this.loading = false;
                }
            }, error => {
                let msj = JSON.stringify(error.error.data);
                this.openSnackBar(msj.substr(2, msj.length - 3));
                this.debug.addError('WS.createPlaycircuit', error.message);
                this.loading = false;
            });
        else {
            this.loading = false;
            this.dialog.warning('DIALOGS.warning.title', 'Es obligatorio seleccionar localizaciòn e idioma').subscribe();
        }
    }

    public update() {
        if (this.form.invalid) return null;
        this.loading = true;

        let formData: any = new FormData();
        formData.append("name", this.form.get('name').value);
        formData.append("idcustomer", this.idcustomer);

        if (this.playcircuit.langs && this.playcircuit.langs.length > 0 && this.checkLocation)
            this.ws.updatePlaycircuit(this.id, formData).subscribe((result: IResult) => {
                if (result.success) {
                    this.debug.addInfo('WS.updatePlaycircuit', 'successful');
                    this.openSnackBar("Playcircuit actualizado correctamente");
                    this.updatePlaycircuitSites(this.id);
                    this.updatePlaycircuitTags(this.id);
                    this.updatePlaycircuitCountries(this.id);
                    this.updatePlaycircuitProvinces(this.id);
                    this.updatePlaycircuitCities(this.id);
                    this.updatePlaycircuitLangs(this.id);
                    if (this.me && this.me.customer) {
                        let config: ICacheConfig = { idcustomer: this.me.customer.idcustomer };
                        if (this.me.site)
                            config = { idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer };
                        setTimeout(() => {

                            this.events.publish('circuit:change', config);
                        }, 1000);
                    }
                    this.router.navigateByUrl('/private/playcircuit/list');
                    this.loading = false;
                }
                else {
                    this.debug.addError('WS.updatePlaycircuit', result.data);
                    this.openSnackBar(result.data);
                    this.loading = false;

                }
            }, error => {
                let msj = JSON.stringify(error.error.data);
                this.openSnackBar(msj.substr(2, msj.length - 3));
                this.debug.addError('WS.updatePlaycircuit', error.message);
                this.loading = false;
            });
        else {
            this.loading = false;
            this.dialog.warning('DIALOGS.warning.title', 'Es obligatorio seleccionar localizaciòn e idioma').subscribe();
        }
    }

    public updatePlaycircuitTags(idplaycircuit: number) {

        if (this.playcircuit.tags)
            this.ws.updatePlaycircuitTags(idplaycircuit, { "tags": this.playcircuit.tags }).subscribe((result: IResult) => {
                if (result.success) {
                    this.debug.addInfo('WS.updatePlaycircuitTags', 'successful');
                }
                else {
                    this.debug.addError('WS.updatePlaycircuitTags', result.data);
                    this.openSnackBar(result.data);
                }
            }, error => {
                this.debug.addError('WS.updatePlaycircuitTags', error.message);
                this.openSnackBar(error.message);
            });
    }

    public updatePlaycircuiSites(idplaycircuit: number) {
        if (this.playcircuit.sites && this.playcircuit.sites.length > 0)
            this.ws.updatePlaycircuitSites(idplaycircuit, { "sites": this.playcircuit.sites }).subscribe((result: IResult) => {
                if (result.success) {
                    this.debug.addInfo('WS.updatePlaycircuitSites', 'successful');
                }
                else {
                    this.debug.addError('WS.updatePlaycircuitSites', result.data);
                    this.openSnackBar(result.data);
                }
            }, error => {
                this.debug.addError('WS.updatePlaycircuitSites', error.message);
                this.openSnackBar(error.message);
            });
    }

    public updatePlaycircuitCountries(idplaycircuit: number) {
        if (this.playcircuit.countries)
            this.ws.updatePlaycircuitCountries(idplaycircuit, { "countries": this.playcircuit.countries }).subscribe((result: IResult) => {
                if (result.success) {
                    this.debug.addInfo('WS.updatePlaycircuitCountries', 'successful');
                }
                else {
                    this.debug.addError('WS.updatePlaycircuitCountries', result.data);
                    this.openSnackBar(result.data);
                }
            }, error => {
                this.debug.addError('WS.updatePlaycircuitCountries', error.message);
                this.openSnackBar(error.message);
            });
    }

    public updatePlaycircuitProvinces(idplaycircuit: number) {
        if (this.playcircuit.provinces)
            this.ws.updatePlaycircuitProvinces(idplaycircuit, { "provinces": this.playcircuit.provinces }).subscribe((result: IResult) => {
                if (result.success) {
                    this.debug.addInfo('WS.updatePlaycircuitProvinces', 'successful');
                }
                else {
                    this.debug.addError('WS.updatePlaycircuitProvinces', result.data);
                    this.openSnackBar(result.data);
                }
            }, error => {
                this.debug.addError('WS.updatePlaycircuitProvinces', error.message);
                this.openSnackBar(error.message);
            });
    }

    public updatePlaycircuitCities(idplaycircuit: number) {

        if (!this.playcircuit.hasOwnProperty('cities')) {
            this.playcircuit.cities = [];
        }

        this.ws.updatePlaycircuitCities(idplaycircuit, { "cities": this.playcircuit.cities }).subscribe((result: IResult) => {
            if (result.success) {
                let data = result.data;
                this.debug.addInfo('WS.updatePlaycircuitCities', 'successful');
            }
            else {
                this.debug.addError('WS.updatePlaycircuitCities', result.data);
                this.openSnackBar(result.data);
            }
        }, error => {
            this.debug.addError('WS.updatePlaycircuitCities', error.message);
            this.openSnackBar(error.message);
        });
    }

    public updatePlaycircuitSites(idplaycircuit: number) {

        if (!this.playcircuit.sites) {
            this.playcircuit.sites = [];
        }

        if (this.playcircuit.sites)
            this.ws.updatePlaycircuitSites(idplaycircuit, { "sites": this.playcircuit.sites }).subscribe((result: IResult) => {
                if (result.success) {
                    let data = result.data;
                    this.debug.addInfo('WS.updatePlaycircuitSites', 'successful');
                }
                else {
                    this.debug.addError('WS.updatePlaycircuitSites', result.data);
                    this.openSnackBar(result.data);
                }
            }, error => {
                this.debug.addError('WS.updatePlaycircuitSites', error.message);
                this.openSnackBar(error.message);
            });
    }

    public updatePlaycircuitLangs(idplaycircuit: number) {
        if (this.playcircuit.langs && this.playcircuit.langs.length > 0)
            this.ws.updatePlaycircuitLangs(idplaycircuit, { "langs": this.playcircuit.langs }).subscribe((result: IResult) => {
                if (result.success) {
                    this.debug.addInfo('WS.updatePlaycircuitLangs', 'successful');
                }
                else {
                    this.debug.addError('WS.updatePlaycircuitLangs', result.data);
                    this.openSnackBar(result.data);
                }
            }, error => {
                this.debug.addError('WS.updatePlaycircuitLangs', error.message);
                this.openSnackBar(error.message);
            });
    }

    public checkLocation(): boolean {
        if (this.playcircuit.cities && this.playcircuit.cities.length > 0)
            return true;
        else if (this.playcircuit.provinces && this.playcircuit.provinces.length > 0)
            return true;
        else if (this.playcircuit.countries && this.playcircuit.countries.length > 0)
            return true;
        else
            return false;
    }

    public onSaveLocationsDialog(playcircuit: PlayCircuit) {


        this.playcircuit = new PlayCircuit(playcircuit);
    }

    public onSaveTagDialog(playcircuit: PlayCircuit) {
        this.selectedTags = playcircuit.tags;
        this.playcircuit = new PlayCircuit(playcircuit);
    }

    public customerSelected(idcustomer: number) {
        this.idcustomer = idcustomer;
        this.getLangByCustomer();
    }

    public onRemoveTag(idtag: number) {
        this.selectedTags.forEach(function (tag, i) {
            if (tag.idtag == idtag)
                this.selectedTags.splice(i, 1)
        }, this)
    }

    public onSaveContentPowerDialog(selectedContentPowers: any[]) {
        this.selectedContentPowers = selectedContentPowers
    }

    public onRemoveContentPower(idcontentpower: number) {
    }


    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }

    public selLang() {
        this.playcircuit.langs = this.lang.value;
        this.langsSelected = this.lang.value;
    }

    compareFnLang(x: Lang, y: Lang) {
        return x && y ? x.idlang === y.idlang : x === y;
    }

    public openDialog() {
        this.dialog.tagcategoryselect("Tags", this.playcircuit.tags, this.idcustomer).afterClosed().subscribe((result) => {
            if (!result.cancel)
                this.playcircuit.tags = result.tags;
            //thi.smodalSave.emit(this.playcircuit)
        })
    }
    private verifyPermissions() {
        this.customersToSelect = this.ustore.hasPermissionByName("get.customers.toselect");
    }

}
