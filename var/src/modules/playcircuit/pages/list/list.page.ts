import { Component, OnInit } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { IResult } from 'src/models/iresult';
import { DebugStore } from 'src/stores/debug.store';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { CacheService, ICacheConfig } from 'src/services/cache.service';
import { EventService } from 'src/shared/event.service';

@Component({
  selector: 'playcircuit-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss']
})
export class PlaycircuitListPage implements OnInit {
  
	public newPlaycircuit: boolean = false;
	public editPlaycircuit: boolean = false;
	public deletedPlaycircuit: boolean = false;
	public customersToSelect: boolean = false;
	public permission: boolean = false;
	public load: boolean = false;
	public me: any;
	public pCircuits:PlayCircuit[] = [];
	public dataTemp:PlayCircuit|any;
	public playCircuitsFull:PlayCircuit|any;
	
  constructor(
    private ws: WSService,
		private debug: DebugStore,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private router: Router,
		private dialog: DialogService,
		private formBuilder:FormBuilder,
		private events: EventService,
		private eventc: CacheService
    ) { }

  ngOnInit(): void {
  	this.verifyPermissions();
    this.me=this.ustore.getUser();
    this.getPlayCircuits();
  }
  
 
  
	private getPlayCircuits(){
		this.ws.playcircuits().subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.circuits', 'successful');
				this.permission = true;
				let results = result.data;
				this.playCircuitsFull = results;
				this.dataTemp = [...this.playCircuitsFull];
				this.pCircuits = results;
				this.load = false;
			} else {
				this.load = false;
				this.dialog.warning('Error en Playcircuit', result.data);
				return null;
			}
		}, error => {
			this.load = false;
			this.debug.addError('Error en Playcircuit', error.message);
			this.openSnackBar(error.message);
			return null;          
		});	
  	}
	public filterByName(q: string) {
		if (!q) {
			this.pCircuits = this.dataTemp;
		} else {
			this.pCircuits = this.dataTemp.filter(
			(content) =>
			content.name.toLowerCase().includes(q.trim().toLowerCase()) 
		);
		}
	} 

	private verifyPermissions() {
		this.newPlaycircuit = this.ustore.hasPermissionByName("post.playcircuit")
		this.editPlaycircuit = this.ustore.hasPermissionByName("post.playcircuit.*")
		this.deletedPlaycircuit = this.ustore.hasPermissionByName("delete.playcircuit.*")
		this.customersToSelect = this.ustore.hasPermissionByName("get.customers.toselect")
	}
    
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
			duration: 1500,
		});
	}

  	public customerSelected(idcustomer:number) {
    	this.pCircuits = this.playCircuitsFull;
		if(idcustomer > 0){
			let filterArray = Object.values(this.pCircuits).filter(val => val.idcustomer == idcustomer);
			this.pCircuits = filterArray;
		}else{
			this.pCircuits = this.playCircuitsFull
		}
  	}
  
	public playcircuitDeleted(id:number){
		if(id > 0){
			let filterArray = this.pCircuits.filter(val => val.idcircuit !== id);
			this.pCircuits = filterArray;
			if(this.me && this.me.customer){
				let config: ICacheConfig = {idcustomer: this.me.customer.idcustomer};
				if(this.me.site)
				  config = {idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer};              
				setTimeout(() => {
				  console.log('publish', config);
				  this.events.publish('circuit:change', config);     
				}, 1000);
			}
		}
	}
}
