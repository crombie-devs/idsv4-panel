import { Component, EventEmitter, Input, Output, OnInit } from "@angular/core";

import { FormatStore } from '../../stores/format.store';
import { Format } from '../../models/format';

@Component({
    providers: [FormatStore],
    selector: 'format-multiselect',
    templateUrl: './format.multiselect.component.html',
    styleUrls: ['./format.multiselect.component.scss']
})
export class FormatMultiselectComponent implements OnInit {

    @Output() onChange: EventEmitter<Format[]> = new EventEmitter();
    @Input() selected: Format[] = [];
    @Input() broadcasting: number = 0;

    public formats: Format[] = [];

    constructor(private fstore: FormatStore) { }

    ngOnInit() {
        this.fstore.onChange.subscribe(formats => {
            this.formats = formats.filter(f => f.broadcasting == this.broadcasting);
        });
    }

    onSelect(format: Format) {
        const index = this.selected.findIndex(f => f.idformat === format.idformat);
        if (index >= 0)
            this.selected.splice(index, 1);
        else
            this.selected.push(format);
        this.onChange.emit(this.selected);
    }

    checkSelect(format: Format) {
        if (this.selected)
            return this.selected.some(f => format.isEquals(f));
        return false;
    }

}