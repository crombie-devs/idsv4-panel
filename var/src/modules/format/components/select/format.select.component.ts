import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Format } from 'src/modules/format/models/format';
import { FormatStore } from '../../stores/format.store';

@Component({
    providers: [FormatStore],
    selector: 'format-select',
    templateUrl: './format.select.component.html',
    styleUrls: ['./format.select.component.scss']
})
export class FormatSelectComponent implements OnInit {

    @Output() onChange: EventEmitter<Format> = new EventEmitter<Format>();
    @Input() selected: Format = null;
    @Input() selectedIndex: number;
    loading: boolean = true;

    constructor(public formats: FormatStore){}

    ngOnInit() {
        this.formats.onChange.subscribe(result => {
            if(result)
                this.loading = false;
                this.refresh();
        });
    }

    private refresh() {
        if(this.selected)
            this.onSelect(this.selected);
        else if(this.selectedIndex >= 0)
            this.onSelectIndex(this.selectedIndex);
    }

    onSelectIndex(index: number) {
        if(!this.formats.hasIndex(index)){ return; }
        const circuit = this.formats.getByIndex(index);
        this.onSelect(circuit);
    }

    onSelect(format: Format) {
        this.selected = (this.selected != null && this.selected.isEquals(format))? null : format;
        this.onChange.emit(this.selected);
    }

    checkSelect(format: Format){
        if(this.selected)
            return this.selected.isEquals(format);

        return false;
    }

}