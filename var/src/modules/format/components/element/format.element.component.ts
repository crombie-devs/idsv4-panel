import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';

import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { WSService } from 'src/services/ws.service';

import { Format } from 'src/modules/format/models/format';
import { IResult } from 'src/models/iresult';

import { DebugStore } from 'src/stores/debug.store';

import { FormatDialog } from '../../dialogs/format/format.dialog';

@Component({
  selector: 'format-element',
  templateUrl: './format.element.component.html',
  styleUrls: ['./format.element.component.scss']
})
export class FormatElementComponent implements OnInit {

  
@Input() format:Format;
@Input() editFormat: boolean;
@Input() deleteFormat: boolean;
@Output() onDeleted = new EventEmitter<number>();
@Output() onUpdate = new EventEmitter<number>();

  constructor(
  	private ws: WSService,
    private dialog: DialogService,
		private router: Router,
		private debug: DebugStore,
		private _snackBar: MatSnackBar,
		private translate: TranslateService,
		public matDialog: MatDialog
    ) { }

	ngOnInit(){
		
	}
	public update(idformat:number){
		if (idformat==0){
			this.dialog.warning('DIALOGS.warning.title', 'COMPONENTS.format.element.update.error.text').subscribe();
		}else{
			let dialogRef;
			dialogRef = this.matDialog.open(FormatDialog, {
				data: this.format
			});
			dialogRef.afterClosed().subscribe(result => {
				if (!result)
					return;
				else if (result){
					this.onUpdate.emit();
					this.openSnackBar(this.translate.instant('Formato editado correctamente'));
				}	
			});			
		}
	}
	
	public delete(idformat:number){
		if(idformat == 0){
			this.dialog.warning('DIALOGS.warning.title', 'COMPONENTS.product_select.warning.text2').subscribe();
		}else{
			this.dialog.confirm('DIALOGS.confirm.title','¿Desea borrar el formato?').subscribe(confirm => {
				if (confirm){
					this.ws.deleteFormat(idformat).subscribe((result: IResult) => {
						if(result.success) {
							this.onDeleted.emit(idformat)
							this.debug.addInfo('WS.deleteFormat', 'successful');
							this.openSnackBar(this.translate.instant('Formato borrado correctamente'));
						}
						else {            
							this.debug.addError('WS.deleteFormat', result.data);	
							this.openSnackBar(result.data);			
						}
					}, error => {
						this.debug.addError('WS.deleteFormat', error.message);
						this.openSnackBar(error.message);	           
					});
				}
			});
		}		
	}
	
	public openSnackBar(message: string) {
		const mess:string = message.toString();
		this._snackBar.open(mess,'', {
		  duration: 1500,
		});
	}
}
