export interface IFormat {
    idformat: number;
    name: string;
    description?: string;
    image_url?: string;
    frame_image_url?: string;
    width?: number;
    height?: number;
    broadcasting?: number;
}

export class Format implements IFormat {
    idformat: number;
    name: string;
    description?: string;
    image_url?: string;
    frame_image_url?: string;
    width?: number;
    height?: number;
    broadcasting?: number;

    constructor(obj?: IFormat) {
        if (!obj)
            return;

        this.idformat = obj.idformat;
        this.name = obj.name;
        this.description = obj.description;
        this.image_url = obj.image_url;
        this.frame_image_url = obj.frame_image_url;
        this.width = obj.width;
        this.height = obj.height;
        this.broadcasting = obj.broadcasting;
    }

    isEquals(format: IFormat) {
        return (this.idformat === format.idformat);
    }
}