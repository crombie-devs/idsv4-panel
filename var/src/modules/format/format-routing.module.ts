
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormatListPage } from './pages/list/list.page';

const routes: Routes = [
  { path: 'format/list', component: FormatListPage, data: { breadcrumb: 'Formatos de reproducción' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class FormatRoutingModule { }
