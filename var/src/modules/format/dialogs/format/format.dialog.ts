import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MaxSizeValidator } from '@angular-material-components/file-input';
import { Format } from 'src/modules/format/models/format';
import { WSService } from 'src/services/ws.service';
import { IResult } from 'src/models/iresult';
import { DebugStore } from 'src/stores/debug.store';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'format-dialog',
    templateUrl: './format.dialog.html',
    styleUrls: ['./format.dialog.scss']
})

export class FormatDialog {

    public title: string;
    public message: string;
    public button: string;
    public form: FormGroup;
    public id: number | null;

    public name: FormControl = new FormControl('', [Validators.required]);
    public description: FormControl = new FormControl('');
    public image_url: FormControl;
    public height: FormControl = new FormControl(0);
    public width: FormControl = new FormControl(0);
    public broadcasting: FormControl = new FormControl(false)
    public maxSize = 16;
    public files;
    public frame_files;
    public loading: boolean = false;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: Format,
        public dialogRef: MatDialogRef<FormatDialog>,
        private fb: FormBuilder,
        private ws: WSService,
        private debug: DebugStore,
        private _snackBar: MatSnackBar
    ) {
        if (this.data)
            this.id = Number(this.data.idformat);
        this.image_url = new FormControl('', [
            Validators.required,
            MaxSizeValidator(this.maxSize * 1024)
        ]);
    }

    public ngOnInit() {
        this.image_url.valueChanges.subscribe((file: File) => {
            this.files = file;
        });
        this.initForm(null);
        if (this.id) {
            this.initForm(this.data);
        }
    }

    private initForm(data: Format | null) {
        if (!data) {
            this.form = this.fb.group({
                name: this.name,
                description: this.description,
                broadcasting: this.broadcasting,
                image_url: this.image_url,
                height: this.height,
                width: this.width
            });
        } else {
            this.form = this.fb.group({
                name: data.name,
                image_url: data.image_url,
                description: data.description,
                broadcasting: data.broadcasting,
                height: data.height,
                width: data.width
            });
        }
    }

    public create() {
        if (this.form.invalid)
            return null;
        this.loading = true;
        const broad = this.form.get('broadcasting').value ? 1 : 0;
        var formData: any = new FormData();
        formData.append("name", this.form.get('name').value);
        formData.append("description", this.form.get('description').value);
        formData.append("image_url", this.form.get('image_url').value);
        formData.append("height", this.form.get('height').value);
        formData.append("width", this.form.get('width').value);
        formData.append("broadcasting", broad);

        this.ws.createFormat(formData).subscribe((result: IResult) => {
            if (result.success) {
                const id = result.data.idcircuit;
                this.debug.addInfo('WS.createFormat', 'successful');
                this.dialogRef.close(true);
                this.loading = false;
            }
            else {
                this.debug.addError('WS.createFormat', result.data);
                this.dialogRef.close(false);
                this.loading = false;
            }
        }, error => {
            let msj = JSON.stringify(error.error.data);
            this.debug.addError('WS.createFormat', error.message);
            this.openSnackBar(msj.substr(2, msj.length - 3));
            this.loading = false;
        });
    }

    public update() {
        this.loading = true;

        var formData: any = new FormData();
        const broad = this.form.get('broadcasting').value ? 1 : 0;
        formData.append("name", this.form.get('name').value);
        formData.append("description", this.form.get('description').value);
        formData.append("image_url", this.form.get('image_url').value);
        formData.append("height", this.form.get('height').value);
        formData.append("width", this.form.get('width').value);
        formData.append("broadcasting", broad);

        this.ws.updateFormat(this.id, formData).subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.updateFormat', 'successful');
                this.dialogRef.close(true);
                this.loading = false;
            }
            else {
                this.debug.addError('WS.updateFormat', result.data);
                this.dialogRef.close(false);
                this.loading = false;

            }
        }, error => {
            let msj = JSON.stringify(error.error.data);
            this.openSnackBar(msj.substr(2, msj.length - 3));
            this.debug.addError('WS.updateFormat', error.message);
            this.loading = false;
        });
    }

    public close() {
        this.dialogRef.close(false);
    }

    public save() {
        if (!this.id)
            this.create();
        else
            this.update();

    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }

}
