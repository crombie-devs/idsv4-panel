import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

import { WSService } from 'src/services/ws.service';

import { IResult } from 'src/models/iresult';
import { IFormat, Format } from '../models/format';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'formats.store.ids';

@Injectable()
export class FormatStore {

    public onChange: BehaviorSubject<Format[]> = new BehaviorSubject([]);
    private data: Format[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const formats: IFormat[] = JSON.parse(store); 
                this.set(formats);
            } else 
                this.load();
        }else
            this.load();
    }

    public get(): Format[] {
        return this.data;
    }

    public set(formats: IFormat[]) {
        this.save(formats);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.formatsToSelect().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }
        });
    }

    private save(formats: IFormat[]) {
        this.data = formats.map(s => new Format(s));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(formats));
    }

    public hasIndex(index: number): boolean {
        return (this.data.length > 0 && this.data.length > index)
    }

    public getByIndex(index: number): Format {
        return (this.hasIndex(index))? this.data[index] : null;
    }
}