import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from "src/shared/shared.module";
import { FormatRoutingModule } from './format-routing.module';

import { NgxMatFileInputModule } from "@angular-material-components/file-input";
import { FlexModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule} from "@angular/forms";

import { FormatStore } from "./stores/format.store";
import { PipesModule } from "src/pipes/pipes.module";

import { FormatElementComponent } from './components/element/format.element.component';
import { FormatMultiselectComponent } from './components/multiselect/format.multiselect.component';
import { FormatSelectComponent } from './components/select/format.select.component';
import { FormatDialog } from './dialogs/format/format.dialog';
import { FormatListPage } from './pages/list/list.page';

import { CONFIG } from 'src/app/app.config';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import moment from 'moment';

export function HttpLoaderFactory(http: HttpClient) {
  let url = CONFIG.lang_url;  
  return new TranslateHttpLoader(http, `${url}/i18n/`, `.json?timestamp=${moment().valueOf()}`);
}
@NgModule({
  declarations: [FormatElementComponent, FormatMultiselectComponent, FormatSelectComponent, FormatDialog, FormatListPage],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule,
    PipesModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    FormatRoutingModule
  ],
  providers: [FormatStore],
  entryComponents:[FormatDialog],
  exports: [FormatElementComponent, FormatMultiselectComponent, FormatSelectComponent]
})
export class FormatModule {
  constructor(private translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('es');
    if(!sessionStorage.getItem('lang')){
      sessionStorage.setItem('lang', 'es');
      translate.use('es');
    }else{
      translate.use(sessionStorage.getItem('lang'));
    }
  }
}
