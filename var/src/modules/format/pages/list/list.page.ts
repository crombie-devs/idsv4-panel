import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { WSService } from 'src/services/ws.service';

import { DebugStore } from 'src/stores/debug.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { FormatStore } from '../../stores/format.store';

import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';

import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { FormatDialog } from '../../dialogs/format/format.dialog';

import { Format } from '../../models/format';

@Component({
  providers: [FormatStore],
  selector: 'format.list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss']
})

export class FormatListPage implements OnInit {
  
	public newFormat: boolean = false;
	public editFormat: boolean = false;
	public deletedFormat: boolean = false;
	public customersToSelect: boolean = false;
	public permission: boolean = false;
	public load: boolean = false;
	public me: any;
	public formats: Format[] = [];	
	public formatsFull: Format[] = [];
	public filterVal:string='';
	
  constructor(
    private ws: WSService,
		private debug: DebugStore,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private dialog: DialogService,
		private fstore: FormatStore,
		public matDialog: MatDialog,
		private translate: TranslateService
    ) {
		this.fstore.onChange.subscribe(formats => { this.formats = formats; });
	}

  	ngOnInit(): void {
	  	this.verifyPermissions();	  
		this.me = this.ustore.getUser();
		console.log(this.formats);
  	}  
   
	private verifyPermissions() {
		this.newFormat = this.ustore.hasPermissionByName("post.format");
		this.editFormat = this.ustore.hasPermissionByName("post.format.*");
		this.deletedFormat = this.ustore.hasPermissionByName("delete.format.*");
		this.customersToSelect = this.ustore.hasPermissionByName("get.customers.toselect");
	}
  
  	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
 
  	public change(){  
		this.fstore.reload();	
	} 

  	public openDialog(){
		let dialogRef;
		dialogRef = this.matDialog.open(FormatDialog, {
			data: {}
		});
		dialogRef.afterClosed().subscribe(result => {
			if(result){
				this.openSnackBar(this.translate.instant('Formato creado correctamente'));
				this.fstore.reload();
			}
			else if(!result)
				this.openSnackBar(this.translate.instant('Error al guardar Formato'));
		});
	}
}
