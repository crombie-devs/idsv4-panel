import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexModule } from '@angular/flex-layout';
import { HttpService } from 'src/services/http.service';

import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from 'src/app/material.module';

import { SOCIOECONOMIC_COMPONENTS } from './components/components';
import { SocioEconomicRoutingModule } from './socio-economic-routing.module';

import { DashboardModule } from 'src/modules/dashboard/dashboard.module';
import { FormFieldModule } from 'src/modules/form-field/form-field.module';

import { SocioeconomicPage } from './pages/socioeconomic.page';

import { SharedModule } from 'src/shared/shared.module';
import { PipesModule } from 'src/pipes/pipes.module';

@NgModule({
  declarations: [SOCIOECONOMIC_COMPONENTS, SocioeconomicPage],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    DashboardModule,
    FormFieldModule,
    SocioEconomicRoutingModule,
    SharedModule,
    PipesModule
  ],
  providers: [HttpService],
  exports:[SOCIOECONOMIC_COMPONENTS]
})
export class SocioEconomicModule {}