import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { ISite, Site } from "src/modules/site/models/site";

const STORE_KEY: string = 'segmentation.store.ids';

@Injectable()
export class SegmentationStore {

    public onChange: BehaviorSubject<Site[]> = new BehaviorSubject(null);
    private data: Site[] = [];

    constructor() {
        let store = sessionStorage.getItem(STORE_KEY);
        if(store) {
            const segmentation: ISite[] = JSON.parse(store); 
            this.set(segmentation);
        }
    }

    public get(): Site[] {
        return this.data;
    }

    public set(segmentation: ISite[]) {
        this.save(segmentation);
        this.onChange.next(this.data);
    }

    private save(items: ISite[]) {
        this.data = items.map(segmentation => new Site(segmentation));
        sessionStorage.setItem(STORE_KEY, JSON.stringify(items));
    }

    public delete(){
        sessionStorage.removeItem(STORE_KEY);
    }
}