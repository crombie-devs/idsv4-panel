import { Component, Input, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { WSService } from 'src/services/ws.service';
import { Site } from 'src/modules/site/models/site';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import { FormField } from 'src/modules/form-field/models/form-field';

import {TranslateService} from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Component({
    selector: 'emissions-sociodemographic-user',
    templateUrl: './view.page.html',
    styleUrls: ['./styles.page.scss']
  })

export class SociodemograficoUserComponent {
    public me: any;
    public site: Site;
	public store: string = null;
    public result:any;
    public hdata: any[] = [];
    public mdata: any[] = [];
    public total = 0;
    public totales:any[] = [];
    
    public data: any[];
    public fieldDataSite: FormField = {
		label: 'store_address',
		name: 'site',
		placeholder: '',
		multiple:false,
		required: true,
        disabled:true
    };
    public fieldDataRatius: FormField = {
		label: 'radio_consider',
		name: 'ratius',
		placeholder: '',
        options:[],
		multiple: false,
		required: true
    };

    public formGroup:FormGroup = new FormGroup({
        site: new FormControl(null, Validators.required),
        ratius: new FormControl(null, Validators.required),
    });

    constructor(
		private ws: WSService,
        private ustore: UserStore,
        private dialog: DialogService,
        private translate: TranslateService) {
    }
    
    ngOnInit(): void {
        this.me = this.ustore.getUser();
        this.formGroup.patchValue({site: this.me.site.address});
        this.getRatius();
        this.formGroup.get('site').disable();
    }

    private getRatius(){
        this.ws.getRatiusOfSites().subscribe(result => {
            if(result.success){
                if(result.data.length > 0){
                    this.fieldDataRatius.options = [];
                    result.data.map(ratius => {
                        this.fieldDataRatius.options.push({value: ratius, name: ratius.toString()});
                    })
                }else{
                    this.dialog.warning('Error','No se han encontrado radios para esta dirección')
                }
            }
        })
    }

    public submit(){
        const data = this.formGroup.getRawValue();
        this.ws.getSocialEconomicData(data).subscribe(result => {
            if(result.success){
                this.result = result.data;
            }
            if(this.result.economic.length === 0 && this.result.demographic.length === 0){
                this.dialog.warning('Resultado', "No se han encontrado datos socioeconómicos para esta selección.");
            }else{
                this.hdata = [];
                this.mdata = [];
                this.total = 0;
                this.totales = [];
                if(this.result.demographic.length !== 0){
                    this.total = this.result.demographic.H.count + this.result.demographic.M.count;
                    this.hdata = Object.values(this.result.demographic.H.range);
                    this.mdata = Object.values(this.result.demographic.M.range);
                    this.hdata.map((h) => {
                        let found = false;
                        this.mdata.map((m) => {
                            if(h.range === m.range){
                                let percentage = ((h.count + m.count)*100) / this.total;
                                let stars = m.totals_stars.filter(star => star.value <= percentage);
                                let value = 0;
                                if(stars.length > 0)
                                    value = stars[stars.length - 1].star_number;
                                this.totales.push({range: h.range, count: h.count + m.count, percentage: percentage, stars: value});
                                found = true;
                            }
                        })
                        if(!found)
                            this.totales.push({range: h.range, count: h.count, percentage: ((h.count)*100) / this.total});
                    })
                    this.mdata.map(m => {
                        let found = false;
                        this.totales.map(t => {
                            if(m.range === t.range)
                                found = true;
                        });
                        if(!found)
                            this.totales.push({range: m.range, count: m.count, percentage: ((m.count)*100) / this.total});
                    })
                }
            }
        });
    }

    private orderRanges(a,b){
        a.range.split('-');
        b.range.split('-');
        return parseInt(a[0]) > parseInt(b[0]) ? -1 : 1;
    }
}