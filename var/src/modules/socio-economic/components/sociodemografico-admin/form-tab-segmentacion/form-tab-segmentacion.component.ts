import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { FormField } from 'src/modules/form-field/models/form-field';
import { WSService } from 'src/services/ws.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import { Site } from 'src/modules/site/models/site';
import { ExcelService } from 'src/services/excel.service';
import { SiteStore } from 'src/modules/site/stores/site.store';

@Component({
    selector: 'analisis-sociodemografico-admin-tab-segmentacion',
    templateUrl: './view.page.html',
    styleUrls: ['./styles.page.scss']
  })

export class FormTabSegmentacionComponent {
    @Input() sites:Site[] = [];
    @Input() loading:boolean = false;
    @Output() submitForm:EventEmitter<any> = new EventEmitter(null);
    @Output() onCleanSites:EventEmitter<any> = new EventEmitter(null);
    public me: any;
    public allAgeToOptions:any[] = [];
    public allSites:Site[] = [];
    public provinceSites:Site[] = [];

    public fieldDataRatius: FormField = {
		label: this.translate.instant('Radio a considerar:'),
		name: 'ratius',
		placeholder: '',
        options:[],
		required: true
    };
    public fieldDataProvince: FormField = {
		label: this.translate.instant('Selecciona provincia/s:'),
		name: 'province',
		placeholder: '',
        options:[],
		multiple: true,
		required: true
    };
    public fieldDataAgeFrom: FormField = {
		label: this.translate.instant('Desde:'),
		name: 'age_from',
		placeholder: '',
        options:[],
		required: false
    };
    public fieldDataAgeTo: FormField = {
		label: this.translate.instant('Hasta:'),
		name: 'age_to',
		placeholder: '',
        options:[],
		required: false
    };
    public fieldDataGender: FormField = {
		label: this.translate.instant('Genero'),
		name: 'sex',
		placeholder: '',
        options:[{value:'H',name: this.translate.instant('Hombre')}, {value:'M', name: this.translate.instant('Mujer')}],
        multiple:true,
		required: false
    };
    public fieldDataIncomePerson: FormField = {
		label: this.translate.instant('Renta Media personal:'),
		name: 'income_person',
		placeholder: '',
        options:[],
        multiple: true,
		required: false
    };
    public formGroup:FormGroup = new FormGroup({
        ratius: new FormControl(null, Validators.required),
        province: new FormControl(null, Validators.required)
    });
    public formGroupDemo:FormGroup = new FormGroup({
        age_from: new FormControl(null, Validators.required),
        age_to: new FormControl(null, Validators.required),
        sex: new FormControl(null, Validators.required)
    });
    public formGroupEco:FormGroup = new FormGroup({
        income_person: new FormControl(null),
    });
    public dataTogetIncomes:any = {};
    public allProvinces:boolean = false;

    constructor(
        private ustore: UserStore,
        private sstore: SiteStore,
        private ws: WSService,
        private excelService:ExcelService,
        private translate: TranslateService
    ) {
        this.sstore.onChange.subscribe(sites => {
            if(sites)
                this.allSites = sites.map(s => new Site(s));
        })
    }
    
    ngOnInit(): void {
        this.me = this.ustore.getUser();
        this.getProvinces();
        this.getRatius();
        this.getAgeVars();
        this.getIncomeRanges();
    }
    private getProvinces(){
        this.ws.getProvincesOfCustomer().subscribe(result => {
            if(result.success){
                this.fieldDataProvince.options = [];
                this.fieldDataProvince.options = [{value: 0, name: "Todas"}];
                result.data.map(province => {
                    this.fieldDataProvince.options.push({value: province.idprovince, name: `${province.name} (${province.sites} ${this.translate.instant('centros')})`});
                })
            }
        })
    }
   
    private getRatius(data = null){
        this.ws.getRatiusOfSites(data).subscribe(result => {
            if(result.success){
                this.fieldDataRatius.options = [];
                result.data.map(ratius => {
                    this.fieldDataRatius.options.push({value: ratius, name: ratius.toString()});
                })
            }
        })
    }

    private getAgeVars(){
        this.ws.getAgeVars().subscribe(result => {
            if(result.success){
                this.fieldDataAgeFrom.options = [];
                this.fieldDataAgeTo.options = [];
                result.data.map(data => {
                    this.fieldDataAgeFrom.options.push({value: parseInt(data.age_from), name: parseInt(data.age_from).toString()});
                    this.allAgeToOptions.push({value: parseInt(data.age_to), name: parseInt(data.age_to).toString()});
                })
                this.fieldDataAgeFrom.options.sort((a,b) => parseInt(a.value) > parseInt(b.value) ? 1 : -1);
                this.allAgeToOptions.sort((a,b) => parseInt(a.value) > parseInt(b.value) ? 1 : -1);
            }
        })
    }

    private getIncomeRanges(){
        this.ws.getIncomeRanges(this.dataTogetIncomes).subscribe(result => {
            if(result.success){
                this.fieldDataIncomePerson.options = [];
                result.data.map(data => {
                    let ranknumbers = data.rank.split('-');
                    let text = this.translate.instant(data.rank_text);
                    ranknumbers[0] = this.formatNumber(ranknumbers[0]);
                    ranknumbers[1] = this.formatNumber(ranknumbers[1]);
                    const ranknumbersString = `(${ranknumbers[0]} - ${ranknumbers[1]})`;
                    const totalChars = text.length + ranknumbersString.length;
                    let dashes:String = '';
                    for(let i = 0; i < 50 - totalChars; i++)
                        dashes +='-';
                    this.fieldDataIncomePerson.options.push({value: data.rank, name: `${text} ${dashes} ${ranknumbersString}` });
                })
            }
        })
    }
    private formatNumber(num) {
        if (!num || num == 'NaN') return '-';
        if (num == 'Infinity') return '&#x221e;';
        num = num.replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        const sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        let cents:Number = num % 100;
        let centsString:String = '';
        num = Math.floor(num / 100).toString();

        if (cents < 10)
            centsString = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
            num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
        return (((sign) ? '' : '-') + num + ((cents > 0) ? ',' + cents : ''));
    }

    public changeAgeFrom(data){
        const ageFrom = this.formGroupDemo.get('age_from').value;
        if(ageFrom >= 0){
            this.fieldDataAgeTo.options = [];
            this.allAgeToOptions.map(option => {
                if(parseInt(option.value) > parseInt(ageFrom))
                    this.fieldDataAgeTo.options.push(option);
            })
        }
    }
    public submitDemographic(){
        const data = {
            common: this.formGroup.getRawValue(),
            demographic: this.formGroupDemo.getRawValue()
        }
        this.submitForm.emit(data)
    }
    public submitEconomic(){
        const data = {
            common: this.formGroup.getRawValue(),
            economic: this.formGroupEco.getRawValue()
        }
        this.submitForm.emit(data)
    }

    public ratiusChanged(data){
        this.dataTogetIncomes.ratius = this.formGroup.get('ratius').value;
        this.getIncomeRanges();
    }
    public provinceChanged(data){
        if(this.allProvinces){
            let selected = this.formGroup.get('province').value;
            let newValues = [];
            let foundCero:boolean = false;
            selected.map((s,i) => {
                if(s !== 0){
                    newValues.push(s);
                }else{
                    foundCero = true;
                }
            })
            if(!foundCero)
                this.formGroup.get('province').setValue(null)
            else
                this.formGroup.get('province').setValue(newValues);
            this.allProvinces = false;
        }else{
            const data = this.formGroup.get('province').value;
            data.map(p => {
                if(p === 0){
                    this.allProvinces = true;
                    let values:number[] = [];
                    this.fieldDataProvince.options.map(o => {
                        values.push(o.value);
                    });
                    this.formGroup.get('province').setValue(values);
                }
            })
        }
        this.dataTogetIncomes.province = this.formGroup.get('province').value;
        this.selectProvincesSites();
        this.getIncomeRanges();
    }

    private selectProvincesSites(){
        let provinces = this.formGroup.get('province').value;
        this.provinceSites = this.allSites.filter(s => provinces.includes(s.province.idprovince));
        this.sites = this.provinceSites;
    }
    public cleanSites(data){
        this.formGroupDemo.reset();
        this.formGroupEco.reset();
        this.onCleanSites.emit(this.provinceSites);
    }
    public exportToExcel(){
        let dataExcel:any[] = [];
        const titles = {
            idcentro: this.translate.instant('idcentro'),
            code: this.translate.instant('código de centro'),
            name: this.translate.instant('Nombre'),
            address: this.translate.instant('Domicilio'),
            cp: this.translate.instant('C.P.')
        }
		this.sites.map(site => {
            let data = {};
            data[titles.idcentro] = site.idsite;
            data[titles.code] = site.code;
            data[titles.name] = site.name;
            data[titles.address] = site.address;
            data[titles.cp] = site.zipcode;
			dataExcel.push(data);
		});
        const formData = this.formGroup.getRawValue();
        let name = `${this.me.customer.name}-`;
        formData.province.map(p => name += `${this.fieldDataProvince.options.filter(o => o.value === p)[0].name}-`);
        name += `${formData.ratius}`;
		this.excelService.exportAsExcelFile(dataExcel, `${name}`);
    }
}