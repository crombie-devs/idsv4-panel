import { EventEmitter } from '@angular/core';
import { Component, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { FormField } from 'src/modules/form-field/models/form-field';
import { WSService } from 'src/services/ws.service';
import { UserStore } from 'src/modules/user/stores/user.store';

@Component({
    selector: 'analisis-sociodemografico-admin-tab-socioeconomic',
    templateUrl: './view.page.html',
    styleUrls: ['./styles.page.scss']
  })

export class FormTabSocioeconomicComponent {
    @Output() submitForm:EventEmitter<any> = new EventEmitter(null);
    public me: any;
    public fieldDataSite: FormField = {
		label: 'Selecciona local/es:',
		name: 'site',
		placeholder: '',
        options:[],
		multiple:true,
		required: true,
    };
    public fieldDataRatius: FormField = {
		label: 'Radio a considerar:',
		name: 'ratius',
		placeholder: '',
        options:[],
		multiple: false,
		required: true
    };
    public fieldDataProvince: FormField = {
		label: 'Selecciona provincia/s:',
		name: 'province',
		placeholder: '',
        options:[],
		multiple: true,
		required: true
    };
    public formGroup:FormGroup = new FormGroup({
        site: new FormControl(null, Validators.required),
        ratius: new FormControl(null, Validators.required),
        province: new FormControl(null, Validators.required)
    });

    public allProvinces:boolean = false;
    public allSites:boolean = false;

    constructor(
        private ws: WSService,
        private ustore: UserStore,
        private dialog: DialogService,
        private translate: TranslateService
    ) {
    }
    
    ngOnInit(): void {
        this.me = this.ustore.getUser();
        this.getProvinces()
    }
    private getProvinces(){
        this.ws.getProvincesOfCustomer().subscribe(result => {
            if(result.success){
                this.fieldDataProvince.options = [{value: 0, name: "Todas"}];
                result.data.map(province => {
                    this.fieldDataProvince.options.push({value: province.idprovince, name: province.name});
                })
            }
        })
    }
    public onChangeProvince(event){
        if(this.allProvinces){
            let selected = this.formGroup.get('province').value;
            let newValues = [];
            let foundCero:boolean = false;
            selected.map((s,i) => {
                if(s !== 0){
                    newValues.push(s);
                }else{
                    foundCero = true;
                }
            })
            if(!foundCero)
                this.formGroup.get('province').setValue(null)
            else
                this.formGroup.get('province').setValue(newValues);
            this.allProvinces = false;
        }else{
            const data = this.formGroup.get('province').value;
            data.map(p => {
                if(p === 0){
                    this.allProvinces = true;
                    let values:number[] = [];
                    this.fieldDataProvince.options.map(o => {
                        values.push(o.value);
                    });
                    this.formGroup.get('province').setValue(values);
                }
            })
        }
        let data = this.formGroup.get('province').value;
        this.getSites(data);
    }

    private getSites(data){
        if(data !== null)
            this.ws.getSitesByProvinces({province: data}).subscribe(result => {
                if(result.success){
                    this.fieldDataSite.options = [];
                    this.fieldDataSite.options.push({value: 0, name: "Todos"});
                    result.data.map(site => {
                        this.fieldDataSite.options.push({value: site.idsite, name: `${site.code} - ${site.name}`});
                    })
                }
            })
        else{
            this.fieldDataSite.options = [];
            this.formGroup.get('site').setValue(null);
            this.fieldDataRatius.options = [];
            this.formGroup.get('ratius').setValue(null);
        }
    }

    public onChangeSite(event){
        if(this.allSites){
            let selected = this.formGroup.get('site').value;
            let newValues = []
            let foundCero:boolean = false;
            selected.map((s,i) => {
                if(s !== 0){
                    newValues.push(s);
                }else{
                    foundCero = true;
                }
            })
            if(!foundCero)
                this.formGroup.get('site').setValue(null);
            else
                this.formGroup.get('site').setValue(newValues);
            this.allSites = false;
        }else{
            const data = this.formGroup.get('site').value;
            data.map(s => {
                if(s === 0){
                    this.allSites = true;
                    let values:number[] = [];
                    this.fieldDataSite.options.map(o => {
                        values.push(o.value);
                    });
                    this.formGroup.get('site').setValue(values);
                }
            })
        }
        let data = this.formGroup.get('site').value;
        this.getRatius(data);
    }
    
    private getRatius(data = null){
        if(data){
            this.ws.getRatiusOfSites({site: data}).subscribe(result => {
                if(result.success){
                    this.fieldDataRatius.options = [];
                    result.data.map(ratius => {
                        this.fieldDataRatius.options.push({value: ratius, name: ratius.toString()});
                    })
                }
            })
        }else{
            this.fieldDataRatius.options = [];
            this.formGroup.get('ratius').setValue(null);
        }
    }

    public submit(){
        const data = this.formGroup.getRawValue();
        this.submitForm.emit(data)
    }
}