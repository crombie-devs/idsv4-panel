import { Component } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { Site } from 'src/modules/site/models/site';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';

import {TranslateService} from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Component({
    selector: 'emissions-sociodemographic-admin',
    templateUrl: './view.page.html',
    styleUrls: ['./styles.page.scss']
  })

export class SociodemograficoAdminComponent {
    public me: any;
    public site: Site;
    public sites:Site[] = [];
    public idsites: number[] = [];
	public store: string = null;
    public totalDemographic:any;
    public selectedDemographic:any;
    public economicResult: any;
    public ratius:number;
    public tab: number = 0;
    public labelTab1 = this.translate.instant("Información Socioeconómica");
	public labelTab2 = this.translate.instant("Segmentación");
    public labelTab3 = this.translate.instant("Centros");
    public lastSegmentation:string = '';
    public loading:boolean = false;
    public loadAutocomplete:boolean = false;
    public lastResult:number[] = [];

    constructor(
		private ws: WSService,
        private ustore: UserStore,
        private dialog: DialogService,
        private _snackBar: MatSnackBar,
        private translate: TranslateService) {
    }
    
    ngOnInit(): void {
        this.me = this.ustore.getUser();
    }

    public submitSocioeconomic(data){
        this.ratius = data.ratius;
        this.ws.getSocialEconomicAdminData(data).subscribe(result => {
            if(result.success){
                this.totalDemographic = result.data.totalDemo;
                this.selectedDemographic = result.data.selectedDemo;
                this.economicResult = result.data.economic;
                this.totalDemographic.range = Object.values(result.data.totalDemo.range);
                this.selectedDemographic.H.range = Object.values(result.data.selectedDemo.H.range);
                this.selectedDemographic.M.range = Object.values(result.data.selectedDemo.M.range);
            }
        });
    }

    public submitSegmentacion(data){
        this.loading = true;
        if(data.demographic){
            if(this.idsites.length > 0 && this.lastSegmentation !== 'demographic'){
                data.sites = this.idsites;
            }else if(this.lastResult.length > 0){
                data.sites = this.lastResult;
            }
            this.ws.getDemographicSegmentation(data).subscribe(result => {
                if(result.success){
                    if(result.data.length > 0){
                        this.lastSegmentation = 'demographic';
                        this.sites.map(s => this.lastResult.push(s.idsite));
                        this.sites = [];
                        result.data.map(site => {
                            this.sites.push(site);
                            this.idsites.push(site.idsite);
                        });
                        this.loading = false;
                    }else{
                        this.loading = false;
                        this.openSnackBar("No hay centros con estas especificaciones");
                    }
                }
                this.loading = false;
            })
        }else if(data.economic){
            if(this.idsites.length > 0 && this.lastSegmentation !== 'economic'){
                data.sites = this.idsites;
            }else if(this.lastResult.length > 0){
                data.sites = this.lastResult;
            }
            this.ws.getEconomicSegmentation(data).subscribe(result => {
                if(result.success){
                    if(result.data.length > 0){
                        this.lastSegmentation = 'economic';
                        this.sites.map(s => this.lastResult.push(s.idsite));
                        this.sites = [];
                        result.data.map(site => {
                            this.sites.push(site);
                            this.idsites.push(site.idsite);
                        });
                        this.loading = false;
                    }else{
                        this.loading = false;
                        this.openSnackBar("No hay centros con estas especificaciones");
                    }
                }
                this.loading = false;
            })
        }
    }
    public changeTab(event:any){
		this.tab = event.index;
        if(event.index === 2){
            this.loadAutocomplete = true;
        }else{
            this.loadAutocomplete = false;
        }
	}
    public cleanSites(data){
        this.sites = data;
    }
    public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
}