import { EventEmitter } from '@angular/core';
import { Component, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { FormField } from 'src/modules/form-field/models/form-field';
import { WSService } from 'src/services/ws.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import { SegmentationStore } from 'src/modules/socio-economic/stores/segmentation.store';
import { Router } from '@angular/router';

@Component({
    providers: [SegmentationStore],
    selector: 'analisis-sociodemografico-admin-tab-segmentation-result',
    templateUrl: './view.page.html',
    styleUrls: ['./styles.page.scss']
  })

export class ResultTabSegmentationComponent {
    @Input() sites:any;
    @Input() center:any = {};
    @Output() onCleanSites:EventEmitter<any> = new EventEmitter<any>(null);

    constructor(
        private segStore:SegmentationStore,
        private router: Router,
    ) {
    }
    
    ngOnInit(): void {
    }

    public cleanSites(){
        this.sites = [];
        this.onCleanSites.emit(this.sites);
    }

    public sendSitesToNewCircuit(){
        this.segStore.set(this.sites);
        this.router.navigate(['/private/playcircuit/create']);
    }
    
}