import { EventEmitter } from '@angular/core';
import { Component, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { FormField } from 'src/modules/form-field/models/form-field';
import { WSService } from 'src/services/ws.service';
import { UserStore } from 'src/modules/user/stores/user.store';

@Component({
    selector: 'analisis-sociodemografico-admin-tab-socioeconomic-result',
    templateUrl: './view.page.html',
    styleUrls: ['./styles.page.scss']
  })

export class ResultTabSocioeconomicComponent {
    @Input() totalDemographic:any;
    @Input() selectedDemographic: any;
    @Input() economicResult:any;
    @Input() ratius: number;

    constructor(
    ) {
    }
    
    ngOnInit(): void {
    }
    
}