import { Component, Input, Output, EventEmitter, AfterViewInit, AfterViewChecked, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { partition } from 'rxjs';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { FormField } from 'src/modules/form-field/models/form-field';
import { WSService } from 'src/services/ws.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import { SiteStore } from 'src/modules/site/stores/site.store';
import { Site } from 'src/modules/site/models/site';
import { ExcelService } from 'src/services/excel.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'analisis-sociodemografico-admin-tab-sites',
    templateUrl: './view.page.html',
    styleUrls: ['./styles.page.scss']
  })

export class FormTabSitesComponent {
    @Input() loadautocomplete:boolean = false;
    @Input() loading:boolean = false;
    @Output() submitForm:EventEmitter<any> = new EventEmitter(null);
    @Output() onCleanSites:EventEmitter<any> = new EventEmitter(null);
    public loadingCoordinates:boolean = false;
    public location:any = {
		lat:40.4378698,
		lng:-3.8196207
	};
    public me: any;
    public allAgeToOptions:any[] = [];

    public fieldDataRatius: FormField = {
		label: this.translate.instant('Radio a considerar'),
		name: 'ratius',
		placeholder: '',
		required: true
    };
    public fieldDataAddress: FormField = {
		label: this.translate.instant('store_address'),
		name: 'address',
		placeholder: '',
		required: true
    };
    public formGroup:FormGroup = new FormGroup({
        ratius: new FormControl(null, Validators.required),
        address: new FormControl(null, Validators.required)
    });
    public dataTogetIncomes:any = {};
    public allProvinces:boolean = false;
    public sites:Site[] = [];
    public sitesToCompare:Site[] = [];

    constructor(
        private ustore: UserStore,
        private ws: WSService,
        private excelService:ExcelService,
        private sistore: SiteStore,
        private _snackBar: MatSnackBar,
        private translate: TranslateService
    ) {
        this.sistore.onChange.subscribe(result => {
            if(result) {
                this.sitesToCompare = result;
            }
        });
    }
    ngOnInit(): void {
        this.me = this.ustore.getUser();
    }  

    ngOnChanges(changes: SimpleChanges): void {
        //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
        //Add '${implements OnChanges}' to the class.
        if(changes.loadautocomplete){
            this.loadAutocomplete();
        }
    }
    loadAutocomplete(){
        const input = document.getElementById("autocomplete") as HTMLInputElement;
		const options = {
		  fields: ["place_id", "geometry", "name", "formatted_address", "address_components"],
		  strictBounds: false,
		  types: ["address"],
		};
		
		const autocomplete = new google.maps.places.Autocomplete(input, options);
		autocomplete.addListener("place_changed", () => {
			const place = autocomplete.getPlace();
			if (!place.place_id) {
			  return;
			}
			this.getCoordinatesBlur(place);
		  });
    }

    getCoordinatesBlur(place){
		this.loadingCoordinates = true;
		const geocoder = new google.maps.Geocoder();
		geocoder.geocode({placeId: place.place_id}, (result, status) => {
			if(status === 'OK'){
				const res = result[0];
				this.location.lat = res.geometry.location.lat();
				this.location.lng = res.geometry.location.lng();
			}
			this.loadingCoordinates = false;
		});
	}
    public cleanSites(data){
        this.onCleanSites.emit(data);
    }
    public exportToExcel(){
        let dataExcel:any[] = [];
        const titles = {
            idcentro: this.translate.instant('idcentro'),
            code: this.translate.instant('código de centro'),
            name: this.translate.instant('Nombre'),
            address: this.translate.instant('Domicilio'),
            cp: this.translate.instant('C.P.')
        }
		this.sites.map(site => {
            let data = {};
            data[titles.idcentro] = site.idsite;
            data[titles.code] = site.code;
            data[titles.name] = site.name;
            data[titles.address] = site.address;
            data[titles.cp] = site.zipcode;
			dataExcel.push(data);
		});
        const formData = this.formGroup.getRawValue();
        let name = `${this.me.customer.name}-`;
        name += `${formData.address}-`;
        name += `${formData.ratius}`;
		this.excelService.exportAsExcelFile(dataExcel, `${name}`);
    }

    public submitSites(){
        const data = this.formGroup.getRawValue();
        this.sites = [];
        this.sitesToCompare.map(site => {
            const point2 = {
                lat: site.latitude,
                lng: site.longitude
            }
            const distance = this.distance(this.location, point2);
            if(distance <= data.ratius){
                this.sites.push(site);
            }
        })
        if(this.sites.length === 0){
            this.openSnackBar(this.translate.instant('No se han encontrado centros dentro del radio seleccionado.'));
        }
    }

    distance(point1, point2) {
        var R = 6371000; // Radio de la tierra en metros
        var rlat1 = point1.lat * (Math.PI/180); // Convert degrees to radians
        var rlat2 = point2.lat * (Math.PI/180); // Convert degrees to radians
        var difflat = rlat2-rlat1; // Radian difference (latitudes)
        var difflon = (point2.lng-point1.lng) * (Math.PI/180); // Radian difference (longitudes)
  
        var d = 2 * R * Math.asin(Math.sqrt(Math.sin(difflat/2)*Math.sin(difflat/2)+Math.cos(rlat1)*Math.cos(rlat2)*Math.sin(difflon/2)*Math.sin(difflon/2)));
        return d;
    }

    public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
}