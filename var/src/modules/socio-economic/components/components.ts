import { SociodemograficoUserComponent } from './sociodemografico-user/sociodemografico-user.component';
import { SociodemograficoAdminComponent } from './sociodemografico-admin/sociodemografico-admin.component';
import { FormTabSocioeconomicComponent } from './sociodemografico-admin/form-tab-socioeconomic/form-tab-socioeconomic.component';
import { ResultTabSocioeconomicComponent } from './sociodemografico-admin/result-tab-socioeconomic/result-tab-socioeconomic.component';
import { FormTabSegmentacionComponent } from './sociodemografico-admin/form-tab-segmentacion/form-tab-segmentacion.component';
import { FormTabSitesComponent } from './sociodemografico-admin/form-tab-sites/form-tab-sites.component';
import { ResultTabSegmentationComponent } from './sociodemografico-admin/result-tab-segmentation/result-tab-segmentation.component';

export const SOCIOECONOMIC_COMPONENTS = [
    SociodemograficoUserComponent,
    SociodemograficoAdminComponent,
    FormTabSocioeconomicComponent,
    ResultTabSocioeconomicComponent,
    FormTabSegmentacionComponent,
    ResultTabSegmentationComponent,
    FormTabSitesComponent
];
