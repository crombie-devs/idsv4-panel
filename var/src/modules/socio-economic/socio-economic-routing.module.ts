import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SocioeconomicPage } from './pages/socioeconomic.page';

const routes: Routes = [
  { path: '', component: SocioeconomicPage},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class SocioEconomicRoutingModule { }
