import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IResult } from 'src/models/iresult';
import { MatGridListModule } from '@angular/material/grid-list';
import { UserStore } from 'src/modules/user/stores/user.store';

@Component({
  selector: 'emissions-socioeconomic-page',
  templateUrl: './view.page.html',
  styleUrls: ['./styles.page.scss']
})

export class SocioeconomicPage {
	
	public me: any;
	public store: string = null;
	public submitted: boolean = false;
	public data: any[];

  	constructor(
		private router: Router,
		private ustore: UserStore) {}

	ngOnInit() {
		this.me = this.ustore.getUser();
	}
}
