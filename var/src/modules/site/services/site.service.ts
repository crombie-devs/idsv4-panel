import { Component, Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { Site } from 'src/modules/site/models/site';
import { HttpService } from 'src/services/http.service';
import { ICrudWS } from 'src/interfaces/ICrudWS.interface';

@Injectable({
  providedIn: 'root',
})

export class SiteService implements ICrudWS<Site>{

  idsite: Subject<number> = new Subject();
  $idsite = this.idsite.asObservable();

  constructor(private http: HttpService) {}

  public create(obj: Site): Observable<Site> {
    return this.http.create('private/site', obj, 'WS.site_add');
  }

  public update(obj: Site): Observable<Site> {
    return this.http.update('private/site/'+obj.idsite, obj, 'WS.site_update');
  }

  public delete(id: number): Observable<Site> {
    return this.http.delete('private/site', [id], 'WS.site_delete');
  }

  public get(id: number): Observable<Site> {
    return this.http.get('private/site', [id], 'WS.site');
  }

  public toSelect(args?: any[]): Observable<Site[]> {
    return  this.http.get('private/sites/toselect', args, 'WS.site');
  }

  public list(args?: any[]): Observable<Site[]> {
    return  this.http.get('private/sites', args, 'WS.site');
  }

  public subjetIdSite(id: number): void {
    this.idsite.next(id);
  }

  public updateSiteTags(idsite:number,data:any): Observable<Site> {
    return this.http.update(`private/site/${idsite}/tags`, data, 'WS.updateSiteTags');
	}

	public updateSitePlaycircuits(idsite:number,data:any): Observable<Site> {
    return this.http.update(`private/site/${idsite}/playcircuits`, data, 'WS.updateSitePlaycircuits');
	}

	public updateSiteHolidays(idsite:number,data:any): Observable<Site> {
    return this.http.update(`private/site/${idsite}/holidays`, data, 'WS.updateSiteHolidays');
	}


    public changeStatus(id: number): Observable<boolean[]> {
        return  this.http.get('private/site/status', [id], 'WS.site_change_status');
      }
      

}


