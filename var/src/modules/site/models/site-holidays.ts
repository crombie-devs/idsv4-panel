export interface ISiteHoliday {
  idsite: number;
  date: Date
}

export class SiteHoliday implements ISiteHoliday {
  idsite: number;
  date: Date

  constructor(obj?: any) {
    if(!obj)
      return;
       
    this.idsite = obj.idsite;
    this.date = obj.date;
  }

 
}