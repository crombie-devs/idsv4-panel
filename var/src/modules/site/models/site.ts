import { Customer, ICustomer } from "src/modules/customer/models/customer";
import { ISiteHoliday, SiteHoliday } from "./site-holidays";
import { ITag, Tag } from 'src/modules/tag/models/tag';
import { IProvince, Province } from "src/modules/province/models/province";
import { ISiteCircuit, SiteCircuit } from './sites-circuits';
export interface ISite {
  idsite: number;
  name: string;
  email: string;
  phone: string;
  address: string;
  zipcode: string;
  code: string;
  latitude: string;
  longitude: string;
  customer: ICustomer;
  idcustomer: number;
  idcity: number;
  idprovince: number;
  idcountry: number;
  site_holidays: ISiteHoliday[];
  playcircuits: ISiteCircuit[];
  tags: ITag[];
  status: boolean;
  province?: IProvince;
  idcircuit?: number;
  has_socioeconomic: number;  
  license_expiration: string;
  closing_day: number;
  total_people_day: number;

}

export class Site implements ISite {
  idsite: number;
  name: string;
  email: string;
  phone: string;
  address: string;
  zipcode: string;
  code: string;
  latitude: string;
  longitude: string;
  customer: Customer;
  idcustomer: number;
  idcity: number;
  idprovince: number;
  idcountry: number;
  site_holidays: ISiteHoliday[] = [];
  playcircuits: ISiteCircuit[] = [];
  tags: ITag[] = [];
  status: boolean;
  province?: IProvince;
  idcircuit?: number;
  has_socioeconomic: number;
  license_expiration: string;
  closing_day: number;
  total_people_day: number;


	constructor(obj?: ISite) {
		if(!obj)
      return;

    this.idsite = obj.idsite;
    this.name = obj.name;
    this.email = obj.email;
    this.phone = obj.phone;
    this.address = obj.address;
    this.zipcode = obj.zipcode; 
    this.code = obj.code;
    this.latitude = obj.latitude; 
    this.longitude = obj.longitude; 
    this.customer = (obj.customer)? new Customer(obj.customer) : null;  
    this.idcustomer = obj.idcustomer;
    this.idcity = obj.idcity;
    this.idprovince = obj.idprovince;
    this.idcountry = obj.idcountry;
    this.total_people_day = obj.total_people_day;
    if(obj.tags)    
      this.tags = obj.tags.map(t => new Tag(t));
    if(obj.site_holidays)    
      this.site_holidays = obj.site_holidays.map(sh => new SiteHoliday(sh));
    if(obj.playcircuits)    
      this.playcircuits = obj.playcircuits.map(p => new SiteCircuit(p));
    this.status = obj.status;
    if(obj.province)
      this.province = new Province(obj.province); 
    if (obj.idcircuit)
      this.idcircuit = obj.idcircuit;
    this.has_socioeconomic = obj.has_socioeconomic;  
    this.license_expiration = obj.license_expiration; 
    this.closing_day = obj.closing_day; 

  }

  isEquals(site: ISite): boolean {
    return this.idsite === site.idsite;
  }
}
