import { City } from "src/modules/city/models/city";
import { Country } from "src/modules/country/models/country";
import { Customer } from "src/modules/customer/models/customer";
import { Lang } from "src/modules/lang/models/lang";
import {  PlayCircuit } from "src/modules/playcircuit/models/playcircuit";
import { Province } from "src/modules/province/models/province";
import { Tag } from "src/modules/tag/models/tag";


export interface ISiteCircuit extends PlayCircuit {
    idcircuit: number;
    name: string;
    image_url?: string;
    image?: File;
    status: boolean;
    deleted: boolean;
    idcustomer: number;
    customer: Customer;
    cities: City[];
    countries: Country[];
    provinces: Province[];
    sites: any[];
    tags: Tag[];
    langs: Lang[];
    is_selected: boolean;

}
export class SiteCircuit implements ISiteCircuit {

    idcircuit: number;
    name: string;
    image_url?: string;
    image?: File;
    status: boolean;
    deleted: boolean;
    idcustomer: number;
    customer: Customer;
    cities: City[] = [];
    countries: Country[] = [];
    provinces: Province[] = [];
    sites: any[];
    tags: Tag[];
    langs: Lang[];
    is_selected: boolean;
    constructor(obj){
        if(!obj)
        return;
        this.idcircuit = obj.idcircuit;
        this.name = obj.name;
        this.image_url = obj.image_url;
        this.image = obj.image;
        this.status = obj.status;
        this.deleted = obj.deleted;
        this.customer = new Customer(obj.customer);
        if(obj.cities)
            this.cities = obj.cities.map(c => new City(c));
        if(obj.countries)
            this.countries = obj.countries.map(c => new Country(c));
        if(obj.provinces)
            this.provinces = obj.provinces.map(p => new Province(p));
        if(obj.sites)
            this.sites = obj.sites;
        if(obj.tags)    
            this.tags = obj.tags.map(t => new Tag(t));
        if(obj.langs)    
            this.langs = obj.langs.map(l => new Lang(l));
        this.is_selected = obj.is_selected;
    }

    isEquals(circuit: ISiteCircuit) {
        return this.idcircuit === circuit.idcircuit;
    }

    citiesFree(){
        return this.cities;
    }

    provincesFree(): Province[]{
        return this.provinces.filter(p => this.cities.every(c => c.idprovince != p.idprovince));
    }

    countriesFree(): Country[]{
        return this.countries.filter(p => this.provinces.every(c => c.idcountry != p.idcountry));
    }

    checkSites(site: any){
        if (this.sites.some(s=> s.idsite === site.idsite))
            return true;
        if (this.citiesFree().some(c=> c.idcity === site.idcity))
            return true;
        if (this.provincesFree().some(p => p.idprovince === site.province.idprovince))
            return true;    
        if (this.countriesFree().some(c => c.idcountry === site.province.idcountry))
            return true; 

        return false;        
    }
        
    }

    

