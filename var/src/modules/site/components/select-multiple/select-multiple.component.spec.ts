import { TestBed, async, inject} from '@angular/core/testing';
import { SiteSelectMultipleComponent } from './select-multiple.component';
import { SiteService } from 'src/modules/site/services/site.service';
import { HttpService } from 'src/services/http.service';
import {  HttpClientModule } from '@angular/common/http';

describe('SelectSitesComponent', () => {
  let component: SiteSelectMultipleComponent;  
  inject([SiteService], (_site: SiteService) => {
    let site = _site;
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SiteSelectMultipleComponent
      ],
      providers: [
        SiteService,
        HttpService
      ],
      imports: [
        HttpClientModule,
      ]
    }).compileComponents();
    
  }));

  /*it('should be initialized', inject([SiteService], (_site: SiteService) => {
    expect(_site).toBeTruthy();
  }));*/
  
});
