import { Component, EventEmitter, Output, Input } from '@angular/core';
import { Site } from 'src/modules/site/models/site';
import { SiteService } from 'src/modules/site/services/site.service';
import { FormControl,  Validators , FormGroup} from '@angular/forms';
Validators
@Component({
  selector: 'select-sites',
  templateUrl: './select-multiple.component.html',
  styleUrls: ['./select-multiple.component.scss'],
})
export class SiteSelectMultipleComponent {
  @Output() onClick: EventEmitter<any> = new EventEmitter();
  @Output() onChange: EventEmitter<number> = new EventEmitter();
  @Input() idsite;
  @Input() isCascade: boolean;
  @Input() required: boolean;
  @Input() select: FormControl;  
  
  site: FormControl;
  form: FormGroup;

  sites: Site[] = [];
  sitesAll: Site[] = [];
  
  constructor(private _site: SiteService) {
    let validator = this.required ? Validators.required : null;
    this.form = new FormGroup({
      idsite: new FormControl('', validator)
    })
    this._site.toSelect().subscribe((items: Site[]) => {
      this.sites = items;
      this.sitesAll = [...this.sites];
      
      if(this.idsite) {
         if(this.idsite.idsite)
          {
             this.form.patchValue({idsite: this.idsite.idsite});
          }
      }
     
    });
  }

  onSelect(event): void {
    this.onClick.emit(event);
    this.onChange.emit(event.value);
    this._site.subjetIdSite(event.value);
  }

  public filterByCustomer(idcustomer: number, idsite?: number): void {
    this.isCascade = false;
    this.sites = this.sitesAll.filter((item) => item.idcustomer == idcustomer);
    if (idsite) {
      this.form.patchValue({ idsite: idsite });
    }
  }

}
