import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from "@angular/core";
import { MYString } from "src/libs/string.libs";
import { FormControl } from "@angular/forms";
import { Site } from "src/modules/site/models/site";
import { SiteStore } from "src/modules/site/stores/site.store";

@Component({
    providers: [ SiteStore ],
    selector: 'site-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.scss']
})

export class SiteSelectComponent implements OnInit {
    loading: boolean = true;
    sites: Site[] = [];
    sitesTemp: Site[] = []; 
    site: FormControl = new FormControl(null)
    @Input() selected: Site;
    @Input() filterByIds: number[] = [];
    @Input() apparence = 'fill';
    @Input() isUser: any = false;
    @Output() onChange: EventEmitter<Site> = new EventEmitter<Site>();

    constructor(private store: SiteStore) {}

    ngOnInit() {
        this.store.onChange.subscribe(result => {
            if(result) {
                this.sitesTemp = [...result];
                this.sites = (!this.filterByIds || this.filterByIds.length == 0)? result : result.filter(si => this.filterByIds.some(idsite => idsite === si.idsite));
                if(this.isUser) {
                    this.sites = this.sites.filter(s => s.idsite == this.isUser); 
                 
                }
                this.loading = false;
                      
            }
        });
    }


    setValue(id): void {
        this.site.setValue(id); 
    }

    ngOnChanges(changes: SimpleChanges): void {
        //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
        //Add '${implements OnChanges}' to the class.
        if(changes.selected && changes.selected.currentValue)
            this.site.setValue(changes.selected.currentValue.idsite);
    }

    onSelect(site:number){
        this.selected = this.sites.filter(s => s.idsite === site)[0];
        this.site.setValue(site);
        this.onChange.emit(this.selected);
    }
    

    checkSelect(site: Site){
        if(this.selected)
            return this.selected.isEquals(site);

        return false;
    }

    filterSite(id): void {
       this.sites = this.sitesTemp.filter( s => s.idcustomer == id);
    }

    public onKey(value) { 
        this.sites = this.search(value);
    }

    public search(value: string) { 
        let filter = MYString.removeAccents(value).toLowerCase();
        return this.sites.filter(s => MYString.removeAccents(s.idcustomer).toLowerCase().startsWith(filter));
    }
}
