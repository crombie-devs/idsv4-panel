import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { MapInfoWindow, MapMarker } from '@angular/google-maps';

import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';

import { SiteService } from 'src/modules/site/services/site.service';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { EventService } from 'src/shared/event.service';
import { CacheService, ICacheConfig } from 'src/services/cache.service';

import { DebugStore } from 'src/stores/debug.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { CountryStore } from 'src/modules/country/stores/country.store';
import { ProvinceStore } from 'src/modules/province/stores/province.store';
import { CityStore } from 'src/modules/city/stores/city.store';
import { PlaycircuitStore } from 'src/modules/playcircuit/stores/playcircuit.store';

import { Site } from 'src/modules/site/models/site';
import { Customer } from 'src/modules/customer/models/customer';
import { Country } from 'src/modules/country/models/country';
import { Province } from 'src/modules/province/models/province';
import { City } from 'src/modules/city/models/city';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';

import { HolidaysDialog } from 'src/modules/site/dialogs/holidays/holidays.dialog';

import {Observable} from 'rxjs';

@Component({
  providers: [CountryStore, ProvinceStore, CityStore, PlaycircuitStore],
  selector: 'site-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss']
})

export class SiteFormPage {
	@ViewChild(MapInfoWindow) infoWindow: MapInfoWindow;

	public form: FormGroup;
	public name: FormControl = new FormControl('', [Validators.required]);
	public email: FormControl = new FormControl();
	public address: FormControl = new FormControl('');
	public zipcode: FormControl = new FormControl('');
	public code: FormControl = new FormControl('');
	public latitude: FormControl = new FormControl('');
	public longitude: FormControl = new FormControl('');
	public idcustomer: FormControl = new FormControl('');
	public tags: FormControl = new FormControl([]);
	public holidays: FormControl = new FormControl([]);
	public country = new FormControl('', [Validators.required]);
	public idcity = new FormControl('', [Validators.required]);
	public idcircuit = new FormControl();
	public province = new FormControl('', [Validators.required]);
	public idsite = new FormControl(null);
	public license_expiration = new FormControl(new Date(new Date().setFullYear(new Date().getFullYear()+1)));
	public closing_day = new FormControl();
	public status: FormControl = new FormControl(true);
	public total_people_day: FormControl = new FormControl(null);
	public location:any = {
		lat:40.4378698,
		lng:-3.8196207
	};

	public site: Site;
	
	public sites: Site[];
	public countries: Country[] = [];
    public provinces: Province[] = [];
	public cities: City[] = [];
	public customers: Customer[];
	public circuits: PlayCircuit[] = [];
	public circuitsSelected: PlayCircuit[] = [];

	public loading: boolean = false;
	public customersToSelect: boolean = false;	
	public provinceAvailable:boolean = false;
    public localityAvailable:boolean = false;
	public selectedAddress: boolean = false;
	public loadingCoordinates: boolean = false;

	public id: number|null;
	public customerSelect: number = null;

	public center: any;

	public display?: google.maps.LatLngLiteral;

	public markerOptions = {draggable: false};
	public markerPositions: google.maps.LatLngLiteral[] = [];

	public mapOptions:string[] = [];
	public filteredMapOptions: Observable<string[]>;
	public mapPredictions:any[] = [];
	
	public me: any;	
	public hide: boolean = true;
	public isSuperAdmin: boolean = false;
	public weekDays: object[] = [
		{id: 1, name: 'Lunes'},
		{id: 2, name: 'Martes'},
		{id: 3, name: 'Miércoles'},
		{id: 4, name: 'Jueves'},
		{id: 5, name: 'Viernes'},
		{id: 6, name: 'Sábado'},
		{id: 7, name: 'Domingo'}
	];
	
   	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private ws: SiteService,
		private dialog: DialogService,
		private events: EventService,
		private eventc: CacheService,
		private debug: DebugStore,
		private ustore: UserStore,
		private countryStore: CountryStore,
        private provinceStore: ProvinceStore,
        private cityStore: CityStore,
		private circuitStore: PlaycircuitStore,		
		private fb: FormBuilder,
		private _snackBar: MatSnackBar,		
		private matDialog: MatDialog		
		) {
			this.id = Number(this.route.snapshot.paramMap.get('idsite'));
			this.ustore.onChange.subscribe(user => { 
				this.me = user; 
				(this.me.customer)?this.hide = false:this.hide = true;
				(this.me.customer)?this.isSuperAdmin = false:this.isSuperAdmin = true;
			});
			this.circuitStore.onChange.subscribe(circuits => { 
				this.circuits = circuits; 				
			});
	  }
  
  	public ngOnInit() {		
		this.debug.addInfo('SiteFormComponent', 'OnInit');
		const input = document.getElementById("autocomplete") as HTMLInputElement;
		const options = {
		  fields: ["place_id", "geometry", "name", "formatted_address", "address_components"],
		  strictBounds: false,
		  types: ["address"],
		};
		
		const autocomplete = new google.maps.places.Autocomplete(input, options);
		autocomplete.addListener("place_changed", () => {
			const place = autocomplete.getPlace();
			if (!place.place_id) {
			  return;
			}
			this.getCoordinatesBlur(place);
		}); 

		this.verifyPermissions();
		this.countryStore.onChange.subscribe( result => {
			if(result)
				this.countries = result;
		});
		this.initForm(null);	
		if (this.id){
			this.hide = false;
			this.provinceAvailable = true;
			this.localityAvailable = true;
			this.ws.get(this.id).subscribe((site: Site) => {
				this.site = new Site(site);		
				this.initForm(site);	
				this.location = {lat: this.site.latitude, lng: this.site.longitude};
			}, error => {
				this.debug.addError('WS.getSite', error.message);
			}); 
		}else{
			this.site = new Site();
			let c = (this.me.customer)?this.me.customer.idcustomer:null;
			this.idcustomer.setValue(c);
		}
	}  


  private initForm(data: Site | null) {
	let  date = new Date();
	date.setFullYear(date.getUTCFullYear()+1);
    if (data) {
      let c = data.customer.idcustomer;
      this.customerSelect = c;
      let value = [];
      data.playcircuits.map((c) => {
        value.push(c.idcircuit);
      });
      let status = data.status ? 1 : 0;
      this.getProvinces(data.idcountry, data.idprovince);
      this.getCities(data.idprovince, data.idcity);
      this.name.setValue(data.name);
      this.email.setValue(data.email);
      this.address.setValue(data.address);
      this.zipcode.setValue(data.zipcode);
      this.code.setValue(data.code);
      this.latitude.setValue(data.latitude);
      this.longitude.setValue(data.longitude);
      this.idcustomer.setValue(c);
      this.tags.setValue(data.tags);
      this.idcity.setValue(data.idcity);
      this.country.setValue(data.idcountry),
      this.province.setValue(data.idprovince);
      this.status.setValue(status);
      this.idcircuit.setValue(value);
	  this.total_people_day.setValue(data.total_people_day);
	  this.license_expiration.setValue(data.license_expiration);
	  this.closing_day.setValue(data.closing_day);
	  this.idsite.setValue(data.idsite);
    } else {
      this.form = this.fb.group({
        name: this.name,    
        email: this.email,
        address: this.address,
        zipcode: this.zipcode,
        code: this.code,
        latitude: this.latitude,
        longitude: this.longitude,
		total_people_day: this.total_people_day,
        idcustomer: this.me.customer
          ? this.me.customer.idcustomer
          : this.idcustomer,
        tags: this.tags,
        holidays: this.holidays,
        idcity: this.idcity,
        idcircuit: this.idcircuit,
        country: this.country,
        province: this.province,
        status: this.status,
		license_expiration: this.license_expiration? this.license_expiration: date,
		closing_day: this.closing_day,
		idsite: this.idsite
      });
    }
  }

  private getCountries(select: number = null) {
    this.countries = this.countryStore.get();
    if (select) this.form.get('country').patchValue(select);
  }

  private getProvinces(country: number = null, select: number = null) {
    let provinces = this.provinceStore.get();
    if (country)
      this.provinceStore.onChange.subscribe((result) => {
        if (result) {
          this.provinces = result.filter((p) => p.idcountry === country);
        }
      });
    if (select) this.form.get('province').patchValue(select);
  }

  private getCities(province, select: number = null) {
    let cities = this.cityStore.get();
    if (province)
      this.cityStore.onChange.subscribe((result) => {
        if (result) {
          this.cities = result.filter((p) => p.idprovince === province);
        }
      });
	 
    if (select) this.form.get('idcity').patchValue(select);
  }

  public selectionCountries() {
    this.provinceAvailable = true;
    this.provinces = [];
    this.getProvinces(this.form.get('country').value);
  }

  public selectionProvinces() {
    this.localityAvailable = true;
    this.cities = [];
    this.getCities(this.form.get('province').value);
  }

 

	public create(){
		if (this.form.invalid) return null;
		this.loading = true;
		let data = this.form.value;

    	this.ws.create(data).subscribe((site: Site) => {
			const id = site.idsite;
			this.openSnackBar("Centro creado correctamente");
			this.updateSiteHolidays(id);
			this.updateSiteTags(id);
			let config: ICacheConfig = {idsite: id, idcustomer: site.idcustomer};
			setTimeout(() => {
				this.events.publish('site:change', config);        
			}, 1000);
			this.router.navigateByUrl('/private/site/list');
			this.loading = false;
		}, error => {
			let msj = JSON.stringify(error.error.data);
			this.openSnackBar(msj.substr(2,msj.length-3)); 
			this.loading = false;          
		});
		   
	}

	public update(){
		if (this.form.invalid) return null;
		this.loading = true;
		this.ws.update(this.form.value).subscribe((site: Site) => {
			this.debug.addInfo('WS.updateSite', 'successful');
			this.openSnackBar("Centro actualizado correctamente");
			this.updateSiteHolidays(site.idsite);
			this.updateSiteTags(site.idsite);
			let config: ICacheConfig = {idsite: site.idsite, idcustomer: site.idcustomer};
			setTimeout(() => {
				this.events.publish('site:change', config);       
			}, 1000);				
			this.router.navigateByUrl('/private/site/list');
			this.loading = false;						
		}, error => {
			let msj = JSON.stringify(error.error.data);
			this.openSnackBar(msj.substr(2,msj.length-3));
			this.debug.addError('WS.updateSite', error.message); 
			this.loading = false;          
		}); 
	}

	public updateSiteTags(idsite: number){
		this.ws.updateSiteTags(idsite, {"tags": this.site.tags}).subscribe((result: Site) => {
		}, error => { 
			this.openSnackBar(error.message);
		}); 
	}	

	public updatePlaycircuitSites(idsite: number){
		this.ws.updateSitePlaycircuits(idsite, {"playcircuits": this.circuitsSelected}).subscribe((result: Site) => {
		}, error => {  
			this.openSnackBar(error.message);
		});  
	}	

	public updateSiteHolidays(idsite: number){
		this.ws.updateSiteHolidays(idsite, {"site_holidays": this.site.site_holidays}).subscribe((result: Site) => {
		}, error => { 
			this.openSnackBar(error.message);
		}); 
	}	

	public openDialogHolidays(site: Site) {
		const dialogRef = this.matDialog.open(HolidaysDialog, {data: site});		
		dialogRef.afterClosed().subscribe((result: Site|null) => {
			this.site.site_holidays = result.site_holidays;
			this.holidays.patchValue(result.site_holidays);		
		});
	} 

    public openDialogTags() {
		this.dialog.tagcategoryselect("Tags", this.site.tags,this.idcustomer.value).afterClosed().subscribe((result) => {
			if(!result.cancel){
				this.site.tags = result.tags;
				this.tags.patchValue(result.tags);
			}
		});
	} 

	public customerSelected(idcustomer:number) {
		(idcustomer==0)?this.hide = true:this.hide = false;
    	this.idcustomer.patchValue(idcustomer);			
	  }
	  
	private verifyPermissions() {		
		this.customersToSelect = this.ustore.hasPermissionByName("get.customers.toselect");
	}

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

	public blurLocations(){
		if(this.form.get('latitude').value && this.form.get('longitude').value){
			this.location = {lat: this.form.get('latitude').value, lng:  this.form.get('latitude').value};
		}
	}

	addMarker(event: google.maps.MouseEvent) {
		this.markerPositions.push(event.latLng.toJSON());
	  }
	
	move(event: google.maps.MouseEvent) {
		this.display = event.latLng.toJSON();
	}
	
    openInfoWindow(marker: MapMarker) {
		this.infoWindow.open(marker);
	}
	
	removeLastMarker() {
		this.markerPositions.pop();
	}

	getCoordinatesBlur(place){
		this.loadingCoordinates = true;
		const geocoder = new google.maps.Geocoder();
		geocoder.geocode({placeId: place.place_id}, (result, status) => {
			if(status === 'OK'){
				let city:City[] = [];
				let province:Province[] = [];
				let country:Country[] = [];
				const res = result[0];
				res.address_components.map(add =>{
					if(city.length === 0)
						city = this.cityStore.get().filter(c => c.name === add.long_name);
					if(province.length === 0)
						province = this.provinceStore.get().filter(p => p.name === add.long_name);
					if(country.length === 0)
						country = this.countryStore.get().filter(c => c.name === add.long_name);
				})
				if(city.length > 0 && province.length > 0 && country.length > 0){
					this.getCountries(country[0].idcountry);
					this.getProvinces(country[0].idcountry, province[0].idprovince);
					this.getCities(province[0].idprovince, city[0].idcity);
				}else if(city.length > 0){
					province = this.provinceStore.get().filter(p => p.idprovince === city[0].idprovince);
					if(province.length > 0){
						if(country.length === 0)
							country = this.countryStore.get().filter(c => c.idcountry === province[0].idcountry);
						this.getCountries(country[0].idcountry);
						this.getProvinces(country[0].idcountry, province[0].idprovince);
						this.getCities(province[0].idprovince, city[0].idcity);
					}
				}else if(province.length > 0){
					if(country.length === 0)
						country = this.countryStore.get().filter(c => c.idcountry === province[0].idcountry);
					this.getCountries(country[0].idcountry);
					this.getProvinces(country[0].idcountry, province[0].idprovince);
				}else if(country.length > 0){
					this.getCountries(country[0].idcountry);
				}

             

				const cp = res.address_components[res.address_components.length - 1].long_name;
                if (Number.isInteger(cp)) {
					this.form.get('zipcode').patchValue(cp);
				  }
               
				this.location.lat = res.geometry.location.lat();
				this.location.lng = res.geometry.location.lng();
				
				this.form.get('latitude').patchValue(this.location.lat);
				this.form.get('longitude').patchValue(this.location.lng);
				let address = res.address_components.length > 6 ? `${res.address_components[1].long_name}, ${res.address_components[0].long_name}` : res.address_components[0].long_name;
				this.form.get('address').patchValue(address);
			}
			this.localityAvailable = true;
			this.provinceAvailable = true;
			this.loadingCoordinates = false;
		});
	}

	public selCircuits(event){
		this.circuitsSelected = event.value;
	}
	
}
