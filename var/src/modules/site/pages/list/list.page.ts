import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';

import { MatPaginator } from '@angular/material/paginator';
import { Sort, MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SelectionModel } from '@angular/cdk/collections';

import { MYString } from 'src/libs/string.libs';

import { SiteService } from 'src/modules/site/services/site.service';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { EventService } from 'src/shared/event.service';

import { DebugStore } from 'src/stores/debug.store';
import { UserStore } from 'src/modules/user/stores/user.store';

import { Site } from 'src/modules/site/models/site';
import { CacheService, ICacheConfig } from 'src/services/cache.service';
import { MatCheckbox } from '@angular/material/checkbox';
import { User } from 'src/modules/user/models/user';
import { MatInput } from '@angular/material/input';
import { from } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'site-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss']
})

export class SiteListPage {

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
    @ViewChildren('table') table: QueryList<MatCheckbox>
    @ViewChild(MatInput) input: MatInput;
  
	public displayedColumns: string[] = ['select','idsite', 'name', 'email', 'address', 'zipcode', 'latitude', 'longitude', 'customer', 'status', 'socioeconomic'];
	public selection = new SelectionModel<Site>(true, []);
	public dataSource = new MatTableDataSource<Site>([]);
	public new: boolean = false;
	public edit: boolean = false;
	public deleted: boolean = false;	
	public permission: boolean = false;
	public load: boolean = true;

	private sortedData: Site[];
	private sites: Site[];
    public me: User;
    private data: Site[];

  	constructor(
		private router: Router,	
		private ws: SiteService,
		private events: EventService,
		private dialog: DialogService,	
		private debug: DebugStore,
		private ustore: UserStore,
		private snackBar: MatSnackBar,
        private translate: TranslateService		
		) {
            this.me = this.ustore.getUser();
        }
  
  	public ngOnInit() {
		this.debug.addInfo('SiteListComponent', 'OnInit');		
		this.verifyPermissions();	
		this.getSites();
	}

    public changeStatus(event) {
        this.ws.changeStatus(event).subscribe( item => {
			let config: ICacheConfig = {idsite: event, idcustomer: this.me.customer.idcustomer};
			setTimeout(() => {
				this.events.publish('site:change', config);        
			}, 1000);
        });
   }


	private getSites(){
		this.debug.addInfo('SiteListComponent', 'getSites');	
		this.ws.list().subscribe((sites: Site[]) => {
			this.permission = true;		
            this.data = sites;		
			this.renderDataTable(sites);
			this.load = false;
		}, error => {
			this.load = false;
			return null;          
		});	
	}
	  
	public renderDataTable(data: Site[]) {
	    this.dataSource = new MatTableDataSource(data);
		this.dataSource.filterPredicate = (data: Site, filter: string): boolean => {
			const dataStr = MYString.removeAccents(Object.keys(data).reduce((currentTerm: string, key: string) => {
			  return (currentTerm + (data as { [key: string]: any })[key] + '◬');
			}, '')).trim().toLowerCase();	  
			return dataStr.indexOf(MYString.removeAccents(filter.trim().toLowerCase())) != -1;
		};

        setTimeout(() => {
	      this.dataSource.paginator = this.paginator;
		}, 500);

        if(this.input)
            this.applyFilter(this.input.value);

		this.sortedData = data.slice();
		this.sites = data;
		this.dataSource.sort = this.sort;
		this.load = false;
	}

	isAllSelected() {
		if(this.dataSource.paginator){
			const numSelected = this.selection.selected.length;
			const page = this.dataSource.paginator.pageSize;
			let endIndex: number;
			if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
			endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
			} else {
			endIndex = this.dataSource.data.length - (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize);
			}
			return numSelected === endIndex;
		}
      }

    masterToggle() {
        this.isAllSelected() ?
          this.selection.clear() : this.selectRows();
    }

    selectRows() {
        let endIndex: number;
        if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
          endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
        } else {
          endIndex = this.dataSource.data.length;
        }        
        for (let index = (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize); index < endIndex; index++) {
          this.selection.select(this.dataSource.data[index]);
        }
    }

	public checkboxLabel(row?: Site): string {
		if (!row) {
			return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
		}
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idsite + 1}`;
	}

	public delete(){
        let sitesByDelete = [];
		this.table.toArray().forEach(item => {
			if(item.checked) 
				sitesByDelete.push(item.value);			
	    });	
		if(sitesByDelete.length == 0){
			this.dialog.warning(this.translate.instant('Aviso'), this.translate.instant('Debe seleccionar al menos un centro')).subscribe();
		}else{
			this.dialog.confirm(this.translate.instant('Confirmar Archivado'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
				if (confirm){
                    this.deleteByIds(sitesByDelete);   
				}
			});
		}		
	}

	public update(){
		if (this.selection.selected.length>1){
			this.dialog.warning('Aviso', 'Sólo puede editar un centro').subscribe();
		}else if(this.selection.selected.length == 0){
			this.dialog.warning('Aviso', 'Debe seleccionar un centro').subscribe();
		}else{
			this.router.navigateByUrl('/private/site/edit/'+this.selection.selected[0].idsite);
		}
	}

	public openSnackBar(message: string) {
		this.snackBar.open(message,'', {
		  duration: 1500,
		});
	}	
	
	public applyFilter(event: string) {
        this.selection.clear();
        if(!event) 
            this.dataSource.data = this.data;	
        else{    
            if(typeof event == 'string'){
                this.dataSource.data = this.data.filter(site => 
                    site.name.toLowerCase().includes(event.trim().toLowerCase()) || 
                    site.idsite.toString().includes(event.trim()) || 
                    (site.phone && site.phone.toString().includes(event.trim())) || 
                    (site.email && site.email.toString().includes(event.trim())) || 
                    (site.address && site.address.toString().includes(event.trim())) || 
                    (site.code && site.code.toString().includes(event.trim())) || 
                    (site.latitude && site.latitude.toString().includes(event.trim())) ||
                    (site.longitude && site.longitude.toString().includes(event.trim()))
                );
            
            }
        }
	}
	
	private verifyPermissions() {
		this.debug.addInfo('SiteListComponent', 'verifyPermissions');
		this.new = this.ustore.hasPermissionByName("post.site");
		this.edit = this.ustore.hasPermissionByName("put.site.*");
		this.deleted = this.ustore.hasPermissionByName("delete.site.*");		
	}

	ngAfterViewInit (){
		setTimeout(() => {
			this.dataSource.paginator = this.paginator;
		  }, 500) 

		this.dataSource.sort = this.sort;
		
	}

	sortData(sort: Sort) {
		const data = this.dataSource.data.slice();
		if (!sort.active || sort.direction === '') {
		  this.sortedData = data;
		  return;
		}
	
		this.sortedData = data.sort((a, b) => {
		  const isAsc = sort.direction === 'asc';
		  switch (sort.active) {
			case 'idsite': return this.compare(a.idsite, b.idsite, isAsc);
			case 'name': return this.compare(a.name? a.name:'', b.name? b.name:'', isAsc);
			case 'email': return this.compare(a.email? a.email:'', b.email? b.email:'', isAsc);
			case 'phone': return this.compare(a.phone? a.phone:'', b.phone? b.phone:'', isAsc);
			case 'address': return this.compare(a.address? a.address:'', b.address? b.address:'', isAsc);
			case 'zipcode': return this.compare(a.zipcode? a.zipcode:'', b.zipcode? b.zipcode:'', isAsc);
			case 'latitude': return this.compare(a.latitude? a.latitude:'', b.latitude? b.latitude:'', isAsc);
			case 'longitude': return this.compare(a.longitude? a.longitude:'', b.longitude? b.longitude:'', isAsc);
			case 'customer': return this.compare(a.customer? a.customer.name:'', b.customer? b.customer.name:'', isAsc);
			default: return 0;
		  }
		});
		this.dataSource.data = this.sortedData;
	}

	compare(a: number | string, b: number | string, isAsc: boolean) {
		return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
	}

    private deleteByIds(ids: number[]) {
        from(ids).pipe(
          concatMap(id => this.ws.delete(id))
        ).subscribe(site => {
            this.load = true;
            let config: ICacheConfig = {idsite: site.idsite, idcustomer: site.idcustomer};
            setTimeout(() => {              
                this.events.publish('site:change', config);
            }, 1000);  
          }, error => {
            this.openSnackBar(error.message);
            this.getSites();
          },
          () => {                        
            this.openSnackBar('Los centros seleccionados han sido archivados correctamente');                       
            this.selection.clear();
            this.getSites();
          }); 
    }
		
}