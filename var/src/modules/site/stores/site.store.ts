import { Injectable } from "@angular/core";

import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from "src/environments/environment";

import { IResult } from 'src/models/iresult';
import { ISite, Site } from "src/modules/site/models/site";

import { SiteService } from "src/modules/site/services/site.service";

const STORE_KEY: string = 'sites.store.ids';

@Injectable()
export class SiteStore {

    private $onChange: BehaviorSubject<Site[]> = new BehaviorSubject([]);
    public onChange: Observable<Site[]> = this.$onChange.asObservable();

    private data: Site[] = [];

    constructor(private ws: SiteService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const items: ISite[] = JSON.parse(store); 
                this.set(items);
            } else 
                this.load();
        } else
            this.load();
    }

    public get(): Site[] {
        return this.data;
    }

    public set(items: ISite[]) {
        this.save(items);
        this.$onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.toSelect().subscribe((sites: Site[]) => {
            this.set(sites);
        });
    }

    private save(items: ISite[]) {
        this.data = items.map(a => new Site(a));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(items));
    }

    public filterCustomer(idcustomer: number){
        return this.data.filter(s => s.idcustomer === idcustomer);
    }

    private filterByCity (idcity : number){
        return this.data.filter(s=> s.idcity === idcity);
    }

    private filterByProvince (idprovince : number){
        return this.data.filter(s=> s.province.idprovince === idprovince);
    }

    private filterByCountry (idcountry : number){
        return this.data.filter(s=> s.province.idcountry === idcountry);
    }

}