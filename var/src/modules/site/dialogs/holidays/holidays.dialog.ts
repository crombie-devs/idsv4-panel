import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { Site } from 'src/modules/site/models/site';
import { SiteHoliday } from 'src/modules/site/models/site-holidays';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import moment from 'moment';

@Component({
	selector: 'holidays-dialog',
  	templateUrl: './holidays.dialog.html',
  	styleUrls: ['./holidays.dialog.css']
})

export class HolidaysDialog {

    public title: string = 'Editar Vacaciones';
    public site: Site;    
    public holidays: SiteHoliday[] = [];
    public date = new FormControl();
    

	constructor(
    public dialogRef: MatDialogRef<HolidaysDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Site) 
    {    
        this.site = new Site(this.data);
    }
  
    public ngOnInit() {
        this.initLocationValues();
    }

    initLocationValues(){
        if(this.site.site_holidays.length > 0)
            this.holidays = this.site.site_holidays.map(h => new SiteHoliday(h));                 
          
    }
    public save() {
        this.site.site_holidays = this.holidays; 
        this.dialogRef.close(this.site);
    }

    public close() {
        this.dialogRef.close(null);
    }
    
    public dateChange(event: MatDatepickerInputEvent<Date>){
        const holiday = new SiteHoliday({"idsite": this.site.idsite, "date": moment(event.value).format('YYYY-MM-DD')})
        this.holidays.push(holiday);
        this.date  = new FormControl();

    }

    public delete(i: number){
        this.holidays.splice(i, 1);
    }
}
