
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SiteFormPage } from './pages/form/form.page';
import { SiteListPage } from './pages/list/list.page';

const routes: Routes = [
  { path: 'list', component: SiteListPage, data: { breadcrumb: 'Centros' } }, 
  { path: 'create', component: SiteFormPage, data: { breadcrumb: 'Nuevo centro' } },
  { path: 'edit/:idsite', component: SiteFormPage, data: { breadcrumb: 'Editar centro' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})


export class SiteRoutingModule { }
