import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';


import { DirectivesModule } from 'src/directive/directives.module';
import { MaterialModule } from 'src/app/material.module';
import { SiteRoutingModule } from './site-routing.module';
import { SiteSelectMultipleComponent } from './components/select-multiple/select-multiple.component';
import { SiteSelectComponent } from './components/select/select.component';
import { SiteStore } from './stores/site.store';

import { HolidaysDialog } from './dialogs/holidays/holidays.dialog';

import { SiteFormPage } from './pages/form/form.page';
import { SiteListPage } from './pages/list/list.page';
import { CustomerModule } from 'src/modules/customer/customer.module';
//google maps
import { AgmCoreModule } from '@agm/core';
import { GoogleMapsModule } from '@angular/google-maps';
import { CONFIG } from 'src/app/app.config';

@NgModule({
  declarations: [SiteSelectMultipleComponent, SiteSelectComponent, SiteFormPage, SiteListPage, HolidaysDialog],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    SiteRoutingModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule,
    DirectivesModule,
    CustomerModule,
    AgmCoreModule.forRoot({ apiKey: CONFIG.gmaps_apikey }),
    GoogleMapsModule,
  ],
  entryComponents:[HolidaysDialog],
  providers: [SiteStore],
  exports: [SiteSelectMultipleComponent, SiteSelectComponent]

})
export class SiteModule {}
