import { TagCategory } from './tagcategory';

export interface ITag {
  idtag: number;
  name: string;
  idcategory?:number;
  color?:string;
  tag_category?: TagCategory;
}

export class Tag implements ITag {
  idtag: number;
  name: string;
  idcategory?:number;
  color?:string;
  tag_category?: TagCategory;

	constructor(obj?: any) {
		if(!obj)
      return;

    this.idtag = obj.idtag;
    this.name = obj.name;  
    this.idcategory = obj.idcategory;
    this.color = obj.color;
    this.tag_category = obj.tag_category;
  }
}
