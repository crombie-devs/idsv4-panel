import { Tag } from 'src/modules/tag/models/tag';


export const TAGTYPE = { personalizacion: 'P', difusion: 'D', mensaje: 'M', mixto: ['D', 'P', 'M'] };
export class TagCategory {
    idcategory: number;
    idcustomer: number;
    name: string;
    color: string;
    tags: Tag[];
    tagtype: string;

    constructor(obj?: any) {
        if (!obj)
            return;

        this.idcategory = obj.idcategory;
        this.name = obj.name;
        this.tagtype = obj.tagtype;
        this.color = obj.color;
    }
}