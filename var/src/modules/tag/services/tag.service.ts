import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpService } from 'src/services/http.service';
import { ICrudWS } from 'src/interfaces/ICrudWS.interface';
import { ITag, Tag } from 'src/modules/tag/models/tag';

@Injectable()
export class TagService implements ICrudWS<Tag>{
  constructor(private http: HttpService) {}

  public create(obj: Tag): Observable<Tag> {
    return this.http.createWithFiles('private/tage', obj, 'WS.tag_add');
  }

  public update(obj: ITag): Observable<Tag> {
    return this.http.updateWithFiles('private/tag', obj, 'WS.tag_update');
  }

  public delete(id: number): Observable<boolean> {
    return this.http.delete('private/tag', [id], 'WS.tag_delete');
  }

  public get(id: number): Observable<Tag> {
    return this.http.get('private/tag', [id], 'WS.tag');
  }

  public toSelect(args?: any[]): Observable<Tag[]> {
    return  this.http.get('private/tags/toselect', args, 'WS.tag');
  }

  public list(args?: any[]): Observable<Tag[]> {
    return  this.http.get('private/tags', args, 'WS.site');
  }

  public byType(args?: any[]): Observable<Tag[]> {
   
    return  this.http.get('private/tags/bytype', args, 'WS.tag');
  }





}
