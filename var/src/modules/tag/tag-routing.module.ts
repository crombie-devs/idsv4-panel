import { NgModule } from '@angular/core';
import { _MatTabLinkBase } from '@angular/material/tabs';
import { Routes, RouterModule } from '@angular/router';
import { TagListPage } from './pages/list/list.page';
import { TagFormPage } from './pages/form/form.page';
const routes: Routes = [
  { path: 'list', component: TagListPage, data: { breadcrumb: 'Etiquetas' }},
  { path: 'create', component: TagFormPage, data: { breadcrumb: 'Categorias de etiquetas' }},
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TagRoutingModule { }
