import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TagCategory } from 'src/modules/tag/models/tagcategory';

@Component({
  selector: 'tag-categoryselectitem',
  templateUrl: './categoryselectitem.component.html',
  styleUrls: ['./categoryselectitem.component.scss']
})
export class TagCategorySelectItemComponent {
  
  @Input() item:TagCategory|any;

  constructor() { 
    
  }

  ngOnInit(): void {
  }

}
