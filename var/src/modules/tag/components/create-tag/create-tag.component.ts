import { Component, Output, EventEmitter, Input, ViewEncapsulation } from "@angular/core";
import { FormGroup } from '@angular/forms';



@Component({
    selector: "create-tag",
    templateUrl: "./create-tag.component.html",
    styleUrls: ['./create-tag.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class CreateTagComponent {
    @Output() onCreateTag: EventEmitter<any> = new EventEmitter();
    @Output() onUpdateTag: EventEmitter<any> = new EventEmitter();
    @Output() onEditTag: EventEmitter<any> = new EventEmitter();
    @Output() onDeleteTag: EventEmitter<any> = new EventEmitter();
    @Output() onDeleteTagCategory: EventEmitter<any> = new EventEmitter();
    @Output() onUpdateTagCategory: EventEmitter<any> = new EventEmitter();

    @Input() tagsByCategory: any;
    @Input() color: any;
    @Input() categoryTagTypes: any[] = [];
    @Input() me: any;
    @Input() index;
    @Input() type;
    @Input() newTagPermission = false;
    @Input() editTagPermission = false;
    @Input() deletedTagPermission = false;
    @Input() deletedTagCategoryPermission = false;
    @Input() editTagCategoryPermission = false;
    newForm: FormGroup;



    createTag(e, v) {
        this.onCreateTag.emit({ e, v });
    }

    updateTag(e) {
        this.onUpdateTag.emit(e);
    }

    editTag(e) {
        this.onEditTag.emit(e);
    }

    deleteTag(e) {
        this.onDeleteTag.emit(e);
    }

    deleteTagCategory(e) {
        this.onDeleteTagCategory.emit(e);
    }

    updateTagCategory(e) {
        this.onUpdateTagCategory.emit(e);
    }


    public changeCategoryTagType(value, category_id) {
        this.categoryTagTypes.map(t => {
            if (t.c_id === category_id)
                t.c_type = value;
        });
    }

    colorChanged(data, idcategory) {
        this.categoryTagTypes.map(category => {
            if (category.c_id === idcategory)
                category.color = data;
        })
        let category = this.categoryTagTypes.filter(tc => tc.c_id === idcategory);
    }






}
