import {Component, Output, EventEmitter, Input} from "@angular/core";
import { TagService } from "src/modules/tag/services/tag.service";
import {Tag} from "src/modules/tag/models/tag";
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: "tags-select-group-tags", 
    templateUrl: "./select-group-tags.component.html"
  })
  
export class SelectGroupTagsComponent {
  @Output() onClick: EventEmitter<any> = new EventEmitter();
  @Output() onChange: EventEmitter<any> = new EventEmitter();

  public categories: Tag[] = [];
  public form: FormGroup;
  public idtag: FormControl;
  @Input() id: number;

  constructor(private _tag : TagService) {
    this.form = new FormGroup({
      idtag: new FormControl('', Validators.required)
    });

    this._tag.toSelect().subscribe((item : Tag[]) => {
       this.categories = this.formatData(item);
       if(this.id) {
         
        this.form.patchValue({idtag: this.id})
      }
   });
  }

  onSelect(event): void {
    this.onClick.emit(event);
    this.onChange.emit(event.value);
  }

  markCheck(): void {
    this.form.markAllAsTouched();
  }

  formatData(data): any {
    const obj: any = {};
    data.forEach((item) => {
      if (item.tag_category !== null) {
        if (!obj.hasOwnProperty(item.tag_category.name)) {
          obj[item.tag_category.name] = [];
        }
        obj[item.tag_category.name].push({
          idtag: item.idtag,
          name: item.name,
        });
      }
    });
    const values = Object.entries(obj);
    return values;
  }
}
