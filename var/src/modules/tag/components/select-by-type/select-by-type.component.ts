import {Component, Output, EventEmitter, Input, OnInit} from "@angular/core";
import { TagService } from "src/modules/tag/services/tag.service";
import {Tag} from "src/modules/tag/models/tag";
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: "tags-by-type", 
    templateUrl: "./select-by-type.component.html"
  })
  
export class SelectByTypeComponent implements OnInit{
  @Output() onClick: EventEmitter<any> = new EventEmitter();
  @Output() onChange: EventEmitter<any> = new EventEmitter();

  public categories: Tag[] = [];
  public form: FormGroup;
  public idtag: FormControl;
  @Input() idtags: number;
  @Input() type: string;

  constructor(private _tag : TagService) {
    this.form = new FormGroup({
       tags: new FormControl('', Validators.required)
    });

    
  }

  ngOnInit(): void {
    this._tag.byType([this.type]).subscribe((item : Tag[]) => {
        this.categories = this.formatData(item);
        if(this.idtags) {
          this.form.patchValue({idtag: this.idtags})
       }
    });
  }

  onSelect(event): void {
    this.onClick.emit(event);
    this.onChange.emit(event.value);
  }

  markCheck(): void {
    this.form.markAllAsTouched();
  }

  formatData(data): any {
    const obj: any = {};
    data.forEach((item) => {
      if (item.tag_category !== null) {
        if (!obj.hasOwnProperty(item.tag_category.name)) {
          obj[item.tag_category.name] = [];
        }
        obj[item.tag_category.name].push({
          idtag: item.idtag,
          name: item.name,
        });
      }
    });
    const values = Object.entries(obj);
    return values;
  }
}
