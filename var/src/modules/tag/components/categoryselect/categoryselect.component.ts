import { Component, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { Tag } from 'src/modules/tag/models/tag';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';

@Component({
    selector: 'tag-categoryselect',
    templateUrl: './categoryselect.component.html',
    styleUrls: ['./categoryselect.component.scss']
})
export class TagCategorySelectComponent {

    @Output() modalSave = new EventEmitter<Tag[]>();
    @Output() removeTag = new EventEmitter<number>();
    @Output() emptyCats = new EventEmitter<boolean>();
    @Input() selectedTags: Tag[];
    @Input() statusCt: number;
    @Input() enableTag: number;
    @Input() playcircuit: PlayCircuit;
    @Input() tagType: any = ['D', 'P'];
    public categoryTags: Tag[];
    public tagTo: any;
    public tagTc: any;

    toggle = true

    public categories: any[] = [];

    constructor(
        private ws: WSService,
        private debug: DebugStore,
        private dialog: DialogService) {
    }

    public ngOnInit() {
        this.getTagsCategory();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes && changes.enableTag && changes.enableTag.currentValue > 0) {
            if (changes.enableTag.currentValue === 2) {
                this.verifyCat(this.selectedTags)
                this.selectedTags = []
            }
        }
        if (changes && changes.statusCt && changes.statusCt.currentValue > 0) {
            if (changes.statusCt.currentValue === 1) {
                this.categories.forEach(val => {
                    if (val.idcategory === changes.statusCt.currentValue) val.estado = false;
                }, this)
            }
            if (changes.statusCt.currentValue === 2) {
                this.categories.forEach(val => {
                    val.estado = false;
                }, this)
            }
        }
        if (changes.selectedTags) {
            this.formatSelectedTags();
        }
    }

    private formatSelectedTags() {
        this.selectedTags.map((tag, i) => {
            if (!tag.tag_category) {
                this.selectedTags[i].tag_category = this.categories.filter(c => c.idcategory === tag.idcategory)[0];
            }
        });
    }
    public openDialog() {

        this.dialog.tagcategoryselect("Tags", this.selectedTags, this.enableTag).afterClosed().subscribe((result) => {
            if (!result.cancel)
                this.modalSave.emit(result.tags);
        })
    }


    private getTagsCategory() {
        this.ws.tagCategoryToSelect().subscribe((result: IResult) => {

            if (result.success) {
                let categoryTags = result.data;

                this.categories = [];
                categoryTags.map(category => {

                    if (this.tagType.some(t => t == category.tagtype)) {
                        this.categories.push(category);
                    }


                })

                if (this.categories.length === 0)
                    this.emptyCats.emit(true);
                else
                    this.formatSelectedTags()
            } else {
                this.debug.addError('WS.formats', result.data);
            }
        }, error => {
            this.debug.addError('WS.formats', error.message);
        });
    }

    private getTagByIdTo(id: number) {
        this.ws.tagById(id).subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.tagId', 'successful');
                this.tagTo = result.data;
            }
            else {
                this.debug.addError('WS.tagId', result.data);
            }
        }, error => {
            this.debug.addError('WS.tagId', error.message);
        });
    }

    private getTagByIdTc(id: number) {
        this.ws.tagById(id).subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.tagId', 'successful');
                this.tagTc = result.data;
            } else {
                this.debug.addError('WS.tagId', result.data);
            }
        }, error => {
            this.debug.addError('WS.tagId', error.message);
        });
    }

    public verifyCat(seleccionados: any) {
        seleccionados.forEach(selec => {
            this.categories.forEach(val => {
                if (val.idcategory == selec.idcategory) val.estado = false;
            }, this)
        })
    }

    changeVal(idx: number) {
        if (this.categories[idx].estado) {
            this.categories[idx].estado = false
        } else {
            this.categories[idx].estado = true
        }
    }
}
