import { Component, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { User } from 'src/modules/user/models/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserStore } from 'src/modules/user/stores/user.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { isThisTypeNode } from 'typescript';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { TagCategory } from 'src/modules/tag/models/tagcategory';
import { borderTopRightRadius } from 'html2canvas/dist/types/css/property-descriptors/border-radius';
@Component({
    selector: 'tag-form',
    templateUrl: './form.page.html',
    styleUrls: ['./form.page.scss']

})

export class TagFormPage {

    public me: User;
    public loading = false;
    public tags = [];
    public tagCategories: TagCategory[] = [];
    public categoryTags = [];
    public tagsByCategory: TagCategory[] = [];
    public load = true;
    public showAddButton = true;
    public showAddForm = false;
    public newTagCategoryPermission: boolean = false;
    public newTagPermission: boolean = false;
    public editTagCategoryPermission: boolean = false;
    public editTagPermission: boolean = false;
    public deletedTagCategoryPermission: boolean = false;
    public deletedTagPermission: boolean = false;
    public tagCategoryListPermission: boolean = false;
    public tagListPermission: boolean = false;
    public customersToSelect: boolean = false;
    public idcustomer: number;
    public colors: FormControl[] = [];
    public categoryTagTypes: any[] = [];
    @ViewChild('categoryName') categoryName: ElementRef;
    @ViewChild('categoryTagType') categoryTagType: ElementRef;
    newForm: FormGroup;
    tagValue: string = 'D';

    tagtypes = [{ name: 'Distribución', code: 'D' }, { name: 'Personalización', code: 'P' }, { name: 'Publicidad', code: 'M' }];


    constructor(
        private ws: WSService,
        private debug: DebugStore,
        private _snackBar: MatSnackBar,
        private dialog: DialogService,
        private ustore: UserStore
    ) {

        this.newForm = new FormGroup({
            categoryName: new FormControl(null, Validators.required),
            categoryTagType: new FormControl(null),
            color: new FormControl(null)
        });

    }

    public ngOnInit() {
        this.me = this.ustore.getUser();
        this.verifyPermissions();
        this.getTagCategories();
    }

    private verifyPermissions() {
        this.newTagCategoryPermission = this.ustore.hasPermissionByName("post.tagcategory")
        this.newTagPermission = this.ustore.hasPermissionByName("post.tag")
        this.editTagCategoryPermission = this.ustore.hasPermissionByName("put.tagcategory.*")
        this.editTagPermission = this.ustore.hasPermissionByName("put.tag.*")
        this.deletedTagCategoryPermission = this.ustore.hasPermissionByName("delete.tagcategory.*")
        this.deletedTagPermission = this.ustore.hasPermissionByName("delete.tag.*")
        this.customersToSelect = this.ustore.hasPermissionByName("get.customers.toselect");


        if (!this.deletedTagPermission) {
            this.load = false;
            this.openSnackBar("No tienes permisos para acceder a este recurso");
        }


    }

    private getTagCategories() {
        this.ws.tagCategories().subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.tagCategories', 'successful');
                this.tagCategories = [];
                result.data.map(res => this.tagCategories.push(res))
                this.tagCategories.map(tc => {
                    this.categoryTagTypes.push({ c_id: tc.idcategory, c_type: tc.tagtype, color: tc.color });
                    this.colors.push(new FormControl(tc.color));
                });

                this.tagCategoryListPermission = true;
                this.getTags()
            } else {
                this.debug.addError('WS.tags', result.data);
                this.load = false;
                return null;
            }
        }, error => {
            this.debug.addError('WS.tags', error.message);
            return null;
        })
    }

    public onTabChanged(event) {

        this.tagValue = this.tagtypes[event.index].code;
    }

    private getTags() {
        this.ws.tags().subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.tags', 'successful');
                this.tags = [];
                this.tags = result.data
                this.tagListPermission = true;
                this.groupTagsByCategory()
            } else {
                this.debug.addError('WS.tags', result.data);
                this.tagListPermission = true;
                this.groupTagsByCategory()
            }
        }, error => {
            this.debug.addError('WS.tags', error.message);
            return null;
        })
    }

    private groupTagsByCategory() {
        let tempGroupTags = [];
        let customerName;
        this.tagCategories.forEach(function (tagCategory) {
            if (tempGroupTags[tagCategory.idcategory]) {
                this.tags.forEach(function (tag) {
                    if (tag.idcategory == tagCategory.idcategory) {
                        tempGroupTags[tagCategory.idcategory]["tags"].push({
                            idtag: tag.idtag,
                            tagName: tag.name
                        });
                    }
                })
            } else {
                tempGroupTags[tagCategory.idcategory] = [];
                tempGroupTags[tagCategory.idcategory]["idCategory"] = tagCategory.idcategory;
                tempGroupTags[tagCategory.idcategory]["categoryName"] = tagCategory.name;
                tempGroupTags[tagCategory.idcategory]["tagtype"] = tagCategory.tagtype;
                tempGroupTags[tagCategory.idcategory]["color"] = tagCategory.color;

                if (!this.me.customer) {
                    if (tagCategory.idcustomer)
                        this.ws.getCustomer(tagCategory.idcustomer).subscribe(res => {
                            if (res.success) {
                                tempGroupTags[tagCategory.idcategory]["customerName"] = res.data.name;
                            }
                        })
                    else
                        tempGroupTags[tagCategory.idcategory]["customerName"] = "Todos";
                }
                tempGroupTags[tagCategory.idcategory]["tags"] = [];
                this.tags.forEach(function (tag) {
                    if (tag.idcategory == tagCategory.idcategory) {
                        tempGroupTags[tagCategory.idcategory]["tags"].push({
                            idtag: tag.idtag,
                            tagName: tag.name
                        });
                    }
                })
            }
        }, this)
        const tag = [];
        for (var key in tempGroupTags) {
            if (tempGroupTags.hasOwnProperty(key))
                tag.push(tempGroupTags[key]);
        }
        this.tagsByCategory = tag;
        console.log(tag)
        this.load = false;
    }

    private handleAddCategory(e) {
        e.preventDefault()
        this.showAddButton = false;
        this.showAddForm = true;
    }

    private handleTagInput(e) {
        //e.setAttribute("class", "democlass");
    }

    public updateTag(idTag) {
        let newValue = (<HTMLInputElement>document.getElementById(idTag)).value;
        this.ws.updateTag(idTag, { name: newValue }).subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.updateTag', 'successful');
                this.openSnackBar("Etiqueta actualizada correctamente");
                this.loading = false;
                document.getElementById(idTag).blur()
            }
            else {
                this.debug.addError('WS.updateTag', result.data);
                this.openSnackBar(result.data);
                this.loading = false;
            }
        }, error => {
            this.debug.addError('WS.updateTag', error.message);
            this.openSnackBar(error.message);
            this.loading = false;
        });
    }

    public changeCategoryTagType(value, category_id) {
        this.categoryTagTypes.map(t => {
            if (t.c_id === category_id)
                t.c_type = value;
        });
    }
    private updateTagCategory(idCategory) {
        let newValue = (<HTMLInputElement>document.getElementById("cat-tag-" + idCategory)).value;
        let tagType = 'P';
        let color = '#';
        this.categoryTagTypes.map(t => {
            if (t.c_id === idCategory) {
                tagType = t.c_type;
                color = t.color;
            }
        });
        if (newValue === null) {
            this.openSnackBar('Tienes que rellenar el campo nombre');
        } else if (tagType === null || (tagType !== 'P' && tagType !== 'D' && tagType !== 'M')) {
            this.openSnackBar('El campo "tipo de tag" no es correcto.');
        } else {
            this.ws.updateTagCategory(idCategory, { name: newValue, tagtype: tagType, color: color }).subscribe((result: IResult) => {
                if (result.success) {
                    this.debug.addInfo('WS.updateCategory', 'successful');
                    this.openSnackBar("Categoría actualizada correctamente");
                    this.loading = false;
                }
                else {
                    this.debug.addError('WS.updateCategory', result.data);
                    this.openSnackBar(result.data);
                    this.loading = false;
                }
            }, error => {
                this.debug.addError('WS.updateCategory', error.message);
                this.openSnackBar(error.message);
                this.loading = false;
            });
        }
    }

    private createTagCategory(e, name, tagtype) {
        if (!this.newForm.valid) {
            if (!this.newForm.value.categoryName)
                this.newForm.controls['categoryName'].markAsTouched();

            if (!this.newForm.value.categoryTagType)
                this.newForm.controls['categoryTagType'].markAsTouched();

            this.openSnackBar("Debes rellenar todos lo campos");
            return;
        }
        let btnAction = document.getElementById("add-new-category-tag")
        btnAction.innerText = "Guardando.."
        btnAction.setAttribute("disabled", "disabled")
        if (name === null || tagtype === null) {
            this.openSnackBar('Tienes que rellenar todos los campos');
        }
        this.ws.me().subscribe((result: IResult) => {
            if (result.success) {
                let idcustomer = (result.data.user && result.data.user.customer) ? result.data.user.customer.idcustomer : this.idcustomer;
                let bodyData = {};

                if (idcustomer) {
                    bodyData = {
                        idcustomer: idcustomer,
                        name: this.newForm.value.categoryName,
                        tagtype: this.tagValue
                    }
                } else {
                    bodyData = {
                        name: this.newForm.value.categoryName,
                        tagtype: this.tagValue
                    }
                }
                this.ws.createTagCategory(bodyData).subscribe((result: IResult) => {
                    if (result.success) {
                        this.debug.addInfo('WS.createTagCategory', 'successful');
                        this.openSnackBar("Categoría creada correctamente");
                        this.loading = false;
                        this.getTagCategories();
                        btnAction.innerText = "Guardar";
                        btnAction.removeAttribute("disabled");
                        this.showAddButton = true;
                        this.showAddForm = false;
                    } else {
                        this.debug.addError('WS.createTagCategory', result.data);
                        this.openSnackBar(result.data);
                        this.loading = false;
                    }
                }, error => {
                    this.debug.addError('WS.createTagCategory', error.message);
                    this.openSnackBar(error.error.data.tagtype);
                    this.loading = false;
                });
            } else {
                this.debug.addError('WS.createTagCategory', result.data);
                this.loading = false;
            }
        }, error => {
            this.debug.addError('WS.createUser', error.message);
            this.loading = false;
        });
    }

    public createTag(e) {

        const idCategoryTag = e.v;
        if (e.e.key == "Enter") {
            let tagValue = (<HTMLInputElement>document.getElementById("add-" + idCategoryTag)).value;
            (<HTMLInputElement>document.getElementById("add-" + idCategoryTag)).value = "";
            (<HTMLInputElement>document.getElementById("add-" + idCategoryTag)).blur();
            (<HTMLInputElement>document.getElementById("add-" + idCategoryTag)).setAttribute("placeholder", "Añadiendo..");

            this.ws.me().subscribe((result: IResult) => {
                if (result.success) {
                    this.ws.createTag({ idcategory: idCategoryTag, name: tagValue }).subscribe((result: IResult) => {
                        if (result.success) {
                            let tagInserted = result.data

                            this.debug.addInfo('WS.createTag', 'successful');
                            this.openSnackBar("Etiqueta creada correctamente");
                            this.loading = false;

                            this.getTagCategories()

                        } else {
                            this.debug.addError('WS.createTag', result.data);
                            this.openSnackBar(result.data);
                            this.loading = false;

                        }
                    }, error => {
                        this.debug.addError('WS.createTag', error.message);
                        this.openSnackBar(error.message);
                        this.loading = false;
                    });

                }
                else {
                    this.debug.addError('WS.createTagCategory', result.data);
                    this.loading = false;

                }
            }, error => {
                this.debug.addError('WS.createUser', error.message);
                this.loading = false;
            });
        }
    }

    private deleteTagCategory(idCategory) {
        this.dialog.confirm('Confirmar eliminación', 'Eliminar categoría de etiqueta?').subscribe(confirm => {
            if (confirm) {
                this.ws.deleteTagCategory(idCategory).subscribe((result: IResult) => {
                    if (result.success) {
                        this.debug.addInfo('WS.deleteTagCategory', 'successful');
                        this.openSnackBar("Categoría de etiqueta eliminada");
                        this.loading = false;

                        this.getTagCategories()
                    }
                    else {
                        this.debug.addError('WS.deleteTagCategory', result.data);
                        this.openSnackBar(result.data);
                        this.loading = false;
                    }
                }, error => {
                    this.debug.addError('WS.deleteTagCategory', error.message);
                    this.openSnackBar(error.message);
                    this.loading = false;
                });
            }
        })
    }

    private deleteTag(tagId) {
        let tagElementParent = document.getElementById(tagId).parentElement;
        this.dialog.confirm('Confirmar eliminación', 'Eliminar etiqueta?').subscribe(confirm => {
            if (confirm) {
                this.ws.deleteTag(tagId).subscribe((result: IResult) => {
                    if (result.success) {
                        this.debug.addInfo('WS.deleteTag', 'successful');
                        this.openSnackBar("Etiqueta eliminada");
                        tagElementParent.remove();
                    }
                    else {
                        this.debug.addError('WS.deleteTag', result.data);
                        this.openSnackBar(result.data);

                    }
                }, error => {
                    this.debug.addError('WS.deleteTag', error.message);
                    this.openSnackBar(error.message);
                });
            }
        })
    }

    public editTag(tagInputId) {
        console.log(tagInputId)
        document.getElementById(tagInputId).focus();
    }
    public customerSelected(idcustomer) {
        if (idcustomer > 0)
            this.idcustomer = idcustomer;
        else
            this.idcustomer = null;
    }
    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }

    colorChanged(data, idcategory) {
        this.categoryTagTypes.map(category => {
            if (category.c_id === idcategory)
                category.color = data;
        })
        let category = this.categoryTagTypes.filter(tc => tc.c_id === idcategory);
    }
}
