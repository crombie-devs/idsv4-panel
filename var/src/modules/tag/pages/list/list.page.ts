import { Component, Input, ViewChild } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Tag } from 'src/modules/tag/models/tag';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'tag-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss']
})

export class TagListPage {

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
  
	public displayedColumns: string[] = ['select','idtag', 'name'];
	public selection = new SelectionModel<Tag>(true, []);
	public dataSource = new MatTableDataSource<Tag>();
	public showTotalPages = 0;
	public length = 0;
	public permission: boolean = false;
	public load: boolean = true;

  	constructor(
		private ws: WSService,
		private debug: DebugStore) {}
  
  	public ngOnInit() {
		this.debug.addInfo('TagListComponent', 'OnInit');
		this.ws.tags().subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.tags', 'successful');
				this.permission = true;
				this.renderDataTable(result.data);
			} else {            
				this.debug.addError('WS.tags', result.data);
				this.load = false;
				this.renderDataTable([]);			
			}
		}, error => {
			this.debug.addError('WS.tags', error.message);  
			this.load = false;
			return null;          
		}); 		
	}
	  
	public renderDataTable(data: Tag []) {
		this.dataSource = new MatTableDataSource();
		this.dataSource.data = data;
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
		this.load = false;
	}

	public isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	public masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource.data.forEach(row => this.selection.select(row));
	}

	public checkboxLabel(row?: Tag): string {
		if (!row) {
		  return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
		}
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idtag + 1}`;
	}
	
}
