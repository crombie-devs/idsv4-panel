import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { WSService } from 'src/services/ws.service';
import { IResult } from 'src/models/iresult';
import { DebugStore } from 'src/stores/debug.store';
import { Tag } from 'src/modules/tag/models/tag';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { UserStore } from '../../../user/stores/user.store';




@Component({
    selector: 'tagcategoryselect-dialog',
    templateUrl: './tagcategoryselect.dialog.html',
    styleUrls: ['./tagcategoryselect.dialog.scss']
})

export class TagCategorySelectDialog {

    public title: string;
    public tagcategories = [];


    public load = true;
    public selectedtags: Tag[] = [];
    public me: any;
    public idcustomer: number;

    constructor(public dialogRef: MatDialogRef<TagCategorySelectDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private ws: WSService,
        private _snackBar: MatSnackBar,
        private translate: TranslateService,
        private debug: DebugStore,
        private ustore: UserStore) {
        this.ustore.onChange.subscribe(user => {
            if (user)
                this.me = user;
        })
    }

    public ngOnInit() {
        this.me = this.ustore.getUser();
        this.getTags(this.data.tagsType, this.data.enableTags)
        this.idcustomer = this.data.enableTags;

    }

    public save() {
        this.tagcategories.map((tagcategory) => {
            tagcategory.tags.forEach((tag) => {
                tag.tags.forEach(element => {
                    if (element.selected && (this.selectedtags.length == 0 || !this.selectedtags.some(t => t.idtag === element.idtag))) {
                        element.categoryName = tag.category;
                        this.selectedtags = [...this.selectedtags, element]
                    }
                });
            });
        })
        this.dialogRef.close({
            cancel: false,
            tags: this.selectedtags
        });
    }

    public cancel() {
        this.dialogRef.close({
            cancel: true,
            tags: []
        });
    }

    private getTags(tagstype, idcustomer) {
        if (!idcustomer) {
            if (this.me && this.me.customer)
                idcustomer = this.me.customer.idcustomer;
        }

        let isArray = false;
        if (Array.isArray(tagstype)) {
            isArray = true;
        }


        this.ws.tagsToSelect().subscribe((result: IResult) => {
            if (result.success) {
                let tempTags = [];
                const mixto = [{ id: 'D', name: 'Dispersion' }, { id: 'P', name: 'Personalizacion' }, { id: 'M', name: 'Mensaje' }];
                const all = [{ id: 'D', name: 'Dispersion' }, { id: 'P', name: 'Personalizacion' }];
                let bucle = [{ id: 'M', name: 'Mensajes' }];
                //  let bucle = [{ id: 'D', name: 'Dispersion' }, { id: 'P', name: 'Personalizacion' }];
                if (!isArray) {
                    if (all.some(a => a == tagstype)) {
                        bucle = all;
                    }
                } else {
                    bucle = mixto;
                }

                result.data.forEach(function (tag) {
                    bucle.forEach(type => {
                        if (tag.tag_category) {
                            if ((tag.tag_category.idcustomer === idcustomer || idcustomer == 0) && tag.tag_category.tagtype === type.id) {
                                if (tempTags[tag.idcategory]) {
                                    tempTags[tag.idcategory]["tags"].push({
                                        idtag: tag.idtag,
                                        name: tag.name,
                                        selected: false,
                                        idcategory: tag.idcategory,
                                        tag_category: tag.tag_category
                                    });
                                } else {
                                    tempTags[tag.idcategory] = [];
                                    tempTags[tag.idcategory]["group"] = type.name;
                                    tempTags[tag.idcategory]["idcategory"] = tag.idcategory;
                                    tempTags[tag.idcategory]["category"] = tag.tag_category.name;
                                    tempTags[tag.idcategory]["tags"] = [];
                                    tempTags[tag.idcategory]["tags"].push({
                                        idtag: tag.idtag,
                                        name: tag.name,
                                        selected: false,
                                        idcategory: tag.idcategory,
                                        tag_category: tag.tag_category
                                    });
                                }
                            }
                        }
                    });
                }, this)
                let tags: any = {};
                tempTags.forEach((tag, index) => {
                    if (!Array.isArray(tags[tag.group])) {
                        tags[tag.group] = [];
                    }
                    tags[tag.group].push({ id: index, category: tag.category, idcategory: tag.idcategory, tags: tag.tags })
                })
                for (var key in tags) {
                    this.tagcategories.push({ category: key, tags: tags[key] });
                }
                if (this.data.selectedTags) {
                    this.mergeTags(this.data.selectedTags)
                }
                this.load = false;
            } else {
                this.debug.addError('WS.tags', result.data);
                this.openSnackBar(this.translate.instant("No se han encontrado tags"));
                this.load = false;
                return null;
            }
        }, error => {
            this.debug.addError('WS.tags', error.message);
            return null;
        })
    }
    private mergeTags(selectedTags: Tag[]) {
        this.tagcategories.map((tagcategory) => {
            if (tagcategory && tagcategory.tags.length > 0)
                tagcategory.tags.forEach((tag) => {
                    tag.tags.forEach(element => {
                        selectedTags.forEach((selectedTag) => {
                            if (element.idtag == selectedTag.idtag)
                                element.selected = true
                        });
                    });
                })
        })
    }
    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }
}
