import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TagCategorySelectDialog } from "./tagcategoryselect.dialog"
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/shared/shared.module';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { OverlayModule } from '@angular/cdk/overlay';
import { DebugStore } from 'src/stores/debug.store';
import { UserModule } from 'src/modules/user/user.module';
import { of } from 'rxjs';

const MatDialogMock = {
    open(){
        return {
            afterClosed: () =>  of({cancel: false})
        };
    }
}

describe("Diálogo: TagCategorySelectDialog", () => {
    let component: TagCategorySelectDialog;
    let fixture: ComponentFixture<TagCategorySelectDialog>;

    beforeEach(() =>{
        TestBed.configureTestingModule({
            imports: [
                UserModule,
                OverlayModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                }),
                HttpClientTestingModule],
            declarations: [],
            providers: [
                WSService,
                MatSnackBar,
                DebugStore,
                {
                    provide: MAT_DIALOG_DATA, useValue: {},
                },
                {
                    provide: MatDialogRef, useValue: MatDialogMock
                }
            ],
            schemas:[CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TagCategorySelectDialog);
        component = fixture.componentInstance;

        fixture.detectChanges();
    });

    it("Componente creado.", () => {
        expect(component).toBeTruthy();
    })
})