import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TagRoutingModule } from "src/modules/tag/tag-routing.module";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { SelectGroupTagsComponent } from './components/select-group-tags/select-group-tags.component';
import { TagService } from 'src/modules/tag/services/tag.service';
import { SharedModule } from 'src/shared/shared.module';
import { DirectivesModule } from 'src/directive/directives.module';
import { TagCategorySelectComponent } from 'src/modules/tag/components/categoryselect/categoryselect.component';
import { TagCategorySelectItemComponent } from 'src/modules/tag/components/categoryselectitem/categoryselectitem.component';
import { TagCategorySelectDialog } from 'src/modules/tag/dialogs/tagcategoryselect/tagcategoryselect.dialog';
import { MaterialModule } from 'src/app/material.module';
import { TagListPage } from './pages/list/list.page';
import { TagFormPage } from './pages/form/form.page';
import { SelectByTypeComponent } from "./components/select-by-type/select-by-type.component";
import { CreateTagComponent } from "./components/create-tag/create-tag.component";

@NgModule({
    declarations: [
        TagCategorySelectItemComponent,
        TagCategorySelectComponent,
        TagCategorySelectDialog,
        SelectGroupTagsComponent,
        TagListPage,
        TagFormPage,
        SelectByTypeComponent,
        CreateTagComponent
    ],
    imports: [
        CommonModule,
        TagRoutingModule,
        MaterialModule,
        MatDialogModule,
        MatCheckboxModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        SharedModule,
        DirectivesModule
    ],
    exports: [
        TagCategorySelectItemComponent,
        TagCategorySelectComponent,
        TagCategorySelectDialog,
        SelectGroupTagsComponent,
        SelectByTypeComponent,
        CreateTagComponent
    ],
    providers: [TagService],
    entryComponents: [TagCategorySelectDialog]
})
export class TagModule { }
