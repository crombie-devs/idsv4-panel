import { EmissionsSalesPage } from './emissions-sales.page';
import { EmissionsSalesFormComponent } from 'src/modules/emission-sale/components/form/form.component';
import { EmissionsSalesResultAnalysisComponent } from 'src/modules/emission-sale/components/result-analysis/result-analysis.component';
import { EmissionsSalesResultSummaryComponent } from 'src/modules/emission-sale/components/result-summary/result-summary.component';

export const EMISSIONSSALES = [
    EmissionsSalesPage,
    EmissionsSalesFormComponent,
    EmissionsSalesResultAnalysisComponent,
    EmissionsSalesResultSummaryComponent
];
