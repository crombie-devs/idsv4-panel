import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmissionsSalesPage } from './pages/emissions-sales/emissions-sales.page';

const routes: Routes = [
  { path: '', component: EmissionsSalesPage},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class EmissionSaleRoutingModule { }
