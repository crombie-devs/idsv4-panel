import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexModule } from '@angular/flex-layout';
import { HttpService } from 'src/services/http.service';

import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from 'src/app/material.module';

import { EMISSION_SALE_COMPONENTS } from './components/components';
import { EmissionSaleRoutingModule } from './emission-sale-routing.module';

import { FormFieldModule } from 'src/modules/form-field/form-field.module';
import { ChartGraphsModule } from 'src/modules/chart/charts.module';

import { EmissionsSalesPage } from './pages/emissions-sales/emissions-sales.page';

import { CONFIG } from 'src/app/app.config';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import moment from 'moment';
import { PipesModule } from 'src/pipes/pipes.module';

export function HttpLoaderFactory(http: HttpClient) {
  let url = CONFIG.lang_url;  
  return new TranslateHttpLoader(http, `${url}/i18n/`, `.json?timestamp=${moment().valueOf()}`);
}

@NgModule({
  declarations: [EMISSION_SALE_COMPONENTS, EmissionsSalesPage],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FormFieldModule,
    ChartGraphsModule,
    EmissionSaleRoutingModule,
    PipesModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [HttpService],
  exports:[EMISSION_SALE_COMPONENTS]
})
export class EmissionSaleModule {
  constructor(private translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('es');
    if(!sessionStorage.getItem('lang')){
      sessionStorage.setItem('lang', 'es');
      translate.use('es');
    }else{
      translate.use(sessionStorage.getItem('lang'));
    }
  }
}