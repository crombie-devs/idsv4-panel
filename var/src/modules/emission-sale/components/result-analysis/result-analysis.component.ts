import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { Auth } from 'src/modules/auth/models/auth';
import { IResult } from 'src/models/iresult';
import {MatGridListModule} from '@angular/material/grid-list';

import moment from 'moment';
import { Moment } from 'moment';
import { User } from 'src/modules/user/models/user';
import { UserStore } from 'src/modules/user/stores/user.store';

import { ChartDataSets } from 'chart.js';
import { Color, SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import {TranslateService} from '@ngx-translate/core';
import { CONFIG } from 'src/app/app.config';

export interface HeaderData{
	sites:string;
	content: string;
	products: string;
	customer: string;
	player: string;
	app: string;
	scale: string;
	dataType: string;
	dateStart: string;
	dateEnd: string;
}

@Component({
  selector: 'emissions-sales-result-analysis',
  templateUrl: './view.component.html',
  styleUrls: ['./styles.component.scss']
})

export class EmissionsSalesResultAnalysisComponent {
	@Input() data: any;
	@Output() onError: EventEmitter<any> = new EventEmitter<any>();
	@Output() onBackToForm:EventEmitter<any> = new EventEmitter(null);

	public loading:boolean = false;
	public emissionsByHour: any[] = [];
	public graphEmisions: any[] = [];
	public totalEmissions: number = 0;
	public emissions: any[] = [];
	public emission:any;
	public diapuntaafluencias:string = '';
	public countdiapuntaafluencias:number = 0;
	public datadifendif: any = {};
	public wspython: any;
	public weekdays:string[] = [
		"WEEKDAYS.mon", 
		"WEEKDAYS.tue", 
		"WEEKDAYS.wed", 
		"WEEKDAYS.thu", 
		"WEEKDAYS.fri", 
		"WEEKDAYS.sat", 
		"WEEKDAYS.sun"];

	public config:any = CONFIG;

	VentasEmisionesAfluenciasLineChartData: ChartDataSets[] = [];
	public final_venta: any[] = [];
	public final_faces:any[] = [];
	public final_emision:any[] = [];
	VentasEmisionesAfluenciasLineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];

	ComparativaConPeriodoBarchartData: ChartDataSets[] = [];
	ComparativaConTiendasBarchartData: ChartDataSets[] = [];
	comparativePeriod:number[] = [];
	positive:boolean = true;

	VentasHoraDiaLineChartData: ChartDataSets[] = [];
	ComparativoPeriodoAnterior: ChartDataSets[] = [];
	ComparativoGrupoContraste: ChartDataSets[] = [];

	public headerData: HeaderData = {
		sites: '',
		content: '',
		products: '',
		customer: '',
		player: this.translate.instant('COMMON.all_male'),
		app: this.translate.instant('COMMON.female'),
		scale: this.translate.instant('COMMON.hours'),
		dataType: this.translate.instant('COMMON.units'),
		dateStart: '',
		dateEnd: ''
	};

	public labelAverSales= this.translate.instant('Ventas diarias medias');

  	constructor(
		  private ws: WSService,
		  private dialog: DialogService,
		  private translate: TranslateService,
	  ) {
  	}
  
  	public ngOnInit() {
		this.getHeaderData();
		this.getBlockOneData();
	}
	
	public getHeaderData(){
		this.getSites();
		this.getCustomer();
		this.getContent();
		this.getProducts();
	}

	public getBlockOneData(){
		this.loading = true;
		const data = this.data.formData;
		const content = this.data.content[0];
		let params: any = {
			idcustomer: data.customer,
			date_start: content.date_on,
			date_end: content.data_off,
			idcampaign: data.content,
		}
		if(data.site !== 0){
			params.idsite = data.site
		}	
		this.ws.analyticsEmisionesCountByHour(params).subscribe(result => {
			if(result.success){
				result.data.map(res => {
					const dato = {
						hour: res._id.hour,
						count: res.count
					}
					this.totalEmissions += res.count;
					this.emissionsByHour.push(dato);
				});
			}
		});
		this.ws.anayticsEmisionesFindAll(params).subscribe(result => {
			if(result.success){
				result.data.map(res => {
					if(res.count > this.countdiapuntaafluencias){
						this.countdiapuntaafluencias=res.count;
						this.diapuntaafluencias = res.date;
						this.graphEmisions.push({day: res.date, count: res.count});
					}
				});
				this.graphEmisions.map(emission => {
					let index:number = -1;
					this.emissions.map((emi,ind) =>{
						if(emission.day === emi.day)
							index = ind;
							return;
					});
					if(index == -1){
						this.emissions.push(emission);
					}else{
						this.emissions[index].count += emission.count;
					}
				});
			}
		});
		const min_date:string = content.date_on;
		const today:Moment = moment();
		const date_off:Moment = moment(content.date_off);
		let max_date: string;
		let max_date_peant: string;
		if(date_off > today){
			max_date = today.format('YYYY-MM-DD');
		}else{
			max_date = content.date_off;
		}
		const minDate_peant:string = moment(data.date).format('YYYY-MM-DD');
		max_date_peant = moment(max_date).subtract(12, 'months').format('YYYY-MM-DD');

		const a_dates = {
			minDate_: min_date,
			maxDate_: max_date,
			minDate_peant: minDate_peant, 
			maxDate_peant: max_date_peant, 
			minDate_campaign: content.date_on, 
			maxDate_campaign:content.date_off
		}
		this.getDataJsonDifenDif(a_dates,data.customer,data.content,data.site);
	}

	public getDataJsonDifenDif(a_dates:any,customer:number,content:number,site:number = null){
		const params:any = {
			dates:a_dates,
			idcustomer: customer,
			idcontent: content,
		}
		if(site){
			params.idsite = site;
		}
		this.ws.emissionSalesData(params).subscribe(result => {
			if(result.success){
				console.log(params, result);
				if(!result.data){
					this.dialog.warning('COMPONENTS.emissions_sales.result_analysis.getDataJsonDifenDif.warning.title', 'COMPONENTS.emissions_sales.result_analysis.getDataJsonDifenDif.warning.text');
					this.onError.emit();
				}
				this.datadifendif = result.data
				this.datadifendif.weekdaymayorafluencia = this.getWeekDayTranslate(this.datadifendif.weekdaymayorafluencia);
				this.datadifendif.a_usold_grpe.map(dat => {
					this.final_venta[dat[0]] = dat[1];
				});
				this.datadifendif.a_faceshour.map(dat => {
					this.final_faces[dat.hour] = dat.count;
				});
				this.emissionsByHour.map(dat => {
					this.final_emision[dat.hour] = dat.count;
				});
				if(CONFIG.demo)
					this.VentasEmisionesAfluenciasLineChartData = [
						{
							label: this.translate.instant('Unidades'),
							data: this.final_venta,
							yAxisID: 'y-axis-1'
						},
						{
							label: this.translate.instant('Afluencias'),
							data: this.final_faces,
							yAxisID: 'y-axis-2'
						}
					];
				else
					this.VentasEmisionesAfluenciasLineChartData = [
						{
							label: this.translate.instant('Unidades'),
							data: this.final_venta,
							yAxisID: 'y-axis-1'
						},
						{
							label: this.translate.instant('Afluencias'),
							data: this.final_faces,
							yAxisID: 'y-axis-2'
						},
						{
							label: this.translate.instant('Emisiones'),
							data: this.final_emision,
							yAxisID: 'y-axis-2'
						}
					];
				this.datadifendif.aweekticket.map((day,index) => {
					let data:number[] = []
					day.map(hour => {
						data.push(hour.count);
					})
					this.VentasHoraDiaLineChartData.push({label: this.translate.instant(this.weekdays[index]), data:data, fill:false });
				});
				this.ComparativaConPeriodoBarchartData = [
					{ data: [this.datadifendif.variacionvmdte.diff_em_preEm], label: this.translate.instant("Periodo Anterior") },
					{ data: [this.datadifendif.variacionvmdte.diff_em_pEm], label: this.translate.instant('Periodo Emisión') },
				];
				this.ComparativaConTiendasBarchartData = [
					{ data: [this.datadifendif.variacionvmdte.diff_em_preEm], label: this.translate.instant('Grupo Emisión P.Ant') },
					{ data: [this.datadifendif.variacionvmdte.diff_co_preEm], label: this.translate.instant('Grupo Contraste P.Ant') },
					{ data: [this.datadifendif.variacionvmdte.diff_em_pEm], label: this.translate.instant('Grupo Emisión P.Emis') },
					{ data: [this.datadifendif.variacionvmdte.diff_co_pEm], label: this.translate.instant('Grupo Contraste P.Emis') }
				];
				if(this.datadifendif.variacionvmdte.diff_var_uni_em !== 'null' && this.datadifendif.variacionvmdte.diff_var_uni_em !== 'null')
					if(parseInt(this.datadifendif.variacionvmdte.diff_var_uni_em) > 0){
						this.comparativePeriod = [parseInt(this.datadifendif.variacionvmdte.diff_var_uni_em),100-parseInt(this.datadifendif.variacionvmdte.diff_var_uni_em)]
						this.positive = true;
					}else {
						this.comparativePeriod = [100+parseInt(this.datadifendif.variacionvmdte.diff_var_uni_em),parseInt(this.datadifendif.variacionvmdte.diff_var_uni_em)]
						this.positive = false;
					}

				this.loading = false;
			}else {
				return null;
			}
		}, error => {
			this.dialog.warning('COMPONENTS.emissions_sales.result_analysis.getDataJsonDifenDif.error.title','COMPONENTS.emissions_sales.result_analysis.getDataJsonDifenDif.error.text')
			this.onError.emit();
		});
	}

	public getCustomer(){
		this.ws.getCustomer(this.data.formData.customer).subscribe((result: IResult) => {
			if(result.success) {
				this.headerData.customer = result.data.name;
			}
			else {
				return null;
			}
		}, error => {
			return null;          
		});	
	}

	public getSites(){
		if(this.data.formData.site === 0){
			this.headerData.sites = 'COMMON.all_male';
		}else{
			this.ws.getSite(this.data.formData.site).subscribe((result: IResult) => {
				if(result.success) {
					this.headerData.sites = result.data.name;
				}
				else {
					return null;
				}
			}, error => {
				return null;          
			});	
		}
	}

	public getContent(){
		const content = this.data.content[0];
		this.headerData.content = `${content.idcontent} - ${content.name}`;
		this.headerData.dateStart = content.date_on;
		this.headerData.dateEnd = content.date_off;
	}
	public getProducts(){
		const products = this.data.products;
		products.map((product,index) => {
			if(product[0])
				if(index === products.length - 1)
					this.headerData.products += `${product[0].value}`;
				else
					this.headerData.products += `${product[0].value}, `;
		})
	}
	volverForm($event){
		this.onBackToForm.emit($event);
	}

	public getWeekDayTranslate(weekDay: string){
		switch (weekDay){
			case "lunes":
				return this.translate.instant('WEEKDAYS.mon');
			case "martes":
				return this.translate.instant('WEEKDAYS.tue');
			case "miercoles":
				return this.translate.instant('WEEKDAYS.wed');
			case "jueves":
				return this.translate.instant('WEEKDAYS.thu');
			case "viernes":
				return this.translate.instant('WEEKDAYS.fri');
			case "sabado":
				return this.translate.instant('WEEKDAYS.sat');
			case "domingo":
				return this.translate.instant('WEEKDAYS.sun');
			default:
				return '';
		}

	}
}
