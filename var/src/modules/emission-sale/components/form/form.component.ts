import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { CONFIG } from 'src/app/app.config';
import { FormField } from 'src/modules/form-field/models/form-field';
import { UserStore } from 'src/modules/user/stores/user.store';
import { IResult } from 'src/models/iresult';
import moment from 'moment';
import { Moment } from 'moment';
import { TranslateService } from '@ngx-translate/core';


@Component({
    selector: 'emissions-sales-form',
    templateUrl: './view.component.html',
    styleUrls: ['./styles.component.scss']
})

export class EmissionsSalesFormComponent {
    @Output() onSubmittedForm: EventEmitter<any> = new EventEmitter<any>();
    public meses: string[] = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    public initialDate: Moment = moment().subtract(1, 'years');
    public showtab: boolean = true;
    public tab: number = 0;
    public formBackup: any;
    public loadingYear: boolean = false;
    public loadingSite: boolean = true;
    public loadingBIPA: boolean = true;
    public loadingMonth: boolean = false;

    public productsFromTickets: any[] = [];
    public productsFromContents: any[] = [];

    public allContentsToSelect: any[] = [];

    public fieldDataCustomer: FormField = {
        label: "Cliente",
        name: 'customer',
        placeholder: '',
        options: [],
        multiple: false,
        required: true
    };
    public fieldDataSite: FormField = {
        label: "Centros",
        name: 'site',
        placeholder: '',
        options: [],
        multiple: false,
        required: false
    };
    public fieldDataBIPA: FormField = {
        label: "Categorías BIPA",
        name: 'bipa',
        placeholder: '',
        options: [],
        multiple: true,
        required: false
    };
    public fieldDataYear: FormField = {
        label: "Año",
        name: 'year',
        placeholder: '',
        options: [{ name: '2021', value: 2021 }],
        multiple: false,
        required: true
    };
    public fieldDataMonth: FormField = {
        label: "Mes",
        name: 'month',
        placeholder: '',
        options: [
            { name: 'Enero', value: 1 },
            { name: 'Febrero', value: 2 },
            { name: 'Marzo', value: 3 },
            { name: 'Abril', value: 4 },
            { name: 'Mayo', value: 5 },
            { name: 'Junio', value: 6 },
            { name: 'Julio', value: 7 },
            { name: 'Agosto', value: 8 },
            { name: 'Septiembre', value: 9 },
            { name: 'Octubre', value: 10 },
            { name: 'Noviembre', value: 11 },
            { name: 'Diciembre', value: 12 }
        ],
        multiple: false,
        required: true
    };


    public fieldDataContent: FormField = {
        label: "Contenido",
        name: 'content',
        placeholder: '',
        options: [],
        required: true
    };
    public fieldDataProducts: FormField = {
        label: "Referencias",
        name: 'products',
        placeholder: '',
        options: [],
        multiple: true,
        required: true
    };
    public fieldDataPrevious: FormField = {
        label: "Periodo anterior",
        name: 'date',
        placeholder: '',
        required: true
    };
    public me: any;
    public formGroup: FormGroup = new FormGroup({
        customer: new FormControl(null, Validators.required),
        site: new FormControl(null),
        bipa: new FormControl(null),
        year: new FormControl(null, Validators.required),
        month: new FormControl(null, Validators.required),
        content: new FormControl(null, Validators.required),
        products: new FormControl(null, Validators.required),
        date: new FormControl(this.initialDate.toDate().toISOString(), Validators.required)
    });

    public labelTab1 = this.translate.instant("Análisis de campaña");
    public labelTab2 = this.translate.instant("Resumen mensual de campañas");

    constructor(
        private ws: WSService,
        private debug: DebugStore,
        private dialog: DialogService,
        private ustore: UserStore,
        private translate: TranslateService) {
            let yearOptions:any = [];
			for(let i = 0; i < 5; i++){
				const year  = moment().subtract(i, 'years').year();
				yearOptions = [...yearOptions, {name: year.toString(), value: year}];
			}
			this.fieldDataYear.options = yearOptions;
    }

    public ngOnInit() {
        this.me = this.ustore.getUser();
        if (this.me.customer) {
            const customer = this.me.customer;
            this.fieldDataCustomer.options = [{ name: customer.name, value: customer.idcustomer }]
            this.formGroup.patchValue({ customer: customer.idcustomer });

			if(this.me.customer.idcustomer === 31){
				this.initialDate = moment('2019-' + moment().format('MM-DD'));
			}
            if (this.me.site) {
                const site = this.me.site;
                this.fieldDataSite.options = [{ name: site.name, value: site.idsite }]
                this.formGroup.patchValue({ site: site.idsite });
                this.loadingSite = false;
            } else {
                this.getSites();
            }
        } else {
            this.getCustomers();
        }
        this.getBIPAS();
        //this.getYears();
    }
    public getBIPAS() {
        this.ws.contentCategoriesToSelect().subscribe((result: IResult) => {
            if (result.success) {
                result.data.map(bipa => {
                    this.fieldDataBIPA.options.push({ name: bipa.name, value: bipa.idcategory });
                })
                this.loadingBIPA = false;
            } else {
                this.debug.addError('WS.contentcategories', result.data);
                this.loadingBIPA = false;
                return null;
            }
        }, error => {
            this.debug.addError('WS.contentcategories', error.message);
            this.loadingBIPA = false;
            return null;
        })
    }
    public getSites() {
        this.fieldDataSite.options = [];
        this.formGroup.patchValue({ site: null });
        this.loadingSite = true;
        this.ws.sites().subscribe((result: IResult) => {
            if (result.success) {
                this.fieldDataSite.options.push({ name: "Todos", value: 0 });
                this.formGroup.patchValue({ site: 0 });
                result.data.map(res => {
                    if (res.code && res.consider_in_sales)
                        this.fieldDataSite.options.push({ name: res.name, value: res.idsite });
                });
                this.loadingSite = false;
            }
            else {
                this.debug.addError('WS.sites', result.data);
                this.loadingSite = false;
                return null;
            }
        }, error => {
            this.debug.addError('WS.sites', error.message);
            this.loadingSite = false;
            return null;
        });
    }
    getCustomers() {
        this.ws.customers().subscribe((result: IResult) => {
            if (result.success) {
                this.fieldDataCustomer.options.push({ name: "Todos", value: 0 });
                this.formGroup.patchValue({ customer: 0 });
                result.data.map(res => {
                    this.fieldDataCustomer.options.push({ name: res.name, value: res.idcustomer });
                });
            }
            else {
                return null;
            }
        }, error => {
            return null;
        });
    }

    getContents(recall:boolean = false) {
        this.fieldDataContent.options = [];
        this.allContentsToSelect = [];
        let bipas = [];
        this.formGroup.patchValue({ content: null });
        var params: any = {
            idcustomer: this.formGroup.get('customer').value,
            status: true
        }
        if (this.formGroup.get('year').value)
            params.year = this.formGroup.get('year').value
        if (this.formGroup.get('month').value)
            params.month = this.formGroup.get('month').value
        if (this.formGroup.get('site').value)
            params.idsite = this.formGroup.get('site').value
        if (this.formGroup.get('bipa').value) {
            bipas = this.formGroup.get('bipa').value;
        }
        if(CONFIG.demo && recall){
            params.year = 2020;
            params.month = 12;
        }
        this.ws.contentsToEmission(params).subscribe((result: IResult) => {
            if(result.success) {
                if (result.data.length > 0) {
                    result.data.map(res => {
                        if (bipas.length > 0 && bipas.some(b => b === res.idcategory)) {
                            res.date_on = res.dates[0].date_on;
                            res.date_off = res.dates[0].date_off;
                            this.fieldDataContent.options.push(res);
                            this.allContentsToSelect.push(res)
                        } else if (bipas.length === 0) {
                            res.date_on = res.dates[0].date_on;
                            res.date_off = res.dates[0].date_off;
                            this.fieldDataContent.options.push(res);
                            this.allContentsToSelect.push(res)
                        }
                    });
                }
                else
                    this.dialog.warning('Contenidos', 'No se han encontrado contenidos');
            } else {
                if (CONFIG.demo && !recall) 
                    this.getContents(true);    
                else
                    this.dialog.warning('Contenidos', 'No se han encontrado contenidos');
            }
        }, error => {
            return null;
        });
    }

    public filterByName(q: string) {
        if (!q) {
            this.fieldDataContent.options = this.allContentsToSelect;
        } else {
            this.fieldDataContent.options = this.allContentsToSelect.filter(
                (content: any) =>
                    content.name.toLowerCase().includes(q.trim().toLowerCase()) ||
                    content.idcontent.toString().includes(q.trim())
            );
        }
    }



    getYears() {
        let params: any = {
            customer_id: this.formGroup.get('customer').value,
        }
        if (this.formGroup.get('customer').value)
            params.customer_id = this.formGroup.get('customer').value;
        if (this.formGroup.get('site').value)
            params.site_id = this.formGroup.get('site').value;

        this.fieldDataYear.options = [];
        this.formGroup.patchValue({ year: null });
        this.loadingYear = true;
        this.ws.emissionYears(params).subscribe((result: IResult) => {
            if (result.success) {
                result.data.map(res => {
                    this.fieldDataYear.options.push({ name: res, value: res });
                });
                this.loadingYear = false;
            } else {
                this.loadingYear = false;
                return null;
            }
        }, error => {
            this.loadingYear = false;
            return null;
        });
    }
    public getMonths() {
        const months = [
            { name: 'Enero', value: 1 },
            { name: 'Febrero', value: 2 },
            { name: 'Marzo', value: 3 },
            { name: 'Abril', value: 4 },
            { name: 'Mayo', value: 5 },
            { name: 'Junio', value: 6 },
            { name: 'Julio', value: 7 },
            { name: 'Agosto', value: 8 },
            { name: 'Septiembre', value: 9 },
            { name: 'Octubre', value: 10 },
            { name: 'Noviembre', value: 11 },
            { name: 'Diciembre', value: 12 }
        ];
        this.fieldDataMonth.options = [];
        this.formGroup.patchValue({ month: null });
        const year = this.formGroup.get('year').value;
        const yearnow = moment().format('YYYY');
        const monthnow = moment().format('M');
        if (year && year === parseInt(yearnow)) {
            for (let i = 0; i < parseInt(monthnow) - 1; i++) {
                this.fieldDataMonth.options.push(months[i]);
            }
        } else {
            this.fieldDataMonth.options = months;
        }
    }
    public getProducts() {
        const content = this.formGroup.get('content').value;
        this.fieldDataProducts.options = [];
        this.ws.getProductsFromCustomer().subscribe(result => {
            if (result.success) {
                this.productsFromTickets = result.data;
                this.formGroup.patchValue({ products: null });

                this.ws.getProductsFromContent(content).subscribe(result => {
                    if (result.success) {
                        if (result.data.length > 0) {
                            this.productsFromContents = result.data;
                            let products: any[] = [];
                            this.productsFromContents.map(product => {
                                products.push(this.productsFromTickets.filter(producto => producto.code === product.code)[0]);
                            });
                            let value: string[] = [];
                            value.push('0');
                            products.map(product => {
                                this.fieldDataProducts.options.push({ name: `${product.code} - ${product.name}`, value: product.code });
                                value.push(product.code);
                            });
                            this.formGroup.patchValue({ products: value });
                            this.debug.addInfo('WS.contentProductos', 'successful');
                        } else {
                            this.dialog.warning('Productos', 'No se han encontrado productos');
                        }
                    }
                    else {
                        this.debug.addError('WS.contentProductos', result.data);
                        return null;
                    }
                }, error => {
                    this.debug.addError('WS.contentProductos', error.message);
                    return null;
                });
                this.debug.addInfo('WS.ticketsProductos', 'successful');
            }
            else {
                this.debug.addError('WS.ticketsProductos', result.data);
                return null;
            }
        }, error => {
            this.debug.addError('WS.ticketsProductos', error.message);
            return null;
        });
    }

    onChangeCustomer() {
        this.getSites();
    }
    onChangeSite(event: Event) {
        if (this.formGroup.get('site').value !== 0) {
            this.showtab = false;
        }
    }

    onChangeBIPA(event: Event) {
        const values = this.formGroup.getRawValue();
        if (values.year && values.month && values.customer)
            this.getContents();
    }

    onChangeDates(event: Event) {
        const values = this.formGroup.getRawValue();
        if (values.year && values.month && values.customer) {
            this.getContents();
        } else if (values.year && values.customer) {
            this.getMonths();
        }
    }

    onChangeContent(product) {
        this.getProducts();
        const content = this.fieldDataContent.options.filter(cont => cont.idcontent === this.formGroup.get('content').value)[0];
        let date = moment(content.date_on).format('MM-DD');
        let initialDate: Moment = moment(content.date_on).subtract(1, 'years');
		if(this.me.customer.idcustomer === 31){
			initialDate = moment('2019-' + date);
		}
        this.formGroup.patchValue({ date: initialDate.toDate().toISOString() });
    }

    public changeTab(event: any) {
        this.tab = event.index;
        if (this.tab === 0) {
            this.formGroup.addControl('date', new FormControl(this.formBackup.date, Validators.required));
            this.formGroup.addControl('content', new FormControl(this.formBackup.content, Validators.required));
            this.formGroup.addControl('products', new FormControl(this.formBackup.products, Validators.required));
        } else {
            this.formBackup = this.formGroup.getRawValue();
            this.formGroup.removeControl('date');
            this.formGroup.removeControl('content');
            this.formGroup.removeControl('products');
        }
        this.formGroup.updateValueAndValidity();
    }
    public submitForm() {
        let data: Object;
        if (this.tab === 0) {
            let products = this.formGroup.get('products').value;
            let prod: string = '';
            prod = products.map(product => { return this.fieldDataProducts.options.filter(prod => prod.value === product) });
            data = {
                formData: this.formGroup.getRawValue(),
                content: this.fieldDataContent.options.filter(content => content.idcontent === this.formGroup.get('content').value),
                tab: this.tab,
                products: prod
            }
        } else {
            let formValues = this.formGroup.getRawValue();
            let months = [3,4,5];
            if(CONFIG.demo){
                formValues.year = 2020; 
                if(months.some(m => m === formValues.month)){
                    formValues.month = 12;
                }
            }
            data = {
                formData: formValues,
                tab: this.tab
            }
        }
        this.onSubmittedForm.emit(data);
    }
}