import { EmissionsSalesFormComponent } from './form/form.component'
import { EmissionsSalesResultAnalysisComponent } from './result-analysis/result-analysis.component';
import { EmissionsSalesResultSummaryComponent } from './result-summary/result-summary.component';
export const EMISSION_SALE_COMPONENTS = [
  EmissionsSalesFormComponent,
  EmissionsSalesResultAnalysisComponent,
  EmissionsSalesResultSummaryComponent
];
