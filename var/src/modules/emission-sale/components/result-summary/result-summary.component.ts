import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { Auth } from 'src/modules/auth/models/auth';
import { IResult } from 'src/models/iresult';
import {MatGridListModule} from '@angular/material/grid-list';

import { Moment } from 'moment';
import moment from 'moment';
import { User } from 'src/modules/user/models/user';
import { UserStore } from 'src/modules/user/stores/user.store';

import { ChartDataSets } from 'chart.js';
import { Color, SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'emissions-sales-result-summary',
  templateUrl: './view.component.html',
  styleUrls: ['./styles.component.scss']
})



export class EmissionsSalesResultSummaryComponent {
	@Input() data: any;
	@Output() onError: EventEmitter<any> = new EventEmitter<any>();
	@Output() onBackToForm:EventEmitter<any> = new EventEmitter(null);

	public meses:string[] = ['',
		'Enero', 
		'Febrero', 
		'Marzo', 
		'Abril', 
		'Mayo', 
		'Junio', 
		'Julio', 
		'Agosto', 
		'Septiembre', 
		'Octubre', 
		'Noviembre', 
		'Diciembre'];
		
	public resumeData:any[];
	public resumeGraph:any[];
	public impactoeuros: number = 0;
	public pesocamp: number = 0;
	public totalimpactomonetario: number = 0;
	public impactototal: number = 0;

	public impactoeurosString: string = '';
	public pesocampString: string = '';
	public totalimpactomonetarioString: string = '';
	public impactototalString: string = '';
	public isLoading:boolean = true;

	public months: string[] = [
		'MONTHS.jan', 
		'MONTHS.feb', 
		'MONTHS.mar', 
		'MONTHS.apr', 
		'MONTHS.may', 
		'MONTHS.jun', 
		'MONTHS.jul', 
		'MONTHS.aug', 
		'MONTHS.sep', 
		'MONTHS.oct', 
		'MONTHS.nov', 
		'MONTHS.dec'
	];
	ImpactoMonetarioLineChartData: ChartDataSets[] = [];
	ImpactoMonetarioLineChartLabels: Label[] = [];
	ImpactoPantallasBarChartData: ChartDataSets[] = [];
	ImpactoPantallasBarChartLabels: Label[] = [];
	public aspectRatio: boolean = false;
	public chartHeihgt: string = '500';

  	constructor(
		  private ws: WSService,
		  private dialog: DialogService,
		  private translate: TranslateService
	  ) {
  	}
  
  	public ngOnInit() {
		this.getSummary();
		this.translate.get(this.months).subscribe(translations => {
			this.ImpactoMonetarioLineChartLabels = Object.values(translations);
			this.ImpactoPantallasBarChartLabels = Object.values(translations);
		})
	}
	
	public getSummary(){
		this.ws.getImpactSummary(this.data.formData).subscribe(result => {
			if(result.success){
				this.resumeData = result.data.summary;
				this.resumeGraph = result.data.graph;
				this.handleResumeData();
				this.handleGraphData();
			}else{
				this.dialog.warning('COMPONENTS.emissions_sales.result_summary.get_summary.warning.title', result.data);
				this.onError.emit();
			}
		}, 
		error => {
			this.dialog.warning('COMPONENTS.emissions_sales.result_summary.get_summary.warning.title', 'COMPONENTS.emissions_sales.result_summary.get_summary.warning.text');
			this.onError.emit();
		})
	}

	public handleResumeData(){
		this.resumeData.map(data => {
			this.impactoeuros += parseFloat(data.impactoeuros);
			this.pesocamp += parseFloat(data.pesocamp);
			this.totalimpactomonetario += parseFloat(data.diff_eur);
			if(this.pesocamp !== 0) {
				this.impactototal = (this.totalimpactomonetario/this.pesocamp) * 100;
			}else{
				this.impactototal = 0;
			}
		});
		this.impactoeurosString = new Intl.NumberFormat("de-DE").format(this.impactoeuros);
		this.pesocampString = new Intl.NumberFormat("de-DE").format(this.pesocamp);
		this.totalimpactomonetarioString = new Intl.NumberFormat("de-DE").format(this.totalimpactomonetario);
		this.impactototalString = new Intl.NumberFormat("de-DE").format(this.impactototal);
	}
	public handleGraphData(){
		let dat_impactototal:any[] = [];
		let dat_impactoeuros:any[] = [];
		let dat_fechas:any[] = [];
		this.resumeGraph.map((value,index, array)=>{
			const month = this.meses[parseInt(value.mes)];
			const strdatetime = `${value.ejercicio} ${month}`;
			dat_fechas[index] = strdatetime;
			let tmpimpactototal = (value.impactoeuros/value.pesocamp) * 100;
			if(Number.isNaN(tmpimpactototal))
				tmpimpactototal = 0;
			dat_impactototal[index] = parseFloat(tmpimpactototal.toString()).toFixed(2);
			dat_impactoeuros[index] = parseFloat(value.impactoeuros).toFixed(2);

			/*if(value.mes < 3 || value.mes > 5){
				let tmpimpactototal = (value.impactoeuros/value.pesocamp) * 100;
				if(Number.isNaN(tmpimpactototal))
					tmpimpactototal = 0;
				dat_impactototal.push(index,parseFloat(tmpimpactototal.toFixed(2)));
			}else{
				dat_impactototal.push([index,0]);
			}*/
		});
		this.ImpactoMonetarioLineChartData = [{data:dat_impactoeuros, label: this.translate.instant('Impacto Monetario')}];
		this.ImpactoMonetarioLineChartLabels = dat_fechas;
		this.ImpactoPantallasBarChartData = [{barThickness: 16, data:dat_impactototal, label: this.translate.instant('% Impacto Pantallas')}];
		this.ImpactoPantallasBarChartLabels = dat_fechas;
		this.isLoading = false;
	}
	volverForm($event){
		this.onBackToForm.emit($event);
	}
}
