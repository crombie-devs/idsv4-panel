import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material/dialog';

import { ConfirmDialog } from 'src/modules/dialog/dialogs/confirm/confirm.dialog';
import { WarningDialog } from 'src/modules/dialog/dialogs/warning/warning.dialog';
import { FormatDialog } from 'src/modules/format/dialogs/format/format.dialog';
import { LocationDialog } from 'src/modules/location/dialogs/location/location.dialog';
import { TagCategorySelectDialog } from 'src/modules/tag/dialogs/tagcategoryselect/tagcategoryselect.dialog';
import { ProductSelectDialog } from 'src/modules/product/dialogs/productselect/productselect.dialog';
import { ProductCategoryFormDialog } from 'src/modules/product/dialogs/product-category-form/product-category-form.dialog';
import { ProductFormDialog } from 'src/modules/product/dialogs/product-form/product-form.dialog';
import { ContentPowerDialog } from 'src/modules/content/dialogs/power/power.dialog';
import { PrimetimeDialog } from 'src/modules/primetime/dialogs/primetime/primetime.dialog';

import { Tag } from 'src/modules/tag/models/tag';
import { Product } from 'src/modules/product/models/product';
import { ProductCategory } from 'src/modules/product/models/product-category';
import { TAGTYPE } from 'src/modules/tag/models/tagcategory';

@Injectable({
    providedIn: 'root',
})
export class DialogService {

    constructor(private dialog: MatDialog) { }

    public confirm(title: string, message: string, button: string = null): Observable<boolean> {
        let dialogRef: MatDialogRef<ConfirmDialog>;
        let config: MatDialogConfig = { panelClass: 'confirm-dialog-panel' };
        dialogRef = this.dialog.open(ConfirmDialog, config);
        dialogRef.componentInstance.title = title;
        dialogRef.componentInstance.message = message;
        dialogRef.componentInstance.button = button;
        return dialogRef.afterClosed();
    }

    public warning(title: string, message: string): Observable<boolean> {
        let dialogRef: MatDialogRef<WarningDialog>;
        let config: MatDialogConfig = { panelClass: 'warning-dialog-panel' };

        dialogRef = this.dialog.open(WarningDialog, config);
        dialogRef.componentInstance.title = title;
        dialogRef.componentInstance.message = message;

        return dialogRef.afterClosed();
    }

    //Dialog Format
    public format(title: string, message: string, button?: string | null): Observable<boolean> {

        let dialogRef: MatDialogRef<FormatDialog>;
        let config: MatDialogConfig = { panelClass: 'confirm-dialog-panel' };

        dialogRef = this.dialog.open(FormatDialog, config);
        dialogRef.componentInstance.title = title;
        dialogRef.componentInstance.message = message;
        dialogRef.componentInstance.button = button;

        return dialogRef.afterClosed();
    }

    //Location Format
    public location(title: string, message: string, data: any): Observable<boolean> {
        let dialogRef: MatDialogRef<LocationDialog>;
        let config: MatDialogConfig = { panelClass: 'confirm-dialog-panel', data: data };
        dialogRef = this.dialog.open(LocationDialog, config);
        dialogRef.componentInstance.title = title;
        dialogRef.componentInstance.message = message;

        return dialogRef.afterClosed();
    }

    public tagcategoryselect(title: string, selectedTags: Tag[], enableTags: number = 0, tagsType: string = TAGTYPE.difusion): MatDialogRef<TagCategorySelectDialog> {

        let dialogRef: MatDialogRef<TagCategorySelectDialog>;
        let config: MatDialogConfig = { panelClass: 'confirm-dialog-panel' };

        dialogRef = this.dialog.open(TagCategorySelectDialog, {
            data: { selectedTags: selectedTags, enableTags, tagsType },
            width: "60%",
            hasBackdrop: false,

        });
        dialogRef.componentInstance.title = title;

        return dialogRef;
    }

    public productselect(title: string, selectedProducts: Product[]): MatDialogRef<ProductSelectDialog> {

        let dialogRef: MatDialogRef<ProductSelectDialog>;
        let config: MatDialogConfig = { panelClass: 'confirm-dialog-panel' };

        dialogRef = this.dialog.open(ProductSelectDialog, {
            data: selectedProducts,
            width: "60%",
            hasBackdrop: false
        });
        dialogRef.componentInstance.title = title;

        return dialogRef;
    }

    public productform(title: string, product: Product): MatDialogRef<ProductFormDialog> {
        let dialogRef: MatDialogRef<ProductFormDialog>;
        let config: MatDialogConfig = { panelClass: 'confirm-dialog-panel' };
        dialogRef = this.dialog.open(ProductFormDialog, {
            data: product,
            width: "30%",
            hasBackdrop: false
        });
        dialogRef.componentInstance.title = title;
        return dialogRef;
    }

    public productcategoryform(title: string, category: ProductCategory): MatDialogRef<ProductCategoryFormDialog> {
        let dialogRef: MatDialogRef<ProductCategoryFormDialog>;
        dialogRef = this.dialog.open(ProductCategoryFormDialog, {
            data: category,
            width: "30%",
            hasBackdrop: false
        });
        dialogRef.componentInstance.title = title;
        return dialogRef;
    }

    public contentpower(title: string, selections): MatDialogRef<ContentPowerDialog> {

        let dialogRef: MatDialogRef<ContentPowerDialog>;
        let config: MatDialogConfig = { panelClass: 'confirm-dialog-panel' };

        dialogRef = this.dialog.open(ContentPowerDialog, {
            data: selections,
            width: "60%",
            hasBackdrop: false
        });
        dialogRef.componentInstance.title = title;

        return dialogRef;
    }

    public primetime(title: string, data): MatDialogRef<PrimetimeDialog> {

        let dialogRef: MatDialogRef<PrimetimeDialog>;
        let config: MatDialogConfig = { panelClass: 'confirm-dialog-panel' };

        dialogRef = this.dialog.open(PrimetimeDialog, {
            data: data,
            width: "60%",
            hasBackdrop: false
        });
        dialogRef.componentInstance.title = title;

        return dialogRef;
    }

}
