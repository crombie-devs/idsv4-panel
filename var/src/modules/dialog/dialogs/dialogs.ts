import { ConfirmDialog } from './confirm/confirm.dialog';
import { WarningDialog } from './warning/warning.dialog';

export const DIALOGS = [
	ConfirmDialog,
	WarningDialog
];