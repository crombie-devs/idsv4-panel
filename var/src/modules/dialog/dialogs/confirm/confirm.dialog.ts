import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
	selector: 'confirm-dialog',
  	templateUrl: './confirm.dialog.html',
  	styleUrls: ['./confirm.dialog.css']
})

export class ConfirmDialog {

	public title: string;
  public message: string;
  public button: string;

	constructor(public dialogRef: MatDialogRef<ConfirmDialog>) {
  }

  public save() {
    this.dialogRef.close(
      true
    );
  }

  public close() {
    this.dialogRef.close(
      false
    );
  }

}
