import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
	selector: 'warning-dialog',
  	templateUrl: './warning.dialog.html',
  	styleUrls: ['./warning.dialog.css']
})

export class WarningDialog {

	public title: string;
  public message: string;
  public label:string;

	constructor(public dialogRef: MatDialogRef<WarningDialog>) {
  }

  public close() {
    this.dialogRef.close(
      false
    );
  }

}
