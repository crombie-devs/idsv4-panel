import { Component, ViewChild } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { App } from 'src/modules/app/models/app';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Router } from '@angular/router';
import { MYString } from 'src/libs/string.libs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss']
})

export class AppListPage {

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
  
	public displayedColumns: string[] = ['select','idapp', 'name', 'apikey'];
	public selection = new SelectionModel<App>(true, []);
	public dataSource = new MatTableDataSource<App>();
	public new: boolean = false;
	public edit: boolean = false;
	public deleted: boolean = false;
	public me: any;	
	public permission: boolean = false;
	public load: boolean = true;
	sortedData: App[];
	apps: App[];

  	constructor(
		private ws: WSService,
		private debug: DebugStore,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private dialog: DialogService,
		private router: Router,
        private translate: TranslateService) {
			this.ustore.onChange.subscribe(user => { 
				this.me = user; 
			});
		}
  
  	public ngOnInit() {
		this.debug.addInfo('AppListComponent', 'OnInit');
		this.getApps();			
	}

	private getApps(){
		this.ws.apps().subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.apps', 'successful');
				this.permission = true;
				this.renderDataTable(result.data);
			}
			else {            
				this.debug.addError('WS.apps', result.data);
				this.load = false;
				return null;
			
			}
		}, error => {
			this.debug.addError('WS.apps', error.message);
			this.load = false; 
			return null;          
		}); 	
	}
	  
	public renderDataTable(data: App []) {
		this.dataSource = new MatTableDataSource(data);
		this.dataSource.filterPredicate = (data: App, filter: string): boolean => {
			const dataStr = MYString.removeAccents(Object.keys(data).reduce((currentTerm: string, key: string) => {
			  return (currentTerm + (data as { [key: string]: any })[key] + '◬');
			}, '')).trim().toLowerCase();	  
			return dataStr.indexOf(MYString.removeAccents(filter.trim().toLowerCase())) != -1;
		};
		this.dataSource.paginator = this.paginator;
		this.sortedData = data.slice();
		this.apps = data;
		this.dataSource.sort = this.sort;
		this.load = false;
		this.show();
	}

	public isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	public masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource.data.forEach(row => this.selection.select(row));
	}

	public checkboxLabel(row?: App): string {
		if (!row) {
		  return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
		}
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idapp + 1}`;
	}	

	public delete(){
		if(this.selection.selected.length == 0){
			this.dialog.warning(this.translate.instant('Aviso'), this.translate.instant('Debe seleccionar al menos una aplicación')).subscribe();
		}else{
			this.dialog.confirm(this.translate.instant('Confirmar Archivado'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
				if (confirm){
					this.selection.selected.forEach(element => {
						this.ws.deleteApp(element.idapp).subscribe((result: IResult) => {
							if(result.success) {
								this.debug.addInfo('WS.deleteApp', 'successful');
								this.openSnackBar('Aplicación borrada correctamente');
								this.getApps();
							}
							else {            
								this.debug.addError('WS.deleteApp', result.data);	
								this.openSnackBar(result.data);			
							}
						}, error => {
							this.debug.addError('WS.deleteUser', error.message);
							this.openSnackBar(error.message);	           
						}); 
					})			  
				}
			});
		}		
	}

	public update(){
		if (this.selection.selected.length>1){
			this.dialog.warning('Aviso', 'Sólo puede editar una aplicación').subscribe();
		}else if(this.selection.selected.length == 0){
			this.dialog.warning('Aviso', 'Debe seleccionar una aplicación').subscribe();
		}else{
			this.router.navigateByUrl('/private/app/'+this.selection.selected[0].idapp);
		}
	}

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

	public show(){		 
		this.me.permissions.forEach(element => {
			if(element.name == "app.put.*" || element.name == "*.*.*"){
			  this.new = true;
			}
			if(element.name == "app.delete.*" || element.name == "*.*.*"){
			  this.edit = true;
			}
			if(element.name == "app.post.*" || element.name == "*.*.*"){
			  this.deleted = true;
			}

		})
	}
	
	public applyFilter(event: string) {
		if(typeof event === 'string'){
			this.dataSource.filter = event;
		
			if (this.dataSource.paginator) {
				this.dataSource.paginator.firstPage();
			}
		}
	}

	ngAfterViewInit (){
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}

	sortData(sort: Sort) {
		
		const data = this.apps.slice();
		if (!sort.active || sort.direction === '') {
		  this.sortedData = data;
		  return;
		}
	
		this.sortedData = data.sort((a, b) => {
		  const isAsc = sort.direction === 'asc';
		  switch (sort.active) {
			case 'idapp': return this.compare(a.idapp, b.idapp, isAsc);
			case 'name': return this.compare(a.name, b.name, isAsc);
			case 'apikey': return this.compare(a.apikey, b.apikey, isAsc);			
			default: return 0;
		  }
		});
		this.dataSource.data = this.sortedData;
	}
	compare(a: number | string, b: number | string, isAsc: boolean) {
		return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
	}
}
