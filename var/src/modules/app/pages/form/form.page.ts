import { Component } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { Customer } from 'src/modules/customer/models/customer';
import { MatSnackBar } from '@angular/material/snack-bar';
import { App } from 'src/modules/app/models/app';

@Component({
  selector: 'app-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss']
})

export class AppFormPage {

	public id: number|null;
	public form: FormGroup;
	public name: FormControl = new FormControl('', [Validators.required]);
	public apikey: FormControl = new FormControl('', [Validators.required, Validators.email]);	
	public idcustomer: FormControl = new FormControl('');
	public customers: Customer[];
	public loading: boolean = false;
	public app: App;

   	constructor(
		private ws: WSService,
		private debug: DebugStore,
		private router: Router,
		private route: ActivatedRoute,
		private fb: FormBuilder,
		private _snackBar: MatSnackBar) {
			this.id = Number(this.route.snapshot.paramMap.get('idapp'));
  	}
  
  	public ngOnInit() {
		this.debug.addInfo('AppFormComponent', 'OnInit');
		this.initForm(null);
		this.getCustomer();	
		if (this.id){
			this.ws.getApp(this.id).subscribe((result: IResult) => {
				if(result.success) {
					this.debug.addInfo('WS.getApp', 'successful');
					this.app=result.data;					
					this.initForm(result.data);	
				}
				else {            
					this.debug.addError('WS.getApp', result.data);
				}
			}, error => {
				this.debug.addError('WS.getApp', error.message);  
				  
			}); 
		}
	}  

	private initForm(data: App|null){		
		if (!data){
			this.form = this.fb.group({
				name: this.name,
				apikey: this.apikey,				
				idcustomer: this.idcustomer
			});
		  }else{	
			let c = data.customer ? data.customer.idcustomer : '';	
			this.form = this.fb.group({
				name: data.name,
				apikey: data.apikey,
				idcustomer: c		
			});
		  } 
	}	

	private getCustomer(){
		this.ws.customers().subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.customers', 'successful');				
				this.customers = result.data;
			}
			else {            
				this.debug.addError('WS.customers', result.data);
			
			}
		}, error => {
			this.debug.addError('WS.customers', error.message);            
		}); 
	}	

	public create(){
		if(this.form.invalid)
			return null;
		this.loading = true;
		this.ws.createApp(this.form.value).subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.createApp', 'successful');
				this.openSnackBar("Aplicación creada correctamente");
				this.router.navigateByUrl('/private/apps');
				this.loading = false;
			}
			else {            
				this.debug.addError('WS.createApp', result.data);
				this.openSnackBar(result.data);
				this.loading = false;			
			}
		}, error => {
			let msj = JSON.stringify(error.error.data);
			this.openSnackBar(msj.substr(2,msj.length-3)); 
			this.debug.addError('WS.createApp', error.message);
			this.loading = false;          
		}); 
	}

	public update(){
		if (this.form.invalid) return null;
		this.loading = true;
		this.ws.updateApp(this.id, this.form.value).subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.updateApp', 'successful');
				this.openSnackBar("Aplicación actualizada correctamente");
				this.router.navigateByUrl('/private/apps');
				this.loading = false;
			}
			else {            
				this.debug.addError('WS.updateApp', result.data);
				this.openSnackBar(result.data);
				this.loading = false;
			
			}
		}, error => {
			let msj = JSON.stringify(error.error.data);
			this.openSnackBar(msj.substr(2,msj.length-3)); 
			this.debug.addError('WS.updateApp', error.message);  
			this.loading = false;          
		}); 
	}

	public compareCustomer(x: Customer, y: Customer): boolean {
		return x && y ? x.idcustomer === y.idcustomer : x === y;
	}	

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}	
}
