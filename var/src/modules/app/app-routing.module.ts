import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppFormPage } from 'src/modules/app/pages/form/form.page';
import { AppListPage } from 'src/modules/app/pages/list/list.page';

const routes: Routes = [
  { path: 'list', component: AppListPage, data: { breadcrumb: 'Aplicación' }},
  { path: 'edit/:idapp', component: AppFormPage, data: { breadcrumb: 'Editar Aplicación' }},
  { path: 'create', component: AppFormPage, data: { breadcrumb: 'Nueva Aplicación' }},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
