import { Customer } from 'src/modules/customer/models/customer';

export class App {
  idapp: number;
  name: string;
  apikey: string; 
  customer: Customer;
  status: boolean;

	constructor(obj?: any) {
		if(!obj)
      return;

    this.idapp = obj.idapp;
    this.name = obj.name;
    this.apikey = obj.apikey; 
    this.customer = obj.customer; 
    this.status = obj.status;   
  }
}
