import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/shared/shared.module';

import { FlexModule } from '@angular/flex-layout';

import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DirectivesModule } from 'src/directive/directives.module';
import { MaterialModule } from 'src/app/material.module';
import { AppRoutingModule } from './app-routing.module';
import { MenuModule } from 'src/modules/menu/menu.module';

import { AppFormPage } from 'src/modules/app/pages/form/form.page';
import { AppListPage } from 'src/modules/app/pages/list/list.page';

@NgModule({
  declarations: [AppFormPage, AppListPage],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule,
    DirectivesModule,
    AppRoutingModule,
    NgxMatFileInputModule,
    MenuModule
  ],
  providers: []
})
export class AppModule {}