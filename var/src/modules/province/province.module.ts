import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PROVINCE_COMPONENTS } from './components/components';
import { ProvinceStore } from './stores/province.store';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/shared/shared.module';


@NgModule({
  declarations: [PROVINCE_COMPONENTS],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule
  ],
  providers: [ProvinceStore],
  exports: [PROVINCE_COMPONENTS]
})
export class ProvinceModule {}