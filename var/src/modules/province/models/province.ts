export interface IProvince {
    idprovince: number;
    idcountry: number;
    name: string;
}
  
export class Province implements IProvince {
  
    idprovince: number;
    idcountry: number;
    name: string;
  
    constructor(obj?: IProvince) {
      if(!obj)
        return;
      this.idprovince = obj.idprovince;
      this.idcountry = obj.idcountry;
      this.name = obj.name;   
    }
  
}