import { Component, OnInit, Input,Output, EventEmitter, SimpleChanges } from '@angular/core';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Router } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { IResult } from 'src/models/iresult';
import { DebugStore } from 'src/stores/debug.store';
import { FormControl, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'province-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class ProvinceSelectComponent implements OnInit {

  
@Output() provincesSelected = new EventEmitter<number>();
@Input() provinceAvailable:boolean;
@Input() idCountry:number;

public provinces:any[] = [];
public filterForm:FormGroup;
public provincias = new FormControl();

  constructor(
    private dialog: DialogService,
		private router: Router,
		private ws: WSService,
		private debug: DebugStore,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private formBuilder:FormBuilder,
    ) {
    }

  ngOnInit(): void {
  }
  
  ngOnChanges(changes: SimpleChanges) {
    //this.getProvinces();
    if(changes.provinceAvailable.currentValue){
    	this.getProvinces()
    	
    }
  }
  
  private getProvinces(){
    this.ws.provincesToSelectById(this.idCountry).subscribe((result: IResult) => {
      if(result.success) {
				this.debug.addInfo('WS.provinces', 'successful');				
				this.provinces = result.data;
			}
			else {            
				this.debug.addError('WS.provinces', result.data);
				this.openSnackBar(result.data);
			}
		}, error => {
			this.debug.addError('WS.provinces', error.message);
			//this.openSnackBar(error.message);
		}); 
		
		
  }
  
  public captureValue() {
		let provincias = this.provincias.value;
		this.provincesSelected.emit(provincias);
		
  }
	
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
	
	
	
	

}
