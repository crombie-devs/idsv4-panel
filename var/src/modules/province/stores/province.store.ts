import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IProvince, Province } from '../models/province';
import { WSService } from '../../../services/ws.service';
import { IResult } from '../../../models/iresult';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'provinces.store.ids';

@Injectable()
export class ProvinceStore {

    public onChange: BehaviorSubject<Province[]> = new BehaviorSubject([]);
    private data: Province[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = localStorage.getItem(STORE_KEY);
            if(store) {
                const provinces: IProvince[] = JSON.parse(store); 
                this.set(provinces);
            } else 
                this.load();
        } else
            this.load();
    }

    public get(): Province[] {
        return this.data;
    }

    public set(provinces: IProvince[]) {
        this.save(provinces);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.provincesToSelect().subscribe((result: IResult) => {
            if(result.success)
              this.set(result.data);
        });
    }

    private save(provinces: IProvince[]) {
        this.data = provinces.map(s => new Province(s));
        if(environment.localstorage)
            localStorage.setItem(STORE_KEY, JSON.stringify(provinces));
    }

    public countryFilter(id : number): Province[]{
        return this.data.filter(p => p.idcountry === id);

    }

    private filterByCountry (idcountry: number){        
        return this.data.filter(p=> p.idcountry === idcountry);
    }

}