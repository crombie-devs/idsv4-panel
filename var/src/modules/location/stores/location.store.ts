import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { ILocation, Location } from 'src/modules/location/models/location';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'location.store.ids';

@Injectable()
export class LocationStore {

    public onChange: BehaviorSubject<Location[]> = new BehaviorSubject([]);
    private data: Location[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const locations: ILocation[] = JSON.parse(store); 
                this.set(locations);
            } else 
                this.load();
        }else
            this.load();
    }

    public get(): Location[] {
        return this.data;
    }

    public set(locations: ILocation[]) {
        this.save(locations);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.locationsToSelect().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }
        });
    }

    private save(formats: ILocation[]) {
        this.data = formats.map(s => new Location(s));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(formats));
    }
}