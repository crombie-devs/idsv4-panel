import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/shared/shared.module';

import { IndoorlocationElement } from './components/indoor-location/element/indoorlocation.element';
import { LocationsComponent } from './components/locations/locations.component';
import { LocationDialog } from './dialogs/location/location.dialog';
import { LocationsDialog } from './dialogs/locations/locations.dialog';
import { LocationStore } from './stores/location.store';
import { MaterialModule } from 'src/app/material.module';

@NgModule({
  declarations: [IndoorlocationElement, LocationsComponent, LocationDialog, LocationsDialog],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedModule
  ],
  entryComponents:[LocationDialog, LocationsDialog],
  providers: [LocationStore],
  exports:[IndoorlocationElement, LocationsComponent]
})
export class LocationModule {}
