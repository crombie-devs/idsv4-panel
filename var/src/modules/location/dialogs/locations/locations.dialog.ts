import { TranslateService } from '@ngx-translate/core';
import { Component, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { Country } from 'src/modules/country/models/country';
import { Site } from 'src/modules/site/models/site';
import { City } from 'src/modules/city/models/city';
import { Province } from 'src/modules/province/models/province';
import { CountryStore } from 'src/modules/country/stores/country.store';
import { ProvinceStore } from 'src/modules/province/stores/province.store';
import { CityStore } from 'src/modules/city/stores/city.store';
import { SiteStore } from 'src/modules/site/stores/site.store';
import { MatSelect } from '@angular/material/select';
import { MatOption } from '@angular/material/core';
import { MYString } from 'src/libs/string.libs';

@Component({
    selector: 'locations-dialog',
    templateUrl: './locations.dialog.html',
    styleUrls: ['./locations.dialog.scss']
})

export class LocationsDialog {
    public rastroSeleccion: any[] = [];
    public title: string = this.translate.instant('Editar Localización');
    public disabledProvince: boolean = true;
    public disabledCity: boolean = true;
    public playcircuit: PlayCircuit;

    public countries: Country[] = [];
    public provinces: Province[] = [];
    public cities: City[] = [];
    public sites: Site[] = [];
    public selectedSites: Site[] = [];

    public idCountry: number;
    public idProvince: number;

    public country = new FormControl();
    public site = new FormControl();
    public city = new FormControl();
    public province = new FormControl();

    public idcustomer: number | null;

    @ViewChild('select') select: MatSelect;

    allSelected = false;
    constructor(
        public dialogRef: MatDialogRef<LocationsDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _snackBar: MatSnackBar,
        private translate: TranslateService,
        private countryStore: CountryStore,
        private provinceStore: ProvinceStore,
        private cityStore: CityStore,
        private siteStore: SiteStore) {
        this.playcircuit = new PlayCircuit(this.data.playcircuit);
        this.idcustomer = this.data.customer;
        this.siteStore.reload();
        this.siteStore.onChange.subscribe(result => {
            if (result) {
                this.sites = result;
                this.getSites();
            }
        })
        this.countryStore.onChange.subscribe(result => {
            this.getCountries();
        })
    }

    public ngOnInit() {
        this.initLocationValues();
    }

    initLocationValues() {
        let value = [];
        if (this.playcircuit.sites && this.playcircuit.sites.length > 0) {
            this.playcircuit.sites.map(site => {
                value.push(site.idsite);
            });
            this.site.setValue(value);
        }

        if (this.playcircuit.countries && this.playcircuit.countries.length > 0) {
            value = [];
            this.playcircuit.countries.map(country => {
                value.push(country.idcountry);
            });
            this.country.setValue(value);
            this.selectionCountries();
        }

        if (this.playcircuit.provinces && this.playcircuit.provinces.length > 0) {
            value = [];
            this.playcircuit.provinces.map(province => {
                value.push(province.idprovince);
            });
            this.province.setValue(value);
            this.selectionProvinces();
        }

        if (this.playcircuit.cities && this.playcircuit.cities.length > 0) {
            value = [];
            this.playcircuit.cities.map(city => {
                value.push(city.idcity);
            });
            this.city.setValue(value);
        }
        this.fillRastroSeleccion()
    }
    public save() {
        if ((!this.site.value || this.site.value.length === 0) && (!this.country.value || this.country.value.length === 0)) {
            this.openSnackBar(this.translate.instant('Tienes que elegir al menos un centro o un país'));
            return;
        }
        let sites: any[];
        let countries: any[];
        let provinces: any[];
        let cities: any[];
        this.playcircuit.sites = null;
        this.playcircuit.countries = null;
        this.playcircuit.provinces = null;
        this.playcircuit.cities = null;
        if (this.site.value) {
            sites = this.sites.filter(site => this.site.value.includes(site.idsite));
            this.playcircuit.sites = sites;
        }
        if (this.country.value) {
            countries = this.countries.filter(country => this.country.value.includes(country.idcountry));
            this.playcircuit.countries = countries;
        }
        if (this.province.value) {
            provinces = this.provinces.filter(province => this.province.value.includes(province.idprovince))
            this.playcircuit.provinces = provinces;
        }
        if (this.city.value) {
            cities = this.cities.filter(city => this.city.value.includes(city.idcity));
            this.playcircuit.cities = cities;
        }
        this.dialogRef.close(this.playcircuit);

        this.siteStore.reload();
    }

    public close() {
        this.dialogRef.close(null);
    }

    private getSites() {
        if (this.idcustomer && this.idcustomer !== 0)
            this.sites = this.siteStore.filterCustomer(this.idcustomer);
        this.selectedSites = this.sites;
    }

    private getCountries() {
        this.countries = this.countryStore.get();
    }

    private getProvinces() {

        if (this.playcircuit.countries && this.playcircuit.countries.length > 0) {
            this.provinces = [];
            this.playcircuit.countries.map(country => {
                let provs = this.provinceStore.get().filter(p => p.idcountry === country.idcountry);
                provs.map(p => this.provinces.push(p))
            })
        } else {
            this.provinces = this.provinceStore.get();
        }
    }

    private getCities() {
        if (this.playcircuit.provinces && this.playcircuit.provinces.length > 0) {
            this.cities = [];
            this.playcircuit.provinces.map(province => {
                let cits = this.cityStore.get().filter(c => c.idprovince === province.idprovince);
                cits.map(c => this.cities.push(c));
            });
        } else if (this.playcircuit.countries && this.playcircuit.countries.length > 0) {
            this.provinces.map(province => {
                let cits = this.cityStore.get().filter(c => c.idprovince === province.idprovince);
                cits.map(c => this.cities.push(c));
            })
        } else {
            this.cities = this.cityStore.get();
        }
    }

    public selectionCountries() {
        this.fillRastroSeleccion();
        if (this.country.value.length > 0) {
            let countries: any[] = this.countries.filter(country => this.country.value.includes(country.idcountry));
            this.playcircuit.countries = countries;
            this.getProvinces();
            this.disabledProvince = false;
        } else {
            this.province.patchValue(null);
            this.city.patchValue(null);
            this.provinces = [];
            this.cities = [];
            this.disabledProvince = true;
            this.disabledCity = true;
            this.rastroSeleccion = [];
        }
    }

    public selectionProvinces() {
        this.fillRastroSeleccion();
        if (this.province.value.length > 0) {
            let provinces: any[] = this.provinces.filter(province => this.province.value.includes(province.idprovince));
            this.playcircuit.provinces = provinces;
            this.getCities();
            this.disabledCity = false;
        } else {
            this.city.patchValue(null);
            this.cities = [];
            this.disabledCity = true;
        }
    }
    public selectionCities() {
        this.fillRastroSeleccion();
    }

    public fillRastroSeleccion() {
        this.rastroSeleccion = [];
        if (this.country.value && this.country.value.length > 0) {
            this.country.value.map(c => {
                let country = this.countries.filter(co => co.idcountry === c);
                this.rastroSeleccion.push({ country: country[0].name });
                if (this.province.value && this.province.value.length > 0) {
                    this.province.value.map(p => {
                        let province = this.provinces.filter(pr => pr.idprovince === p);
                        if (country[0].idcountry === province[0].idcountry) {
                            this.rastroSeleccion.map((rs, index) => {
                                if (rs.country === country[0].name) {
                                    if (!this.rastroSeleccion[index].provinces)
                                        this.rastroSeleccion[index].provinces = [];
                                    this.rastroSeleccion[index].provinces.push({ province: province[0].name });
                                }
                            })
                        }
                        if (this.city.value && this.city.value.length > 0) {
                            this.city.value.map(ci => {
                                let city = this.cities.filter(cit => cit.idcity === ci);
                                if (province[0].idprovince === city[0].idprovince) {
                                    this.rastroSeleccion.map((rs, i) => {
                                        if (rs.country === country[0].name) {
                                            if (rs.provinces)
                                                rs.provinces.map((rsp, i2) => {
                                                    if (rsp.province === province[0].name) {
                                                        if (!this.rastroSeleccion[i].provinces[i2].cities)
                                                            this.rastroSeleccion[i].provinces[i2].cities = [];
                                                        this.rastroSeleccion[i].provinces[i2].cities.push({ city: city[0].name });
                                                    }
                                                })
                                        }
                                    })
                                }
                            })
                        }
                    });
                }
            })
        }
    }
    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }

    toggleAllSelection() {
        if (this.allSelected) {
            this.select.options.forEach((item: MatOption) => item.select());
        } else {
            this.select.options.forEach((item: MatOption) => item.deselect());
        }
    }

    optionClick() {
        let newStatus = true;
        this.select.options.forEach((item: MatOption) => {
            if (!item.selected)
                newStatus = false;
        });
        this.allSelected = newStatus;
    }

    public classHide(name: string, value: string) {
        return MYString.removeAccents(name).toLowerCase().startsWith(MYString.removeAccents(value).toLowerCase())
    }

}
