import { Component, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../../../user/models/user';
import { Location } from '../../models/location';
import { UserStore } from '../../../user/stores/user.store';


@Component({
	selector: 'location-dialog',
  	templateUrl: './location.dialog.html',
  	styleUrls: ['./location.dialog.css']
})

export class LocationDialog {
	public title: string;
  public message: string;
  public button: string;
  public me: User;
  public form: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    idcustomer: new FormControl('', [Validators.required])
  });  

	constructor(
    @Inject(MAT_DIALOG_DATA) public data: Location,
	  public dialogRef: MatDialogRef<LocationDialog>,
    private fb: FormBuilder,
    private ustore: UserStore) {
  }

public ngOnInit() {
  this.ustore.onChange.subscribe(user => {
    this.me = user;
    this.initForm(this.data);
  });
}

  public save(formValue:any) {
    this.dialogRef.close(formValue);
  }

  public close() {
    this.dialogRef.close(false);
  }
  
  private initForm(data: Location){
    if(data)
      if(data.name)
        this.form.patchValue({idcustomer: data.idcustomer, name: data.name});
      else
        this.form.patchValue({idcustomer:this.me.customer ? this.me.customer.idcustomer : data.idcustomer});
    else
      this.form.patchValue({idcustomer: this.me.customer.idcustomer});
	}	

}
