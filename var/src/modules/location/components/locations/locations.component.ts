import { Component,  Input, Output, EventEmitter } from '@angular/core';
import { DebugStore } from 'src/stores/debug.store';

import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { MatDialog } from '@angular/material/dialog';

import { LocationsDialog } from 'src/modules/location/dialogs/locations/locations.dialog';

@Component({
  selector: 'locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent {
  
  @Output() onSave = new EventEmitter<PlayCircuit>();

  @Input() playcircuit: PlayCircuit = new PlayCircuit(null);
  @Input() idcustomer: number = null;

  constructor(
    private debug: DebugStore,
		private matDialog: MatDialog) { }
 
  public openDialog() {
    const dialogRef = this.matDialog.open(LocationsDialog, {data: {playcircuit:this.playcircuit, customer:this.idcustomer}});
    
    dialogRef.afterClosed().subscribe((result: PlayCircuit|null) => {
      if (result)
  	    this.onSave.emit(result)
  	})
  } 

}
