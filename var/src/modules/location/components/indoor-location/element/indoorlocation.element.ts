import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { MatSnackBar } from '@angular/material/snack-bar';
import { WSService } from 'src/services/ws.service';
import { MatDialog } from '@angular/material/dialog';
import { Location } from 'src/modules/location/models/location';
import { IndoorlocationDialog } from 'src/modules/indoor-location/dialogs/indoorlocation/indoorlocation.dialog';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'indoorlocation-element',
  templateUrl: './indoorlocation.element.html',
  styleUrls: ['./indoorlocation.element.scss']
})

export class IndoorlocationElement {

  
	@Input() indoorlocation:Location;
	@Input() editIndoorlocation: boolean;
	@Input() deletedIndoorlocation: boolean;
	@Output() onDeleted = new EventEmitter<number>();
	@Output() onUpdate = new EventEmitter<number>();

  	constructor(
  		private ws: WSService,
    	private dialog: DialogService,
		private debug: DebugStore,
		private _snackBar: MatSnackBar,
		public matDialog: MatDialog,
		private translate: TranslateService
		
    ) {}  
  
    public update(idlocation:number){
		if (idlocation==0){
			this.dialog.warning('DIALOGS.warning.title', 'COMPONENTS.indoor_location.element.update.error').subscribe();
		}else{
			let dialogRef;
			dialogRef = this.matDialog.open(IndoorlocationDialog, {
				data: this.indoorlocation
			});
			dialogRef.afterClosed().subscribe(result => {
				if (!result)
					return;
				if (result.success){
					this.onUpdate.emit();
					this.openSnackBar(this.translate.instant('COMPONENTS.indoor_location.element.update.success'));
				}
				else
					this.openSnackBar(this.translate.instant('COMPONENTS.indoor_location.element.update.error2'));	
			});			
		}
	}
	
	public delete(idlocation:number){
		if(idlocation == 0){
			this.dialog.warning('DIALOGS.warning.title','COMPONENTS.indoor_location.element.warning.text').subscribe();
		}else{
			this.dialog.confirm('COMPONENTS.indoor_location.title','COMPONENTS.indoor_location.confirm.text').subscribe(confirm => {
				if (confirm){
						this.ws.deleteIndoorLocation(idlocation).subscribe((result: IResult) => {
							if(result.success) {
								this.onDeleted.emit();
								this.debug.addInfo('WS.deleteIndoorLocation', 'successful');
								this.openSnackBar(this.translate.instant('Localización eliminada correctamente'));
							}
							else {            
								this.debug.addError('WS.deleteIndoorLocation', result.data);	
								this.openSnackBar(result.data);			
							}
						}, error => {
							this.debug.addError('WS.deleteIndoorLocation', error.message);
							this.openSnackBar(error.message);	           
						});
						
				}
			});
		}		
	}
	
	public openSnackBar(message: string) {
		const mess:string = message.toString();
		this._snackBar.open(mess,'', {
		  duration: 1500,
		});
	}
}
