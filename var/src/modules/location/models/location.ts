export interface ILocation {
    idlocation: number;
    idcustomer: number
    name: string;
}

export class Location implements ILocation {
    idlocation: number;
    idcustomer: number
    name: string;
    
    constructor(obj?: any) {
        if(!obj)
            return;
            
        this.idlocation = obj.idlocation;
        this.idcustomer = obj.idcustomer;
        this.name = obj.name;
    }
}