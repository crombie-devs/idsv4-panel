import { Component, OnInit, ViewChild, Input, SimpleChanges, ElementRef } from '@angular/core';
import {ChangeDetectorRef } from '@angular/core';
// For graphics
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, SingleDataSet, Label, BaseChartDirective } from 'ng2-charts';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'multi-linear-chart',
  templateUrl: './multi-linear-chart.component.html',
  styleUrls: ['./multi-linear-chart.component.scss']
})

export class MultiLinearChartComponent {
  public load: boolean = true;
  public url: string = '';
  public frame: any;
  @Input() data: ChartDataSets[] = [];
  @Input() labels: Label[];
  @Input() loading: boolean = true;
  @Input() aspectRatio: boolean = false;
  @Input() height: string = '250';

  lineChartOptions = {
    legend: {
      labels: {
        fontFamily: '"Poppins","Roboto", sans-serif',
        fontSize:12,
      }
    },
    responsive: true,
    maintainAspectRatio: this.aspectRatio,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{
        gridLines:{
          color:'#fff'
        },
        ticks: {
          autoSkip: true,
          maxTicksLimit: 10
        }
      }],
      yAxes: [
        {
          id: 'y-axis-1',
          scaleLabel: {
            display: true,
            labelString: ''
          },
          ticks: {
            beginAtZero: true,
            callback: function(value) {if (value % 1 === 0) {return value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");;}}
          },
          position: 'left',
          gridLines:{
            borderDash: [8, 4],
          }
        },
        {
          id: 'y-axis-2',
          scaleLabel: {
            display: true,
            labelString:''
          },
          ticks: {
            beginAtZero: true,
            callback: function(value) {if (value && value % 1 === 0) {return value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");;}}
          },
          position: 'right',
          gridLines:{
            color:'#fff',
            borderDash: [8, 4],
          }
        }
      ]
    },
  };
  lineChartColors
  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType: ChartType = 'line';
  gradient1; gradient2; gradient3; gradient4;
   
  @ViewChild('chart', {static: false}) set content(elementRef: ElementRef<HTMLCanvasElement>) {
    if(elementRef) {
      const color1 = '#00CCFF';
      const bgColor1 = 'rgba(0,204,255,0.5)';
      const color2 = '#448AFF';
      const bgColor2 = 'rgba(68,138,255,0.5)';
      const color3 = '#7C4DFF';
      const bgColor3 = 'rgba(124,77,255,0.5)';
      const color4 = '#FF6666';
      const bgColor4 = 'rgba(255,102,102,0.5)';
      const color5 = '#E07A59';
      const bgColor5 = 'rgba(244,122,89,0.5)';
      const color6 = '#C18E4C';
      const bgColor6 = 'rgba(193,142,76,0.5)';
      const color7 = '#A2A240';
      const bgColor7 = 'rgba(162,162,64,0.5)';
      const color8 = '#83B633';
      const bgColor8 = 'rgba(131,182,51,0.5)';
  
      this.lineChartColors = [
        {
          borderColor: color1,
          backgroundColor:bgColor1,
          pointBorderColor: '#fff',
          pointBackgroundColor: color1,
          pointHoverBorderColor: color1
        },
        {
          borderColor: color2,
          backgroundColor:bgColor2,
          pointBorderColor: '#fff',
          pointBackgroundColor: color2,
          pointHoverBorderColor: color2
        },
        {
          borderColor: color3,
          backgroundColor:bgColor3,
          pointBorderColor: '#fff',
          pointBackgroundColor: color3,
          pointHoverBorderColor: color3
        },
        {
          borderColor: color4,
          backgroundColor:bgColor4,
          pointBorderColor: '#fff',
          pointBackgroundColor: color4,
          pointHoverBorderColor: color4
        },
        {
          borderColor: color5,
          backgroundColor:bgColor5,
          pointBorderColor: '#fff',
          pointBackgroundColor: color5,
          pointHoverBorderColor: color5
        },
        {
          borderColor: color6,
          backgroundColor:bgColor6,
          pointBorderColor: '#fff',
          pointBackgroundColor: color6,
          pointHoverBorderColor: color6
        },
        {
          borderColor: color7,
          backgroundColor:bgColor7,
          pointBorderColor: '#fff',
          pointBackgroundColor: color7,
          pointHoverBorderColor: color7
        },
        {
          borderColor: color8,
          backgroundColor:bgColor8,
          pointBorderColor: '#fff',
          pointBackgroundColor: color8,
          pointHoverBorderColor: color8
        }
      ];
    }
  }

  constructor(
    private translate: TranslateService,
    private cdref: ChangeDetectorRef) {
    }
  
    public panelOpenState = false;
    public status: boolean = false;
    public me: any;
    public titleBreadcrumbs: string;

    public step = 0;

    ngAfterContentChecked() {
      if(this.data.length > 0){
        this.lineChartOptions.scales.yAxes[0].scaleLabel.labelString = this.translate.instant(this.data[0].label);
        if(this.data[1])
          this.lineChartOptions.scales.yAxes[1].scaleLabel.labelString = this.translate.instant(this.data[1].label);
      }
      this.cdref.detectChanges();
    }
}

