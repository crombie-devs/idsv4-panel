import { ChangeDetectorRef } from '@angular/core';
import { Component, OnInit, ViewChild, Input, SimpleChanges, ElementRef } from '@angular/core';
// For graphics
import { ChartDataSets, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'linear-chart',
  templateUrl: './linear-chart.component.html',
  styleUrls: ['./linear-chart.component.scss']
})

export class LinearChartComponent {
  public load: boolean = true;
  public url: string = '';
  public frame: any;
  public heightNumber: Number = window.innerHeight*0.3;
  @Input() data: ChartDataSets[] = [];
  @Input() labels: Label[];
  @Input() loading: boolean = true;
  @Input() aspectRatio: boolean = false;
  @Input() height: string = String(this.heightNumber);
  @Input() beginAtZero: boolean = true;

  lineChartOptions = {
    legend: {
      labels: {
        fontFamily: '"Poppins","Roboto", sans-serif',
        fontSize:12,
      }
    },
    responsive: true,
    maintainAspectRatio: this.aspectRatio,
    scales: {
      xAxes: [{
        gridLines:{
          color:'#fff'
        }
      }],
      yAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: ''
          },
          ticks: {
            autoSkip: true,
            maxTicksLimit: 5,
            beginAtZero: this.beginAtZero
          },
          position: 'left',
          gridLines:{
            borderDash: [8, 4],
          }
        },
      ]
    },
  };
  lineChartColors;
  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType: ChartType = 'line';
  
 
  @ViewChild('chart', {static: false}) set content(elementRef: ElementRef<HTMLCanvasElement>) {
    if(elementRef) {
      const color1 = '#00CCFF';
      const bgColor1 = 'rgba(0,204,255,0.5)';
      const color2 = '#448AFF';
      const bgColor2 = 'rgba(68,138,255,0.5)';
      const color3 = '#7C4DFF';
      const bgColor3 = 'rgba(124,77,255,0.5)';
      const color4 = '#FF6666';
      const bgColor4 = 'rgba(255,102,102,0.5)';
      const color5 = '#E07A59';
      const bgColor5 = 'rgba(244,122,89,0.5)';
      const color6 = '#C18E4C';
      const bgColor6 = 'rgba(193,142,76,0.5)';
      const color7 = '#A2A240';
      const bgColor7 = 'rgba(162,162,64,0.5)';
      const color8 = '#83B633';
      const bgColor8 = 'rgba(131,182,51,0.5)';
      this.lineChartOptions.scales.yAxes[0].ticks.beginAtZero = this.beginAtZero;
      this.lineChartColors = [
        {
          borderColor: color1,
          pointBorderColor: '#fff',
          backgroundColor:bgColor1,
          pointBackgroundColor: color1,
          pointHoverBorderColor: color1
        },
        {
          borderColor: color2,
          pointBorderColor: '#fff',
          backgroundColor:bgColor2,
          pointBackgroundColor: color2,
          pointHoverBorderColor: color2
        },
        {
          borderColor: color3,
          pointBorderColor: '#fff',
          backgroundColor:bgColor3,
          pointBackgroundColor: color3,
          pointHoverBorderColor: color3
        },
        {
          borderColor: color4,
          pointBorderColor: '#fff',
          backgroundColor:bgColor4,
          pointBackgroundColor: color4,
          pointHoverBorderColor: color4
        },
        {
          borderColor: color5,
          pointBorderColor: '#fff',
          backgroundColor:bgColor5,
          pointBackgroundColor: color5,
          pointHoverBorderColor: color5
        },
        {
          borderColor: color6,
          pointBorderColor: '#fff',
          backgroundColor:bgColor6,
          pointBackgroundColor: color6,
          pointHoverBorderColor: color6
        },
        {
          borderColor: color7,
          pointBorderColor: '#fff',
          backgroundColor:bgColor7,
          pointBackgroundColor: color7,
          pointHoverBorderColor: color7
        },
        {
          borderColor: color8,
          pointBorderColor: '#fff',
          backgroundColor:bgColor8,
          pointBackgroundColor: color8,
          pointHoverBorderColor: color8
        }
      ];
    }
  }

  constructor(private cdr: ChangeDetectorRef) { }

  public panelOpenState = false;
  public status: boolean = false;
  public me: any;
  public titleBreadcrumbs: string;
  public step = 0;

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }

}

