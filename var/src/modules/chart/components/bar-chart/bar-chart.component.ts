import { Component, OnInit, ViewChild, Input, SimpleChanges, ElementRef } from '@angular/core';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { User } from 'src/modules/user/models/user';
import { UserStore } from 'src/modules/user/stores/user.store';
import moment from 'moment';
// For graphics
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, SingleDataSet, Label, BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})

export class BarChartComponent implements OnInit {
  @Input() data: ChartDataSets[];
  @Input() labels: Label[];
  @Input() loading: boolean = true;
  @Input() height: string = '350';

  ChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales: { xAxes: [{
      gridLines:{
        color:'#fff'
      },
    }], yAxes: [{
      gridLines:{
        borderDash: [8, 4],
      },
      ticks: {
        beginAtZero: true,
      },
    }] },
  };
  ChartLegend = true;
  ChartPlugins = [];
  ChartType: ChartType = 'bar';
  ChartColors = [
    {
      backgroundColor: '#00CCFF',
    },
    {
      backgroundColor: '#ff8888',
    },
    {
      backgroundColor: '#0088FF',
    },
    {
      backgroundColor: '#FF5555',
    },
    {
      backgroundColor: '#7C4DFF',
    },
    {
      backgroundColor: '#C18E4C',
    },
    {
      backgroundColor: '#A2A240',
    },
    {
      backgroundColor: '#83B633',
    }
  ];
  
  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(
    private store: AuthStore,  
    private localStorage: MYLocalStorage,
    private ws: WSService,
    private debug: DebugStore,
    private dialog: DialogService,
    private ustore: UserStore) {      
    }
  
    public panelOpenState = false;
    public status: boolean = false;
    public me: any;
    public titleBreadcrumbs: string;

    public step = 0;

    public ngOnInit(): void {
    }

    public ngOnChanges(changes: SimpleChanges): void {
      //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
      //Add '${implements OnChanges}' to the class.
      if(!changes.data.firstChange){
        this.loading = false;
      }
    }

}

