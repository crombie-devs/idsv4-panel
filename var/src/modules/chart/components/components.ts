import { LinearChartComponent } from "./linear-chart/linear-chart.component";
import { MultiLinearChartComponent } from "./multi-linear-chart/multi-linear-chart.component";
import { PieChartComponent } from "./pie-chart/pie-chart.component";
import { BarChartComponent } from "./bar-chart/bar-chart.component";
import { DoughnutChartComponent } from './doughnut-chart/doughnut-chart.component';

export const CHART_COMPONENTS = [
	LinearChartComponent,
	MultiLinearChartComponent,
	PieChartComponent,
	BarChartComponent,
	DoughnutChartComponent
];