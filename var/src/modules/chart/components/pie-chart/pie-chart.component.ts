import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
// For graphics
import { ChartDataSets, ChartType } from 'chart.js';
import { Label, BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})

export class PieChartComponent implements OnInit {
  public load: boolean=true;
  public url: string = '';
  public frame: any;

  @Input() data: ChartDataSets[];
  @Input() labels: Label[];
  @Input() loading: boolean = true;
  @Output() onClickedChart = new EventEmitter<string>();

  ChartOptions = {
    responsive: true,
    legend: {
      position: 'bottom',
      labels:{
        fontSize: 12,
        usePointStyle: true
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          try {
            let label = ' ' + data.labels[tooltipItem.index] || '';
            if (label) {
              label += ': ';
            }
            const sum = data.datasets[0].data.reduce((accumulator, curValue) => {
              return accumulator + curValue;
            });
            const value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
            label += value;
            return label;
          } catch (error) {
            console.log(error);
          }
        }
      }
    }
  };
  ChartLegend = true;
  ChartType: ChartType = 'pie';
  ChartColors = [
    {
      backgroundColor: ['#00CCFF','#448AFF','#7C4DFF','#FF6666','#E07A59','#C18E4C','#A2A240','#83B633'],
    },
  ];
  
  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor() { }
  
    public panelOpenState = false;
    public status: boolean = false;
    public titleBreadcrumbs: string;

    public step = 0;

    public ngOnInit(): void {
    }

    public chartClicked(data){
      this.onClickedChart.emit(data.active[0]._index);
    }
}

