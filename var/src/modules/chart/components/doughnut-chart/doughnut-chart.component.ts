import { Component, OnInit, ViewChild, Input, SimpleChanges, ElementRef } from '@angular/core';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { User } from 'src/modules/user/models/user';
import { UserStore } from 'src/modules/user/stores/user.store';
import moment from 'moment';
// For graphics
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, SingleDataSet, Label, BaseChartDirective, PluginServiceGlobalRegistrationAndOptions } from 'ng2-charts';


@Component({
  selector: 'doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.scss']
})

export class DoughnutChartComponent implements OnInit {
  public load: boolean=true;
  public url: string = '';
  public frame: any;
  @Input() data: ChartDataSets[];
  @Input() labels: Label[];
  @Input() loading: boolean = true;
  @Input() positive:boolean = true;
  @Input() height:string = "60";
  @Input() fromProfiles:boolean = false;

  ChartOptions = {
    cutoutPercentage: 70,
    responsive: true,
    tooltips: {enabled: false},
    hover: {mode: null},
    legend: {
      display: false,
    },
  };
  ChartLegend = false;
  ChartPlugins = [];
  ChartType: ChartType = 'doughnut';
  ChartColors = [
    {
      backgroundColor: ['#00CCFF','#FFFFFF','#7C4DFF','#FF6666','#E07A59','#C18E4C','#A2A240','#83B633'],
      borderWidth:0.5,
      fromProfiles: this.fromProfiles
    },
  ];
  
  public doughnutChartPlugins: PluginServiceGlobalRegistrationAndOptions[] = [{
    beforeDraw(chart:any) {
      const ctx = chart.ctx;
      let txt:string = '';
      if(chart.config.data.datasets[0].backgroundColor[0] === '#fff'){
        if(!chart.config.data.datasets[0].fromProfiles)
          txt = new Intl.NumberFormat("de-DE").format(Math.round(chart.config.data.datasets[0].data[1])) + "%";
        else
          txt = new Intl.NumberFormat("de-DE").format(chart.config.data.datasets[0].data[1].toFixed(2)) + "%"; 
      }else{
        if(!chart.config.data.datasets[0].fromProfiles)
          txt = new Intl.NumberFormat("de-DE").format(Math.round(chart.config.data.datasets[0].data[0])) + "%";
        else
          txt = new Intl.NumberFormat("de-DE").format(chart.config.data.datasets[0].data[0].toFixed(2)) + "%";
      }
      

      //Get options from the center object in options
      const sidePadding = 60;
      const sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)

      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      const centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
      const centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);

      //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
      const stringWidth = ctx.measureText(txt).width;
      const elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

      // Find out how much the font can grow in width.
      const widthRatio = elementWidth / stringWidth;
      const newFontSize = Math.floor(30 * widthRatio) - 2;
      const elementHeight = (chart.innerRadius * 2);

      // Pick a new font size so it will not be larger than the height of label.
      const fontSizeToUse = newFontSize;
      ctx.font = `0.7rem Poppins, Roboto, Helvetica Neue, sans-serif`;
      ctx.fillStyle = 'black';

      ctx.fillText(txt, centerX, centerY);
    }
  }];

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(
    private store: AuthStore,  
    private localStorage: MYLocalStorage,
    private ws: WSService,
    private debug: DebugStore,
    private dialog: DialogService,
    private ustore: UserStore) {      
    }
  
    public panelOpenState = false;
    public status: boolean = false;
    public me: any;
    public titleBreadcrumbs: string;

    public step = 0;

    public ngOnInit(): void {
    }

    public ngOnChanges(changes: SimpleChanges): void {
      //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
      //Add '${implements OnChanges}' to the class.
      if(changes.fromProfiles){
        this.ChartColors[0].fromProfiles = changes.fromProfiles.currentValue;
      }
      if(changes.positive){
        if(changes.positive.currentValue){
          this.ChartColors = [
            {
              backgroundColor: ['#00CCFF', '#fff'],
              borderWidth:0.5,
              fromProfiles: this.fromProfiles
            },
          ];
        } else{
          this.ChartColors = [
            {
              backgroundColor: ['#fff','#00CCFF'],
              borderWidth:0.5,
              fromProfiles: this.fromProfiles
            },
          ];
        }
      }
    }

}

