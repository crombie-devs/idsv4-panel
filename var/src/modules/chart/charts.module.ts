import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from 'src/app/material.module';

import { CHART_COMPONENTS } from './components/components';
import { ChartsModule } from 'ng2-charts';
 
@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    CHART_COMPONENTS
  ],
  imports:[
      CommonModule,
      ChartsModule,
      FlexLayoutModule,
      MaterialModule,
  ],
  exports:[CHART_COMPONENTS]
})
export class ChartGraphsModule {}


