import { Component, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { Auth } from 'src/modules/auth/models/auth';
import { IResult } from 'src/models/iresult';
import { MatGridListModule } from '@angular/material/grid-list';

import moment from 'moment';
import { User } from 'src/modules/user/models/user';
import { UserStore } from 'src/modules/user/stores/user.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';

@Component({
  selector: 'emissions-page',
  templateUrl: './view.page.html',
  styleUrls: ['./styles.page.scss']
})

export class EmissionsPage {
	
	public me: any;
	public store: string = null;
	public submitted: boolean = false;
	public data: any[];

  	constructor(
		private router: Router,
		private ws: WSService,
		private debug: DebugStore,	
		private dialog: DialogService,
    	private astore: AuthStore,
		private localStorage: MYLocalStorage,
		private ustore: UserStore) {
			
  	}
  
  	public ngOnInit() {
		this.debug.addInfo('EmissionsPage', 'OnInit');
	}

	public submitForm(data: any){
		this.data = data;
		this.submitted = true;
	}
	backToForm(){
		this.data = [];
		this.submitted = false;
	}
}
