import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmissionsPage } from './pages/emissions/emissions.page';

const routes: Routes = [
  { path: '', component: EmissionsPage},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class EmissionRoutingModule { }
