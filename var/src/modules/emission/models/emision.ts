import { Hour } from 'src/models/hour';
import moment from 'moment';

export class Emision {

  viewer_email: string;
  month: number;
  idviewer: number;
  idsite: number;
  idcustomer: number;
  idapp: number;
  day: number;
  date: Date
  count: number;
  hours: Hour[];  

  constructor(obj?: any) {
    if(!obj)
      return;

    this.viewer_email = obj.viewer_email;
    this.month = obj.month;
    this.idviewer = obj.idviewer;     
    this.idsite = obj.idsite;
    this.idcustomer = obj.idcustomer;
    this.day = obj.day;
    this.date = obj.date;
    this.count = obj.count;
    this.hours = obj.hours;
  }

  public getOnehourPrevious(){
    const hour = moment().hour();
    return this.hours.find(h => h.hour === hour-1);     
  }

  public getTwohourPrevious(){
    const hour = moment().hour();
    return this.hours.find(h => h.hour === hour-2);    
  }
 
}