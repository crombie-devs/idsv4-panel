import { EmissionsFormComponent } from './form/form.component'
import { EmissionsResultComponent } from './result/result.component';

export const EMISSION_COMPONENTS = [
  EmissionsFormComponent,
  EmissionsResultComponent
];
