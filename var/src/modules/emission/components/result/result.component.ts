import { Component, Input, SimpleChange, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { Auth } from 'src/modules/auth/models/auth';
import { IResult } from 'src/models/iresult';
import {MatGridListModule} from '@angular/material/grid-list';

import moment from 'moment';
import { User } from 'src/modules/user/models/user';
import { Content } from 'src/modules/content/models/content';
import { UserStore } from 'src/modules/user/stores/user.store';

// For exports
import { ExcelService } from 'src/services/excel.service';
import { PdfService } from 'src/services/pdf.service';

// For graphics
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'emissions-result',
  templateUrl: './view.component.html',
  styleUrls: ['./styles.component.scss']
})

export class EmissionsResultComponent {
	@ViewChild('htmlData') htmlData:ElementRef;
	@Input() data:any;
	@Output() onBackToForm:EventEmitter<any> = new EventEmitter(null);
	public emisionesHora: any[] = [];
	public emisionesDia: any[] = [];
	public content: Content = null;
	public me: any;
	public store: string = null;
	public isLoading: boolean = true;
	public aspectRatio: boolean = false;
	public chartHeihgt: string = '200';

	emisionesHoraLineChartData: ChartDataSets[] = [];
	emisionesHoraLineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];
	emisionesDiaLineChartData: ChartDataSets[] = [];
	EmisionesDiaLineChartLabels: Label[] = ['1', '', '', '4', '', '', '7', '', '', '10', '', '', '13', '', '', '16', '', '', '19', '', '', '22', '', '', '25', '', '', '28', '', '', '31'];

	constructor(
		private router: Router,
		private ws: WSService,
		private debug: DebugStore,	
		private dialog: DialogService,
    	private astore: AuthStore,
		private localStorage: MYLocalStorage,
		private ustore: UserStore,
		private excelService:ExcelService,
		private pdfService:PdfService,
		private translate: TranslateService) {
			
  	}
  
  	public ngOnInit() {
		this.debug.addInfo('ResultEmissionsPage', 'OnInit');
		this.me = this.ustore.getUser();
		this.syncViewer();
	}
	public ngOnChanges(changes: SimpleChange){
	  if(!changes['data'].isFirstChange())
		this.syncViewer();
	}

	private syncViewer() {
		this.getContent();
	}

	public getContent(){
		this.ws.content(this.data.content).subscribe((result: IResult) => {
			if(result.success) { 
				this.content = result.data;
				this.getEmisiones();
			  } else {
				this.debug.addError('WS.findPeople', result.data);
				this.dialog.warning('findPeople', result.data); 
			  }
		  });
	}

	public getEmisiones(){
		let params: any = {
			idcustomer: this.data.customer,
			idcampaign: this.data.content
		}
		if(this.data.site && this.data.site !== 0)
			params.idsite = this.data.site;
		
		if(this.data.player && this.data.player !== 0)
			params.idplayer = this.data.player;

		this.ws.analyticsEmisionesCountByHour(params).subscribe((result: IResult) => {
		  if(result.success) { 
			  let data:number[] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
			  result.data.map(elem =>{
				data[elem._id.hour] = elem.count;
			  })
			  this.emisionesHoraLineChartData = [
				{ data: data, label: this.translate.instant('Emisiones') },
			  ];
			  this.emisionesHora = result.data;
			  this.isLoading = false;
			}else {
			  this.debug.addError('WS.findPeople', result.data);
			  this.dialog.warning('findPeople', result.data); 
			}
		});
		this.ws.analyticsEmisionesCountByDateStr(params).subscribe((result: IResult) => {
			this.EmisionesDiaLineChartLabels = [];
			if(result.success) {
				result.data.sort(this.compare);
				let data: number[] = []; 
				result.data.map((elem, index) => {
					this.EmisionesDiaLineChartLabels.push(moment(elem._id.date).format('DD-MM'));
					data[index] = elem.count;
				});
				this.emisionesDiaLineChartData = [{data: data, label: this.translate.instant("Emisiones")}];
				this.emisionesDia = result.data;
				this.isLoading = false;
			  }else {
				this.debug.addError('WS.findPeople', result.data);
				this.dialog.warning('findPeople', result.data); 
			  }
		  });
	}
	public compare( a, b ) {
		if ( a._id.date < b._id.date ){
		  return -1;
		}
		if ( a._id.date > b._id.date ){
		  return 1;
		}
		return 0;
	}

	public excelExport(){
		let dataExcel:any[] = [];
		this.emisionesDia.map(emision => {
			dataExcel.push({
				"idcontenido": this.content.idcontent ,
				"NombreContenido": this.content.name,
				"FechaInicioContenido": this.content.dates[0].date_on,
				"FechaFinContenido": this.content.dates[0].date_off,
				"IDVisor": emision._id.idviewer,
				"EmailVisor": emision._id.viewer_email,
				"Fecha": moment(emision._id.date).format("DD-MM"), 
				"Número_Emisiones": emision.count,				
			});
		});
		let name = (this.me.customer)? this.me.customer.name : '';
		this.excelService.exportAsExcelFile(dataExcel, `${this.content.name}-${name}`);
	}
	public pdfExport(){
		let nameCustomer = (this.me.customer)? this.me.customer.name : '';
		const DATA = this.htmlData.nativeElement;
		const name = `${this.content.name}-${nameCustomer}`;
		this.pdfService.generatePdf(DATA, name);
	}
	volverForm($event){
		this.onBackToForm.emit($event);
	}
}
