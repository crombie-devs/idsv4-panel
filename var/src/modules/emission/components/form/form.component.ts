import { CONFIG } from 'src/app/app.config';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { FormField } from 'src/modules/form-field/models/form-field';
import { UserStore } from 'src/modules/user/stores/user.store';
import { IResult } from 'src/models/iresult';
import moment from 'moment';
@Component({
  selector: 'emissions-form',
  templateUrl: './view.component.html',
  styleUrls: ['./styles.component.scss']
})

export class EmissionsFormComponent {
	@Output() onSubmittedForm: EventEmitter<any> = new EventEmitter<any>();
	public meses:string[] = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
	public fieldDataCustomer: FormField = {
		label: "Cliente",
		name: 'customer',
		placeholder: '',
		options: [],
		multiple:false,
		required: true
	};
	public fieldDataSite: FormField = {
		label: "Centros",
		name: 'site',
		placeholder: '',
		options: [],
		multiple:false,
		required: false
	};
	public fieldDataPlayer: FormField = {
		label: "Visor",
		name: 'player',
		placeholder: '',
		options: [],
		multiple:false,
		required: false
	};
	public yearOptions = [
		{name: '2021', value: 2021 }
	];
	public fieldDataYear: FormField = {
		label: "Año",
		name: 'year',
		placeholder: '',
		options: this.yearOptions,
		multiple:false,
		required: true
	};
	public fieldDataMonth: FormField = {
		label: "Mes",
		name: 'month',
		placeholder: '',
		options: [
			{name: 'Enero', value: 1},
			{name: 'Febrero', value: 2},
			{name: 'Marzo', value: 3},
			{name: 'Abril', value: 4},
			{name: 'Mayo', value:5},
			{name: 'Junio', value: 6 },
			{name: 'Julio', value: 7 },
			{name: 'Agosto', value: 8 },
			{name: 'Septiembre', value: 9 },
			{name: 'Octubre', value: 10 },
			{name: 'Noviembre', value: 11 },
			{name: 'Diciembre', value: 12 }
		],
		multiple:false,
		required: true
	};
	public fieldDataContent: FormField = {
		label: "Contenido",
		name: 'content',
		placeholder: '',
		options: [],
		multiple:true,
		required: true
	};
	public me: any;
	public formGroup:FormGroup = new FormGroup({
		customer: new FormControl(null, Validators.required),
		site: new FormControl(null),
		player: new FormControl(null),
		year: new FormControl(null,Validators.required),
		month: new FormControl(null, Validators.required),
		content: new FormControl(null, Validators.required)
	});

	public loadingCustomers: boolean = true;
	public loadingYears: boolean = false;
	public loadingSites: boolean = true;
	public loadingMonths: boolean = false;
	public loadingPlayers: boolean = true;
	public loadingContents: boolean = true;
	public allContentsToSelect:any[] = [];

  	constructor(
		private ws: WSService,
		private debug: DebugStore,
		private dialog: DialogService,
		private ustore: UserStore) {
			this.yearOptions = [];
			for(let i = 0; i < 5; i++){
				const year  = moment().subtract(i, 'years').year();
				this.yearOptions = [...this.yearOptions, {name: year.toString(), value: year}];
			}
			this.fieldDataYear.options = this.yearOptions;
  	}
  
  	public ngOnInit() {
		this.me = this.ustore.getUser();
		if(this.me && this.me.customer){
			const customer = this.me.customer;
			this.fieldDataCustomer.options = [{name: customer.name, value: customer.idcustomer }]
			this.formGroup.patchValue({customer: customer.idcustomer});
			this.loadingCustomers = false;
			if(this.me.site){
				const site = this.me.site;
				this.fieldDataSite.options = [{name: site.name, value: site.idsite }]
				this.formGroup.patchValue({site: site.idsite});
				this.loadingSites = false;
				this.getPlayers();
			}else{
				this.getSites();
			}
		}else{
			this.getCustomers();
		}
	}

	getSites(){
		this.fieldDataSite.options = [];
		this.formGroup.patchValue({site: null});
		this.loadingSites = true;
		this.ws.sites().subscribe((result: IResult) => {
			if(result.success) {
				this.fieldDataSite.options.push({name: "Todos", value: 0});
				this.formGroup.patchValue({site: 0});
				result.data.map(res => {
					if(res.code && res.consider_in_sales)
						this.fieldDataSite.options.push({name: res.name, value: res.idsite });
				});				
				this.loadingSites = false;
			} else {
				this.loadingSites = false;
				return null;
			}
		}, error => {
			this.loadingSites = false;
			return null;          
		});	
	}
	getCustomers(){
		this.loadingCustomers = true;
		this.ws.customers().subscribe((result: IResult) => {
			if(result.success) {
				this.fieldDataCustomer.options.push({name: "Todos", value: 0 });
				this.formGroup.patchValue({ customer: 0});
				result.data.map(res => {
					this.fieldDataCustomer.options.push({name: res.name, value: res.idcustomer });
				});
				this.loadingCustomers = false;
			}
			else {      
				this.loadingCustomers = false;
				return null;
			}
		}, error => {
			this.loadingCustomers = false;
			return null;          
		});	
	}
	getPlayers(){
		let params: any = {
			idcustomer: this.formGroup.get('customer').value,
			idsite: this.formGroup.get('site').value
		}
		this.fieldDataPlayer.options = [];
		this.formGroup.patchValue({player: null});
		this.loadingPlayers = true;
		this.ws.playersToAnalytics(params).subscribe((result: IResult) => {
			if(result.success) {
				this.fieldDataPlayer.options.push({name: "Todas", value: 0 });
				this.formGroup.patchValue({player: 0});
				result.data.map(res => {
					this.fieldDataPlayer.options.push({name: res.email, value: res.idplayer });
				});
				this.loadingPlayers = false;
			}
			else {      
				this.loadingPlayers = false;
				return null;
			}
		}, error => {
			this.loadingPlayers = false;
			return null;          
		});	
	}
	getContents(){
		this.fieldDataContent.options = [];
		this.allContentsToSelect = [];
		this.formGroup.patchValue({content: null});
		var params: any = {
			idcustomer: this.formGroup.get('customer').value,
		}
		if(this.formGroup.get('year').value)
			params.year = this.formGroup.get('year').value;
		if(this.formGroup.get('month').value)
			params.month = this.formGroup.get('month').value;
        /*if(CONFIG.demo) {            
			if(params.month != "11" && params.month != "12") {
                params.month = 12;
            }
        }*/   
		if(this.formGroup.get('site').value)
			params.idsite = this.formGroup.get('site').value;
		if(this.formGroup.get('player').value)
			params.idplayer = this.formGroup.get('player').value;

		this.loadingContents = true;
		this.ws.contentsToEmission(params).subscribe((result: IResult) => {
			if(result.success) {
				if(result.data.length > 0){
					result.data.map(res => {
						this.fieldDataContent.options.push(res);
						this.allContentsToSelect.push(res)
					});
				}
				else
					this.dialog.warning('Contenidos','No se han encontrado contenidos');

				this.loadingContents = false;
			} else {      
				this.dialog.warning('Contenidos','No se han encontrado contenidos');
				this.loadingContents = false;
			}
		}, error => {   
			this.dialog.warning('Contenidos','No se han encontrado contenidos');
			this.loadingContents = false;
		});	
	}

	public filterByName(q: string) {
		if (!q) {
			this.fieldDataContent.options = this.allContentsToSelect;
     	} else {
		  this.fieldDataContent.options = this.allContentsToSelect.filter(
			(content: any) =>
			  content.name.toLowerCase().includes(q.trim().toLowerCase()) ||
			  content.idcontent.toString().includes(q.trim())
		  );
		}
	  }

	getYearsAndMonths(){
		let params: any = {
			customer_id: this.formGroup.get('customer').value,
		}
		if(this.formGroup.get('customer').value)
			params.customer_id = this.formGroup.get('customer').value;
		if(this.formGroup.get('site').value)
			params.site_id = this.formGroup.get('site').value;
		if(this.formGroup.get('player').value)
			params.player_id = this.formGroup.get('player').value;
		this.fieldDataYear.options = [];
		this.formGroup.patchValue({year: null});
		this.loadingYears = true;
		this.ws.emissionYears(params).subscribe((result: IResult) => {
			if(result.success) {
				result.data.map(res => {
					this.fieldDataYear.options.push({name: res, value:res});
				});
				this.loadingYears = false;
			} else {      
				this.loadingYears = false;
				return null;
			}
		}, error => {
			this.loadingYears = false;
			return null;          
		});	
		this.fieldDataMonth.options = [];
		this.formGroup.patchValue({month: null});
		this.loadingMonths = true;
		this.ws.emissionMonths(params).subscribe((result: IResult) => {
			if(result.success) {
				result.data.map(res => {
					this.fieldDataMonth.options.push({ name:this.meses[res], value:res});
				});
				this.loadingMonths = false;
			} else {      
				this.loadingMonths = false;
				return null;
			}
		}, error => {
			this.loadingMonths = false;
			return null;          
		});	
	}
	onChangeCustomer(){
		this.getSites();

	}
	onChangeSite(event:Event){
		this.getPlayers();

	}

	onChangeDates(event:Event){
       const values = this.formGroup.getRawValue();
       if(values.year && values.month && values.customer){
			this.getContents();
		}
	}

	public submitForm(){
		this.onSubmittedForm.emit(this.formGroup.getRawValue());
	}
	  
}
