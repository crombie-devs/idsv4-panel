import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EMISSION_COMPONENTS } from './components/components';
import { EmissionRoutingModule } from './emission-routing.module';
import { MaterialModule } from 'src/app/material.module';
import { HttpService } from 'src/services/http.service';
import { FormFieldModule } from 'src/modules/form-field/form-field.module';
import { ChartGraphsModule } from 'src/modules/chart/charts.module';

import { EmissionsPage } from './pages/emissions/emissions.page';
import { SharedModule } from 'src/shared/shared.module';

import { CONFIG } from 'src/app/app.config';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import moment from 'moment';

export function HttpLoaderFactory(http: HttpClient) {
  let url = CONFIG.lang_url;  
  return new TranslateHttpLoader(http, `${url}/i18n/`, `.json?timestamp=${moment().valueOf()}`);
}

@NgModule({
  declarations: [EMISSION_COMPONENTS, EmissionsPage],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    FormFieldModule,
    ChartGraphsModule,
    SharedModule,
    EmissionRoutingModule
  ],
  providers: [HttpService],
  exports:[EMISSION_COMPONENTS]
})
export class EmissionModule {
  constructor(private translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('es');
    if(!sessionStorage.getItem('lang')){
      sessionStorage.setItem('lang', 'es');
      translate.use('es');
    }else{
      translate.use(sessionStorage.getItem('lang'));
    }
  }
}