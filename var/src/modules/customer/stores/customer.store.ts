import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { ICustomer, Customer } from 'src/modules/customer/models/customer';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'customers.store.ids';

@Injectable()
export class CustomerStore {

    public onChange: BehaviorSubject<Customer[]> = new BehaviorSubject([]);
    private data: Customer[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const items: ICustomer[] = JSON.parse(store); 
                this.set(items);
            } else 
                this.load();
        }else
            this.load();
    }

    public get(): Customer[] {
        return this.data;
    }

    public set(items: ICustomer[]) {
        this.save(items);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.customersToSelect().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }
        });
    }

    private save(items: ICustomer[]) {
        this.data = items.map(a => new Customer(a));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(items));
    }
}