import { Component, EventEmitter, Input, Output, OnInit } from "@angular/core";
import { FormatStore } from "src/modules/format/stores/format.store";



@Component({
    providers: [FormatStore],
    selector: 'type-message',
    templateUrl: './type-message.component.html',
    styleUrls: ['./type-message.component.scss']
})
export class TypeMessageComponent implements OnInit {

    @Output() onChange: EventEmitter<any> = new EventEmitter();
    @Input() selected: any[] = [];
    @Input() filters: string[] = [];
    newFormast: any[] = [];
    public formats: any[] = [];
    constructor(private fstore: FormatStore) { }

    ngOnInit() {
        this.fstore.onChange.subscribe(formats => {
            this.formats = formats.filter(f => f.broadcasting == 1);
        });
    }


    filterFormats(filter) {
        if (filter.length > 0) {
            this.formats = this.formats.filter(f => filter.some(s => s == parseInt(f.key)));
        }
    }



    setFormats(formats): void {
        this.formats.forEach(item => {
            formats.forEach(element => {
                if (element[item.key] != null || element[item.key] == 0) {
                    this.selected.push(item)
                }
            });
        });
    }

    onSelect(format: any) {
        if (this.selected.some(s => s.idformat == format.idformat)) {
            this.selected = this.selected.filter(s => s.idformat != format.idformat)
        } else {
            this.selected.push(format)
        }

        this.onChange.emit(this.selected);
    }

    checkSelect(format: any) {
        return this.selected.some(s => s.idformat == format.idformat)
    }



}