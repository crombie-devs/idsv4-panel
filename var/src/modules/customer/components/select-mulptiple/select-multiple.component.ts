import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FormControl,  Validators , FormGroup} from '@angular/forms';
import { Customer } from 'src/modules/customer/models/customer';
import { CustomerService } from 'src/modules/customer/services/customer.service';

Validators
@Component({
  selector: 'select-customers',
  templateUrl: './select-multiple.component.html',
  styleUrls: ['./select-multiple.component.scss'],
})
export class SelectCustomersComponent {
  @Output() onClick: EventEmitter<any> = new EventEmitter();
  @Output() onChange: EventEmitter<number> = new EventEmitter();
  @Input() idcustomer: number;
  curstomer: FormControl;
  form: FormGroup;

  customers: Customer[] = [];

  constructor(private _customer: CustomerService) {
    this.form = new FormGroup({
      idcustomer: new FormControl('', Validators.required)
    })
    this._customer.toSelect().subscribe((items: Customer[]) => {
      this.customers = items;
      if(this.idcustomer)
      {
        this.form.patchValue({idcustomer: this.idcustomer})
      }
    });
  }

  onSelect(event): void {
    this.onClick.emit(event);
    this.onChange.emit(event.value);
  }

  
}
