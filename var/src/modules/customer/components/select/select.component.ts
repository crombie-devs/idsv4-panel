import { Component, OnInit, Input,Output, EventEmitter, SimpleChanges } from '@angular/core';
import { MYString } from "src/libs/string.libs";
import { FormControl, FormBuilder, FormGroup} from '@angular/forms';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Customer } from 'src/modules/customer/models/customer';
import { CustomerService } from '../../services/customer.service';


@Component({
  selector: 'customer-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class CustomerSelectComponent implements OnInit {

	@Output() onSelected = new EventEmitter<number>();
	@Input() customersToSelect:boolean;
	@Input() selected: number = null;
	@Input() apparence: boolean;
	@Input() idcustomer: number;


	public customers:Customer[] = [];
	public filterForm:FormGroup;
	public superAdmin:boolean = false;

  	constructor(
        private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private _customer: CustomerService
	) {	
        this.filterForm = new FormGroup({
			cliente: new FormControl('')
		})

    	
    }

	ngOnInit(): void {
		this.verifyPermissions();
		
		setTimeout(() =>{
			this.getCustomers();
		}, 600)
	
    }

    setValue(id): void {
        this.idcustomer = id;
        this.filterForm.patchValue({cliente: id});

    }
  
	private getCustomers(){
       	if(this.superAdmin){
			this._customer.toSelect().subscribe((result: any) => {
				if(result) {
					this.customers = result;
                   
					if(this.idcustomer){
						
		        		this.filterForm.patchValue({cliente: this.idcustomer+''})
					}


				} else {            
					this.openSnackBar(result);
				}
			}, error => {
				
			}); 
		}
	}

    


  
  	public captureValue() {
		let idcustomer = this.filterForm.value.cliente;
		this.onSelected.emit(idcustomer);
	}
	
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
	
	private verifyPermissions() {
		this.superAdmin = this.ustore.isSuperAdmin();
	}

	public ngOnChanges(changes: SimpleChanges): void {
		if(changes.selected)
			this.filterForm.get('cliente').setValue(changes.selected.currentValue);
	}

	public onKey(value) { 
        this.customers = this.search(value);
    }

    public search(value: string) { 
        let filter = MYString.removeAccents(value).toLowerCase();
        return this.customers.filter(c => MYString.removeAccents(c.name).toLowerCase().startsWith(filter));
    }

}
