import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { CustomerListPage } from './pages/list/customer.list.page';
import { CustomerFormPage } from './pages/form/customer.form.page';

const routes: Routes = [
  { path: 'customer/list', component: CustomerListPage},
  { path: 'customer/create', component: CustomerFormPage, data: { breadcrumb: 'Nuevo cliente' }},
  { path: 'customer/edit/:idcustomer', component: CustomerFormPage, data: { breadcrumb: 'Editar cliente' }}
  ];

  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  
  export class CustomerRoutingModule { }
  