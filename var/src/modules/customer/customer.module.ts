import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FlexModule} from "@angular/flex-layout";
import { NgxMatFileInputModule } from "@angular-material-components/file-input";
import { FormsModule, ReactiveFormsModule} from "@angular/forms";
import { CustomerRoutingModule } from "src/modules/customer/customer.routing.module";
import { MaterialModule } from "src/app/material.module";
import { CustomerSelectComponent } from './components/select/select.component';
import { SelectCustomersComponent } from './components/select-mulptiple/select-multiple.component';
import { CustomerStore } from './stores/customer.store';
import { CustomerListPage } from './pages/list/customer.list.page';
import { CustomerFormPage } from './pages/form/customer.form.page';
import { SharedModule } from '../../shared/shared.module';
import { DirectivesModule } from 'src/directive/directives.module';
import { PipesModule } from "src/pipes/pipes.module";
import { FormatModule } from "../format/format.module";
import { TypeMessageComponent } from "./components/type-message/type-message.component";


@NgModule({
  declarations: [CustomerSelectComponent, SelectCustomersComponent, CustomerListPage, CustomerFormPage, TypeMessageComponent],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CustomerRoutingModule,
    SharedModule,
    DirectivesModule,
    PipesModule,
    FormatModule
  ],
  providers: [CustomerStore],

  exports: [CustomerSelectComponent, SelectCustomersComponent,CustomerFormPage, TypeMessageComponent]
})
export class CustomerModule {}
