import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { Customer } from 'src/modules/customer/models/customer';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Router } from '@angular/router';
import { MYString } from 'src/libs/string.libs';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatInput } from '@angular/material/input';
import { from } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { CustomerService } from '../../services/customer.service';

@Component({
    selector: 'customer-list',
    templateUrl: './customer.list.page.html',
    styleUrls: ['./customer.list.page.scss']
})

export class CustomerListPage {

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChildren('table') table: QueryList<MatCheckbox>;
    @ViewChild(MatInput) input: MatInput;

    public displayedColumns: string[] = ['select', 'idcustomer', 'name', 'phone', 'status'];
    public selection = new SelectionModel<Customer>(true, []);
    public dataSource = new MatTableDataSource<Customer>();
    public new: boolean = false;
    public edit: boolean = false;
    public deleted: boolean = false;
    public permission: boolean = false;
    public load: boolean = true;
    public me: any;
    sortedData: Customer[];
    customers: Customer[];
    private data: Customer[];



    constructor(
        private ws: WSService,
        private debug: DebugStore,
        private ustore: UserStore,
        private _snackBar: MatSnackBar,
        private dialog: DialogService,
        private router: Router,
        private translate: TranslateService,
        private _customer: CustomerService) {
        this.ustore.onChange.subscribe(user => {
            this.me = user;
        });
    }

    public ngOnInit() {
        this.debug.addInfo('CustomerListComponent', 'OnInit');
        this.verifyPermissions();
        this.getCustomers();
    }


    public changeStatus(event) {
        this._customer.changeStatus(event).subscribe(item => {

        });

    }

    private getCustomers() {
        this.ws.customers().subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.customers', 'successful');
                this.permission = true;
                this.data = result.data;
                this.renderDataTable(result.data);
                this.load = false;
            }
            else {
                this.debug.addError('WS.customers', result.data);
                this.load = false;
                return null;

            }
        }, error => {
            this.debug.addError('WS.customers', error.message);
            this.load = false;
            return null;
        });
    }

    public renderDataTable(data: Customer[]) {
        const arr = [...data];
        console.log(arr)
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.filterPredicate = (data: Customer, filter: string): boolean => {
            const dataStr = MYString.removeAccents(Object.keys(data).reduce((currentTerm: string, key: string) => {
                return (currentTerm + (data as { [key: string]: any })[key] + '◬');
            }, '')).trim().toLowerCase();
            return dataStr.indexOf(MYString.removeAccents(filter.trim().toLowerCase())) != -1;
        };

        setTimeout(() => {
            this.dataSource.paginator = this.paginator;
        }, 500);

        if (this.input)
            this.applyFilter(this.input.value);




        this.sortedData = data.slice();
        this.customers = data;
        this.dataSource.sort = this.sort;
        this.load = false;
    }

    isAllSelected() {
        if (this.dataSource.paginator) {
            const numSelected = this.selection.selected.length;
            const page = this.dataSource.paginator.pageSize;
            let endIndex: number;
            if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
                endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
            } else {
                endIndex = this.dataSource.data.length - (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize);
            }
            return numSelected === endIndex;
        }
    }

    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() : this.selectRows();
    }

    selectRows() {
        let endIndex: number;
        if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
            endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
        } else {
            endIndex = this.dataSource.data.length;
        }
        for (let index = (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize); index < endIndex; index++) {
            this.selection.select(this.dataSource.data[index]);
        }
    }

    public checkboxLabel(row?: Customer): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idcustomer + 1}`;
    }


    public delete() {
        let customerByDelete = [];
        this.table.toArray().forEach(item => {
            if (item.checked) {
                customerByDelete.push(item.value);
            }
        });
        if (customerByDelete.length == 0) {

            this.dialog.warning(this.translate.instant('Aviso'), this.translate.instant('Debe seleccionar al menos un usuario')).subscribe();
        }
        else {
            this.dialog.confirm(this.translate.instant('Confirmar Archivado'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
                if (confirm) {
                    this.deleteByIds(customerByDelete);
                }
            });
        }
    }

    public update() {
        if (this.selection.selected.length > 1) {
            this.dialog.warning('Aviso', 'Sólo puede editar un cliente').subscribe();
        } else if (this.selection.selected.length == 0) {
            this.dialog.warning('Aviso', 'Debe seleccionar un cliente').subscribe();
        } else {
            this.router.navigateByUrl('/private/customer/edit/' + this.selection.selected[0].idcustomer);
        }
    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    public applyFilter(event: string) {
        this.selection.clear();
        if (!event)
            this.dataSource.data = this.data;
        else {
            if (typeof event === 'string') {
                this.dataSource.data = this.data.filter(site =>
                    site.name.toLowerCase().includes(event.trim().toLowerCase()) ||
                    site.idcustomer.toString().includes(event.trim()) ||
                    (site.phone && site.phone.toString().includes(event.trim())) ||
                    (site.email && site.email.toString().includes(event.trim())) ||
                    (site.gaid && site.gaid.toString().includes(event.trim()))
                );
            }
        }
    }


    sortData(sort: Sort) {
        const data = this.dataSource.data.slice();
        if (!sort.active || sort.direction === '') {
            this.sortedData = data;
            return;
        }

        this.sortedData = data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'name': return this.compare(a.name ? a.name : '', b.name ? b.name : '', isAsc);
                case 'idcustomer': return this.compare(a.idcustomer, b.idcustomer, isAsc);
                case 'email': return this.compare(a.email ? a.email : '', b.email ? b.email : '', isAsc);
                case 'phone': return this.compare(a.phone ? a.phone : '', b.phone ? b.phone : '', isAsc);
                case 'gaid': return this.compare(a.gaid ? a.gaid : '', b.gaid ? b.gaid : '', isAsc);
                case 'status': return this.compare(a.status.toString(), b.status.toString(), isAsc);
                default: return 0;
            }
        });
        this.dataSource.data = this.sortedData;
    }

    compare(a: number | string, b: number | string, isAsc: boolean) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }

    private verifyPermissions() {
        this.new = this.ustore.hasPermissionByName("post.customer");
        this.edit = this.ustore.hasPermissionByName("put.customer.*");
        this.deleted = this.ustore.hasPermissionByName("delete.customer.*");
    }

    private deleteByIds(ids: number[]) {
        from(ids).pipe(
            concatMap(id => this.ws.deleteCustomer(id))
        ).subscribe(customer => {
            this.load = true;
        }, error => {
            this.openSnackBar(error.message);
            this.getCustomers();
        },
            () => {
                this.openSnackBar('Los clientes seleccionados han sido archivados correctamente');
                this.selection.clear();
                this.getCustomers();
            });
    }

}
