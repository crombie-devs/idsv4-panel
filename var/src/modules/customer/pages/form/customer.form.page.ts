import { Component } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Customer } from 'src/modules/customer/models/customer';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Format } from 'src/modules/format/models/format';
import { Lang } from 'src/modules/lang/models/lang';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { CustomerService } from 'src/modules/customer/services/customer.service';
import { ICustomer } from 'src/modules/customer/models/customer'



@Component({
    providers: [FormatStore, LangStore],
    selector: 'customer-form',
    templateUrl: './customer.form.page.html',
    styleUrls: ['./customer.form.page.scss'],
})

export class CustomerFormPage {
    public id: number | null;
    public form: FormGroup;
    public langs: Lang[];
    public templates: any[];
    public sales: Number = 0;
    public influxes: Number = 0;
    public emissions: Number = 1;
    public trends: Number = 0;
    public streaming: Number = 0;
    public audioasync: Number = 0;
    public has_external_cms: Number = 0;
    public has_algorithms: Number = 0;
    public customers: Customer[];
    public formats: Format[] = [];
    public formatSelected: Format[] = [];
    public typeSelected: Set<any> = new Set();
    public loading: boolean = false;
    public customer: Customer;
    public image: string;
    public files;
    public maxSize = 16;
    public types: any[] = [];

    constructor(
        private ws: WSService,
        private debug: DebugStore,
        private router: Router,
        private route: ActivatedRoute,
        private fstore: FormatStore,
        private lstore: LangStore,
        private _snackBar: MatSnackBar,
        private _customer: CustomerService
    ) {
        this.fstore.onChange.subscribe((formats) => {
            this.formats = formats;

        });

        this.id = Number(this.route.snapshot.paramMap.get('idcustomer'));
        this.lstore.onChange.subscribe((langs) => {
            this.langs = langs;
        });


        this.form = new FormGroup({
            idcustomer: new FormControl(null),
            name: new FormControl(null, [Validators.required]),
            code: new FormControl(null),
            phone: new FormControl(null),
            image_url: new FormControl(null, Validators.required),
            idlang: new FormControl(null, [Validators.required]),
            idtemplate: new FormControl(null),
            idformat: new FormControl(null),
            category_level: new FormControl(null),
            has_influxes: new FormControl(false),
            has_sales: new FormControl(false),
            has_emissions: new FormControl(1),
            has_trends: new FormControl(false),
            has_streaming: new FormControl(null),
            has_audioasync: new FormControl(false),
            has_external_cms: new FormControl(false),
            has_algorithms: new FormControl(false),
            max_file_size: new FormControl(100),
            influxes_type: new FormControl('Ladorian Básico'),
            typeMessage: new FormControl([]),
            status: new FormControl(1),
        });
    }

    public ngOnInit() {
        this.initForm(null);
        if (this.id) {
            this.ws.getCustomer(this.id).subscribe((result: IResult) => {
                if (result.success) {
                    const customer = result.data;
                    this.customer = new Customer(customer);
                    if (customer.config_customer) {
                        let config_customer = customer.config_customer

                        this.has_external_cms = config_customer.has_external_cms;
                        this.has_algorithms = config_customer.has_algorithms;


                    } else {

                    }
                    this.initForm(this.customer);
                    if (result.data.formats) this.formatSelected = result.data.formats;
                } else {
                    this.debug.addError('WS.getCustomer', result.data);
                }
            },
                (error) => {
                    this.debug.addError('WS.getCustomer', error.message);
                }
            );
        }
        this.getTemplate();
    }

    onDisabledChanged(value: boolean) {
        if (!value) {
            this.form.value.image_url.enable();
        } else {
            this.form.value.image_url.disable();
        }
    }

    changeType(events): void {
        this.types = [...events];
    }
    private initForm(data: Customer | null) {
        if (data) {
            this.image = data.image_url ? data.image_url : null;
            this.influxes = data.has_influxes ? 1 : 0;
            this.sales = data.has_sales ? 1 : 0;
            this.streaming = data.streaming ? data.streaming : 0;
            this.audioasync = data.has_audioasync ? 1 : 0;
            this.has_external_cms = (data.config_customer && data.config_customer.has_external_cms) ? data.config_customer.has_external_cms : 0;
            this.has_algorithms = (data.config_customer && data.config_customer.has_algorithms) ? data.config_customer.has_algorithms : 0;
            let langs: number[] = [];
            data.langs.map((lang, i) => langs.push(lang.idlang));
            this.form.setValue({
                idcustomer: data.idcustomer,
                name: data.name,
                code: data.code ? data.code : null,
                phone: data.phone ? data.phone : '',
                image_url: this.image,
                idformat: data.formats,
                idlang: langs,
                idtemplate: [],
                has_influxes: data.has_influxes,
                has_sales: this.sales,
                has_emissions: data.has_emissions,
                has_trends: data.has_trends,
                has_audioasync: this.audioasync,
                has_external_cms: this.has_external_cms,
                has_algorithms: this.has_algorithms,
                has_streaming: this.streaming,
                influxes_type: data.influxes_type,
                category_level: data.category_level,
                max_file_size: data.max_file_size,
                status: data.status,
                typeMessage: data.typeMessage ? data.typeMessage : null
            });
        }
    }

    public create() {
        if (this.formatSelected.length == 0)
            this.openSnackBar("Seleccione al menos un formato");
        else if (this.form.valid) {
            this.loading = true;
            let langsArr = this.form.get('idlang').value;
            let langs = [];
            if (langsArr) langsArr.map((val) => langs.push({ idlang: val }));
            if (this.form.value.category_level == '') {
                this.form.value.category_level = 'null';
            }
            this.form.value.streaming = this.form.value.has_streaming;
            this.form.value.idformat = JSON.stringify(this.formatSelected);
            this.form.value.langs = JSON.stringify(langs);
            this.form.value.typeMessage = JSON.stringify(this.types);
            this.form.value.has_external_cms = this.has_external_cms;
            this.form.value.has_algorithms = this.has_algorithms;

            this._customer.create(this.form.value).subscribe((result: ICustomer) => {
                if (result) {
                    this.debug.addInfo('WS.createCustomer', 'successful');
                    let data = result;
                    let idCustomer = data.idcustomer;
                    this.updateCustomerFormats(idCustomer);
                    this.updateTemplatesCustomer(idCustomer);
                    this.openSnackBar('Cliente creado correctamente');
                    this.router.navigateByUrl('/private/customer/list');
                    this.loading = false;
                } else {
                    this.debug.addError('WS.updateCustomer', "vacio");
                    this.openSnackBar("No existe el cliente");
                    this.loading = false;
                }
            });

        } else
            this.openSnackBar("Por favor revise el formulario, faltan campos por rellenar");

    }

    public update() {
        if (this.formatSelected.length == 0)
            this.openSnackBar("Seleccione al menos un formato");
        else {
            this.loading = true;
            let langsArr = this.form.get('idlang').value;
            let langs = [];
            if (langsArr) langsArr.map((val) => langs.push({ idlang: val }));
            if (this.form.value.category_level == '') {
                this.form.value.category_level = 'null';
            }
            this.form.value.has_external_cms = this.has_external_cms;
            this.form.value.has_algorithms = this.has_algorithms;
            this.form.value.streaming = this.form.value.has_streaming;
            this.form.value.typeMessage = JSON.stringify(this.types);
            this.form.value.langs = JSON.stringify(langs);
            this._customer.update(this.form.value).subscribe(
                (result: ICustomer) => {
                    if (result) {
                        this.debug.addInfo('WS.updateCustomer', 'successful');
                        let data = result;
                        let idCustomer = data.idcustomer;
                        this.updateCustomerFormats(idCustomer);
                        this.updateTemplatesCustomer(idCustomer);
                        this.openSnackBar('Cliente actualizado correctamente');
                        this.router.navigateByUrl('/private/customer/list');
                        this.loading = false;
                    } else {
                        this.debug.addError('WS.updateCustomer', "vacio");
                        this.openSnackBar("No existe el cliente");
                        this.loading = false;
                    }
                },
                (error) => {
                    let msj = JSON.stringify(error.error.data);
                    this.openSnackBar(msj.substr(2, msj.length - 3));
                    this.debug.addError('WS.updateCustomer', error.message);
                    this.loading = false;
                }
            );
        }
    }

    public updateCustomerFormats(idcustomer: number) {
        let selectedFormatsIds = [];
        if (this.formatSelected) {
            this.formatSelected.forEach((format) => {
                selectedFormatsIds.push({
                    idformat: format.idformat,
                });
            }, this);
        }
        let body = {
            formats: selectedFormatsIds,
        };
        this.ws.updateCustomerFormats(idcustomer, body).subscribe(
            (result: IResult) => {
                if (result.success) {
                    let data = result.data;
                    this.debug.addInfo('WS.updateCustomerFormats', 'successful');
                } else {
                    this.debug.addError('WS.updateCustomerFormats', result.data);
                }
            },
            (error) => {
                this.debug.addError('WS.updateCustomerFormats', error.message);
            }
        );
    }

    public changeHasAudioSync(event: MatSlideToggleChange) {
        this.audioasync = event.checked ? 1 : 0;
        this.form.patchValue({ has_audioasync: this.audioasync });
    }
    public changeHasExternalCMS(event: MatSlideToggleChange) {
        this.has_external_cms = event.checked ? 1 : 0;
        this.form.patchValue({ has_external_cms: this.has_external_cms });
    }
    public changeHasAlgorithms(event: MatSlideToggleChange) {
        this.has_algorithms = event.checked ? 1 : 0;
        this.form.patchValue({ has_algorithms: this.has_algorithms });
    }
    public changeHasInfluxes(event: MatSlideToggleChange) {
        this.influxes = event.checked ? 1 : 0;
        this.form.patchValue({ has_influxes: this.influxes })
    }
    public changeHasSales(event: MatSlideToggleChange) {
        this.sales = event.checked ? 1 : 0;
        this.form.patchValue({ has_sales: this.sales })
    }
    public changeHasStatus(event: MatSlideToggleChange) {
        const status = event.checked ? 1 : 0;
        this.form.patchValue({ status: status })
    }
    public changeHasEmissions(event: MatSlideToggleChange) {
        this.emissions = event.checked ? 1 : 0;
        this.form.patchValue({ has_emissions: this.emissions })
    }
    public changeHasTrends(event: MatSlideToggleChange) {
        this.trends = event.checked ? 1 : 0;
        this.form.patchValue({ has_trends: this.trends })
    }
    public changeHasStreaming(event: MatSlideToggleChange) {
        this.streaming = event.checked ? 1 : 0;
        this.form.patchValue({ has_streaming: this.streaming })
    }
    changeFormat(formats: Format[]) {
        this.formatSelected = formats;
    }
    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }
    public getFileData(data) {
        this.files = this.form.value.image_url;
        if (this.customer) {
            this.customer.image_url = URL.createObjectURL(this.files);
        } else {
            this.customer = new Customer();
            this.customer.image_url = URL.createObjectURL(this.files);
        }
    }
    private getTemplate() {
        let value;
        this.ws.templatesCustomersByIdcustomerMaster().subscribe(
            (result: IResult) => {
                if (result.success) {
                    let data = result.data;
                    this.templates = data;
                    this.form.patchValue({ idtemplate: this.templates });
                    if (this.id) {
                        value = this.templates.find((t) => t.idcustomer === this.id);
                        if (value) {
                            this.form.patchValue({ idtemplate: value.idcustomer });
                        } else {
                            this.ws.templatesCustomers().subscribe(
                                (result: IResult) => {
                                    if (result.success) {
                                        value = result.data.find((t) => t.idcustomer === this.id);
                                        if (value) {
                                            this.form.patchValue({ idtemplate: value.idcustomer_master });
                                        }
                                        this.debug.addInfo('WS.templatesCustomers', 'successful');
                                    } else {
                                        this.debug.addError('WS.templatesCustomers', result.data);
                                    }
                                },
                                (error) => {
                                    this.debug.addError('WS.templatesCustomers', error.message);
                                }
                            );
                        }
                    }
                    this.debug.addInfo(
                        'WS.templatesCustomersByIdcustomerMaster',
                        'successful'
                    );
                } else {
                    this.debug.addError(
                        'WS.templatesCustomersByIdcustomerMaster',
                        result.data
                    );
                }
            },
            (error) => {
                this.debug.addError(
                    'WS.templatesCustomersByIdcustomerMaster',
                    error.message
                );
            }
        );
    }

    private updateTemplatesCustomer(idcustomer: number) {
        let obj: Object;
        if (!this.form.value.idtemplate.length) {
            obj = {
                idcustomer: idcustomer,
                idcustomer_master: this.form.value.idtemplate,
            };
            this.ws.updateTemplatesCustomer(idcustomer, obj).subscribe(
                (result: IResult) => {
                    if (result.success) {
                        this.debug.addInfo('WS.updateTemplatesCustomer', 'successful');
                    } else {
                        this.debug.addError('WS.updateTemplatesCustomer', result.data);
                    }
                },
                (error) => {
                    this.debug.addError('WS.updateTemplatesCustomer', error.message);
                }
            );
        }
    }
}
