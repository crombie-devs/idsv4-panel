import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ICustomer } from 'src/modules/customer/models/customer'
import { HttpService } from 'src/services/http.service';
import { ICrudWS } from 'src/interfaces/ICrudWS.interface';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class CustomerService implements ICrudWS<ICustomer> {
    constructor(private http: HttpService) { }

    public create(obj: ICustomer): Observable<ICustomer> {



        return this.http.createWithFiles(
            'private/customer',
            obj,
            'customer_create'
        );
    }

    public update(obj: ICustomer): Observable<ICustomer> {
        if (typeof obj.image_url === 'string') {
            return this.http.updateWithFiles(
                'private/customer/' + obj.idcustomer,
                obj,
                'customer_update'
            );
        } else {
            return this.http.updateWithFiles(
                'private/customer/' + obj.idcustomer,
                obj,
                'customer_update'
            );
        }
    }

    public delete(id): Observable<boolean> {
        return this.http
            .delete('private/audiocategory', [id], 'WS.audiocaregoty_delete')
            .pipe(tap(() => id));
    }

    public get(): Observable<ICustomer> {
        return of();
    }

    public toSelect(args?: any[]): Observable<ICustomer[]> {
        return this.http.get('private/customers/toselect', [], 'WS.customer_toselect');
    }

    public list(args: any[]): Observable<ICustomer[]> {
        return this.http.get('private/audiocategory/list', [], 'WS.audiocategory');
    }


    public changeStatus(id: number): Observable<boolean[]> {
        return this.http.get('private/customer/status', [id], 'WS.customer_change_status');
    }

}
