import { Format } from 'src/modules/format/models/format';
import { Lang } from 'src/modules/lang/models/lang';
import { IConfigCustomer, ConfigCustomer } from './config-customer';

export interface ICustomer {
  idcustomer: number;
  name: string;
  code: string;
  email: string;
  phone: string;
  image_url: string;
  gaid: string;
  has_sales:boolean;
  has_influxes: boolean;
  has_emissions: boolean;
  has_trends: boolean;
  has_audioasync: boolean;
  config_customer: IConfigCustomer;
  streaming: number;
  idtemplate: number;
  formats: Format[];
  langs: Lang[];
  influxes_type: string;
  max_file_size: number;
  category_level: string;
  status: boolean;
  typeMessage?: any;
}

export class Customer implements ICustomer {
  idcustomer: number;
  name: string;
  code: string;
  email: string;
  phone: string;
  image_url: string;
  gaid: string;
  has_sales: boolean;
  has_influxes: boolean;
  has_emissions: boolean;
  has_trends: boolean;
  has_audioasync: boolean;
  config_customer: ConfigCustomer;
  streaming: number;
  idtemplate: number;
  formats: Format[];
  langs: Lang[];
  influxes_type: string;
  category_level: string;
  max_file_size: number;
  status: boolean;
  typeMessage?: any;

	constructor(obj?: ICustomer) {
		if(!obj)
      return;

    this.idcustomer = obj.idcustomer;
    this.name = obj.name;
    this.code = obj.code;
    this.email = obj.email;
    this.phone = obj.phone;
    this.image_url = obj.image_url;
    this.gaid = obj.gaid;
    this.has_sales = obj.has_sales ? obj.has_sales : false;
    this.has_influxes = obj.has_influxes;
    this.has_emissions = obj.has_emissions;
    this.has_trends = obj.has_trends;
    this.has_audioasync = obj.has_audioasync;
    this.config_customer = obj.config_customer;
    this.streaming = obj.streaming;
    this.idtemplate = obj.idtemplate;
    this.influxes_type = obj.influxes_type;
    this.category_level = obj.category_level;
    this.max_file_size = obj.max_file_size;
    this.typeMessage = obj.typeMessage;
    if(obj.formats)    
      this.formats = obj.formats.map(f => new Format(f));
    if(obj.langs)
      this.langs = obj.langs.map(l => new Lang(l));
    this.status = obj.status;
  }
}
