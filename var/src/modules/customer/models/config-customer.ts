export interface IConfigCustomer {
  has_external_cms: Number;
  has_algorithms: Number;
}

export class ConfigCustomer implements IConfigCustomer {
  has_external_cms: Number;
  has_algorithms: Number;

	constructor(obj?: IConfigCustomer) {
		if(!obj)
      return;

    this.has_external_cms = obj.has_external_cms;
    this.has_algorithms = obj.has_algorithms;
  }
}
