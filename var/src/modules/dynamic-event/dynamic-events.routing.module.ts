import { Routes, RouterModule } from '@angular/router';
import { DynamicEventsComponent } from './dynamic-events.component';
import { NgModule } from '@angular/core';
import { DynamicEventsListPage } from './pages/dynamic-events/list/list.page';
import { DynamicEventsFormPage } from './pages/dynamic-events/add/form.page';
import { EventsPreviewPage } from './pages/events-preview/events-preview.page';


const routes: Routes = [
    {
      path: '',
      component: DynamicEventsComponent,
      children: [
        { path: 'list', component: DynamicEventsListPage },
        { path: 'add', component: DynamicEventsFormPage },
        { path: 'add/:id', component: DynamicEventsFormPage },
        { path: 'add/:id/:customer', component: DynamicEventsFormPage },
        { path: 'events-preview', component: EventsPreviewPage},
        { path: '', redirectTo: 'list', pathMatch: 'full' },
      ],
    },
  ];


  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  
  export class DynamicEventsRoutingModule { }
  