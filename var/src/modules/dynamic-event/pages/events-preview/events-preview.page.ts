import {Component, OnInit, ViewChild} from "@angular/core";
import moment from "moment";
import { PlayerService } from "src/modules/player/services/player.service";
import { Player } from "src/modules/player/models/player";
import { SiteSelectMultipleComponent } from "src/modules/site/components/select-multiple/select-multiple.component";
import { DynamicEventsService } from "src/modules/dynamic-event/services/dynamic-events.service";
import { IDynamicEventsRule } from "src/modules/dynamic-event/models/DynamicEventsRule";
import { SiteService } from 'src/modules/site/services/site.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import { SelectPlayersComponent } from 'src/modules/player/components/select-players/select-players.component';


@Component({selector: "events-preview",
 templateUrl: "./events-preview.page.html", 
 styleUrls: ["./events-preview.page.scss"]})

export class EventsPreviewPage implements OnInit {
  @ViewChild(SelectPlayersComponent)player: SelectPlayersComponent;
  @ViewChild(SiteSelectMultipleComponent)site: SiteSelectMultipleComponent;
  date: any;
  playerValue: Player;
  players: Player[] = [];
  customerSelect: boolean;
  loading: boolean = false;
  siteSelect: boolean;
  idcustomer: number;
  idsite: number;

  constructor(private _player : PlayerService, private _user : UserStore, private _event : DynamicEventsService, private _site: SiteService) {
    const user = this._user.getUser();
    this.idcustomer = (user && user.customer) ? user.customer.idcustomer : null;
    this.customerSelect = user.roles.some((item) => item.name === "superadmin");
    this.siteSelect = user.roles.some((item) => item.name === "admin" || item.name === "superadmin");
    this.date = new Date();
    this.date = moment(this.date).format("YYYY-M-dd");
    this._site.$idsite.subscribe(idsite => {
      this.idsite = idsite;
    });
    this._player.$subject.subscribe((idplayer : number) => {
      this.loading = false;
      if (this.players.length <= 0) {
        this._player.toSelect().subscribe((players : Player[]) => {
          this.players = players;
          this.playerValue = players.filter((item) => item.idplayer == idplayer)[0];
        });
      } else {
        this.playerValue = this.players.filter((item) => item.idplayer == idplayer)[0];
      }
    });
  }

  send(event : {
    rule: IDynamicEventsRule;
    socketsIds: string[]
  }): void {

    let data = {
      data: event.rule.tags,
      event: "viewer." + event.rule.action + "tags",
      from: this._user.getUser().email,
      socket: ""
    };
    event.socketsIds.forEach((socketId : string) => {
      data.socket = socketId;
      this._event.setTags(data).subscribe((tagResponse : boolean) => {
      });
    });
  }

  ngOnInit(): void {}

  setSites(idsite : number): void {
    this.player.filterBySite(idsite);
  }

  setCustomer(idcustomer : number): void {
    this.site.filterByCustomer(idcustomer);
  }

  setPlayers(idplayer : number) {
    this.loading = true;
  }
}
