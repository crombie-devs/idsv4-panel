import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IDynamicEvent } from 'src/modules/dynamic-event/models/DynamicEvent';
import { UserStore } from 'src/modules/user/stores/user.store';
import { DebugStore } from 'src/stores/debug.store';
import { Site } from 'src/modules/site/models/site';
import { Player } from 'src/modules/player/models/player';
import { MatDialog } from '@angular/material/dialog';
import { TagsDialogComponent } from 'src/modules/dynamic-event/dialogs/tags-dialog.component';
import { DynamicEventsRule } from 'src/modules/dynamic-event/models/DynamicEventsRule';
import { DynamicEventsService } from 'src/modules/dynamic-event/services/dynamic-events.service';
import { SelectPlayersComponent } from 'src/modules/player/components/select-players/select-players.component';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'dynamic-events-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss'],
})
export class DynamicEventsFormPage {
  public id: number | null;
  public loading: boolean = false;
  public me: any;
  public hide: boolean = true;
  public form: FormGroup;
  public customerToSelect: any;
  public sites: Site[];
  public players: Player[];
  public playerTemp: Player[];
  public siteTemp: Site[];
  public events: IDynamicEvent;
  public idevent: any;
  public rules: DynamicEventsRule[] = [];
  public idsite: number;
  public idplayer: number;
  
  @ViewChild(SelectPlayersComponent) selectPlayer: SelectPlayersComponent;

  public event: IDynamicEvent;
  constructor(
    private debug: DebugStore,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private ustore: UserStore,
    private matDialog: MatDialog,
    private _events: DynamicEventsService,
    private router: Router,
    private translate: TranslateService
  ) {
    this.me = this.ustore.getUser();
    
    this.form = new FormGroup({
      id: new FormControl(''),
      name: new FormControl('', Validators.required),
      source_url: new FormControl('', Validators.required),
      is_right_now: new FormControl(false),
      frecuency: new FormControl(''),
      status: new FormControl(true),
      idcustomer: new FormControl(
        this.me.customer ? this.me.customer.idcustomer: false,
        Validators.required
      ),
      idsite: new FormControl(''),
      idplayer: new FormControl(''),
    });
  }

  public ngOnInit() {
    this.debug.addInfo('CameraFormComponent', 'OnInit');
    this.verifyPermissions();

    this.route.params.subscribe((param) => {
      // Editar
      if (param.id) {
        this._events.get(param.id).subscribe((item) => {
          this.form.setValue({
            id: item.id,
            name: item.name,
            source_url: item.source_url,
            is_right_now: item.is_right_now,
            frecuency: item.frecuency,
            status: item.status,
            idplayer: item.idplayer,
            idcustomer: item.idcustomer,
            idsite: item.idsite,
          });
          this.idsite = item.idsite;
      
          this.selectPlayer.filterBySite(item.idsite, item.idplayer);

          this.rules = item.rules;
        });
      }
        
      if (param.customer) {
        this.form.patchValue({ idcustomer: param.customer });
      }
    });
  }

  close(): void {
   this.form.reset();
   this.router.navigate(['../list'],{ relativeTo: this.route })
  }

  setSites(idsite: number): void {
 
    let customer: any = this.selectPlayer.customer(idsite);

    this.form.patchValue({idcustomer: customer.idcustomer})

    this.form.patchValue({ idsite: idsite });
    this.selectPlayer.filterBySite(idsite);
  }

  setPlayers(idplayer: number) {
    
    this.form.patchValue({ idplayer: idplayer });
  }

  openDialog(): void {
    let dialogRef;

    dialogRef = this.matDialog.open(TagsDialogComponent, { width: '30%' });

    dialogRef.afterClosed().subscribe((result) => {
      let arr = [...this.rules];
      let rule = new DynamicEventsRule();
      if (result) {
        rule.action = result.action;
        rule.comp_value = result.comp_value;
        rule.entity = result.entity;
        rule.symbol = result.symbol;
        rule.times = result.times;
        rule.identity = result.identity;
        arr.push(rule);
        this.rules = arr;
      }
    });
  }

  delete(event): void {
    this.rules = event;
  }

  update(row): void {
    const data = row.row;
    let dialogRef;
    dialogRef = this.matDialog.open(TagsDialogComponent, {
      width: '30%',
      data,
    });
    dialogRef.afterClosed().subscribe((result) => {
      const rules = [...this.rules];
      rules[row.index] = result;
      this.rules = rules;
    });
  }

  public save() {
    if(this.rules.length <= 0) {
      this.openSnackBar(
        this.translate.instant(
          'Debes crear al menos una regla'
        )
      );
      return;
    }
   this.form.value.is_right_now =  this.form.value.is_right_now ? true : false;
   this.form.value.status =  this.form.value.status ? true : false;
   
   if (this.form.valid) {
      this.form.value.rules = JSON.stringify(this.rules);
      this._events.save(this.form.value).subscribe((event) => {
        this.router.navigate(['/private/dynamic-events/list']);
      });
    }
  }

  public openSnackBar(message: string) {
    this._snackBar.open(message, '', { duration: 1500 });
  }

  private verifyPermissions() {
    this.customerToSelect = this.ustore.hasPermissionByName(
      'get.customers.toselect'
    );
  }
}
