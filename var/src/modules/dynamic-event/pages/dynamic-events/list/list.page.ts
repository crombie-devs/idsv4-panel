import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MatSort, Sort } from '@angular/material/sort';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { MYString } from 'src/libs/string.libs';
import { DebugStore } from 'src/stores/debug.store';
import { DynamicEventsService } from 'src/modules/dynamic-event/services/dynamic-events.service';
import { IDynamicEvent } from 'src/modules/dynamic-event/models/DynamicEvent';


@Component({
  selector: 'dynamic-events-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class DynamicEventsListPage implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  loading: boolean = true;

  public displayedColumns: string[] = [
    'select',
    'id',
    'name',
    'source_url',
    'is_right_now',
    'frecuency',
    'status',
  ];

  public selection = new SelectionModel<any>(true, []);
  public dataSource = new MatTableDataSource<any>();
  public data: any[];
  public events: IDynamicEvent[];
  public load: boolean;
  public sortedData: any[];

  dataTemp: any[] = [];

  constructor(
    private debug: DebugStore,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private http: DynamicEventsService,
    private _events: DynamicEventsService,
    private dialog: DialogService,
    private _snackBar: MatSnackBar,
    private router: Router,
  ) {
  
  }

  public ngOnDestroy() {}

  public ngOnInit() {
    this._events.list([]).subscribe((item) => {
      this.renderDataTable(item);
    });
    this.debug.addInfo('ContentListComponent', 'OnInit');
  }

  deleteEvent() {
    if(this.selection.selected.length == 0){
			this.dialog.warning(this.translate.instant('Aviso'), this.translate.instant('Debe seleccionar al menos un evento')).subscribe();
		}else{
			this.dialog.confirm(this.translate.instant('Confirmar Archivado'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
				if (confirm){
          if (this.selection.selected.length > 0) {
            let data = [];
            this.selection.selected.forEach((item) => {
              this.http.delete(item.id).subscribe((_) => {
                data = this.dataSource.data.filter((v) => v.id != item.id);
                this.dataSource.data = data;
                this.openSnackBar(
                  this.translate.instant(
                    'event_update'
                  )
                );
              });
            });
          }
        }
      });  
    }
  }

  updateEvent() {
    if (this.selection.selected.length > 1) {
      this.dialog.warning('Aviso', 'Sólo puede editar un audio').subscribe();
    } else if (this.selection.selected.length == 0) {
      this.dialog.warning('Aviso', 'Debe seleccionar un audio').subscribe();
    } else {
      this.router.navigate(['../add', this.selection.selected[0].id],{ relativeTo: this.route });
    }
  }

  sortData(sort: Sort) {
    const data = this.events.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }
  }

  public isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  public masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  public checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.idcontent + 1
    }`;
  }

  public deleteEvents() {
    if (this.selection.selected.length == 0) {
      this.dialog.warning(this.translate.instant('Aviso'), this.translate.instant('Debe seleccionar al menos un evento')).subscribe();
    } else {
        this.dialog.confirm(this.translate.instant('Confirmar Archivado'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
          if (confirm) {
            if (this.selection.selected.length > 0) {
              let data = [];
              this.selection.selected.forEach((item) => {
                this.http.delete(item.idaudio).subscribe((_) => {
                  data = this.dataSource.data.filter(
                    (v) => v.idaudio != item.idaudio
                  );
                  this.dataSource.data = data;
                  this.openSnackBar(this.translate.instant('audio_update'));
                });
              });
            }
          }
        });
    }
  }

  public renderDataTable(data: any[]) {
    this.loading = false;
    this.dataSource = new MatTableDataSource();
    this.dataSource.filterPredicate = (
      data: IDynamicEvent,
      filter: string
    ): boolean => {
      const dataStr = MYString.removeAccents(
        Object.keys(data).reduce((currentTerm: string, key: string) => {
          return currentTerm + (data as { [key: string]: any })[key] + '◬';
        }, '')
      )
        .trim()
        .toLowerCase();
      return (
        dataStr.indexOf(MYString.removeAccents(filter.trim().toLowerCase())) !=
        -1
      );
    };
    this.dataSource.data = data;
    this.dataTemp = [...this.dataSource.data];

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public openSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 1500,
    });
  }

  public applyFilter(event: string) {
    if (typeof event === 'string') {
      this.dataSource.filter = event;

      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
    }
  }
}
