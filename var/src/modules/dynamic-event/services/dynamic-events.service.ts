import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';



import { ICrudWS } from '../../../interfaces/ICrudWS.interface';
import { HttpService } from '../../../services/http.service';
import { IDynamicEvent } from '../models/DynamicEvent';





@Injectable({
  providedIn: 'root',
})
export class DynamicEventsService implements ICrudWS<IDynamicEvent>{

events: Subject<IDynamicEvent[]> = new Subject();
$events = this.events.asObservable();

constructor(private http: HttpService) {}

 public save(obj: IDynamicEvent): Observable<IDynamicEvent> {
   if(obj.hasOwnProperty('id') && obj.id) {
    return this.update(obj);
   } else {
     return this.create(obj);
   }
 } 

  public create(obj: IDynamicEvent): Observable<IDynamicEvent> {
    return this.http.create('private/dynamic-events', obj, 'WS.dynamic-events_add');
  }

  public update(obj: IDynamicEvent): Observable<IDynamicEvent> {
    return this.http.update('private/dynamic-events', obj, 'WS.dynamic-events_add');
  }

  public delete(id: number): Observable<boolean> {
    return this.http.delete('private/dynamic-events', [id], 'WS.dynamic-events_delete');
  }

  public get(id: number): Observable<IDynamicEvent> {
    return this.http.get('private/dynamic-events', [id], 'WS.dynamic-events');
  }

  public toSelect(args?: any[]): Observable<IDynamicEvent[]> {
       return  this.http.get('private/dynamic-events/toselect', args, 'WS.dynamic-events');
  }


 public list(args: any[])  : Observable<IDynamicEvent[]> {
   
  return  this.http.get('private/dynamic-events', args, 'WS.dynamic-events');
 }


 public rulesByPlayer(idcustomer: number, idsite: number ,idplayer: number):  Observable<IDynamicEvent[]> {
  return this.http.get('private/dynamic-events/byplayer', [idcustomer, idsite, idplayer], 'WS.dynamic-events')
}


public setTags(data: any): Observable<any>  {
    return this.http.update('private/send/push', data, 'WS.dynamic-events-set-tags');
} 




}


