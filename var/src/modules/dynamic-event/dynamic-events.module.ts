import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SharedModule } from 'src/shared/shared.module';
import { DirectivesModule } from 'src/directive/directives.module';
import { DynamicEventsComponent } from 'src/modules/dynamic-event/dynamic-events.component';
import { TagsDialogComponent } from './dialogs/tags-dialog.component';
import { RulesListComponent } from './components/rules/rules-list.component';
import { TagModule } from 'src/modules/tag/tag.module';
import { DynamicEventsRoutingModule } from './dynamic-events.routing.module';
import { SiteModule } from '../../modules/site/site.module';
import { PlayerModule } from 'src/modules/player/player.module';
import { BlockEventsListComponent } from './components/block-events-list/block-events-list.component';

import { EventsPreviewPage } from './pages/events-preview/events-preview.page';
import { DynamicEventsFormPage } from './pages/dynamic-events/add/form.page';
import { DynamicEventsListPage } from 'src/modules/dynamic-event/pages/dynamic-events/list/list.page';

import { CustomerModule } from 'src/modules/customer/customer.module';

@NgModule({
  declarations: [
    DynamicEventsComponent,
    TagsDialogComponent,
    RulesListComponent,
    BlockEventsListComponent,
    DynamicEventsFormPage,
    DynamicEventsListPage,
    EventsPreviewPage,
  ],
  imports: [
    CommonModule,
    DynamicEventsRoutingModule,
    SharedModule,
    MatPaginatorModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    MatProgressSpinnerModule,
    FlexModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatCheckboxModule,
    MatIconModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MatSliderModule,
    DirectivesModule,
    TagModule,
    SiteModule,
    PlayerModule,
    CustomerModule
  ],
  providers: [
  
  ],

  exports: [   
    DynamicEventsComponent,
    TagsDialogComponent,
    RulesListComponent,
    BlockEventsListComponent,
   ]
})
export class DynamicEventsModule {
  
}
