import { DynamicEventsRule } from './DynamicEventsRule';



 
export interface IDynamicEvent {
  id?: number;
  name: string;
  source_url: string;
  is_right_now: boolean;
  idcustomer: number;
  frecuency: number;
  status: boolean;
  idsite: number;
  idplayer: number;
  rules: DynamicEventsRule[];
 
}





export class DynamicEvent implements IDynamicEvent {
  id?: number;
  name: string;
  source_url: string;
  is_right_now: boolean;
  idcustomer: number;
  frecuency: number;
  status: boolean;
  idsite: number;
  idplayer: number;
  rules: DynamicEventsRule[];
 

  constructor(obj?: IDynamicEvent) {
   
    this.id = obj && obj.id ? obj.id : null;
    this.name = obj && obj.name ? obj.name : null;
    this.source_url = obj  &&  obj.source_url ? obj.source_url : null;
    this.is_right_now = obj && obj.is_right_now ? obj. is_right_now : false;
    this.status =  obj &&  obj.status ? obj.status : true;
    this.idcustomer =  obj && obj.idcustomer ? obj.idcustomer : null;
    this.frecuency =  obj && obj.frecuency ? obj.frecuency : 0;
    this.idplayer =  obj && obj.idplayer ? obj.idplayer : null;
    this.rules =  obj && obj.rules ?  obj.rules : [];

    
  }





}
