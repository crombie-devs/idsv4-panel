import { Tag } from 'src/modules/tag/models/tag';



enum Symbol {
  igual = "==",
  distinto = "!=",
  mayorIgual = ">=",
  mayor = ">",
  menorIgual = "<=",
  menor = "<"
 }


 enum Action {
   add = "add",
   del = "del"
 }

 enum Entity {
   tag = "tag",
   content = "content",
   widget = "widget"
 }

export interface IDynamicEventsRule {
  id: number;
  idevent: number;
  symbol: Symbol;
  comp_value: string;
  action?: Action;
  entity: Entity;
  identity: number;
  times: boolean;
  tags: Tag[];

}

export class DynamicEventsRule implements  IDynamicEventsRule {
  id: number;
  idevent: number;
  symbol: Symbol;
  comp_value: string;
  action?: Action;
  entity: Entity;
  identity: number;
  times: boolean;
  tags: Tag[];


  constructor(obj?:  DynamicEventsRule) {
    if (!obj) {
      return;
    }

    this.id = obj.id ? obj.id : null;
    this.idevent = obj.idevent ? obj.idevent : null;
    this.symbol = obj.symbol ? obj.symbol : null;
    this.comp_value = obj.comp_value ? obj.comp_value : null;
    this.action = obj.action ? obj.action : null;
    this.entity = obj.entity ? obj.entity : null;
    this.times = obj.times ? obj.times : null;
    this.tags = obj.tags ? obj.tags.map(t => new Tag(t)) : [];
  }
}
