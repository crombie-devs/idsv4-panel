import {Component, Inject, ViewChild} from "@angular/core";
import {FormGroup, Validators, FormControl} from "@angular/forms";
import {DynamicEventsRule} from "../models/DynamicEventsRule";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import { SelectGroupTagsComponent } from 'src/modules/tag/components/select-group-tags/select-group-tags.component';

@Component({selector: "tags-dialog", templateUrl: "./tags-dialog.component.html", styleUrls: ["./tags-dialog.component.scss"]})
export class TagsDialogComponent {
  public form: FormGroup;
  public idtag: number;
  public symbols = [
    "=",
    "!=",
    ">=",
    ">",
    "<=",
    "<"
  ];
  public actions = ["add", "del"];
  public entities = ["tag", "content", "widget"];
  @ViewChild(SelectGroupTagsComponent) tags: SelectGroupTagsComponent;
  constructor(@Inject(MAT_DIALOG_DATA)public data : any, public dialogRef : MatDialogRef<any>) {}

  public ngOnInit() {
    this.initForm(this.data);
  }

  private initForm(data : DynamicEventsRule | null) {
    if (data) {

      this.idtag = data.identity; 

      this.form = new FormGroup({
         
        symbol: new FormControl(data.symbol, Validators.required),
        comp_value: new FormControl(data.comp_value, Validators.required),
        action: new FormControl(data.action, Validators.required),
        entity: new FormControl(data.entity, Validators.required),
        identity: new FormControl(data.identity),
        times: new FormControl(data.times)
      });
    } else {
      this.form = new FormGroup({
        symbol: new FormControl("", Validators.required),
        comp_value: new FormControl("", Validators.required),
        action: new FormControl("", Validators.required),
        entity: new FormControl("tag", Validators.required),
        identity: new FormControl(0),
        times: new FormControl(0)
      });
    }
  }

  public save() {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
      return;
    } else {
   
      if(!this.form.value.identity) {
        this.tags.markCheck();
      }

      this.form.markAllAsTouched();
    }
  }
  public close() {
	  if(!this.data) {
		  this.data = null
	 }

 	this.dialogRef.close(this.data);
}


public getTags(event) {
   this.form.value.identity = event;
} 

}
