import {Component, Input, Output, EventEmitter} from "@angular/core";
import {DynamicEventsRule} from "../../models/DynamicEventsRule";

@Component({selector: "rules-list", templateUrl: "./rules-list.component.html"})
export class RulesListComponent {
  @Output()emmitRules = new EventEmitter<any>();
  @Output()emmitUpdate = new EventEmitter<any>();
  @Output()openDialog = new EventEmitter<any>();

  @Input()rules: DynamicEventsRule[] = [];
  public displayedColumns: string[] = [
    "id",
    "symbol",
    "comp_value",
    "entity",
    "times",
    "delete",
    "update"
  ];

  constructor() {}

  open(): void {
    this.openDialog.emit(true);
  }

  delete(row): void {
    this.rules = this.rules.filter((item) => item.id != row.id);
    this.emmitRules.emit(this.rules);
  }

  update(row, index): void {
    this.emmitUpdate.emit({row, index});
  }
}
