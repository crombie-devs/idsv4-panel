import {Component, Output, EventEmitter, Input} from "@angular/core";
import {DynamicEventsService} from "src/modules/dynamic-event/services/dynamic-events.service";
import {IDynamicEvent} from "src/modules/dynamic-event/models/DynamicEvent";
import {PlayerService} from "src/modules/player/services/player.service";
import {IDynamicEventsRule} from "src/modules/dynamic-event/models/DynamicEventsRule";

@Component({selector: "block-events-list", templateUrl: "./block-events-list.component.html", styleUrls: ["./block-events-list.componet.scss"]})
export class BlockEventsListComponent {
  public events: IDynamicEvent[] = [];
  public idSocket = new Set();
  @Input() idcustomer: number = null;
  @Input() idsite: number = null;
  @Output()onClick: EventEmitter<any> = new EventEmitter();

  constructor(private _events : DynamicEventsService, private _player : PlayerService) {

    this._player.$subject.subscribe((idplayer : number) => {
      this._events.rulesByPlayer(this.idcustomer,  this.idsite, idplayer).subscribe((events : IDynamicEvent[]) => {
        if (Array.isArray(events)) {
          
          this.events = events;
        }
      });
    });

    this._player.$socketId.subscribe((socketId : string) => {
      
      this.idSocket.add(socketId);

    });
  }

  send(rule : IDynamicEventsRule): void {
    
    if (this.idSocket.size > 0) {
      this.onClick.emit({rule: rule, socketsIds: this.idSocket});
    }
  }
}
