import { IUser, User } from 'src/modules/user/models/user';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Role } from 'src/modules/user/models/role';
import { Permission } from 'src/modules/user/models/permission';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';

const STORE_KEY: string = 'user.store.ids';

@Injectable()
export class UserStore {
  private data: User = null;

  private $onChange: BehaviorSubject<User> = new BehaviorSubject(null);
  public onChange: Observable<User> = this.$onChange.asObservable();

  constructor() {
    this.load();
  }

  private load() {
    let store = sessionStorage.getItem(STORE_KEY);
    if (store) {
      const user: IUser = JSON.parse(store);
      this.set(user);
    }
  }

  private save(data: IUser) {
    this.data = data ? new User(data) : null;
    sessionStorage.setItem(STORE_KEY, JSON.stringify(data));
  }

  public get(): User {
    return this.data;
  }

  public set(data: IUser) {
    this.save(data);
    this.$onChange.next(this.data);
  }

  public isLogged() {
    return this.data != null && this.data != undefined;
  }

  public delete() {
    this.save(null);
    this.$onChange.next(this.data);
  }

  public hasRole(name: string) {
    if(!this.isLogged()){ return false; }
    return this.data.roles.some((role: Role) => role.name === name);
  }

  public hasPermission(id: number): boolean {
    if(!this.isLogged()){ return false; }

    return this.data.permissions.some(
      (permission: Permission) =>
        permission.id === id || permission.name === '*.*.*'
    );
  }

  public hasPermissionByName(name: string): boolean {
    if(!this.isLogged()){ return false; }

    return this.data.permissions && this.data.permissions.some(
      (permission: Permission) =>
        permission.name === name || permission.name === '*.*.*'
    );
  }

  public hasCircuit(circuit: PlayCircuit): boolean {
    if(this.data.idcircuits && this.data.idcircuits.length > 0 && this.hasRole('restricted')) {
      let circuits: number[] = this.data.idcircuits.split(',').map(value => parseInt(value));
      return circuits.some(idcircuit => idcircuit === circuit.idcircuit);
    }
    return true;
  }

  public has_influxes() {
    if(!this.isLogged()){ return false; }

    let res: boolean = false;
    if (this.data.customer) res = this.data.customer.has_influxes;
    else res = this.hasRole('superadmin');
    return res;
  }

  public has_sales() {
    if(!this.isLogged()){ return false; }

    let res: boolean = false;
    if (this.data.customer) res = this.data.customer.has_sales;
    else res = this.hasRole('superadmin');
    return res;
  }

  public has_audioasync() {
    if(!this.isLogged()){ return false; }

    let res: boolean = false;
    if (this.data.customer) res = this.data.customer.has_audioasync;
    else res = this.hasRole('superadmin');
    return res;
  }

  public has_algorithms() {
    if(!this.isLogged()){ return false; }

    let res: boolean = false;
    if (this.data.customer) res = (this.data.config_customer && this.data.config_customer.has_algorithms === 1) ? true : false;
    else res = this.hasRole('superadmin');
    return res;
  }

  public has_emissions() {
    if(!this.isLogged()){ return false; }
    let res: boolean = false;
    if (this.data.customer) res = this.data.customer.has_emissions;
    else res = this.hasRole('superadmin');
    return res;
  }

  public isSuperAdmin() {
    return this.data.isSuperadmin();
  }
  public isAdmin() {
    return this.data.isAdmin();
  }
  public isUser() {
    return this.data.isUser();
  }

  public getRoles() {
    let roles = [];
    if(this.data) 
      roles = this.data.roles;
    return roles;  
  }

  public getUser() {
    return this.data;
  }
}
