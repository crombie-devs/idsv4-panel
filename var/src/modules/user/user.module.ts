import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserRoutingModule } from './user-routing.module';
import { DirectivesModule } from 'src/directive/directives.module';
import { MaterialModule } from 'src/app/material.module';
import { UserListPage } from './pages/list/list.page';
import { UserFormPage } from './pages/form/form.page';
import { SharedModule } from 'src/shared/shared.module';
import { CustomerModule } from '../customer/customer.module';
import { UserStore } from './stores/user.store';
import { SiteModule } from '../site/site.module';
import {  SelectUserComponent } from './componets/select-mulptiple/select-multiple.component';
import { UserSelectComponent } from './componets/select/select.component';
@NgModule({
  declarations: [UserListPage, UserFormPage, SelectUserComponent, UserSelectComponent],
  imports: [
    CommonModule,
    FlexModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    DirectivesModule,
    UserRoutingModule,
    SharedModule,
    CustomerModule,
    SiteModule
  ],
  providers: [UserStore],
  entryComponents: [],
  exports:[ SelectUserComponent, UserSelectComponent]
})
export class UserModule {}