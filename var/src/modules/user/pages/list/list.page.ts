import { AfterViewInit, Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { User } from 'src/modules/user/models/user';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MYString } from 'src/libs/string.libs';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatInput } from '@angular/material/input';
import { from } from 'rxjs';
import { concatMap } from 'rxjs/operators';

@Component({
  selector: 'user-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss']
})

export class UserListPage implements AfterViewInit {

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
    @ViewChildren('table') table: QueryList<MatCheckbox>
    @ViewChild(MatInput) input: MatInput;
  
	public displayedColumns: string[] = ['select','iduser', 'name', 'email', 'customer', 'site'];
	public selection = new SelectionModel<User>(true, []);
	public dataSource = new MatTableDataSource<User>();
	public id: number;
	public me: any;
	public new: boolean = false;
	public edit: boolean = false;
	public deleted: boolean = false;
	public permission: boolean = false;
	public load: boolean = true;
	sortedData: User[];
	users: User[];
    private data: User[];

  	constructor(
		private ws: WSService,
		private debug: DebugStore,
		private _snackBar: MatSnackBar,
		private router: Router,
		private dialog: DialogService,
		private ustore: UserStore) {}
  
  	public ngOnInit() {
		this.debug.addInfo('UserListComponent', 'OnInit');
		this.me=this.ustore.getUser();
		this.getUsers();
	}

	private getUsers(){
		this.ws.users().subscribe((result: IResult) => {			
			if(result.success) {
				this.debug.addInfo('WS.users', 'successful');
				this.permission = true;	
                this.data = result.data;
				this.renderDataTable(result.data);
			}
			else {            
				this.debug.addError('WS.users', result.data);
				this.load = false;
				return null;
			}
		}, error => {
			this.debug.addError('WS.users', error.message);
			this.load = false;  
			return null;          
		}); 
	}
	  
	public renderDataTable(data: User []) {
		this.dataSource = new MatTableDataSource(data);
		this.dataSource.filterPredicate = (data: User, filter: string): boolean => {
			const dataStr = MYString.removeAccents(Object.keys(data).reduce((currentTerm: string, key: string) => {
			  return (currentTerm + (data as { [key: string]: any })[key] + '◬');
			}, '')).trim().toLowerCase();	  
			return dataStr.indexOf(MYString.removeAccents(filter.trim().toLowerCase())) != -1;
		};

		setTimeout(() => {
            this.dataSource.paginator = this.paginator;
        }, 500);
  
        if(this.input)
            this.applyFilter(this.input.value);

		this.sortedData = data.slice();
		this.users = data;
		this.dataSource.sort = this.sort;
		this.load = false;
		this.show();
	}


	ngAfterViewInit() {
    	setTimeout(() => {
			this.dataSource.paginator = this.paginator; 
			this.dataSource.sort = this.sort;
		  }, 500) 
	}

	isAllSelected() {
		if(this.dataSource.paginator){
			const numSelected = this.selection.selected.length;
			const page = this.dataSource.paginator.pageSize;
			let endIndex: number;
			if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
			endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
			} else {
			endIndex = this.dataSource.data.length - (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize);
			}
			return numSelected === endIndex;
		}
      }

    masterToggle() {
        this.isAllSelected() ?
          this.selection.clear() : this.selectRows();
    }

    selectRows() {
        let endIndex: number;
        if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
          endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
        } else {
          endIndex = this.dataSource.data.length;
        }        
        for (let index = (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize); index < endIndex; index++) {
          this.selection.select(this.dataSource.data[index]);
        }
    }

	public checkboxLabel(row?: User): string {
		if (!row) {
			return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
		}
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.iduser + 1}`;
	}

	public delete(){
        let usersByDelete = [];
		this.table.toArray().forEach(item => {
			if(item.checked) 
                usersByDelete.push(item.value);			
	    });	
       
		if(usersByDelete.length == 0){
			this.dialog.warning('Aviso', 'Debe seleccionar al menos un usuario').subscribe();
		}else{
			this.dialog.confirm('DIALOGS.confirm.title', 'COMPONENTS.user_list.confirm.text').subscribe(confirm => {
				if (confirm){
                    this.deleteByIds(usersByDelete);   
				}
			});
		}			
	}

	public update(){
		if (this.selection.selected.length>1){
			this.dialog.warning('Aviso', 'Sólo puede editar un usuario').subscribe();
		}else if(this.selection.selected.length == 0){
			this.dialog.warning('Aviso', 'Debe seleccionar un usuario').subscribe();
		}else{
			this.router.navigateByUrl('/private/user/edit/'+this.selection.selected[0].iduser);
		}
	}

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

	public show(){		 
		this.me.permissions.forEach(element => {
			if(element.name === "user.put.*" || element.name === "*.*.*"){
				this.new = true;
			}
			if(element.name === "user.delete.*" || element.name === "*.*.*"){
				this.edit = true;
			}
			if(element.name === "user.post.*" || element.name === "*.*.*"){
				this.deleted = true;
			}
		});
	}

    public applyFilter(event: string) {
        this.selection.clear();
        if(!event) 
            this.dataSource.data = this.data;	
        else{    
            if(typeof event == 'string'){
                this.dataSource.data = this.data.filter(user => 
                    user.name.toLowerCase().includes(event.trim().toLowerCase()) || 
                    user.iduser.toString().includes(event.trim()) || 
                    (user.email && user.email.toString().includes(event.trim())) || 
                    (user.site && user.site.name.toLowerCase().includes(event.trim().toLowerCase())) || 
					(user.customer && user.customer.name.toLowerCase().includes(event.trim().toLowerCase())) 
                );
            
            }
        }
	}

	sortData(sort: Sort) {
		const data = this.dataSource.data.slice();
		if (!sort.active || sort.direction === '') {
		  this.sortedData = data;
		  return;
		}
		this.sortedData = data.sort((a, b) => {
		  const isAsc = sort.direction === 'desc';
		  switch (sort.active) {
			case 'iduser': return this.compare(a.iduser, b.iduser, isAsc);
			case 'name': return this.compare(a.name, b.name, isAsc);
			case 'email': return this.compare(a.email, b.email, isAsc);
			case 'site': return this.compare(a.site.name, b.site.name, isAsc);
			case 'customer': return this.compare(a.customer.name, b.customer.name, isAsc);
			default: return 0;
		  }
		});
		this.dataSource.data = this.sortedData;
	}

	compare(a: number | string, b: number | string, isAsc: boolean) {
		return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
	}	

    private deleteByIds(ids: number[]) {
        from(ids).pipe(
          concatMap(id => this.ws.deleteUser(id))
        ).subscribe(player => {
            this.load = true;
            }, 
            error => {
                this.openSnackBar(error.message);
                this.getUsers();
            },
            () => {                       
                this.openSnackBar('Los usuarios seleccionados han sido borrados correctamente');                       
                this.selection.clear();
                this.getUsers();
          }); 
    }
	
}
