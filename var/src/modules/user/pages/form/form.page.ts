import { Component, ViewChild} from '@angular/core';
import { DebugStore } from 'src/stores/debug.store';
import { User } from 'src/modules/user/models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormControl} from '@angular/forms';
import { Rol } from 'src/modules/user/models/rol';
import { Customer } from 'src/modules/customer/models/customer';
import { Site } from 'src/modules/site/models/site';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { UserService } from 'src/modules/user/services/user.service';
import { ContentCategory } from 'src/modules/content/models/category';
import { SiteSelectMultipleComponent } from 'src/modules/site/components/select-multiple/select-multiple.component';


@Component({
  selector: 'user-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss']
})

export class UserFormPage {

	public iduser: number|null;
	public form: FormGroup;
    public roles: Rol[];
	public customers: Customer[];
	public sites: Site[];
	public loading: boolean = false;
	public user: User;
	public site: Site|null;
	public categories: ContentCategory[] = [];
	public circuits: PlayCircuit[] = [];
	public constensTemp: ContentCategory[] = [];
	public circuitsTemp: PlayCircuit[] = [];
	public idcustomer: number;
	public idsite:number;
	public showBipaCategories: boolean;
	public showSite: boolean;
	@ViewChild(SiteSelectMultipleComponent) siteCpnt: SiteSelectMultipleComponent;

   	constructor(
		private debug: DebugStore,
		private router: Router,
		private route: ActivatedRoute,
        private _snackBar: MatSnackBar,
		private _user: UserService) {
			this.iduser = Number(this.route.snapshot.paramMap.get('iduser'));
  	}
   
  	public ngOnInit() {            
		this._user.alert().subscribe(item => {
			let msj = JSON.stringify(item);
			const m = msj.split('1062')[1].split(' ');
			const me  = m[1] + ' ' + m[2] + ' ' + m[3];
			this.openSnackBar(me);
			this.loading = false;    
		})
		this.form = new FormGroup({
			name:  new FormControl('', [Validators.required]),
			email: new FormControl('', [Validators.email, Validators.required]),
			password: new FormControl('', []),
			role: new FormControl('', [Validators.required]),
			idcustomer: new FormControl(''),
			idsite: new FormControl(''),
			idcategories: new FormControl(''),
			idcircuits: new FormControl(''),
			iduser: new FormControl('')
		})
		this.debug.addInfo('UserFormComponent', 'OnInit');
		this.getRoles();
		if (this.iduser){
			this._user.get(this.iduser).subscribe((user: any) => {
            	if(user) {
					this.user=user.user;
					if(user.user.customer){
						this.idcustomer = user.user.customer.idcustomer;
						this.getSiteByCustomer(user.user.customer.idcustomer);
					}
					this.initForm(user.user);	
			   	}
			});
		}
	}  
   	public chargeSelect(idcustomer: number): void {   
		this.form.patchValue({idcustomer: idcustomer}); 
		this.getSiteByCustomer(idcustomer, true)
		if(this.categories.length <= 0 || this.circuits.length <= 0) {   
        	this._user.customerCircuits([]).subscribe(circuits => {
			this.circuits = circuits.filter(item => item.idcustomer == idcustomer);
			this.circuitsTemp = [...this.circuits];
	    });
		this._user.customerContents([]).subscribe(contents => {
			this.categories = contents.filter(item => item.idcustomer == idcustomer);
			this.constensTemp = [...this.categories];
		});
		}  else {
	  		this.circuits = this.circuitsTemp.filter(item => item.idcustomer == idcustomer);
	  		this.categories = this.constensTemp.filter(item => item.idcustomer == idcustomer);
    	}
	}
	private initForm(data: User|null){		
		if (data){
			this.site = data.site ? data.site : null;	
				this.form.setValue({
				idcustomer: data.customer ? data.customer.idcustomer.toString() : '',
				idsite: data.site ? data.site.idsite.toString() : '',
				name: data.name,
				email: data.email,
				role: data.roles[0].name,
				idcategories: data.idcategories ? data.idcategories.split(',') : null,
				idcircuits: data.idcircuits ? data.idcircuits.split(','): null,
				iduser: this.iduser,
				password: ''
	     	});
            this.showBipaCategories = data.roles.some(d => d.name == 'restricted');
		    this.showSite =  (data.roles[0].name == 'user') ? true : false;
            this.chargeSelect(this.form.value.idcustomer);
		} 
	}
	private getRoles(){
     	this._user.roles([]).subscribe((result: any) => {
			if(result) {
		    	this.roles = result;
			} 
		});
	}
	setSite(event): void {
		console.log(event)
		this.form.patchValue({idsite: event});
	}
	typeRole(event): void {
		this.showBipaCategories = (event.value == 'restricted') ? true : false;
		this.form.value.site = !this.showBipaCategories ? null :  this.form.value.site;
		this.showSite = (event.value == 'user') ? true : false;
	}
	close():void {
     	let url = '../list';
		if(this.iduser) {
          url = '../../list';
	     }
        this.router.navigate([url], { relativeTo: this.route })
	}
	public getSiteByCustomer(idcustomer: number, select: boolean = false){
		if(idcustomer!=null){          
            if(this.siteCpnt) {
				this.siteCpnt.filterByCustomer(idcustomer);
			} 
			this.form.get('idsite').enable();
			this.site && !select? this.form.patchValue({ idsite: this.site.idsite.toString()}):null;								
			select?	this.form.patchValue({ idsite: ""}):null;
		} else
			this.form.get('idsite').disable();
	}
	public create(){         
		if (this.form.invalid) return null;
		this.loading = true;
		if(this.form.value.idcategories) {
          this.form.value.idcategories =  this.form.value.idcategories.join(',');
        }
		if(this.form.value.idcircuits) {
			this.form.value.idcircuits =  this.form.value.idcircuits.join(',');
		}
		if(this.form.value.idsite) {
			this.form.value.idsite =  this.form.value.idsite ?  this.form.value.idsite : null;	
		}
       	this._user.save(this.form.value).subscribe((item: any) => {  
			this.openSnackBar("Usuario creado correctamente");
			if(this.iduser) {
				this.router.navigate(['../../list'],{relativeTo: this.route });
			} else {
				this.router.navigate(['../list'],{relativeTo: this.route });
			}
		    this.loading = false;	 
		});
	}
	public compareCustomer(x: Customer, y: Customer): boolean {
		return x && y ? x.idcustomer === y.idcustomer : x === y;
	}
	public compareSite(x: Site, y: Site): boolean {
		return x && y ? x.idsite === y.idsite : x === y;
	}
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
}
