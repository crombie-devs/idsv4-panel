import { Component, OnInit, Input,Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormControl,  FormGroup} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserStore } from '../../stores/user.store';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'user-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class UserSelectComponent implements OnInit {

	@Output() onSelected = new EventEmitter<number>();
   	@Input() usersToSelect:boolean;
	@Input() selected: number = null;
	@Input() apparence: boolean = true;
	@Input() iduser: number;


	public users:User[] = [];
	public filterForm:FormGroup;
	public superAdmin:boolean = false;

  	constructor(
        private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private _user: UserService
	) {	
        this.filterForm = new FormGroup({
			user: new FormControl('')
		})

    	
    }

	ngOnInit(): void {
		this.verifyPermissions();
		
		setTimeout(() =>{
			this.getusers();
		}, 500)
	
    }
  
	private getusers(){
		if(this.superAdmin){
			this._user.list([]).subscribe((result: any) => {
				if(result) {
					
					this.users = result;
                    console.log(this.users) 
					if(this.iduser){
						
		        		this.filterForm.patchValue({user: this.iduser+''})
					}


				} else {            
					this.openSnackBar(result);
				}
			}, error => {
				
			}); 
		}
	}

    


  
  	public captureValue() {
		let iduser = this.filterForm.value.user;
		this.onSelected.emit(iduser);
	}
	
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
	
	private verifyPermissions() {
		this.superAdmin = this.ustore.isSuperAdmin();
	}

	public ngOnChanges(changes: SimpleChanges): void {
		if(changes.selected)
			this.filterForm.get('user').setValue(changes.selected.currentValue);
	}

}
