import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FormControl,  Validators , FormGroup} from '@angular/forms';
import {  IUser } from '../../models/user';
import { UserService } from '../../services/user.service';


Validators
@Component({
  selector: 'select-users',
  templateUrl: './select-multiple.component.html',
  styleUrls: ['./select-multiple.component.scss'],
})
export class SelectUserComponent {
  @Output() onClick: EventEmitter<any> = new EventEmitter();
  @Output() onChange: EventEmitter<number> = new EventEmitter();
  @Input() iduser: number;
  curstomer: FormControl;
  form: FormGroup;

  users: IUser[] = [];

  constructor(private _user: UserService) {
    this.form = new FormGroup({
      iduser: new FormControl('', Validators.required)
    })
    this._user.toSelect().subscribe((items: IUser[]) => {
      this.users = items;
      if(this.iduser)
      {
        this.form.patchValue({iduser: this.iduser})
      }
    });
  }

  onSelect(event): void {
    this.onClick.emit(event);
    this.onChange.emit(event.value);
  }

  
}
