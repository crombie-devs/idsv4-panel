import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListPage } from './pages/list/list.page';
import { UserFormPage } from './pages/form/form.page';


const routes: Routes = [
  
  { path: 'list', component: UserListPage, data: { breadcrumb: { label: 'Usuarios', info:  'Usuarios' }}},
  { path: 'edit/:iduser', component: UserFormPage, data: { breadcrumb: 'Editar Usuario' }},
  { path: 'create', component: UserFormPage, data: { breadcrumb: 'Nuevo Usuario' }},
  { path: '', pathMatch: 'full', redirectTo: 'create'}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class UserRoutingModule { }
