export interface IRole {
  id: number;
  name: string; 
  guard_name: string 
}

export class Role implements IRole {
  id: number;
  name: string; 
  guard_name: string

	constructor(obj?: IRole) {
		if(!obj)
      return;

    this.id = obj.id;
    this.name = obj.name;
    this.guard_name = obj.guard_name;
    
  }
}
