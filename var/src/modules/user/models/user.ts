import { IRol, Rol } from "src/modules/user/models/rol";
import { IPermission, Permission } from "src/modules/user/models/permission";
import { ISite, Site } from "src/modules/site/models/site";
import { Customer, ICustomer } from "src/modules/customer/models/customer";
import { ConfigCustomer, IConfigCustomer } from 'src/modules/customer/models/config-customer';

export interface IUser {
    iduser: number;
    name: string;
    email: string;
    roles: IRol[];
    permissions: IPermission[];
    site: ISite;
    customer: ICustomer;
    status: boolean;
    idcategories?: string;
    idcircuits?: string;
    config_customer: IConfigCustomer;
    langs: [];
}

export class User implements IUser {
    iduser: number;
    name: string;
    email: string;
    roles: Rol[];
    permissions: Permission[];
    site: Site;
    customer: Customer;
    status: boolean;
    idcategories?: string;
    idcircuits?: string;
    config_customer: IConfigCustomer;
    langs: [];


    constructor(obj?: IUser) {
        if (!obj)
            return;

        this.iduser = obj.iduser;
        this.name = obj.name;
        this.email = obj.email;
        if (obj.roles)
            this.roles = obj.roles.map(o => new Rol(o));
        if (obj.permissions)
            this.permissions = obj.permissions.map(o => new Permission(o));
        this.site = (obj.site) ? new Site(obj.site) : null;
        this.customer = (obj.customer) ? new Customer(obj.customer) : null;
        this.status = obj.status;
        this.idcategories = obj.idcategories ? obj.idcategories : null;;
        this.idcircuits = obj.idcircuits ? obj.idcircuits : null;
        this.config_customer = obj.config_customer;
        this.langs = obj.langs ? obj.langs : [];

    }

    isSuperadmin(): boolean {
        return (this.customer == null && this.site == null);
    }

    isAdmin(): boolean {
        return (this.customer != null && this.site == null);
    }

    isUser(): boolean {
        return (this.customer != null && this.site != null);
    }
}
