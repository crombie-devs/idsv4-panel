export interface IRol {
  id: number;
  name:string; 
}

export class Rol implements IRol {
  id: number;
  name:string; 

	constructor(obj?: IRol) {
		if(!obj)
      return;

    this.id = obj.id;
    this.name = obj.name;
    
  }
}
