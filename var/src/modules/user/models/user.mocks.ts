import { User } from './user';

export const userMock:any = {
    iduser:2,
    name: 'adminbp',
    email: 'adminbp@ladorian.com',
    customer: {
        idcustomer: 31,
        name: "BP",
        email: "contacto@bp.com",
        phone: "93565858",
        image_url: "https://ids.ladorianids.es/customers/BP-logo-blanco.png",
        gaid: "null",
        status: true,
        has_sales: true,
        has_influxes: true,
        has_emissions: true,
        influxes_type: "Ladorian Básico",
        category_level: "N3",
        has_trends: true,
        streaming: 2,
        has_audioasync: false,
        max_file_size: 100
    },
    permissions: [
        {
          id: 2,
          name: "get.customer.*"
        },
        {
          id: 4,
          name: "get.sites"
        },
        {
          id: 5,
          name: "get.site.*"
        },
        {
          id: 6,
          name: "put.site.*"
        },
        {
          id: 7,
          name: "get.players"
        },
        {
          id: 8,
          name: "get.player.*"
        },
        {
          id: 9,
          name: "put.player.*"
        },
        {
          id: 10,
          name: "get.tags.toselect"
        },
        {
          id: 11,
          name: "get.tags"
        },
        {
          id: 12,
          name: "get.tag.*"
        },
        {
          id: 13,
          name: "put.tag.*"
        },
        {
          id: 14,
          name: "post.tag"
        },
        {
          id: 15,
          name: "delete.tag.*"
        },
        {
          id: 16,
          name: "get.tagcategories.toselect"
        },
        {
          id: 17,
          name: "get.tagcategories"
        },
        {
          id: 18,
          name: "get.tagcategory.*"
        },
        {
          id: 19,
          name: "put.tagcategory.*"
        },
        {
          id: 20,
          name: "post.tagcategory"
        },
        {
          id: 21,
          name: "delete.tagcategory.*"
        },
        {
          id: 22,
          name: "get.cameras"
        },
        {
          id: 23,
          name: "get.camera.*"
        },
        {
          id: 24,
          name: "put.camera.*"
        },
        {
          id: 25,
          name: "get.playareas"
        },
        {
          id: 26,
          name: "get.playarea.*"
        },
        {
          id: 27,
          name: "get.playareas.toselect"
        },
        {
          id: 28,
          name: "get.format.*"
        },
        {
          id: 29,
          name: "get.formats.toselect"
        },
        {
          id: 30,
          name: "get.lang.*"
        },
        {
          id: 31,
          name: "get.langs.toselect"
        },
        {
          id: 32,
          name: "get.indoorlocations.toselect"
        },
        {
          id: 33,
          name: "get.indoorlocations"
        },
        {
          id: 34,
          name: "get.indoorlocation.*"
        },
        {
          id: 35,
          name: "put.indoorlocation.*"
        },
        {
          id: 36,
          name: "post.indoorlocation"
        },
        {
          id: 37,
          name: "delete.indoorlocation.*"
        },
        {
          id: 38,
          name: "get.playcircuits"
        },
        {
          id: 39,
          name: "get.playcircuit.*"
        },
        {
          id: 40,
          name: "post.playcircuit.*"
        },
        {
          id: 41,
          name: "post.playcircuit"
        },
        {
          id: 42,
          name: "delete.playcircuit.*"
        },
        {
          id: 43,
          name: "get.countries.toselect"
        },
        {
          id: 44,
          name: "get.provinces.toselect"
        },
        {
          id: 45,
          name: "get.cities.toselect"
        },
        {
          id: 46,
          name: "get.content-types.toselect"
        },
        {
          id: 47,
          name: "get.content-categories.toselect"
        },
        {
          id: 48,
          name: "get.content-category.*"
        },
        {
          id: 49,
          name: "get.content-categories"
        },
        {
          id: 50,
          name: "post.content-category.*"
        },
        {
          id: 51,
          name: "post.content-category"
        },
        {
          id: 52,
          name: "delete.content-category.*"
        },
        {
          id: 53,
          name: "get.asset-types.toselect"
        },
        {
          id: 54,
          name: "get.assets.toselect"
        },
        {
          id: 55,
          name: "get.assets"
        },
        {
          id: 56,
          name: "get.asset.*"
        },
        {
          id: 57,
          name: "post.asset.*"
        },
        {
          id: 58,
          name: "post.asset"
        },
        {
          id: 59,
          name: "delete.asset.*"
        },
        {
          id: 60,
          name: "get.contents.toselect"
        },
        {
          id: 61,
          name: "get.content.*"
        },
        {
          id: 62,
          name: "get.contents"
        },
        {
          id: 63,
          name: "put.content.*"
        },
        {
          id: 64,
          name: "post.content"
        },
        {
          id: 65,
          name: "delete.content.*"
        },
        {
          id: 66,
          name: "get.playlogics.toselect"
        },
        {
          id: 67,
          name: "get.playlogics"
        },
        {
          id: 68,
          name: "get.playlogic.*"
        },
        {
          id: 69,
          name: "put.playlogic.*"
        },
        {
          id: 70,
          name: "post.playlogic"
        },
        {
          id: 71,
          name: "delete.playlogic.*"
        },
        {
          id: 72,
          name: "get.playcircuits.toselect"
        },
        {
          id: 73,
          name: "post.playarea"
        },
        {
          id: 74,
          name: "put.playarea.*"
        },
        {
          id: 75,
          name: "delete.playarea.*"
        },
        {
          id: 76,
          name: "post.contents.toemission"
        },
        {
          id: 77,
          name: "post.format"
        },
        {
          id: 78,
          name: "post.player"
        },
        {
          id: 79,
          name: "delete.player.*"
        },
        {
          id: 80,
          name: "get.player.toselect"
        },
        {
          id: 81,
          name: "get.cameras.toselect"
        },
        {
          id: 82,
          name: "put.playcircuit.*.sites"
        },
        {
          id: 83,
          name: "put.playcircuit.*.site.*"
        },
        {
          id: 84,
          name: "put.playarea.*.playlogics"
        },
        {
          id: 85,
          name: "delete.playarea.*.playlogic.*"
        },
        {
          id: 86,
          name: "put.contents.order"
        },
        {
          id: 87,
          name: "put.playcircuit.*.langs"
        },
        {
          id: 88,
          name: "put.playcircuit.*.lang.*"
        },
        {
          id: 89,
          name: "get.playareas.touser"
        },
        {
          id: 90,
          name: "get.langs.touser"
        },
        {
          id: 91,
          name: "get.formats.touser"
        },
        {
          id: 92,
          name: "put.playcircuit.*.tags"
        },
        {
          id: 93,
          name: "put.playcircuit.*.tag.*"
        },
        {
          id: 94,
          name: "put.playcircuit.*.countries"
        },
        {
          id: 95,
          name: "put.playcircuit.*.country.*"
        },
        {
          id: 96,
          name: "put.playcircuit.*.provinces"
        },
        {
          id: 97,
          name: "put.playcircuit.*.province.*"
        },
        {
          id: 98,
          name: "put.playcircuit.*.cities"
        },
        {
          id: 99,
          name: "put.playcircuit.*.city.*"
        },
        {
          id: 100,
          name: "put.player.*.playlogics"
        },
        {
          id: 101,
          name: "delete.player.*.playlogic.*"
        },
        {
          id: 102,
          name: "put.player.*.contents.*"
        },
        {
          id: 103,
          name: "put.playarea.*.contents.*"
        },
        {
          id: 104,
          name: "get.contents.order"
        },
        {
          id: 105,
          name: "delete.customer.*.format.*"
        },
        {
          id: 106,
          name: "post.site"
        },
        {
          id: 107,
          name: "put.site.*.holidays"
        },
        {
          id: 108,
          name: "delete.site.*.holiday.*"
        },
        {
          id: 109,
          name: "put.site.*.tags"
        },
        {
          id: 110,
          name: "delete.site.*.tag.*"
        },
        {
          id: 111,
          name: "post.camera"
        },
        {
          id: 112,
          name: "delete.camera.*"
        },
        {
          id: 113,
          name: "put.player.*.powers"
        },
        {
          id: 114,
          name: "delete.player.*.power.*"
        },
        {
          id: 115,
          name: "put.camera.*.powers"
        },
        {
          id: 116,
          name: "delete.camera.*.power.*"
        },
        {
          id: 117,
          name: "put.customer.*.formats"
        },
        {
          id: 122,
          name: "delete.playarea.*.force"
        },
        {
          id: 123,
          name: "delete.indoorlocation.*.force"
        },
        {
          id: 124,
          name: "delete.site.*"
        },
        {
          id: 125,
          name: "post.format.*"
        },
        {
          id: 126,
          name: "delete.format.*"
        },
        {
          id: 127,
          name: "get.formats"
        },
        {
          id: 128,
          name: "delete.asset.*.force"
        },
        {
          id: 129,
          name: "post.support"
        },
        {
          id: 132,
          name: "post.support.camera"
        },
        {
          id: 133,
          name: "post.support.player"
        },
        {
          id: 136,
          name: "get.templates"
        },
        {
          id: 137,
          name: "get.template.*"
        },
        {
          id: 139,
          name: "post.playareas.tocreateplaylist"
        },
        {
          id: 140,
          name: "get.landingpages"
        },
        {
          id: 141,
          name: "get.landingpage.*"
        },
        {
          id: 142,
          name: "put.landingpage.*"
        },
        {
          id: 143,
          name: "delete.landingpage.*"
        },
        {
          id: 144,
          name: "get.audios"
        },
        {
          id: 145,
          name: "post.audios.*"
        },
        {
          id: 146,
          name: "put.audios.*"
        },
        {
          id: 147,
          name: "delete.audios.*"
        },
        {
          id: 148,
          name: "get.audiocategory.*"
        },
        {
          id: 149,
          name: "post.audiocategory.*"
        },
        {
          id: 150,
          name: "put.audiocategory.*"
        },
        {
          id: 151,
          name: "delete.audiocategory.*"
        },
        {
          id: 152,
          name: "get.audio.*"
        },
        {
          id: 153,
          name: "post.audio.*"
        },
        {
          id: 154,
          name: "put.audio.*"
        },
        {
          id: 155,
          name: "delete.audio.*"
        }
      ],
      roles: [
        {
          id: 2,
          name: "admin"
        }
      ]
}

export const mockUsers: User[] = [ userMock ];