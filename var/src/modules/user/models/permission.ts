export interface IPermission {
  id: number;
  name: string; 
  guard_name: string 
}

export class Permission implements IPermission {
  id: number;
  name: string; 
  guard_name: string

	constructor(obj?: IPermission) {
		if(!obj)
      return;

    this.id = obj.id;
    this.name = obj.name;
    this.guard_name = obj.guard_name;
    
  }
}
