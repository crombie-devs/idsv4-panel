import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { ICrudWS } from '../../../interfaces/ICrudWS.interface';
import { HttpService } from '../../../services/http.service';
import { IUser } from '../models/user';


@Injectable({
  providedIn: 'root',
})
export class UserService implements ICrudWS<IUser>{

events: Subject<IUser[]> = new Subject();
$events = this.events.asObservable();

constructor(private http: HttpService) {}

 alert (): Observable<any>  {
   return this.http.$alert;
 } 


 public save(obj: IUser): Observable<IUser> {
   if(obj.hasOwnProperty('iduser') && obj.iduser) {
     return this.update(obj);
   } else {
     return this.create(obj);
   }
 } 
  public create(obj: IUser): Observable<IUser> {
    return this.http.create('private/user', obj, 'WS.user_add');
  }
  public update(obj: IUser): Observable<IUser> {
    return this.http.update(`private/user/${obj.iduser}`, obj, 'WS.user_update');
  }
  public delete(id: number): Observable<boolean> {
    return this.http.delete('private/user', [id], 'WS.user_delete');
  }
  public get(id: number): Observable<IUser> {
    return this.http.get('private/user', [id], 'WS.user');
  }
  public toSelect(args?: any[]): Observable<IUser[]> {
       return  this.http.get('private/users/toselect', args, 'WS.user');
  }
 public list(args: any[])  : Observable<IUser[]> {
    return  this.http.get('private/users', args, 'WS.user');
 }
 public roles(args: any[]): Observable<any> {
    return  this.http.get('private/roles', args, 'WS.user_roles');
 }
 public customers(args: any[]): Observable<any> {
  return  this.http.get('private/customers', args, 'WS.user_customers');
 }
 public getCustomer(args: number[]): Observable<any> {
  return  this.http.get(`private/customer`, args, 'WS.user_customers');
 }
 public customerCircuits(args: any[]): Observable<any>  {
    return  this.http.get('private/playcircuits', args, 'WS.user');
 }

 public customerContents(args: any[]): Observable<any>  {
    return  this.http.get('private/content-categories', args, 'WS.user');
 }






 




}


