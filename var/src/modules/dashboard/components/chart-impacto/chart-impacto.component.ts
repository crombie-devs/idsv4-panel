import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import moment from 'moment';
import { ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'dashboard-chart-impacto',
  templateUrl: './chart-impacto.component.html',
  styleUrls: ['./chart-impacto.component.scss']
})

export class ChartImpactoComponent implements OnInit {
  public load: boolean=true;
  public url: string = '';
  public frame: any;
  public isLoading: boolean = true;
  @Input() ventas: any = [];
  @Input() emissions: any = [];

  lineChartData: ChartDataSets[] = [{label : this.translate.instant('Emisiones'), data : [0], yAxisID: 'y-axis-1'}, {label : this.translate.instant('Ventas'), data : [0], yAxisID: 'y-axis-2'}];
  lineChartLabels: Label[] = [];

  constructor(
    private translate: TranslateService) {      
    }
  
    public panelOpenState = false;
    public status: boolean = false;
    public me: any;
    public titleBreadcrumbs: string;

    public step = 0;

    public ngOnInit(): void {
      this.processAllVentas();
    }
    public ngOnChanges(changes: SimpleChanges){
        this.processAllVentas();
    }

    private processAllVentas(){
      if(this.emissions.length > 0){
        this.lineChartLabels = [];
        let finalData = [];
        this.emissions.map((emission,index) => {
          if(this.translate.currentLang === 'es')
            this.lineChartLabels.push(moment(emission.date).format("DD/MM"));
          else
            this.lineChartLabels.push(moment(emission.date).format("MM/DD"));
          finalData.push(emission.count);
        })
        this.lineChartData[0].data = finalData;
      }
      if(this.ventas.length > 0){
        let finalData = [];
        let lastpos = 0;
        this.ventas.map(venta => {
          const pos = parseInt(moment(venta.day).format('D')) - 1;
          if(pos - lastpos > 0){
            do{
              lastpos++
              finalData[lastpos] = 0;
            }while(pos - lastpos > 0)
          }
          finalData[pos] = venta.count;
          lastpos = pos;
        })
        this.lineChartData[1].data = finalData;
      }
      this.isLoading = false;  
    }
}

