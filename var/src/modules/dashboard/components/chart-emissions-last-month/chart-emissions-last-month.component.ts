import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import moment from 'moment';
import { ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { WSService } from 'src/services/ws.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import {TranslateService} from '@ngx-translate/core';
import { CONFIG } from 'src/app/app.config';

@Component({
  selector: 'dashboard-chart-emissions-last-month',
  templateUrl: './chart-emissions-last-month.component.html',
  styleUrls: ['./chart-emissions-last-month.component.css']
})

export class ChartEmissionsLastMonthComponent implements OnInit {
  public load: boolean=true;
  public url: string = '';
  public frame: any;
  public isLoading: boolean = true;
  public emissions:any[] = [];
  @Input() store:number;

  lineChartData: ChartDataSets[] = [{label : this.translate.instant('Emisiones'), data : [0]}];
  lineChartLabels: Label[] = [];

  constructor(
    private ws: WSService,
    private ustore: UserStore,
    private translate: TranslateService) {      
  }
  
    public me: any;

    public ngOnInit(): void {
      this.me = this.ustore.getUser();
      this.getEmisions();
    }
    private getEmisions(){
      const year = moment().format("YYYY");
      const month = moment().subtract(1, 'months').format("MM");
      let params:any = {
        date: `${year}-${month}`,
        cos: this.me.site || this.store ? 'site' : 'customer'
      }
      this.ws.dashboardEmisionesCountByDate(params).subscribe((result: any) => {
        const idcustomer = (!CONFIG.demo && this.me.customer.idcustomer !== 31) ? this.me.customer.idcustomer : 31;
        const fromCustomer = result.filter(data => data.idcustomer === idcustomer);
        this.emissions = fromCustomer[0].dates;
        this.processAllEmissions();			
      });
    }
    private processAllEmissions(){
      if(this.emissions.length > 0){
        this.lineChartLabels = [];
        let finalData = [];
        this.emissions.map((emission,index) => {
          this.lineChartLabels.push(moment(emission.date).format("MM-DD"));
          finalData.push(emission.count);
        })
        this.lineChartData[0].data = finalData;
      }
      this.isLoading = false;  
    }
}

