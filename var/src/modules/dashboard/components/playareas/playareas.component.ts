import { Component, OnInit } from '@angular/core';
import { WSService } from 'src/services/ws.service';

@Component({
  selector: 'dashboard-playareas',
  templateUrl: './playareas.component.html',
  styleUrls: ['./playareas.component.css']
})

export class PlayareasComponent implements OnInit {

  public load: boolean = true;
  public playareas:number;

  constructor(private ws: WSService) {}
  
    public ngOnInit(): void {
      this.getPlayareas()
    } 

    private getPlayareas(){
      this.ws.playareas().subscribe(result => {
        if(result.success){
          this.playareas = result.data.length;
          this.load = false;
        }
      })
    }
}

