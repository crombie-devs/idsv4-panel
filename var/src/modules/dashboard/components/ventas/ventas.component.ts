import { Component, OnInit, ViewChild, Input, SimpleChanges } from '@angular/core';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { UserStore } from 'src/modules/user/stores/user.store';
@Component({
  selector: 'dashboard-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})

export class VentasComponent implements OnInit {

  @Input() ventas: SimpleChanges;
  public load: boolean = false;

  constructor(
    private store: AuthStore,  
    private localStorage: MYLocalStorage,
    private ws: WSService,
    private debug: DebugStore,
    private dialog: DialogService,
    private ustore: UserStore) {}  
    

    public showSubmenu: boolean = false;
    public showSubSubMenu: boolean = false;
  
    public panelOpenState = false;
    public status: boolean = false;
    public me: any;
    public titleBreadcrumbs: string;

    public step = 0;

  
    public ngOnInit(): void {
    }
    public ngOnChanges(changes: SimpleChanges){
      if(!changes.ventas.isFirstChange())
        this.load = false;
    }
}

