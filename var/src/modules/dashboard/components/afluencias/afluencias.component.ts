import { Component, OnInit, ViewChild, Input, SimpleChanges } from '@angular/core';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { Emision } from 'src/modules/emission/models/emision';
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { User } from 'src/modules/user/models/user';
import { UserStore } from 'src/modules/user/stores/user.store';
import moment from 'moment';

@Component({
  selector: 'dashboard-afluencias',
  templateUrl: './afluencias.component.html',
  styleUrls: ['./afluencias.component.scss']
})

export class AfluenciasComponent implements OnInit {

  @Input() afluencias: any = '0';
  public load: boolean = true;
  public counter: any = '0';

  constructor(
    private ustore: UserStore) {
      
    }

    public showSubmenu: boolean = false;
    public showSubSubMenu: boolean = false;
  
    public panelOpenState = false;
    public status: boolean = false;
    public me: any;
    public titleBreadcrumbs: string;

    public step = 0;

  
    public ngOnInit(): void {
      this.me = this.ustore.getUser();
    }

    public ngOnChanges(changes: SimpleChanges){
      if(!changes.afluencias.isFirstChange())
        this.getAfluencias();
    }

    public getAfluencias() {
      this.counter =  new Intl.NumberFormat("de-DE").format(this.afluencias);
      this.load = false;  
    }
}

