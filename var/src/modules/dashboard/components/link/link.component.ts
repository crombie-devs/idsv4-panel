import { Component, OnInit, ViewChild, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'dashboard-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss']
})

export class LinkDashboardComponent implements OnInit {

  @Input() title:string;
  @Input() icon:string;
  @Input() link:string;
 
  constructor() {
  }

  public ngOnInit(): void {
  }
}

