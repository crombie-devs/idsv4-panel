import { Component, OnInit } from '@angular/core';
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { Camera } from 'src/modules/camera/models/camera';

import { UserStore } from 'src/modules/user/stores/user.store';
import moment from 'moment';
import { CameraStore } from 'src/modules/camera/stores/camera.store';
import { CONFIG } from 'src/app/app.config';

@Component({
  selector: 'dashboard-camera-player',
  templateUrl: './camera-player.component.html',
  styleUrls: ['./camera-player.component.scss']
})

export class CameraPlayerComponent implements OnInit {
  public loading:boolean = true;
  public camera: Camera = null;
  public email: string = null;
  public hombres: number = 320;
  public mujeres: number = 289;
  public config = CONFIG;
  public streaming : number = 0;
 
  constructor(private ws: WSService, private me: UserStore, public cameras: CameraStore) {}

    ngOnInit(): void {
      this.loadCamera();
      const user = this.me.getUser();
      this.streaming = user.customer.streaming;
      this.streaming = 2;
    }

    changeCamera(camera: Camera = null) {
      if (!camera) {
       let camera:any = {};
       camera.email = 'icreative@ladorian.com';
      }
      this.email = null;
      if(CONFIG.demo)  {
            camera.email = 'icreative@ladorian.com';
            this.streaming = 2;
      }
      
      setTimeout(() => { this.email = camera.email; }, 100);
      this.loadEmision();
    }

    private loadCamera() {
      this.ws.getCamerasFromPusher().subscribe(result => {
        if(result.success){
            const cameras = result.data;
            this.cameras.onChange.subscribe(data => {
              let camsToRand:Camera[] = [];
              data.map(cam => {
                  if(cameras.some(c => c === `camera-${cam.email}` || c === `camera.email.${cam.email}`))
                    camsToRand.push(cam);
              })
              if(camsToRand.length > 0){
                const index: number = Math.floor(Math.random() * camsToRand.length);
                this.camera = camsToRand[index];
                this.changeCamera(this.camera);
              }
            });
        }
      })
    }

    private loadEmision(day = null){
      if(!day)
        var today = moment().format("YYYY-MM-DD"); 
      else
        var today = moment(day).format("YYYY-MM-DD");

      let params: any = 
      {
        filter: {
            customer_id: this.me.get().customer.idcustomer,
            date:today
        },
        group: {
            _id:{gender: "$gender"},
            count: {"$sum": 1}
        }
    }
    if(this.me.get().site){ params.filter.site_id = this.me.get().site.idsite }
      this.loading = true;
        this.ws.detailFacesByCamera(params).subscribe((result: IResult) => {
          this.loading = false;
          if(result.success) 
            if(result.data.length > 0)
              this.processAllEmision(result.data); 
        });
    }
  
    private processAllEmision(elements: any[], day:string = 'today'){
      elements.map(e => {
        e.gender = e._id.gender;
        delete e._id;
      })
      let hombres = elements.filter(emision => emision.gender === 'male');
      let mujeres = elements.filter(emision => emision.gender === 'female');
      hombres[0] ? this.hombres = hombres[0].count : 0;
      mujeres[0] ? this.mujeres = mujeres[0].count : 0;    
      if(CONFIG.demo){
        this.hombres += 320;
        this.mujeres += 289;
      }
    }
}

