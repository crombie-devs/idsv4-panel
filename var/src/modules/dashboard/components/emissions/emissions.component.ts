import { Component, OnInit, ViewChild, Input, SimpleChanges } from '@angular/core';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { Emision } from 'src/modules/emission/models/emision';
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { User } from 'src/modules/user/models/user';
import { UserStore } from 'src/modules/user/stores/user.store';
import moment from 'moment';

@Component({
  selector: 'dashboard-emissions',
  templateUrl: './emissions.component.html',
  styleUrls: ['./emissions.component.css']
})

export class EmissionsComponent implements OnInit {

  @Input() emissions: SimpleChanges;
  public load: boolean=true;

  constructor(
    private store: AuthStore,  
    private localStorage: MYLocalStorage,
    private ws: WSService,
    private debug: DebugStore,
    private dialog: DialogService,
    private ustore: UserStore) {
      
    }

    public showSubmenu: boolean = false;
    public showSubSubMenu: boolean = false;
  
    public panelOpenState = false;
    public status: boolean = false;
    public me: any;
    public titleBreadcrumbs: string;

    public step = 0;

  
    public ngOnInit(): void {
      this.me=this.ustore.getUser();
    }

    public ngOnChanges(emissions: SimpleChanges){
      if(!emissions.emissions.firstChange)
        this.load = false;
    }    
  
}

