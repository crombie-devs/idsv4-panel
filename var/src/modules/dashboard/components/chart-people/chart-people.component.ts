import { Component, OnInit } from '@angular/core';
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { CONFIG } from 'src/app/app.config';
// For graphics
import { ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'dashboard-chart-people',
  templateUrl: './chart-people.component.html',
  styleUrls: ['./chart-people.component.css']
})

export class ChartPeopleComponent implements OnInit {
  public url: string = '';
  public frame: any;
  public isLoading: boolean = true;
	public me: any;

  lineChartData: ChartDataSets[] = [{label : this.translate.instant("Ayer"), data : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}, {label : this.translate.instant("Hoy"), data : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}];
  lineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];

  constructor(
    private ws: WSService,
    private ustore: UserStore,
    private translate: TranslateService) {      
  }
  
  public ngOnInit(): void {
    this.me = this.ustore.getUser();
    this.getEmision();
  }
  public getEmision(){
    this.isLoading = true; 
    let today = moment().format("YYYY-MM-DD"); 
    let yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");
    let params: any = { 
      filter:
      {
        date: {
          "$lte":today, 
          "$gte":yesterday
        }, 
        customer_id: this.me.customer.idcustomer 
      },
      group:{
        _id:{hour: "$hour", date:"$date"},
        count:{ "$sum": 1}
      },
      sort:{
        date:1,
        hour:1
      }
    }

    if(this.me.site){
      params.filter.site_id = this.me.site.idsite
    }
    if(this.me.customer.idcustomer === 113)
      params.filter.customer_id = 22;
    this.ws.detailFacesByCamera(params, true).subscribe((result: IResult) => {
      if(result.success) {
        this.processAllEmision(result.data); 
      }else {
        this.isLoading = false;   
      }
    }, err => {
      this.isLoading = false;
    });
  }

  private processAllEmision(data){
    if(!CONFIG.demo){
      data.map(d => {
        d.date = d._id.date;
        d.hour = d._id.hour;
        delete d._id;
      })
    }
    let today = moment().format("YYYY-MM-DD"); 
    let yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");
    let afluencias = [];
    if(CONFIG.demo){
      yesterday = "2021-03-07";
      today = "2021-03-08";
    }
    const yData = data.filter(emision => emision.date === yesterday);
    const tData = data.filter(emision => emision.date === today);
    afluencias["yesterday"] = this.getChartData(yData)
    afluencias['today'] = this.getChartData(tData)
    this.lineChartData = [
      { data: afluencias['yesterday'], label: this.translate.instant('Ayer') },
      { data: afluencias['today'], label: this.translate.instant('Hoy') },
    ];
    this.isLoading = false;  
  }

  getChartData(data){
    let result:number[] = [];
    data.map(d => {
      result[d.hour] = d.count;
    })
    for(let i = 0; i < 24; i++){
      if(!result[i]){
        result[i] = 0;
      }
    }
    return result;
  }

  private groupBy(elements: any [], field: string){
    let result = [];
    elements.map(element => {
      if(result[element[field]]){
        result[element[field]]++;
      }else{
        result[element[field]] = 1
      }
    });
    for(let i = 0; i < 24; i++){
      if(!result[i])
        result[i] = 0;
    }
    return result;
  }
}

