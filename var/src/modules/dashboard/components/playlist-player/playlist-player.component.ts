import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Component, OnInit, ViewChild, Input, SimpleChanges } from '@angular/core';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { User } from 'src/modules/user/models/user';
import { PlayArea} from 'src/modules/playarea/models/playarea';
import { Player} from 'src/modules/player/models/player';
import { UserStore } from 'src/modules/user/stores/user.store';
import moment from 'moment';
import { CONFIG } from 'src/app/app.config';
import { PlayerStore } from 'src/modules/player/stores/player.store';

export interface Viewer {
  email:string,
  thumbnail:string
}

@Component({
  selector: 'dashboard-playlist-player',
  templateUrl: './playlist-player.component.html',
  styleUrls: ['./playlist-player.component.scss']
})

export class PlaylistPlayerComponent implements OnInit {

  public area: PlayArea = null;
  public player: Player = null;
  
  constructor(private store: PlayerStore, private ws: WSService) {}

  ngOnInit(): void {
    this.store.onChange.subscribe(players => {
      if(players.length > 0) {
          this.player = this.random(players);
          this.area = this.player.area;
      }
    });
  }

  onChangeArea(areas: PlayArea){
    this.area = areas;
    this.randomByArea(this.area);

  }

  private randomByArea(area: PlayArea) {
    let players = this.store.get().filter(p => area.isEquals(p.area));
    if(players.length > 0)
      this.player = this.random(players);
  }

  private random(players: Player[]): Player {
    let index = Math.floor(Math.random() * players.length);
    return players[index];
  }
    
}

