import { Component, OnInit } from '@angular/core';
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

// For graphics
import { ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'dashboard-chart-emissions',
  templateUrl: './chart-emissions.component.html',
  styleUrls: ['./chart-emissions.component.css']
})

export class ChartEmissionsComponent implements OnInit {
  public url: string = '';
  public frame: any;
  public isLoading: boolean = true;
  public emissions: any[] = [];
	public me: any;

  lineChartData: ChartDataSets[] = [{label : this.translate.instant("Ayer"), data : []}, {label : this.translate.instant("Hoy"), data : []}];
  lineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];

  constructor(
    private ws: WSService,
    private ustore: UserStore,
    private translate: TranslateService) {      
  }
  
  public ngOnInit(): void {
    this.me = this.ustore.getUser();
    this.getEmision();
  }
  public getEmision(){
    let today = moment().format("YYYY-MM-DD"); 
    let yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");
    let params:any = {
      idcustomer : this.me.customer.idcustomer,
      date_start : yesterday,
    }
    if(this.me.site){
      params.site_id = this.me.site.idsite
    }

    this.ws.analyticsEmisionesCountByDay(params).subscribe((result: IResult) => {
      if(result.success) {
        this.emissions[0] = result.data;
        params.date_start = today;
        this.ws.analyticsEmisionesCountByDay(params).subscribe((result: IResult) => {
          if(result.success){
            this.emissions[1] = result.data;
            this.processAllEmision(this.emissions);
          }

        })
      }else {
        this.isLoading = false;   
      }
    });
  }

  private processAllEmision(data){
    data.map((e,index) => {
        e.map(em => {
          this.lineChartData[index].data.push(em.count)
        })
    })
    this.isLoading = false;  
  }
}

