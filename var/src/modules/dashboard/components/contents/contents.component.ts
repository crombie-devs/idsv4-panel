import { Component, OnInit } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import moment from 'moment';

@Component({
  selector: 'dashboard-contents',
  templateUrl: './contents.component.html',
  styleUrls: ['./contents.component.css']
})

export class ContentsComponent implements OnInit {

  public load: boolean = true;
  public contents: number;

  constructor(
    private ws: WSService) {}

  
    public ngOnInit(): void {
      this.getContents()
    }  
    private getContents(){
      let month = moment().format("YYYY-MM");
      this.ws.contentsByMonth(month).subscribe(result => {
        if(result.success){
          this.contents = result.data.length;
          this.load = false;
        }
      })
    }
}

