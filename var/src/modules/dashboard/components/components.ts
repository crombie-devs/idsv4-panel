
import { VentasComponent } from './ventas/ventas.component';
import { AfluenciasComponent } from './afluencias/afluencias.component';
import { CameraPlayerComponent } from './camera-player/camera-player.component';
import { ChartImpactoComponent } from './chart-impacto/chart-impacto.component';
import { ChartPeopleComponent } from './chart-people/chart-people.component';
import { EmissionsComponent } from './emissions/emissions.component';
import { ImpactoComponent } from './impacto/impacto.component';
import { MapaTiendasComponent } from './mapa-tiendas/mapa-tiendas.component';
import { PlaylistPlayerComponent } from './playlist-player/playlist-player.component';
import { BlankComponent } from './blank/blank.component';
import { LinkDashboardComponent } from './link/link.component';
import { ChartEmissionsComponent } from './chart-emissions/chart-emissions.component';
import { ChartEmissionsLastMonthComponent } from './chart-emissions-last-month/chart-emissions-last-month.component';
import { ContentsComponent } from './contents/contents.component';
import { PlayareasComponent } from './playareas/playareas.component';


export const DASHBOARD_COMPONENTS = [
  AfluenciasComponent,
  CameraPlayerComponent,
  ChartImpactoComponent,
  ChartPeopleComponent,
  EmissionsComponent,
  ImpactoComponent,
  MapaTiendasComponent,
  PlaylistPlayerComponent,
  VentasComponent,
  BlankComponent,
  LinkDashboardComponent,
  ChartEmissionsComponent,
  ChartEmissionsLastMonthComponent,
  ContentsComponent,
  PlayareasComponent
];
