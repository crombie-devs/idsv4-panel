import { Component, OnInit, ViewChild, Input, SimpleChanges, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { User } from 'src/modules/user/models/user';
import { UserStore } from 'src/modules/user/stores/user.store';
import moment from 'moment';

@Component({
  selector: 'dashboard-mapa-tiendas',
  templateUrl: './mapa-tiendas.component.html',
  styleUrls: ['./mapa-tiendas.component.css']
})

export class MapaTiendasComponent implements OnInit, AfterViewInit {
  @Output() filtered = new EventEmitter<string>();

  public load: boolean=true;
  public lat = 40.4310341;
  public lng = -3.7436507;
  zoom = 5;
  public sitesLoaded:boolean = false;
  public fitbounds: boolean = false;
  @Input() tiendas:SimpleChanges;
  @Input() height:string = '250px';
   
  constructor(
    private store: AuthStore,  
    private localStorage: MYLocalStorage,
    private ws: WSService,
    private debug: DebugStore,
    private dialog: DialogService,
    private ustore: UserStore) {
      
    }

    public showSubmenu: boolean = false;
    public showSubSubMenu: boolean = false;
  
    public panelOpenState = false;
    public status: boolean = false;
    public me: any;
    public titleBreadcrumbs: string;

    public step = 0;

  
    public ngOnInit(): void {
    }

    ngAfterViewInit() {
    }

    public ngOnChanges(changes: SimpleChanges){
      if(changes.tiendas.currentValue.length > 0)
        if(changes.tiendas.currentValue.length <= 1){
          this.lat = Number(changes.tiendas.currentValue[0].latitude);
          this.lng = Number(changes.tiendas.currentValue[0].longitude);
          this.fitbounds = false;
        }else{
          this.sitesLoaded = true;
          this.fitbounds = true;
        }
    }

    public filtrar(tienda){
      this.filtered.emit(tienda);
    }

    onMouseOver(infoWindow, gm) {
      if (gm.lastOpen != null) {
          gm.lastOpen.close();
      }
      gm.lastOpen = infoWindow;
      infoWindow.open();
  }
}

