import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexModule } from '@angular/flex-layout';

import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DASHBOARD_COMPONENTS } from './components/components';
import { DashboardRoutingModule } from './dashboard-routing.module';

import { ChartGraphsModule } from 'src/modules/chart/charts.module';
import { CameraModule } from 'src/modules/camera/camera.module';
import { MaterialModule } from 'src/app/material.module'; 
import { HomePage } from './pages/home/home.page';
import { SharedModule } from 'src/shared/shared.module';

import { AgmCoreModule } from '@agm/core';
import { GoogleMapsModule } from '@angular/google-maps';
import { CONFIG } from 'src/app/app.config';
import { PlayerModule } from 'src/modules/player/player.module';
import { PlayareaModule } from 'src/modules/playarea/playarea.module';

import { PipesModule } from 'src/pipes/pipes.module';

@NgModule({
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [DASHBOARD_COMPONENTS, HomePage],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    AgmCoreModule.forRoot({ apiKey: CONFIG.gmaps_apikey }),
    GoogleMapsModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    ChartGraphsModule,
    CameraModule,
    MaterialModule,
    DashboardRoutingModule,
    PlayerModule,
    PlayareaModule,
    PipesModule
  ],
  providers: [],
  exports:[DASHBOARD_COMPONENTS]
})
export class DashboardModule {}