import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numbersp'
})
export class SpanishDecimalPipe implements PipeTransform {

  transform(val: number): string {
    // Format the output to display any way you want here.
    // For instance:
    if (val !== undefined && val !== null) {
      return new Intl.NumberFormat('de-DE', {
        minimumFractionDigits: 0,      
        maximumFractionDigits: 2,
     }).format(val);
    } else {
      return '';
    }
  }
}