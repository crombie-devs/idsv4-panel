import { CONFIG } from 'src/app/app.config';
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { CountryStore } from 'src/modules/country/stores/country.store';
import { ProvinceStore } from 'src/modules/province/stores/province.store';
import { CityStore } from 'src/modules/city/stores/city.store';
import { IResult } from 'src/models/iresult';
import moment from 'moment';
import { User } from 'src/modules/user/models/user';
import { UserStore } from 'src/modules/user/stores/user.store';
import { PlayerStore } from 'src/modules/player/stores/player.store';
import { CameraStore } from 'src/modules/camera/stores/camera.store';
import { SiteStore } from 'src/modules/site/stores/site.store';
import { SiteService } from 'src/modules/site/services/site.service';
import { Site } from 'src/modules/site/models/site';

@Component({
  providers: [ PlayerStore, CameraStore ],
  selector: 'home-page',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss']
})

export class HomePage {
	public emissions: any = null;
	public afluencias: any = '0';
	public ventas: string = '6.365.234';
	public ventasChart: any[] = [];
	public emissionsLastMonthChart: any[] = [];
	public emissionsChart: any[] = [];
	public tiendas: any[] = [];
	public ventasLastMonth: string = '';
	public me: User;
	public store: string = null;
	public colspanFirstRow: number = 4;
	public colspanThirdRow: number = 4;
	public cameraToShow: any = {};
	public loadingCamera: boolean = true;

  	constructor(
		private router: Router,
		private ws: WSService,
		private wsSite: SiteService,
		private debug: DebugStore,	
		private dialog: DialogService,
		private costore: CountryStore,
		private pstore: ProvinceStore,
		private cistore: CityStore,
		private cstore: CameraStore,
		private ustore: UserStore,
		private sistore: SiteStore
	) {
		this.ustore.onChange.subscribe(user => { 
			if(user){
				this.me = user;
				if(this.me.customer && (!this.me.customer.has_sales && !this.me.customer.has_emissions))
					this.router.navigateByUrl('/private/analisis/afluencias');
			}
		});			
  	}
  
  	public ngOnInit() {
		this.debug.addInfo('HomePage', 'OnInit');
		this.costore.onChange.subscribe(result => {
        })
        this.pstore.onChange.subscribe(result => {
        });
		this.pstore.reload;
        this.cistore.onChange.subscribe(result => {
        });
		this.sistore.onChange.subscribe(result => {
        });
		this.syncViewer()
	  }
	  public ngOnChanges(tiendas: any[]){
		  this.syncViewer();
	  }
	  private syncViewer() {
		this.getEmissions();
		this.getAfluencias();
		this.getVentas();
		this.getLastMonthVentas();
		this.getLastMonthVentasChart();
		this.getTiendas();
	  }
	  public getEmissions() {
		const day = moment().format('DD');
		let month = moment();
		if(day === '01'){
			month.subtract(1, 'months');
		}

		let params:any = {
				date: `${month.format("YYYY-MM")}`,
				cos: this.me.site || this.store ? 'site' : 'customer'
			}
		this.ws.dashboardEmisionesCountByDate(params).subscribe((result: any) => {
			let results;

			if(this.me.customer && !this.me.site && !this.store){
				const idcustomer = (!CONFIG.demo && this.me.customer.idcustomer !== 31) ? this.me.customer.idcustomer : 31;
				results = result.filter(data => data.idcustomer === idcustomer);
			}

			if(this.me.site || this.store)  {
					results = result.filter(emissions => emissions.idsite === this.me.site.idsite || emissions.idsite === this.store)
			}
			
	
            if(results ) {
				if(results.length === 0){
					this.emissions = 0
				}else{
					let emissions = 0;
					results[0].dates.map(d => {
						emissions +=d.count;
					})
					this.emissions = new Intl.NumberFormat("de-DE").format(emissions);
				}
			} else {

			this.emissions = 0
			}
			


		});
	  }

	  public getAfluencias(){
		if(!this.me.customer || !this.me.customer.has_influxes){
			return;
		}
		const day = moment().format('DD');
		let month = moment();
		if(day === '01'){
			month.subtract(1, 'months');
		}
		let params = {
			year: parseInt(moment().format("YYYY")),
			month: parseInt(month.format("MM")),
			customer_id: null,
			site_id:null
		}
	
		if(this.me.customer)
		  params.customer_id = this.me.customer.idcustomer;
 	   	if(this.me.site)
		  params.site_id = this.me.site.idsite
		if(this.store)
		  params.site_id = this.store;
 		return this.ws.findPeople(params).subscribe((result: IResult) => {
		  if(result.success) { 
			  if(result.data.length > 0)
				  this.afluencias =  result.data[0].count; 
			}else {
			  this.debug.addError('WS.findPeople', result.data);
			  this.dialog.warning('findPeople', result.data); 
			}
		}, error => {
			this.dialog.warning("error", "Ha habido un error al recoger los datos");
		});
	  }

	  public getVentas(last:boolean = false) {
		let type = 'customer';
		if(! this.me.customer || !this.me.customer.has_sales){
			return null
		}
		if(this.me.site || this.store){
			type = 'site';
		}
		let date:string;
		if(last)
			date = moment().subtract(1, 'months').format("YYYY-MM")
		else
			date = moment().format("YYYY-MM")

		this.ws.amountsByMonthFromJson(date, type).subscribe(result => {
			if(result){
				let value = 0;
				if(CONFIG.demo){
					result = result.filter(res => res.idcustomer === 31);
				}else if(type === 'customer'){
					result = result.filter(res => res.idcustomer === this.me.customer.idcustomer);
				}else{
					result = result.filter(res => res.idsite === this.me.site.idsite)
				}
				result.map(data => {
					value += parseFloat(data.total_amount);
				})
				this.ventas = value.toString();
				if(CONFIG.demo && this.ventas === '0' && !last){
					this.getVentas(true)
				}
			}
		}, err => {
			this.ventas = '0';
			if(CONFIG.demo && !last){
				this.getVentas(true);
			}
		});
	  }

	  public getLastMonthVentas() {
		if(!this.me.customer || !this.me.customer.has_sales){
			return null
		}
		const date:string = moment().subtract(1, 'months').format("YYYY-MM");
		const dateArray:string[] = date.split('-'); 
		let params:any = {
			year: parseInt(dateArray[0]),
			month: parseInt(dateArray[1])
		  }
		if(this.me.customer)
			params.customer_id = this.me.customer.idcustomer;
		if(this.me.site)
			params.site_id = this.me.site.idsite
		if(this.store)
		  params.site_id = this.store;
		  
		  return this.ws.getImportesByMonth(params).subscribe((result: IResult) => {
			let total = 0;
			if(result.success) {
				if(result.data.length === 0)
					total = 0;
				else 
					total = result.data[0].total_amount;
				this.ventasLastMonth = new Intl.NumberFormat("de-DE").format(total);
			  }else {
				this.debug.addError('WS.findPeople', result.data);
				this.dialog.warning('findPeople', result.data); 
				this.ventas ="Error al hacer la llamada"
			  }
		  });
	  }

	  public getLastMonthVentasChart() {
		if(!this.me.customer || !this.me.customer.has_sales){
			return null
		}
		const date = moment().subtract(1, 'months').format("YYYY-MM");
		let params:any = {
			date: `${date}`,
			cos: this.me.site || this.store ? 'site' : 'customer'
		}
		this.ws.dashboardEmisionesCountByDate(params).subscribe((result: any) => {
			const idcustomer = (!CONFIG.demo && this.me.customer.idcustomer !== 31) ? this.me.customer.idcustomer : 31;
			const fromCustomer = result.filter(data => data.idcustomer === idcustomer);
			if(fromCustomer.length > 0)
				this.emissionsChart = fromCustomer[0].dates;
			else
				this.emissionsChart = [];
		});
		this.ws.amountsByDateFromJson(params.date, params.cos).subscribe(result => {
			const idcustomer = (!CONFIG.demo && this.me.customer.idcustomer !== 31) ? this.me.customer.idcustomer : 31;
			if(result){
				const fromCustomer = result.filter(data => data.idcustomer === idcustomer);
				this.ventasChart = fromCustomer;
			}
		}) 
	  }
  
	  public getTiendas(){
		if(this.me.site){
			this.tiendas = [this.me.site];
		}else{
			this.wsSite.list().subscribe((sites: Site[]) => {
					this.tiendas = sites;
			}, error => {
				this.debug.addError('WS.sites', error.message);  
				return null;          
			});	
		}
	  }

	  public filterPageData($event){
		  this.store = $event;
		  this.syncViewer();
	  }
}
