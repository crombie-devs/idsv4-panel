import { AfterViewInit, QueryList, ViewChildren, Component, ViewChild } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Content } from 'src/modules/content/models/content';
import { User } from 'src/modules/user/models/user';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Router } from '@angular/router';
import { Sort } from '@angular/material/sort';
import { MYString } from 'src/libs/string.libs';
import { MatCheckbox } from '@angular/material/checkbox';
import { EventService } from 'src/shared/event.service';
import { CacheService, ICacheConfig } from 'src/services/cache.service';
import { MatDialog } from '@angular/material/dialog';
import { ListAreasCircuitsDialog } from 'src/modules/content/dialogs/list-area-circuits/list-area-circuits.dialog';
import { ContentService } from 'src/modules/content/services/contents.service';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { FormField } from 'src/modules/form-field/models/form-field';
import { ExcelService } from 'src/services/excel.service';
import { TranslateService } from '@ngx-translate/core';
import { MatInput } from '@angular/material/input';




@Component({
    templateUrl: './list.page.html',
    styleUrls: ['./list.page.scss']
})

export class ContentListPage {

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChildren('table') table: QueryList<MatCheckbox>;
    @ViewChild(MatInput) input: MatInput;

    public displayedColumns: string[] = ['select', 'id', 'name', 'category', 'playcircuit', 'area', 'from', 'to', 'state'];
    public selection = new SelectionModel<Content>(true, []);
    public dataSource = new MatTableDataSource<Content>();
    public data: Content[];
    public filterData: Content[];
    public new: boolean = false;
    public edit: boolean = false;
    public deleted: boolean = false;
    public permission: boolean = false;
    public load: boolean = true;
    public me: User;
    public isUser: boolean = false;
    public sortedData: Content[];
    public contents: Content[];
    public archived: boolean = true;
    public fieldDataFilterContent: FormField = {
        label: "",
        name: 'filter',
        placeholder: '',
        options: [{ value: 'all', name: 'Todos' }, { value: 'active', name: 'Activos' }, { value: 'inactive', name: 'Inactivo' }, { value: 'archived', name: 'Archivados' }],
        multiple: false,
        required: false
    };
    public formGroup: FormGroup = new FormGroup({
        filter: new FormControl('all', [Validators.required])
    });

    constructor(
        private ws: WSService,
        private debug: DebugStore,
        private ustore: UserStore,
        private _snackBar: MatSnackBar,
        private dialog: DialogService,
        private router: Router,
        private events: EventService,
        private excelService: ExcelService,
        private matDialog: MatDialog,
        private _content: ContentService,
        private translate: TranslateService
    ) { }

    deleteSelectedAssets() {
        var arr = []; // Array to hold the keys
        for (var i = 0; i < localStorage.length; i++) {

            if (localStorage.key(i).substring(0, 15) == 'selectedAssets_') {
                arr.push(localStorage.key(i));
            }
        }
        // Iterate over arr and remove the items by key
        for (var i = 0; i < arr.length; i++) {
            localStorage.removeItem(arr[i]);
        }
    }
    public ngOnInit() {
        this.debug.addInfo('ContentListComponent', 'OnInit');
        this.deleteSelectedAssets();
        this.me = this.ustore.getUser();
        this.isUser = this.ustore.isUser();
        if (this.isUser)
            this.displayedColumns = ['select', 'id', 'name', 'category', 'area', 'from', 'to', 'state'];
        this.getContents('all');
    }

    private openDialog(rows, type): void {
        let dialogRef;
        dialogRef = this.matDialog.open(ListAreasCircuitsDialog, {
            data: { data: rows, type: type, width: 'auto', height: 'auto', minHeight: '600px', minWidth: '800px' }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {

            }
        });
    }

    private getContents(status: string) {
        this.load = true;
        this.data = [];
        this._content.contentsbystatus(status).subscribe((result: Content[]) => {
            let contents = result.map(c => new Content(c));
            const categories = this.me.idcategories ? this.me.idcategories.split(',') : [];
            if (categories.length > 0) {
                this.data = contents.filter(co => categories.some(ca => parseInt(ca, 10) == co.category.idcategory));
            } else {
                this.data = contents;
            }
            this.debug.addInfo('WS.contents', 'successful');
            this.permission = true;
            this.renderDataTable(this.data);
        }, error => {
            this.debug.addError('WS.contents', error.message);
            this.load = false;
            return null;
        });
    }

    public renderDataTable(data: Content[]) {
        this.dataSource = new MatTableDataSource(data);

        this.dataSource.filterPredicate = (data: Content, filter: string): boolean => {
            const dataStr = MYString.removeAccents(Object.keys(data).reduce((currentTerm: string, key: string) => {
                return (currentTerm + (data as { [key: string]: any })[key] + '◬');
            }, '')).trim().toLowerCase();
            return dataStr.indexOf(MYString.removeAccents(filter.trim().toLowerCase())) != -1;
        };

        setTimeout(() => {
            this.dataSource.paginator = this.paginator;
        });

        if (this.input)
            this.applyFilter(this.input.value);

        this.load = false;
        this.sortedData = data.slice();
        this.contents = data;

        if (this.sort)
            this.sortData(this.sort);
        else
            this.sortData({ active: 'id', direction: 'desc' });
        this.show();
    }

    isAllSelected() {
        if (this.dataSource.paginator) {
            const numSelected = this.selection.selected.length;
            const page = this.dataSource.paginator.pageSize;
            let endIndex: number;
            if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
                endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
            } else {
                endIndex = this.dataSource.data.length - (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize);
            }
            return numSelected === endIndex;
        }
    }

    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() : this.selectRows();
    }

    selectRows() {
        let endIndex: number;
        if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
            endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
        } else {
            endIndex = this.dataSource.data.length;
        }
        for (let index = (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize); index < endIndex; index++) {
            this.selection.select(this.dataSource.data[index]);
        }
    }

    public checkboxLabel(row?: Content): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idcontent + 1}`;
    }


    public delete() {
        let contentsByDelete = [];
        this.table.toArray().forEach(item => {
            if (item.checked)
                contentsByDelete.push(item.value);
        });

        if (contentsByDelete.length == 0)
            this.dialog.warning('Aviso', 'Debe seleccionar al menos un contenido').subscribe();
        else
            this.dialog.confirm(this.translate.instant('Confirmar Archivado'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
                if (confirm) {
                    this.ws.deleteContent(contentsByDelete).subscribe(result => {
                        if (result.success) {
                            let config: ICacheConfig = (this.me.site) ? { idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer } : { idcustomer: this.me.customer.idcustomer };
                            setTimeout(() => { this.events.publish('content:change', config); }, 1000);
                            this.selection.clear();
                            this.data = this.data.filter(co => contentsByDelete.every(id => co.idcontent !== id));
                            this.dataSource.data = this.dataSource.data.filter(co => contentsByDelete.every(id => co.idcontent !== id));
                            this.dialog.warning(this.translate.instant('Aviso'), this.translate.instant('Los contenidos han sido archivados')).subscribe();
                            if (this.me && this.me.customer) {
                                let config: ICacheConfig = { idcustomer: this.me.customer.idcustomer };
                                if (this.me.site)
                                    config = { idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer };
                                setTimeout(() => {
                                    this.events.publish('content:change', config);
                                }, 1000);
                            }
                        }
                    })
                }
            });
    }

    public changeStatus(elem) {
        this._content.changeStatus(elem).subscribe(item => {
            this.data.map((c, i) => {
                if (c.idcontent === item.idcontent) {
                    let content = new Content(item);
                    this.data[i].statusString = content.statusString;
                }
            });
            if (this.me && this.me.customer) {
                let config: ICacheConfig = { idcustomer: this.me.customer.idcustomer };
                if (this.me.site)
                    config = { idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer };
                setTimeout(() => {
                    this.events.publish('content:change', config);
                }, 1000);
            }
        });
    }


    public update() {
        if (this.selection.selected.length > 1)
            this.dialog.warning('Aviso', 'Sólo puede editar un contenido').subscribe();
        else if (this.selection.selected.length == 0)
            this.dialog.warning('Aviso', 'Debe seleccionar un contenido').subscribe();
        else
            this.router.navigateByUrl('/private/content/edit/' + this.selection.selected[0].idcontent);
    }

    public applyFilter(event: string) {
        this.selection.clear();
        if (!event)
            this.dataSource.data = this.data;
        else {
            if (typeof event == "string") {
                this.dataSource.data = this.data.filter(content =>
                    content.name.toLowerCase().includes(event.trim().toLowerCase()) ||
                    content.idcontent.toString().includes(event.trim()) ||
                    (content.category && content.category.name.toLowerCase().includes(event.trim().toLowerCase())) ||
                    (content.playcircuits && this.searchByPlaycircuits(content.playcircuits, event)) ||
                    (content.playareas && this.searchByPlayAreas(content.playareas, event))
                );
            }
        }
    }

    private searchByPlaycircuits(playcircuits, q) {
        let exist: boolean = false;
        playcircuits.map(c => {
            if (c.name.toLowerCase().includes(q.trim().toLowerCase()))
                exist = true;
        });
        return exist
    }

    private searchByPlayAreas(playareas, q) {
        let exist: boolean = false;
        let name: string = '';
        playareas.map(c => {
            name = `${c.format.name} - ${c.location.name}`;
            if (name.toLowerCase().includes(q.trim().toLowerCase()))
                exist = true;
        });
        return exist
    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }

    public show() {
        this.new = this.ustore.hasPermissionByName("post.content")
        this.edit = this.ustore.hasPermissionByName("put.content.*")
        this.deleted = this.ustore.hasPermissionByName("delete.content.*") && (this.formGroup.get('filter').value !== 'archived')
    }

    sortData(sort: Sort) {
        const data = this.dataSource.data.slice();
        if (!sort.active || sort.direction === '') {
            this.sortedData = data;
            return;
        }
        this.sortedData = data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'name': return this.compare(a.name, b.name, isAsc);
                case 'id': return this.compare(a.idcontent, b.idcontent, isAsc);
                case 'category': return this.compare(a.category ? a.category.toString() : '', b.category ? b.category.toString() : '', isAsc);
                case 'state': return this.compare(a.status.toString(), b.status.toString(), isAsc);
                case 'playcircuit': return this.compare(a.playcircuits.toString(), b.playcircuits.toString(), isAsc);
                case 'area': return this.compare(a.playareas.toString(), b.playareas.toString(), isAsc);
                case 'from': return this.compare(a.dates[0].date_on.toString(), b.dates[0].date_on.toString(), isAsc);
                case 'to': return this.compare(a.dates[0].date_off.toString(), b.dates[0].date_off.toString(), isAsc);
                default: return 0;
            }
        });
        this.data = this.dataSource.data = this.sortedData;
    }

    compare(a: number | string, b: number | string, isAsc: boolean) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }
    public onChangeStatus(data) {
        let filter = this.formGroup.get('filter').value;
        this.getContents(filter);
    }

    public excelExport() {
        let dataExcel: any[] = [];
        this.dataSource.data.map(content => {
            let circuitos = '';
            let areas = '';
            content.playcircuits.map((c, i) => circuitos += (content.playcircuits.length === i + 1) ? `${c.name}` : `${c.name}, `);
            content.playareas.map((a, i) => areas += (content.playareas.length === i + 1) ? `${a.format.name} ${a.location.name}` : `${a.format.name} ${a.location.name}, `)
            dataExcel.push({
                "id": content.idcontent,
                "Nombre": content.name,
                "Categoría": content.category.name,
                "Circuitos": circuitos,
                "Areas de reproducción": areas,
                "Fecha Inicio": content.dates[0].date_on,
                "Fecha Fin": content.dates[0].date_off,
                "Estado": content.status
            });
        });
        let name = (this.me.customer) ? this.me.customer.name : '';
        this.excelService.exportAsExcelFile(dataExcel, `${name}`);
    }
}
