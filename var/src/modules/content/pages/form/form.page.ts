import { SiteService } from 'src/modules/site/services/site.service';
import { AfterViewInit, ChangeDetectorRef, Component, SimpleChanges, ViewChild, AfterViewChecked } from '@angular/core';
import { ContentService } from 'src/modules/content/services/contents.service';
import { DebugStore } from 'src/stores/debug.store';
import { PlayArea } from 'src/modules/playarea/models/playarea';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { Asset } from 'src/modules/asset/models/asset';
import { Content, IContent } from 'src/modules/content/models/content';
import { Product } from 'src/modules/product/models/product';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { UserStore } from 'src/modules/user/stores/user.store';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Recomendation } from 'src/modules/recomendation/models/recomendation';
import { Site } from 'src/modules/site/models/site';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { Customer } from 'src/modules/customer/models/customer';
import moment from 'moment';
import { ContentDate } from 'src/models/date';
import { AudioCategoryModel } from 'src/modules/audio/models/audio-category.model';
import { ContentStore } from 'src/modules/content/stores/content.store';
import { RecomendationStore } from 'src/modules/recomendation/stores/recomendations.store';
import { CONFIG } from 'src/app/app.config';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Power } from 'src/modules/power/models/power';
import { Tag } from 'src/modules/tag/models/tag';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { PlayareaStore } from 'src/modules/playarea/stores/playarea.store';
import { ICacheConfig } from 'src/services/cache.service';
import { EventService } from 'src/shared/event.service';
import { Observable, from } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { CreateDialog } from 'src/modules/content/dialogs/create/create.dialog';
import { concatMap, filter, shareReplay } from 'rxjs/operators';
import { PlayerStore } from 'src/modules/player/stores/player.store';
import { Player } from 'src/modules/player/models/player';
import { SiteStore } from 'src/modules/site/stores/site.store';
import { MessagesService } from 'src/modules/messages/services/messages.service';
import { CalculatePeopleComponent } from '../../components/calculate-people/calculate-people.component';
import { MsgTextContainersComponent } from '../../components/msg-text-containers/msg-text-containers.component';
import { ListMessageComponent } from '../../components/list-messages/list-message.component';
import { isThisTypeNode } from 'typescript';



@Component({
    providers: [
        FormatStore,
        LangStore,
        RecomendationStore,
        PlayareaStore,
        ContentStore,
        SiteStore,
        PlayerStore,
        UserStore,
        DebugStore,
        SiteService,
        DialogService,
    ],
    selector: 'content-form',
    templateUrl: './form.page.html',
    styleUrls: ['./form.page.scss']
})

export class ContentFormPage implements AfterViewInit {
    public id: number | null;
    public content: Content;
    public contents: Content[] = [];
    public contentErrors: Content[] = [];
    public contentIndex: number = 0;
    public isAdmin: boolean = false;
    public isUser: boolean = false;
    public areasToSelect: number[] = [];
    public showContentCreate: boolean = false;
    public showAreas: boolean = false;
    public hideButton: boolean = true;
    public isEditing: boolean = false;
    public showSave: boolean = false;
    public dateChecked: boolean = false;
    public recomendation: Recomendation;
    public playcircuits: PlayCircuit[] = [];
    public load: boolean = true;
    public me: any;
    public hide: boolean = true;
    public sites: any = [];
    public all_areas: PlayArea[] = [];
    public user: boolean;
    public types: any[] = [];
    public langs: [] = [];
    public showTxt: boolean;
    public datetime;
    public houres;
    public selectedCircuits: PlayCircuit[] = [];
    public selectedAreas: PlayArea[] = [];
    public selectedAssets: Asset[] = [];
    public selected: Set<any> = new Set();
    public updateSelected: any[] = [];
    public customers: Customer[];
    public customersToSelect: boolean = false;
    public dates: ContentDate[] = [];
    public path: string;
    public idcustomer: number = null;
    public customerSelect: number = null;
    public savedContents: boolean = false;
    public contentElementSaved: number = 0;
    public disabled: boolean = false;
    public disableSubmit: boolean = false;
    public pass: boolean;
    public email: boolean;
    public sms: boolean;
    public whatsapp: boolean;
    public format: any = [];
    public dis: boolean[] = [];
    public csv: File;
    public contentForm: FormGroup = new FormGroup({
        idaudiocategory: new FormControl(),
        name: new FormControl(null),
        idtype: new FormControl(2, [Validators.required]),
        code: new FormControl("code", [Validators.required]),
        thumbnail_url: new FormControl("thumbnail_url", [Validators.required]),
        tracker_url: new FormControl("tracker_url", [Validators.required]),
        idcategory: new FormControl(null, [Validators.required]),
        is_exclusive: new FormControl(false),
        status: new FormControl(1),
        primetime: new FormControl(1),
        frequency: new FormControl(0),
        frequencyMethod: new FormControl(0),
        maxEmissions: new FormControl(null),
        pixelUrl: new FormControl(null),
        priority: new FormControl(1, [Validators.required])
    });

    public playAreasToSelect = [];
    public categories: AudioCategoryModel[] = [];
    public audioCategory: string;
    public selectedTags: Tag[] = [];
    public selectedProducts: any[] = [];
    public selectedContentPowers: Power[] = [];
    public statusCt: number = 0;
    public statusCtProd: number = 0;
    public idsite: number = null;
    public storageItems: any;
    public assetsToStorage = [];
    public sendMail: number = 0;
    public formatsLangs = [];
    public enableTag: number = 0;
    public selectedSites: { idsite: number, idcircuit: number }[] = [];
    public sites_filtered: Site[] = [];
    public players_filtered: Player[] = [];
    public idareas_filtered: number[] = [];
    public all_sites: Site[] = [];
    public all_players: Player[] = [];
    public loaded: boolean = false;
    public loadedCount: number = 0;

    @ViewChild(CalculatePeopleComponent) calculate: CalculatePeopleComponent;
    @ViewChild(ListMessageComponent) listMsg: ListMessageComponent;

    constructor(
        private ustore: UserStore,
        private dialog: DialogService,
        private route: ActivatedRoute,
        private router: Router,
        private _content: ContentService,
        private rstore: RecomendationStore,
        private debug: DebugStore,
        private cdr: ChangeDetectorRef,
        private cstore: ContentStore,
        private _snackBar: MatSnackBar,
        private events: EventService,
        private matDialog: MatDialog,
        private siStore: SiteStore,
        private plStore: PlayerStore,
        private _message: MessagesService
    ) {
        this.siStore.onChange.subscribe(result => {
            if (result && result.length > 0) {
                this.all_sites = result.map(s => new Site(s));
                this.loadedCount += 1;
                if (this.loadedCount > 1)
                    this.loaded = true;
            }
        });
        this.plStore.onChange.subscribe(players => {
            if (players && players.length > 0) {
                this.all_players = players.map(p => new Player(p));
                this.loadedCount += 1;
                if (this.loadedCount > 1)
                    this.loaded = true;
            }
        });
        this.plStore.onChange.subscribe();

        this.id = Number(this.route.snapshot.paramMap.get('idcontent'));


        if (this.route.snapshot.paramMap.get('playlist')) {
            this.path = '/private/playlists';
        } else {
            this.path = '/private/content/list';
        }

        this.ustore.onChange.subscribe(user => {
            if (user) {
                this.me = user;
                this.langs = this.me.langs;

                this.langs.forEach((i: any) => {
                    this.dis.push(false);
                    this.format.push({ email: false, idlang: i.idlang, sms: false, whatsapp: false, active: true })
                });


                this.hide = this.me.customer ? false : true;
                this.isAdmin = this.me.isAdmin();
                this.isUser = this.me.isUser();
                this.customersToSelect = this.ustore.hasPermissionByName("get.customers.toselect");
            }
        });
        this.rstore.onChange.subscribe(recomendation => {
            this.recomendation = recomendation;
        });
    }

    public disableLang(idlang) {
        var pos = this.format.map(function (e) {
            return e.idlang;
        }).indexOf(idlang);
        this.format[pos].active = !this.format[pos].active;
    }

    getMessage(event): void {
        this.format = this.format.filter(f => f.idlang != event.idlang);
        this.format.push(event);
    }


    checkFormat(): any {
        this.format = this.format.filter(f => f.active == true);
        let response: any = [];
        for (let i = 0; i < this.format.length; i++) {
            const keys = Object.keys(this.format[i]);
            for (let e = 0; e < keys.length; e++) {
                if (this[keys[e]]) {
                    if (this.format[i][keys[e]]) {
                        response.push(true);
                    } else {
                        response.push(false);
                    }
                }
            }
        }
        const pass = !response.some(r => r == false);
        return pass;
    }

    isParentComponentRoute(url: string): boolean {
        return (
            url
                .split('')
                .reduce((acc: number, curr: string) => (curr.indexOf('/') > -1 ? acc + 1 : acc), 0) === 1
        );

    }

    public ngAfterViewInit() {
        if (this.contents && this.contents.length > 0)
            this.playcircuitChanged(this.contents[this.contentIndex].playcircuits);
    }

    public userCircuits(circuits) {
        if (this.ustore && this.ustore.isUser()) {
            this.user = true;
            this.hideButton = false;
            this.playcircuitChanged(circuits);
        }
    }

    public ngOnChanges(changes: SimpleChanges): void {
        //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
        //Add '${implements OnChanges}' to the class.
        if (changes.hasOwnProperty('content')) {
            this.dates = [];
            changes.content.currentValue.dates.map(date => {
                this.dates.push({ date_on: date.date_on, date_off: date.date_off });
            });
            this.contents[this.contentIndex].dates = this.dates;
            this.selectedAssets = changes.content.currentValue.assets.map(a => new Asset(a));
        }
    }

    public ngOnInit() {
        this.getTypes();
        this.dates.push({ date_on: moment().format('YYYY-MM-DD'), date_off: null });
        if (this.id) {
            this.isEditing = true;
            this._content.get(this.id).subscribe((content: Content) => {
                if (content) {
                    this.contents[this.contentIndex] = new Content(content);
                    this.selectedSites = (content.sites) ? content.sites.map(si => { return { idsite: si.idsite, idcircuit: si.idcircuit }; }) : [];
                    this.showContentCreate = true;
                    this.hideButton = true;
                    this.selectedAreas = this.contents[this.contentIndex].playareas;
                    this.dates = this.contents[this.contentIndex].dates;
                    this.selectedAssets = this.contents[this.contentIndex].assets.map(a => new Asset(a));
                    this.selectedContentPowers = this.contents[this.contentIndex].powers.map(p => new Power(p));
                    this.selectedProducts = this.contents[this.contentIndex].products.map(p => new Product(p));
                    this.changedTags(this.contents[this.contentIndex].tags);
                    this.playcircuitChanged(this.contents[this.contentIndex].playcircuits);
                    this.sitesChanged(this.selectedSites);
                    this.onSelectedAreasChange(this.selectedAreas);

                    if (content.message) {
                        this.datetime = content.message[0].datetime;
                        this.houres = moment(this.datetime[0]).format('hh:mm:ss');
                    }

                    content.message[0].formats.forEach((item: any) => {
                        this.updateSelected.push(item);
                        this.showTxt = true;
                        setTimeout(() => {
                            this.listMsg.renderDataTable(content.message);
                            this.changeType([item])
                        }, 800);

                    });
                }
            }, error => {
                this.load = false;
                this.debug.addError('WS.contetn', error.message);
                switch (error.status) {
                    case 403: {
                        this.dialog.warning("Error", "No tienes permiso para editar este contenido.");
                        break;
                    }
                    case 500: {
                        this.dialog.warning("Error", "Ha habido un error al hacer la consulta.");
                        break;
                    }
                }
                this.router.navigateByUrl(this.path);
            });
        }
        if (this.recomendation && CONFIG.demo) {
            const actualMonth = moment().month();
            const recomendationMonth = this.recomendation.month;
            const actualYear = moment().year();
            let dateOn;
            let dateOff;
            if (actualMonth < recomendationMonth) {
                dateOn = new Date(actualYear, recomendationMonth - 1, 1);
                dateOff = new Date(actualYear, recomendationMonth, 0);
            } else {
                dateOn = new Date(actualYear + 1, recomendationMonth - 1, 1);
                dateOff = new Date(actualYear + 1, recomendationMonth, 0);
            }
            this.dates[0] = { date_on: moment(dateOn).format("YYYY-MM-DD"), date_off: moment(dateOff).format('YYYY-MM-DD') };
        }
        this.load = false;
    }
    ngAfterContentChecked() {
        this.cdr.detectChanges();
    }


    saveMessage(id: number, subject: string): void {
        let data = this.formatData();
        data.forEach((el, index) => {
            data[index].idcontent = id;
            data[index].subject = subject;

        });
        this._message.save(data).subscribe(item => {
            if (this.csv) {
                this.uploadCSV(item.id)
            }
        });
    }

    extracHours(datetime): string {
        const hours = moment(datetime).format('HH:mm');
        return hours;
    }


    changeType(events): void {
        this.showTxt = false;
        this.types = [...events];
        this.email = false;
        this.sms = false;
        this.whatsapp = false;
        this.types.forEach(item => {
            let v = item.name.toLowerCase();
            this[v] = true;
            this.showTxt = true;
        });
    }

    setDatetime() {
        const date = moment(this.datetime).format("DD/MM/YYYY");
        const datetime = moment(date + ' ' + this.houres, 'DD/MM/YYYY HH:mm:ss');
        return moment(datetime).format('MM/DD/YYYY HH:mm:ss');
    }


    chargeCSV(csv): void {
        this.csv = csv
    }


    uploadCSV(idmessage: any) {
        this._content.uploadCSV(this.csv, idmessage, this.datetime).subscribe(response => {
            let reader: FileReader = new FileReader();
            reader.readAsText(this.csv);
            reader.onload = (e) => {
                let csv: string = reader.result as string;
                return csv;
            }
        });
    }

    formatData() {
        this.datetime = this.setDatetime();
        const data = [];
        this.types.forEach(type => {
            this.format.forEach((format, index) => {
                const keys = Object.keys(format);
                keys.forEach(key => {
                    if (key == type.name.toLowerCase()) {
                        data.push({ idformat: type.idformat, message: format[key], idlang: format['idlang'], datetime: this.datetime })
                    };
                })
            });
        });

        return data;
    }

    private getTypes() {
        if (this.me.hasOwnProperty('config_customer')) {
            const types = this.me.config_customer;
            const keys = Object.keys(types);
            keys.forEach(key => {
                if (key != 'id' && key != 'idcustomer') {
                    if (types[key] != null) {
                        this.types.push(types[key]);
                    }
                }
            });
            if (this.types.length > 0) {
                this.pass = true;
            }
        }
    }



    public onContentNavigate() {
        if (this.isAdmin) {
            if (this.selectedCircuits.length > 0 && this.dates.length > 0) {
                this.initContents();
                this.showContentCreate = true;
            } else {
                if (this.selectedCircuits.length === 0) {
                    this.dialog.warning("Error", "Seleccione al menos un circuito");
                }
                if (this.dates.length === 0) {
                    this.dialog.warning("Error", "Seleccione al menos un periodo de emisión");
                }
            }
        } else {
            this.showContentCreate = true;
            this.initContents();
        }
        if (this.showContentCreate) {
            this.hideButton = true;
        }
    }
    private initContents() {
        let content: IContent = {
            dates: this.dates,
            playareas: this.selectedAreas,
            playcircuits: this.selectedCircuits,
            priority: 1,
            idcategory: null,
            is_exclusive: false,
            primetime: true,
            status: 1,
            assets: [],
            powers: [],
            products: [],
            tags: []
        }
        this.contents.push(new Content(content));
    }
    private getAreasFromProduct(product) {
        const lastNumberOfProduct = parseInt(product[product.length - 1]);
        let selectedAreasIds: number[] = []
        switch (true) {
            case (lastNumberOfProduct < 4):
                selectedAreasIds = [208, 203, 205];
                break;
            case (lastNumberOfProduct < 8):
                selectedAreasIds = [236, 261, 209];
                break;
            default:
                selectedAreasIds = [262, 263, 264];
                break;
        }
        this.all_areas.map(aa => {
            if (selectedAreasIds.some(sa => sa === aa.idarea))
                this.selectedAreas.push(aa);
        })
    }

    public onSelectedAreasChange(selectedAreas: PlayArea[]) {
        this.selectedAreas = selectedAreas.map(a => new PlayArea(a));
        this.selectedAssets = this.selectedAssets.filter(s => this.selectedAreas.some(a => a.format.idformat === s.format.idformat));
        if (this.selectedAssets)
            this.selectedAssetsChanged(this.selectedAssets);
        this.contents[this.contentIndex].playareas = selectedAreas.map(sa => new PlayArea(sa));
    }
    public onAreasToSelectChange(ids: number[]) {
        this.areasToSelect = [...ids];
    }
    public onNewCircuit() {
        this.showContentCreate = false;
        this.selectedAreas = [];
    }
    public onNewContent() {
        this.selectedAreas = [];
    }
    public playcircuitChanged(playcircuits) {
        this.selectedCircuits = playcircuits;
        this.filterSitesAreas();
        let datesChecked = this.datesChecked();
        if (this.selectedCircuits.length > 0 && datesChecked) {
            this.dateChecked = true;
            this.hideButton = false;
            this.disabled = false;
        }
        if (this.selectedCircuits.length === 0)
            this.disabled = true;
        if (!datesChecked && !this.isUser) {
            this.openSnackBar("Tienes que rellenar todos los periodos que has generado.");
        }
        if (this.contents[this.contentIndex]) {
            this.contents[this.contentIndex].playcircuits = this.selectedCircuits;
        }
    }
    public filterSitesAreas() {
        this.sites_filtered = this.all_sites.filter(si => {
            return this.selectedCircuits.some(ci => {
                let sites_found = this.selectedSites.length > 0 ? this.selectedSites.filter(sic => ci.idcircuit === sic.idcircuit) : this.getSitesFromSelectedPlayCircuit(ci);
                let is_valid = (sites_found && sites_found.length > 0) ? sites_found.some(sic => sic.idsite === si.idsite) : ci.checkSites(si);
                return is_valid;
            });
        });
        this.players_filtered = this.all_players.filter(pl => this.sites_filtered.some(si => si.idsite === pl.idsite));
        this.players_filtered.map(pl => pl.idarea).forEach(idarea => { this.idareas_filtered = [...this.idareas_filtered, idarea]; });
    }

    private getSitesFromSelectedPlayCircuit(ci: PlayCircuit) {
        let sites: Site[] = [];
        ci.sites.map(site => {
            sites = [...sites, new Site(site)];
        })
        return sites;
    }

    public sitesChanged(sites: { idsite: number, idcircuit: number }[]) {
        this.selectedSites = sites;
        this.filterSitesAreas();
    }

    private datesChecked() {
        let ok = true;
        this.dates.map(date => {
            if (date.date_on === null || date.date_on === "Invalid date" || date.date_off === "Invalid date" || date.date_off === null) {
                ok = false;
            }
        })
        return ok;
    }

    public customerSelected(idcustomer: number) {
        this.hide = (idcustomer === 0) ? true : false;
        this.idcustomer = idcustomer;
    }

    public saveAllContents() {

        if (!this.checkFormat() && !this.id) {
            alert("Debes crear mensajes")
            return;
        }
        this.disableSubmit = true;
        this.contentElementSaved = 0;
        this.contentErrors = [];



        from(this.contents)
            .pipe(concatMap((content, index) => this.prepareAndSave(content, index)))
            .subscribe(value => {
                if (value != null) {
                    this.contentElementSaved++;
                } else {
                    console.warn('save content error');
                }
            }, err => {
                console.warn('saveAllContents error', err);
            }, () => {
                if (this.contentElementSaved === this.contents.length)
                    this.endSaveContent();
            })
    }
    private endSaveContent() {
        if (this.contentErrors.length > 0) {
            this.dialog.warning("Error al crear contenido", "Algún contenido no se ha podido guardar correctamente. Los verás al cerrar esta ventana.");
            this.contents = this.contentErrors;
        } else {
            if (this.me && this.me.customer) {
                let config: ICacheConfig = { idcustomer: this.me.customer.idcustomer };
                if (this.me.site)
                    config = { idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer };
                setTimeout(() => {
                    this.events.publish('content:change', config);
                }, 1000);
            }
            this.savedContents = true;
            this.disableSubmit = false;
            this.cstore.reload();
            localStorage.clear();
            this.getItems();
            if (!this.isEditing) {
                let dialogRef;
                dialogRef = this.matDialog.open(CreateDialog, {});
                dialogRef.afterClosed().subscribe((result) => {
                    if (result) {
                        this.router.navigateByUrl(this.path);
                    } else {
                        window.location.reload();
                    }
                });
            } else {
                this.dialog.warning("Actualización de contenido", "El contenido se ha actualizado correctamente.");
                this.router.navigateByUrl(this.path);
            }
        }
    }
    private prepareAndSave(content: Content, index: number): Promise<Content> {
        return new Promise(resolve => {
            this.setContentFormDataFromContent(content);
            if (this.contentForm.valid) {
                this.saveContent(content, index).subscribe(co => { resolve(co); }, err => { resolve(null); });
            } else {
                let controls = this.contentForm.controls;
                let errorMessage = "Completar los siguientes campos obligatorios: "
                for (const name in controls) {
                    if (controls[name].invalid) {
                        errorMessage += name + ', ';
                    }
                }
                this.dialog.warning("Error", errorMessage).subscribe(() => { resolve(null); });
            }
        });
    }

    private saveContent(content: Content, index: number): Observable<any> {
        Object.keys(this.contentForm.value).forEach((key) => (this.contentForm.value[key] == null) && delete this.contentForm.value[key]);
        this.contentForm.value.idaudiocategory = this.contentForm.value.idaudiocategory ? this.contentForm.value.idaudiocategory : null;
        let contentToSave = this.generateContentToSave();
        return (this.isEditing) ? this.updateContent(contentToSave, index) : this.newContent(contentToSave, index);
    }

    private generateContentToSave() {
        let contentToSave = {
            form: this.contentForm.value,
            areas: [],
            circuits: [],
            dates: this.dates,
            powers: [],
            products: [],
            assets: [],
            tags: [],
            sites: []
        }
        this.selectedAssets.map(a => {
            contentToSave.assets.push({ idasset: a.idasset });
        })
        this.selectedProducts.map(p => {
            contentToSave.products.push({ idproduct: p.idproduct });
        })
        this.selectedCircuits.map(c => {
            contentToSave.circuits.push({ idcircuit: c.idcircuit });
        })
        this.selectedAreas.map(c => {
            contentToSave.areas.push({ idarea: c.idarea });
        })
        this.selectedTags.map(t => {
            contentToSave.tags.push({ idtag: t.idtag })
        });
        this.selectedSites.map(s => {
            contentToSave.sites.push(s)
        });
        contentToSave.powers = this.selectedContentPowers.map(p => new Power(p));
        return contentToSave;
    }

    private setContentFormDataFromContent(content) {
        this.contentForm.patchValue({
            idaudiocategory: content.idaudiocategory,
            name: content.name,
            idtype: content.idtype,
            code: content.code,
            thumbnail_url: content.thumbnail_url,
            tracker_url: content.tracker_url,
            idcategory: content.idcategory,
            is_exclusive: content.is_exclusive,
            status: content.status,
            primetime: content.primetime,
            frequency: content.frequency,
            frequencyMethod: content.frequency_method,
            maxEmissions: content.max_emissions,
            pixelUrl: content.pixelUrl,
            priority: content.priority
        });

        if (content.assets)
            this.selectedAssets = content.assets.map(a => new Asset(a));
        if (content.playcircuits)
            this.selectedCircuits = content.playcircuits;
        if (content.powers)
            this.selectedContentPowers = content.powers;
        if (content.products)
            this.selectedProducts = content.products;
        if (content.tags)
            this.selectedTags = content.tags;
        if (content.areas)
            this.selectedAreas = content.areas;
    }
    public getItems() {
        let itemsStor = {};
        this.formatsLangs.forEach(function (formatLang) {
            formatLang.data.forEach(function (data, i) {
                if (!itemsStor[data.idformat]) itemsStor[data.idformat] = {}
                itemsStor[data.idformat][data.langid] = this.getLocalStorageItem(data.idformat, data.langid);
            }, this)
        }, this)
        this.storageItems = itemsStor;
    }
    public deleteItemsStore() {
        localStorage.removeItem('allAreas');
        localStorage.setItem('contentDateOn', moment().format("YYYY-MM-D"))
        this.getItems();
        this.router.navigateByUrl(this.path);
    }
    private newContent(contentToSave: any, index: number) {
        return new Observable(obs => {
            if (this.selectedAssets.length === 0) {
                this.openSnackBar("Debes seleccionar al menos un asset.");
            } else if (this.dates.length === 0) {
                this.openSnackBar("Debes seleccionar al menos rango de fechas.");
            } else {

                this._content.create(contentToSave).subscribe((content: Content) => {

                    if (this.showTxt) {
                        this.saveMessage(content.idcontent, content.name);
                    }


                    if (content) {
                        obs.next(contentToSave);
                        obs.complete();
                    } else {
                        this.contentErrors.push(this.contents[index]);
                        this.dialog.warning("Error", "No se ha podido guardar el contenido. Revisa los campos.").subscribe(() => { obs.error(content); });
                    }
                }, error => {
                    this.contentErrors.push(this.contents[index]);
                    const err = error.error.data;
                    let firstKey = Object.keys(err)[0];
                    this.debug.addError('WS.createContent', error.message);
                    this.dialog.warning("Error", err[firstKey][0]).subscribe(() => { obs.error(error.message); });
                });
            }
        });
    }




    private updateContent(contentToSave: any, index: number) {
        return new Observable(obs => {
            this._content.update(contentToSave, this.id).subscribe((content: Content) => {
                if (content) {
                    if (this.showTxt) {

                        this.saveMessage(content.idcontent, content.name);
                    } else {

                        this._message.deleteMsg(content.idcontent).subscribe(item => {

                        });

                    }
                    obs.next(contentToSave);
                    obs.complete();
                } else {
                    this.contentErrors.push(this.contents[index]);
                }
            }, error => {
                this.contentErrors.push(this.contents[index]);
            });
        });
    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }

    public changedForm(form) {
        if (this.me) {
            this.contentForm = form;
            this.contents[this.contentIndex].idaudiocategory = this.contentForm.get('idaudiocategory').value;
            this.contents[this.contentIndex].code = this.contentForm.get('code').value;
            this.contents[this.contentIndex].frequency = this.contentForm.get('frequency').value;
            this.contents[this.contentIndex].frequency_method = this.contentForm.get('frequencyMethod').value;
            this.contents[this.contentIndex].idcategory = this.contentForm.get('idcategory').value;
            this.contents[this.contentIndex].customer = !this.me.customer ? this.contentForm.get('idcustomer').value : this.me.customer.idcustomer;
            this.contents[this.contentIndex].idtype = this.contentForm.get('idtype').value;
            this.contents[this.contentIndex].is_exclusive = this.contentForm.get('is_exclusive').value;
            this.contents[this.contentIndex].max_emissions = this.contentForm.get('maxEmissions').value;
            this.contents[this.contentIndex].priority = this.contentForm.get('priority').value;
            this.contents[this.contentIndex].name = this.contentForm.get('name').value;
            this.contents[this.contentIndex].primetime = this.contentForm.get('primetime').value;
            this.contents[this.contentIndex].status = this.contentForm.get('status').value;
            this.contents[this.contentIndex].thumbnail_url = this.contentForm.get('thumbnail_url').value;
        }
    }

    public changedDates(dates) {
        this.dates = dates;
        if (this.contents[this.contentIndex])
            this.contents[this.contentIndex].dates = this.dates;
    }
    public changedTags(tags) {

        this.selectedTags = tags.map(t => new Tag(t));
        if (this.contents[this.contentIndex])
            this.contents[this.contentIndex].tags = this.selectedTags.map(t => new Tag(t));
        if (this.calculate) {
            this.calculate.calculate(this.selectedTags);
        }

    }
    public changedProducts(products) {
        this.selectedProducts = products.map(p => new Product(p));
        if (this.contents[this.contentIndex])
            this.contents[this.contentIndex].products = this.selectedProducts;
        if (CONFIG.demo && this.selectedAreas.length === 0) {
            this.getAreasFromProduct(this.selectedProducts[0]);
        }
    }
    public changedPowers(powers) {
        this.selectedContentPowers = powers.map(p => new Power(p));
        if (this.contents[this.contentIndex])
            this.contents[this.contentIndex].powers = this.selectedContentPowers;
    }
    public selectedAssetsChanged(assets) {
        let changeName: boolean = false;
        if (this.contents.length <= 0)
            return;
        if (!this.contents[this.contentIndex].name || this.contents[this.contentIndex].name === '' || this.selectedAssets[0] && this.selectedAssets[0].idasset !== this.contents[this.contentIndex].assets[0].idasset && (this.selectedAssets[0].name === this.contents[this.contentIndex].name))
            changeName = true;
        this.selectedAssets = assets.map(a => new Asset(a));
        if (this.contents[this.contentIndex])
            this.contents[this.contentIndex].assets = this.selectedAssets.map(a => new Asset(a));

        if (changeName && this.selectedAssets[0]) {
            this.contents[this.contentIndex].name = this.selectedAssets[0].name;
            this.contentForm.patchValue({ name: this.selectedAssets[0].name });
        }
    }
    public datesChanged(data) {
        this.dateChecked = this.checkDates(data);
        if (this.dateChecked) {
            this.dates[data.position] = { date_on: data.date_on, date_off: data.date_off };
            let datesChecked = this.datesChecked();
            if (this.selectedCircuits.length > 0 && datesChecked) {
                this.hideButton = false;
            }
        } else {
            this.dialog.warning("Fechas", 'Las fechas introducidas no son correctas');
            this.dates[data.position] = { date_on: null, date_off: null };
        }
    }
    private checkDates(data) {
        let checked: boolean = true;
        if (data.length > 0) {
            data.map(dat => {
                if (dat.date_on !== "Invalid date" && dat.date_off !== "Invalid date")
                    if (moment(dat.date_on) <= moment(dat.date_off))
                        this.dates.map((date, i) => {
                            if (i !== dat.position) {
                                if ((dat.date_on <= date.date_off && dat.date_off >= date.date_on))
                                    checked = false;
                            }
                        })
                    else
                        checked = false;
                else {
                    if (dat.date_on !== "Invalid date")
                        this.dates.map((date, i) => {
                            if (i !== dat.position) {
                                if (dat.date_on >= date.date_on && dat.date_on <= date.date_off) {
                                    checked = false;
                                }
                            }
                        })
                    if (dat.date_off !== "Invalid date")
                        this.dates.map((date, i) => {
                            if (i !== dat.position) {
                                if (dat.date_off >= date.date_on && dat.date_off <= date.date_off) {
                                    checked = false;
                                }
                            }
                        })
                }
            })
        } else {
            if (data.date_on !== "Invalid date" && data.date_off !== "Invalid date")
                if (moment(data.date_on) <= moment(data.date_off))
                    this.dates.map((date, i) => {
                        if (i !== data.position) {
                            if ((data.date_on <= date.date_off && data.date_off >= date.date_on))
                                checked = false;
                        }
                    })
                else
                    checked = false;
            else {
                if (data.date_on !== "Invalid date")
                    this.dates.map((date, i) => {
                        if (i !== data.position) {
                            if (data.date_on >= date.date_on && data.date_on <= date.date_off) {
                                checked = false;
                            }
                        }
                    })
                if (data.date_off !== "Invalid date")
                    this.dates.map((date, i) => {
                        if (i !== data.position) {
                            if (data.date_off >= date.date_on && data.date_off <= date.date_off) {
                                checked = false;
                            }
                        }
                    })
            }
        }
        return checked;
    }
    public addDate() {
        this.dates.push({ date_on: null, date_off: null });
    }

    public deleteDate(i) {
        this.dates.splice(i, 1);
        this.dateChecked = this.datesChecked();
        if (this.selectedCircuits.length > 0 && this.dateChecked) {
            this.hideButton = false;
        }
    }
    public createNewContent(data) {
        let formData = this.contentForm.getRawValue();
        let content: IContent = {
            idaudiocategory: formData.idaudiocategory,
            playcircuits: this.selectedCircuits,
            playareas: this.selectedAreas,
            dates: this.dates,
            tags: this.selectedTags,
            powers: this.selectedContentPowers,
            products: [],
            assets: [],
            code: formData.code,
            frequency: formData.frequency,
            frequency_method: formData.frequencyMethod,
            idcategory: formData.idcategory,
            customer: formData.idcustomer,
            idtype: formData.idtype,
            is_exclusive: formData.is_exclusive,
            max_emissions: formData.maxEmissions,
            name: null,
            primetime: formData.primetime,
            status: formData.status,
            thumbnail_url: formData.thumbnail_url,
            priority: formData.priority
        }
        this.contents.push(new Content(content));
        this.contentIndex = this.contents.length - 1;
    }

    public selectContentIndex(index) {
        this.contentIndex = index;
        this.selectedCircuits = this.contents[this.contentIndex].playcircuits;
        this.selectedAreas = this.contents[this.contentIndex].playareas.map(a => new PlayArea(a));
        this.selectedContentPowers = this.contents[this.contentIndex].powers.map(p => new Power(p));
        this.selectedProducts = this.contents[this.contentIndex].products.map(p => new Product);
        this.selectedTags = this.contents[this.contentIndex].tags.map(t => new Tag(t));
        this.dates = this.contents[this.contentIndex].dates;
        this.contentForm.patchValue({
            idaudiocategory: this.contents[this.contentIndex].idaudiocategory,
            name: this.contents[this.contentIndex].name,
            idtype: this.contents[this.contentIndex].idtype,
            code: this.contents[this.contentIndex].code,
            thumbnail_url: this.contents[this.contentIndex].thumbnail_url,
            tracker_url: this.contents[this.contentIndex].tracker_url,
            idcategory: this.contents[this.contentIndex].idcategory,
            is_exclusive: this.contents[this.contentIndex].is_exclusive,
            status: this.contents[this.contentIndex].status,
            primetime: this.contents[this.contentIndex].primetime,
            frequency: this.contents[this.contentIndex].frequency,
            frequencyMethod: this.contents[this.contentIndex].frequency_method,
            maxEmissions: this.contents[this.contentIndex].max_emissions,
            pixelUrl: null,
            priority: this.contents[this.contentIndex].priority
        });
    }
}