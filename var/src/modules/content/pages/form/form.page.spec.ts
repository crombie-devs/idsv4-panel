import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { ContentFormPage } from './form.page';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';

import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

import { MaterialModule } from 'src/app/material.module';
import { OverlayModule } from '@angular/cdk/overlay';

import { WSService } from 'src/services/ws.service';
import { HttpClient } from "@angular/common/http";
import { HttpLoaderFactory } from "src/shared/shared.module";
import { DebugStore } from 'src/stores/debug.store';
import moment from 'moment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogModule } from 'src/modules/dialog/dialog.module';
import { userMock } from 'src/modules/user/models/user.mocks';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { of } from "rxjs";

const MatDialogMock = {
    open(){
        return {
            afterClosed: () =>  of({cancel: false})
        };
    }
}

describe('Página: ContentFormPage', () => {
    let component: ContentFormPage;
    let fixture: ComponentFixture<ContentFormPage>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports:[
                MaterialModule,
                OverlayModule,
                RouterTestingModule,
                BrowserAnimationsModule,
                DialogModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                }),
                HttpClientTestingModule
            ],
            declarations: [
                ContentFormPage
            ],
            providers:[
                WSService, 
                DebugStore,
                {
                    provide: MAT_DIALOG_DATA, useValue: {},
                },
                {
                    provide: MatDialogRef, useValue: MatDialogMock
                }
            ],
            schemas:[CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        }).compileComponents();
    });

    beforeEach(async(() => {
        fixture = TestBed.createComponent(ContentFormPage);
        component = fixture.componentInstance;
        component.me = userMock;
        fixture.whenStable().then(() => {
            fixture.detectChanges();
        })
    }));

    it("Creando componente de página", () => {
        expect(component).toBeTruthy();
    });
    /**
     * Inicio test para fechas
     */
    it("Dates Función: datesChanged", () => {
        let data = {position: 0, date_on: moment().format("YYYY-MM-DD"), date_off: moment().format("YYYY-MM-DD")};
        const spy = spyOn((component as any), 'datesChecked').and.callThrough();
        component.datesChanged(data);
        expect(spy).toHaveBeenCalled();
        expect(component.dateChecked).toEqual(true); 
    })
    it("Dates Función: checkDates:true", () => {
        let data = {position: 0, date_on: moment().format("YYYY-MM-DD"), date_off: moment().format("YYYY-MM-DD")};
        const spy = spyOn((component as any), 'checkDates').and.callThrough();
        component.datesChanged(data);
        expect(spy).toHaveBeenCalled();
        expect(component.dateChecked).toEqual(true);
    });
    //Comentada para que no salga el dialogo de error
    xit("Dates Función: checkDates:false", () => {
        let data = {position: 1, date_on: moment().format("YYYY-MM-DD"), date_off: moment().format("YYYY-MM-DD")};
        component.dates = [{date_on: moment().format("YYYY-MM-DD"), date_off: moment().format("YYYY-MM-DD")}];
        const spy = spyOn((component as any), 'checkDates').and.callThrough();
        component.datesChanged(data);
        expect(spy).toHaveBeenCalled();
        expect(component.dateChecked).toEqual(false);
    });
    it("Dates Función: deleteDate", () => {
        component.deleteDate(0);
        expect(component.dateChecked).toEqual(true);
    });
    it("Dates Función: addDate", () => {
        let initLength = component.dates.length;
        component.addDate();
        expect(component.dates.length).toBeGreaterThan(initLength);
    });
    // Fin tests para fechas
});