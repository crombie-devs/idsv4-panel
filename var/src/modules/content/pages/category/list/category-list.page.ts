import { Component, OnInit } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { IResult } from 'src/models/iresult';
import { DebugStore } from 'src/stores/debug.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { ContentCategory } from 'src/modules/content/models/category';
import { MatDialog } from '@angular/material/dialog';
import { ContentCategoryDialog } from 'src/modules/content/dialogs/category/category.dialog';
import { ContentCategoryStore } from 'src/modules/content/stores/category.store';

@Component({
  providers: [ContentCategoryStore],
  selector: 'content-category-list',
  templateUrl: './category-list.page.html',
  styleUrls: ['./category-list.page.scss']
})

export class ContentCategoryListPage implements OnInit {
  
	public newContentCategory: boolean = false;
	public editContentCategory: boolean = false;
	public deletedContentCategory: boolean = false;
	public customersToSelect: boolean = false;
	public permission: boolean = false;
	public load: boolean = false;
	public me: any;
	public contentcategories: ContentCategory[] = [];	
	public contentcategoriesFull: ContentCategory[] = [];
	public filterVal:string='';
	public customerId: number = null;
	
  constructor(
    private ws: WSService,
		private debug: DebugStore,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private dialog: DialogService,
		public matDialog: MatDialog,
		private ccstore: ContentCategoryStore
    ) {
		this.ustore.onChange.subscribe(user => { 
            this.me = user; 
        });  
		this.ccstore.onChange.subscribe(categories => {
		
			this.contentcategoriesFull = categories;
			this.contentcategories = categories;
			let customer = !this.customerId?this.me.customer:this.customerId;
			this.customerSelected(customer);
		})
	}

  	ngOnInit(): void {
	  	this.verifyPermissions();	 
  	}   
   

	private verifyPermissions() {
		this.newContentCategory = this.ustore.hasPermissionByName("post.content-category");
		this.editContentCategory = this.ustore.hasPermissionByName("post.content-category.*");
		this.deletedContentCategory = this.ustore.hasPermissionByName("delete.content-category.*");
		this.customersToSelect = this.ustore.hasPermissionByName("get.customers.toselect");
	}
    
  
  	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

  	public customerSelected(idcustomer:number) {
		this.customerId = idcustomer;
    	this.contentcategories = this.contentcategoriesFull;
		if(idcustomer > 0){
			let filterArray = Object.values(this.contentcategories).filter(val => val.idcustomer == idcustomer);				
			this.contentcategories = filterArray;
		}else
			this.contentcategories = this.contentcategoriesFull
  	}
  
  	public change(){  
		this.ccstore.reload();
	} 

  	public openDialog(){		  
		let dialogRef;
		if(!this.me.customer){  
			if(this.customerId && this.customerId>0){
				dialogRef = this.matDialog.open(ContentCategoryDialog, {
					data: {"idcustomer": this.customerId }
				});
				dialogRef.afterClosed().subscribe(result => {
					this.openSnackBar(result);
					this.ccstore.reload();
				});		
			}else{
				this.dialog.warning('Aviso', 'Debe seleccionar un cliente').subscribe();
			}
		}else{
			dialogRef = this.matDialog.open(ContentCategoryDialog, {
				data: {}
			});
			dialogRef.afterClosed().subscribe(result => {
				this.openSnackBar(result);
				this.ccstore.reload();
			});		
		}
	}
}
