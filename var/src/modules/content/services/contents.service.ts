import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Content } from 'src/modules/content/models/content';
import { HttpService } from 'src/services/http.service';
import { ICrudWS } from 'src/interfaces/ICrudWS.interface';

Request
@Injectable({
    providedIn: 'root',
})
export class ContentService implements ICrudWS<Content>{
    constructor(private http: HttpService) { }

    public create(obj: any): Observable<Content> {
        return this.http.create('private/content', obj, 'WS.site  _add');
    }
    public update(obj: any, id: number): Observable<Content> {
        return this.http.update(`private/content/${id}`, obj, 'WS.site_add');
    }
    public delete(id: number): Observable<boolean> {
        return this.http.delete('private/content', [id], 'WS.site_delete');
    }
    public get(id: number): Observable<Content> {
        return this.http.get('private/content', [id], 'WS.site');
    }
    public toSelect(args?: any[]): Observable<Content[]> {
        return this.http.get('private/contents/toselect', args, 'WS.site');
    }
    public list(args?: any[]): Observable<Content[]> {
        return this.http.get('private/contents', args, 'WS.site');
    }
    public updateArchived(args?: number[]): Observable<boolean[]> {
        return this.http.get('private/content/archived', args, 'WS.content_update_archived');
    }
    public contentsbystatus(status: string,) {
        return this.http.get(`private/contents/${status}`, null, 'WS.content');
    }
    public changeStatus(id: number): Observable<Content> {
        return this.http.get('private/content/status', [id], 'WS.content_change_status');
    }


    public uploadCSV(csv: File, idmessage: string, datetime: any): Observable<any> {
        return this.http.uploadCSV('private/people/csv', csv, idmessage, datetime, 'WS.content_change_status');
    }
}


