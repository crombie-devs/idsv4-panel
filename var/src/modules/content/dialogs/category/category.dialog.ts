import { Component, Inject, resolveForwardRef, Type, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { AssetTypeStore } from 'src/modules/asset/stores/assettype.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { ContentCategory } from 'src/modules/content/models/category';
import { Customer } from 'src/modules/customer/models/customer';
import { MaxSizeValidator } from '@angular-material-components/file-input';
import { DebugStore } from 'src/stores/debug.store';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
    providers: [FormatStore, LangStore, AssetTypeStore],
    selector: 'content-category-dialog',
    templateUrl: './category.dialog.html',
    styleUrls: ['./category.dialog.scss']
})

export class ContentCategoryDialog {
    public id: number | null;
    public idcustomer: number | null;
    public me: any;
    public form: FormGroup;

    public name: FormControl = new FormControl('', [Validators.required]);
    public duration: FormControl = new FormControl('');
    public customer: FormControl = new FormControl('');
    public random: FormControl = new FormControl(false);
    public image_url: FormControl;

    public customers: Customer[];

    public loading: boolean = false;
    public contentcategory: ContentCategory = new ContentCategory(null);
    public files;
    public maxSize = 500;
    public url: SafeHtml;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: ContentCategory,
        public dialogRef: MatDialogRef<ContentCategoryDialog>,
        private ws: WSService,
        private ustore: UserStore,
        private debug: DebugStore,
        private fb: FormBuilder,
        private _snackBar: MatSnackBar,
        private sanitizer: DomSanitizer) {
        this.id = Number(this.data.idcategory);
        this.image_url = new FormControl('', [
            MaxSizeValidator(this.maxSize * 1024)
        ]);
        this.ustore.onChange.subscribe(user => {
            this.me = user;
        });
    }

    public ngOnInit() {
        this.image_url.valueChanges.subscribe((file: File) => {
            this.files = file;
            this.url = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(this.files));

        });
        this.initForm(null);
        if (this.id) {
            this.ws.contentCategory(this.id).subscribe((result: IResult) => {
                if (result.success) {
                    this.debug.addInfo('WS.contentCategory', 'successful');
                    this.contentcategory = new ContentCategory(result.data);
                    this.idcustomer = this.contentcategory.idcustomer;
                    this.initForm(this.contentcategory);
                }
                else {
                    this.debug.addError('WS.contentCategory', result.data);
                }
            }, error => {
                this.debug.addError('WS.contentCategory', error.message);
            });
        } else {
            this.idcustomer = (this.me.customer) ? this.me.customer.idcustomer : this.data.idcustomer;
            this.initForm(null);
        }
    }

    onDisabledChanged(value: boolean) {
        if (!value) {
            this.image_url.enable();
        } else {
            this.image_url.disable();
        }
    }

    private initForm(data: ContentCategory | null) {
        if (!data) {
            this.form = this.fb.group({
                name: this.name,
                image_url: this.image_url,
                duration: this.duration,
                customer: this.customer,
                random: this.random
            });
        } else {
            this.url = data.image_url;
            this.form = this.fb.group({
                name: data.name,
                image_url: data.image_url,
                duration: data.duration,
                customer: data.idcustomer,
                random: data.random
            });
        }
    }

    public create() {
        if (this.form.invalid)
            return null;
        this.loading = true;

        let formData: any = new FormData();
        formData.append("name", this.form.get('name').value);
        formData.append("idcustomer", this.idcustomer);
        formData.append("image_url", this.form.get('image_url').value);
        formData.append("duration", this.form.get('duration').value);
        formData.append("random", this.form.get('random').value);

        this.ws.createCategory(formData).subscribe((result: IResult) => {
            if (result.success) {
                const id = result.data.idcircuit;
                this.debug.addInfo('WS.createCategory', 'successful');
                this.dialogRef.close("Categoria creada correctamente");
                this.loading = false;
            }
            else {
                this.debug.addError('WS.createCategory', result.data);
                this.dialogRef.close("La Categoria no se ha creado correctamente" + result.data);
                this.loading = false;
            }
        }, error => {
            let msj = JSON.stringify(error.error.data);
            this.debug.addError('WS.createCategory', error.message);
            this.openSnackBar(msj.substr(2, msj.length - 3));
            this.loading = false;
        });
    }

    public update() {
        this.loading = true;

        let formData: any = new FormData();
        formData.append("name", this.form.get('name').value);
        formData.append("idcustomer", this.idcustomer);
        formData.append("image_url", this.files ? this.files : this.form.get('image_url').value);
        formData.append("duration", this.form.get('duration').value);
        formData.append("random", this.form.get('random').value);

        this.ws.updateCategory(this.id, formData).subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.updateCategory', 'successful');
                this.dialogRef.close(result);
                this.loading = false;
            }
            else {
                this.debug.addError('WS.updateCategory', result.data);
                this.dialogRef.close(result);
                this.loading = false;

            }
        }, error => {
            let msj = JSON.stringify(error.error.data);
            this.debug.addError('WS.updateCategory', error.message);
            this.openSnackBar(msj.substr(2, msj.length - 3));
            this.loading = false;
        });
    }

    public close() {
        this.dialogRef.close();
    }

    public save() {
        if (!this.id)
            this.create();
        else
            this.update();

    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }

}