import { Component,  Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'create-dialog',
  	templateUrl: './create.dialog.html',
  	styleUrls: ['./create.dialog.css']
})

export class CreateDialog { 
  constructor(public dialogRef: MatDialogRef<CreateDialog>, @Inject(MAT_DIALOG_DATA) public data) {
    dialogRef.disableClose = true;
  }
  
  public save() {
    this.dialogRef.close(true);
  }

  public close() {
    this.dialogRef.close(false);
  } 
   
}
