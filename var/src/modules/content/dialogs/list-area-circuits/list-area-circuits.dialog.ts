import { Component,  Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';




@Component({
	selector: 'list-area-circuits-dialog',
  	templateUrl: './list-area-circuits.dialog.html',
  	styleUrls: ['./list-area-circuits.dialog.css']
})

export class ListAreasCircuitsDialog {
  
  rows: any;
  type: string;
  title: string;
 
  constructor(public dialogRef: MatDialogRef<ListAreasCircuitsDialog>, @Inject(MAT_DIALOG_DATA) public data) {
    
          this.rows = data.data;
          this.type = data.type;
          this.title =  'list_' + data.type;
        
        
  }
  
  public save() {
    this.dialogRef.close(true);
}

public close() {
    this.dialogRef.close(false);
} 
   
}
