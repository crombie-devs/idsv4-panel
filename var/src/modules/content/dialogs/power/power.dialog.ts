import { Component, ComponentRef, Inject, ViewChild, ViewContainerRef, ComponentFactoryResolver, Type, SimpleChange, SimpleChanges } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { WSService } from 'src/services/ws.service';
import { IResult } from 'src/models/iresult';
import { ContentPower } from 'src/modules/content/models/power';
import { DebugStore } from 'src/stores/debug.store';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { WEEKDAYS, TIMES } from 'src/modules/content/models/powerdata';
import { PowerSelectComponent } from 'src/modules/power/components/select/select.component';
import { Power } from 'src/modules/power/models/power';
import { throwIfEmpty } from 'rxjs/operators';
import { UserStore } from "src/modules/user/stores/user.store";
import { FormControl, FormGroup } from '@angular/forms';
import { FormField } from 'src/modules/form-field/models/form-field';

@Component({
	selector: 'content-power-dialog',
  	templateUrl: './power.dialog.html',
  	styleUrls: ['./power.dialog.css']
})

export class ContentPowerDialog {
    
    @ViewChild('powersContainer', {read: ViewContainerRef}) powersContainer: ViewContainerRef;
    
    components = [];
    PowerSelectComponentClass = PowerSelectComponent

    public title: string;
    public load = false;
    public contentpowers: ContentPower[] = [];
    public selectedPowers:any[] =  [];
    public dataBefore: {};
    public weekdays = WEEKDAYS
    public times = TIMES;
    public isMouseDown = false
    public powerCount: number = 1
    public newSelection: any = {};
    public selecting:boolean = true;
    public selectedData:any = {};
    public has_primetime:boolean = false;
    public dayFrom: string = "";
    public daysTo: string[] = [];
    public me: any;
    public selectorController = new FormControl('M');
    public primetimeData:any = {};
    public timetable:string = 'M';
    public showPrimetime:boolean = false;
    public initPowers: Power[] = [];

    public is_exclusive = false;

    public fieldDataSites: FormField = {
		label: "Centros",
		name: 'site',
		placeholder: '',
		options: [],
		multiple:false,
		required: true
	};
	public formGroup:FormGroup = new FormGroup({
        site: new FormControl(null)
    })
	constructor(public dialogRef: MatDialogRef<ContentPowerDialog>,
	    @Inject(MAT_DIALOG_DATA) public data,
	    private ws: WSService,
	    private debug: DebugStore,
        private ustore: UserStore,
	    private componentFactoryResolver: ComponentFactoryResolver) {
            this.ustore.onChange.subscribe(user => { 
                this.me = user; 
              });
    }
  
    public ngOnInit() {
        if(this.data.selections) {
            this.selectedPowers = this.data.selections.map(p => new Power(p));
            this.initPowers = this.selectedPowers.map(p => new Power(p));
            this.paintPowers();
        }
        if(this.data.data){
            this.selectedData = this.data.data;
        }
        if(this.data.has_primetime){
            this.has_primetime = this.data.has_primetime;
            this.getPrimetimeData();
        }
        if(this.data.is_exclusive) {
            this.is_exclusive = this.data.is_exclusive;
        }
    }
    private resetTimes(){
        this.times.map(day => {
            day.times.map(time => {
                time.selected = false;
                time.full = true;
            })
        })
    }
    private paintPowers(site = null){
        this.resetTimes();
        if(!site)
            this.times.map((day) => {
                this.selectedPowers.map(sp => {
                    if(sp.time_on && sp.time_off) {
                        if(sp.weekday === day.day_number){
                            let time_on = sp.time_on.split(':');
                            let time_off = sp.time_off.split(':');
                            day.times.map(t => {
                                if(t.value >= parseInt(time_on[0]) && t.value <= parseInt(time_off[0])){
                                    t.selected = true;
                                    if(t.value === parseInt(time_on[0]) && time_on[1] !== '00'){
                                        t.full = false;
                                    }
                                    if(t.value === parseInt(time_off[0]) && time_off[1] !== '59'){
                                        t.full = false;
                                    }
                                }
                            })
                        }
                    }
                   })
            })
        else{
            let sitetimes = this.primetimeData[site];
            this.times.map((day) => {
                sitetimes.map(sp => {
                    if(sp.weekday === day.day_number){
                        day.times.map(t => {
                            if(t.value === sp.time){
                                t.selected = true;
                                t.full = true;
                            }
                        })
                    }
                })
            })
        }
    }

    public copyDays(){     
        let selectionsToCopy:any[] = [];
        this.selectedPowers.map(day => {
            if(day.weekday === parseInt(this.dayFrom)){
                selectionsToCopy.push(day);
            }
        })
        this.daysTo.map((day) => {
            selectionsToCopy.map(sel => {
                this.selectedPowers.push({weekday: parseInt(day), time_on: sel.time_on, time_off: sel.time_off, new: true})
            })
        })
        this.orderSelectedPowers();
        this.paintPowers();
    }
       
    public save() {
        this.initPowers = this.selectedPowers;
        this.dialogRef.close(this.selectedPowers);
    }

    public close() {
        this.selectedPowers = this.initPowers.map(p => new Power(p));
        this.orderSelectedPowers();
        this.paintPowers();
        this.dialogRef.close(false);
    }
    public addSelection(selection){
      
        selection.map(sel => { this.selectedPowers.push(sel) });
      
        this.orderSelectedPowers();
        this.paintPowers();
    }

    public removeSelection(selection){
        this.selectedPowers.map((s,index) => {
            if(s.weekday === selection.weekday && s.time_on === selection.time_on && s.time_off && selection.time_off){
                this.selectedPowers.splice(index, 1);
            }
        })
        this.orderSelectedPowers();
        this.paintPowers();
    }
    public setSelection(e, selection) {
        if(this.selecting)
            selection.selected = true;
        else 
            selection.selected = false;
    }
    
    public onMouseDown(e, weekday, selection) {
        this.isMouseDown = true;
        if(selection.selected){
            this.selecting = false;
        }else{
            this.selecting = true;
        }
        this.newSelection = {
            weekday: weekday.day_number, 
            new: true,
            time_on: selection.name
        }
        this.setSelection(e, selection)
    }
    
    public onMouseOver(e, weekday, selection) {
        if(this.isMouseDown && this.newSelection.weekday === weekday.day_number) {
            this.setSelection(e, selection)
        }
    }
    
    public onMouseUp(e, weekday, selection) {
        if(this.selecting){
            if(this.newSelection.weekday === weekday.day_number){
                let sel = selection.name.split(':');
                let timeInit = this.newSelection.time_on.split(':');
                if(parseInt(sel) < parseInt(timeInit)){
                    this.newSelection.time_on = `${sel[0]}:00`;
                    this.newSelection.time_off = `${timeInit[0]}:59`;
                }else{
                    this.newSelection.time_off = `${sel[0]}:59`;
                }
                this.selectedPowers.push(this.newSelection);
                this.newSelection = {};
            }
        } else {
            this.newSelection.time_off = selection.name;
            this.removeNewSelection();
        }
        this.orderSelectedPowers();
        this.isMouseDown = false
        this.paintPowers();
    }

    private removeNewSelection(){
        let times = this.times.filter(t => t.day_number === this.newSelection.weekday);
        let selected = this.selectedPowers.filter(sp => sp.weekday === this.newSelection.weekday);
        let arrayNewSelections:any[] = [];
       
        let selectedTimeOn:boolean = false;
        let isManual:boolean = false;
        let arrayTimes = times[0].times;
        arrayTimes.map(t => {
            if(t.selected && !t.full){
                isManual = !isManual;
                selected.forEach(sel => {
                    if(sel.time_on.split(':')[1] !== '00' || sel.time_off.split(':')[1] !== '59' )
                        if(!arrayNewSelections.some(ns => ns.time_on === sel.time_on))
                            arrayNewSelections.push(sel)
                })
            }
            if(t.selected && !selectedTimeOn && !isManual){
                arrayNewSelections.push({weekday: this.newSelection.weekday, time_on: t.name, new:true});
                selectedTimeOn = true;
            }else if(!t.selected && selectedTimeOn){
                let hour = parseInt(t.name.split(':')[0]) - 1;
                let hourStr = '';
                if(hour < 10)
                    hourStr = `0${hour}:59`;
                else
                    hourStr = `${hour}:59`;
                arrayNewSelections[arrayNewSelections.length - 1].time_off = hourStr;
                selectedTimeOn = false;
            }
        })
        this.selectedPowers.map((sp,i) => {
            if(sp.weekday === this.newSelection.weekday)
                this.selectedPowers.splice(i,1);
        });
        arrayNewSelections.map(ns => this.selectedPowers.push(ns));

        this.selectedPowers = this.selectedPowers.filter( pow => pow.hasOwnProperty('time_off'))

        this.orderSelectedPowers();
    }

    private orderSelectedPowers(){
        this.selectedPowers.sort((a,b) =>{
            if (a.weekday === b.weekday) {
                let atime = a.time_on.split(':');
                let btime = b.time_on.split(':');
                return parseInt(atime[0]) - parseInt(btime[0]);
             }
             return a.weekday > b.weekday ? 1 : -1;
        });
    }
    public selectDayFrom(e, dayFrom){
        e.target.parentNode.childNodes.forEach(function(elm) { if(elm.classList) elm.classList.remove("selected");});
        e.target.classList.toggle("selected");
        if(dayFrom && this.dayFrom != dayFrom) this.dayFrom = dayFrom;
        else this.dayFrom = "";
    }
    
    public selectDayTo(e, dayTo){
        e.target.classList.toggle("selected");
        var i = this.daysTo.indexOf(dayTo);
        if(i == -1) this.daysTo.push(dayTo);
        else this.daysTo.splice(i, 1);
    }

    public changeTimetable(selectedTimetable){
       
        this.timetable = selectedTimetable;
        if(this.timetable === 'M'){
            this.selectorController.setValue('M');
            this.paintPowers();
        }else{
            this.selectorController.setValue('P');
            let site = this.formGroup.get('site').value;
            this.paintPowers(site);
        }
    }

    public getPrimetimeData(){
        const products = this.selectedData.products;
        const circuits = this.selectedData.circuits;
        const sites = this.selectedData.sites;
        this.primetimeData = this.selectedData.primetimeData;
        let params:any = {categories:[], sites:[]};
        if(products)
            products.map(product => {
                if(!params.categories.some(c => c === product.category.code))
                    params.categories.push(product.category.code);
            })
        if(circuits){
            circuits.map(circuit => {
                let complete = true;
                circuit.sites.map(site => {
                    if(sites && sites.some(s => site.idsite === s.idsite)){
                        complete = false;
                        if(!params.sites.some(s => s === site.code)){
                            params.sites.push(site.code);
                            this.fieldDataSites.options.push({value: site.name, name: site.name});
                        }

                    }
                })
                if(complete){
                    circuit.sites.map(site =>{
                        params.sites.push(site.code);
                        this.fieldDataSites.options.push({value: site.name, name: site.name});
                    });
                }
            })
            this.formGroup.patchValue({site:this.fieldDataSites.options[0].value});
        }else if(sites){
            sites.map(site =>{
                params.sites.push(site.code);
                this.fieldDataSites.options.push({value: site.name, name: site.name});
            });
            this.formGroup.patchValue({site:this.fieldDataSites.options[0].value});
        }
        this.showPrimetime = true;
      }

      organize(data){
        let result = [];
        data.map(element => {
          if(!result[element.site]){
            result[element.site] = [];
            result[element.site].push({weekday: element.weekday, time: element.hour});
          }else{
            if(!result[element.site].some(el => el.weekday === element.weekday && el.time === element.hour))
                result[element.site].push({weekday: element.weekday, time: element.hour});
          }
        });
        return result;
    }

    public onChangeSite(data){
        const site = this.formGroup.get('site').value;
        this.paintPowers(site);
    }

    public cleanTimetable(){
        this.selectedPowers = [];
        this.paintPowers();
    }
}
