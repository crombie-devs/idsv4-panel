import { Component, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Site } from "src/modules/site/models/site";
import { MatSort, Sort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { SiteStore } from 'src/modules/site/stores/site.store';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
    providers: [SiteStore],
	selector: 'content-sites-dialog',
  	templateUrl: './sites.dialog.html',
  	styleUrls: ['./sites.dialog.css']
})

export class ContentSitesDialog {

    @ViewChild(MatSort) sort: MatSort;
    
    public sites: Site[] = [];
    public searchText: string = '';

    public displayedColumns: string[] = ['select', 'idsite', 'name'];
    public selection = new SelectionModel<Site>(true, []);
    public dataSource = new MatTableDataSource<Site>();
    public sortedData: Site[];

    constructor(
        private store: SiteStore,
        @Inject(MAT_DIALOG_DATA) public data: { selected: number[], filterByCircuit?: PlayCircuit, invert: boolean },
        public dialogRef: MatDialogRef<ContentSitesDialog>) {
    }

    public ngOnInit() {
        this.store.onChange.subscribe(sites => {
            this.sites = (!this.data.filterByCircuit)? sites : sites.filter(si => this.data.filterByCircuit.checkSites(si));
            this.renderDataTable();
        });
    }

    private renderDataTable() {
        this.dataSource = new MatTableDataSource(this.sites);
        this.sortedData = this.sites.slice();
        this.dataSource.sort = this.sort;
        this.renderCheckboxes();
    }

    private renderCheckboxes() {
        this.selection.clear();
        if(this.data.selected && this.data.selected.length > 0) {
            this.sites.forEach(si => {
                if(this.data.selected.some(idsite => idsite === si.idsite))
                    this.selection.select(si);
            });
        }
        else if(this.data.invert) {
            this.sites.forEach(row => this.selection.select(row));
        }
    }

    public save() {   
        let sites_selected: number[] | null = this.selection.selected.map(si => si.idsite);
        if(sites_selected.length === this.sites.length)
            sites_selected = [];
        else if(sites_selected.length === 0)
            sites_selected = null;

        this.dialogRef.close(sites_selected);
    }

    public close(){
        this.dialogRef.close(false);
    }

    public isAllSelected() {
        const numRows = this.sites.length;
        const numSelected = this.selection.selected.length;

        return (numSelected === numRows);
    }

    public masterToggle(event: MatCheckboxChange) {
        if(this.selection.hasValue() && !this.isAllSelected()){
			event.source.checked = false;
		} 
        if(this.selection.hasValue())
            this.selection.clear();
        else
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    public checkboxLabel(row?: Site): string {
        let label =  `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        if(row)
            label =  `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idsite + 1}`;

        return label;
    }	

    public filterByName(q: string) {
        this.dataSource.data = (!q)? this.sites : this.sites.filter(si => si.name.toLowerCase().includes(q.trim().toLowerCase()) || si.idsite.toString().includes(q.trim()));
    }
}
