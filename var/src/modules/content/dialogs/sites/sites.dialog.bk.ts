import { Component, Inject, Input, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Site } from "src/modules/site/models/site";
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { isThisTypeNode } from 'typescript';
import { ContentsCarouselComponent } from '../../components/contents-carousel/contents-carousel.component';

@Component({
	selector: 'content-sites-dialog',
  	templateUrl: './sites.dialog.html',
  	styleUrls: ['./sites.dialog.css']
})

export class ContentSitesDialog {

  @ViewChild(MatSort) sort: MatSort;

  public title: string;
  public message: string; 
  public sites: Site[];
 
  public contentSite: any[];
  public idcircuit: number;
  public displayedColumns: string[] = ['select', 'idsite', 'name'];
	public selection = new SelectionModel<Site>(true, []);
	public dataSource = new MatTableDataSource<Site>();
  public sortedData: Site[];
  public dataTemp: any;
  public seleccionados: boolean;
  public total: number;
  public selectSites: Site[];
  public checkeado: boolean;
  public selecAll: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
	  public dialogRef: MatDialogRef<ContentSitesDialog>) {
  }

  public ngOnInit() {
    this.sites = this.data.sites;
    this.dataTemp = [...this.data.sites];
    this.idcircuit = this.data.idcircuit;
    this.contentSite = this.data.contentsSites?this.data.contentsSites:[];
    this.checkeado = this.data.checkeado;
    this.selecAll = this.data.selecAll;
    this.renderDataTable();
  }

  public renderDataTable() {
    
    this.dataSource = new MatTableDataSource(this.sites);
    this.sortedData = this.sites.slice();
    this.dataSource.sort = this.sort;

      if(this.contentSite.length == this.sites.length) {
     
      this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
      
    


    } else {

    
  this.sites.filter(s => this.contentSite.some(cs =>  s.idsite == cs.idsite)).map(row =>  this.selection.select(row));
      this.checkeado = true;

   if(!this.checkeado){
      this.selection.clear()
    }
       
    
    }

  
  }


  public save() {   
    
       
    var response = []; 
    this.selection.selected.map(s => {
  
    if (this.sites.find(si => si.idsite == s.idsite )){
            response.push(s);
          }
    }); 
    

    var items = response.filter( cs => {
     
      if(!this.selection.selected.includes(this.sites.find(s=> s.idsite === cs.idsite)))
        return true;
    });


  


  items.map( i => {
      if (response.findIndex(site => site.idsite === i.idsite)>=0)
      if (response.findIndex(cs => cs.idsite === i.idsite && cs.idcircuit === this.idcircuit)>=0)
          response.splice(this.contentSite.findIndex(site => site.idsite === i.idsite), 1) ;   
    });

  
   this.dialogRef.close(response);
  }

  public close(){
    this.dialogRef.close(false);
  }

  public isAllSelected() {

    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  public masterToggle() {
    this.seleccionados = false;
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  public checkboxLabel(row?: Site): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idsite + 1}`;
  }	
  public filterByName(q: string) {
    if (!q) {
      this.dataSource.data = this.dataTemp;
    } else {
      this.dataSource.data = this.dataTemp.filter(
        (content) =>
          content.name.toLowerCase().includes(q.trim().toLowerCase())
      );
    }
  }
}
