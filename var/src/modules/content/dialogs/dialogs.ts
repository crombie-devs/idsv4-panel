import { ContentCategoryDialog } from 'src/modules/content/dialogs/category/category.dialog';
import { ContentPowerDialog } from 'src/modules/content/dialogs/power/power.dialog';
import { ContentSitesDialog } from 'src/modules/content/dialogs/sites/sites.dialog';
import { CreateDialog } from 'src/modules/content/dialogs/create/create.dialog';
import { ListAreasCircuitsDialog } from 'src/modules/content/dialogs/list-area-circuits/list-area-circuits.dialog';

export const CONTENT_DIALOGS = [
    ContentCategoryDialog,
    ContentPowerDialog,
    ContentSitesDialog,
    CreateDialog,
    ListAreasCircuitsDialog 
];
