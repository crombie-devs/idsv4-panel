export class ContentPower {
    idtime: number;
    idcontent: number;
    weekday: number;
    time_on: string;
    time_off: string;
    selected: boolean;
    
    constructor(obj?: any) {
        if(!obj)
            return;
            
        this.idtime = obj.idtime;
        this.idcontent = obj.idcontent;
        this.weekday = obj.weekday;
        this.time_on = obj.time_on;
        this.time_off = obj.time_off;
        this.selected = obj.selected;
    }
}