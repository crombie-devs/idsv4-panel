export interface IContentType {
    idtype: number;
    name: string;
  }

export class ContentType implements IContentType{
    idtype: number;
    name: string;

    constructor(obj?: IContentType) {
        if(!obj)
            return;
            
        this.idtype = obj.idtype;
        this.name = obj.name;        
    }
}