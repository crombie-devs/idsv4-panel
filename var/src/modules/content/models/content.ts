import { ISite, Site } from "src/modules/site/models/site";
import { IContentDate, ContentDate } from "src/models/date";
import { Customer, ICustomer } from 'src/modules/customer/models/customer';
import { ContentCategory, IContentCategory } from './category';
import { ContentType } from './content-type';
import { ITag, Tag } from 'src/modules/tag/models/tag';
import { IPlayCircuit, PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { IPlayArea, PlayArea } from 'src/modules/playarea/models/playarea';
import { IContentType } from './type';
import { Power } from 'src/modules/power/models/power';
import { Asset } from 'src/modules/asset/models/asset';
import { Product } from 'src/modules/product/models/product';
import { ContentOrder } from 'src/modules/content/models/order';
import moment from 'moment';
import { IMessages } from "src/modules/messages/models/IMessages";


export interface IContent {
    idaudiocategory?: number;
    idcontent?: number;
    name?: string;
    idcategory?: number;
    category?: IContentCategory;
    code?: string;
    frequency?: number;
    frequency_method?: number;
    customer?: ICustomer;
    site?: ISite;
    idtype?: number;
    type?: IContentType;
    is_exclusive?: boolean;
    max_emissions?: number;
    state?: boolean;
    tags?: ITag[];
    thumbnail_url?: string;
    tracker_url?: string;
    playareas?: IPlayArea[];
    playcircuits?: IPlayCircuit[];
    order?: number;
    status?: number;
    primetime?: boolean;
    down_error?: boolean;
    save_error?: boolean;
    load_error?: boolean;
    play_error?: boolean;
    powers?: Power[];
    idsite?: number;
    dates: IContentDate[];
    assets?: Asset[];
    sites?: Site[];
    products?: Product[];
    priority?: number;
    archived?: boolean;
    deleted?: number;
    orders?: ContentOrder[];
    statusString?: string;
    message?: IMessages[]
}

export class Content implements IContent {
    idaudiocategory?: number;
    idcontent?: number;
    name?: string;
    code?: string;
    frequency?: number;
    frequency_method?: number;
    idcategory?: number;
    category?: ContentCategory;
    customer?: Customer;
    site?: Site;
    idtype?: number;
    type?: ContentType;
    tags?: Tag[];
    playareas?: PlayArea[];
    playcircuits?: PlayCircuit[];
    is_exclusive?: boolean;
    max_emissions?: number;
    state?: boolean;
    thumbnail_url?: string;
    tracker_url?: string;
    order?: number;
    status?: number;
    primetime?: boolean;
    down_error?: boolean;
    save_error?: boolean;
    load_error?: boolean;
    play_error?: boolean;
    powers?: Power[];
    idsite?: number;
    dates: ContentDate[];
    assets?: Asset[];
    sites?: Site[];
    products?: Product[];
    priority?: number;
    archived?: boolean;
    deleted?: number;
    orders?: ContentOrder[];
    statusString?: string;
    message?: IMessages[]

    constructor(obj?: IContent) {
        if (!obj)
            return;

        this.idcontent = obj.idcontent;
        this.name = obj.name;
        this.code = obj.code;
        if (obj.site)
            this.site = new Site(obj.site);
        if (obj.customer)
            this.customer = new Customer(obj.customer);
        this.idcategory = obj.idcategory;
        if (obj.category) {
            this.idcategory = obj.category.idcategory;
            this.category = new ContentCategory(obj.category);
        }
        this.frequency = obj.frequency;
        this.frequency_method = obj.frequency_method;
        this.idtype = obj.idtype;
        if (obj.type) {
            this.idtype = obj.type.idtype;
            this.type = new ContentType(obj.type);
        }
        this.is_exclusive = obj.is_exclusive;
        this.max_emissions = obj.max_emissions;
        if (obj.tags) {
            this.tags = obj.tags.map(t => new Tag(t));
        }
        this.idaudiocategory = obj.idaudiocategory;
        this.thumbnail_url = obj.thumbnail_url;
        this.tracker_url = obj.tracker_url;
        if (obj.playareas)
            this.playareas = obj.playareas.map(pa => new PlayArea(pa));
        if (obj.playcircuits)
            this.playcircuits = obj.playcircuits.map(pc => new PlayCircuit(pc));

        this.order = obj.order;
        this.status = obj.status;
        this.primetime = obj.primetime;
        this.down_error = obj.down_error;
        this.save_error = obj.save_error;
        this.load_error = obj.load_error;
        this.play_error = obj.play_error;
        this.powers = (obj.powers) ? obj.powers.map(po => new Power(po)) : [];
        if (obj.idsite)
            this.idsite = obj.idsite;
        this.dates = (obj.dates) ? obj.dates.map(cd => new ContentDate(cd)) : [];
        this.assets = (obj.assets) ? obj.assets.map(a => new Asset(a)) : [];
        this.products = obj.products ? obj.products.map(p => new Product(p)) : [];
        this.sites = obj.sites ? obj.sites.map(s => new Site(s)) : [];
        this.priority = obj.priority;
        this.archived = obj.archived ? obj.archived : true;
        this.deleted = obj.deleted;

        if (obj.orders)
            this.orders = obj.orders;

        this.statusString = this.getStatusString(obj);
        this.message = obj && obj.message ? obj.message : [];
    }

    getStatusString(obj) {
        let status = '';
        const today = moment().format('YYYY-MM-DD');

        let active = false;
        obj.dates.map(date => {
            const date_on = moment(date.date_on).format('YYYY-MM-DD');
            const date_off = moment(date.date_off).format('YYYY-MM-DD');
            if (date_on <= today && date_off >= today)
                active = true;
        })
        if (obj.deleted === 1) {
            status = 'archived';
        } else if (obj.status === 1 && active) {
            status = 'active';
        } else {
            status = 'inactive'
        }
        return status;
    }

    isEquals(content: IContent): boolean {
        return this.idcontent === content.idcontent;
    }

    isError(): boolean {
        return (this.down_error || this.save_error || this.load_error || this.play_error);
    }

    canPlay(date?: Date): boolean {
        return (!this.isError() && (this.powers.length == 0 || this.powers.some(p => p.isValid(date))));
    }

}
