export interface IContentOrder {
    idcontent: number;
    idarea?: number;
    idplayer?: number;
    order: number;
}

export class ContentOrder implements IContentOrder {
    idcontent: number;
    idarea?: number;
    idplayer?: number;
    order: number = 9999 ;
    
    constructor(obj?: IContentOrder) {
        if(!obj)
            return;
            
        this.idcontent = obj.idcontent;
        if(obj.order)
            this.order = obj.order;
        if(obj.idarea)
            this.idarea = obj.idarea;
        if(obj.idplayer)
            this.idplayer = obj.idplayer;
    }

    isEquals(co: IContentOrder): boolean {
        return (this.idcontent === co.idcontent && this.idarea === co.idarea && this.idplayer === co.idplayer);
    }
}