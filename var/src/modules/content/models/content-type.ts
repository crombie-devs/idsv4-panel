export class ContentType {
    idtype: number;
    name: string;
    image_url: string;
    
    constructor(obj?: any) {
        if(!obj)
            return;
            
        this.idtype = obj.idtype;
        this.name = obj.name;
        this.image_url = obj.image_url;
    }
}