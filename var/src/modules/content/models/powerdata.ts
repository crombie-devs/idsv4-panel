export const WEEKDAYS = [
    {
        name: "WEEKDAYS.mon",
        short_name: "L",
        day_number: 1
    },
    {
        name: "WEEKDAYS.tue",
        short_name: "M",
        day_number: 2
    },
    {
        name: "WEEKDAYS.wed",
        short_name: "X",
        day_number: 3
    },
    {
        name: "WEEKDAYS.thu",
        short_name: "J",
        day_number: 4
    },
    {
        name: "WEEKDAYS.fri",
        short_name: "V",
        day_number: 5
    },
    {
        name: "WEEKDAYS.sat",
        short_name: "S",
        day_number: 6
    },
    {
        name: "WEEKDAYS.sun",
        short_name: "D",
        day_number: 7
    },
]

export const TIMES = [
    {
        day_number:1,
        times: [
            {
                name: "00:00",
                value:0,
                selected: false,
                full: true
            },
            {
                name: "01:00",
                value:1,
                selected: false,
                full: true
            },
            {
                name: "02:00",
                value:2,
                selected: false,
                full: true
            },
            {
                name: "03:00",
                value:3,
                selected: false,
                full: true
            },
            {
                name: "04:00",
                value:4,
                selected: false,
                full: true
            },
            {
                name: "05:00",
                value:5,
                selected: false,
                full: true
            },
            {
                name: "06:00",
                value:6,
                selected: false,
                full: true
            },
            {
                name: "07:00",
                value:7,
                selected: false,
                full: true
            },
            {
                name: "08:00",
                value:8,
                selected: false,
                full: true
            },
            {
                name: "09:00",
                value:9,
                selected: false,
                full: true
            },
            {
                name: "10:00",
                value:10,
                selected: false,
                full: true
            },
            {
                name: "11:00",
                value:11,
                selected: false,
                full: true
            },
            {
                name: "12:00",
                value:12,
                selected: false,
                full: true
            },
            {
                name: "13:00",
                value:13,
                selected: false,
                full: true
            },
            {
                name: "14:00",
                value:14,
                selected: false,
                full: true
            },
            {
                name: "15:00",
                value:15,
                selected: false,
                full: true
            },
            {
                name: "16:00",
                value:16,
                selected: false,
                full: true
            },
            {
                name: "17:00",
                value:17,
                selected: false,
                full: true
            },
            {
                name: "18:00",
                value:18,
                selected: false,
                full: true
            },
            {
                name: "19:00",
                value:19,
                selected: false,
                full: true
            },
            {
                name: "20:00",
                value:20,
                selected: false,
                full: true
            },
            {
                name: "21:00",
                value:21,
                selected: false,
                full: true
            },
            {
                name: "22:00",
                value:22,
                selected: false,
                full: true
            },
            {
                name: "23:00",
                value:23,
                selected: false,
                full: true
            }
        ]
    },
    {
        day_number:2,
        times: [
            {
                name: "00:00",
                value:0,
                selected: false,
                full: true
            },
            {
                name: "01:00",
                value:1,
                selected: false,
                full: true
            },
            {
                name: "02:00",
                value:2,
                selected: false,
                full: true
            },
            {
                name: "03:00",
                value:3,
                selected: false,
                full: true
            },
            {
                name: "04:00",
                value:4,
                selected: false,
                full: true
            },
            {
                name: "05:00",
                value:5,
                selected: false,
                full: true
            },
            {
                name: "06:00",
                value:6,
                selected: false,
                full: true
            },
            {
                name: "07:00",
                value:7,
                selected: false,
                full: true
            },
            {
                name: "08:00",
                value:8,
                selected: false,
                full: true
            },
            {
                name: "09:00",
                value:9,
                selected: false,
                full: true
            },
            {
                name: "10:00",
                value:10,
                selected: false,
                full: true
            },
            {
                name: "11:00",
                value:11,
                selected: false,
                full: true
            },
            {
                name: "12:00",
                value:12,
                selected: false,
                full: true
            },
            {
                name: "13:00",
                value:13,
                selected: false,
                full: true
            },
            {
                name: "14:00",
                value:14,
                selected: false,
                full: true
            },
            {
                name: "15:00",
                value:15,
                selected: false,
                full: true
            },
            {
                name: "16:00",
                value:16,
                selected: false,
                full: true
            },
            {
                name: "17:00",
                value:17,
                selected: false,
                full: true
            },
            {
                name: "18:00",
                value:18,
                selected: false,
                full: true
            },
            {
                name: "19:00",
                value:19,
                selected: false,
                full: true
            },
            {
                name: "20:00",
                value:20,
                selected: false,
                full: true
            },
            {
                name: "21:00",
                value:21,
                selected: false,
                full: true
            },
            {
                name: "22:00",
                value:22,
                selected: false,
                full: true
            },
            {
                name: "23:00",
                value:23,
                selected: false,
                full: true
            }
        ]
    },
    {
        day_number:3,
        times: [
            {
                name: "00:00",
                value:0,
                selected: false,
                full: true
            },
            {
                name: "01:00",
                value:1,
                selected: false,
                full: true
            },
            {
                name: "02:00",
                value:2,
                selected: false,
                full: true
            },
            {
                name: "03:00",
                value:3,
                selected: false,
                full: true
            },
            {
                name: "04:00",
                value:4,
                selected: false,
                full: true
            },
            {
                name: "05:00",
                value:5,
                selected: false,
                full: true
            },
            {
                name: "06:00",
                value:6,
                selected: false,
                full: true
            },
            {
                name: "07:00",
                value:7,
                selected: false,
                full: true
            },
            {
                name: "08:00",
                value:8,
                selected: false,
                full: true
            },
            {
                name: "09:00",
                value:9,
                selected: false,
                full: true
            },
            {
                name: "10:00",
                value:10,
                selected: false,
                full: true
            },
            {
                name: "11:00",
                value:11,
                selected: false,
                full: true
            },
            {
                name: "12:00",
                value:12,
                selected: false,
                full: true
            },
            {
                name: "13:00",
                value:13,
                selected: false,
                full: true
            },
            {
                name: "14:00",
                value:14,
                selected: false,
                full: true
            },
            {
                name: "15:00",
                value:15,
                selected: false,
                full: true
            },
            {
                name: "16:00",
                value:16,
                selected: false,
                full: true
            },
            {
                name: "17:00",
                value:17,
                selected: false,
                full: true
            },
            {
                name: "18:00",
                value:18,
                selected: false,
                full: true
            },
            {
                name: "19:00",
                value:19,
                selected: false,
                full: true
            },
            {
                name: "20:00",
                value:20,
                selected: false,
                full: true
            },
            {
                name: "21:00",
                value:21,
                selected: false,
                full: true
            },
            {
                name: "22:00",
                value:22,
                selected: false,
                full: true
            },
            {
                name: "23:00",
                value:23,
                selected: false,
                full: true
            }
        ]
    },
    {
        day_number:4,
        times: [
            {
                name: "00:00",
                value:0,
                selected: false,
                full: true
            },
            {
                name: "01:00",
                value:1,
                selected: false,
                full: true
            },
            {
                name: "02:00",
                value:2,
                selected: false,
                full: true
            },
            {
                name: "03:00",
                value:3,
                selected: false,
                full: true
            },
            {
                name: "04:00",
                value:4,
                selected: false,
                full: true
            },
            {
                name: "05:00",
                value:5,
                selected: false,
                full: true
            },
            {
                name: "06:00",
                value:6,
                selected: false,
                full: true
            },
            {
                name: "07:00",
                value:7,
                selected: false,
                full: true
            },
            {
                name: "08:00",
                value:8,
                selected: false,
                full: true
            },
            {
                name: "09:00",
                value:9,
                selected: false,
                full: true
            },
            {
                name: "10:00",
                value:10,
                selected: false,
                full: true
            },
            {
                name: "11:00",
                value:11,
                selected: false,
                full: true
            },
            {
                name: "12:00",
                value:12,
                selected: false,
                full: true
            },
            {
                name: "13:00",
                value:13,
                selected: false,
                full: true
            },
            {
                name: "14:00",
                value:14,
                selected: false,
                full: true
            },
            {
                name: "15:00",
                value:15,
                selected: false,
                full: true
            },
            {
                name: "16:00",
                value:16,
                selected: false,
                full: true
            },
            {
                name: "17:00",
                value:17,
                selected: false,
                full: true
            },
            {
                name: "18:00",
                value:18,
                selected: false,
                full: true
            },
            {
                name: "19:00",
                value:19,
                selected: false,
                full: true
            },
            {
                name: "20:00",
                value:20,
                selected: false,
                full: true
            },
            {
                name: "21:00",
                value:21,
                selected: false,
                full: true
            },
            {
                name: "22:00",
                value:22,
                selected: false,
                full: true
            },
            {
                name: "23:00",
                value:23,
                selected: false,
                full: true
            }
        ]
    },
    {
        day_number:5,
        times: [
            {
                name: "00:00",
                value:0,
                selected: false,
                full: true
            },
            {
                name: "01:00",
                value:1,
                selected: false,
                full: true
            },
            {
                name: "02:00",
                value:2,
                selected: false,
                full: true
            },
            {
                name: "03:00",
                value:3,
                selected: false,
                full: true
            },
            {
                name: "04:00",
                value:4,
                selected: false,
                full: true
            },
            {
                name: "05:00",
                value:5,
                selected: false,
                full: true
            },
            {
                name: "06:00",
                value:6,
                selected: false,
                full: true
            },
            {
                name: "07:00",
                value:7,
                selected: false,
                full: true
            },
            {
                name: "08:00",
                value:8,
                selected: false,
                full: true
            },
            {
                name: "09:00",
                value:9,
                selected: false,
                full: true
            },
            {
                name: "10:00",
                value:10,
                selected: false,
                full: true
            },
            {
                name: "11:00",
                value:11,
                selected: false,
                full: true
            },
            {
                name: "12:00",
                value:12,
                selected: false,
                full: true
            },
            {
                name: "13:00",
                value:13,
                selected: false,
                full: true
            },
            {
                name: "14:00",
                value:14,
                selected: false,
                full: true
            },
            {
                name: "15:00",
                value:15,
                selected: false,
                full: true
            },
            {
                name: "16:00",
                value:16,
                selected: false,
                full: true
            },
            {
                name: "17:00",
                value:17,
                selected: false,
                full: true
            },
            {
                name: "18:00",
                value:18,
                selected: false,
                full: true
            },
            {
                name: "19:00",
                value:19,
                selected: false,
                full: true
            },
            {
                name: "20:00",
                value:20,
                selected: false,
                full: true
            },
            {
                name: "21:00",
                value:21,
                selected: false,
                full: true
            },
            {
                name: "22:00",
                value:22,
                selected: false,
                full: true
            },
            {
                name: "23:00",
                value:23,
                selected: false,
                full: true
            }
        ]
    },
    {
        day_number:6,
        times: [
            {
                name: "00:00",
                value:0,
                selected: false,
                full: true
            },
            {
                name: "01:00",
                value:1,
                selected: false,
                full: true
            },
            {
                name: "02:00",
                value:2,
                selected: false,
                full: true
            },
            {
                name: "03:00",
                value:3,
                selected: false,
                full: true
            },
            {
                name: "04:00",
                value:4,
                selected: false,
                full: true
            },
            {
                name: "05:00",
                value:5,
                selected: false,
                full: true
            },
            {
                name: "06:00",
                value:6,
                selected: false,
                full: true
            },
            {
                name: "07:00",
                value:7,
                selected: false,
                full: true
            },
            {
                name: "08:00",
                value:8,
                selected: false,
                full: true
            },
            {
                name: "09:00",
                value:9,
                selected: false,
                full: true
            },
            {
                name: "10:00",
                value:10,
                selected: false,
                full: true
            },
            {
                name: "11:00",
                value:11,
                selected: false,
                full: true
            },
            {
                name: "12:00",
                value:12,
                selected: false,
                full: true
            },
            {
                name: "13:00",
                value:13,
                selected: false,
                full: true
            },
            {
                name: "14:00",
                value:14,
                selected: false,
                full: true
            },
            {
                name: "15:00",
                value:15,
                selected: false,
                full: true
            },
            {
                name: "16:00",
                value:16,
                selected: false,
                full: true
            },
            {
                name: "17:00",
                value:17,
                selected: false,
                full: true
            },
            {
                name: "18:00",
                value:18,
                selected: false,
                full: true
            },
            {
                name: "19:00",
                value:19,
                selected: false,
                full: true
            },
            {
                name: "20:00",
                value:20,
                selected: false,
                full: true
            },
            {
                name: "21:00",
                value:21,
                selected: false,
                full: true
            },
            {
                name: "22:00",
                value:22,
                selected: false,
                full: true
            },
            {
                name: "23:00",
                value:23,
                selected: false,
                full: true
            }
        ]
    },
    {
        day_number:7,
        times: [
            {
                name: "00:00",
                value:0,
                selected: false,
                full: true
            },
            {
                name: "01:00",
                value:1,
                selected: false,
                full: true
            },
            {
                name: "02:00",
                value:2,
                selected: false,
                full: true
            },
            {
                name: "03:00",
                value:3,
                selected: false,
                full: true
            },
            {
                name: "04:00",
                value:4,
                selected: false,
                full: true
            },
            {
                name: "05:00",
                value:5,
                selected: false,
                full: true
            },
            {
                name: "06:00",
                value:6,
                selected: false,
                full: true
            },
            {
                name: "07:00",
                value:7,
                selected: false,
                full: true
            },
            {
                name: "08:00",
                value:8,
                selected: false,
                full: true
            },
            {
                name: "09:00",
                value:9,
                selected: false,
                full: true
            },
            {
                name: "10:00",
                value:10,
                selected: false,
                full: true
            },
            {
                name: "11:00",
                value:11,
                selected: false,
                full: true
            },
            {
                name: "12:00",
                value:12,
                selected: false,
                full: true
            },
            {
                name: "13:00",
                value:13,
                selected: false,
                full: true
            },
            {
                name: "14:00",
                value:14,
                selected: false,
                full: true
            },
            {
                name: "15:00",
                value:15,
                selected: false,
                full: true
            },
            {
                name: "16:00",
                value:16,
                selected: false,
                full: true
            },
            {
                name: "17:00",
                value:17,
                selected: false,
                full: true
            },
            {
                name: "18:00",
                value:18,
                selected: false,
                full: true
            },
            {
                name: "19:00",
                value:19,
                selected: false,
                full: true
            },
            {
                name: "20:00",
                value:20,
                selected: false,
                full: true
            },
            {
                name: "21:00",
                value:21,
                selected: false,
                full: true
            },
            {
                name: "22:00",
                value:22,
                selected: false,
                full: true
            },
            {
                name: "23:00",
                value:23,
                selected: false,
                full: true
            }
        ]
    }
]