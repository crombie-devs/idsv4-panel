
export interface IContentCategory {
    idcategory: number;
    name: string;
    image_url: string;
    duration: number;
    idcustomer: number;
    user_local: boolean;
    random: number;
}

export class ContentCategory implements IContentCategory {
    idcategory: number;
    name: string;
    image_url: string;
    duration: number;
    idcustomer: number;
    user_local: boolean;
    random: number;

    constructor(obj?: IContentCategory) {
        if (!obj)
            return;

        this.idcategory = obj.idcategory;
        this.name = obj.name;
        this.image_url = obj.image_url;
        this.idcustomer = obj.idcustomer;
        this.duration = obj.duration ? obj.duration : 0;
        this.user_local = obj.user_local;
        this.random = obj.random;
    }

    isEquals(category: IContentCategory): boolean {
        return this.idcategory === category.idcategory;
    }
}