import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentListPage } from './pages/list/list.page';
import { ContentFormPage } from './pages/form/form.page';
import { ContentCategoryListPage } from './pages/category/list/category-list.page';
import { PlaylogicsPage } from 'src/modules/playlogic/pages/playlogics.page';

const routes: Routes = [
    { path: 'content/list', component: ContentListPage, data: { breadcrumb: 'Contenidos' } },
    { path: 'content/create', component: ContentFormPage, data: { breadcrumb: 'Crear contenido' } },
    { path: 'content/edit/:idcontent', component: ContentFormPage, data: { breadcrumb: 'Editar contenido' } },
    { path: 'content/edit/:idcontent/:playlist', component: ContentFormPage, data: { breadcrumb: 'Editar contenido' } },
    { path: 'contentcategories', component: ContentCategoryListPage, data: { breadcrumb: 'Categorías' }, },
    { path: 'playlists', component: PlaylogicsPage, data: { breadcrumb: 'Editar playlists' }, },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ContentRoutingModule { }
