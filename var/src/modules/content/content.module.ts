import { DebugStore } from 'src/stores/debug.store';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/shared/shared.module';
import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CONTENT_COMPONENTS } from './components/components';
import { CONTENT_DIALOGS } from './dialogs/dialogs';
import { ContentRoutingModule } from './content-routing.module';
import { AssetModule } from 'src/modules/asset/asset.module';
import { TagModule } from 'src/modules/tag/tag.module';
import { ProductModule } from 'src/modules/product/product.module';
import { PrimetimeModule } from 'src/modules/primetime/primetime.module';
import { PlaycircuitModule } from 'src/modules/playcircuit/playcircuit.module';
import { PlayareaModule } from 'src/modules/playarea/playarea.module';
import { PlayerModule } from 'src/modules/player/player.module';
import { PlaylogicModule } from 'src/modules/playlogic/playlogic.module';
import { SiteModule } from 'src/modules/site/site.module';
import { ContentStore } from './stores/content.store';
import { ContentCategoryStore } from './stores/category.store';
import { ContentOrderStore } from './stores/order.store';
import { ContentTypeStore } from './stores/type.store';
import { FormFieldModule } from 'src/modules/form-field/form-field.module';
import { TemplateModule } from 'src/modules/template/template.module';
import { PowerModule } from 'src/modules/power/power.module';
import { ContentListPage } from './pages/list/list.page';
import { ContentFormPage } from './pages/form/form.page';
import { ContentCategoryListPage } from './pages/category/list/category-list.page';
import { PlaylogicsPage } from 'src/modules/playlogic/pages/playlogics.page';
import { DirectivesModule } from 'src/directive/directives.module';
import { CONFIG } from 'src/app/app.config';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import moment from 'moment';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { SiteDataComponent } from './components/site-data/site-data.component';
import { MsgTextContainersComponent } from './components/msg-text-containers/msg-text-containers.component';
import { CustomerModule } from '../customer/customer.module';
import { FormatTemplateDialog } from './dialogs/form-template/format-template.dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { CalculatePeopleComponent } from './components/calculate-people/calculate-people.component';
import { PeopleModule } from '../people/people.module';
import { FormatModule } from '../format/format.module';
import { ListMessageComponent } from './components/list-messages/list-message.component';
import { MessageStatsComponent } from './components/message-stats/message-stats.component';



export function HttpLoaderFactory(http: HttpClient) {
    let url = CONFIG.lang_url;
    return new TranslateHttpLoader(http, `${url}/i18n/`, `.json?timestamp=${moment().valueOf()}`);
}
@NgModule({
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    declarations: [CONTENT_COMPONENTS,
        CONTENT_DIALOGS,
        ContentListPage,
        ContentFormPage,
        ContentCategoryListPage,
        PlaylogicsPage,
        SiteDataComponent,
        MsgTextContainersComponent,
        CalculatePeopleComponent,
        FormatTemplateDialog,
        ListMessageComponent,
        MessageStatsComponent],
    imports: [
        CommonModule,
        FlexModule,
        NgxMatFileInputModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient],
            },
        }),
        SharedModule,
        AssetModule,
        TagModule,
        ProductModule,
        PrimetimeModule,
        PlayareaModule,
        PlaycircuitModule,
        FormFieldModule,
        PlayerModule,
        PlaylogicModule,
        PowerModule,
        ContentRoutingModule,
        IvyCarouselModule,
        TemplateModule,
        SiteModule,
        DirectivesModule,
        TagModule,
        CustomerModule,
        MatExpansionModule,
        PeopleModule,
        FormatModule,

    ],
    entryComponents: [CONTENT_DIALOGS],
    providers: [ContentStore, ContentCategoryStore, ContentOrderStore, ContentTypeStore, DebugStore],
    exports: [CONTENT_COMPONENTS, SiteDataComponent, MsgTextContainersComponent, FormatTemplateDialog, CalculatePeopleComponent, ListMessageComponent, MessageStatsComponent]
})
export class ContentModule {
    constructor(private translate: TranslateService) {
        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('es');
        if (!sessionStorage.getItem('lang')) {
            sessionStorage.setItem('lang', 'es');
            translate.use('es');
        } else {
            translate.use(sessionStorage.getItem('lang'));
        }
    }
}