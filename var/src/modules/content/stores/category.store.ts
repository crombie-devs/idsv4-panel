import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { ContentCategory, IContentCategory } from 'src/modules/content/models/category';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'content-category.store.ids';

@Injectable()
export class ContentCategoryStore {

    public onChange: BehaviorSubject<ContentCategory[]> = new BehaviorSubject([]);
    private data: ContentCategory[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const contents: IContentCategory[] = JSON.parse(store); 
                this.set(contents);
            } else 
                this.load();            
        }else
            this.load();
    }

    public get(): ContentCategory[] {
        return this.data;
    }

    public set(contents: IContentCategory[]) {
        this.save(contents);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.contentCategoriesToSelect().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }
        });
    }

    private save(contents: IContentCategory[]) {
        this.data = contents.map(c => new ContentCategory(c));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(contents));        
    }
}