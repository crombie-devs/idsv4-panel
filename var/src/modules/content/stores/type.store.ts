import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { ContentType, IContentType } from 'src/modules/content/models/type';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'content-types.store.ids';

@Injectable()
export class ContentTypeStore {

    public onChange: BehaviorSubject<ContentType[]> = new BehaviorSubject([]);
    private data: ContentType[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const contents: IContentType[] = JSON.parse(store); 
                this.set(contents);
            } else 
                this.load();
        }else
            this.load();
    }

    public get(): ContentType[] {
        return this.data;
    }

    public set(contents: IContentType[]) {
        this.save(contents);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.contentTypesToSelect().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }
        });
    }

    private save(contents: IContentType[]) {
        this.data = contents.map(t => new ContentType(t));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(contents));        
    }
}