import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { IContent, Content } from 'src/modules/content/models/content';
import { IPlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { IPlayArea } from 'src/modules/playarea/models/playarea';
import { IPlayer } from 'src/modules/player/models/player';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'contents.store.ids';

@Injectable()
export class ContentStore {

    public onChange: BehaviorSubject<Content[]> = new BehaviorSubject([]);
    private data: Content[] = [];

    constructor(private ws: WSService) {
        this.load();
    }

    public get(): Content[] {
        return this.data;
    }

    public set(items: IContent[]) {
        this.save(items);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    public filter(circuit: IPlayCircuit, area: IPlayArea, player: IPlayer, validContents?:Content[]): Content[] {
        let contents: Content[] = this.data;
        if(validContents)            //validContents.filter(c => c.canPlay());
            contents = contents.filter(c => validContents.some(vc => vc.idcontent === c.idcontent ));        
        if(circuit)
            contents = contents.filter(c => c.playcircuits.some(pc => pc.isEquals(circuit)));
        if(player)
            contents = contents.filter(c => c.playareas.some(pa => pa.isEquals(player.area)));
        else if(area)
            contents = contents.filter(c => c.playareas.some(pa => pa.isEquals(area)));

        return contents;
    }

    private load() {
        this.ws.contentsToSelect().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }
        });
    }

    private save(items: IContent[]) {
        this.data = items.map(a => new Content(a));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(items));
    }
}