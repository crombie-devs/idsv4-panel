import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { IContentOrder, ContentOrder } from 'src/modules/content/models/order';
import { IPlayArea, PlayArea } from 'src/modules/playarea/models/playarea';
import { IPlayer } from 'src/modules/player/models/player';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'contentsorders.store.ids';

@Injectable()
export class ContentOrderStore {

    public onChange: BehaviorSubject<ContentOrder[]> = new BehaviorSubject([]);
    private data: ContentOrder[] = [];

    constructor(private ws: WSService) {
        this.load();
    }
    public get(): ContentOrder[] {
        return this.data;
    }
    public set(items: IContentOrder[]) {
        this.save(items);
        this.onChange.next(this.data);
    }
    public reload() {
        this.load();
    }
    public filter(area: IPlayArea, player: IPlayer): ContentOrder[] {
        let orders: ContentOrder[] = this.data;
        if(player)
            orders = this.filterByPlayer(player);
        else if(area)
            orders = this.filterByArea(area);

        return orders;
    }
    private filterByArea(area: IPlayArea): ContentOrder[] {
        return this.data.filter(d => d.idarea === area.idarea);
    }
    private filterByPlayer(player: IPlayer): ContentOrder[] {
        let order = this.data.filter(d => d.idplayer === player.idplayer);
        if(order.length <= 0)
            order = this.data.filter(d => d.idarea === player.area.idarea);
        return order;
    }
    private load() {
        this.ws.contentsOrders().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }
        });
    }
    private save(items: IContentOrder[]) {
        this.data = items.map(a => new ContentOrder(a));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(items));
    }
}