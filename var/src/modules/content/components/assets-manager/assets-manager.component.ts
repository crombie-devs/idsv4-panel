import { Component, Input, SimpleChanges, Output, EventEmitter, ChangeDetectorRef, ViewChild, AfterViewInit, OnChanges, OnInit } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { AssetDialog } from 'src/modules/asset/dialogs/asset/asset.dialog';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Tag } from 'src/modules/tag/models/tag';

import { Asset, ASSET_TYPES } from 'src/modules/asset/models/asset';
import { IResult } from 'src/models/iresult';
import { IPlayCircuit, PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { PlayArea } from 'src/modules/playarea/models/playarea';
import { MatDialog } from '@angular/material/dialog';
import { PreviewDialog } from 'src/modules/asset/dialogs/preview/preview.dialog';
import { Format } from 'src/modules/format/models/format';
import { Lang } from 'src/modules/lang/models/lang';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { AssetFromDrop } from 'src/modules/asset/models/asset-from-drop';
import { FileHandle } from 'src/directive/drag-drop.directive';
import { TemplateDialog } from 'src/modules/template/dialogs/template/template.dialog';
import { ContentStore } from 'src/modules/content/stores/content.store';
import { TranslateService } from '@ngx-translate/core';
import { Content, IContent } from 'src/modules/content/models/content';
import { Landingpage } from 'src/modules/landing-page/models/landingpage';
import { LandingpageDialog } from 'src/modules/landing-page/dialogs/landingpage/landingpage.dialog';
import { isThisTypeNode } from 'typescript';

interface IGroup {
    lang?: Lang,
    format?: Format,
    assets: Asset[],
    name?: string
}

@Component({
    providers: [FormatStore, LangStore, ContentStore],
    selector: 'content-assets-manager',
    templateUrl: './assets-manager.component.html',
    styleUrls: ['./assets-manager.component.scss']
})


export class AssetsManagerComponent implements OnInit, OnChanges {
    @Input() isEditing: boolean;
    @Input() selectedAreas: PlayArea[] = [];
    @Input() savedContents: boolean = false;
    @Input() playcircuits: IPlayCircuit[];
    @Input() selectedTags: Tag[] = [];
    @Input() contents: Content[] = [];
    @Input() contentIndex: number = 0;
    @Input() maxSize: number;
    @Output() onSelectedAssetsChange: EventEmitter<Asset[]> = new EventEmitter();
    @Output() onCreateNewContent: EventEmitter<IContent[]> = new EventEmitter();
    @Output() onSelectContent: EventEmitter<number> = new EventEmitter();

    public selectedAssets: Asset[] = [];
    public load: boolean = true;
    public files: any = [];
    public uploadedFilesFormats: any = [];
    public loadFiles = false;
    public permission: boolean = false;
    public onEdit: boolean = false;
    public formatsLangs = [];
    public storageItems: any;
    public assetHist: any[] = [];
    public groups: IGroup[] = [];
    public assets: Asset[] = [];
    private allAsset: Asset[] = [];
    public filteredAssets: Asset[] = [];
    public formats: Format[] = [];
    public langs: Lang[] = [];
    public filterName: string = '';
    public loadingAssets: boolean = false;
    public firstRequest: boolean = true;
    public asset: AssetFromDrop = new AssetFromDrop();
    public slidesToShow: number = 4;
    public sliderWith: number = 620;
    public langsFromCircuits: number[] = [];
    public selectedCircuits: IPlayCircuit[];
    public lastFormat: number;
    public lastLang: number;
    public addNewCol: boolean = true;
    public position: number = 0;
    public flexIzda: number = 90;
    public flexDcha: number = 10;
    public displayed: boolean = false;
    public landingpages: Landingpage[] = [];
    public idcustomer: number = null;
    public reload: boolean = false;

    constructor(
        private ws: WSService,
        private ustore: UserStore,
        private _snackBar: MatSnackBar,
        private domSanitizer: DomSanitizer,
        private translate: TranslateService,
        private matDialog: MatDialog,
        private fstore: FormatStore,
        private lstore: LangStore
    ) { }

    public ngOnInit() {
        this.loadData();
        this.load = false;
    }

    public setPlayCircuits(playcircuits: PlayCircuit[]): void {
        if (playcircuits) {
            this.selectedCircuits = playcircuits.map(pc => new PlayCircuit(pc));
            this.selectedCircuits.map(c => {
                c.langs.map(l => {
                    if (!this.langsFromCircuits.some(lc => lc === l.idlang))
                        this.langsFromCircuits.push(l.idlang);
                })
            })
        }
    }


    public setContents(contents: any): void {
        if (contents) {
            this.contents = contents;
            this.run();
        }
    }


    public setSelectedAreas(selectedAreas: any): void {
        if (selectedAreas) {
            this.run();
            this.getLangsToSelect();
        }
    }


    public loadData() {
        const that = this;
        let count: number = 3;
        this.fstore.onChange.subscribe(formats => {
            this.formats = formats;
            decrement();
        });
        this.lstore.onChange.subscribe(langs => {
            this.langs = langs;
            decrement();
        });
        this.ws.assets().subscribe((result: IResult) => {
            if (result.success) {
                this.permission = true;
                this.allAsset = result.data.map(a => new Asset(a));
                this.assets = this.filterAssets(this.allAsset);
            }
            decrement();
        });


        function decrement() {
            count--;
            that.loadingAssets = (count > 0);
            if (!that.loadingAssets) that.proccessData(that.filterName);
        }

        this.ws.landingpages().subscribe((result: IResult) => {
            if (result.success)
                this.landingpages = result.data.map(lp => new Landingpage(lp));
        });
    }

    public run() {
        this.assets = this.filterAssets(this.allAsset);                        
        this.proccessData(this.filterName);
    }

    public proccessData(filter?: string) {
        this.groups = [];
        const is_widget = 0;
        let widgets: Asset[] = this.assets.filter(asset => asset.is_widget === 1);
        this.langs.forEach((lang: Lang) => {
            if (this.langsFromCircuits.some(l => l == lang.idlang)) {
                this.formats.forEach((format: Format) => {
                    let assets = this.assets.filter(a => a.check(lang, format, is_widget));
                    assets = assets.sort(this.compare);
                    if (filter && filter.length > 0)
                        assets = assets.filter(a => a.name.toLocaleLowerCase().includes(filter));
                    if (assets && assets.length > 0) {
                        const group: IGroup = { 'lang': lang, 'format': format, 'assets': assets };
                        this.groups.push(group);
                    }
                });
            } else if (this.ustore && this.ustore.isUser()) {
                this.formats.forEach((format: Format) => {
                    let assets = this.assets.filter(a => a.check(lang, format, is_widget));
                    if (filter && filter.length > 0)
                        assets = assets.filter(a => a.name.toLocaleLowerCase().includes(filter));
                    if (assets && assets.length > 0) {
                        const group: IGroup = { 'lang': lang, 'format': format, 'assets': assets };
                        this.groups.push(group);
                    }
                });
            }
        });
        if (widgets.length > 0)
            this.groups.push({ name: 'Widgets', assets: widgets }) 
            this.groups = this.filterAssetsGroup(this.groups);
    }
    private compare(a,b){
        if ( a.idasset < b.idasset ){
            return 1;
          }
          if ( a.idasset > b.idasset ){
            return -1;
          }
          return 0;
    }
    ngOnChanges(changes: SimpleChanges) {
        if (changes.selectedAreas) {
            this.run();
            this.getLangsToSelect();
        }

        if (changes.playcircuits && changes.playcircuits.currentValue) {
            this.selectedCircuits = changes.playcircuits.currentValue.map(pc => new PlayCircuit(pc));
            this.selectedCircuits.map(c => {
                c.langs.map(l => {
                    if (!this.langsFromCircuits.some(lc => lc === l.idlang))
                        this.langsFromCircuits.push(l.idlang);
                })
            })
        }

        if (changes.contents && changes.contents.currentValue) {
            this.run();
        }
    }


    private filterAssets(assets: Asset[]) {
        let filteredAssets = []
        assets.forEach(function (asset, i) {
            let assetPushed = false
            if (asset.is_widget) {
                filteredAssets.push(asset);
            } else {
                for (let area of this.selectedAreas) {
                    if (!assetPushed) {
                        if (area.format && area.format.idformat == asset.format.idformat) {
                            filteredAssets.push(asset);
                            assetPushed = true;
                        }
                    }
                }
            }
        }, this);
        return filteredAssets
    }

    private filterAssetsGroup(group: IGroup[]) {
        group.sort(function (a, b) {
            if (a.format && b.format) {
                return a.format.idformat - b.format.idformat
            }
        })
        return group
    }

    private getLangsToSelect() {
        const langsFromCircuits = this.langsFromCircuits;
        let that = this;
        this.ws.langsToSelect().subscribe((result: IResult) => {
            if (result.success) {
                this.formatsLangs = []
                let tempFormatsLangs = [];
                this.selectedAreas.forEach(function (value) {
                    tempFormatsLangs[value.format.idformat] = [];
                    tempFormatsLangs[value.format.idformat]["format"] = value.format.name;
                    tempFormatsLangs[value.format.idformat]["data"] = [];
                    this.langs.forEach(function (lang) {
                        if (langsFromCircuits.some(l => l === lang.idlang))
                            tempFormatsLangs[value.format.idformat]["data"].push({
                                idformat: value.format.idformat,
                                format: value.format.name,
                                img: value.format.image_url,
                                langid: lang.idlang,
                                lang: lang.name
                            });
                        else if (that.ustore && that.ustore.isUser())
                            tempFormatsLangs[value.format.idformat]["data"].push({
                                idformat: value.format.idformat,
                                format: value.format.name,
                                img: value.format.image_url,
                                langid: lang.idlang,
                                lang: lang.name
                            });
                    })
                }, this)
                tempFormatsLangs.forEach(function (value) {
                    this.formatsLangs.push(value);
                }, this)
            } else {
                this.load = false;
                return null;
            }
        }, error => {
            this.load = false;
            return null;
        })
    }

    public uploadFile(event, idFormat, format) {
        const reader = new FileReader();
        const _URL = window.URL || window.webkitURL;
        for (let index = 0; index < event.length; index++) {
            const element = event[index];
            let type = element.type.split("/")[0];
            let ext = element.type.split("/")[1];
            if (type == "image") {
                if (ext == "png" || ext == "jpg" || ext == "jpeg") {
                    let img = new Image();
                    var width = 0;
                    var height = 0;
                    var thisObject = this;
                    let objectUrl = _URL.createObjectURL(element);
                    img.src = objectUrl;
                    img.onload = function () {
                        width = img.width
                        height = img.height
                        thisObject.files.push({
                            idFormat: idFormat,
                            format: format,
                            type: "Imagen",
                            duration: "",
                            audio: false,
                            width: width,
                            height: height,
                            formatWidth: format.width,
                            formatHeight: format.height,
                            url: objectUrl.replace("blob:", "")
                        })
                        thisObject.loadFiles = true;
                    }
                } else {
                    window.alert("El formato de imagen es incorrecto.");
                }
            } else if (type == "video") {
                if (ext == "mp4" || ext == "x-msvideo" || ext == "mpeg" || ext == "ogg" || ext == "webm") {
                    let video = document.createElement("video");
                    var width = 0;
                    var height = 0;
                    var thisObject = this;
                    let objectUrl = _URL.createObjectURL(element);
                    video.src = objectUrl;
                    video.addEventListener("loadedmetadata", function () {
                        width = video.videoWidth
                        height = video.videoHeight
                        let duration = String((video.duration / 60)).split(".")[0] + ":00";
                        thisObject.files.push({
                            idFormat: idFormat,
                            format: format,
                            type: "Vídeo",
                            duration: duration,
                            audio: true,
                            width: width,
                            height: height,
                            formatWidth: format.width,
                            formatHeight: format.height,
                            url: objectUrl.replace("blob:", "")
                        })
                        thisObject.loadFiles = true;
                    });
                } else {
                    window.alert("El formato de vídeo es incorrecto.");
                }
            } else {
                window.alert("El formato es incorrecto.");
            }
        }
    }

    public deleteAttachment(index) {
        this.files.splice(index, 1)
    }

    public sanitizeUrl(url) {
        return this.domSanitizer.bypassSecurityTrustUrl("blob:" + url);
    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }

    public onClickAsset(asset: Asset, position = false) {
        const assets = this.contents[this.contentIndex].assets.map(a => new Asset(a));
        let exists = false;
        assets.map((a, i) => {
            if (a.format.idformat === asset.format.idformat && a.lang.idlang === asset.lang.idlang) {
                this.contents[this.contentIndex].assets[i] = new Asset(asset);
                exists = true;
            }
        });
        if (!exists)
            this.contents[this.contentIndex].assets.push(new Asset(asset));
        this.onSelectedAssetsChange.emit(this.contents[this.contentIndex].assets);
    }

    Items(asset) {
        let that = this;
        this.formatsLangs.forEach(function (formatLang) {
            formatLang.data.forEach(function (data, i) {
                if ((asset.format && data.idformat !== asset.format.idformat)
                    || (asset.lang && data.langid !== asset.lang.idlang)) {
                    if ((!asset.is_widget || asset.is_widget === 'false') && that.addNewCol)
                        that.storageItems[data.idformat][data.langid].unshift({ asset: null });
                }
            });
        });
    }

    preview(asset: Asset) {
        this.openDialogPreview(asset);
    }

    public openDialogPreview(asset: Asset) {
        const dialogRef = this.matDialog.open(PreviewDialog, { data: asset });
        dialogRef.afterClosed().subscribe((result: Asset) => { });
    }

    public onChangeAsset(assets: Asset[]) {
        this.run();
    }

    public onKeyup(event: KeyboardEvent) {
        this.proccessData(this.filterName);
    }

    openNewFormFromDrop(asset) {
        this.position = asset.position;
        const dialogRef = this.matDialog.open(AssetDialog, { data: asset });
        dialogRef.afterClosed().subscribe((result: Asset) => {
            if(result){
                this.allAsset = [...this.allAsset, result];
                this.run();
                this.onClickAsset(result, true);
            }
        });
    }

    OnFileDropped(data: any) {
        const size = this.toMb(data.file.file.size);
        if (size > this.maxSize) {
            this.openSnackBar(`${this.translate.instant('El archivo no puede exceder de ')} ${this.maxSize}mb`);
            return;
        }
        let is_widget = false;
        if (!data.lang && !data.format)
            is_widget = true
        const asset = new AssetFromDrop({
            name: data.file.file.name,
            duration: 0,
            format: data.format,
            size: data.file.file.size,
            lang: data.lang,
            asset: data.file.file,
            is_widget: is_widget,
            position: data.position,
            delay:data.delay
        });
        this.openNewFormFromDrop(asset)
    }
    toMb(size) {
        return size / (1024 * 1024);
    }

    openNewForm(type = null) {
        let dialogRef;

        let formats = this.formatsLangs.map(format => format.data[0].idformat);
        if (type) {
            dialogRef = this.matDialog.open(AssetDialog, { data: { idtype: type, langs: this.langsFromCircuits, formats: formats } });
            dialogRef.afterClosed().subscribe((result: Asset[]) => {
                this.run();
                result.map(asset => this.onClickAsset(asset))
            });
        } else {
            dialogRef = this.matDialog.open(AssetDialog, { data: { langs: this.langsFromCircuits, formats: formats } });
            dialogRef.afterClosed().subscribe((result: Asset[]) => {
                this.run();
                result.map(asset => this.onClickAsset(asset))
            });
        }


    }

    openNewTemplate() {
        const dialogRef = this.matDialog.open(TemplateDialog);
        dialogRef.afterClosed().subscribe((result: any) => {
            if (result) {
                let asset: Asset = new Asset();
                asset.asset_url = result.url;
                asset.idtype = ASSET_TYPES.url;
                const dialogRef = this.matDialog.open(AssetDialog, { data: asset });
                dialogRef.afterClosed().subscribe((result: Asset) => {
                    this.run();
                    this.onClickAsset(result);
                });
            }
        });
    }

    filesDropped(file: FileHandle[], data: any): void {
        let lang: Lang = {
            idlang: data.langid,
            name: data.lang,
            isEquals: data.langid
        }
        let format: Format = {
            idformat: data.idformat,
            name: data.foramt,
            isEquals: data.iformat
        }
        const dataToSend = {
            file: file[0],
            lang: lang,
            format: format
        }
        this.OnFileDropped(dataToSend);
    }

    public createNewContent() {
        this.onCreateNewContent.emit(this.contents);
    }

    public selectContent(contentIndex) {
        this.onSelectContent.emit(contentIndex);
    }

    public openDialogLandingpage(landingpage: Landingpage) {
        let formats = this.formatsLangs.map(format => format.data[0].idformat);
        const dialogRef = this.matDialog.open(
            LandingpageDialog,
            {
                data: { "landingpage": landingpage, asset: null, langs: this.langsFromCircuits, formats: formats },
                width: 'auto',
                height: 'auto',
                minHeight: '600px',
                minWidth: '800px'
            }
        );
        dialogRef.afterClosed().subscribe((result: Asset[]) => {
            this.run();
            result.map(asset => this.onClickAsset(asset))
        });
    }

    public toggleAssetsList() {
        
        let max = 40;
        if (this.contents.length < 3) {
            max = 50;
        }
        if (!this.displayed) {
           
            this.flexDcha += max;
            this.flexIzda -= max;
        } else {
            this.flexDcha -= max;
            this.flexIzda += max;
        }
        this.displayed = !this.displayed;

       

    }
}