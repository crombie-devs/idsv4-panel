import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'site-data',
    templateUrl: './site-data.component.html',
    styleUrls: ['./site-data.component.scss']
})
export class SiteDataComponent implements OnInit {

    @Input() item;


    constructor() { }

    ngOnInit(): void {
    }

}
