import { SelectionModel } from '@angular/cdk/collections';
import { Component, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

SelectionModel



@Component({
    selector: 'list-message',
    templateUrl: './list-message.component.html',
    styleUrls: ['./list-message.component.scss']
})

export class ListMessageComponent {


    @Input() messages: any[] = [];
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = ['id', 'format', 'datetime', 'body', 'lang'];
    public selection = new SelectionModel<any>(true, []);

    constructor() { }

    public renderDataTable(data: any[]) {
        data = data.filter(d => d.status == 1);
        this.dataSource = new MatTableDataSource(data);
    }
}
