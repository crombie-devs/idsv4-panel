import { Component, Input, Renderer2 } from "@angular/core";
import { Content } from 'src/modules/content/models/content';


@Component({
    selector: 'contents',
    templateUrl: './contents.component.html',
    styleUrls: ['./contents.component.scss']
  })
export class ContentsComponent {
    
    @Input() contents: Content[];

    constructor(private renderer: Renderer2){

        if (window.addEventListener) { 
            window.addEventListener("message", (e) => {  
                if(e.data.text === "played"){
                    this.renderer.setStyle(document.getElementById("content"+e.data.data), 'background-color', '#FF6666');
                } 
                if(e.data.text === "end"){
                    this.renderer.setStyle(document.getElementById("content"+e.data.data), 'background-color', '#303030');
                }      
            }, false); 
        }
    }
    
}

