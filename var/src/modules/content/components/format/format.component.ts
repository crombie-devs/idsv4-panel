import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'content-format',
  templateUrl: './format.component.html',
  styleUrls: ['./format.component.scss']
})
export class ContentFormatComponent implements OnInit {
  
  @Input() item;
  @Input() isEmpty;

  constructor() { }

  ngOnInit(): void {
  }

}
