import { userMock } from 'src/modules/user/models/user.mocks';
import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { ContentSelectCategoryComponent } from './select-category.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';

import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

import { MaterialModule } from 'src/app/material.module';
import { OverlayModule } from '@angular/cdk/overlay';

import { WSService } from 'src/services/ws.service';
import { HttpClient } from "@angular/common/http";
import { HttpLoaderFactory } from "src/shared/shared.module";
import { DebugStore } from 'src/stores/debug.store';
import { UserModule} from 'src/modules/user/user.module';
import moment from 'moment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogModule } from 'src/modules/dialog/dialog.module';
import { MatCheckbox } from "@angular/material/checkbox";
import { of } from 'rxjs';
import { ContentCategory, IContentCategory } from 'src/modules/content/models/category';

const WSServiceMock = {
    contentCategoriesToSelect: () => of(ContentCategory),
}

describe('Componente: ContentSelectCategoryComponent', () => {
    let component: ContentSelectCategoryComponent;
    let fixture: ComponentFixture<ContentSelectCategoryComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports:[ 
                UserModule,
                MaterialModule,
                OverlayModule,
                RouterTestingModule,
                BrowserAnimationsModule,
                DialogModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                }),
                HttpClientTestingModule],
            declarations: [
                ContentSelectCategoryComponent
            ],
            providers:[
                DebugStore,
                {
                    provide: WSService,
                    useValue: WSServiceMock
                }
            ],
            schemas:[CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        }).compileComponents();
    });

    beforeEach(async(() => {
        fixture = TestBed.createComponent(ContentSelectCategoryComponent);
        component = fixture.componentInstance;
        fixture.whenStable().then(() => {
            fixture.detectChanges();
        });
    }));

    it("Creando componente", () => {
        const spy = spyOn(component as any, 'getContentCategoriesToSelect').and.callThrough();
        component.ngOnInit();
        expect(spy).toHaveBeenCalled();
        expect(component).toBeTruthy();
    });

    it('Función: onChangeContentCategory', () => {
        const spy = spyOn(component.OnContentCategoryChange, 'emit');
        component.onChangeContentCategory();
        expect(spy).toHaveBeenCalled();
    });

    it("Función: userFilterCategories", () => {
        const categories: IContentCategory[] = [
            {
                idcategory: 1,
                name: 'categoria 1',
                image_url: 'string',
                duration: 10,
                idcustomer: 21,
                user_local: true
            },
            {
                idcategory: 2,
                name: 'categoria 2',
                image_url: 'string',
                duration: 10,
                idcustomer: 21,
                user_local: true
            },
            {
                idcategory: 3,
                name: 'categoria 3',
                image_url: 'string',
                duration: 10,
                idcustomer: 21,
                user_local: true
            }
        ];
        const cats = categories.map(c => new ContentCategory(c));
        let catsResult = component.userFilterCategories(cats);
        expect(catsResult).not.toBeNull();
    })
});