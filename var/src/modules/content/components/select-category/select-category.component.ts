import { Component,  Output, EventEmitter, ChangeDetectorRef, SimpleChanges, Input } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { IResult } from 'src/models/iresult';

import { FormControl} from '@angular/forms';

import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ContentCategory } from 'src/modules/content/models/category';
import { User } from 'src/modules/user/models/user';


@Component({
	providers: [],
	selector: 'content-select-category',
	templateUrl: './select-category.component.html',
	styleUrls: ['./select-category.component.scss']
})

export class ContentSelectCategoryComponent {
	@Input() selectedCategory: number;
	@Output() OnContentCategoryChange: EventEmitter<ContentCategory> = new EventEmitter();
	public contentCategories: ContentCategory[] = [];
	public idcategory: FormControl = new FormControl('');
	public user: User;

	constructor(
		private ws: WSService,
		private ustore: UserStore,
		private _snackBar: MatSnackBar) {
			this.user = this.ustore.getUser();
	}

	public ngOnInit() {  
		this.getContentCategoriesToSelect();
	}

	ngOnChanges(changes: SimpleChanges): void {
		//Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
		//Add '${implements OnChanges}' to the class.
		if(changes.selectedCategory.currentValue)
			this.idcategory.setValue(changes.selectedCategory.currentValue);
	}
	private getContentCategoriesToSelect() {
		this.ws.contentCategoriesToSelect().subscribe((result: IResult) => {
			if (result.success) {
				if (this.ustore && this.ustore.isUser()) {
					this.contentCategories = this.userFilterCategories(result.data.filter(cc => cc.user_local === 1).map(c => new ContentCategory(c)));
				} else {
					this.contentCategories = this.userFilterCategories(result.data.map(c => new ContentCategory(c)));
				}
				return;    
			} else {
				return null;
			}
		}, error => {
			return null;          
		})
	}

    userFilterCategories(categoryAll:  ContentCategory[]): ContentCategory[] {
        if(this.user && !this.user.hasOwnProperty('idcategories')) {
	       return categoryAll
		}
		if(this.user && !this.user.idcategories) {
			return categoryAll;
		}
		let	categoryUser;
		if(this.user)
			categoryUser = this.user.idcategories.split(',');
		categoryAll = categoryAll.map( (category: any) => {
        	if(categoryUser && categoryUser.some(e =>  e == category.idcategory)) {
				return category;
			}
		}).filter(item => item != undefined);
		return categoryAll;
	}
	 
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

	public onChangeContentCategory(event) {
        
        let category = this.contentCategories.filter(c => c.idcategory == event.value)[0];

     
		this.OnContentCategoryChange.emit(category);
	}
}