
import { Component, ElementRef, Input, Output, ViewChild, EventEmitter } from "@angular/core";
import { CreateCsvPeople } from "src/modules/people/components/create-csv-people/create-csv-people.component";
import { People } from "src/modules/people/models/IPeople";
import { PeopleService } from "src/modules/people/services/people.service";
import { Tag } from "src/modules/tag/models/tag";
import { ContentService } from "../../services/contents.service";



@Component({
    selector: 'message-stats',
    templateUrl: './message-stats.component.html',
    styleUrls: ['./message-stats.component.scss']
})
export class MessageStatsComponent {


    tags: Tag[] = [];
    count: number = 0;
    peoples: People[] = [];
    users: Set<number> = new Set();
    filename: string;
    @ViewChild('elem') elem: ElementRef;
    @Output() chargeCSV: EventEmitter<any> = new EventEmitter();
    @ViewChild(CreateCsvPeople) csv: CreateCsvPeople;

    constructor(private _people: PeopleService, private _content: ContentService) {
        this.getPeoples();
    }

    calculate(tags): void {
        if (tags.length > 0) {
            this.count = this.filterByTags(tags);
        } else {
            this.count = tags.length;
        }
    }


    getPeoples(): void {
        this._people.toSelect().subscribe(peoples => {
            this.peoples = peoples;
            this.count = this.peoples.length;
            this.peoples.forEach(people => {
                this.users.add(people.id)
            });
        })
    }

    filterByTags(tags): number {
        this.peoples.forEach(people => {
            tags.forEach(tag => {
                if (people.tags.some(t => t.idtag == tag.idtag)) {
                    this.users.add(people.id);
                }
            });
        });
        return this.users.size;
    }


    public uploadCSV(): void {
        this.elem.nativeElement.click()
    }




    public changeListener(files: FileList) {
        if (files && files.length > 0) {
            let file: File = files.item(0);
            this.filename = file.name;
            this.chargeCSV.emit(file);
        }
    }
}

