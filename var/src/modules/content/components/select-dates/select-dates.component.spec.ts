import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { ContentSelectDatesComponent } from './select-dates.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { MaterialModule } from 'src/app/material.module';
import { OverlayModule } from '@angular/cdk/overlay';
import moment from 'moment';

describe('Componente: ContentSelectDatesComponent', () => {
    let component: ContentSelectDatesComponent;
    let fixture: ComponentFixture<ContentSelectDatesComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports:[MaterialModule, OverlayModule],
            declarations: [
                ContentSelectDatesComponent
            ],
            providers:[],
            schemas:[CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        }).compileComponents();
    });

    beforeEach(async(() => {
        fixture = TestBed.createComponent(ContentSelectDatesComponent);
        component = fixture.componentInstance;
        fixture.whenStable().then(() => {
            fixture.detectChanges();
        });
    }));

    it("Creando componente", () => {
        expect(component).toBeTruthy();
    });

    it('onChangeStartDate', () => {
        const spy = spyOn(component.onDateChange, 'emit');
        component.formGroup.patchValue({startDate: moment()});
        component.formGroup.patchValue({endDate: moment()});
        component.onChangeStartDate();
        expect(spy).toHaveBeenCalled();
    });
    it('onChangeEndDate', () => {
        component.formGroup.patchValue({startDate: moment()});
        component.formGroup.patchValue({endDate: moment()});
        const spy = spyOn(component.onDateChange, 'emit');
        component.onChangeEndDate();
        expect(spy).toHaveBeenCalled();
    });
    it('openSnackBar', () => {
        const _snackbar = fixture.debugElement.injector.get(MatSnackBar)
        const spy = spyOn(_snackbar, 'open');
        component.openSnackBar('Mensaje a mostrar');
        expect(spy).toHaveBeenCalled();
    });
});