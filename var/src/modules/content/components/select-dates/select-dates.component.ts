import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, HostListener } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import moment from 'moment';
import { ContentDate } from 'src/models/date';
import { FormField } from 'src/modules/form-field/models/form-field';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
  providers: [],
  selector: 'content-select-dates',
  templateUrl: './select-dates.component.html',
  styleUrls: ['./select-dates.component.scss']
})
export class ContentSelectDatesComponent implements OnInit {
	@Input() date: ContentDate;
	@Input() position: number;
	@Output() onDateChange: EventEmitter<ContentDate> = new EventEmitter();

	public minDate:string = moment().format('YYYY-MM-DD');
	public dateDisabled:boolean = false;
	public formGroup:FormGroup = new FormGroup({
		startDate: new FormControl(null,[Validators.required]),
		endDate: new FormControl(null,[Validators.required]),
	});
	public fieldDataStart: FormField = {
		label: "Desde",
		name: 'startDate',
		placeholder: '',
		multiple:false,
		required: true,
		disabled:this.dateDisabled,
		minDate: moment(this.minDate).format('YYYY-MM-DD'),
		hideTime: true
	};
	public fieldDataEnd: FormField = {
		label: "Hasta",
		name: 'endDate',
		placeholder: '',
		multiple:false,
		required: true,
		minDate: moment(this.minDate).format('YYYY-MM-DD'),
		disabled:this.formGroup.get('startDate').value === null,
		hideTime: true
	};
  	
	constructor(
		private _snackBar: MatSnackBar
    ) {
    }

	ngOnChanges(changes: SimpleChanges): void {
		//Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
		//Add '${implements OnChanges}' to the class.
		if(changes.date.currentValue){
			this.formGroup.get('startDate').setValue(moment(changes.date.currentValue.date_on));
			this.formGroup.get('endDate').setValue(moment(changes.date.currentValue.date_off));
			this.fieldDataEnd.minDate = changes.date.currentValue.date_on;
		}
	}
  	ngOnInit(): void {}
 
  	fillData(){
  	}
	
  	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
	  		duration: 1500,
		});
  	}
	public onChangeStartDate(){
     
		this.fieldDataEnd.minDate = this.formGroup.get('startDate').value;
		const dates = {
			date_on: this.formGroup.get('startDate').value.format('YYYY-MM-DD'),
			date_off:this.formGroup.get('endDate').value.format('YYYY-MM-DD'),
			position: this.position
		}
		this.onDateChange.emit(dates)
	}
	 
	public onChangeEndDate(){
        this.fieldDataStart.maxDate = this.formGroup.get('endDate').value;
		const dates = {
			date_on: this.formGroup.get('startDate').value.format('YYYY-MM-DD'),
			date_off:this.formGroup.get('endDate').value.format('YYYY-MM-DD'),
			position: this.position
		}
		this.onDateChange.emit(dates)
	}
}
