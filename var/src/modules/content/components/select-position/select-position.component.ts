import { Component, ElementRef, ViewChild, Input, SimpleChanges, Output, EventEmitter, ChangeDetectorRef, ComponentFactoryResolver } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { IResult } from 'src/models/iresult';
import {  Router } from '@angular/router';
import { FormGroup, Validators, FormControl, FormBuilder} from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ContentCategory } from 'src/modules/content/models/category';
import { HttpClient } from "@angular/common/http";
import { TranslateService } from '@ngx-translate/core';

import { ConstantPool } from '@angular/compiler';

@Component({
	providers: [],
	selector: 'content-select-position',
	templateUrl: './select-position.component.html',
	styleUrls: ['./select-position.component.scss']
})

export class ContentSelectPositionComponent {
	@Output() OnContentPositionChange: EventEmitter<any> = new EventEmitter();
	@Input() selectedPosition: number = 1;

	public priority: FormControl = new FormControl(1, [Validators.required]);
	public priorities = [{ "priority" : 1, "name": "position_1"}, 
						 { "priority" : 2, "name": "position_2"},
						 { "priority" : 3, "name": "position_3"},
						 { "priority" : 4, "name": "position_4"}];
	constructor(
		private ws: WSService,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private cdr: ChangeDetectorRef) {
	}

	public ngOnInit() {  
	}
	ngOnChanges(changes: SimpleChanges): void {
		//Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
		//Add '${implements OnChanges}' to the class.
		if(changes.selectedPosition.currentValue)
			this.priority.setValue(changes.selectedPosition.currentValue);
	}
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

	public onChangeContentPosition(position) {
		this.priority.setValue(position.priority);
		this.OnContentPositionChange.emit(position);
	}
}