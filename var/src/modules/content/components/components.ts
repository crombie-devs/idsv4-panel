import { AssetsManagerComponent } from './assets-manager/assets-manager.component';
import { ContentSelectCategoryComponent } from 'src/modules/content/components/select-category/select-category.component';
import { ContentCategoryComponent } from 'src/modules/content/components/category/category.component';
import { ContentCategoryElement } from 'src/modules/content/components/category/element/category.element';
import { ContentElement } from 'src/modules/content/components/content/element/content.element';
import { ContentListComponent } from 'src/modules/content/components/content/list/list.component';
import { ContentsComponent } from 'src/modules/content/components/contents/contents.component';
import { ContentFormDataComponent } from 'src/modules/content/components/form-data/form-data.component';
import { ContentFormatComponent } from 'src/modules/content/components/format/format.component';
import { ContentSelectDatesComponent } from 'src/modules/content/components/select-dates/select-dates.component';
import { ContentSelectPositionComponent } from 'src/modules/content/components/select-position/select-position.component';
import { ContentsCarouselComponent } from 'src/modules/content/components/contents-carousel/contents-carousel.component';

export const CONTENT_COMPONENTS = [
    ContentFormDataComponent,
    AssetsManagerComponent,
    ContentSelectCategoryComponent,
    ContentCategoryComponent,
    ContentCategoryElement,
    ContentElement,
    ContentListComponent,
    ContentsComponent,
    ContentFormatComponent,
    ContentSelectDatesComponent,
    ContentSelectPositionComponent,
    ContentsCarouselComponent
];
