import { Component, Input, ViewChild, ViewChildren, QueryList, Renderer2, Output, EventEmitter, AfterViewInit } from "@angular/core";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent, CKEditorComponent } from "@ckeditor/ckeditor5-angular";
import { FormControl, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { FormatTemplateDialog } from "../../dialogs/form-template/format-template.dialog";
import { MessagesService } from "src/modules/messages/services/messages.service";
import { MatListItem } from "@angular/material/list";



@Component({
    selector: 'msg-text-containers',
    templateUrl: './msg-text-containers.component.html',
    styleUrls: ['./msg-text-containers.component.scss']

})
export class MsgTextContainersComponent implements AfterViewInit {

    @Input() email;
    @Input() sms;
    @Input() whatsapp;
    @Input() format;
    @Input() id;
    @Input() idlang;
    @Input() txt;
    @ViewChildren('template') template: QueryList<MatListItem>;
    @Output() messages: EventEmitter<any> = new EventEmitter();
    tags: any = {

    };
    form: FormGroup;
    body: string;
    public typeSelected: Set<any> = new Set();
    public Editor = ClassicEditor;
    templates: any[] = [];

    @ViewChild(CKEditorComponent) ckeditor: CKEditorComponent;


    constructor(private matDialog: MatDialog, private _message: MessagesService, private renderer: Renderer2) {
        this.form = new FormGroup({
            body: new FormControl({ email: false, sms: false, whatsapp: false, idlang: this.idlang, active: true }),
        });

        this._message.getTemplate().subscribe(templates => {
            this.templates = templates;

        });
    }

    public ngAfterViewInit(): void {

    }

    public onChange({ editor }: ChangeEvent) {
        const data = editor.getData();
        if (this.id) {
            this.form.value.body = data;
        } else {
            this.form.value.body.email = data;
        }
        this.form.value.body.idlang = this.idlang;
        this.messages.emit(this.form.value.body);

    }

    public openDialogTemplate() {
        const dialogRef = this.matDialog.open(FormatTemplateDialog, {

        });

        dialogRef.afterClosed().subscribe((result: never) => {
            if (result)
                this.templates.unshift(result);
        });
    }



    public setTemplate(index): void {
        const elem = this.template.toArray()[index];
        this.template.toArray().forEach(el => {
            this.renderer.setStyle(el._getHostElement(), 'background-color', 'white')
        });
        this.renderer.setStyle(elem._getHostElement(), 'background-color', 'gray');
        this.form.value.body['whatsapp'] = this.templates[index].name_template;
        this.form.value.body.idlang = this.idlang;
        this.messages.emit(this.form.value.body);
    }

    public setBody(event: any, format: string): void {
        this.form.value.body[format] = event.target.value;
        this.form.value.body.idlang = this.idlang;
        this.messages.emit(this.form.value.body);
    }









}

