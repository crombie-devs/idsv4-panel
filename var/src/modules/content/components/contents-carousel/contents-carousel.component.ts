import { Component, Input, Output, EventEmitter, SimpleChanges, ElementRef, ViewChild, HostListener, AfterViewInit, OnInit } from '@angular/core';
import { Content } from 'src/modules/content/models/content';
import { FileHandle } from 'src/directive/drag-drop.directive';
import { Format } from 'src/modules/format/models/format';
import { Lang } from 'src/modules/lang/models/lang';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { Asset, ASSET_TYPES } from 'src/modules/asset/models/asset';
import { PreviewDialog } from 'src/modules/asset/dialogs/preview/preview.dialog';
import { MatDialog } from '@angular/material/dialog';
import { AssetDialog } from 'src/modules/asset/dialogs/asset/asset.dialog';

@Component({
	providers: [FormatStore, LangStore],
	selector: 'content-contents-carousel',
	templateUrl: './contents-carousel.component.html',
	styleUrls: ['./contents-carousel.component.scss']
})

export class ContentsCarouselComponent implements OnInit, AfterViewInit{
    @Input() contents: Content[] = [];
	@Input() contentIndex: number = 0;
	@Input() formatsLangs:any[] = [];
	@Input() displayed:boolean = false;
	@Output() onSelectContent: EventEmitter<number> = new EventEmitter();
	@Output() onFileDropped: EventEmitter<any> = new EventEmitter(null);

	public carouselPosition:number = 0;
	public elementsView:number = 0;
	public langs: Lang[] = [];
	public formats: Format[] = [];
	public colWidth: number = 220;

	@ViewChild('contentWrapper', {static: true}) contentWrapper: ElementRef;
	
	@HostListener('window:resize', ['$event.target']) 
	onResize() {
	  this.resizeWorks();
	}
	constructor(
		private fstore: FormatStore,
		private lstore: LangStore,
		private matDialog: MatDialog,
	) {
		this.fstore.onChange.subscribe(formats => {
			this.formats = formats;
		});
		this.lstore.onChange.subscribe(langs => {
			this.langs = langs;
		});
	}

	public ngOnInit() {
	}

	ngOnChanges(changes: SimpleChanges): void {
		//Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
		//Add '${implements OnChanges}' to the class.
		this.resizeWorks();
	}
	ngAfterViewInit(){
		this.resizeWorks();
	}

	public next(){
		const posInit = this.contentWrapper.nativeElement.offsetLeft;
		let posFinal = posInit - this.colWidth;
     
		const maxLeft = -(this.contents.length * this.colWidth) - (this.elementsView * this.colWidth);
		let i = posInit;
		if(maxLeft > posFinal)
			posFinal = maxLeft;
		let anim = setInterval(() => {
			if(i >= posFinal){
				this.contentWrapper.nativeElement.style.left = `${i}px`;
				i -= 10;
			}else{
				clearInterval(anim);
			}
		}, 1);
		this.carouselPosition++;
	}

	public prev(){
		const posInit = this.contentWrapper.nativeElement.offsetLeft;
		let posFinal = posInit + this.colWidth;
  		let i = posInit;
		if(posFinal  > -this.colWidth)
			posFinal = 0;
		let anim = setInterval(() => {
			if(i <= posFinal){
				this.contentWrapper.nativeElement.style.left = `${i}px`;
				i += 10;
			}else{
				clearInterval(anim);
			}
		}, 1);
		this.carouselPosition--;
	}

	private goToSelected(){
		const posInit = this.contentWrapper.nativeElement.offsetLeft;
		const maxLeft = -(((this.contents.length) * this.colWidth) - (this.elementsView * this.colWidth));
		const maxPosition = this.contents.length - this.elementsView;  
		let i = posInit;
      	if(this.contentIndex >= this.elementsView){
    		if(i >= maxLeft){
				let anim = setInterval(() => {
					if(i >= maxLeft){
						this.contentWrapper.nativeElement.style.left = `${i}px`;
						i -= 10;
					}else{
						clearInterval(anim);
					}
				}, 1);
			}else{
                  i = i - 220;
				let anim = setInterval(() => {
					if(i <= maxLeft){
                     
						this.contentWrapper.nativeElement.style.left = `${i}px`;
						i += 10;
					}else{
						clearInterval(anim);
					}
				}, 1);
			}
			this.carouselPosition = maxPosition;			
		}
	}
	public selectContent(contentIndex){
		this.onSelectContent.emit(contentIndex);
	}
	
	private resizeWorks(){
        this.elementsView = Math.floor(this.contentWrapper.nativeElement.offsetWidth / this.colWidth) ;
      	if((this.contents.length > this.elementsView) && (this.contentIndex === this.contents.length - 1)) {
			this.goToSelected();
		}
	}

	public deleteAsset(asset){
		this.contents[this.contentIndex].assets.map((a,index) => {
			if(a === asset)
				this.contents[this.contentIndex].assets.splice(index,1);
		})
	}

    public editAsset(asset){
        const dialogRef = this.matDialog.open(AssetDialog, {data: asset});
		dialogRef.afterClosed().subscribe((result: Asset) => {
          
        }); 
	}

	filesDropped(file: FileHandle[], l:number, f: number): void {
		let lang = this.langs.filter(la => la.idlang === l)[0];
		let format = this.formats.filter(fa => fa.idformat === f)[0];
		const data = {
		  file:file[0],
		  lang:lang,
		  format:format
		}
		this.onFileDropped.emit(data);
	}
    
	public deleteContent(index){
		let newIndex = 0;
		if(index < this.contentIndex){
			newIndex = this.contentIndex - 1;
		}
		this.contents.splice(index, 1);
		this.selectContent(newIndex)
	}

	public setBackground(url){
		let style = {
			backgroundImage: 'url('+url+')',
		}
		return style;
	}

	openPreview(asset) {
		const dialogRef = this.matDialog.open(PreviewDialog, {data: asset});
		dialogRef.afterClosed().subscribe((result: Asset) => {}); 
	  }
}