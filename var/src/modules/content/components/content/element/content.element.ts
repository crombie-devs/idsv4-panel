import { Component, Input } from "@angular/core";
import { Content } from "src/modules/content/models/content";

@Component({
    selector: 'content-element',
    templateUrl: './content.element.html',
    styleUrls: ['./content.element.scss']
  })
export class ContentElement {

    @Input() content: Content = null;

    public is_play = false;
    public is_power = false;

    constructor() {     
      if (window.addEventListener) { 
        window.addEventListener("message", (e) => {  
            if(e.data.key === "is_play" && this.content  && e.data.value === this.content.idcontent) 
              this.is_play = true;                           
            else  
              this.is_play = false;

            if(e.data.key === "is_power" && this.content && e.data.value === this.content.idcontent)
              this.is_power = true; 
            else  
              this.is_power = false;      
        }, false); 
      }     
    }

}