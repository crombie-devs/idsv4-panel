import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from "@angular/core";
import { content } from 'html2canvas/dist/types/css/property-descriptors/content';
import { ContentOrder } from 'src/modules/content/models/order';
import { PlayArea } from 'src/modules/playarea/models/playarea';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { Player } from 'src/modules/player/models/player';
import { Playlogic } from 'src/modules/playlogic/models/playlogic';
import { ContentOrderStore } from 'src/modules/content/stores/order.store';
import { PlaylogicStore } from 'src/modules/playlogic/stores/playlogic.store';
import { Content } from "src/modules/content/models/content";
import { ContentStore } from "src/modules/content/stores/content.store";



@Component({
    selector: 'content-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
  })
export class ContentListComponent implements OnInit, OnChanges  {

    @Input() circuit: PlayCircuit = null;
    @Input() area: PlayArea = null;
    @Input() player: Player = null;
    loading: boolean = true;

    loops: any = []
    playlogics: Playlogic[] = [];
    contents: Content[] = [];

    constructor(
        private contentStore: ContentStore, 
        private orderStore: ContentOrderStore, 
        private playlogicStore: PlaylogicStore,
    ) {
        if (window.addEventListener) { 
            window.addEventListener("message", (e) => {  
                if(e.data.key === "contents"){
                    let validContents = e.data.value.map(c => new Content(c)); 
                    this.refresh(validContents); 
                }                 
            }, false); 
        }        
    }

    ngOnInit() {
        this.contentStore.onChange.subscribe(result => {
            console.log(result, 'list component')
            if(result) {
                this.loading = false;
                this.refresh();
            }
        });
        this.orderStore.onChange.subscribe(result => {
            if(result) {
                this.loading = false;
                this.refresh();
            }
        });
        this.playlogicStore.onChange.subscribe(result => {
            if(result) {
                this.loading = false;
                this.refresh();
            }
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if(changes.area)
            this.refresh();
        else if(changes.circuit)
            this.refresh();
    }

    private refresh(validContents?: Content[]) {
        this.playlogics = this.playlogicStore.filter(this.area, this.player);
        this.contents = this.contentStore.filter(this.circuit, this.area, this.player, validContents);
        this.loops = [];
        let max: number = this.maxContent();
        if(this.playlogics.length>0)
            for(let i=0; i<max; i++) 
                this.loops.push(this.playlogics);            
        else            
            this.loops.push(this.contents);
           
        this.order();
    
    }

    private order() {
        const orders = this.orderStore.filter(this.area, this.player);        
        this.contents = this.contents.map(c => {
            let found = orders.find(o => o.idcontent === c.idcontent);
            c.order = (found)? found.order : 0;
            return c;
        });
        this.contents = this.contents.sort((a, b) => a.order - b.order);
    }

    public getContent(playlogic: Playlogic, contentIndex: number): Content {
        let retorno = null;
        let contents = this.contents.filter(c => c.idcategory === playlogic.idcategory);
        if(contents.length > 0) {
            let index = contentIndex % contents.length;
            retorno = contents[index];
        }
        return retorno;
    }

    private maxContent() {
        let max: number = 0;
        this.playlogics.forEach(pl => {
            let count = this.contents.filter(c => c.idcategory === pl.idcategory).length;
            max = (count > max)? count : max; 
        });
        return max;
    }
}