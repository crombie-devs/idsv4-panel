import { Component, ElementRef, ViewChild, Input, SimpleChanges, Output, EventEmitter, ChangeDetectorRef, ComponentFactoryResolver } from '@angular/core';
import { FormGroup, Validators, FormControl, FormBuilder} from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatAutocomplete } from '@angular/material/autocomplete';
import { WSService } from 'src/services/ws.service';
// Carga de Modelos
import { IResult } from 'src/models/iresult';
import { Tag } from 'src/modules/tag/models/tag';
import { Product } from 'src/modules/product/models/product';
import { Primetime } from 'src/modules/primetime/models/primetime';
import { User } from 'src/modules/user/models/user';
import { Recomendation } from 'src/modules/recomendation/models/recomendation';
import { ContentCategory } from 'src/modules/content/models/category';
import { Power } from 'src/modules/power/models/power';
import { Content } from 'src/modules/content/models/content';
import { ContentDate } from 'src/models/date';
import { AudioCategoryModel } from 'src/modules/audio/models/audio-category.model';
import { Site } from 'src/modules/site/models/site';
// Carga de Stores
import { DebugStore } from 'src/stores/debug.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { RecomendationStore} from 'src/modules/recomendation/stores/recomendations.store';
import { ContentStore } from 'src/modules/content/stores/content.store';
import { SiteStore } from 'src/modules/site/stores/site.store';
import { AudioCategoryStore } from 'src/modules/audio/stores/audiocategory.store';

import moment from 'moment';
import { CONFIG } from 'src/app/app.config';
@Component({
	providers: [ RecomendationStore, ContentStore, SiteStore],
	selector: 'content-form-data',
	templateUrl: './form-data.component.html',
	styleUrls: ['./form-data.component.scss']
})
export class ContentFormDataComponent {
	@Input() dates: ContentDate[] = [];
	@Input() isEditing: boolean;
	@Input() content: Content;
	@Input() sites: Site[];
	@Output() onChangedForm: EventEmitter<any> = new EventEmitter();
	@Output() onChangedDates: EventEmitter<ContentDate[]> = new EventEmitter();
	@Output() onChangedTags: EventEmitter<Tag[]> = new EventEmitter();
	@Output() onChangedProducts: EventEmitter<Product[]> = new EventEmitter();
	@Output() onChangedPowers: EventEmitter<Power[]> = new EventEmitter();

	@ViewChild('productInput') productInput: ElementRef<HTMLInputElement>;
	@ViewChild('auto') matAutocomplete: MatAutocomplete;
	@ViewChild('contentName') contentName: ElementRef;

	public selectedTags: Tag[] = [];
	public selectedProducts: Product[] = [];
	public selectedContentPowers:Power[] = [];
	public selectedContentData:any = {};
	public load: boolean = true;
	public files: any = [];
	public loadFiles = false;
	public permission: boolean = false;
	public onEdit: boolean = false;
	public contentCategories: ContentCategory[] = [];
	public contentCategoryDuration: number = 0;
	public storageItems:any;
	public statusCt:number = 0;
	public statusCtProd:number = 0;
	public enableTag:number = 0;
	public filterName: string = '';
	public firstRequest: boolean = true;
	public primetimes: Primetime[] = [];
	public weekdays = ['', 'WEEKDAYS.mon', 'WEEKDAYS.tue', 'WEEKDAYS.wed', 'WEEKDAYS.thu', 'WEEKDAYS.fri', 'WEEKDAYS.sat', 'WEEKDAYS.sun'];	
	public frequencyMethods = [0, 1];
	public emptyCats:boolean = false;
	public showAudioSync:boolean = false;
	public loadingPrimetime:boolean = false;
	public contentCounter:number = 1;

	public contentForm: FormGroup;
	public idaudiocategory: FormControl = new FormControl();
	public name: FormControl = new FormControl(null);
	public idtype: FormControl = new FormControl(2, [Validators.required]);
	public code: FormControl = new FormControl("code", [Validators.required]);
	public thumbnail_url: FormControl = new FormControl("thumbnail_url", [Validators.required]);
	public tracker_url: FormControl = new FormControl("tracker_url", [Validators.required]);
	public idcategory: FormControl = new FormControl(null);
	public is_exclusive: FormControl = new FormControl(false);
	public status: FormControl = new FormControl(1);
	public primetime: FormControl = new FormControl(1);
	public frequency: FormControl = new FormControl(0);
	public frequencyMethod: FormControl = new FormControl(0);
	public maxEmissions: FormControl = new FormControl(null);
	public pixelUrl: FormControl = new FormControl(null);
	public priority: FormControl = new FormControl(1, [Validators.required]);
	public idcustomer: number = null;
	public idsite: number = null;
	public me: User;
	public recomendation:Recomendation;
	public reload:boolean = false;
    public categories: AudioCategoryModel[] = [];
	public audioCategory: string;
	public sendMail: number = 0;
	public excluyente: boolean;

	constructor(
		private ws: WSService,
		private debug: DebugStore,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
	    private fb: FormBuilder, 
		private domSanitizer: DomSanitizer,
		private cdr: ChangeDetectorRef,	
		private rstore: RecomendationStore,
		private store: AudioCategoryStore) {
			this.rstore.onChange.subscribe(recomendation => {
				this.recomendation = recomendation;
			});
	}

	public ngOnInit() {  
    
     	this.getAudioCategories(); 
		this.me = this.ustore.getUser();
		if(this.me){
			if(!this.me.customer.has_sales){
				this.primetime.setValue(0);
			}
			if(this.me.customer) {
				this.showAudioSync = this.me.customer.has_audioasync ? true : false;
			} else {
				this.showAudioSync = false;
			}
			
			this.idcustomer = this.me.customer ? this.me.customer.idcustomer : null;
			
			this.idsite = this.me.site ? this.me.site.idsite : null;
		}
		this.selectedContentData.primetimeData = [];
		this.selectedContentData.sites = this.sites;

      
		if(this.content)
			this.initForm(this.content);
        else
            this.initForm();
		this.getContentCategoriesToSelect();
		this.load = false;
		if(this.recomendation){
			this.rstore.delete();
			let products:Product[] = [];
			products.push(this.recomendation.product)
			if(this.recomendation.assocProd)
				products.push(this.recomendation.assocProd)
			this.onSaveProductDialog(products, true);
			this.getTagsFromRecomendations();
		}
	}
	
	public ngOnChanges(changes: SimpleChanges): void {
		//Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
		//Add '${implements OnChanges}' to the class.
		if(changes.content){
			this.initializeFromContent();
		}

        if(changes.sites){
			this.initializeFromSites();
        }
	}

	private initializeFromSites(){
		this.selectedContentData.sites = this.sites;
		this.getPrimetimeData();
	}

	private initializeFromContent(){
		this.dates = [];
		this.content.dates.map(date => {
			this.dates.push({date_on: date.date_on, date_off: date.date_off});
		})
		this.selectedContentPowers = this.content.powers.map(p => new Power(p));
		this.selectedProducts = this.content.products.map(p => new Product(p));
		this.selectedTags = this.content.tags.map(t => new Tag(t));
		this.initForm(this.content);
		this.onChangeContentCategory(this.content.idcategory);
		this.onChangeContentPosition(this.content.priority);
		this.onSaveTagDialog(this.selectedTags);
	}

	getAudioCategories(): void {
       this.store.reload();
		this.store.onChange.subscribe(items => {
			if(items){
				this.categories = items;
			}  
		});
	} 

	addAudio(event): void {
		this.audioCategory = event.value;
		this.onChangedForm.emit(this.contentForm);
	}

	getContentToUpdate(){
		if(this.content.idcontent)
			this.onEdit = true
		const content = this.content;
		if(content.tags && content.tags.length > 0) {
			this.onSaveTagDialog(content.tags);	
		}
		if(content.powers && content.powers.length > 0){
			let selectedPowers: Power[] = [];
			content.powers.map(power => {
				selectedPowers.push(power);
			});
			this.onSaveContentPowerDialog(selectedPowers);
		}		
	}

	composePower(power){
		let powerToAdd:any;
		powerToAdd = {
			day: this.weekdays[power.weekday],
			day_number: power.weekday,
			times:[],
			selections:[]
		}
		const time_on = moment("2015-01-16T"+power.time_on);
		const time_off = moment("2015-01-16T"+power.time_off);
		powerToAdd.times.push({ time_on: time_on.format("HH:mm"), time_off: time_off.format("HH:mm")});
		do{
			powerToAdd.selections.push(time_on.format("HH:mm"));
			time_on.add(1,'hours');
		}while((time_on <= time_off) && (time_on.format('mm') === '00' && time_off.format('mm') === '59'))
		return powerToAdd
	}

	composeManualPower(power){
		const time_on = moment("2015-01-16T"+power.time_on);
		const time_off = moment("2015-01-16T"+power.time_off);
		let powerToAdd:any = {};
		if(time_on.format('mm') !== '00' || time_off.format('mm') !== '59'){
			powerToAdd = {
				day: this.weekdays[power.weekday],
				day_number: power.weekday,
				time_on: time_on.format("HH:mm"),
				time_off: time_off.format("HH:mm")
			}
		}
		return powerToAdd
	}

	private initForm(content:Content = null) {
		if(content){
			let obj: Object = {
				name: content.name,
				idtype: content.type ? content.type : 1,
				code: content.code !== null && content.code !== '' ? content.code : 'code',
				thumbnail_url: content.thumbnail_url,
				tracker_url: content.tracker_url !== null && content.tracker_url !== '' ? content.tracker_url : 'tracker_url',
				idcategory: content.idcategory ? content.idcategory : null,
				is_exclusive: content.is_exclusive,
				status: content.status,
				primetime: content.primetime,
				frequency: content.frequency,
				frequencyMethod: content.frequency_method,
				maxEmissions: content.max_emissions,
				priority: content.priority ? content.priority : 1,
				idaudiocategory: content.idaudiocategory
			}
			if(content.is_exclusive) {
				this.excluyente = true;
			}
			this.contentForm = this.fb.group(obj);
	
			this.getContentToUpdate();			
			if(this.idcustomer)
				this.contentForm.addControl('idcustomer', new FormControl(this.idcustomer, Validators.required));
			if(this.idsite)
				this.contentForm.addControl('idsite', new FormControl(this.idsite, Validators.required));
			if (content.idcategory)
				this.onChangeContentCategory(content.idcategory);
			if(content.priority)
				this.onChangeContentPosition(content.priority);
			this.onChangedForm.emit(this.contentForm);
		}
	}

	private getContentCategoriesToSelect() {
		this.ws.contentCategoriesToSelect().subscribe((result: IResult) => {
			if (result.success) {
				this.contentCategories = result.data.map(c => new ContentCategory(c));
				if (this.ustore && this.ustore.isUser())
					this.contentCategories = result.data.filter(cc => cc.user_local === 1).map(c => new ContentCategory(c));
				if(!this.content || (this.content && !this.content.idcategory)){
					this.onChangeContentCategory(this.contentCategories[0])
				}
				this.load = false;
			} else {
				this.debug.addError('WS.contentcategories', result.data);
				this.load = false;
				return null;
			}
		}, error => {
			this.debug.addError('WS.contentcategories', error.message);  
			this.load = false;
			return null;          
		})
	}

	public deleteAttachment(index) {
		this.files.splice(index, 1)
	}

	public sanitizeUrl(url) {
		return this.domSanitizer.bypassSecurityTrustUrl("blob:" + url);
	}
	
	public onSaveTagDialog(selectedTags: Tag[]) {
		selectedTags.map(selectedTag => {
			if(!this.selectedTags.some(t => t.idtag === selectedTag.idtag))
				this.selectedTags.push(selectedTag);
		});
		this.onChangedTags.emit(this.selectedTags);
	}
	
	public onRemoveTag(idtag: number) {
		this.selectedTags.forEach(function(tag, i) {
			let tagCat = tag['idcategory'];
			if(tag.idtag == idtag){
				this.selectedTags.splice(i, 1)
				this.statusCat(tagCat)
			}
		}, this)
	}
	
	public statusCat(id:number){
		let categoryExist = false
		this.statusCt = 0;
		this.selectedTags.forEach(function(tag, i) {
			if(tag['idcategory'] == id){
				categoryExist = true;
			}
		}, this);
		
		if(!categoryExist){
			this.statusCt = id;
		}
	}
	
	public statusCatProduct(){
		let categoryExist = false
		this.statusCtProd = 0;
		if(this.selectedProducts){
			if(this.selectedProducts.length > 0){
				categoryExist = true
			};
		};
		if(!categoryExist){
			this.statusCtProd = 1;
		}
	}
	
	public statusSelectedProduct(){
		let ProductExist = false
		this.enableTag = 0;
		if(this.selectedProducts){
			if(this.selectedProducts.length > 0){
				ProductExist = true;
			}else{
				ProductExist = false;
			}
		};
		if(ProductExist){
			this.enableTag = 1;
		};
	}
	
	public onSaveProductDialog(selectedProducts, fromRecomendation:boolean = false) {  
        this.selectedProducts = selectedProducts;
		let productsArray:number[] = [];
		let tagsArray:Tag[] = [];
		this.selectedProducts.map(product => {
			productsArray.push(product.idproduct);
		});
		this.selectedContentData.products = this.selectedProducts;
		if(this.contentForm.get("primetime").value)
			this.getPrimetimeData();
		let selectedTags:Tag[] = [];
		this.ws.getAllProductTags(productsArray).subscribe(result => {
			if(result.success){
				result.data.map(product => {
					if(product.tags.length > 0){
						product.tags.map(pt => {
							if(!selectedTags.some(t => t.idtag === pt.idtag))
								selectedTags.push(pt);
						});
						this.onSaveTagDialog(selectedTags);
					} else
						this.selectTagsFromOtherProductOfCategory(product);
				});
			}
		});	
		if(!fromRecomendation)
			this.statusSelectedProduct();
		this.onChangedProducts.emit(this.selectedProducts);
	}

	private selectTagsFromOtherProductOfCategory(product){
		let selectedTags:Tag[] = [];
		this.ws.getProductsFromCustomer().subscribe((result) => {
			if (result.success) {
				let products = result.data.filter(p => p.idcategory === product.idcategory).sort((a,b) => {
					if (a.idproduct < b.idproduct) {
					  return -1;
					}
					if (a.idproduct > b.idproduct) {
					  return 1;
					}
					return 0;
				});
				products.map(p => {
					if(p.tags.length > 0){
						p.tags.map(t => {
							if(!selectedTags.some(ta => ta.idtag === t.idtag))
								selectedTags.push(t);
						})
					}
				})
				this.onSaveTagDialog(selectedTags);
			}
		});
	}

	public getPrimetimeData(){
       
		if(!this.selectedContentData.products || !this.selectedContentData.sites){ return; }

		this.loadingPrimetime = true;
        const products = this.selectedContentData.products;
		const sites = this.selectedContentData.sites;
        let params:any = {categories:[], sites:[]};



        products.map(product => {
            if(product.category != null) {
                if(!params.categories.some(c => c === product.category.code))
                    params.categories.push(product.category.code);
            }
          
        })
        
		sites.map(site => {
            if(!params.sites.some(s => s === site))
                params.sites.push(site.code);
        })
     
		this.selectedContentData.primetimeData = [];
        if(params.sites.length > 0 && params.categories.length > 0)
            this.ws.getContentPrimeTime(params).subscribe(result => {
            if(result.success){
                this.selectedContentData.primetimeData = this.organize(result.data);
                this.loadingPrimetime = false;
            }
            })  
	}

	organize(data){
        let result = [];
        data.map(element => {
          if(!result[element.site]){
            result[element.site] = [];
            result[element.site].push({weekday: element.weekday, time: element.hour});
          }else{
            if(!result[element.site].some(el => el.weekday === element.weekday && el.time === element.hour))
                result[element.site].push({weekday: element.weekday, time: element.hour});
          }
        });
        return result;
    }

	public onRemoveProduct(idproduct: number) {
		this.selectedProducts.forEach(function(product, i) {
			let tagCatProd = product['category'];
			if(product.idproduct == idproduct)
				this.selectedProducts.splice(i, 1);
			this.statusCatProduct();
			}, this)
	}
	
	public onSaveContentPowerDialog(selections) {
		this.selectedContentPowers = [];
		selections.map(power => {
			power.time_on = moment("2015-01-16T"+power.time_on).format('HH:mm');
			power.time_off = moment("2015-01-16T"+power.time_off).format('HH:mm');
			power.new = false;
			this.selectedContentPowers.push(power);
		})
		this.onChangedPowers.emit(this.selectedContentPowers);
	}
	
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
	
	getPrimeTime(){
		if(this.selectedProducts[0] && (this.selectedProducts[0].category)){
			let params:any = {
				code_site: 60060
			}
			params.idcategory = this.selectedProducts[0].category.code;
			this.ws.getProductPrimeTime(params).subscribe(result => {
				if(result.success){
					const res = this.groupByWeekday(result.data);
					let powers:any[] = [];
					res.map((elem, index) => {
						let time_on = 0;
						let time_off = 0;
						if(elem.length > 1){
							elem.map(el => {
								if(time_on === 0){
									time_on = el;
									time_off = el;
								}else if(el === time_off + 1){
									time_off = el;
								}
								if(time_off !== el || el === elem[elem.length - 1]){
									let time = { weekday: index, time_on: `${time_on < 10 ? '0'+time_on : time_on}:00`, time_off: `${time_off < 10 ? '0'+time_off: time_off}:59`, new: true }
									time_on = 0;
									time_off = 0;
									powers.push(time);						
								}
							})
						}else{
							powers.push({ weekday:index, time_on: `${elem[0] < 10 ? '0'+elem[0] : elem[0]}:00`, time_off: `${elem[0] < 10 ? '0'+elem[0]: elem[0]}:59`, new: true });
						}
					})
					this.onSaveContentPowerDialog(powers);
				}
			})
		}
	}

	groupByWeekday(data){
		let result = [];
		data.map(element => {
		  if(!result[element.weekday]){
			result[element.weekday] = [];
		  }
		  result[element.weekday].push(element.hour);
		});
		return result;
	}

	getTagsFromRecomendations(){
		if(!CONFIG.demo) 
			return;

		this.ws.tags().subscribe(result => {
			let tags:Tag[] = [];
			if(result.success){
				result.data.map(tag => {
					if(this.recomendation.genre.some(t => t === tag.idtag) || 
					   this.recomendation.type.some(t => t ===tag.idtag)  || 
					   this.recomendation.temperatura.some(t => t === tag.idtag)){
							tags.push(tag);
					}
				})
				this.onSaveTagDialog(tags);
			}
		}, err => {
		});
	}
	
	public onChangeContentCategory(category) {
		if(category && category.idcategory){
			this.contentCategoryDuration = category.duration;
			this.contentForm.patchValue({idcategory : category.idcategory});
			this.idcategory.setValue(category.idcategory);
		}else{
            if(this.contentForm) {
                this.contentForm.patchValue({idcategory : category});
			    this.idcategory.setValue(category);
            }
		}
		this.onChangedForm.emit(this.contentForm);
	}

	public onChangeContentPosition(priority) {
		if(priority.priority){
			this.contentForm.patchValue({priority : priority.priority});
			this.priority.setValue(priority.priority);
		}
		else{
			this.priority.setValue(priority);
			this.contentForm.patchValue({priority : priority});
		}
		this.onChangedForm.emit(this.contentForm);
	}
	public onEmptyCats(e){
		this.emptyCats = true;
	}

	public checkPrimetime(data){		
		if(data.checked && this.contentForm.get("is_exclusive").value) 
			this.contentForm.get("primetime").patchValue(false);
		let prime = this.contentForm.get("primetime").value;
		if(prime && this.selectedContentData.primetimeData.length === 0 && this.selectedContentData.products && this.selectedContentData.products.length > 0)
			this.getPrimetimeData();
		this.onChangedForm.emit(this.contentForm);
	}

	ngAfterContentChecked() {
		this.cdr.detectChanges();
	}

	public fieldChanged(data){
        this.excluyente = this.contentForm.value.is_exclusive;
		if(data.source){
			if(data.source.name === 'exclusive' && data.checked) {
				this.contentForm.get("primetime").patchValue(!this.excluyente);
			 }
			 if(data.source.name === 'primetime' && data.checked && this.contentForm.get("is_exclusive").value) {
			  this.contentForm.get("primetime").patchValue(false);
		   }
		}
        this.onChangedForm.emit(this.contentForm);
	}

}