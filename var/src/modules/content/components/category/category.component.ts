import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, EventEmitter, Input, Output, Renderer2, SimpleChanges } from "@angular/core";
import { first } from 'rxjs/operators';
import { Content } from 'src/modules/content/models/content';
import { ContentCategory } from 'src/modules/content/models/category';
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { ContentCategoryStore } from 'src/modules/content/stores/category.store';
import { DebugStore } from 'src/stores/debug.store';


@Component({
    selector: 'content-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss']
  })
export class ContentCategoryComponent {
    
    @Input() category: ContentCategory;
    @Output() onChange: EventEmitter<void> = new EventEmitter();
    public contents: Content[] = [];
    public filterContents: Content[] = [];
    public loading: boolean = true;

    isChecked = false;

    constructor( private ws: WSService, private debug: DebugStore){ 
    }

    public drop(event: CdkDragDrop<Content[]>) {
		moveItemInArray(this.filterContents, event.previousIndex, event.currentIndex);	
        //this.updateOrder(event.container.data);
        this.filterContents = event.container.data;
		this.filterContents.forEach((c, index) => c.order = index);
    }
    	
    
    public ngOnChanges(changes: SimpleChanges) {
        if (changes.category.firstChange)
            this.getContent(true);
        if (changes.category)
            this.filterContents = this.filter(this.category);        
    }

    private filter(category: ContentCategory){
        return this.contents.filter(c => c.idcategory === category.idcategory);				

    }

    private getContent(first:boolean = false){
        this.ws.contents().subscribe((result: IResult) => {
            if(result.success) {
                this.loading = false;
                this.debug.addInfo('WS.contents', 'successful');
                this.contents = result.data;
                if (first)
                    this.filterContents = this.filter(this.category);
            }
            else {            
                this.debug.addError('WS.contents', result.data);
            }
        }, error => {
            this.debug.addError('WS.contents', error.message);            
        }); 
    }

    public add(){
        this.ws.contentsOrder(this.filterContents).subscribe((result: IResult) => {
            if(result.success) {
                this.debug.addInfo('WS.contentsToSelect', 'successful');    
            } else {          
                this.debug.addError('WS.contentsToSelect', result.data);        
            }    
        }, error => {
            this.debug.addError('WS.contentsToSelect', error.message);            
        }); 
        this.onChange.emit();
    }

    public changeStatus(content: Content){
        let data = {"status": content.status};
            this.ws.updateContent(content.idcontent, data ).subscribe((result: IResult) => {
				if(result.success) {
                    this.debug.addInfo('WS.playlogicToSelect', result.data);    
                }
				else {            
					this.debug.addError('WS.playlogicToSelect', result.data);
				
				}
			}, error => {
				this.debug.addError('WS.playlogicToSelect', error.message);         
			});       
    }

    public close() {
        this.onChange.emit();
    }
    
}

