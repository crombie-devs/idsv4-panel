import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Router } from '@angular/router';
import { PlayCircuit } from 'src/modules/playcircuit/models/playcircuit';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { MatSnackBar } from '@angular/material/snack-bar';
import { WSService } from 'src/services/ws.service';
import { MatDialog } from '@angular/material/dialog';
import { ContentCategoryDialog } from 'src/modules/content/dialogs/category/category.dialog';
import { ContentCategory } from 'src/modules/content/models/category';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'content-category-element',
  templateUrl: './category.element.html',
  styleUrls: ['./category.element.scss']
})

export class ContentCategoryElement {

  
	@Input() contentcategory:ContentCategory;
	@Input() editContentcategory: boolean;
	@Input() deletedContentcategory: boolean;
	@Output() onDeleted = new EventEmitter<number>();
	@Output() onUpdate = new EventEmitter<number>();

  	constructor(
  		private ws: WSService,
    	private dialog: DialogService,
		private debug: DebugStore,
		private _snackBar: MatSnackBar,
		public matDialog: MatDialog,
		private translate: TranslateService
    ) {}  
  
    public update(idcategory:number){
		if (idcategory==0){
			this.dialog.warning('DIALOGS.warning.title', 'COMPONENTS.contents_category.element.update.error.text').subscribe();
		}else{
			let dialogRef;
			dialogRef = this.matDialog.open(ContentCategoryDialog, {
				data: this.contentcategory
			});
			dialogRef.afterClosed().subscribe(result => {
				if (!result)
					return;
				if (result.success){
					//this.contentcategory = result.data;
					this.onUpdate.emit();
					this.openSnackBar(this.translate.instant('COMPONENTS.contents_category.element.update.success'));
				}
				else
					this.openSnackBar(this.translate.instant('COMPONENTS.contents_category.element.update.error.text2'));	
			});			
		}
	}
	
	public delete(idcategory:number){
		if(idcategory == 0){
			this.dialog.warning('DIALOGS.warning.title', 'COMPONENTS.contents_category.element.delete.error.text').subscribe();
		}else{
			this.dialog.confirm('COMPONENTS.contents_category.element.delete.confirm.title','COMPONENTS.contents_category.element.delete.confirm.text').subscribe(confirm => {
				if (confirm){
					this.ws.deleteCategory(idcategory).subscribe((result: IResult) => {
						if(result.success) {
							this.onDeleted.emit();
							this.debug.addInfo('WS.deleteCategory', 'successful');
							this.openSnackBar(this.translate.instant('COMPONENTS.contents_category.element.delete.success'));
						} else {            
							this.debug.addError('WS.deleteCategory', result.data);	
							this.openSnackBar(result.data);			
						}
					}, error => {	
						if (error.status === 403){
							this.openSnackBar(this.translate.instant("No puede borrar categorías con contenidos asociados"));
						}else{
							this.openSnackBar(error.message);
						}
						this.debug.addError('WS.deleteCategory', error.message);           
					});						
				}
			});
		}		
	}
	
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
	

}
