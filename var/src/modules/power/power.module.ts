import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { POWER_COMPONENTS } from './components/components';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/shared/shared.module';

@NgModule({
  declarations: [POWER_COMPONENTS],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule
  ],
  providers: [],
  exports: [POWER_COMPONENTS]
})
export class PowerModule {}