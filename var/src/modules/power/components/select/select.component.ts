import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'power-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class PowerSelectComponent implements OnInit {
  
  @Input() powerCount: number
  @Input() selections
  @Output() onSelect = new EventEmitter();
  @Output() onSave = new EventEmitter();
  public selection:any[] = [];
  public day: string;
  public selectedWeekdays = [];
  public timeOn: string;
  public timeOff: string;
  public weekdays = [
      {
          name: "WEEKDAYS.mon",
          short_name: "L",
          day_number: "1",
          selected: false
      },
      {
          name: "WEEKDAYS.tue",
          short_name: "M",
          day_number: "2",
          selected: false
          
      },
      {
          name: "WEEKDAYS.wed",
          short_name: "X",
          day_number: "3",
          selected: false
      },
      {
          name: "WEEKDAYS.thu",
          short_name: "J",
          day_number: "4",
          selected: false
      },
      {
          name: "WEEKDAYS.fri",
          short_name: "V",
          day_number: "5",
          selected: false
      },
      {
          name: "WEEKDAYS.sat",
          short_name: "S",
          day_number: "6",
          selected: false
      },
      {
          name: "WEEKDAYS.sun",
          short_name: "D",
          day_number: "7",
          selected: false
      }
  ]

  constructor(
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.powerCount = Number(this.powerCount)
    if(this.selections) {
      for(let i of Object.keys(this.selections)){
        if(i != "powerCount") {
          let selection = this.selections[i]
        
          this.weekdays.map((weekday) => {
            if(weekday.name == selection.day) {
              this.selectedWeekdays.push(weekday)
              weekday["selected"] = true
            }
          })
          
          this.timeOn = selection.time_on
          this.timeOff = selection.time_off 
        }
      }
    }
  }
  
  public selectDay(e, weekday) {   
    if(weekday.selected){
      this.selectedWeekdays.map((selectedWeekday, i) => {
        if(weekday.name == selectedWeekday.name) {
          this.selectedWeekdays.splice(i, 1)
        }
      }, this)
      weekday.selected = !weekday.selected
    } else {
      weekday.selected = true
      this.selectedWeekdays.push(weekday)
    }
  }
   
  public onSavePower(e) {
    e.preventDefault();
    if(this.selectedWeekdays.length > 0 && this.timeOn && this.timeOff) {
      if(this.timesValidated()){
        this.selection = [];
        this.selectedWeekdays.map(day => {
          this.selection.push({weekday: parseInt(day.day_number), time_on: this.timeOn, time_off: this.timeOff, new:true});
        })
        this.weekdays.map(day => {
          day.selected = false;
        })
        this.selectedWeekdays = [];
        this.timeOn = null;
        this.timeOff = null;
        this.onSave.emit(this.selection)
      }else{
        this.openSnackBar("Las horas seleccionadas no son correctas. la hora de finalización tiene que ser superior a la de inicio.");
      }
    }else{
      this.openSnackBar("Tienes que completar el formulario antes de poder guardarlo.");
    }
  }

  private timesValidated(){
    let res = false;
    console.log(this.timeOn, this.timeOff);
    return this.timeOff > this.timeOn;
  }

  public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}
}
