import moment from 'moment';
import { MYDate } from '../../../libs/date.libs';

export class Power {
	id?: number;
    weekday: number;
    time_on: string;
	time_off: string;
	new?:boolean;
	full?:boolean;
	
	constructor(po?: any) {
		if(!po)
			return;

		this.id = po.id;
		this.weekday = po.weekday;
		this.new = po.new;
		this.full = po.full;
		this.time_on = po.time_on;
		this.time_off = po.time_off;
	}

	initWithJson(json: any) {
		if(!json)
			return;

		this.id = json.idchromepower;
		this.weekday = json.weekday;
		this.new = json.new;
		this.full = json.full;
		this.time_on = json.time_on;
		this.time_off = json.time_off;
	}

	public compare(po: Power): number {
		if(this.weekday < po.weekday) {
			return -1;
		}
		else if(this.weekday === po.weekday) {
			if(this.time_on < po.time_on)
				return -1;
			else if(this.time_on == po.time_on)
				return 0;
			else
				return 1;
		}
		else {
			return 1;	
		}	
	}

	public timestampOn(date?: Date): number {
		const currentDate = (date)? new Date(date.getTime()) : new Date();
		return  MYDate.DatetimeToTimestamp(currentDate, this.time_on);
	}

	public timestampOff(date?: Date): number {
		const currentDate = (date)? new Date(date.getTime()) : new Date();
		return  MYDate.DatetimeToTimestamp(currentDate, this.time_off);
	}

	private isValidYesterday(date?: Date): boolean {
		const todayDate: Date = (date)? new Date(date.getTime()) : new Date();
		const todayWeekday = MYDate.weekDayByDate(todayDate);
		const todayTimestamp: number = todayDate.getTime();
		
		const timestamp_on: number = this.timestampOn(todayDate);
		let timestamp_off: number = this.timestampOff(todayDate);
		if(timestamp_on <= timestamp_off){ return false; }

		return (((this.weekday + 1) === todayWeekday) && (timestamp_off > todayTimestamp));
	}

	private isValidToday(date?: Date): boolean {
		const todayDate: Date = (date)? new Date(date.getTime()) : new Date();
		const todayWeekday = MYDate.weekDayByDate(todayDate);
		const todayTimestamp: number = todayDate.getTime();
		
		const timestamp_on: number = this.timestampOn(todayDate);
		let timestamp_off: number = this.timestampOff(todayDate);
		if(timestamp_off <= timestamp_on) 
			timestamp_off = this.timestampOff(MYDate.addDays(todayDate, 1));

		return ((this.weekday === todayWeekday) && (timestamp_on < todayTimestamp && timestamp_off > todayTimestamp));
	}

	public isValid(date?: Date): boolean {
		return (this.isValidYesterday(date) || this.isValidToday(date));
	}

	public isAfter(date?: Date): boolean {
		const currentDate: Date = (date)? new Date(date.getTime()) : new Date();
		const currentTimestamp: number = currentDate.getTime();
		
		const timestamp_on: number = this.timestampOn(currentDate);
		let timestamp_off: number = this.timestampOff(currentDate);
		if(timestamp_off <= timestamp_on) 
			timestamp_off = this.timestampOff(MYDate.addDays(currentDate, 1));

		return (this.isWeekday(currentDate) && (timestamp_on > currentTimestamp && timestamp_off > currentTimestamp));
	}

	public isWeekday(date?: Date): boolean {
		const currentDate: Date = (date)? new Date(date.getTime()) : new Date();
		const currentWeekday = MYDate.weekDayByDate(currentDate);

		return (this.weekday === currentWeekday);
	}
}