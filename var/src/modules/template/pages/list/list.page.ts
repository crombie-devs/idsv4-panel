import { Component, ViewChild } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Router } from '@angular/router';
import { Template } from 'src/modules/template/models/template';
import { MYString } from 'src/libs/string.libs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'template-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss']
})

export class TemplateListPage {

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
  
	public displayedColumns: string[] = ['select','idtemplate', 'name', 'html'];
	public selection = new SelectionModel<Template>(true, []);
	public dataSource = new MatTableDataSource<Template>();
	public new: boolean = false;
	public edit: boolean = false;
	public deleted: boolean = false;
	public me: any;	
	public permission: boolean = false;
	public load: boolean = true;
	sortedData: Template[];
	templates: Template[];

  	constructor(
		private ws: WSService,
		private debug: DebugStore,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private dialog: DialogService,
		private router: Router,
        private translate: TranslateService) {
			this.ustore.onChange.subscribe(result => { this.me = result; });
		}
  
  	public ngOnInit() {
		this.debug.addInfo('TemplateListComponent', 'OnInit');
		this.getTemplates();
	}

	private getTemplates(){
		this.ws.templates().subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.templates', 'successful');
				this.permission = true;
				this.renderDataTable(result.data);
			}
			else {            
				this.debug.addError('WS.templates', result.data);
				this.load = false;
				return null;
			
			}
		}, error => {
			this.debug.addError('WS.templates', error.message);
			this.load = false; 
			return null;          
		}); 	
	}
	  
	public renderDataTable(data: Template []) {
		this.dataSource = new MatTableDataSource(data);
		this.dataSource.filterPredicate = (data: Template, filter: string): boolean => {
			const dataStr = MYString.removeAccents(Object.keys(data).reduce((currentTerm: string, key: string) => {
			  return (currentTerm + (data as { [key: string]: any })[key] + '◬');
			}, '')).trim().toLowerCase();	  
			return dataStr.indexOf(MYString.removeAccents(filter.trim().toLowerCase())) != -1;
		};
		this.dataSource.paginator = this.paginator;
		this.sortedData = data.slice();
		this.templates = data;
		this.dataSource.sort = this.sort;
		this.load = false;
		this.show();
	}

	public isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	public masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource.data.forEach(row => this.selection.select(row));
	}

	public checkboxLabel(row?: Template): string {
		if (!row) {
		  return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
		}
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idtemplate + 1}`;
	}	

	public delete(){
		if(this.selection.selected.length == 0){
			this.dialog.warning(this.translate.instant('Aviso'), this.translate.instant('Debe seleccionar al menos una plantilla')).subscribe();
		}else{
			this.dialog.confirm(this.translate.instant('Confirmar Archivado'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
				if (confirm){
					this.selection.selected.forEach(element => {
						this.ws.deleteTemplate(element.idtemplate).subscribe((result: IResult) => {
							if(result.success) {
								this.debug.addInfo('WS.deleteApp', 'successful');
								this.openSnackBar('Plantilla borrada correctamente');
								this.getTemplates();
							}
							else {            
								this.debug.addError('WS.deleteTemplate', result.data);	
								this.openSnackBar(result.data);			
							}
						}, error => {
							this.debug.addError('WS.deleteUser', error.message);
							this.openSnackBar(error.message);	           
						}); 
					})			  
				}
			});
		}		
	}

	public update(){
		if (this.selection.selected.length>1){
			this.dialog.warning('Aviso', 'Sólo puede editar una plantilla').subscribe();
		}else if(this.selection.selected.length == 0){
			this.dialog.warning('Aviso', 'Debe seleccionar una plantilla').subscribe();
		}else{
			this.router.navigateByUrl('/private/templates/edit/'+this.selection.selected[0].idtemplate);
		}
	}

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

	public show(){		 
		this.me.permissions.forEach(element => {
			if(element.name == "template.put.*" || element.name == "*.*.*"){
			  this.new = true;
			}
			if(element.name == "template.delete.*" || element.name == "*.*.*"){
			  this.edit = true;
			}
			if(element.name == "template.post.*" || element.name == "*.*.*"){
			  this.deleted = true;
			}

		})
	}
	
	public applyFilter(event: string) {
		if(typeof event === 'string'){
			this.dataSource.filter = event;
		
			if (this.dataSource.paginator) {
				this.dataSource.paginator.firstPage();
			}
		}
	}

	ngAfterViewInit (){
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}

	sortData(sort: Sort) {
		const data = this.templates.slice();
		if (!sort.active || sort.direction === '') {
		  this.sortedData = data;
		  return;
		}
	
		this.sortedData = data.sort((a, b) => {
		  const isAsc = sort.direction === 'asc';
		  switch (sort.active) {
			case 'idtemplate': return this.compare(a.idtemplate, b.idtemplate, isAsc);
			case 'name': return this.compare(a.name, b.name, isAsc);
			case 'html': return this.compare(a.html, b.html, isAsc);			
			default: return 0;
		  }
		});
		this.dataSource.data = this.sortedData;
	}
	compare(a: number | string, b: number | string, isAsc: boolean) {
		return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
	}
}
