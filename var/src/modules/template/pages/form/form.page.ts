import { Component, ViewChild } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Template } from 'src/modules/template/models/template';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent, CKEditorComponent } from '@ckeditor/ckeditor5-angular';

@Component({
  selector: 'template-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss']
})

export class TemplateFormPage {

	public id: number|null;
	public form: FormGroup;
	public name: FormControl = new FormControl('');
	public html: FormControl = new FormControl('');	
	public loading: boolean = false;
	public template: Template;
	public sHtml: string;

	public Editor = ClassicEditor;
	@ViewChild(CKEditorComponent) ckeditor : CKEditorComponent;

   	constructor(
		private ws: WSService,
		private debug: DebugStore,
		private router: Router,
		private route: ActivatedRoute,
		private fb: FormBuilder,
		private _snackBar: MatSnackBar) {
			this.id = Number(this.route.snapshot.paramMap.get('idtemplate'));
  	} 
  
  	public ngOnInit() {
		this.debug.addInfo('TemplateFormComponent', 'OnInit');
		this.initForm(null);
		if (this.id){
			this.ws.template(this.id).subscribe((result: IResult) => {
				if(result.success) {
					this.debug.addInfo('WS.template', 'successful');
					this.template = result.data;
					this.sHtml = result.data.html;					
					this.initForm(result.data);	
				}
				else {            
					this.debug.addError('WS.template', result.data);
				
				}
			}, error => {
				this.debug.addError('WS.template', error.message);  
				  
			}); 
		}
	}  

	private initForm(data: Template|null){		
		if (!data){
			this.form = this.fb.group({
				name: this.name,
				html: this.html
			});
		  }else{		
			this.form = this.fb.group({
				name: data.name,
				html: data.html		
			});
		  } 
	}		

	public create(){
		if(this.form.invalid)
			return null;
		this.loading = true;
		this.ws.createTemplate(this.form.value).subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.createTemplate', 'successful');
				this.openSnackBar("Plantilla creada correctamente");
				this.router.navigateByUrl('/private/template/list');
				this.loading = false;
			}
			else {            
				this.debug.addError('WS.createTemplate', result.data);
				this.openSnackBar(result.data);
				this.loading = false;			
			}
		}, error => {
			let msj = JSON.stringify(error.error.data);
			this.openSnackBar(msj.substr(2,msj.length-3)); 
			this.debug.addError('WS.createTemplate', error.message);
			this.loading = false;          
		}); 
	}

	public update(){
		if (this.form.invalid) return null;
		this.loading = true;
		this.ws.updateTemplate(this.id, this.form.value).subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.updateTemplate', 'successful');
				this.openSnackBar("Plantilla actualizada correctamente");
				this.router.navigateByUrl('/private/template/list');
				this.loading = false;
			}
			else {            
				this.debug.addError('WS.updateTemplate', result.data);
				this.openSnackBar(result.data);
				this.loading = false;
			
			}
		}, error => {
			let msj = JSON.stringify(error.error.data);
			this.openSnackBar(msj.substr(2,msj.length-3)); 
			this.debug.addError('WS.updateTemplate', error.message);  
			this.loading = false;          
		}); 
	}	

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}	

	public onChange( { editor }: ChangeEvent ) {
        const data = editor.getData();
		this.form.get('html').patchValue(data);
    }
}
