import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexModule } from '@angular/flex-layout';
import { PipesModule } from 'src/pipes/pipes.module';

import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import { TemplateRoutingModule } from './template-routing.module';
import { TemplateDialog } from './dialogs/template/template.dialog';
import { TemplateStore } from './stores/template.store';
import { TemplateFormPage } from './pages/form/form.page';
import { TemplateListPage } from './pages/list/list.page';

import { CONFIG } from 'src/app/app.config';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import moment from 'moment';

export function HttpLoaderFactory(http: HttpClient) {
  let url = CONFIG.lang_url;  
  return new TranslateHttpLoader(http, `${url}/i18n/`, `.json?timestamp=${moment().valueOf()}`);
}
@NgModule({
  declarations: [TemplateDialog, TemplateFormPage, TemplateListPage],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    TemplateRoutingModule,
    MaterialModule,
    PipesModule
  ],
  entryComponents:[TemplateDialog],
  providers: [{ provide: 'Window',  useValue: window }, TemplateStore],
  exports: [TemplateDialog]
})

export class TemplateModule {
  constructor(private translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('es');
    if(!sessionStorage.getItem('lang')){
      sessionStorage.setItem('lang', 'es');
      translate.use('es');
    }else{
      translate.use(sessionStorage.getItem('lang'));
    }
  }
}
