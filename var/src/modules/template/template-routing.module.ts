import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TemplateFormPage } from './pages/form/form.page';
import { TemplateListPage } from './pages/list/list.page';

const routes: Routes = [
  { path: 'list', component: TemplateListPage},
  { path: 'create', component: TemplateFormPage, data: { breadcrumb: 'Nueva plantilla' }},
  { path: 'edit/:idtemplate', component: TemplateFormPage, data: { breadcrumb: 'Editar plantilla' }}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TemplateRoutingModule { }
