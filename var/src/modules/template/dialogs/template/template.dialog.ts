import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { CONFIG } from 'src/app/app.config';
import { Asset } from 'src/modules/asset/models/asset';
import { UserStore } from 'src/modules/user/stores/user.store';

@Component({
	selector: 'template-dialog',
  	templateUrl: './template.dialog.html',
  	styleUrls: ['./template.dialog.scss']
})

export class TemplateDialog {
    public me: any;
    public url: string = `${CONFIG.template}/plantilla.php`;

	constructor(
        @Inject(MAT_DIALOG_DATA) public data: Asset,
        public dialogRef: MatDialogRef<TemplateDialog>,
        private ustore: UserStore,
        private sanitizer: DomSanitizer
        ){
        window.addEventListener("message", (message: any) => {
            if (message.data.message==="close")
                dialogRef.close(null);
            else{
                let asset_url: string = message.data;
                dialogRef.close({url: asset_url, asset: this.data});
            }
        }, false);
        this.ustore.onChange.subscribe(user => { 
            this.me = user;  
            let env = '';
            if (CONFIG.demo)
                env= 'demo';
            if(!this.data) {
                 this.url=`${CONFIG.template}/plantilla.php?customer=${this.me.customer.idcustomer}&entorno=${env}`;  
             }else{
                      
         
                let urlParts = this.data.asset_url.split('/');

                let id = urlParts[urlParts.length - 1];
                id = id.substring(0,id.length-5);
                this.url=`${CONFIG.template}/plantilla.php?customer=${this.me.customer.idcustomer}&entorno=${env}&json=${id}`;
              
            }
        
              
        });
    }
}