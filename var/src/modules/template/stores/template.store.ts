import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { ITemplate, Template } from 'src/modules/template/models/template';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'templates.store.ids';

@Injectable()
export class TemplateStore {

    public onChange: BehaviorSubject<Template[]> = new BehaviorSubject([]);
    private data: Template[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const templates: ITemplate[] = JSON.parse(store); 
                this.set(templates);
            } else 
                this.load();
        } else
            this.load();
    }

    public get(): Template[] {
        return this.data;
    }

    public set(templates: ITemplate[]) {
        this.save(templates);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.templates().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }
        });
    }

    private save(templates: ITemplate[]) {
        this.data = templates.map(t => new Template(t));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(templates));        
    }
}