export interface ITemplate {
  idtemplate: number;
  name: string;
  html?: string
}

export class Template implements ITemplate  {
  idtemplate: number;
  name: string;
  html: string;   

	constructor(obj?: any) {
		if(!obj)
      return;

    this.idtemplate = obj.idtemplate;
    this.name = obj.name;
    this.html = obj.html;   
  }
}
