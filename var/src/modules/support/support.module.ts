import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexModule } from '@angular/flex-layout';
import { HttpService } from 'src/services/http.service';

import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from 'src/app/material.module';

import { SupportRoutingModule } from './support-routing.module';

import { NotificationSupportPage } from './pages/notification/notification.page';
import { CameraSupportPage } from './pages/camera/camera.page';
import { PlayerSupportPage } from './pages/player/player.page';

@NgModule({
  declarations: [NotificationSupportPage, CameraSupportPage, PlayerSupportPage],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SupportRoutingModule
  ],
  providers: [HttpService],
  exports:[]
})
export class SupportModule {}