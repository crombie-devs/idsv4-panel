import { Component } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { CONFIG } from 'src/app/app.config';
import { UserStore } from 'src/modules/user/stores/user.store';


@Component({
	selector:'player-support',
	templateUrl: './player.page.html',
	styleUrls: ['./support.page.scss']
})

export class PlayerSupportPage {

	public me: any;
	public url: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.support_url}/players/customers`);

	constructor(
		private ustore: UserStore,
		private sanitizer: DomSanitizer) {
			
			this.ustore.onChange.subscribe(user => { 
				this.me = user; 
			});				
  	}

	public ngOnInit() {
		if(this.me.customer)		
			this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.support_url}/players/summary/${this.me.customer.idcustomer}/${this.me.customer.name}`);
		

	}
}

