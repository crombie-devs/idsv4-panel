import { Component } from '@angular/core';
import { UserStore } from 'src/modules/user/stores/user.store';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { CONFIG } from 'src/app/app.config';

@Component({
  	templateUrl: 'notification.page.html',
  	styleUrls: ['./support.page.scss']
})

export class NotificationSupportPage {
	public me: any;
	public url: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.support_url}/players/summary`);
	constructor(
		private ustore: UserStore,
		private sanitizer: DomSanitizer) {
			this.ustore.onChange.subscribe(user => { 
				this.me = user; 
			});				
  	}
	public ngOnInit() {
		if(this.me.customer)		
			this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.support_url}/players/notifications/${this.me.customer.idcustomer}/${this.me.customer.name}`);
	}
}