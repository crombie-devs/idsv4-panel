import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//import { NotificationSupportPage } from './pages/notification/notification.page';
import { CameraSupportPage } from './pages/camera/camera.page';
import { PlayerSupportPage } from './pages/player/player.page';

const routes: Routes = [
  //{ path: 'notifications', component: NotificationSupportPage, data: { breadcrumb: 'Notificaciones' }},
  { path: 'cameras', component: CameraSupportPage, data: { breadcrumb: 'Cámaras' }},
  { path: 'players', component: PlayerSupportPage, data: { breadcrumb: 'Visores' }},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class SupportRoutingModule { }
