import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Auth, IAuth } from "src/modules/auth/models/auth";

const STORE_KEY: string = 'auth.store.ids';

@Injectable()
export class AuthStore {

	public auth: Auth = null;
	public onChange: BehaviorSubject<Auth> = new BehaviorSubject(null);

	constructor() {
		this.load();
	}

	private load() {
		let store = localStorage.getItem(STORE_KEY) ? localStorage.getItem(STORE_KEY) : sessionStorage.getItem(STORE_KEY);
		if(store) {
			const data: IAuth = JSON.parse(store);
			this.set(data, localStorage.getItem(STORE_KEY) ? true : false);
		}
	}

	private save(data: IAuth, remember:boolean) {
		this.auth = (data) ? new Auth(data) : null;
		if(remember)
			localStorage.setItem(STORE_KEY, JSON.stringify(data));
		else
			sessionStorage.setItem(STORE_KEY, JSON.stringify(data));
	}

	public get(): Auth {
		return this.auth;
	}

	public set(data: IAuth, remember:boolean) {
		this.save(data, remember);
		this.onChange.next(this.auth);
	}
	
  	public isLogged() {
  		return (this.auth != null && this.auth != undefined);
	}

	public delete() {
		this.save(null, false);
		this.onChange.next(this.auth);
	}

	public clear(){
		this.delete();
		localStorage.clear();
	}
}
