export interface IAuth {
  access_token: string;
  token_type:string;
  expires_at: Date;
}

export class Auth implements IAuth {
  access_token: string;
  token_type:string;
  expires_at: Date;  

	constructor(obj?: IAuth) {
		if(!obj)
      return;
    this.access_token = obj.access_token;
    this.token_type = obj.token_type;
    this.expires_at = obj.expires_at;
  }
}
