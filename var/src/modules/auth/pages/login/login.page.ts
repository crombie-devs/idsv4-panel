import { Component, Input, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { APP, VERSION } from 'src/app/app.config';
import { Router, UrlTree } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { IResult } from 'src/models/iresult';
import {TranslateService} from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CookieService } from 'ngx-cookie-service';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { IAuth } from 'src/modules/auth/models/auth';
import { UserStore } from 'src/modules/user/stores/user.store';
import { RecoveryDialog } from 'src/modules/auth/dialogs/recovery/recovery.dialog';
import { ErrorDialog } from 'src/modules/auth/dialogs/error/error.dialog';

@Component({
  selector: 'login-page',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})

export class LoginPage {

	public name: FormControl = new FormControl(null, [Validators.required]);
	public password: FormControl = new FormControl(null, [Validators.required]);
	public remember: FormControl = new FormControl(false);

	@ViewChild('v') v: ElementRef<HTMLVideoElement>;
	
	public loading: boolean = false;
	public error: boolean = false;
	public app = APP;
	public version = VERSION;

	public hide = true;
	public c_hide = true;
	public loadingRecovery: boolean = false;
	public errorRecovery: boolean = false;
	public form: FormGroup;
	private url: UrlTree;
	public token: string;
	private email: string
	c_password: FormControl = new FormControl('', [Validators.required]);

  	constructor(
		private router: Router,
		private ws: WSService,
		private debug: DebugStore,	
		private astore: AuthStore,
		private ustore: UserStore,
		private translate: TranslateService,
		private _snackBar: MatSnackBar,
		private fb: FormBuilder,
		private matDialog: MatDialog,
		private store: AuthStore,
		private localStorage: MYLocalStorage,
		private cookieService: CookieService) {
			this.url = this.router.parseUrl(this.router.url);
			this.token = this.url.queryParams['token'];
			this.email = this.url.queryParams['email'];
			if(this.token)
				this.form = this.fb.group({
					token: this.token,
					email: this.email,
					password: '',	
					c_password: ''
				},{validators: passwordMatchValidator});	
  	}
  
  	public ngOnInit() {
		this.debug.addInfo('LoginPage', 'OnInit');
	}
	ngAfterViewInit(): void {
		this.v.nativeElement.muted = true;
		this.v.nativeElement.play();
	}
  	public requestRegister() {
		if(this.name.invalid)
			return;
		this.loading = true;
		this.error = false;
		this.ws.login(this.name.value, this.password.value, this.remember.value).subscribe((result: IResult) => {
			if(result.success) {
				localStorage.clear();
				this.saveAuth(result.data.data);
			} else {
				this.loading = false;
				this.error = true;
                this.matDialog.open(ErrorDialog);
			}
		}, error => {
            this.matDialog.open(ErrorDialog);
			this.loading = false;
			this.error = true;
		});
	}

	private saveAuth(data: IAuth) {
		this.astore.set(data, this.remember.value);
		this.jumpToNextPage();
	}

	private jumpToNextPage() {
		this.router.navigate(['private/dashboard']);
	}
	  
	public selectLanguage(lang:string){
		this.translate.use(lang);
		sessionStorage.setItem('lang', lang);
	}
	public recover(){
		this.matDialog.open(RecoveryDialog);
	}

	private initForm(){	
		this.form = this.fb.group({
			token: this.token,
			email: this.email,
			password: '',	
			c_password: ''
		},{validators: passwordMatchValidator});	 
	}	

	public requestRecovery(){
		if (this.form.valid){
			this.loadingRecovery = true;
			this.errorRecovery = false;
			this.ws.updatePassword(this.form.value).subscribe((result: IResult) => {
				if(result.success) {
					this.token = null;
					this.openSnackBar("Contraseña cambiada correctamente");
				}
				else {
					this.loadingRecovery = false;
					this.errorRecovery = true;
				
				}
			}, error => {
				this.loadingRecovery = false;
				this.errorRecovery = true;				
			});
		}
	}

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

	public logout() {  
		this.ustore.delete();
		this.store.clear();  
		this.localStorage.clear();
		let page_size = this.cookieService.get('page-size');
		this.cookieService.deleteAll();
		this.cookieService.set('page-size', page_size);
	}	
}
export const passwordMatchValidator: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
	if (formGroup.get('password').value === formGroup.get('c_password').value)
	  return null;
	else
	  return {passwordMismatch: true};
  };
