import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';


@Component({
	selector: 'user-error-dialog',
  	templateUrl: './error.dialog.html',
  	styleUrls: ['./error.dialog.scss']
})

export class ErrorDialog {


    constructor(public dialogRef: MatDialogRef<ErrorDialog>){}  

    close(){
        this.dialogRef.close(null);
    }
   

}