import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';

@Component({
	selector: 'user-recovery-dialog',
  	templateUrl: './recovery.dialog.html',
  	styleUrls: ['./recovery.dialog.scss']
})

export class RecoveryDialog {
    is_send: boolean = false;
    is_error: boolean = false;
    is_error_email: boolean = false;

    public form: FormGroup;
    public email: FormControl = new FormControl('', [Validators.required,Validators.email]);	

	constructor(
        public dialogRef: MatDialogRef<RecoveryDialog>, 
        private ws: WSService,
        private fb: FormBuilder,
        private debug: DebugStore){}  

    public ngOnInit() {
        this.initForm();
    }

    private initForm(){
        this.form = this.fb.group({
            email: ''
        });
    }

    public send(){        
        this.is_send = false;
        this.is_error_email = false;
        this.is_error = false;
        this.ws.recovery(this.form.value).subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.recovery', 'successful');
				this.is_send = true;
			} else {            
                this.debug.addError('WS.recovery', result.data);
                this.is_error_email = true;
				return null;
			}
		}, error => {
			this.debug.addError('WS.recovery', error.message);  
			this.is_error = true;
			return null;          
		});	
    }
   
    public close() {
        this.dialogRef.close(null);
    }
}