import { ISite, Site } from "src/modules/site/models/site";
import { Customer, ICustomer } from 'src/modules/customer/models/customer';
import { Power } from 'src/modules/power/models/power';

export interface ICamera {
    idcamera: number;
    name: string;
    email: string;
    code: string;
    site?: ISite;
    customer?: ICustomer;
    status?: boolean;
    powers: Power[];
    idsite: number;
    face_timeout: number;
    section_start_x: number,
    section_start_y: number,
    section_width: number,
    section_height: number,
    face_size: number,
    face_threshold: number,
    cosine_threshold: number,
    embedding_threshold: number,
    is_monitored: number

}

export class Camera implements ICamera {
    idcamera: number;
    name: string;
    email: string;
    code: string;
    site?: Site;
    customer?: Customer;
    status?: boolean;
    powers: Power[];
    idsite: number;
    face_timeout: number;
    section_start_x: number;
    section_start_y: number;
    section_width: number;
    section_height: number;
    face_size: number;
    face_threshold: number;
    cosine_threshold: number;
    embedding_threshold: number;
    is_monitored: number;


    facesToday?: number;
    facesYesterday?: number;

    constructor(obj?: ICamera) {
        if (!obj)
            return;

        this.idcamera = obj.idcamera;
        this.name = obj.name;
        this.email = obj.email;
        this.code = obj.code;
        this.site = new Site(obj.site);
        this.idsite = obj.idsite;
        this.customer = new Customer(obj.customer);
        this.status = obj.status;
        this.face_timeout = obj.face_timeout;
        this.section_start_x = obj.section_start_x;
        this.section_start_y = obj.section_start_y
        this.section_width = obj.section_width;
        this.section_height = obj.section_height;
        this.face_size = obj.face_size;
        this.face_threshold = obj.face_threshold;
        this.cosine_threshold = obj.cosine_threshold;
        this.embedding_threshold = obj.embedding_threshold;
        this.is_monitored = obj.is_monitored;

        this.facesToday = 0;
        this.facesYesterday = 0;
        if (obj.powers)
            this.powers = obj.powers.map(p => new Power(p));
    }
}
