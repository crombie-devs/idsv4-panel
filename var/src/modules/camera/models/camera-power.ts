export interface ICameraPower {
    weekday: number;
    time_on: string;
    time_off: string;
}

export class CameraPower implements ICameraPower {
    weekday: number;
    time_on: string;
    time_off: string;
    day: string;
    
    constructor(obj?: any) {
        if(!obj)
            return;
        this.weekday = obj.weekday;
        this.time_on = obj.time_on;
        this.time_off = obj.time_off;
        this.day = this.getDay(this.weekday);
    }

    getDay(weekday: number): string{
        if (weekday == 1)
            return 'Domingo';
        if (weekday == 2)
            return 'Lunes';
        if (weekday == 3)
            return 'Martes';
        if (weekday == 4)
            return 'Miércoles';
        if (weekday == 5)
            return 'Jueves';
        if (weekday == 6)
            return 'Viernes';
        if (weekday == 7)
            return 'Sábado';

    }
}