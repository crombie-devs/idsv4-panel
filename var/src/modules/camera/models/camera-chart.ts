export class CameraChart {

  email: string;
  gender: string;
  age: number;
  month: number;
  camera_id: number;
  type: string;
  site_id: number;
  customer_id: number;
  day: number;
  hour: string;
  date: Date
  count: number;  

  constructor(obj?: any) {
    if(!obj)
      return;

    this.gender = obj.gender;
    this.age = obj.age;
    this.email = obj.email;
    this.month = obj.month;
    this.camera_id = obj.camera_id;   
    this.type = obj.type;  
    this.site_id = obj.site_id;
    this.customer_id = obj.customer_id;
    this.hour = obj.hour;
    this.day = obj.day;
    this.date = obj.date;
    this.count = obj.count;
  }

 
}