import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CameraDetailPage } from 'src/modules/camera/pages/detail/detail.page';
import { CameraListPage } from 'src/modules/camera/pages/list/list.page';
import { CameraFormPage } from 'src/modules/camera/pages/form/form.page';

const routes: Routes = [
  { path: 'camera/detail/:email/:streaming', component: CameraDetailPage, data: { breadcrumb: 'detalle de cámara' }},
  { path: 'camera/list', component: CameraListPage },
  { path: 'camera/create', component: CameraFormPage, data: { breadcrumb: 'Crear cámara' }},
  { path: 'camera/edit/:idcamera', component: CameraFormPage, data: { breadcrumb: 'Editar cámara' }}
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CameraRoutingModule { }
