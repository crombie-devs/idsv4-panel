import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IResult } from 'src/models/iresult';
import { CameraService } from 'src/modules/camera/services/camera.service';
import { ICamera, Camera } from 'src/modules/camera/models/camera';
import { CONFIG } from 'src/app/app.config';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'cameras.store.ids';

@Injectable()
export class CameraStore {

    public onChange: BehaviorSubject<Camera[]> = new BehaviorSubject([]);
    private data: Camera[] = [];
    public defaultCamera: Camera = {
        idcamera: 82,
        email: 'icreative@ladorian.com',
        code: '',
        name: '',
        powers: [],
        idsite: null,
        face_timeout: 0,
        section_start_x: 0,
        section_start_y: 0,
        section_width: 0,
        section_height: 0,
        face_size: 0,
        face_threshold: 0,
        cosine_threshold: 0,
        embedding_threshold: 0,
        is_monitored: 0

    };

    constructor(private ws: CameraService) {
        if (environment.localstorage) {
            let store = sessionStorage.getItem(STORE_KEY);
            if (store && !CONFIG.demo) {
                const cameras: ICamera[] = JSON.parse(store);
                this.set(cameras);
            } else {
                this.load();
            }
        } else
            this.load();
    }

    public get(): Camera[] {
        return this.data;
    }

    public set(cameras: ICamera[]) {
        this.save(cameras);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        if (!CONFIG.demo)
            this.ws.toSelect().subscribe((cameras: Camera[]) => {
                if (cameras) {
                    this.set(cameras);
                }
            });
        else
            this.set([this.defaultCamera])
    }

    private save(cameras: ICamera[]) {
        this.data = cameras.map(a => new Camera(a));
        if (environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(cameras));
    }

}
