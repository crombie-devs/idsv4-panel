import { CameraAfluenceComponent } from "./afluence/camera.afluence.component";
import { CameraHeatmapComponent } from "./heatmap/camera.heatmap.component";
import { CameraStreamingComponent } from './streaming/camera.streaming.component';
import { CameraStreamingHlsComponent } from './streaming-hls/camera.streaming-hls.component';
import { CameraStreamingFlvComponent } from './streaming-flv/camera.streaming-flv.component';
import { CameraGenderHourComponent } from './gender-hour-chart/gender-hour.component';
import { CameraUserTypeHourComponent } from './user-type-hour-chart/user-type-hour.component';
import { CameraGenderComponent } from './gender-pie/gender.component';
import { CameraUserTypeComponent } from './user-type-pie/user-type.component';
import { CameraSelectComponent } from './select/select.component';

export const CAMERA_COMPONENTS = [
	CameraAfluenceComponent,
	CameraHeatmapComponent,
    CameraStreamingComponent,
    CameraStreamingHlsComponent,
    CameraGenderHourComponent,
    CameraUserTypeHourComponent,
    CameraGenderComponent,
    CameraUserTypeComponent,
    CameraSelectComponent,
    CameraStreamingFlvComponent
];