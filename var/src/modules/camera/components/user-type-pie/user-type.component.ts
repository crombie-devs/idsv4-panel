import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import moment from 'moment';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { IResult } from 'src/models/iresult';

@Component({
  selector: 'camera-user-type-pie',
  templateUrl: './user-type.component.html',
  styleUrls: ['./user-type.component.scss']
})
export class CameraUserTypeComponent {

    @Input() aspectRatio: number;
    @Input() height:number;
    @Input() email:string;
    @Input() factorCorrector:number;
    public load: boolean = true;

    public pieChartOptions: ChartOptions = {
        responsive: true,
    };
    public typePieChartLabels: Label[] = ['Adultos', 'Jóvenes', 'Senior'];
    public typePieChartData: SingleDataSet = [0,0,0,0];
    public pieChartType: ChartType = 'pie';
    public pieChartLegend = true;
    public pieChartPlugins = [];
    public isChecked = false;
    public resultData : any[] = []

    constructor(
        private ws: WSService,
        private dialog: DialogService,
      ) {}
    
      public ngOnInit() {
        this.syncViewer();
      }
    
      private syncViewer() {
        this.renderDataTable();
      }
    
      public renderDataTable() {
        if(this.resultData.length === 0) 
          this.getEmision();         
      }
    
      getEmision(day = null){
        let today:string;
        let yesterday: string;
        if(!day){
          today = moment().format("YYYY-MM-DD"); 
          yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");
        } else{
          today = moment(day).format("YYYY-MM-DD");
          yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");
        }
        let params:any = {
          filter:{
            date: today,
            email: this.email
          },
          group:{
            _id:{ age: "$age"},
            count: {"$sum": 1}
          }
        }
        this.ws.detailFacesByCamera(params).subscribe((result: IResult) => {
          if(result.success) { 
            this.resultData = result.data;
            this.processAllEmision(); 
            this.load = false;
          }else {
            this.dialog.warning('emisionCustomer', result.data);
            this.load = false;   
          }
        });
      } 
    
      private processAllEmision(day:string = 'today'){
        const elements = this.resultData;
        let afluencias = [];
        afluencias['typePie'] = [];
        let adultos = elements.filter(emision  => emision._id.age > 20 && emision._id.age <= 50);
        let adultosCount = 0;
        let joven = elements.filter(emision  => emision._id.age <= 20);
        let jovenCount = 0;
        let senior = elements.filter(emision  => emision._id.age > 50);
        let seniorCount = 0;
        adultos.forEach(data => adultosCount += data.count);
        joven.forEach(data => jovenCount += data.count);
        senior.forEach(data => seniorCount += data.count);

        afluencias['typePie']['adultos'] = adultosCount / this.factorCorrector;
        afluencias['typePie']['joven'] = jovenCount / this.factorCorrector;
        afluencias['typePie']['senior'] = seniorCount / this.factorCorrector;
        
        this.typePieChartData = [afluencias['typePie']['adultos'], afluencias['typePie']['joven'], afluencias['typePie']['senior']];
    }
      private groupBy(elements: any[], field: string){
        let result = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
        for( let i = 0; i < 24; i++){
          elements.map(element => {
            if(element._id.hour === i){
              result[i] += element.count;
            }
          });
        }
    
        for(let i = 0; i < 24; i++){
          if(!result[i])
            result[i] = 0;
        }
        let res:number[] = [];
        result.map(r => res.push(r/this.factorCorrector));
        return res;
      }
}