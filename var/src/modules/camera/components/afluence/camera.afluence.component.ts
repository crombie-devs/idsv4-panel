import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { SocketService } from 'src/services/socket.service';
import { WSService } from 'src/services/ws.service';
import { Afluence } from 'src/modules/flow/models/afluence';
import moment from 'moment';

@Component({
  selector: 'camera-afluence',
  templateUrl: './camera.afluence.component.html',
  styleUrls: ['./camera.afluence.component.scss']
})
export class CameraAfluenceComponent implements OnDestroy, OnChanges {

    loading: boolean = true;
    afluences: Afluence[] = [];

    @Input() email: string;
    @Output() onChange: EventEmitter<Afluence> = new EventEmitter();

    constructor(private socket: SocketService, private ws: WSService) {}

    ngOnDestroy() {
        this.socket.onConnect.subscribe(connected => {
            if(!connected){ return; }
            this.socket.leave(`client-${this.email}`);
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if(changes.email) {
            this.getLastAfluences();
            this.initialize();
        }
    }

    private initialize() {
        this.socket.onConnect.subscribe(connected => {
            if(!connected){ return; }
            this.socket.join(`client-${this.email}`);
        });

        this.socket.onMessage.subscribe(message => {
            if(message.name == 'send:afluencia') {
                let peoples: Afluence[] = message.data.response.map(p => new Afluence(p));
                peoples.forEach(data => {
                    this.addAfluence(data); 
                });
            }
        });
    }

    private getLastAfluences() {
        const today = moment().format('YYYY-MM-DD');
        const filter: object = {email: this.email, date: today};
        this.ws.detailFacesByCamera({filter: filter}).subscribe(result => {
            this.loading = false;
            if(result.success)
                this.afluences = result.data.slice(-4).map(p => new Afluence(p));
        })
    }

    private addAfluence(data: Afluence) {
        if(this.afluences.length > 4) { this.afluences.shift(); }
        this.afluences.push(data);
        this.onChange.emit(data);
    }
}