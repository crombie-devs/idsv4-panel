import { AfterViewInit, Component, ElementRef, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { CONFIG } from 'src/app/app.config';

import * as h337 from 'heatmap.js';

const gradientCfg = { // heatmap gradient color range
    '0.15': '#6ad180', // green
    '0.25': '#7cd573',
    '0.35': '#90d865',
    '0.45': '#a4da57',
    '0.55': '#badc48',
    '0.65': '#c9cf35',
    '0.75': '#d6c226',
    '0.80': '#e2b41c',
    '0.85': '#e2961d',
    '0.90': '#dd7826',
    '0.95': '#d25c30',
    '1.0': '#c24039' // highest red
};

@Component({
  selector: 'camera-heatmap',
  templateUrl: './camera.heatmap.component.html',
  styleUrls: ['./camera.heatmap.component.scss']
})
export class CameraHeatmapComponent implements AfterViewInit {

    @ViewChild('heatmap') heatmap: ElementRef;
    @Input() email: string;

    private canvas: any;

    constructor(private ws: WSService){}

    ngAfterViewInit() {
      this.load();
    }

    private load() {
      let now: Date = new Date();
      let year = now.getFullYear();
      let month = now.getMonth() + 1;
      if(CONFIG.demo){
        year = 2021;
        month = 3;
      }
      let params: any = {
        filter: {email: this.email, year: year, month: month},
        group: {
          _id:{ x: "$positions.x", y: "$positions.y" },
          count: {"$sum": 1}
        },
        unwind: "$positions"
      }
      this.ws.filterAndGroupPeople(params).subscribe(result => {
        if(result.success)
          this.paint(result.data);
      });
    }

    @HostListener('window:resize', ['$event']) 
    onResize(event) { 
        this.canvas.repaint();
    }


    private paint(positions){
        if(!this.heatmap){ return; }
        const config = { 
          container: this.heatmap.nativeElement,
          opacity: .7,
          radius: 50,
          visible: true,
          gradient: gradientCfg,
          backgroundColor: 'inherit',
        };

        const width = this.heatmap.nativeElement.offsetWidth;
        const height = this.heatmap.nativeElement.offsetHeight;
        let coords = positions.map(p => { return {x: Math.round((p._id.x * width)/640), y: Math.round((p._id.y * height)/480), value: p.count}; });
        let counts: number[] = positions.map(p => p.count);
        let max: number = Math.max.apply(null, counts);
        this.canvas = h337.create(config);
        this.canvas.setData({ max: max, data: coords });
      }

}