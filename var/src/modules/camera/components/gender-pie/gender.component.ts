import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import moment from 'moment';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { IResult } from 'src/models/iresult';

@Component({
  selector: 'camera-gender-pie',
  templateUrl: './gender.component.html',
  styleUrls: ['./gender.component.scss']
})
export class CameraGenderComponent {

    @Input() aspectRatio: number;
    @Input() height:number;
    @Input() email:string;
    @Input() factorCorrector:number;
    public load: boolean = true;

    public pieChartOptions: ChartOptions = {
        responsive: true,
    };
    public genderPieChartLabels: Label[] = ['Hombres', 'Mujeres'];
    public genderPieChartData: SingleDataSet = [0,0];
    public pieChartType: ChartType = 'pie';
    public pieChartLegend = true;
    public pieChartPlugins = [];
    public isChecked = false;
    public resultData : any[] = []

    constructor(
        private ws: WSService,
        private dialog: DialogService,
      ) {}
    
      public ngOnInit() {
        this.syncViewer();
      }
    
      private syncViewer() {
        this.renderDataTable();
      }
    
      public renderDataTable() {
        if(this.resultData.length === 0) 
          this.getEmision();         
      }
    
      getEmision(day = null){
        let today:string;
        let yesterday: string;
        if(!day){
          today = moment().format("YYYY-MM-DD"); 
          yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");
        } else{
          today = moment(day).format("YYYY-MM-DD");
          yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");
        }
        let params:any = {
          filter:{
            date: today,
            email: this.email
          },
          group:{
            _id:{ gender: "$gender"},
            count: {"$sum": 1}
          }
        }
        this.ws.detailFacesByCamera(params).subscribe((result: IResult) => {
          if(result.success) { 
            this.resultData = result.data;
            this.processAllEmision(); 
            this.load = false;
          }else {
            this.dialog.warning('emisionCustomer', result.data);
            this.load = false;   
          }
        });
      } 
    
      private processAllEmision(day:string = 'today'){
        const elements = this.resultData;
        let afluencias = [];
        afluencias['genderPie'] = [];
        afluencias['genderPie']["hombres"] = Math.round(elements.filter(emision  => emision._id.gender === 'male')[0].count / this.factorCorrector);
        afluencias['genderPie']["mujeres"] = Math.round(elements.filter(emision => emision._id.gender === 'female')[0].count / this.factorCorrector);
        
        this.genderPieChartData = [afluencias["genderPie"]['hombres'],afluencias["genderPie"]['mujeres']];
      }
      private groupBy(elements: any[], field: string){
        let result = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
        for( let i = 0; i < 24; i++){
          elements.map(element => {
            if(element._id.hour === i){
              result[i] += element.count;
            }
          });
        }
    
        for(let i = 0; i < 24; i++){
          if(!result[i])
            result[i] = 0;
        }
        let res:number[] = [];
        result.map(r => res.push(r/this.factorCorrector));
        return res;
      }
}