import { OnChanges } from '@angular/core';
import { AfterViewInit, Component, ElementRef, HostListener, Input, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ISocketMessage, SocketService } from 'src/services/socket.service';
import { WSService } from 'src/services/ws.service';
import { CONFIG } from 'src/app/app.config';
import { VideoPlayerHlsComponent } from 'src/shared/videoPlayerHls/video-player-hls.component';

@Component({
  selector: 'camera-streaming-hls',
  templateUrl: './camera.streaming-hls.component.html',
  styleUrls: ['./camera.streaming-hls.component.scss']
})
export class CameraStreamingHlsComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('container') container: ElementRef;
    @ViewChild(VideoPlayerHlsComponent) video: VideoPlayerHlsComponent;
    @Input() email: string;
    show: boolean = false;
    loadingStreaming:boolean = false;
    stopped:boolean = false;
    frame: string = null;
    uisHls: boolean;
    url: string;
    src : any;

    constructor(private ws: WSService,private socket: SocketService, private renderer: Renderer2) {}

    ngOnInit() {
        this.socket.onConnect.subscribe(connected => {
            if(!connected){ return; }
            this.connectToCamera();
        });

        this.socket.onMessage.subscribe(message => {
            if(message.name == 'streaming:opened')
                this.loadSrc(message.data);
            else if(message.name == 'streaming:close')
                this.unloadStreaming();
        })
    }

    ngOnDestroy() {
        this.socket.onConnect.subscribe(connected => {
            if(!connected){ return; }
            this.socket.leave(`client-${this.email}`);
        });
    }

    ngAfterViewInit() {
        this.resize();
    }

    openStreaming() {
        this.src = null;
        this.loadingStreaming = true;
        let message: ISocketMessage = { name: 'streaming:open', to: `camera.email.${this.email}`,  from: this.socket.id};
        this.socket.send(message);
    }

    unloadStreaming(){
        this.src = {};
        this.stopped = true;
    }

    loadSrc(src){
        this.src =  { 
            autoplay: true, 
            controls: true, 
            aspectRatio: '16:9',
            responsive: true,
            fluid: true,
            sources: [ { src: src, type: 'application/x-mpegURL' }  ],
            html5: {
                hls: {
                    overrideNative: true
                }
            }};
        this.loadingStreaming = false;
        this.stopped = false;
    }
  
    async connectToCamera() {
        let camera = `camera.email.${this.email}`;
        let email = this.email;
        this.ws.getCamerasFromPusher().subscribe(result => {
            if(result.success){
                const cameras = result.data;
                if(!cameras.some(c => c === camera) && CONFIG.demo){
                    email = 'icreative@ladorian.com';
                }
                this.email = email;
               
                this.socket.join(`client-${email}`);
                this.openStreaming();
            }
        })
    }

    @HostListener('window:resize', ['$event']) 
    onResize(event) { 
        this.resize();
    }

    private resize() {
        if(!this.container){ return; }

        const FRAME_WIDTH = 640;
        const FRAME_HEIGHT = 480;
        
        let width = this.container.nativeElement.offsetWidth;
        let height = (width * FRAME_HEIGHT) / FRAME_WIDTH;

        if(height > 390) {
            height = 390;
            width = (height * FRAME_WIDTH) / FRAME_HEIGHT;
            this.renderer.setStyle(this.container.nativeElement, 'width', `${width}px`);
            this.renderer.setStyle(this.container.nativeElement, 'height', `${height}px`);
        } else {
            width = 'calc(100% - 12px)'
            this.renderer.setStyle(this.container.nativeElement, 'width', width);
            this.renderer.setStyle(this.container.nativeElement, 'height', `${height}px`);
        }
    }
}