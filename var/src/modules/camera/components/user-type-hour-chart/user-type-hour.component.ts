import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import moment from 'moment';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { IResult } from 'src/models/iresult';

@Component({
  selector: 'camera-user-type-hour-chart',
  templateUrl: './user-type-hour.component.html',
  styleUrls: ['./user-type-hour.component.scss']
})
export class CameraUserTypeHourComponent {
    @Input() aspectRatio: number;
    @Input() height:number;
    @Input() email:string;
    @Input() factorCorrector:number;
    public load: boolean = true;

    public typeHourLineChartData: ChartDataSets[] = 
    [
      { data: [0], label: 'Adultos hoy' },
      { data: [0], label: 'Jóvenes hoy' },
      { data: [0], label: 'Senior hoy' },
      { data: [0], label: 'Adultos ayer' },
      { data: [0], label: 'Jóvenes ayer' },
      { data: [0], label: 'Senior ayer' }
    ];

    public typeHourLineChartLabels: Label[] = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
    //Para gráfico en linea
    lineChartOptions = {
        responsive: true,
        scales: {
        // We use this empty structure as a placeholder for dynamic theming.
        xAxes: [{}],
        yAxes: [
            {
            ticks: {
                beginAtZero: true,
                callback: function(value) {if (value % 1 === 0) {return value;}}
            },
            position: 'left',
            }
        ]
        },
    };

    lineChartLegend = true;
    lineChartPlugins = [];
    lineChartType = 'line';
    public resultData : any[] = [];

    constructor(
        private ws: WSService,
        private dialog: DialogService,
      ) {}
    
      public ngOnInit() {
        this.syncViewer();
      }
    
      private syncViewer() {
        this.renderDataTable();
      }
    
      public renderDataTable() {
        if(this.resultData.length === 0) 
          this.getEmision();         
      }
    
    getEmision(day = null){
        let today:string;
        let yesterday: string;
        if(!day){
          today = moment().format("YYYY-MM-DD"); 
          yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");
        } else{
          today = moment(day).format("YYYY-MM-DD");
          yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");
        }
        let params:any = {
          filter:{
            date: {
              "$gte": yesterday,
              "$lte": today
            },
            email: this.email
          },
          group:{
            _id:{ date: "$date",hour:"$hour", age: "$age"},
            count: {"$sum": 1}
          }
        }
        this.ws.detailFacesByCamera(params).subscribe((result: IResult) => {
          if(result.success) { 
            this.resultData = result.data;
            this.processAllEmision(); 
            this.load = false;
          }else {
            this.dialog.warning('emisionCustomer', result.data);
            this.load = false;   
          }
        });
      } 
    
    private processAllEmision(day:string = 'today'){
        const elements = this.resultData;
        let afluencias = [];
        let today = moment().format("YYYY-MM-DD"); 
        let yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD")
        afluencias['typeLine'] = [];
        let hoy = elements.filter(elem => elem._id.date === today);
        let ayer = elements.filter(elem => elem._id.date === yesterday);

        afluencias['typeLine']['adultos-hora-hoy'] = this.groupBy(hoy.filter(emision  => emision._id.age > 20 && emision._id.age <= 50), 'hour');
        afluencias['typeLine']['joven-hora-hoy'] = this.groupBy(hoy.filter(emision  => emision._id.age <= 20), 'hour');
        afluencias['typeLine']['senior-hora-hoy'] = this.groupBy(hoy.filter(emision  => emision._id.age > 50), 'hour');

        afluencias['typeLine']['adultos-hora-ayer'] = this.groupBy(ayer.filter(emision  => emision._id.age > 20 && emision.age <= 50), 'hour');
        afluencias['typeLine']['joven-hora-ayer'] = this.groupBy(ayer.filter(emision  => emision._id.age <= 20), 'hour');
        afluencias['typeLine']['senior-hora-ayer'] = this.groupBy(ayer.filter(emision  => emision._id.age > 50), 'hour');
        
        this.typeHourLineChartData = [
            { data: afluencias['typeLine']['adultos-hora-hoy'], label: 'Adultos hoy' },
            { data: afluencias['typeLine']['joven-hora-hoy'], label: 'Jóvenes hoy' },
            { data: afluencias['typeLine']['senior-hora-hoy'], label: 'Senior hoy' },
            { data: afluencias['typeLine']['adultos-hora-ayer'], label: 'Adultos ayer' },
            { data: afluencias['typeLine']['joven-hora-ayer'], label: 'Jóvenes ayer' },
            { data: afluencias['typeLine']['senior-hora-ayer'], label: 'Senior ayer' }
        ];
    }
    private groupBy(elements: any[], field: string){
        let result = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
        for( let i = 0; i < 24; i++){
          elements.map(element => {
            if(element._id.hour === i){
              result[i] += element.count;
            }
          });
        }
    
        for(let i = 0; i < 24; i++){
          if(!result[i])
            result[i] = 0;
        }
        let res:number[] = [];
        result.map(r => res.push(r/this.factorCorrector));
        return res;
    }
}