import { Component, OnInit, Input,Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormControl, FormBuilder, FormGroup} from '@angular/forms';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Camera } from 'src/modules/camera/models/camera';
import { CameraService } from 'src/modules/camera/services/camera.service';
import { FormField } from 'src/modules/form-field/models/form-field';

@Component({
  selector: 'camera-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class CameraSelectComponent implements OnInit {

	@Output() onSelected = new EventEmitter<number>();
	@Input() selected: number;
	@Input() site:number;
	@Input() required: boolean = false;

	public loadingCameras:boolean = false;

	public fieldDataCamera: FormField = {
		label: "Cámara/Puerta",
		name: 'camera',
		placeholder: 'Selecciona una cámara',
		options: [{value:0, name: 'TODAS'}],
		multiple:false,
		required: this.required
	};

	public cameras:Camera[] = [];
	public formGroup:FormGroup = new FormGroup({
		camera: new FormControl(null)
	});

  	constructor(
		private _snackBar: MatSnackBar,
		private _camera: CameraService
	) {	
    }

	ngOnInit(): void {
		this.getCameras();
    }
  
	private getCameras(){
		this.loadingCameras = true;
		this._camera.toSelect().subscribe((result: any) => {
			if(result) {
				this.cameras = result.map(c => new Camera(c));
				this.filterBySite();			
			} else {            
				this.openSnackBar(result);
			}
			this.loadingCameras = false;
		}, error => {
			this.loadingCameras = false;
		}); 
	}  
  
	private filterBySite(){
		this.fieldDataCamera.options = [{value:0, name: "Todas"}];
		this.formGroup.patchValue({camera: 0});
		let cameras = this.cameras.filter(c => c.idsite === this.site);
		cameras.map(c => {
			this.fieldDataCamera.options.push({value: c.idcamera, name: c.name});
		});
		this.onSelected.emit(this.formGroup.get('camera').value);
	}
	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

	ngOnChanges(changes: SimpleChanges): void {
		//Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
		//Add '${implements OnChanges}' to the class.
		if(changes.selected && changes.selected.currentValue)
			this.formGroup.patchValue({camera: changes.selected.currentValue});
		if(changes.site && changes.site.currentValue)
			this.filterBySite();
	}
	public cameraChanged(data){
		this.onSelected.emit(this.formGroup.get('camera').value);
	}
}
