import { OnChanges } from '@angular/core';
import { AfterViewInit, Component, ElementRef, HostListener, Input, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ISocketMessage, SocketService } from 'src/services/socket.service';
import { WSService } from 'src/services/ws.service';
import { CONFIG } from 'src/app/app.config';

@Component({
  selector: 'camera-streaming',
  templateUrl: './camera.streaming.component.html',
  styleUrls: ['./camera.streaming.component.scss']
})
export class CameraStreamingComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('container') container: ElementRef;
    @Input() email: string;
    show: boolean = true;
    frame: string = null;

    constructor(private ws: WSService,private socket: SocketService, private renderer: Renderer2) {}

    ngOnInit() {
        this.socket.onConnect.subscribe(connected => {
            if(!connected){ return; }
            this.connectToCamera();
        });

        this.socket.onMessage.subscribe(message => {
            if(message.name == 'send:frame')
                this.frame = message.data.image;
            else if(message.name == 'streaming:close')
                this.frame = null;
        })
    }

    ngOnDestroy() {
        this.socket.onConnect.subscribe(connected => {
            if(!connected){ return; }
            this.socket.leave(`client-${this.email}`);
        });
    }

    ngAfterViewInit() {
        this.resize();
    }

    openStreaming() {
        let message: ISocketMessage = { name: 'streaming:open', to: `camera-${this.email}`,  from: this.socket.id, data: ''};
        this.socket.send(message);
    }

    async connectToCamera() {
        let camera = `camera-${this.email}`;
        let email = this.email;
        this.ws.getCamerasFromPusher().subscribe(result => {
            if(result.success){
                const cameras = result.data;
                if(!cameras.some(c => c === camera) && CONFIG.demo){
                    email = 'icreative@ladorian.com';
                }
                this.email = email;
                this.socket.join(`client-${email}`);
                this.openStreaming();
            }
        })
    }

    @HostListener('window:resize', ['$event']) 
    onResize(event) { 
        this.resize();
    }

    private resize() {
        if(!this.container){ return; }

        const FRAME_WIDTH = 640;
        const FRAME_HEIGHT = 480;
        
        let width = this.container.nativeElement.offsetWidth;
        let height = (width * FRAME_HEIGHT) / FRAME_WIDTH;

        if(height > 390) {
            height = 390;
            width = (height * FRAME_WIDTH) / FRAME_HEIGHT;
            this.renderer.setStyle(this.container.nativeElement, 'width', `${width}px`);
            this.renderer.setStyle(this.container.nativeElement, 'height', `${height}px`);
        } else {
            width = 'calc(100% - 12px)'
            this.renderer.setStyle(this.container.nativeElement, 'width', width);
            this.renderer.setStyle(this.container.nativeElement, 'height', `${height}px`);
        }
    }
}