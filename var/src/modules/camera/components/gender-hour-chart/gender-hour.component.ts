import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import moment from 'moment';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { IResult } from 'src/models/iresult';

@Component({
  selector: 'camera-gender-hour-chart',
  templateUrl: './gender-hour.component.html',
  styleUrls: ['./gender-hour.component.scss']
})

export class CameraGenderHourComponent {
  @Input() aspectRatio: number;
  @Input() height:number;
  @Input() email:string;
  @Input() factorCorrector:number;

  public load: boolean = true;
  public genderHourLineChartData: ChartDataSets[] = [      
    { data: [0], label: 'Hombres hoy' },
    { data: [0], label: 'Mujeres hoy' },
    { data: [0], label: 'Hombres ayer' },
    { data: [0], label: 'Mujeres ayer' }
  ];
  public genderHourLineChartLabels: Label[] = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
  public genderHourlineChartColors: Color[] = [{
      borderColor: '#ff6384',
      backgroundColor: 'rgba(255,99,132,0.5)',
    },
    {
      borderColor: '#4caced',
      backgroundColor:"rgba(76,172,237,0.5)"
    }
  ];
  public resultData : any[] = [];
    
  constructor(
    private ws: WSService,
    private dialog: DialogService,
  ) {}

  public ngOnInit() {
    this.syncViewer();
  }

  private syncViewer() {
    this.renderDataTable();
  }

  public renderDataTable() {
    if(this.resultData.length === 0) 
      this.getEmision();         
  }

  getEmision(day = null){
    let today:string;
    let yesterday: string;
    if(!day){
      today = moment().format("YYYY-MM-DD"); 
      yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");
    } else{
      today = moment(day).format("YYYY-MM-DD");
      yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");
    }
    let params:any = {
      filter:{
        date: {
          "$gte": yesterday,
          "$lte": today
        },
        email: this.email
      },
      group:{
        _id:{ date: "$date",hour:"$hour", gender: "$gender"},
        count: {"$sum": 1}
      }
    }
    this.ws.detailFacesByCamera(params).subscribe((result: IResult) => {
      if(result.success) { 
        this.resultData = result.data;
        this.processAllEmision(); 
        this.load = false;
      }else {
        this.dialog.warning('emisionCustomer', result.data);
        this.load = false;   
      }
    });
  } 

  private processAllEmision(day:string = 'today'){
    const elements = this.resultData;
    let afluencias = [];
    let today = moment().format("YYYY-MM-DD"); 
    let yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD")
    afluencias['genderLine'] = [];
    let hoy = elements.filter(elem => elem._id.date === today);
    let ayer = elements.filter(elem => elem._id.date === yesterday);
  
    afluencias['genderLine']["mujeres-hora-hoy"] = this.groupBy(hoy.filter(emision => emision._id.gender === 'female'), 'hour');
    afluencias['genderLine']["hombres-hora-hoy"] = this.groupBy(hoy.filter(emision => emision._id.gender === 'male'), 'hour');
  
    afluencias['genderLine']["mujeres-hora-ayer"] = this.groupBy(ayer.filter(emision => emision._id.gender === 'female'), 'hour');
    afluencias['genderLine']["hombres-hora-ayer"] = this.groupBy(ayer.filter(emision => emision._id.gender === 'male'), 'hour');
    
    this.genderHourLineChartData = [
      { data: afluencias['genderLine']["hombres-hora-hoy"], label: 'Hombres hoy' },
      { data: afluencias['genderLine']["mujeres-hora-hoy"], label: 'Mujeres hoy' },
      { data: afluencias['genderLine']["hombres-hora-ayer"], label: 'Hombres ayer' },
      { data: afluencias['genderLine']["mujeres-hora-ayer"], label: 'Mujeres ayer' },
    ];
  }
  private groupBy(elements: any[], field: string){
    let result = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    for( let i = 0; i < 24; i++){
      elements.map(element => {
        if(element._id.hour === i){
          result[i] = element.count;
        }
      });
    }

    for(let i = 0; i < 24; i++){
      if(!result[i])
        result[i] = 0;
    }
    let res:number[] = [];
    result.map(r => res.push(r/this.factorCorrector));
    return res;
  }
}