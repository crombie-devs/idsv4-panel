import { AfterViewInit, QueryList, ViewChildren } from '@angular/core';
import { Component, ViewChild } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { CameraService } from 'src/modules/camera/services/camera.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Camera } from 'src/modules/camera/models/camera';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Router } from '@angular/router';
import moment from 'moment';
import { Sort } from '@angular/material/sort';
import { MatInput } from '@angular/material/input';
import { MYString } from 'src/libs/string.libs';
import { MatCheckbox } from '@angular/material/checkbox';
import { from } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'camera-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss']
})

export class CameraListPage implements AfterViewInit{

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
    @ViewChildren('table') table: QueryList<MatCheckbox>
    @ViewChild(MatInput) input: MatInput;

	ngAfterViewInit() {
		setTimeout(() => {
			this.dataSource.paginator = this.paginator;
		  }, 500) 
		this.dataSource.sort = this.sort;
	}

	public displayedColumns: string[] = ['select', 'idcamera', 'email',  'site', 'faces_today', 'faces_yesterday', 'link', 'status'];
	public selection = new SelectionModel<Camera>(true, []);
	public dataSource = new MatTableDataSource<Camera>();
	public new: boolean = false;
	public edit: boolean = false;
	public deleted: boolean = false;
	public customersToSelect: boolean = false;
	public me: any;	
	public permission: boolean = false;
	public load: boolean = true;
	sortedData: Camera[];
	cameras: Camera[];
    data: Camera[];

	constructor(
		private ws: WSService,
		private cs: CameraService,
		private debug: DebugStore,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private dialog: DialogService,
		private router: Router,
        private translate: TranslateService,
        private _cameras: CameraService) {
			this.ustore.onChange.subscribe(user => { 
				this.me = user; 
			});		
  	}
  
  	public ngOnInit() {
		this.verifyPermissions();
		this.getCameras();
	}

    
    public changeStatus(event) {
        this._cameras.changeStatus(event).subscribe( item => {
        
        });

   }


	private getCameras(){
		this.cs.list().subscribe((cameras:Camera[]) => {
			this.permission = true;
            this.data = cameras;
			this.renderDataTable(cameras);
            this.load = false;
		}, error => {
			this.debug.addError('WS.cameras', error.message);  
			this.load = false;
			return null;          
		});
	}

	  
	public renderDataTable(data: Camera []) {
		this.dataSource = new MatTableDataSource(data);	
		this.dataSource.filterPredicate = (data: Camera, filter: string): boolean => {
			const dataStr = MYString.removeAccents(Object.keys(data).reduce((currentTerm: string, key: string) => {
			  return (currentTerm + (data as { [key: string]: any })[key] + '◬');
			}, '')).trim().toLowerCase();	  
			return dataStr.indexOf(MYString.removeAccents(filter.trim().toLowerCase())) != -1;
		};	
        this.render(data);
		const today = moment().format("YYYY-MM-DD"); 
		const yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");		
		let params:any;
		if(this.me.customer)
			params = {
				"filter": {
					"customer_id": this.me.customer.idcustomer,
					"date": { "$gte": yesterday, "$lte": today }
				},
				"group": {
					"_id":{"camera_id": "$camera_id", "date": "$date"},
					"count": {"$sum": 1},
					"camera_id": {"$last": "$camera_id"},
					"date": {"$last": "$date"},
					"customer_id": {"$last": "$customer_id"}
				},
				"sort": {"camera_id": 1}  
			};
		else
			params = {
				"filter": {
					"date": {"$gte": yesterday, "$lte": today}
				},
				"group": {
					"_id":{"camera_id": "$camera_id", "date": "$date"},
					"count": {"$sum": 1},
					"camera_id": {"$last": "$camera_id"},
					"date": {"$last": "$date"},
					"customer_id": {"$last": "$customer_id"}
				},
				"sort": {"camera_id": 1}  
			};

		this.ws.getTotalFacesByCamera(params).subscribe((result: IResult) => {
			if(result.success) { 
				data.map((camera,index) => {
					let resultToday = result.data.find(res => res.date === today && res.camera_id === camera.idcamera);
					let resultYesterday = result.data.find(res => res.date === yesterday && res.camera_id === camera.idcamera);
					data[index].facesToday = 0;
					data[index].facesYesterday = 0;
					if(resultToday)
						data[index].facesToday = Number (resultToday.count);
					if (resultYesterday)	
						data[index].facesYesterday = Number (resultYesterday.count);
				});
				this.dataSource.data = data;
				this.sortedData = data.slice();
				this.cameras = data;
				setTimeout(() => {
					this.dataSource.paginator = this.paginator;
				  }, 500) 
				this.dataSource.sort = this.sort;
				this.sortData({active: 'faces_yesterday', direction: 'desc'});
				this.load = false;
				this.show();
			}
	  	});	
      
		
	}

	public render(data){
        if(this.input)
            this.applyFilter(this.input.value);
		this.sortedData = data.slice();
		this.cameras = data;
		setTimeout(() => {
			this.dataSource.paginator = this.paginator;
		  }, 500) 
		
		this.dataSource.sort = this.sort;
		this.load = false;
		this.show();
	}

	isAllSelected() {
		if(this.dataSource.paginator){
			const numSelected = this.selection.selected.length;
			const page = this.dataSource.paginator.pageSize;
			let endIndex: number;
			if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
			endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
			} else {
			endIndex = this.dataSource.data.length - (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize);
			}
			return numSelected === endIndex;
		}
      }

    masterToggle() {
        this.isAllSelected() ?
          this.selection.clear() : this.selectRows();
    }

    selectRows() {
        let endIndex: number;
        if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
          endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
        } else {
          endIndex = this.dataSource.data.length;
        }        
        for (let index = (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize); index < endIndex; index++) {
          this.selection.select(this.dataSource.data[index]);
        }
    }

	public checkboxLabel(row?: Camera): string {
		if (!row) {
			return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
		}
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idcamera + 1}`;
	}

	
	public delete(){
        let camerasByDelete = [];
		this.table.toArray().forEach(item => {
			if(item.checked) 
				camerasByDelete.push(item.value);			
	    });	
		if(camerasByDelete.length == 0){
			this.dialog.warning(this.translate.instant('Aviso'), this.translate.instant('Debe seleccionar al menos una camara')).subscribe();
		}else{
			this.dialog.confirm(this.translate.instant('Confirmar Archivados'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
				if (confirm){
                    this.deleteByIds(camerasByDelete);   
				}
			});
		}		
	}

	public update(){
		if (this.selection.selected.length>1){
			this.dialog.warning('Aviso', '"only_one_camera"').subscribe();
		}else if(this.selection.selected.length == 0){
			this.dialog.warning('Aviso', 'you_must_select_a_camera').subscribe();
		}else{
			this.router.navigateByUrl('/private/camera/edit/'+this.selection.selected[0].idcamera);
		}
	}

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

	public show(){		 
		this.me.permissions.forEach(element => {
			if(element.name == "camera.put.*" || element.name == "*.*.*"){
			  this.new = true;
			}
			if(element.name == "camera.delete.*" || element.name == "*.*.*"){
			  this.edit = true;
			}
			if(element.name == "camera.post.*" || element.name == "*.*.*"){
			  this.deleted = true;
			}
		})
	}
	
	public goToDetail(row){
		this.router.navigateByUrl(`/private/camera/detail/${row.email}/${row.customer.streaming}`);
	}
	
	sortData(sort: Sort) {
		const data = this.dataSource.data.slice();
		if (!sort.active || sort.direction === '') {
		  this.sortedData = data;
		  return;
		}
	
		this.sortedData = data.sort((a, b) => {
		  const isAsc = sort.direction === 'asc';
		  switch (sort.active) {
			case 'site': return this.compare(a.site?a.site.name:'', b.site?b.site.name:'', isAsc);
			case 'idcamera': return this.compare(a.idcamera, b.idcamera, isAsc);
			case 'email': return this.compare(a.email? a.email:'', b.email?b.email:'', isAsc);
			case 'faces_yesterday' : return this.compare(a.facesYesterday, b.facesYesterday, isAsc);
			case 'faces_today': return this.compare(a.facesToday, b.facesToday, isAsc);
			default: return 0;
		  }
		});
		this.dataSource.data = this.sortedData;
	}
	
	compare(a: number | string, b: number | string, isAsc: boolean) {
	  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
	}

	public applyFilter(event: string) {
        this.selection.clear();
        if(!event) 
            this.dataSource.data = this.data;	
        else{
            if(typeof event === 'string'){
                this.dataSource.data = this.data.filter(camera => 
					camera.email.toLowerCase().includes(event.trim().toLowerCase()) || 
					camera.idcamera.toString().includes(event.trim()) || 
					(camera.site && camera.site.name.toLowerCase().includes(event.trim().toLowerCase()))
				);
            }
        }
	}

	private verifyPermissions() {
		this.new = this.ustore.hasPermissionByName("post.camera");
		this.edit = this.ustore.hasPermissionByName("put.camera.*");
		this.deleted = this.ustore.hasPermissionByName("delete.camera.*");
		this.customersToSelect = this.ustore.hasPermissionByName("get.customers.toselect");
	}

    private deleteByIds(ids: number[]) {
        from(ids).pipe(
          concatMap(id => this.cs.delete(id))
        ).subscribe(result => {  
                this.load = true;        
            }, error => {
                this.openSnackBar(error.message);
                this.getCameras();
            },
            () => {
                this.openSnackBar('Las cámaras seleccionadas han sido archivadas correctamente');                       
                this.selection.clear();
                this.getCameras(); 
            }); 
    }
}
