import { Component } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormControl, FormBuilder, Form } from '@angular/forms';
import { Customer } from 'src/modules/customer/models/customer';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Camera } from 'src/modules/camera/models/camera';
import { Site } from 'src/modules/site/models/site';
import { ICameraPower, CameraPower } from 'src/modules/camera/models/camera-power';
import { UserStore } from 'src/modules/user/stores/user.store';
import moment from 'moment';
import { Power } from 'src/modules/power/models/power';
import { SiteService } from 'src/modules/site/services/site.service';
import { CameraService } from 'src/modules/camera/services/camera.service';

@Component({
    selector: 'camera-form',
    templateUrl: './form.page.html',
    styleUrls: ['./form.page.scss']
})

export class CameraFormPage {

    public id: number | null;

    public form: FormGroup;

    public weekdays = ['', 'WEEKDAYS.mon', 'WEEKDAYS.tue', 'WEEKDAYS.wed', 'WEEKDAYS.thu', 'WEEKDAYS.fri', 'WEEKDAYS.sat', 'WEEKDAYS.sun'];

    public customers: Customer[] = [];
    public sites: Site[] = [];
    public powersc: ICameraPower[] = [];

    public camera: Camera;
    public site: Site | null;

    public loading: boolean = false;
    public customersToSelect: boolean = false;
    public customerSelect: number = null;

    public selectedCameraPowers: Power[] = [];
    public check: boolean;
    public me: any;
    public hide: boolean = true;
    public is_monitored: boolean;

    constructor(
        private ws: WSService,
        private wsSite: SiteService,
        private wsCamera: CameraService,
        private debug: DebugStore,
        private router: Router,
        private route: ActivatedRoute,
        private _snackBar: MatSnackBar,
        private ustore: UserStore) {
        this.id = Number(this.route.snapshot.paramMap.get('idcamera'));
        this.ustore.onChange.subscribe(user => {
            this.me = user;
            (this.me.customer) ? this.hide = false : this.hide = true;
        });


        this.form = new FormGroup({
            name: new FormControl('', Validators.required),
            code: new FormControl(''),
            email: new FormControl('', [Validators.required, Validators.email]),
            idsite: new FormControl(Validators.required),
            idcustomer: new FormControl(0),
            powers: new FormControl([]),
            is_monitored: new FormControl(false),
            face_timeout: new FormControl(300),
            section_start_x: new FormControl(0),
            section_start_y: new FormControl(0),
            section_width: new FormControl(640),
            section_height: new FormControl(480),
            status: new FormControl(0),
            face_size: new FormControl(20),
            face_threshold: new FormControl(0.57),
            cosine_threshold: new FormControl(0.62),
            embedding_threshold: new FormControl(16.0)
        });

    }

    public ngOnInit() {
        this.debug.addInfo('CameraFormComponent', 'OnInit');
        this.verifyPermissions();
        this.initForm(null);
        this.getSites();
        if (this.id) {
            this.hide = false;
            this.wsCamera.get(this.id).subscribe((camera: Camera) => {
                this.debug.addInfo('WS.getCamera', 'successful');
                this.camera = new Camera(camera);
                if (this.camera.powers.length > 0) {
                    let powers: Power[] = [];
                    this.camera.powers.map(power => {
                        power.time_on = moment("2015-01-16T" + power.time_on).format('HH:mm');
                        power.time_off = moment("2015-01-16T" + power.time_off).format('HH:mm');
                        power.new = false;
                        powers.push(power);
                    });
                    this.selectedCameraPowers = powers;
                }
                this.getSiteByCustomer(camera.customer.idcustomer);
                this.initForm(camera);
            }, error => {
                this.debug.addError('WS.getCamera', error.message);
            });
        }
    }

    public onSaveContentPowerDialog(selections) {
        this.selectedCameraPowers = [];
        selections.map(power => {
            power.time_on = moment("2015-01-16T" + power.time_on).format('HH:mm');
            power.time_off = moment("2015-01-16T" + power.time_off).format('HH:mm');
            power.new = false;
            this.selectedCameraPowers.push(power);
        })
    }

    private getSites() {
        this.wsSite.toSelect().subscribe((sites: Site[]) => {
            this.sites = sites;
        }, error => {
            return null;
        });
    }

    private initForm(data) {

        if (data) {
            this.customerSelect = data.customer.idcustomer;
            this.site = data.site ? new Site(data.site) : null;
            this.form.patchValue({
                name: data.name,
                code: data.code,
                email: data.email,
                idcustomer: data.customer.idcustomer,
                idsite: data.idsite,
                powers: [],
                is_monitored: data.is_monitored,
                face_timeout: data.face_timeout,
                section_start_x: data.section_start_x,
                section_start_y: data.section_start_y,
                section_width: data.section_width,
                section_height: data.section_height,
                status: data.status == '1' ? true : false,
                face_size: data.face_size,
                face_threshold: data.face_threshold,
                cosine_threshold: data.cosine_threshold,
                embedding_threshold: data.embedding_threshold
            });
            this.is_monitored = this.form.value.is_monitored;
            this.check = this.form.value.status;

        }
    }



    public getSiteByCustomer(idcustomer: number | null, select: boolean = false) {
        if (idcustomer != null) {
            if (idcustomer == 0)
                this.getSites();
            else
                this.ws.getCustomer(idcustomer).subscribe((result: IResult) => {
                    if (result.success) {
                        this.debug.addInfo('WS.getCustomer', 'successful');
                        this.sites = result.data.sites;
                        this.form.get('idsite').enable();
                        this.site && !select ? this.form.get('idsite').setValue(this.site.idsite) : null;
                        select ? this.form.get('idsite').setValue("") : null;
                    }
                    else {
                        this.debug.addError('WS.getCustomer', result.data);
                        this.form.get('idsite').disable();

                    }
                }, error => {
                    this.debug.addError('WS.getCustomer', error.message);
                    this.form.get('idsite').disable();
                });
        }
        else
            this.form.get('idsite').disable();

    }

    public create() {
        if (this.form.invalid) return null;
        this.loading = true;
        this.wsCamera.create(this.form.value).subscribe((camera: Camera) => {
            const id = camera.idcamera;
            this.debug.addInfo('WS.createCamera', 'successful');
            this.updateCameraPowers(id);
            this.openSnackBar("Cámara creada correctamente");
            this.router.navigateByUrl('/private/camera/list');
            this.loading = false;
        }, error => {
            let msj = JSON.stringify(error.error.data);
            this.openSnackBar(msj.substr(2, msj.length - 3));
            this.debug.addError('WS.createCamera', error.message);
            this.loading = false;
        });
    }

    public update() {
        if (this.form.invalid) return null;
        let form = this.form.getRawValue();
        this.loading = true;
        let data: Camera = new Camera(this.form.value);
        data.idcamera = this.id;
        this.form.value.idcamera = this.id;
        this.wsCamera.update(this.form.value).subscribe((camera: Camera) => {
            this.debug.addInfo('WS.updateCamera', 'successful');
            this.updateCameraPowers(this.id);
            this.openSnackBar("Cámara actualizada correctamente");
            this.router.navigateByUrl('/private/camera/list');
            this.loading = false;
        }, error => {
            let msj = JSON.stringify(error.error.data);
            this.openSnackBar(msj.substr(2, msj.length - 3));
            this.debug.addError('WS.updateCamera', error.message);
            this.loading = false;
        });
    }

    public updateCameraPowers(idplayer: number) {
        let selectedPowersData = []

        if (this.selectedCameraPowers.length > 0) {
            this.selectedCameraPowers.map(power => {
                selectedPowersData.push(power)
            })
        }

        let body = {
            "powers": selectedPowersData
        }
        this.ws.updateCameraPowers(idplayer, body).subscribe((result: IResult) => {
            if (result.success) {
                let data = result.data;
                this.debug.addInfo('WS.updateCameraPowers', 'successful');
            } else {
                this.debug.addError('WS.updateCameraPowers', result.data);
            }
        }, error => {
            this.debug.addError('WS.updateCameraPowers', error.message);
        });
    }

    public customerSelected(idcustomer: number) {
        (idcustomer == 0) ? this.hide = true : this.hide = false;
        this.form.setValue({ idcustomer: idcustomer });
        this.getSiteByCustomer(idcustomer);

    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }

    private verifyPermissions() {
        this.customersToSelect = this.ustore.hasPermissionByName("get.customers.toselect");
    }

    public onSaveCameraPowerDialog(selections) {
        this.selectedCameraPowers = [];
        selections.map(power => {
            power.time_on = moment("2015-01-16T" + power.time_on).format('HH:mm');
            power.time_off = moment("2015-01-16T" + power.time_off).format('HH:mm');
            power.new = false;
            this.selectedCameraPowers.push(power);
        })
    }

    public changeSite(event) {
        this.form.patchValue({ idsite: event.idsite });
        this.site = new Site(event);
    }

}
