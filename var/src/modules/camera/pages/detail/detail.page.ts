import { Component, ViewChild, Renderer2, OnInit, ElementRef } from '@angular/core';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import { FormControl, FormGroup } from '@angular/forms';
import { FormField } from 'src/modules/form-field/models/form-field';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { WSService } from 'src/services/ws.service';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { CameraStore } from 'src/modules/camera/stores/camera.store';
import { IResult } from 'src/models/iresult';
import { DebugStore } from 'src/stores/debug.store';
import { ISocketMessage, SocketService } from 'src/services/socket.service';
import { IStreamMessage, StreamMessage } from 'src/models/stream-message';
import moment from 'moment';
import { concatMap } from 'rxjs/operators';
import { from } from 'rxjs';
import { hasLifecycleHook } from '@angular/compiler/src/lifecycle_reflector';
import { CameraChart } from 'src/modules/camera/models/camera-chart';
import { analyzeAndValidateNgModules } from '@angular/compiler';
// For graphics
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

import * as h337 from 'heatmap.js';
import { ThemePalette } from '@angular/material/core';
import { CONFIG } from 'src/app/app.config';
import { UserStore } from "src/modules/user/stores/user.store";

@Component({
  selector: 'camera-detail',
  templateUrl: './detail.html',
  styleUrls: ['./detail.scss']
})

export class CameraDetailPage{

  public data: boolean = false;
  public idsite: any;
  public idcustomer: any;
  public streamMessage: IStreamMessage;
  public result: IStreamMessage[] = [];
  public loading:HTMLDivElement;
  public icon: HTMLDivElement;
  public load: boolean = true;
  public loadSocked: boolean=true;
  public email: string;
  public loadFacesToday: boolean = true;
  public frame: any;
  public afluencias: any  = []; 
  public isStreaming: boolean = false;
  public color = 'primary';
  public aspectRatio: boolean = false;
  public height:string = "400";
  public streaming: SafeResourceUrl = '';
  public hls : any;
  public defaultCamera: string = 'icreative@ladorian.com';
  public config = CONFIG;
  public me: any;
  public isLoading:boolean = false;
  
  // Cambiar esta variable cuando no se quiera usar la cámara por defecto.
  public useDefault: boolean = false;

  // Para mapa de calor
  gradientCfg = { // heatmap gradient color range
    '0.15': '#6ad180', // green
    '0.25': '#7cd573',
    '0.35': '#90d865',
    '0.45': '#a4da57',
    '0.55': '#badc48',
    '0.65': '#c9cf35',
    '0.75': '#d6c226',
    '0.80': '#e2b41c',
    '0.85': '#e2961d',
    '0.90': '#dd7826',
    '0.95': '#d25c30',
    '1.0': '#c24039' // highest red
  };
  heatmap: any = null; // heatmap instance
  coordinates: Array<Coordinate> = []; // heatmap coordinates array
  heatmapContainer: HTMLElement;  // heatmap container HTML element
  maxCount:number = 0;

  //Para gráfico en linea
  genderHourLineChartData: ChartDataSets[] = [      
    { data: [0], label: 'Hombres hoy' },
    { data: [0], label: 'Mujeres hoy' },
    { data: [0], label: 'Hombres ayer' },
    { data: [0], label: 'Mujeres ayer' }
  ];
  typeHourLineChartData: ChartDataSets[] = 
    [
      { data: [0], label: 'Adultos hoy' },
      { data: [0], label: 'Jóvenes hoy' },
      { data: [0], label: 'Senior hoy' },
      { data: [0], label: 'Adultos ayer' },
      { data: [0], label: 'Jóvenes ayer' },
      { data: [0], label: 'Senior ayer' }
    ];

  genderHourLineChartLabels: Label[] = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
  typeHourLineChartLabels: Label[] = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];

	timesHoraLineChartData: ChartDataSets[] = [];
	timesDiaLineChartData: ChartDataSets[] = [];
	timesDiaSemanaLineChartData: ChartDataSets[] = [];

	timesHoraLineChartLabels: Label[] = ['00:00', '', '', '03:00', '', '', '06:00', '', '', '09:00', '', '', '12:00', '', '', '15:00', '', '', '18:00', '', '', '21:00', '', ''];
	timesDiaLineChartLabels: Label[] = [];
	timesDiaSemanaLineChartLabels: Label[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];


  lineChartOptions = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            callback: function(value) {if (value % 1 === 0) {return value;}}
          },
          position: 'left',
        }
      ]
    },
  };

  genderHourlineChartColors: Color[] = [
    {
      borderColor: '#ff6384',
      backgroundColor: 'rgba(255,99,132,0.5)',
    },
    {
      borderColor: '#4caced',
      backgroundColor:"rgba(76,172,237,0.5)"
    }
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public genderPieChartLabels: Label[] = ['Hombres', 'Mujeres'];
  public genderPieChartData: SingleDataSet = [0,0];
  public typePieChartLabels: Label[] = ['Adultos', 'Jóvenes', 'Senior'];
  public typePieChartData: SingleDataSet = [0,0,0,0];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public imageWidth:string = '734px';
  public leftPos:string = '0px';
  public isChecked = false;
  public resultData : any[] = []
  public factorCorrector:number = 4;
	public fieldDataTimes: FormField = {
		label: "Elegir:",
		name: 'times',
		placeholder: '',
		options: [
			{name: 'Horas', value: 1 },
			{name: 'Días', value: 2 },
			{name: 'Días de la semana', value: 3 }
		],
		multiple:false,
		required: false
	};
  public formGroup:FormGroup = new FormGroup({
		times: new FormControl(1)
	});

  public totalMediumTime: number = 0;
  constructor(
    private store: CameraStore, 
    private dialog: DialogService,
    private route: ActivatedRoute,
    private ws: WSService,
    private sanitizer: DomSanitizer,
    private debug: DebugStore,
    private socket: SocketService,
    private ustore: UserStore,
    private renderer: Renderer2) {
      moment.updateLocale('es', {});
      this.email = this.useDefault ? this.defaultCamera : this.route.snapshot.paramMap.get('email');
      this.hls = this.useDefault ? this.defaultCamera : this.route.snapshot.paramMap.get('streaming');
      if(CONFIG.demo){
        this.email = this.defaultCamera;
        this.hls = 2;
      }
      this.ustore.onChange.subscribe(user => { 
        this.me = user; 
      });

      monkeyPatchChartJsTooltip();
      monkeyPatchChartJsLegend();
    }
 
  public ngOnInit() {
    this.isLoading = true;
    this.syncViewer();
    setTimeout(()=>{ 
      if (this.loadSocked)
        this.loadSocked = false;
    }, 5000);
  }

  public heatMapChecked(){
    this.isChecked = !this.isChecked;
    if(this.isChecked)
      setTimeout(() => {this.syncViewer()}, 500);
  }

  public ngOnDestroy(){
    this.socket.leave('client-'+this.email);
  }

  changeAfluences(){
    this.syncViewer()
  }

  private syncViewer() {
    this.renderDataTable();
  }

  public renderDataTable() {
    if(this.resultData.length === 0) 
      this.getEmision();         
  }

  getEmision(day = null){
    let today:string;
    let yesterday: string;
    if(!day){
      today = moment().format("YYYY-MM-DD"); 
      yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");
    } else{
      today = moment(day).format("YYYY-MM-DD");
      yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD");
    }
    const firstday = moment().startOf('month').format('YYYY-MM-DD');
    const lastday = moment().endOf('month').format('YYYY-MM-DD');
    let params:any = {
      filter:{
        date: {
          "$gte": firstday,
          "$lte": lastday
        },
        email: this.email
      }
    }
    this.ws.detailFacesByCamera(params).subscribe((result: IResult) => {
      if(result.success) { 
        this.resultData = result.data;
        this.totalMediumTime = this.calculateMediumTimes(this.resultData);
        this.processAllEmision(); 
        if(this.isChecked){
          //this.generateCoordinates();
        }
        this.load = false;

      }else {
        this.debug.addError('WS.emisionCustomer', result.data);
        this.dialog.warning('emisionCustomer', result.data);
        this.load = false;   
      }
    });
  } 

  private calculateMediumTimes(data){
		let total = 0;
		data.map(afluence => {
			total += afluence.duration;
		});
		return total / data.length;
	}
  private processAllEmision(day:string = 'today'){
    const elements = this.resultData;
    this.timesDiaLineChartLabels = this.getAfluenciasDiaChartLabels(elements);
    let afluencias = [];
    let today = moment().format("YYYY-MM-DD"); 
    let yesterday = moment(today).subtract(1, 'days').format("YYYY-MM-DD")
    afluencias['genderPie'] = [];
    afluencias['genderLine'] = [];
    afluencias['typePie'] = [];
    afluencias['typeLine'] = [];
    afluencias['times'] = [];

    let hoy = elements.filter(elem => elem.date === today);
    let ayer = elements.filter(elem => elem.date === yesterday);
    afluencias['genderPie']["hombres"] = Math.round(hoy.filter(emision  => emision.gender === 'male').length / this.factorCorrector);
    afluencias['genderPie']["mujeres"] = Math.round(hoy.filter(emision => emision.gender === 'female').length / this.factorCorrector);
    afluencias['typePie']['adultos'] = Math.round(hoy.filter(emision  => emision.age > 20 && emision.age <= 50).length / this.factorCorrector);
    afluencias['typePie']['joven'] = Math.round(hoy.filter(emision  => emision.age <= 20).length / this.factorCorrector);
    afluencias['typePie']['senior'] = Math.round(hoy.filter(emision  => emision.age > 50).length / this.factorCorrector);

    afluencias['genderLine']["mujeres-hora-hoy"] = this.groupBy(hoy.filter(emision => emision.gender === 'female'), 'hour');
    afluencias['genderLine']["hombres-hora-hoy"] = this.groupBy(hoy.filter(emision => emision.gender === 'male'), 'hour');

    afluencias['typeLine']['adultos-hora-hoy'] = this.groupBy(hoy.filter(emision  => emision.age > 20 && emision.age <= 50), 'hour');
    afluencias['typeLine']['joven-hora-hoy'] = this.groupBy(hoy.filter(emision  => emision.age <= 20), 'hour');
    afluencias['typeLine']['senior-hora-hoy'] = this.groupBy(hoy.filter(emision  => emision.age > 50), 'hour');

    afluencias['genderLine']["mujeres-hora-ayer"] = this.groupBy(ayer.filter(emision => emision.gender === 'female'), 'hour');
    afluencias['genderLine']["hombres-hora-ayer"] = this.groupBy(ayer.filter(emision => emision.gender === 'male'), 'hour');

    afluencias['typeLine']['adultos-hora-ayer'] = this.groupBy(ayer.filter(emision  => emision.age > 20 && emision.age <= 50), 'hour');
    afluencias['typeLine']['joven-hora-ayer'] = this.groupBy(ayer.filter(emision  => emision.age <= 20), 'hour');
    afluencias['typeLine']['senior-hora-ayer'] = this.groupBy(ayer.filter(emision  => emision.age > 50), 'hour');

		afluencias['times'][`hora`] = this.calculateTimes(elements, 'hour');
		afluencias['times'][`dia`] = this.calculateTimes(elements, 'date');
		afluencias['times'][`weekday`] = this.calculateTimes(elements, 'weekday');

    this.genderHourLineChartData = [
      { data: afluencias['genderLine']["hombres-hora-hoy"], label: 'Hombres hoy' },
      { data: afluencias['genderLine']["mujeres-hora-hoy"], label: 'Mujeres hoy' },
      { data: afluencias['genderLine']["hombres-hora-ayer"], label: 'Hombres ayer' },
      { data: afluencias['genderLine']["mujeres-hora-ayer"], label: 'Mujeres ayer' },
    ];
    this.typeHourLineChartData = [
      { data: afluencias['typeLine']['adultos-hora-hoy'], label: 'Adultos hoy' },
      { data: afluencias['typeLine']['joven-hora-hoy'], label: 'Jóvenes hoy' },
      { data: afluencias['typeLine']['senior-hora-hoy'], label: 'Senior hoy' },
      { data: afluencias['typeLine']['adultos-hora-ayer'], label: 'Adultos ayer' },
      { data: afluencias['typeLine']['joven-hora-ayer'], label: 'Jóvenes ayer' },
      { data: afluencias['typeLine']['senior-hora-ayer'], label: 'Senior ayer' }
    ];
    this.genderPieChartData = [afluencias["genderPie"]['hombres'],afluencias["genderPie"]['mujeres']];
    this.typePieChartData = [afluencias['typePie']['adultos'], afluencias['typePie']['joven'], afluencias['typePie']['senior']];

    this.timesHoraLineChartData.push({data: afluencias['times'][`hora`], label: 'Tiempo medio de estancia', fill:false});
		this.timesDiaLineChartData.push({data: afluencias['times'][`dia`], label: 'Tiempo medio de estancia', fill:false});
		this.timesDiaSemanaLineChartData.push({data: afluencias['times'][`weekday`], label: 'Tiempo medio de estancia', fill:false});
    this.isLoading = false;
  }
	private calculateTimes(elements: any [], field: string){
		let result = [];
		if(field === 'hour')
			result = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		let fields:number[] = [];
		elements.map(element => {
		  if(result[element[field]]){
			const val = result[element[field]];
			fields[element[field]]++;
			result[element[field]] = (val + element.duration);
		  }else{
			fields[element[field]] = 1;
			result[element[field]] = element.duration;
		  }
		});
		let keys = Object.keys(result);
		if(result.length > 0)
			result.map((res, i) =>{
				result[i] = result[i] / fields[i];
			});
		else
			for (const property in result) {
				result[property] = result[property] / fields[property];
				console.log(`${property}: ${result[property]}`);
			}
		if(field === 'date'){
			let resp = [];
			this.timesDiaLineChartLabels.map((date, index) =>{
				resp[index] = 0;
				for(let i in result){
					if(i === date)
						resp[index] = result[i];
				}
			});
			result = resp;
		}
		if(field === 'weekday'){
			let resp = [0,0,0,0,0,0,0]
			for(let i in result){
				resp[parseInt(i) - 1] = result[i];
			}
			result = resp;
		}
		return result;
	}
	private getAfluenciasDiaChartLabels(data){
		let array = [];
		data.sort((a,b) => a.date > b.date ? 1 : -1);
		data.map(d => {
			if(!array.some(a => a === d.date))
				array.push(d.date);
		})
		return array;
	}

  public generateCoordinates(): void {
    const elements = this.resultData;
    const elem = document.getElementById('movie');
    const widthElem = elem.offsetWidth;
    const heightElem = elem.offsetHeight;
    let array = [];
    let totalArray = [];

    elements.map(element =>{
      if(element.positions)
      element.positions.map(position =>{
        array.push(position);
      });
    });
    array.sort();
    let counter = 1;
    array.map((elem,index) => {
      let x = 0;
      let y = 0;
      if(index > 0){
        x = Math.floor((array[index - 1].x * widthElem) / 640);
        y = Math.floor((array[index - 1].y * heightElem) / 480);
      }else{
        x = (array[index].x * widthElem) / 640;
        y = (array[index].y * heightElem) / 480;        
      }
      if(index > 0 && (x === array[index - 1].x && y === array[index - 1].y) && index < array.length - 1){
        counter++;
      } else if (index > 0){
        const c = counter;
        // add to dataset
        this.coordinates.push({ x: x, y: y, value: c });
        if(this.maxCount < counter)
          this.maxCount = counter;
        counter = 1;
      } else if(index === array.length - 1){
        const c = counter;
        // add to dataset
        this.coordinates.push({ x: x, y: y, value: c });
      }
    });
    this.paintHeatMap();
  }

  public paintHeatMap(){
    const elem = document.getElementById('movie');
    this.imageWidth = elem.offsetWidth+'px';
    this.leftPos = elem.offsetLeft+'px';
    const container = document.getElementById('heatmapContainer');
    const heatmapConfig = { // heatmap config object. For more info visit documentation
      container: container,
      opacity: .7,
      radius: 50,
      visible: true,
      gradient: this.gradientCfg,
      backgroundColor: 'inherit',
    };
    this.heatmap = h337.create(heatmapConfig); // creating the instance
    this.heatmap.setData({ max: this.maxCount, data: this.coordinates }); // passing the dummy coordinates
  }
  
  private groupBy(elements: CameraChart [], field: string){
    let result = [];
    elements.map(element => {
      if(result[element[field]]){
        result[element[field]]++;
      }else{
        result[element[field]] = 1
      }
    });
    for(let i = 0; i < 24; i++){
      if(!result[i])
        result[i] = 0;
    }
    let res:number[] = [];
    result.map(r => res.push(r/this.factorCorrector));
    return res;
  }

 }

 export interface Coordinate {
  x: number;
  y: number;
  value: number;
}