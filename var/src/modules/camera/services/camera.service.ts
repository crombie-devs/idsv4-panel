import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Camera } from 'src/modules/camera/models/camera';
import { HttpService } from 'src/services/http.service';
import { ICrudWS } from 'src/interfaces/ICrudWS.interface';

@Injectable({
	providedIn: 'root',
})
export class CameraService implements ICrudWS<Camera>{
	
	constructor(private http: HttpService) {}

	public list(args?:any[]): Observable<Camera[]> {
		return this.http.get(`private/cameras`, args, 'WS.camera');
	}

	public toSelect(args?: any[]): Observable<Camera[]> {
		return this.http.get(`private/cameras/toselect`, args, 'WS.camera');
	}

	public delete(id: number): Observable<boolean> {
		return this.http.delete('private/camera', [id], 'WS.camera_delete');
	}
	public deleteForce(id: number, args?:any[]): Observable<Camera> {
		return this.http.delete(`private/camera/${id}/force`, args, 'WS.camera_delete');
	}

	public update(obj: Camera): Observable<Camera> {
		return this.http.update(`private/camera/${obj.idcamera}`,obj, 'WS.asset_add');
	}

	public create(data: any): Observable<Camera> {
		return this.http.create(`private/camera`, data, 'WS.asset_add');
	}

	public get(id: number): Observable<Camera> {
		return this.http.get('private/camera', [id], 'WS.asset');
	}

    public changeStatus(id: number): Observable<boolean[]> {
        return  this.http.get('private/camera/status', [id], 'WS.camera_change_status');
      }
      
}


