import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from 'src/directive/directives.module';

import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';

import { MaterialModule } from 'src/app/material.module';

import { CameraRoutingModule } from './camera-routing.module';
import { CAMERA_COMPONENTS } from './components/components';

import { CameraStore } from './stores/camera.store';

import { ChartGraphsModule } from 'src/modules/chart/charts.module';
import { PrimetimeModule } from 'src/modules/primetime/primetime.module';
import { FormFieldModule } from 'src/modules/form-field/form-field.module';
import { CustomerModule } from 'src/modules/customer/customer.module';
import { SiteModule } from 'src/modules/site/site.module';

import { CameraDetailPage } from 'src/modules/camera/pages/detail/detail.page';
import { CameraListPage } from 'src/modules/camera/pages/list/list.page';
import { CameraFormPage } from 'src/modules/camera/pages/form/form.page';
import { SharedModule } from 'src/shared/shared.module';
import { PipesModule } from 'src/pipes/pipes.module';

import { CONFIG } from 'src/app/app.config';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import moment from 'moment';

export function HttpLoaderFactory(http: HttpClient) {
  let url = CONFIG.lang_url;  
  return new TranslateHttpLoader(http, `${url}/i18n/`, `.json?timestamp=${moment().valueOf()}`);
}
@NgModule({
  declarations: [
    CAMERA_COMPONENTS,
    CameraDetailPage,
    CameraListPage,
    CameraFormPage
 
  ],
  imports: [
    CommonModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    NgxMatFileInputModule,
    FlexModule,
    CustomerModule,
    SiteModule,
    ChartGraphsModule,
    PrimetimeModule,
    FormFieldModule,
    CameraRoutingModule,
    SharedModule,
    PipesModule,
    DirectivesModule
  ],
  exports:[
    CAMERA_COMPONENTS
  ],
  entryComponents: [],
  providers: [CameraStore],
})
export class CameraModule {
  constructor(private translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('es');
    if(!sessionStorage.getItem('lang')){
      sessionStorage.setItem('lang', 'es');
      translate.use('es');
    }else{
      translate.use(sessionStorage.getItem('lang'));
    }
  }
}