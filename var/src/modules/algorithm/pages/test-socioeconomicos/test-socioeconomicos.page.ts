import { Component } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { WSService } from 'src/services/ws.service';
import { FormField } from 'src/modules/form-field/models/form-field';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as XLSX from 'xlsx';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
@Component({
    selector: 'algoritmos-test-socioeconomicos',
    templateUrl: './view.page.html',
    styleUrls: ['./styles.page.scss']
  })

export class TestSocioeconomicosPage {
  public me: any;
	public store: string = null;
  public loading: boolean = false;
  public data: any[];
  public fieldDataRadius: FormField = {
	  label: "Radio",
	  name: 'radius',
		placeholder: 'Pone el radio en metros',
		required: true
  };
  public fieldDataFile: FormField = {
		label: "Archivo Excel",
		name: 'file',
    placeholder: 'Selecciona un archivo de excel',
    accept:'.xlsx,.xls',
		required: true
  };
  public formGroup:FormGroup = new FormGroup({
    radius: new FormControl(500, Validators.required),
    file: new FormControl(null, {validators: [Validators.required]})
  });


  public associations: any[] = [];
  public horas:number[] = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
  public dias:string[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
  public site_data:any[] = [];
  public sites:any[] = [];
  public categories: any[] = [];
  public isLoading: boolean = false;

  constructor(
		private ws: WSService,
		private dialog: DialogService,
    private _snackBar: MatSnackBar) {
			
    }
    
    ngOnInit(): void {
    }

    getData(){
      this.isLoading = true;
      const formdata = this.formGroup.getRawValue();
      let workBook = null;
      let jsonData = null;
      const reader = new FileReader();
      const file = formdata.file;
      reader.onload = (event) => {
        const data = reader.result;
        workBook = XLSX.read(data, { type: 'binary' });
        jsonData = workBook.SheetNames.reduce((initial, name) => {
          const sheet = workBook.Sheets[name];
          initial[name] = XLSX.utils.sheet_to_json(sheet);
          return initial;
        }, {});
        let filteredData:any[] = [];
        let keys = Object.keys(jsonData);
        jsonData[keys[0]].map(dat => {
            dat.radius = parseInt(this.formGroup.get('radius').value)
            dat.latitude = parseFloat(dat.latitude);
            dat.longitude = parseFloat(dat.longitude);
            filteredData.push(dat);
        })
        this.wsCall(filteredData);
      }
      reader.readAsBinaryString(file);
    }

    wsCall(data){
      this.ws.getTestSocioeconomico(data).subscribe(result => {
        if(result === ''){
          this.openSnackBar("No hay datos en la respuesta")
        }
        this.isLoading = false;
      }, 
      err =>{
        this.openSnackBar("Ha habido un error al procesar los datos")
        this.isLoading = false;
      })
    }

    public openSnackBar(message: string) {
      this._snackBar.open(message,'', {
        duration: 1500,
      });
    }
}