import { AfterViewInit, Component, EventEmitter, Input, Output, ViewChild, SimpleChanges, AfterViewChecked } from '@angular/core';

@Component({
    selector: 'result-test-primetime',
    templateUrl: './view.page.html',
    styleUrls: ['./styles.page.scss']
  })

export class ResultTestPrimetimePage {
  @Input() data:any[] = [];
  @Input() cat_data:any[] = [];
  @Input() site_data:any = [];
  @Input() isLoading:boolean = false;
  @Input() isPainting:boolean = false;
  @Input() category:string;
  @Input() sites:any[] = [];
  @Input() categories: any[] = [];
  @Output() onLoaded: EventEmitter<boolean> = new EventEmitter(null);

  public me: any;
	public store: string = null;
  public loadingCategories: boolean = false;
  public loading: boolean = false;

  public associations: any[] = [];
  public horas:number[] = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
  public dias:string[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

  constructor() {}

  ngOnChanges(changes:SimpleChanges){
    if(changes.cat_data.currentValue.length > 0 && changes.site_data.currentValue.length > 0)
      this.onLoaded.emit(!this.isLoading);
  }
}