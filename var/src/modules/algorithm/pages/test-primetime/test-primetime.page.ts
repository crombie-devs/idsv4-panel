import { Component, Input, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { WSService } from 'src/services/ws.service';
import { FormField } from 'src/modules/form-field/models/form-field';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as XLSX from 'xlsx';
import moment from 'moment';
import { DialogService } from 'src/modules/dialog/services/dialog.service';

@Component({
    selector: 'algoritmos-test-primetime',
    templateUrl: './view.page.html',
    styleUrls: ['./styles.page.scss']
  })

export class TestPrimetimePage {
  public me: any;
	public store: string = null;
  public loadingCategories: boolean = false;
  public loading: boolean = false;
  public data: any[];
  public fieldDataCategory: FormField = {
	  label: "Categoría",
	  name: 'category',
		placeholder: 'Selecciona una categoría',
		options: [],
		multiple:false,
		required: true
  };
  public fieldDataFile: FormField = {
		label: "Archivo Excel",
		name: 'file',
    placeholder: 'Selecciona un archivo de excel',
    accept:'.xlsx,.xls',
		required: true
  };
  public formGroup:FormGroup = new FormGroup({
    category: new FormControl(null, Validators.required),
    file: new FormControl(null, {validators: [Validators.required]})
  });

  public associations: any[] = [];
  public horas:number[] = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
  public dias:string[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
  public cat_data:any[] = [];
  public site_data:any[] = [];
  public category:string;
  public sites:any[] = [];
  public categories: any[] = [];
  public isLoading: boolean = false;
  public isPainting: boolean = false;

  constructor(
		private ws: WSService,
		private dialog: DialogService,
    private _snackBar: MatSnackBar) {
			
    }
    
    ngOnInit(): void {
		  //this.getCategories();
    }

    public getCategories(){
        this.ws.getAssociationsCategories().subscribe(result => {
            if(result.success){
              if(result.data.length === 0){
                this.dialog.warning('Categorías', "No se han encontrado categorías");
                this.loadingCategories = false;
                return;
              }
              result.data.sort((a,b) => a.name > b.name ? 1 : -1)
              result.data.map(category => {
                this.categories.push({ name: category.name, value: category.code });
                this.loadingCategories = false;
              });
            }else{
              this.dialog.warning('Categorías', "No se han encontrado categorías");
              this.loadingCategories = false;
              return;
            }
        },
        error => {
          this.dialog.warning('Categorías', "Ha habido un error");
          this.loadingCategories = false;
          return;
        });
    }

    getData(){
      this.isLoading = true;
      setTimeout(() => {
        const formdata = this.formGroup.getRawValue();
        let workBook = null;
        let jsonData = null;
        const reader = new FileReader();
        const file = formdata.file;
        reader.onload = (event) => {
          const data = reader.result;
          workBook = XLSX.read(data, { type: 'binary' });
          jsonData = workBook.SheetNames.reduce((initial, name) => {
            const sheet = workBook.Sheets[name];
            initial[name] = XLSX.utils.sheet_to_json(sheet);
            return initial;
          }, {});
          let filteredData:any[] = [];
          let keys = Object.keys(jsonData);
          jsonData[keys[0]].map(dat => {
            if(dat.Idcategoria == formdata.category){
              filteredData.push(dat);
              this.category = dat.Nombrecategoria;
            }
          })
          let finalData:any[] = []
          filteredData.map(d => {
            if(!this.sites.some(s => s.idsite === d.Idcentro)){
              this.sites.push({idsite: d.Idcentro, name: d.Nombrecentro});
            }
            finalData.push({
                codestacion: d.Idcentro,
                estacion: d.Nombrecentro,
                idsubcategoriaN3: d.Idcategoria,
                subcategoriaN3: d.Nombrecategoria,
                importe_sum: d.Importe,
                fecha: moment((d.Fecha - (25567 + 1))*86400*1000).format("YYYY-MM-DD"),
                hora: d.Hora
            })
          })
          if(finalData.length > 0)
            this.wsCall(finalData);
          else{
            this.openSnackBar("La categoría seleccionada no contiene datos en la hoja excel. Selecciona otra categoría");
            this.isLoading = false;
          }
        }
        reader.readAsBinaryString(file);
      }, 1000)
    }

    wsCall(data){
      this.cat_data = [];
      this.site_data = [];
      this.ws.getTestPrimetime(data).subscribe(result => {
        if(result !== ''){
          this.cat_data = result.output_cat;
          this.site_data = result.output_est;
          const colorData = this.cat_data.sort((a,b) => (a.ventas_med > b.ventas_med) ? -1 : (b.ventas_med > a.ventas_med) ? 1 : 0);
          this.getColors(colorData);
          this.cat_data.sort((a,b) => a.ndiasem < b.ndiasem ? -1 : 1 );
          this.site_data.sort((a,b) => a.id_estac < b.id_estac ? -1 : 1);
          this.site_data.sort((a,b) => a.ndiasem < b.ndiasem ? -1 : 1);
          this.isLoading = false;
          this.isPainting = true;
        }else{
          this.openSnackBar("No hay datos en la respuesta")
        }
      }, 
      err =>{
        this.openSnackBar("Ha habido un error al procesar los datos")
        this.isLoading = false;
      })
    }
    compare( a, b ) {
      if ( a.ventas_med < b.ventas_med ){
        return -1;
      }
      if ( a.ventas_med > b.ventas_med ){
        return 1;
      }
      return 0;
    }
    public openSnackBar(message: string) {
      this._snackBar.open(message,'', {
        duration: 1500,
      });
    }	

    public onChangeFile(data){
      this.isLoading = true;
      this.isPainting = false;
      this.formGroup.patchValue({'category' : null});
      const formdata = this.formGroup.getRawValue();
      setTimeout(() => {
        this.fieldDataCategory.options = [];
        let workBook = null;
        let jsonData = null;
        const reader = new FileReader();
        const file = formdata.file;
        reader.onload = (event) => {
          const data = reader.result;
          workBook = XLSX.read(data, { type: 'binary' });
          jsonData = workBook.SheetNames.reduce((initial, name) => {
            const sheet = workBook.Sheets[name];
            initial[name] = XLSX.utils.sheet_to_json(sheet);
            return initial;
          }, {});
          let categories:any[] = []
          let keys = Object.keys(jsonData);
          jsonData[keys[0]].map(elem => {
            if(!categories.some(cat => cat.value === elem.Idcategoria)){
              categories.push({value: elem.Idcategoria, name: elem.Nombrecategoria})
            }
          })
          categories.map(dat => {
            this.fieldDataCategory.options.push(dat);
          })
          if(this.fieldDataCategory.options.length === 0)
            this.openSnackBar("No se han encontrado categorías en el archivo seleccionado");
        }
        reader.readAsBinaryString(file);
        this.isLoading = false;
      }, 1000)
    }

    private getColors(data){
      this.cat_data.map(cat => {
        const val = this.getVal(cat.ventas_med)
        if(val < 3){
          cat.textColor = '#000000'
        }else{
          cat.textColor = '#FFFFFF'
        }
        cat.color = this.degradado(val);
      })
    }
    private getVal(val){
      let final = 0;
      if(val >= 100 && val <= 150)
        final = 1;
      else if(val > 150 && val <= 200)
        final = 2;
      else if(val > 200 && val <= 250)
        final = 3;
      else if(val > 250 && val <= 300)
        final = 4;
      else if(val > 300)
        final = 5;
      
      return final;
    }
    private degradado(val){
      switch (val) {
          case 0:
            return '#FFFFFF';
            break;
          case 1:
              return '#A0FAB7';
              break;
          case 2:
              return '#33FF68';
              break;
          case 3:
              return '#2ECC71';
              break;
          case 4:
              return '#28B463';
              break;
          case 5:
              return '#145A32';
              break;
      }
  }
  public removeSpinner(isLoading){
    this.isLoading = false;
  }
}