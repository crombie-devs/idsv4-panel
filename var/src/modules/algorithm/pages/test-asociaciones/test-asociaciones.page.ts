import { Component, Input, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { WSService } from 'src/services/ws.service';
import { FormField } from 'src/modules/form-field/models/form-field';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as XLSX from 'xlsx';
import moment from 'moment';
import { DialogService } from 'src/modules/dialog/services/dialog.service';

@Component({
    selector: 'algoritmos-test-primetime',
    templateUrl: './view.page.html',
    styleUrls: ['./styles.page.scss']
  })

export class TestAsociacionesPage {
  public me: any;
	public store: string = null;
  public loadingCategories: boolean = true;
  public loading: boolean = false;
  public data: any[];
  public keys:string[];

  public fieldDataFile: FormField = {
		label: "Archivo Excel",
		name: 'file',
    placeholder: 'Selecciona un archivo de excel',
    accept:'.xlsx,.xls',
		required: true
  };
  public formGroup:FormGroup = new FormGroup({
    file: new FormControl(null, {validators: [Validators.required]})
  });


  public associations: any[] = [];
  public horas:number[] = [6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
  public dias:string[] = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
  public cat_data:any[] = [];
  public site_data:any[] = [];
  public category:string;
  public sites:any[] = [];
  public categories: any[] = [];
  public dataTable: any[] = [];

  constructor(
		private ws: WSService,
		private dialog: DialogService,
    private _snackBar: MatSnackBar) {
			
    }
    
    ngOnInit(): void {
    }

    getData(){
      this.loading = true;
      const formdata = this.formGroup.getRawValue();
      let workBook = null;
      let jsonData = null;
      const reader = new FileReader();
      const file = formdata.file;
      reader.onload = (event) => {
        const data = reader.result;
        workBook = XLSX.read(data, { type: 'binary' });
        jsonData = workBook.SheetNames.reduce((initial, name) => {
          const sheet = workBook.Sheets[name];
          initial[name] = XLSX.utils.sheet_to_json(sheet);
          return initial;
        }, {});
        let filteredData:any[] = [];
        let keys = Object.keys(jsonData);
        jsonData[keys[0]].map(dat => {
            filteredData.push(dat);
        })
        let finalData:any[] = []
        filteredData.map(d => {
          if(!this.categories.some(c => c.value === d.Idcategoria))
            this.categories.push({ name: d.Nombrecategoria, value: d.Idcategoria });

          finalData.push({
              codestacion: d.Idcentro,
              estacion: d.Nombrecentro,
              idsubcategoriaN3: d.Idcategoria,
              subcategoriaN3: d.Nombrecategoria,
              importe_sum: d.Importe,
              idtickets: d.Idticket,
              fecha: moment((d.Fecha - (25567 + 1))*86400*1000).format("YYYY-MM-DD"),
              hora: d.Hora
          })
        })
        this.categories.sort((a,b) => a.name < b.name ? -1 : 1);
        if(finalData.length > 0)
          this.wsCall(finalData);
        else
          this.openSnackBar("La categoría seleccionada no contiene datos en la hoja excel. Selecciona otra categoría");
      }
      reader.readAsBinaryString(file);
    }

    wsCall(data){
      this.cat_data = [];
      this.site_data = [];
      this.ws.getTestAsociaciones(data).subscribe(result => {
        this.dataTable = [];
        let resultOrdered:any[] = [];
        this.categories.map(cat =>{
          result.map(r => {
            if(r.idsubcategoriaN3 === cat.value)
              resultOrdered.push(r);
          })
        })
        resultOrdered.map((row,index) => {
          this.dataTable[index] = [];
          this.keys = Object.keys(row);
          this.keys.splice(this.keys.length - 1, 1);
          this.keys.map((key,index) => {
            this.categories.map(cat => {
              if(cat.value == key)
                this.keys[index] = cat.name;
            })
          })
          this.keys.sort((a,b) => a < b ? -1 : 1);
          let circle:boolean = false;
          this.keys.map(col => {
            this.categories.map(cat => {
              if(cat.name == col){
                if(row[cat.value] == '-' || row[cat.value] === NaN)
                  circle = true;
                if(circle)
                  this.dataTable[index].push({name:cat.name, idcategory:cat.value, value: row[cat.value]})
                else
                  this.dataTable[index].push({name:cat.name, idcategory:cat.value, value: row[cat.value], color: this.getColor(row[cat.value])})
              }
            })
          })
        })
        this.loading = false;
      }, 
      err =>{
        this.openSnackBar("Ha habido un error al procesar los datos")
        this.loading = false;
      })
    }

    private getColor(val){
      let color = '#000000';
      if(val >= 1.2)
        color = '#00FF13';
      else if(val < 1.2 && val >= 0.95)
        color = '#FFC100';
      else if( val < 0.95 && val >= 0.85)
        color = '#FF2700';
      else if( val < 0.85)
        color = '#000000';
      
      return color
    }
    public openSnackBar(message: string) {
      this._snackBar.open(message,'', {
        duration: 1500,
      });
    }	
  }