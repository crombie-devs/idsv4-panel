import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';

@Component({
    templateUrl: './algorithm.page.html',
    selector: 'algorithm-page',
    styleUrls: ['./algorithm.page.scss'],
  })
  
  export class AlgorithmPage {

    loading: boolean = true;
    route: string[] = [];
    link: string;
    origsize: boolean = true;
    src: SafeResourceUrl;

    constructor(
        private router: Router,
        private sanitizer: DomSanitizer,
        private translate: TranslateService
    ) {
        this.route = this.router.url.split('/');
        this.link = this.route[this.route.length - 1];
        this.src = this.sanitizer.bypassSecurityTrustResourceUrl(`assets/pdf/${this.link}_${this.translate.currentLang}.pdf#zoom=200%&toolbar=0&statusbar=0&scroll=0&view=FitBV,0,0`);;
    }

    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.

    }

}