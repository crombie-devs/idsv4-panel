import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/shared/shared.module';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FlexModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DirectivesModule } from 'src/directive/directives.module';
import { MaterialModule } from 'src/app/material.module';
import { AlgorithmRoutingModule } from './algorithm-routing.module';
import { ResultTestPrimetimePage } from './pages/test-primetime/result/result-test-primetime.page';
import { TestSocioeconomicosPage } from './pages/test-socioeconomicos/test-socioeconomicos.page';
import { TestAsociacionesPage } from './pages/test-asociaciones/test-asociaciones.page';
import { TestPrimetimePage } from './pages/test-primetime/test-primetime.page';
import { AlgorithmPage } from './pages/algorithm/algorithm.page';
import { FormFieldModule } from 'src/modules/form-field/form-field.module';
import { PipesModule } from 'src/pipes/pipes.module';
@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    TestSocioeconomicosPage, 
    TestAsociacionesPage, 
    TestPrimetimePage, 
    AlgorithmPage, 
    ResultTestPrimetimePage],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    FormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule,
    DirectivesModule,
    AlgorithmRoutingModule,
    PipesModule
  ],
  providers: [],
  exports: [ResultTestPrimetimePage]
})
export class AlgorithmModule {}