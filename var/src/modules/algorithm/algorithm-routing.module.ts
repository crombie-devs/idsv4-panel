import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestSocioeconomicosPage } from './pages/test-socioeconomicos/test-socioeconomicos.page';
import { TestAsociacionesPage } from './pages/test-asociaciones/test-asociaciones.page';
import { TestPrimetimePage } from './pages/test-primetime/test-primetime.page';
import { AlgorithmPage } from './pages/algorithm/algorithm.page';

const routes: Routes = [
  { 
    path: 'faqs', 
    component: AlgorithmPage, 
    data: { breadcrumb: "FAQ's" }
  },
  { 
    path: 'variables', 
    component: AlgorithmPage, 
    data: { breadcrumb: 'Variables' }
  },
  { path: 'resultados', 
    component: AlgorithmPage, 
    data: { breadcrumb: 'Resultados' }
  },
  {
    path: 'test-primetime',
    component: TestPrimetimePage,
    data: { breadcrumb: 'test algoritmo Primetime'}
  },
  {
    path: 'test-asociaciones',
    component: TestAsociacionesPage,
    data: { breadcrumb: 'test algoritmo de Asociaciones'}
  },
  {
    path: 'perfiles-socioeconomicos',
    component: TestSocioeconomicosPage,
    data: { breadcrumb: 'Perfiles socioeconómicos'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AlgorithmRoutingModule { }
