import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from 'src/shared/shared.module';
import { DirectivesModule } from 'src/directive/directives.module';
import { PipesModule } from 'src/pipes/pipes.module';

import { PlayareaRoutingModule } from './playarea-routing.module';

import { PlayareaStore } from 'src/modules/playarea/stores/playarea.store';

import { CONFIG } from 'src/app/app.config';
import moment from 'moment';

import { PlayareaDialog } from 'src/modules/playarea/dialogs/playarea/playarea.dialog';

import { PlayareasListPage } from 'src/modules/playarea/pages/list/list.page';
import { IndoorLocationModule } from 'src/modules/indoor-location/indoor-location.module';
import { FormatModule } from 'src/modules/format/format.module';

import { COMPONENTS } from './components/components';
import { AddPlayareaDialog } from './dialogs/add-areas/add-playarea.dialog';

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, `${CONFIG.lang_url}/i18n/`, `.json?timestamp=${moment().valueOf()}`);
}
@NgModule({
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    declarations: [
        COMPONENTS,
        PlayareaDialog,
        PlayareasListPage,
        AddPlayareaDialog
    ],
    imports: [
        CommonModule,
        PlayareaRoutingModule,
        MatProgressSpinnerModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient],
            },
        }),
        FlexModule,
        NgxMatFileInputModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        DirectivesModule,
        PipesModule,
        IndoorLocationModule,
        FormatModule
    ],
    providers: [PlayareaStore],
    entryComponents: [PlayareaDialog],
    exports: [
        COMPONENTS
    ]
})
export class PlayareaModule { }