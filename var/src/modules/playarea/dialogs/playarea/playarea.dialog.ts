import { Component, Inject } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { WSService } from 'src/services/ws.service';

import { DebugStore } from 'src/stores/debug.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { ContentTypeStore } from 'src/modules/content/stores/type.store';
import { ContentCategoryStore } from 'src/modules/content/stores/category.store';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { LocationStore } from 'src/modules/location/stores/location.store';

import { IResult } from 'src/models/iresult';
import { IPlaylogic, Playlogic } from 'src/modules/playlogic/models/playlogic';
import { ContentCategory } from 'src/modules/content/models/category';
import { ContentType } from 'src/modules/content/models/type';
import { User } from 'src/modules/user/models/user';
import { Format } from 'src/modules/format/models/format';
import { Location } from 'src/modules/location/models/location';

@Component({
	providers: [ContentTypeStore, ContentCategoryStore, FormatStore, LocationStore],
	selector: 'playarea-dialog',
  	templateUrl: './playarea.dialog.html',
  	styleUrls: ['./playarea.dialog.css']
})

export class PlayareaDialog {

	public title: string;
	public button: string;

	public form: FormGroup;
	public name: FormControl = new FormControl('');
	public idformat: FormControl = new FormControl('');
	public idlocation: FormControl = new FormControl('');
	public idcatergory: FormControl = new FormControl('');
	public idtype: FormControl = new FormControl('');
	public order: FormControl = new FormControl('');
	public area: FormControl = new FormControl('');

  	public loading: boolean = false;

  	public playlogics: Playlogic[] = [];

  	public me: User = null;
  	public categories: ContentCategory[];
	public types: ContentType[];
	public formats: Format[];
	public locations: Location[];

	constructor(
		@Inject(MAT_DIALOG_DATA) public data: any,
	  	public dialogRef: MatDialogRef<PlayareaDialog>,
		private fb: FormBuilder,
		private ws: WSService,
		private debug: DebugStore,
		private _snackBar: MatSnackBar,
		private ustore: UserStore,
		private fstore: FormatStore,
		private lstore: LocationStore,
		private tstore: ContentTypeStore,
		private cstore: ContentCategoryStore,
		private translate: TranslateService
    )
	{
		this.ustore.onChange.subscribe(user => { 
            this.me = user; 
		});
		
		this.tstore.onChange.subscribe(types => {
			this.types = types;			
		  });
	  
		this.cstore.onChange.subscribe(categories => {
			console.log(categories);
			this.categories = categories.filter(category => category.idcustomer === this.data.idcustomer);			
		});

		this.fstore.onChange.subscribe(formats => {
			this.formats = formats;
		});

		this.lstore.onChange.subscribe(locations => {
			this.locations = locations;
		});
	}

	public drop(event: CdkDragDrop<Playlogic[]>) {
		moveItemInArray(this.playlogics, event.previousIndex, event.currentIndex);
		this.playlogics = event.container.data;
		this.playlogics.forEach((p, index) => p.order = index);
	}	


	ngOnInit() {
		this.initForm();
		if(this.data.area === 0)
			this.title = this.translate.instant("Nueva área");		
		else{
			this.title = this.translate.instant("Editar área");
			this.getPlaylogicByArea(this.data.area);
		}
	}
  
	private initForm(){	
		this.form = this.fb.group({
			name: this.data.name,
			idlocation: this.data.location,
			idformat: this.data.format,
			idtype: 2,
			idcategory: '',
			area: this.data.area === 0?'':this.data.area
		});		 
	}
	
	private setName(){
		if (this.data.name === ''){
			let format = this.formats.find(f => f.idformat == this.data.format);	
			let location = this.locations.find(f => f.idlocation == this.data.location);
			this.form.get('name').setValue(location.name+" "+format.name);
		}

	}

	public getPlaylogicByArea(idarea: number){
		this.ws.playlogicToSelect().subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.playlogicToSelect', 'successful');							
				this.playlogics = result.data
								.filter(p => (p.area && p.area.idarea === idarea))
								.sort( (a, b) => a.order - b.order);				
			} else {            
				this.debug.addError('WS.playlogicToSelect', result.data);
			
			}
		}, error => {
			this.debug.addError('WS.playlogicToSelect', error.message);         
		});
	}	

	public save() {		
		if(this.form.valid && this.playlogics.length>0){
			this.setName();
			let result = {form:this.form.value, playlogics: this.playlogics };
			this.dialogRef.close(
				result
			);
		}else{
			this.openSnackBar('Complete los campos requeridos');
		} 
	}
	
	public close() {
		this.dialogRef.close(false);
	}

	public add(){
		let category: ContentCategory = this.categories.find(c => c.idcategory == this.form.get('idcategory').value);
		let type : ContentType = this.types.find(t => t.idtype == this.form.get('idtype').value);
		let obj: IPlaylogic = {
			order: this.playlogics.length, 
			area: null,
			content_category: category,
			customer: this.me.customer,
			type: type, 
			idarea: null,
			idcategory: category.idcategory,
			idcustomer: this.me.customer ? this.me.customer.idcustomer: this.data.idcustomer, 
			idtype: type.idtype
		}		
		this.playlogics.push(new Playlogic(obj));
		console.log(this.playlogics);
	}

	public delete(playlogic: Playlogic){
		let index = this.playlogics.findIndex(p => p.order === playlogic.order);
		this.playlogics.splice(index,1);		
   }

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

	public compareCategory(x: ContentCategory, y: ContentCategory): boolean {
		return x && y ? x.idcategory === y.idcategory : x === y;
	}

}
