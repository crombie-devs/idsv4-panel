import { Component, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { ContentTypeStore } from 'src/modules/content/stores/type.store';
import { ContentCategoryStore } from 'src/modules/content/stores/category.store';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { LocationStore } from 'src/modules/location/stores/location.store';
import { IResult } from 'src/models/iresult';
import { Playlogic } from 'src/modules/playlogic/models/playlogic';
import { ContentCategory } from 'src/modules/content/models/category';
import { ContentType } from 'src/modules/content/models/type';
import { User } from 'src/modules/user/models/user';
import { Format } from 'src/modules/format/models/format';
import { Location } from 'src/modules/location/models/location';
import { PlayareaStore } from '../../stores/playarea.store';
import { PlayArea } from '../../models/playarea';

@Component({
    providers: [ContentTypeStore, ContentCategoryStore, FormatStore, LocationStore],
    selector: 'add-playarea-dialog',
    templateUrl: './add-playarea.dialog.html',
    styleUrls: ['./add-playarea.dialog.css']
})

export class AddPlayareaDialog {

    public title: string;
    public button: string;
    public areas: PlayArea[] = [];
    public form: FormGroup;
    public area: FormControl = new FormControl('');
    public idformat: FormControl = new FormControl('');
    public idlocation: FormControl = new FormControl('');

    public loading: boolean = false;
    public playlogics: Playlogic[] = [];
    public me: User = null;
    public categories: ContentCategory[];
    public types: ContentType[];
    public formats: Format[];
    public locations: Location[];

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<AddPlayareaDialog>,
        private fb: FormBuilder,
        private ws: WSService,
        private debug: DebugStore,
        private _snackBar: MatSnackBar,
        private astore: PlayareaStore,
        private translate: TranslateService
    ) {
        this.astore.onChange.subscribe(areas => {
            this.areas = areas;

        });
    }



    public chargeBipa() {

        this.getPlaylogicByArea(this.form.value.area)
    }


    ngOnInit() {
        this.initForm();
        if (this.data.area === 0)
            this.title = this.translate.instant("Nueva área");
        else {
            this.title = this.translate.instant("Editar área");
            this.getPlaylogicByArea(this.data.area);
        }
    }

    private initForm() {
        this.form = this.fb.group({
            area: this.data.area === 0 ? '' : this.data.area,
            idlocation: this.data.location,
            idformat: this.data.format
        });
    }



    public getPlaylogicByArea(idarea: number) {
        this.ws.playlogicToSelect().subscribe((result: IResult) => {

            if (result.success) {
                this.debug.addInfo('WS.playlogicToSelect', 'successful');
                this.playlogics = result.data
                    .filter(p => (p.area && p.area.idarea == idarea))
                    .sort((a, b) => a.order - b.order);


            } else {
                this.debug.addError('WS.playlogicToSelect', result.data);

            }
        }, error => {
            this.debug.addError('WS.playlogicToSelect', error.message);
        });
    }

    public save() {
        if (this.form.valid && this.playlogics.length > 0) {
            let result = { form: this.form.value, playlogics: this.playlogics };

            this.dialogRef.close(
                result
            );
        } else {
            this.openSnackBar('Complete los campos requeridos');
        }
    }

    public close() {
        this.dialogRef.close(false);
    }


    public delete(playlogic: Playlogic) {
        let index = this.playlogics.findIndex(p => p.order === playlogic.order);
        this.playlogics.splice(index, 1);
    }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }

    public compareCategory(x: ContentCategory, y: ContentCategory): boolean {
        return x && y ? x.idcategory === y.idcategory : x === y;
    }

}
