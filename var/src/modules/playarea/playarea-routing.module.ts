
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlayareasListPage } from './pages/list/list.page';

const routes: Routes = [
  { path: 'playareas', component: PlayareasListPage, data: { breadcrumb: 'Áreas de reproducción' } }, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PlayareaRoutingModule { }
