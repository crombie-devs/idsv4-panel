import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

import { IResult } from 'src/models/iresult';
import { IPlayArea, PlayArea } from '../models/playarea';

import { WSService } from 'src/services/ws.service';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'playareas.store.ids';
@Injectable()
export class PlayareaStore {

    public onChange: BehaviorSubject<PlayArea[]> = new BehaviorSubject([]);
    private data: PlayArea[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const playareas: IPlayArea[] = JSON.parse(store); 
                this.set(playareas);
            } else 
                this.load();
        }else
            this.load();
    }

    public get(): PlayArea[] {
        return this.data;
    }

    public hasIndex(index: number): boolean {
        return (this.data.length > 0 && this.data.length > index)
    }

    public getByIndex(index: number): PlayArea {
        return (this.hasIndex(index))? this.data[index] : null;
    }

    public set(playareas: IPlayArea[]) {
        this.save(playareas);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.playareasToSelect().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }
        });
    }

    private save(playareas: IPlayArea[]) {
        this.data = playareas.map(a => new PlayArea(a));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(playareas));
    }
}