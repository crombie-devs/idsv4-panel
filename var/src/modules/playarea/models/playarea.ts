import { Format, IFormat } from "src/modules/format/models/format";
import { ILocation, Location } from "src/modules/location/models/location";
export interface IPlayArea {
    idarea: number;
    name: string;
    description: string;
    format: IFormat;
    location: ILocation;
    height: number;
    width: number;
}
export class PlayArea implements IPlayArea {
    idarea: number;
    name: string;
    description: string;
    format: Format;
    location: Location;
    height: number;
    width: number;
    is_selected: boolean = false;
    
    constructor(obj?: IPlayArea) {
        if(!obj)
            return;
            
        this.idarea = obj.idarea;
        this.name = obj.name;
        this.description = obj.description;
        this.format = new Format(obj.format);
        this.location = new Location(obj.location);
        this.height = obj.height;
        this.width = obj.width;
    }

    isEquals(area: IPlayArea) {
        return this.idarea === area.idarea;
    }
}