import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';

import { WSService } from 'src/services/ws.service';

import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { PlayareaDialog } from 'src/modules/playarea/dialogs/playarea/playarea.dialog';
import { LocationDialog } from 'src/modules/location/dialogs/location/location.dialog';

import { Playlogic } from 'src/modules/playlogic/models/playlogic';
import { IResult } from 'src/models/iresult';
import { User } from 'src/modules/user/models/user';
import { PlayArea } from 'src/modules/playarea/models/playarea';
import { Location } from 'src/modules/location/models/location';
import { Format } from 'src/modules/format/models/format';
import { Customer } from 'src/modules/customer/models/customer';

import { PlayareaStore } from 'src/modules/playarea/stores/playarea.store';
import { PlaylogicStore } from 'src/modules/playlogic/stores/playlogic.store';
import { FormatStore } from 'src/modules/format/stores/format.store';
import { LocationStore } from 'src/modules/location/stores/location.store';
import { DebugStore } from 'src/stores/debug.store';
import { UserStore } from 'src/modules/user/stores/user.store';
import { EventService } from 'src/shared/event.service';
import { CacheService, ICacheConfig } from 'src/services/cache.service';
import { AddPlayareaDialog } from '../../dialogs/add-areas/add-playarea.dialog';

@Component({
    providers: [PlayareaStore, PlaylogicStore, FormatStore, LocationStore],
    selector: 'app-playareas.list',
    templateUrl: './list.page.html',
    styleUrls: ['./list.page.scss']
})
export class PlayareasListPage implements OnInit {

    public newPlayarea: boolean = false;
    public newFormat: boolean = false;
    public newLocation: boolean = false;
    public editPlayarea: boolean = false;
    public deletedPlayarea: boolean = false;
    public customersToSelect: boolean = false;
    public permission: boolean = false;
    public load: boolean = true;
    public me: User;
    public pAreas: PlayArea | any;
    public formats: Format[] = [];
    public allLocations: Location[] = [];
    public locations: Location[] = [];
    public customers: Customer[] = [];
    public playAreasFull: PlayArea | any;
    public resultAreaFull: PlayArea | any;
    public idcustomer: number = 0;

    constructor(
        private ws: WSService,
        private debug: DebugStore,
        private ustore: UserStore,
        private _snackBar: MatSnackBar,
        private dialog: DialogService,
        private router: Router,
        private matDialog: MatDialog,
        private store: PlayareaStore,
        private playlogicStore: PlaylogicStore,
        private translate: TranslateService,
        private fstore: FormatStore,
        private lstore: LocationStore,
        private events: EventService,
        private cache: CacheService
    ) {
        this.lstore.onChange.subscribe(locations => {
            this.allLocations = locations;
            this.locations = locations;
            this.load = false;
        });
        this.store.onChange.subscribe(playareas => {
            this.permission = true;
            let results = playareas;
            this.resultAreaFull = results;
            let allAreas: PlayArea[] = [];
            results.forEach(function (res) {
                if (!allAreas[res.location.idlocation + ""]) allAreas[res.location.idlocation + ""] = {}
                allAreas[res.location.idlocation + ""][res.format.idformat + ""] = { nombre: res.name, descripcion: res.description, idarea: res.idarea, customer: res.location.idcustomer, img: res.format.image_url };
            }, this)
            this.playAreasFull = allAreas;
            this.pAreas = allAreas;
        })
        this.fstore.onChange.subscribe(formats => { this.formats = formats; });
        this.ustore.onChange.subscribe(result => { this.me = result; });
    }

    ngOnInit(): void {
        this.verifyPermissions();
    }

    public delete(location: number, format: number, idarea: number) {
        if (idarea == 0) {
            this.dialog.warning(this.translate.instant('Aviso'), this.translate.instant('Debe seleccionar al menos un usuario')).subscribe();
        } else {
            this.dialog.confirm(this.translate.instant('Confirmar Archivado'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
                if (confirm) {
                    this.ws.deletePlayarea(idarea).subscribe((result: IResult) => {
                        if (result.success) {
                            this.debug.addInfo('WS.deletePlayarea', 'successful');
                            this.openSnackBar(this.translate.instant('Playarea borrado correctamente'));
                            delete this.playAreasFull[location + ""][format + ""];
                            this.store.reload();
                        } else {
                            this.debug.addError('WS.deletePlayarea', result.data);
                            this.openSnackBar(result.data);
                        }
                    }, error => {
                        this.debug.addError('WS.deletePlayarea', error.message);
                        this.openSnackBar(this.translate.instant("No se ha podido eliminar el área porque tiene lógicas de reproducción asociadas"));
                    });
                }
            });
        }
    }

    private verifyPermissions() {
        this.newFormat = this.ustore.hasPermissionByName("post.format")
        this.newLocation = this.ustore.hasPermissionByName("post.indoorlocation")
        this.newPlayarea = this.ustore.hasPermissionByName("post.playarea")
        this.editPlayarea = this.ustore.hasPermissionByName("put.playarea.*")
        this.deletedPlayarea = this.ustore.hasPermissionByName("delete.playarea.*")
        this.customersToSelect = this.ustore.hasPermissionByName("get.customers.toselect")
    }


    public openSnackBar(message: string) {
        this._snackBar.open(message, '', {
            duration: 1500,
        });
    }

    public captureValue(idcustomer: number) {
        this.idcustomer = idcustomer;
        this.filter(idcustomer);
        this.filterLocations(idcustomer);
    }

    public filter(idcustomer) {
        this.pAreas = this.playAreasFull;
        if (idcustomer > 0) {
            let allAreas = [];
            this.resultAreaFull.forEach(function (res) {
                if (res.location.idcustomer == idcustomer) {
                    if (!allAreas[res.location.idlocation + ""]) allAreas[res.location.idlocation + ""] = {}
                    allAreas[res.location.idlocation + ""][res.format.idformat + ""] = { nombre: res.name, descripcion: res.description, idarea: res.idarea, customer: res.location.idcustomer, img: res.format.image_url };
                }
            }, this)
            this.pAreas = allAreas;
        } else {
            this.pAreas = this.playAreasFull;
        }
    }

    public filterLocations(idcustomer) {
        this.locations = [];
        if (idcustomer > 0) {
            this.allLocations.map(loc => {
                if (loc.idcustomer === idcustomer) {
                    this.locations.push(loc);
                }
            }, this)
        } else {
            this.locations = this.allLocations;
        }
    }

    public openDialog() {
        this.dialog.format('', this.translate.instant('Nuevo Formato'), '').subscribe(confirm => {
            if (confirm) {
                this.fstore.reload();
            }
        })
    }
    public openDialogLocation() {
        let ids = { idcustomer: this.idcustomer };
        this.dialog.location('', this.translate.instant('Nueva ubicación'), ids).subscribe(confirm => {
            if (confirm) {
                this.createIndoorLocation(confirm);
            }
        });
    }
    public openDialogArea(idformat = 0, idlocation = 0, idcustomer = 0, idarea = 0, name = '') {
        let ids = { format: idformat, location: idlocation, area: idarea, name: name, idcustomer: idcustomer };

        const dialogRef = this.matDialog.open(PlayareaDialog, { data: ids });
        dialogRef.afterClosed().subscribe((result: any) => {
            if (result != false) {
                if (ids.area > 0) {
                    this.updatePlayArea(ids.area, result.form, result.playlogics);
                } else {
                    this.createPlayArea(result.form, result.playlogics);
                }
                this.router.navigateByUrl('/private/playareas');
            }
        });

    }


    public openDialogIpa(idformat = 0, idlocation = 0, idcustomer = 0, idarea = 0, name = '') {

        let ids = { format: idformat, location: idlocation, area: idarea, name: name, idcustomer: idcustomer };

        const dialogRef = this.matDialog.open(AddPlayareaDialog, { data: ids });
        dialogRef.afterClosed().subscribe((result: any) => {
            if (result != false) {
                console.log(result)
                this.createPlayArea(result.form, result.playlogics);

                this.router.navigateByUrl('/private/playareas');
            }
        });

    }




    //playlogic
    public replacePlaylogic(idarea: number, playlogics: Playlogic[]) {
        this.ws.replacePlaylogicToArea(idarea, playlogics).subscribe((result: IResult) => {
            if (result.success) {
                let data = result.data;
                this.debug.addInfo('WS.replacePlaylogic', 'successful');
                this.openSnackBar("Área creada correctamente");
                this.playlogicStore.reload();
            }
            else {
                this.debug.addError('WS.replacePlaylogic', result.data);
                this.openSnackBar(result.data);

            }
        }, error => {
            this.debug.addError('WS.replacePlaylogic', error.message);
            this.openSnackBar(error.message);
        });
    }


    //Areas
    public createPlayArea(form, playlogics: Playlogic[]) {
        this.ws.createPlayarea(form).subscribe((result: IResult) => {
            if (result.success) {
                let data = result.data;
                this.debug.addInfo('WS.createPlayArea', 'successful');
                if (playlogics && playlogics.length > 0) {
                    playlogics.forEach(p => p.idarea = result.data.idarea);
                    this.replacePlaylogic(result.data.idarea, playlogics);
                }
                if (!this.playAreasFull[data.idlocation + ""]) this.playAreasFull[data.idlocation + ""] = {};
                //this.getPlayAreas();
                let config: ICacheConfig = { idcustomer: this.me.customer.idcustomer };
                if (this.me.site)
                    config = { idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer };
                setTimeout(() => {
                    this.events.publish('playlogic:change', config);
                }, 1000);
                this.store.reload();
            }
            else {
                this.debug.addError('WS.createPlayArea', result.data);
                this.openSnackBar(result.data);
            }
        }, error => {
            this.debug.addError('WS.createPlayArea', error.message);
            this.openSnackBar(error.message);
        });
    }

    public updatePlayArea(id: number, form: any, playlogics: Playlogic[]) {
        this.ws.updatePlayarea(id, form).subscribe((result: IResult) => {
            if (result.success) {
                let data = result.data;
                this.debug.addInfo('WS.updatePlayarea', 'successful');
                if (playlogics && playlogics.length > 0) {
                    playlogics.forEach(p => p.idarea = result.data.idarea);
                    this.replacePlaylogic(result.data.idarea, playlogics);
                }
                //this.getPlayAreas();
                let config: ICacheConfig = { idcustomer: this.me.customer.idcustomer };
                if (this.me.site)
                    config = { idsite: this.me.site.idsite, idcustomer: this.me.customer.idcustomer };
                setTimeout(() => {
                    this.events.publish('playlogic:change', config);
                }, 1000);
                this.store.reload();
            }
            else {
                this.debug.addError('WS.updatePlayarea', result.data);
                this.openSnackBar(result.data);

            }
        }, error => {
            this.debug.addError('WS.updatePlayarea', error.message);
            this.openSnackBar(error.message);
        });

    }

    //Formats
    public createFormat(form) {
        this.ws.createFormat(form).subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.createFormat', 'successful');
                this.openSnackBar("Formato creado correctamente");
                this.store.reload();
                this.lstore.reload()
                this.fstore.reload();
            }
            else {
                this.debug.addError('WS.createFormat', result.data);
                this.openSnackBar(result.data);
            }
        }, error => {
            this.debug.addError('WS.createFormat', error.message);
            this.openSnackBar(error.message);
        });
    }

    public hideFormat(format: Format) {
        this.dialog.confirm('Confirmar ocultación', `¿Desea ocultar el formato "${format.name}"?`).subscribe(confirm => {
            if (!confirm) { return; }

            this.ws.hideFormat(this.me.customer.idcustomer, format.idformat).subscribe(result => {
                this.openSnackBar("Formato ocultado correctamente");
                this.fstore.reload();
                //this.getFormats();
            });
        });
    }

    //IndoorLocation
    public createIndoorLocation(form) {
        this.ws.createIndoorLocation(form).subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.createIndoorLocation', 'successful');
                this.openSnackBar("Formato creado correctamente");
                this.locations.push(result.data);
            }
            else {
                this.debug.addError('WS.createIndoorLocation', result.data);
                this.openSnackBar(result.data);
            }
        }, error => {
            this.debug.addError('WS.createIndoorLocation', error.message);
            this.openSnackBar(error.message);
        });
    }

    private updateLocation(location: Location) {
        this.ws.updateIndoorLocation(location.idlocation, { name: location.name }).subscribe(result => {
            if (result.success)
                this.openSnackBar("Ubicación actualizada con éxito");
            else
                this.openSnackBar(result.data);
        });
    }

    public openEditLocation(location: Location) {
        const dialogRef = this.matDialog.open(LocationDialog, { data: location });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                location.name = result.name;
                this.updateLocation(location);
            }
        });
    }

    public deleteLocation(location: Location) {
        this.dialog.confirm(this.translate.instant('Confirmar Archivado'), this.translate.instant('¿Desea archivar los elementos seleccionados?')).subscribe(confirm => {
            if (!confirm) { return; }

            this.ws.deleteIndoorLocation(location.idlocation).subscribe(result => {
                if (result.success) {
                    this.openSnackBar("Ubicación eliminada correctamente");
                    this.lstore.reload();
                } else {
                    this.openSnackBar(result.data);
                }
            });
        });
    }
}
