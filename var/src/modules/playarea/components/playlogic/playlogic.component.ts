import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ContentCategory } from 'src/modules/content/models/category';
import { Playlogic } from 'src/modules/playlogic/models/playlogic';


@Component({
    selector: 'playlogic',
    templateUrl: './playlogic.component.html',
    styleUrls: ['./playlogic.component.scss']
  })
export class PlaylogicComponent {
    
    @Input() playlogic: Playlogic;
    @Input() show: boolean = true;
    @Input() arrow: boolean = true;
    @Output() onView = new EventEmitter<ContentCategory>()

    view(category: ContentCategory){
      this.onView.emit(category);
    }
  
}

