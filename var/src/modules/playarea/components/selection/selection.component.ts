import { Component, Input, Output, EventEmitter, SimpleChanges, OnInit, AfterViewInit } from '@angular/core';

import { PlayArea } from 'src/modules/playarea/models/playarea';
import { PlayareaStore } from '../../stores/playarea.store';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { CONFIG } from 'src/app/app.config';
@Component({
    selector: 'playarea-selection',
    templateUrl: './selection.component.html',
    styleUrls: ['./selection.component.scss']
})

export class PlayAreaSelectionComponent implements OnInit {

    @Input() selected?: PlayArea[] = [];
    @Input() filtered?: number[] = [];
    @Input() disabled: boolean = false;
    @Input() recomendations?: boolean = false;
    @Output() onChange = new EventEmitter<PlayArea[]>();

    public load: boolean = false;
    public areas: PlayArea[] = [];
    private all_areas: PlayArea[] = [];

    constructor(private pastore: PlayareaStore) {
        this.load = true;
        this.pastore.onChange.subscribe(playareas => {
            this.all_areas = playareas;
            this.load = false;
            this.filterAreas();
        });
    }

    ngOnInit() {
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.disabled && changes.disabled.currentValue) {
            this.onChange.emit([]);
        }
        if (changes.filtered) {
            this.filterAreas();
        }
    }

    private selectAreas() {
        this.selectArea(this.areas[Math.floor(Math.random() * (this.areas.length - 1))]);
        this.selectArea(this.areas[Math.floor(Math.random() * (this.areas.length - 1))])
    }
    public checkboxToggle(event: MatCheckboxChange) {
        if (this.hasSelected() && !this.hasAllSelected()) {
            event.source.checked = false;
        }

        if (event.source.checked) {
            this.changeSelection(this.areas);
        } else {
            this.changeSelection([]);
        }
    }

    private changeSelection(areas: PlayArea[]) {
        this.selected = areas;
        this.onChange.emit(this.selected);
    }

    public selectArea(area: PlayArea) {
        if (area) {
            if (this.isSelected(area))
                this.selected = this.selected.filter(ar => ar.idarea !== area.idarea);
            else
                this.selected.push(area);
            this.onChange.emit(this.selected);
        }
    }

    public isSelected(area: PlayArea): boolean {
        return this.selected.length > 0 && area && this.selected.some(ar => ar.idarea === area.idarea);
    }

    public hasSelected(): boolean {
        return (this.selected && this.selected.length > 0);
    }

    public hasAllSelected(): boolean {
        return (this.selected && this.selected.length > 0 && this.selected.length === this.areas.length)
    }

    public filterAreas() {
        if (this.filtered.length > 0 && this.all_areas.length > 0) {
            this.selected = (this.filtered && this.filtered.length > 0) ? this.selected.filter(pa => this.filtered.some(idarea => idarea === pa.idarea)) : this.selected;
            this.areas = (this.filtered && this.filtered.length > 0) ? this.all_areas.filter(pa => this.filtered.some(idarea => idarea === pa.idarea)).filter(a => a.format.broadcasting == 0) : [];
        }
        console.log(this.areas)
        if (CONFIG.demo && this.recomendations && this.areas.length > 0)
            this.selectAreas();
        if (this.selected.length > 0)
            this.onChange.emit(this.selected);
    }
}

