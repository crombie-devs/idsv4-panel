import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { PlayAreaSelectionComponent } from './selection.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';

import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

import { MaterialModule } from 'src/app/material.module';
import { OverlayModule } from '@angular/cdk/overlay';

import { WSService } from 'src/services/ws.service';
import { HttpClient } from "@angular/common/http";
import { HttpLoaderFactory } from "src/shared/shared.module";
import { DebugStore } from 'src/stores/debug.store';
import { UserModule} from 'src/modules/user/user.module';
import moment from 'moment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogModule } from 'src/modules/dialog/dialog.module';
import { MatCheckbox } from "@angular/material/checkbox";
import { PlayareaStore } from 'src/modules/playarea/stores/playarea.store';

describe('Componente: PlayAreaSelectionComponent', () => {
    let component: PlayAreaSelectionComponent;
    let fixture: ComponentFixture<PlayAreaSelectionComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports:[ 
                UserModule,
                MaterialModule,
                OverlayModule,
                RouterTestingModule,
                BrowserAnimationsModule,
                DialogModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                }),
                HttpClientTestingModule],
            declarations: [PlayAreaSelectionComponent],
            providers:[WSService, PlayareaStore],
            schemas:[CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        }).compileComponents();
    });

    beforeEach(async(() => {
        fixture = TestBed.createComponent(PlayAreaSelectionComponent);
        component = fixture.componentInstance;
        fixture.whenStable().then(() => {
            fixture.detectChanges();
        });
    }));

    it("Creando componente", () => {
        expect(component).toBeTruthy();
    });
});