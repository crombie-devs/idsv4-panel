import { PlayAreaSelectComponent } from './select/select.component';
import { PlayAreaSelectShowComponent } from './select-show/select-show.component';

import { PlayAreaMultiselectComponent } from './multiselect/multiselect.component';
import { PlayAreaSelectionComponent } from './selection/selection.component';
import { PlayAreaOptionComponent } from './option/option.component';
import { PlaylogicComponent } from './playlogic/playlogic.component';

export const COMPONENTS = [
    PlayAreaSelectComponent,
    PlayAreaSelectShowComponent,
    PlayAreaMultiselectComponent,
    PlayAreaSelectionComponent,
    PlayAreaOptionComponent,
    PlaylogicComponent
];
