import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PlayArea } from '../../models/playarea';
@Component({
  selector: 'playarea-option',
  templateUrl: './option.component.html',
  styleUrls: ['./option.component.scss']
})
export class PlayAreaOptionComponent implements OnInit {
  
  @Input() area: PlayArea;
  @Input() selected: boolean;
  @Output() onSelect = new EventEmitter<boolean>()

  constructor() {}

  ngOnInit(): void {}

  public select() {
    this.selected = !this.selected;
		this.onSelect.emit(this.selected);
  }

}
