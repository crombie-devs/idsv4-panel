import { Component, EventEmitter, Input, Output, OnInit } from "@angular/core";

import { PlayareaStore } from 'src/modules/playarea/stores/playarea.store';
import { PlayArea } from 'src/modules/playarea/models/playarea';

@Component({
    providers: [PlayareaStore],
    selector: 'playarea-multiselect',
    templateUrl: './multiselect.component.html',
    styleUrls: ['./multiselect.component.scss']
  })
export class PlayAreaMultiselectComponent implements OnInit {
   
    @Output() onChange: EventEmitter<PlayArea[]> = new EventEmitter();
    @Input() selected: PlayArea[] = [];

    public playareas: PlayArea[] = [];

    constructor(private astore: PlayareaStore){}

    ngOnInit() {
        this.astore.onChange.subscribe(playareas => {
            this.playareas = playareas;			
        });
    }

    onSelect(area: PlayArea) {
        this.selected.push(area); 
    }

    checkSelect(area: PlayArea){
        return this.selected.some(a => area.isEquals(a));
    }

}