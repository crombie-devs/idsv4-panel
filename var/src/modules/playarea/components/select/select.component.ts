import { Component, EventEmitter, Input, Output, OnInit, SimpleChanges } from "@angular/core";
import { PlayArea } from 'src/modules/playarea/models/playarea';
import { PlayareaStore } from 'src/modules/playarea/stores/playarea.store';

@Component({
    providers: [PlayareaStore],
    selector: 'playarea-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.scss']
  })

export class PlayAreaSelectComponent implements OnInit {

    @Output() onChange: EventEmitter<PlayArea> = new EventEmitter<PlayArea>();
    @Input() selected: PlayArea = null;
    @Input() idcustomer:number = 0;
    @Input() selectedIndex: number;
    @Input() error: string = "#ddd";

    public areas: PlayArea[] = [];
    public allAreas: PlayArea[] = [];
    loading: boolean = true;

    constructor(public playareas: PlayareaStore){}

    ngOnInit() {
        this.playareas.onChange.subscribe(result => {
            if(result.length > 0) {
                this.loading = false;
                this.allAreas = result;
                this.areas = this.allAreas;
                if(!this.selected && this.selectedIndex >= 0)
                    this.onSelectIndex(this.selectedIndex);
            }
        });
    }
    
    setError(color): void {
        this.error = color;
    }

    ngOnChanges(changes: SimpleChanges): void {
        //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
        //Add '${implements OnChanges}' to the class. 
        if(changes.idcustomer){
            if (Number(changes.idcustomer.currentValue)!=0)
                this.areas = this.allAreas.filter(area => area.location.idcustomer === changes.idcustomer.currentValue);
            else    
                this.areas = this.allAreas; 
        }
    }

    onSelectIndex(index: number) {
        if(!this.playareas.hasIndex(index)){ return; }
        const playarea = this.playareas.getByIndex(index);
        this.onSelect(playarea);
    }

    onSelect(area: PlayArea){
        this.error = "#ddd";
        this.selected = (this.selected != null && this.selected.isEquals(area))? null : area;
        this.onChange.emit(this.selected);
    }

    checkSelect(area: PlayArea){
        if(this.selected)
            return this.selected.isEquals(area);
        return false;
    }

}