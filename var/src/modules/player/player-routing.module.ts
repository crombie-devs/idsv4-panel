import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlayerFormPage } from './pages/form/form.page';
import { PlayerListPage } from './pages/list/list.page';

const routes: Routes = [
  { path: 'list', component: PlayerListPage },
  { path: 'create', component: PlayerFormPage, data: { breadcrumb: 'Nuevo visor' }},
  { path: 'edit/:idsite', component: PlayerFormPage, data: { breadcrumb: 'Editar visor' }}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PlayerRoutingModule { }
