import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IResult } from 'src/models/iresult';
import { WSService } from 'src/services/ws.service';
import { IPlayer, Player } from 'src/modules/player/models/player';
import { environment } from "src/environments/environment";

const STORE_KEY: string = 'players.store.ids';

@Injectable()
export class PlayerStore {

    public onChange: BehaviorSubject<Player[]> = new BehaviorSubject([]);
    private data: Player[] = [];

    constructor(private ws: WSService) {
        if(environment.localstorage){
            let store = sessionStorage.getItem(STORE_KEY);
            if(store) {
                const players: IPlayer[] = JSON.parse(store); 
                this.set(players);
            } else 
                this.load();
        } else
            this.load();
    }

    public get(): Player[] {
        return this.data;
    }

    public set(players: IPlayer[]) {
        this.save(players);
        this.onChange.next(this.data);
    }

    public reload() {
        this.load();
    }

    private load() {
        this.ws.playerToSelect().subscribe((result: IResult) => {
            if(result.success) {
              this.set(result.data);
            }
        });
    }

    private save(players: IPlayer[]) {
        this.data = players.map(a => new Player(a));
        if(environment.localstorage)
            sessionStorage.setItem(STORE_KEY, JSON.stringify(players));
    }

    public filterByArea(idarea: number){
        return this.data.filter(p=> p.idarea === idarea);
    }

    public filterBySite(idsite: number){
        return this.data.filter(p=> p.idsite === idsite);
    }

}