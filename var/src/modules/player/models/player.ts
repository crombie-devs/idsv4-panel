import { Site } from 'src/modules/site/models/site';
import { IPlayArea, PlayArea } from 'src/modules/playarea/models/playarea';
import { ITag, Tag } from 'src/modules/tag/models/tag';
import { Power } from 'src/modules/power/models/power';
import { Lang } from 'src/modules/lang/models/lang';
import { Customer, ICustomer } from "src/modules/customer/models/customer";
import { Template } from "src/modules/template/models/template";
import { PlayCircuit } from "src/modules/playcircuit/models/playcircuit";



export interface IPlayer {
    idplayer: number;
    code: string;
    email: string;
    idsite: number;
    area: IPlayArea;
    idlang: number;
    tags: ITag[];
    powers: Power[];
    idarea: number;
    site: Site;
    customer: ICustomer;
    status: boolean;
    is_sync: boolean;
    idtemplate: Template;
    instalation_date: string;
    support_end_date: string;
    hardware_type: string;
    software_version: string;
    serial_number: string;
    screen_resolution: string;
    installer_name: string;
    connected_point_number: number;
    is_monitored: number;
}

export class Player implements IPlayer {
    idplayer: number;
    code: string;
    email: string;
    idsite: number;
    area: PlayArea;
    idlang: number;
    tags: ITag[];
    powers: Power[];
    idarea: number;
    site: Site;
    customer: Customer;
    status: boolean;
    is_sync: boolean;
    idtemplate: Template;
    instalation_date: string;
    support_end_date: string;
    hardware_type: string;
    software_version: string;
    serial_number: string;
    screen_resolution: string;
    installer_name: string;
    connected_point_number: number;
    is_monitored: number;

    constructor(obj?: IPlayer) {
        if (!obj)
            return;

        this.idplayer = obj.idplayer;
        this.code = obj.code;
        this.email = obj.email;
        this.area = new PlayArea(obj.area);
        this.idsite = obj.idsite;
        this.site = obj.site ? new Site(obj.site) : null;
        this.customer = new Customer(obj.customer);
        this.idlang = obj.idlang;
        if (obj.tags)
            this.tags = obj.tags.map(t => new Tag(t));
        if (obj.powers)
            this.powers = obj.powers.map(p => new Power(p));
        this.idarea = obj.idarea;
        this.status = obj.status;
        this.is_sync = obj.is_sync;
        this.idtemplate = obj.idtemplate;
        this.instalation_date = obj.instalation_date;
        this.support_end_date = obj.support_end_date;
        this.hardware_type = obj.hardware_type;
        this.software_version = obj.software_version;
        this.serial_number = obj.serial_number;
        this.screen_resolution = obj.screen_resolution;
        this.installer_name = obj.installer_name;
        this.connected_point_number = obj.connected_point_number;
        this.is_monitored = obj.is_monitored;
    }

    isEquals(player: IPlayer): boolean {
        return this.idplayer === player.idplayer;
    }

    checkCircuits(circuits: PlayCircuit[]): boolean {
        return circuits.some(c => this.checkCircuit(c));
    }

    checkCircuit(circuit: PlayCircuit): boolean {
        const checkSites: boolean = this.checkSites(circuit.sites);
        const checkLangs: boolean = this.checkLangs(circuit.langs);
        return circuit.sites.some(s => s.idsite === this.idsite);
    }

    checkSites(sites: Site[]): boolean {
        if (sites && sites.length > 0) {
            return sites.some(s => s.idsite == this.idsite);
        }
        return true;

    }

    checkLangs(langs: Lang[]): boolean {
        if (langs && langs.length > 0) {
            return langs.some(s => s.idlang == this.idlang);
        }
        return true;
    }

    checkAddresses(circuits: PlayCircuit[]): boolean {
        return circuits.some(c => this.checkAddress(c));
    }


    checkAddress(circuit: PlayCircuit): boolean {



        const checkCountry = this.checkCountry(circuit.sites);
        const checkProvince = this.checkProvince(circuit);
        const checkCity = this.checkCity(circuit.sites);
        return true;
        // return address.some(s => s.idlang == this.idlang);
    }


    checkCountry(sites: Site[]) {
        if (sites && sites.length > 0) {
            return sites.some(s => s.idcountry === this.site.province.idcountry);
        }
        return true;
    }

    checkProvince(circuit: PlayCircuit) {
        if (circuit && circuit.provinces) {
            return circuit.provinces.some(s => s.idprovince === this.site.province.idprovince);
        }
        return true;


    }

    checkCity(sites: Site[]) {
        if (sites && sites.length > 0) {
            return sites.some(s => s.idcity === this.site.idcity && s.idsite === this.idsite);
        }
        return true;

    }





}
