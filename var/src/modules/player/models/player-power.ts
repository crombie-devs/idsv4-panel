export interface IPlayerPower {
    idplayer: number;
    weekday: number;
    time_on: string;
    time_off: string;
    new?:boolean;
}

export class PlayerPower implements IPlayerPower {
    idplayer: number;
    weekday: number;
    time_on: string;
    time_off: string;
    new?:boolean;
    
    constructor(obj?: any) {
        if(!obj)
            return;
            
        this.idplayer = obj.idplayer;
        this.weekday = obj.weekday;
        this.time_on = obj.time_on;
        this.time_off = obj.time_off;       
        this.new = obj.new;
    }

   
}