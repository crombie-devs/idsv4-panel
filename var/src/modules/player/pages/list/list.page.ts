import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Player } from 'src/modules/player/models/player';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { Router } from '@angular/router';
import { Sort } from '@angular/material/sort';
import { MYString } from 'src/libs/string.libs';
import { PlayerService } from '../../services/player.service';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatInput } from '@angular/material/input';
import { from } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { ICacheConfig } from 'src/services/cache.service';
import { EventService } from 'src/shared/event.service';

@Component({
  selector: 'player-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss']
})

export class PlayerListPage {

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
    @ViewChildren('table') table: QueryList<MatCheckbox>
    @ViewChild(MatInput) input: MatInput;
  
	public displayedColumns: string[] = ['select', 'idplayer',  'email', 'site', 'status'];
	public selection = new SelectionModel<Player>(true, []);
	public dataSource = new MatTableDataSource<Player>();
	public permission: boolean = false;
	public load: boolean = true;
	public new: boolean = false;
	public edit: boolean = false;
	public deleted: boolean = false;
	public customersToSelect: boolean = false;
	public me: any;	
	sortedData: Player[];
	players: Player[];
    data: Player[];

	constructor(
		private ws: PlayerService,
		private debug: DebugStore,
		private ustore: UserStore,
		private _snackBar: MatSnackBar,
		private dialog: DialogService,
		private router: Router,
        private _player: PlayerService,
		private events: EventService, 
		) {
			this.ustore.onChange.subscribe(user => { 
				this.me = user; 
			});			
  	}
  
  	public ngOnInit() {
		this.debug.addInfo('PlayerListComponent', 'OnInit');
		this.verifyPermissions();
		this.getPlayers();
	}

    public changeStatus(event) {
        this._player.changeStatus(event).subscribe( item => {
			if (this.me.customer) {
				let config: ICacheConfig = {
					idplayer: event,
					idcustomer: this.me.customer.idcustomer
				};
				setTimeout(() => {
					this.events.publish('player:change', config);
				}, 1000);
			}
        });
	}
	public getPlayers(){
		this.ws.list().subscribe((players: Player[]) => {
				this.debug.addInfo('WS.players', 'successful');
				this.permission = true;                
                this.data = players;
				this.renderDataTable(players);
				this.load = false;
		}, error => {  
			this.load = false;
			return null;          
		}); 	
	}
	  
	public renderDataTable(data: Player []) {
		this.dataSource = new MatTableDataSource(data);
		this.dataSource.filterPredicate = (data: Player, filter: string): boolean => {
			const dataStr = MYString.removeAccents(Object.keys(data).reduce((currentTerm: string, key: string) => {
			  return (currentTerm + (data as { [key: string]: any })[key] + '◬');
			}, '')).trim().toLowerCase();	  
			return dataStr.indexOf(MYString.removeAccents(filter.trim().toLowerCase())) != -1;
		};

		setTimeout(() => {
			this.dataSource.paginator = this.paginator;
		}, 500);

        if(this.input)
            this.applyFilter(this.input.value);

    	this.sortedData = data.slice();
		this.players = data;
		this.dataSource.sort = this.sort;
		this.load = false;
	}

	isAllSelected() {
		if(this.dataSource.paginator){
			const numSelected = this.selection.selected.length;
			const page = this.dataSource.paginator.pageSize;
			let endIndex: number;
			if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
			endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
			} else {
			endIndex = this.dataSource.data.length - (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize);
			}
			return numSelected === endIndex;
		}
      }

    masterToggle() {
        this.isAllSelected() ?
          this.selection.clear() : this.selectRows();
    }

    selectRows() {
        let endIndex: number;
        if (this.dataSource.data.length > (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize) {
          endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
        } else {
          endIndex = this.dataSource.data.length;
        }        
        for (let index = (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize); index < endIndex; index++) {
          this.selection.select(this.dataSource.data[index]);
        }
    }

	public checkboxLabel(row?: Player): string {
		if (!row) {
			return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
		}
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idplayer + 1}`;
	}


	public delete(){
        let playersByDelete = [];
		this.table.toArray().forEach(item => {
			if(item.checked) 
				playersByDelete.push(item.value);			
	    });	
		if(playersByDelete.length == 0){
			this.dialog.warning('Aviso', 'Debe seleccionar al menos un visor').subscribe();
		}else{
			this.dialog.confirm('Confirmar Borrado', '¿Desea borrar los elementos seleccionados?').subscribe(confirm => {
				if (confirm){
                    this.deleteByIds(playersByDelete);   
				}
			});
		}
	}

	public update(){
		if (this.selection.selected.length>1){
			this.dialog.warning('Aviso', 'Sólo puede editar un visor').subscribe();
		}else if(this.selection.selected.length == 0){
			this.dialog.warning('Aviso', 'Debe seleccionar un visor').subscribe();
		}else{
			this.router.navigateByUrl('/private/player/edit/'+this.selection.selected[0].idplayer);
		}
	}

	public openSnackBar(message: string) {
		this._snackBar.open(message,'', {
		  duration: 1500,
		});
	}

	public applyFilter(event: string) {
        this.selection.clear();
        if(!event) 
			this.dataSource.data = this.data;	
        else{    
            if(typeof event == 'string'){
                this.dataSource.data = this.data.filter(player => 
					player.email.toLowerCase().includes(event.trim().toLowerCase()) || 
					player.idplayer.toString().includes(event.trim()) || 
					(player.site && player.site.name.toLowerCase().includes(event.trim().toLowerCase())) || 
					(player.code && player.code.toString().includes(event.trim()))
				);
               
            }
        }
	}

	private verifyPermissions() {
		this.new = this.ustore.hasPermissionByName("post.player");
		this.edit = this.ustore.hasPermissionByName("put.player.*");
		this.deleted = this.ustore.hasPermissionByName("delete.player.*");
		this.customersToSelect = this.ustore.hasPermissionByName("get.customers.toselect");
	}

	ngAfterViewInit (){
		setTimeout(() => {
			this.dataSource.paginator = this.paginator;
		  }, 500) 
		this.dataSource.sort = this.sort;
	}

	sortData(sort: Sort) {
		const data = this.players.slice();
		if (!sort.active || sort.direction === '') {
		  this.sortedData = data;
		  return;
		}
	
		this.sortedData = data.sort((a, b) => {
		  const isAsc = sort.direction === 'asc';
		  switch (sort.active) {
			case 'idplayer': return this.compare(a.idplayer, b.idplayer, isAsc);
			case 'code': return this.compare(a.code?a.code:'', b.code?b.code:'', isAsc);
			case 'email': return this.compare(a.email?a.email:'', b.email?b.email:'', isAsc);
			case 'site': return this.compare(a.site?a.site.name:'', b.site?b.site.name:'', isAsc);
			case 'status': return this.compare(a.status.toString(), b.status.toString(), isAsc);
			default: return 0;
		  }
		});
		this.dataSource.data = this.sortedData;
	}

	compare(a: number | string, b: number | string, isAsc: boolean) {
		return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
	}

    private deleteByIds(ids: number[]) {
        from(ids).pipe(
          concatMap(id => this.ws.delete(id))
        ).subscribe(player => {
                this.load = true;
				if (this.me.customer) {
                    let config: ICacheConfig = {
                        idplayer: player.idplayer,
                        idcustomer: this.me.customer.idcustomer
                    };
                    setTimeout(() => {
                        this.events.publish('player:change', config);
                    }, 1000);
                }
            }, 
            error => {
                this.openSnackBar(error.message);
                this.getPlayers();
            },
            () => {                       
                this.openSnackBar('Los visores seleccionados han sido archivados correctamente');                       
                this.selection.clear(); 
                this.getPlayers();
          }); 
    }
}
