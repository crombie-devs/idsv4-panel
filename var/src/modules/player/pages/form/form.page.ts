import { Component, ViewChild } from '@angular/core';
import { WSService } from 'src/services/ws.service';
import { DebugStore } from 'src/stores/debug.store';
import { IResult } from 'src/models/iresult';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { Customer } from 'src/modules/customer/models/customer';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Site } from 'src/modules/site/models/site';
import { MapInfoWindow } from '@angular/google-maps';
import { ITag } from 'src/modules/tag/models/tag';
import { DialogService } from 'src/modules/dialog/services/dialog.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import { Player } from 'src/modules/player/models/player';
import { Lang } from 'src/modules/lang/models/lang';
import { PlayArea } from 'src/modules/playarea/models/playarea';
import { LangStore } from 'src/modules/lang/stores/lang.store';
import { TemplateStore } from 'src/modules/template/stores/template.store';
import moment from 'moment';
import { Power } from 'src/modules/power/models/power';
import { Template } from 'src/modules/template/models/template';
import { PlayAreaSelectComponent } from 'src/modules/playarea/components/select/select.component';
import { CacheService, ICacheConfig } from 'src/services/cache.service';
import { EventService } from 'src/shared/event.service';

@Component({
    providers: [
        LangStore, TemplateStore
    ],
    selector: 'player-form',
    templateUrl: './form.page.html',
    styleUrls: ['./form.page.scss']
})
export class PlayerFormPage {
    @ViewChild(MapInfoWindow) infoWindow: MapInfoWindow;

    public id: number | null;

    public form: FormGroup;
    public email: FormControl = new FormControl('', [Validators.email, Validators.required]);
    public idcustomer: FormControl = new FormControl('');
    public idlang: FormControl = new FormControl('', [Validators.required]);
    public idsite: FormControl = new FormControl('', [Validators.required]);
    public idarea: FormControl = new FormControl('', [Validators.required]);
    public is_monitored: FormControl = new FormControl(false, [Validators.required]);
    public powers: FormControl = new FormControl([]);
    public idtemplate: FormControl = new FormControl(1, [Validators.required]);
    public status: FormControl = new FormControl(true);
    public is_sync: FormControl = new FormControl(false);

    public instalation_date: FormControl = new FormControl('');
    public support_end_date: FormControl = new FormControl('');
    public hardware_type: FormControl = new FormControl('');
    public software_version: FormControl = new FormControl('');
    public serial_number: FormControl = new FormControl('');
    public screen_resolution: FormControl = new FormControl('');
    public installer_name: FormControl = new FormControl('');
    public connected_point_number: FormControl = new FormControl('');

    public weekdays = [
        '',
        'WEEKDAYS.mon',
        'WEEKDAYS.tue',
        'WEEKDAYS.wed',
        'WEEKDAYS.thu',
        'WEEKDAYS.fri',
        'WEEKDAYS.sat',
        'WEEKDAYS.sun'
    ];

    public players: Player[];
    public langs: Lang[];
    public templates: Template[];
    public customers: Customer[];
    public componentPowers: Power[] = [];
    public allSites: Site[] = [];
    public sites: Site[] = [];

    public loading: boolean = false;
    public customersToSelect: boolean = false;
    public customerSelect: number = null;

    public player: Player;
    public site: Site | null;
    public area: PlayArea = new PlayArea();
    public error: string = '#ddd';
    public selectedPlayerPowers: any[] = [];
    public me: any;
    public hide: boolean = true;
    public disabledSite: boolean = false;
    public isSuperAdmin: boolean = false;

    public sta: Number = 0;

    @ViewChild(PlayAreaSelectComponent) areas: PlayAreaSelectComponent;

    constructor(
        private ws: WSService, 
        private debug: DebugStore, 
        private router: Router, 
        private route: ActivatedRoute, 
        private fb: FormBuilder, 
        private _snackBar: MatSnackBar, 
        private dialog: DialogService, 
        private lstore: LangStore, 
        private tstore: TemplateStore, 
        private ustore: UserStore, 
        private events: EventService, 
        private eventc: CacheService) {
        this.ustore.onChange.subscribe((user) => {
            this.me = user;
            if (this.me.customer) {
                this.idcustomer.setValue(this.me.customer.idcustomer);
                this.hide = false;
            }
            if (this.me.site) {
                this.idsite.setValue(this.me.site.idsite);
                this.disabledSite = true;
            }
            this.me.customer
                ? (this.isSuperAdmin = false)
                : (this.isSuperAdmin = true);
        });
        this.id = Number(this.route.snapshot.paramMap.get('idsite'));
        this.lstore.onChange.subscribe((langs) => {
            this.langs = langs;
            if (this.me.customer)
                this.getLangsWithCustomer(this.me.customer.idcustomer);
        }
        );
        this.tstore.onChange.subscribe((templates) => {
            this.templates = templates;
        });
    }
    public ngOnInit() {
        this.debug.addInfo('PlayerFormComponent', 'OnInit');
        this.verifyPermissions();
        this.initForm(null);
        this.getSites();
        if (this.id) {
            this.hide = false;
            this.ws.getPlayer(this.id).subscribe((result: IResult) => {
                if (result.success) {
                    this.debug.addInfo('WS.getPlayer', 'successful');
                    this.player = result.data;
                    if (this.player.powers.length > 0) {
                        let powers: Power[] = [];
                        this.player.powers.map((power) => {
                            power.time_on = moment('2015-01-16T' + power.time_on).format('HH:mm');
                            power.time_off = moment('2015-01-16T' + power.time_off).format('HH:mm');
                            power.new = false;
                            powers.push(power);
                        });
                        this.selectedPlayerPowers = powers;
                    }
                    this.getPlayArea(this.player.idarea);
                    if (!this.me.site)
                        this.getSiteByCustomer(result.data.customer.idcustomer);
                    this.initForm(result.data);
                } else {
                    this.debug.addError('WS.getPlayer', result.data);
                }
            }, (error) => {
                this.debug.addError('WS.getPlayer', error.message);
            });
        } else {
            this.player = new Player();
        }
    }

    public onSaveContentPowerDialog(selections) {
        this.selectedPlayerPowers = [];
        selections.map((power) => {
            power.time_on = moment('2015-01-16T' + power.time_on).format('HH:mm');
            power.time_off = moment('2015-01-16T' + power.time_off).format('HH:mm');
            power.new = false;
            this.selectedPlayerPowers.push(power);
        });
    }

    private getPlayArea(id: number) {
        this.ws.playarea(id).subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.playarea', 'successful');
                this.area = new PlayArea(result.data);
            } else {
                this.debug.addError('WS.playarea', result.data);
            }
        }, (error) => {
            return null;
        });
    }

    private getSites() {
        this.ws.getSiteToSelect().subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.getSiteToSelect', 'successful');
                this.sites = result.data;
                this.idsite = result.data.idsite;
            } else {
                this.debug.addError('WS.getSiteToSelect', result.data);
            }
        }, (error) => {
            return null;
        });
    }

    private getLangs(): any {
        this.ws.langToSelect().subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.langToSelect', 'successful');
                this.langs = result.data;
            } else {
                this.debug.addError('WS.langToSelect', result.data);
            }
        }, (error) => {
            return null;
        });
    }

    private initForm(data: Player | null) {
        if (!data) {
            this.form = this.fb.group({
                email: this.email,
                idcustomer: this.idcustomer,
                idlang: this.idlang,
                idsite: this.idsite,
                idarea: this.idarea,
                powers: this.powers,
                is_monitored: this.is_monitored,
                status: this.status,
                is_sync: this.is_sync,
                idtemplate: this.idtemplate,
                instalation_date: this.instalation_date,
                support_end_date: this.support_end_date,
                hardware_type: this.hardware_type,
                software_version: this.software_version,
                serial_number: this.serial_number,
                screen_resolution: this.screen_resolution,
                installer_name: this.installer_name,
                connected_point_number: this.connected_point_number
            });
        } else {
            this.customerSelect = data.customer.idcustomer;
            this.sta = data.status
                ? 1
                : 0;
            this.form = this.fb.group({
                email: data.email,
                idcustomer: this.customerSelect,
                idlang: data.idlang,
                idsite: data.idsite,
                idarea: data.idarea,
                powers: [],
                status: data.status,
                is_monitored: data.is_monitored,
                is_sync: data.is_sync,
                idtemplate: data.idtemplate,
                instalation_date: data.instalation_date,
                support_end_date: data.support_end_date,
                hardware_type: data.hardware_type,
                software_version: data.software_version,
                serial_number: data.serial_number,
                screen_resolution: data.screen_resolution,
                installer_name: data.installer_name,
                connected_point_number: data.connected_point_number
            });
        }
    }

    public getSiteByCustomer(idcustomer: number | null, select: boolean = false) {
        if (idcustomer != null) {
            if (idcustomer == 0)
                this.getSites();
            else
                this.ws.getCustomer(idcustomer).subscribe((result: IResult) => {
                    if (result.success) {
                        this.debug.addInfo('WS.getCustomer', 'successful');
                        this.sites = result.data.sites;
                        this.form.get('idsite').enable();
                        this.site && !select
                            ? this.form.get('idsite').setValue(this.site.idsite)
                            : null;
                        select
                            ? this.form.get('idsite').setValue('')
                            : null;
                    } else {
                        this.debug.addError('WS.getCustomer', result.data);
                        this.form.get('idsite').disable();
                    }
                }, (error) => {
                    this.debug.addError('WS.getCustomer', error.message);
                    this.form.get('idsite').disable();
                });
        }
        else
            this.form.get('idsite').disable();
    }

    public create() {
        if (!this.form.get('idarea').value) {
            this.openSnackBar('Tienes que seleccionar al menos un area');
            this.areas.setError('red');
            return null;
        }

        if (this.form.invalid) {
            this.openSnackBar('El formulario no está correctamente relleno');
            return null;
        }

        this.loading = true;
        this.ws.createPlayer(this.form.value).subscribe((result: IResult) => {
            if (result.success) {
                const id = result.data.idplayer;
                this.debug.addInfo('WS.createPlayer', 'successful');
                this.openSnackBar('Player creado correctamente');
                this.updatePlayerTags(id);
                this.updatePlayersPowers(id);
                if (this.me.customer) {
                    let config: ICacheConfig = {
                        idplayer: id,
                        idcustomer: this.me.customer.idcustomer
                    };
                    setTimeout(() => {
                        this.events.publish('player:change', config);
                    }, 1000);
                }
                this.router.navigateByUrl('/private/player/list');
                this.loading = false;
            } else {
                this.debug.addError('WS.createPlayer', result.data);
                this.openSnackBar(result.data);
                this.loading = false;
            }
        }, (error) => {
            let msj = JSON.stringify(error.error.data);
            this.openSnackBar(msj.substr(2, msj.length - 3));
            this.debug.addError('WS.createPlayer', error.message);
            this.loading = false;
        });
    }

    public update() {
        if (this.form.invalid) {
            this.openSnackBar('El formulario no está correctamente relleno');
            return null;
        }
        this.loading = true;
        this.ws.updatePlayer(this.id, this.form.value).subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.updatePlayer', 'successful');
                this.openSnackBar('Player actualizado correctamente');
                this.updatePlayerTags(this.id);
                this.updatePlayersPowers(this.id);
                if (this.me.customer) {
                    let config: ICacheConfig = {
                        idplayer: this.id,
                        idcustomer: this.me.customer.idcustomer
                    };
                    setTimeout(() => {
                        console.log('publish', config);
                        this.events.publish('player:change', config);
                    }, 1000);
                }
                this.router.navigateByUrl('/private/player/list');
                this.loading = false;
            } else {
                this.debug.addError('WS.updatePlayer', result.data);
                this.openSnackBar(result.data);
                this.loading = false;
            }
        }, (error) => {
            let msj = JSON.stringify(error.error.data);
            this.openSnackBar(msj.substr(2, msj.length - 3));
            this.debug.addError('WS.updatePlayer', error.message);
            this.loading = false;
        });
    }

    public updatePlayerTags(idsite: number) {
        this.getTags();
        if (this.player.tags)
            this.ws.updatePlayerTags(idsite, { tags: this.player.tags }).subscribe((result: IResult) => {
                if (result.success) {
                    this.debug.addInfo('WS.updatePlayerTags', 'successful');
                } else {
                    this.debug.addError('WS.updatePlayerTags', result.data);
                    this.openSnackBar(result.data);
                }
            }, (error) => {
                this.debug.addError('WS.updatePlayerTags', error.message);
                this.openSnackBar(error.message);
            });
    }

    public updatePlayersPowers(idplayer: number) {
        let selectedPowersData = [];

        if (this.selectedPlayerPowers.length > 0) {
            this.selectedPlayerPowers.map((power) => {
                selectedPowersData.push(power);
            });
        }

        let body = {
            powers: selectedPowersData
        };
        this.ws.updatePlayerPowers(idplayer, body).subscribe((result: IResult) => {
            if (result.success) {
                this.debug.addInfo('WS.updatePlayerPowers', 'successful');
            } else {
                this.debug.addError('WS.updatePlayerPowers', result.data);
            }
        }, (error) => {
            this.debug.addError('WS.updatePlayerPowers', error.message);
        });
    }

    private getTags(): ITag[] {
        return this.player.tags;
    }

    public openDialogTags() {
        this.dialog.tagcategoryselect('Tags', this.player.tags, this.form.get('idcustomer').value).afterClosed().subscribe((result) => {
            if (!result.cancel)
                this.player.tags = result.tags;
        }
        );
    }

    public compareCustomer(x: Customer, y: Customer): boolean {
        return x && y
            ? x.idcustomer === y.idcustomer
            : x === y;
    }

    public selectionSite(event) { }

    public openSnackBar(message: string) {
        this._snackBar.open(message, '', { duration: 1500 });
    }

    public customerSelected(idcustomer: number) {
        idcustomer == 0
            ? (this.hide = true)
            : (this.hide = false);

        this.form.patchValue({ idcustomer: idcustomer });
        this.getLangsWithCustomer(idcustomer);
        this.getSiteByCustomer(idcustomer);
    }

    private getLangsWithCustomer(idcustomer: number) {
        let langs: Lang[] = [];
        if (idcustomer != null) {
            if (idcustomer == 0)
                this.getLangs();
            else
                this.ws.getCustomer(idcustomer).subscribe((result) => {
                    if (result.success) {
                        const customer = result.data;
                        langs = customer.langs;
                        const allLangs = this.langs;
                        this.langs = [];
                        this.langs = allLangs.filter((l) => langs.some((la) => la.idlang === l.idlang));
                    } else {
                        this.openSnackBar(result.data);
                    }
                }, (err) => {
                    this.openSnackBar(err);
                });
        }
    }

    private verifyPermissions() {
        this.customersToSelect = this.ustore.hasPermissionByName('get.customers.toselect');
    }

    public onSavePlayerPowerDialog(selections) {
        this.selectedPlayerPowers = [];
        selections.map((power) => {
            power.time_on = moment('2015-01-16T' + power.time_on).format('HH:mm');
            power.time_off = moment('2015-01-16T' + power.time_off).format('HH:mm');
            power.new = false;
            this.selectedPlayerPowers.push(power);
        });
    }

    changeArea(area: PlayArea) {
        let idarea: number = area
            ? area.idarea
            : null;
        this.form.patchValue({ idarea: idarea });
    }
}
