import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FlexModule } from '@angular/flex-layout';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';

import { MaterialModule } from 'src/app/material.module';
import { DirectivesModule } from 'src/directive/directives.module';

import { PLAYER_COMPONENTS } from './components/components';

import { PlayerFormPage } from './pages/form/form.page';
import { PlayerListPage } from './pages/list/list.page';
import { CustomerModule } from 'src/modules/customer/customer.module';
import { PrimetimeModule } from 'src/modules/primetime/primetime.module';
import { PlayareaModule } from 'src/modules/playarea/playarea.module';
import { RouterModule } from '@angular/router';
import { PlayerRoutingModule } from './player-routing.module';
import { PlayerStore } from './stores/player.store';

@NgModule({
  declarations: [PLAYER_COMPONENTS, PlayerFormPage, PlayerListPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FlexModule,
    NgxMatFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule,
    CustomerModule,
    PrimetimeModule,
    RouterModule,
    PlayareaModule,
    PlayerRoutingModule,
    DirectivesModule
  ],
  providers: [PlayerStore, { provide: 'Window',  useValue: window }],
  exports: [PLAYER_COMPONENTS]
})

export class PlayerModule {}
