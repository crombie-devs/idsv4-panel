import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpService } from 'src/services/http.service';
import { ICrudWS } from 'src/interfaces/ICrudWS.interface';
import { Player } from 'src/modules/player/models/player';
import { idText } from 'typescript';

@Injectable({
  providedIn: 'root',
})
export class PlayerService implements ICrudWS<Player>{
  subject: Subject<number> = new Subject();
  $subject = this.subject.asObservable();
  socketId: Subject<string> = new Subject();
  $socketId = this.socketId.asObservable();

  constructor(private http: HttpService) {}

  public create(obj: Player): Observable<Player> {
    return this.http.createWithFiles('private/player', obj, 'WS.player    _add');
  }

  public update(obj: Player): Observable<Player> {
    return this.http.updateWithFiles('private/player/'+obj.idplayer, obj, 'WS.player_add');
  }

  public delete(id: number): Observable<Player> {
    return this.http.delete('private/player', [id], 'WS.player_delete');
  }

  public get(id: number): Observable<Player> {
    return this.http.get('private/player', [id], 'WS.player');
  }

  public toSelect(args?: any[]): Observable<Player[]> {
    return  this.http.get('private/players/toselect', args, 'WS.player');
  }

  public list(args?: any[]): Observable<Player[]> {
    return  this.http.get('private/players', args, 'WS.player');
  }


  public subjetIdPlayer(id: number): void {
    this.subject.next(id);
  }

  public subjectSocketId(sockerId: string) : void {
      this.socketId.next(sockerId)
  }

  public changeStatus(id: number): Observable<boolean[]> {
    return  this.http.get('private/player/status', [id], 'WS.player_change_status');
  }
  


}


