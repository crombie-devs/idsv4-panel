import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnDestroy, Output, Renderer2, SimpleChanges, ViewChild, EventEmitter } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { fromEvent, Observable, Subscription } from "rxjs";
import { CONFIG } from 'src/app/app.config';
import { PostMessageService } from "src/services/postmessage.service";


@Component({
    selector: 'player-text',
    templateUrl: './text.component.html',
    styleUrls: ['./text.component.scss']
})
export class PlayerTextComponent implements OnChanges, OnDestroy {
    
    @Input() email: string;
    @Input() idcircuit: number;
    @Input() date: string;
    @ViewChild('container') container: ElementRef;

    public widgetURL: SafeResourceUrl; 
    public windows;
    public origin;
    private listener: Subscription;


    constructor(private sanitizer: DomSanitizer, private renderer: Renderer2){ 
        this.listener = fromEvent(window, "message").subscribe((e: MessageEvent) => {    
            if(e.data.key === "is_play"){  
                let value =  e.data.value;
                this.windows = e.source; 
                this.origin = e.origin;
                this.sendMessage(value);
            }
        });    
    }

    ngOnDestroy(){
        this.listener.unsubscribe();        
    }

    private sendMessage(value: string){
        let frame = <HTMLIFrameElement>document.getElementById('viewer_content_text');
        frame.contentWindow.postMessage({key: "is_play", value: value, domain: "panel.ids.ladorian.com"}, '*');
    }
    public ngOnChanges(changes: SimpleChanges) {
        this.reloadUrl();           
    }  

    private reloadUrl() {
        if(this.email && this.idcircuit && this.date)
            this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.preview_text}/${this.email}/circuit/${this.idcircuit}/date/${this.date}`);   
        else if(this.email && !this.idcircuit && !this.date) 
            this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.preview_text}/${this.email}`);
        else if(this.email && this.idcircuit && !this.date) 
            this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.preview_text}/${this.email}/circuit/${this.idcircuit}`);
        else if(this.email && !this.idcircuit && this.date) 
            this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.preview_text}/${this.email}/date/${this.date}`);
    }

}

