import { Component, EventEmitter, Output, Input } from '@angular/core';
import { PlayerService } from 'src/modules/player/services/player.service';
import { Player } from 'src/modules/player/models/player';
import { FormControl, Validators, FormGroup } from '@angular/forms';



@Component({
  selector: 'player-select-players',
  templateUrl: './select-players.component.html',
  styleUrls: ['./select-players.component.scss'],
})
export class SelectPlayersComponent {
  @Output() onClick: EventEmitter<any> = new EventEmitter();
  @Output() onChange: EventEmitter<number> = new EventEmitter();
  @Input() isCascade: boolean;
  @Input() required: boolean;

  players: Player[] = [];
  playersAll: Player[] = [];
  idplayer: FormControl;
  form: FormGroup;


  constructor(private ws: PlayerService) {
    let validator = this.required ? Validators.required : null;
    this.form = new FormGroup({
      idplayer: new FormControl('', validator),
    });
    
    this.ws.toSelect().subscribe((items: Player[]) => {
      this.players = items;
      this.playersAll = [...this.players];
    });
  }

  onSelect(event): void {
    this.onClick.emit(event);
    this.onChange.emit(event.value);
    this.ws.subjetIdPlayer(event.value);
  
  }

  public filterBySite(idsite: number, idplayer?: number): void {
    this.isCascade = false;
    this.players = this.playersAll.filter((item) => item.idsite == idsite);
    if (idplayer) {
      this.form.patchValue({ idplayer: idplayer });
    }
  }


  public customer(idsite: number) {
   
    return this.playersAll.filter(item => item.idsite == idsite)[0];
  }
}
