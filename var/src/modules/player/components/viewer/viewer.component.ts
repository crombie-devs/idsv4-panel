import { AfterViewInit, Component, ElementRef, HostListener, Input, OnChanges, Renderer2, SimpleChanges, ViewChild } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { Content } from 'src/modules/content/models/content';

@Component({
    selector: 'player-viewer',
    templateUrl: './viewer.component.html',
    styleUrls: ['./viewer.component.scss']
  })
export class PlayerViewerComponent implements AfterViewInit, OnChanges {
    
    @Input() width: number = 500;
    @Input() height: number = 270;
    @Input() email: string;
    @Input() idcircuit: number;
    @Input() date: string;
    @Input() emittingContent:string;

    @ViewChild('container') container: ElementRef;

    public widgetURL: SafeResourceUrl; 

    constructor(private sanitizer: DomSanitizer, private renderer: Renderer2){}

    public ngOnChanges(changes: SimpleChanges) {
        if(changes.width || changes.height){
            setTimeout(()=>this.resize(),100);        
            this.reloadUrl();
        }
    }

    private reloadUrl() {
        if(this.email && this.idcircuit && this.date)
            this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.preview}/${this.email}/circuit/${this.idcircuit}/date/${this.date}`);   
        else if(this.email && !this.idcircuit && !this.date) 
            this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.preview}/${this.email}`);
        else if(this.email && this.idcircuit && !this.date) 
            this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.preview}/${this.email}/circuit/${this.idcircuit}`);
        else if(this.email && !this.idcircuit && this.date) 
            this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.preview}/${this.email}/date/${this.date}`);          
    }

    ngAfterViewInit () {
        this.resize();
    }

    @HostListener('window:resize', ['$event']) 
    onResize(event) { 
        this.resize();
    }

    private resize() {
        if(!this.container){ return; }

        let width = this.container.nativeElement.offsetWidth;
        let height = (width * this.height) / this.width;

        if(height > 350) {
            height = 350;
            width = (height * this.width) / this.height;
            this.renderer.setStyle(this.container.nativeElement, 'width', `${width}px`);
            this.renderer.setStyle(this.container.nativeElement, 'height', `${height}px`);
        } else {
            width = 'calc(100% - 12px)'
            this.renderer.setStyle(this.container.nativeElement, 'width', width);
            this.renderer.setStyle(this.container.nativeElement, 'height', `${height}px`);
        }
    }    
}

