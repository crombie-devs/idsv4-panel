import { SelectPlayersComponent } from './select-players/select-players.component';
import { PlayerSelectComponent } from './select/select.component';
import { PreviewPlayerComponent } from './preview-player/preview-player.component';
import { PlayerTextComponent } from './text/text.component';
import { PlayerViewerComponent } from './viewer/viewer.component';

export const PLAYER_COMPONENTS = [
  SelectPlayersComponent,
  PreviewPlayerComponent,
  PlayerSelectComponent,
  PlayerTextComponent,
  PlayerViewerComponent
];
