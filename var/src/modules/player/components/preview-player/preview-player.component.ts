import { AfterViewInit, Component, ElementRef, HostListener, Input, OnChanges, Renderer2, SimpleChanges, ViewChild } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { CONFIG } from 'src/app/app.config';
import { PlayerService } from 'src/modules/player/services/player.service';




@Component({
    selector: 'preview-player',
    templateUrl: './preview-player.component.html',
    styleUrls: ['./preview-player.component.scss']
  })
export class PreviewPlayerComponent implements AfterViewInit, OnChanges {
    
    @Input() width: number = 500;
    @Input() height: number = 270;
    @Input() email: string;
    @Input() idcircuit: number;
    @Input() date: string;


    @ViewChild('container') container: ElementRef;

    public widgetURL: SafeResourceUrl; 
    public windows;
    public origin;
    constructor(private sanitizer: DomSanitizer, private renderer: Renderer2, private _player: PlayerService){
        if (window.addEventListener) {
       
            window.addEventListener("message", (e) => {
              /*   if (e.origin === CONFIG.ids_front) { */
                    if (e.data.key === "is_play") {
                    let value = e.data.value;
                    this.windows = e.source;
                    this.origin = e.origin;
                    //this.sendMessage(value);
          
        
                    } else if (e.data.key === "preview") {
                
                        this._player.subjectSocketId(e.data.value);
        
                    }
              /* } */
            }, false);
          }
    }

    private sendMessage(value : string) {
        let frame = <HTMLIFrameElement>( document.getElementById("viewer_content_video") ); 
        frame.contentWindow.postMessage({
            key: "is_play",
            value: value,
            domain: "panel.ids.ladorian"
          }, "*"); 
        } 
          

    public ngOnChanges(changes: SimpleChanges) {
        if(changes.width || changes.height)
            setTimeout(()=>this.resize(),100);        
        this.reloadUrl();      
    }

    

    private reloadUrl() {
        if(this.email && this.idcircuit && this.date)
            this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.preview}/${this.email}/circuit/${this.idcircuit}/date/${this.date}`);   
        else if(this.email && !this.idcircuit && !this.date) 
            this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.preview}/${this.email}`);
        else if(this.email && this.idcircuit && !this.date) 
            this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.preview}/${this.email}/circuit/${this.idcircuit}`);
        else if(this.email && !this.idcircuit && this.date) {
              this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.preview}/${this.email}/date/${this.date}`);
              
        }
    }

    ngAfterViewInit () {
        this.resize();
        
    }

    @HostListener('window:resize', ['$event']) 
    onResize(event) { 
        this.resize();
    }

    private resize() {
        if(!this.container){ return; }

        let width = this.container.nativeElement.offsetWidth;
        let height = (width * this.height) / this.width;

        if(height > 350) {
            height = 350;
            width = (height * this.width) / this.height;
            this.renderer.setStyle(this.container.nativeElement, 'width', `${width}px`);
            this.renderer.setStyle(this.container.nativeElement, 'height', `${height}px`);
        } else {
            width = 'calc(100% - 12px)'
            this.renderer.setStyle(this.container.nativeElement, 'width', width);
            this.renderer.setStyle(this.container.nativeElement, 'height', `${height}px`);
        }
    }    



  

    
}

