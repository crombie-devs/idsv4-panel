import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from "@angular/core";
import { MYString } from "src/libs/string.libs";
import { Site } from "src/modules/site/models/site";
import { Player } from "src/modules/player/models/player";
import { PlayerStore } from "src/modules/player/stores/player.store";



@Component({
    providers: [ PlayerStore ],
    selector: 'player-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.scss']
  })
export class PlayerSelectComponent implements OnInit, OnChanges {
    loading: boolean = true;
    players: Player[] = [] ;

    selectedPlayer: Player[] = [] ;


    @Input() site: Site = null;
    @Input() selected: Player = null;
    @Output() onChange: EventEmitter<Player> = new EventEmitter<Player>();

    constructor(private store: PlayerStore) {}

    ngOnInit() {
        this.store.onChange.subscribe(result => {
            if(result) {
                this.players = result;
                if(result.length === 1){
                    this.selected = result[0];
                    this.onSelect(this.selected);
                }
                this.selectedPlayer = this.players;
                this.loading = false;
                this.refresh();
            }
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if(changes.site)
            this.refresh();
    }

    private refresh() {
        this.players = this.store.get();
        if(this.site)
            this.players = this.players.filter(p => p.idsite == this.site.idsite);
        this.selectedPlayer = this.players;
    }

    private exist(): boolean {
        if(this.selected)
            return this.players.some(p => p.isEquals(this.selected));

        return false;
    }

    private clear() {
        this.onSelect(null);
    }

    onSelect(player: Player){
        this.selected = player;
        this.onChange.emit(this.selected);
    }

    checkSelect(player: Player){
        if(this.selected)
            return this.selected.isEquals(player);

        return false;
    }

    public onKey(value) { 
        this.selectedPlayer = this.search(value);
    }

    public search(value: string) { 
        let filter = MYString.removeAccents(value).toLowerCase();
        return this.players.filter(p => MYString.removeAccents(p.email).toLowerCase().startsWith(filter));
    }

}