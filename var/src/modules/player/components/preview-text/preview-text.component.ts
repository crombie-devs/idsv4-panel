import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  Renderer2,
  SimpleChanges,
  ViewChild
} from "@angular/core";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {CONFIG} from "src/app/app.config";
import { PlayerService } from 'src/modules/player/services/player.service';

@Component({
  selector: "player-preview-text", 
  templateUrl: "./preview-text.component.html", 
  styleUrls: ["./preview-text.component.scss"]
})

export class PreviewTextComponent implements OnChanges {
  @Input()email: string;
  @Input()idcircuit: number;
  @Input()date: string;

  @ViewChild("container")container: ElementRef;

  public widgetURL: SafeResourceUrl;
  public windows;
  public origin;

  constructor(private sanitizer : DomSanitizer, private ws: PlayerService) {
    if (window.addEventListener) {
      window.addEventListener("message", (e) => {
        if (e.origin === CONFIG.ids_front) {
          if (e.data.key === "is_play") {
            let value = e.data.value;
            this.windows = e.source;
            this.origin = e.origin;
            this.sendMessage(value);

          } else if (e.data.key === "preview") {
             
            this.ws.subjectSocketId(e.data.value);

          }
        }
      }, false);
    }
  }

  private sendMessage(value : string) {
    let frame = <HTMLIFrameElement>( document.getElementById("viewer_content_text") ); 
    frame.contentWindow.postMessage({
        key: "is_play",
        value
      }, "*"); 
    } 
      
      
      public ngOnChanges(changes: SimpleChanges) {
        this.reloadUrl();
      }

      private reloadUrl() {
        
           if (this.email && this.idcircuit && this.date)  {  
             
              this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.preview_text}/${this.email}/circuit/${this.idcircuit}/date/${this.date}`);
            } else if (this.email && !this.idcircuit && !this.date) {
               this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.preview_text}/${this.email}`); 
          }
      
        else if (this.email && this.idcircuit && !this.date) 
          this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.preview_text}/${this.email}/circuit/${this.idcircuit}`);
        else if (this.email && !this.idcircuit && this.date) 
          this.widgetURL = this.sanitizer.bypassSecurityTrustResourceUrl(`${CONFIG.preview_text}/${this.email}/date/${this.date}`);
          }
      }
