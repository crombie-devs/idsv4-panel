import { Component, ElementRef, HostBinding } from "@angular/core";

@Component({
  selector: "mat-header-cell[resizable]",
  templateUrl: "./resizable.template.html",
  styleUrls: ["./resizable.scss"],
})
export class ResizableComponent {
  @HostBinding("style.width.px")
  width: number | null = null;

  onResize(width: number) {
    this.width = width;
  }
}