import { Injectable, Inject } from "@angular/core";
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

export interface ILocalStorage {
	get(key: string): any;
	set(key: string, value: any): void;
}

@Injectable()
export class MYLocalStorage implements ILocalStorage {

	constructor(
		@Inject(LOCAL_STORAGE) 
		private localStorage: StorageService) {

	}

	get(key: string): any {
		return this.localStorage.get(key);
	}

	set(key: string, value: any): void {
		this.localStorage.set(key, value);
	}

	clear(){
		this.localStorage.clear();
	}
}