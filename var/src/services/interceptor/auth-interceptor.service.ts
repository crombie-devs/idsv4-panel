import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError, tap } from 'rxjs/operators';
import { Auth } from 'src/modules/auth/models/auth';
import { AuthStore } from 'src/modules/auth/stores/auth.store';



@Injectable({
    providedIn: 'root'
})
export class AuthInterceptorService {

    constructor(
        private router: Router,
        private astore: AuthStore
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const auth = new Auth(this.astore.auth);

        let request = req;

        const path = request.url.split('/')[3];
        if (path != 'i18n') {
            if (auth.access_token) {
                request = req.clone({
                    setHeaders: { authorization: `Bearer ${auth.access_token.replace(/['"]+/g, '')}` }
                });
            }
        }




        return next.handle(request).pipe(
            tap(
                event => {
                    if (event instanceof HttpResponse) {
                        //api call success
                        // console.log('success in calling API : ', event);
                    }
                },
                error => {
                    if (event instanceof HttpResponse) {
                        this.router.navigateByUrl('/login');
                        console.log('error in calling API : ', event);
                    }
                }
            )
        )


    }
}
