import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { shareReplay, first, filter } from 'rxjs/operators';

@Injectable()
export class CacheInterceptor implements HttpInterceptor {
  public store: Record<string, Observable<HttpEvent<any>>> = {};

  constructor() {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    
    if (req.headers.get('reset')) {
      this.extractMainPath(req);
    }

    return next.handle(req);
    
  

    const cachedObservable =
      this.store[req.urlWithParams] ||
      (this.store[req.urlWithParams] = next.handle(req).pipe(
        filter((res) => res instanceof HttpResponse),
        shareReplay(1)
      ));

    return cachedObservable.pipe(first());
  }

  extractMainPath(req: HttpRequest<any>) {
    const path = req.headers.get('reset');
    const keys = Object.keys(this.store);
    
    keys.forEach((item, index) => {
      let split = item.split('/');
      const pathReq = split[3] + '/' + split[4];

      console.log(pathReq)

      if (pathReq === path) {
        delete this.store[item];
      }
    });
  }
}
