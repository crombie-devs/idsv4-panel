import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class DashboardService {

    private url = environment.ws_url + '/private/dashboard-messages';

    constructor(
        private http: HttpClient
    ) { }

    getMessages(): Observable<any[]> {
        return this.http.get<any[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getLastMessages(): Observable<any[]> {
        return this.http.get<any[]>(this.url + '/latest').pipe( 
            map( data => {
                return data;
            })
        );
    }

}