import { Injectable } from '@angular/core';
import { BehaviorSubject, fromEvent, Observable, Subject } from 'rxjs';
import { endWith } from 'rxjs/operators';

@Injectable()
export class PostMessageService {

    private $onMessage: BehaviorSubject<MessageEvent> = new BehaviorSubject(null);
    public onMessage: Observable<MessageEvent> = this.$onMessage.asObservable();

    constructor() { 
        fromEvent(window, "message").subscribe( (e: MessageEvent) =>{  
            console.log('postmessage',e);   
            if(e.data.domain.endWith("ids.ladorian"))
                this.$onMessage.next(e);
            
        });
    }
}