import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { EventService } from 'src/shared/event.service';
import { WSService } from 'src/services/ws.service';

import { DebugStore } from "src/stores/debug.store";

import { IResult } from "src/models/iresult";

import { CONFIG } from 'src/app/app.config';

@Injectable({
    providedIn: 'root',
})

export class NotificationService  extends EventService {
    constructor(private events: EventService,
		private ws: WSService,  
		private debug: DebugStore, 
		private http: HttpClient) 
		{
		super();
		this.events.subscribe('site:notification', (id: number, action:string = '') => {
			this.sendNotificacion(id, 'site', action);
		});

		this.events.subscribe('customer:notification', (id: number, action:string = '') => {
			this.sendNotificacion(id, 'customer', action);
		});

		this.events.subscribe('player:notification', (id: number, action:string = '') => {
			this.sendNotificacion(id, 'player');
		});
		//Duda
		this.events.subscribe('area:notification', (id: number, action:string = '') => {
			this.sendNotificacion(id, 'area');
		});
    }

	private sendNotificacion(id: number = null, to: string = null, action: string = null){
		this.httpNotification(id, to, action).subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.httpNotification', result.data);
			}
			else {            
				this.debug.addError('WS.httpNotification', result.data);
				return null;
			}
		}, error => {
			this.debug.addError('WS.httpNotification', error.message);  
			return null;          
		});   	
	} 

	public httpNotification(id: number = null, to: string = null, action: string = null){	
		if (!action)
			return this.http.get<IResult>(`${CONFIG.ws_url}/private/sendNotificacion/${to}/${id}`)
		else
			return this.http.get<IResult>(`${CONFIG.ws_url}/private/sendNotificacion/${action}/${to}/${id}`)
	}	
   
}