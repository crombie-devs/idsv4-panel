import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpService } from '../services/http.service';
import { ITicket } from 'src/modules/ticket/interfaces/ITicket.interface';
import { ITicketCategory } from 'src/modules/ticket/interfaces/ITicketCategory.interface';
import { ITicketStatus } from 'src/modules/ticket/interfaces/ITicketStatus.interface';
import { ITicketPriority } from 'src/modules/ticket/interfaces/ITicketPriority.interface';
import { ITicketDepartment } from 'src/modules/ticket/interfaces/ITicketDepartment.interface';
import { ITicketResponsible } from 'src/modules/ticket/interfaces/ITicketResponsible.interface';

@Injectable({
    providedIn: 'root',
})
export class TicketService {

    constructor(
        private http: HttpClient,
        private http_service: HttpService
    ) { }

    public getTickets<T>(routerParams?: Params[]): Observable<T> {

        let queryParams: Params = {};

        if (routerParams) {
            queryParams = this.setParameter(routerParams);
        }

        return this.http.get<T>(environment.ws_url + '/private/tickets', { params: queryParams });
    }

    public getTicketsGroupBy<T>(routerParams?: Params[]): Observable<T> {

        let queryParams: Params = {};

        if (routerParams) {
            queryParams = this.setParameter(routerParams);
        }

        return this.http.get<T>(environment.ws_url + '/private/tickets/groupby', { params: queryParams });
    }

    public getCustomers(args?: any[]): Observable<any[]> {
        return this.http_service.get('private/customers/toselect', args, 'WS.customer_toselect');
    }

    public getCategories(args?: any[]): Observable<ITicketCategory[]> {
        return this.http_service.get('private/tickets/category', args, 'WS.get_ticket_category');
    }

    public getSites(args?: any[]): Observable<any[]> {
        return this.http_service.get('private/sites', args, 'WS.site');
    }

    public getStatus(args?: any[]): Observable<ITicketStatus[]> {
        return this.http_service.get('private/tickets/status', args, 'WS.get_ticket_status');
    }

    public getPriorities(args?: any[]): Observable<ITicketPriority[]> {
        return this.http_service.get('private/tickets/priority', args, 'WS.get_ticket_priority');
    }

    public getDepartments(args?: any[]): Observable<ITicketDepartment[]> {
        return this.http_service.get('private/departments', args, 'WS.get_ticket_departments');
    }

    public getResponsibles(args?: any[]): Observable<ITicketResponsible[]> {
        return this.http_service.get('private/responsibles', args, 'WS.get_ticket_responsibles');
    }

    public create(obj: ITicket): Observable<ITicket> {
        return this.http_service.createWithFiles('private/ticket', obj, 'WS.ticket_add');
    }

    public update(obj: ITicket): Observable<ITicket> {
        return this.http_service.updateWithArrayFiles('private/ticket', obj, 'WS.ticket_update');
    }

    public deleteArray(ids: number[]): Observable<ITicket> {
        return this.http_service.update('private/ticket/delete', { ids: ids }, 'WS.ticket_delete');
    }

    public deletedImages(id: number): Observable<any> {
        return this.http.delete(environment.ws_url + '/private/ticket/delete-images/' + id);
    }

    public list(args?: any[]): Observable<any[]> {
        return this.http_service.get('private/tickets', args, 'WS.ticket');
    }

    private setParameter(routerParams: Params): HttpParams {

        let queryParams = new HttpParams();

        for (const key in routerParams) {

            if (routerParams.hasOwnProperty(key)) {

                queryParams = queryParams.set(key, routerParams[key]);
            }

        }

        return queryParams;
    }

}


