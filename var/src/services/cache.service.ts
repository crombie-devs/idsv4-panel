import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { EventService } from 'src/shared/event.service';
import { WSService } from 'src/services/ws.service';

import { DebugStore } from "src/stores/debug.store";

import { IResult } from "src/models/iresult";

import { CONFIG } from 'src/app/app.config';
import { Observable } from 'rxjs';
import { NotificationService } from 'src/services/notification.service';

export interface ICacheConfig {
	idcustomer: number;
	idsite?: number;	
	idplayer?: number;
	idarea?: number;
	idcontent?: number;
};  

@Injectable({
    providedIn: 'root',
})
export class CacheService {

	constructor(private events: EventService, private ws: WSService,  private debug: DebugStore, private http: HttpClient, private notification: NotificationService) {
		this.events.subscribe('site:change', (config: ICacheConfig) => {
			this.buildPlayer(config, 'update');
			this.buildCircuit(config, 'update');
            this.buildContent(config, 'update');
		});	
		this.events.subscribe('player:change', (config: ICacheConfig) => {
			this.buildPlayer(config);
            this.buildPlaylogic(config, 'refresh');
		});
		this.events.subscribe('circuit:change', (config: ICacheConfig) => {
			this.buildCircuit(config, 'update');
		});	
		this.events.subscribe('playlogic:change', (config: ICacheConfig) => {
			this.buildPlaylogic(config, 'refresh');
		});

		//Duda agrupo en uno sólo
		this.events.subscribe('content:change', (config: ICacheConfig) => {
			this.buildContent(config, 'refresh');
		});
		this.events.subscribe('asset:change', (config: ICacheConfig) => {
			this.buildContent(config, 'refresh');
		});

		this.events.subscribe('area:change', (config: ICacheConfig) => {
			this.buildPlaylogic(config);
		});	

		this.events.subscribe('all:change', () => {
			this.buildPlaylist();
		}); 

		this.events.subscribe('send:mail', (idcontent: number) => {
			this.sendMailAlertGeneratePlaylist(idcontent);
		});
    }

	public buildPlaylist(){
		this.build('generateplaylist');
	}

    public buildPlayer(config: ICacheConfig, action: string = ''){		
		if (config.idplayer)
			this.build('generateplayers', config.idplayer, 'player',  action);
		else if(config.idsite)
			this.build('generateplayers', config.idsite, 'site', action);
		else if (config.idcustomer)
			this.build('generateplayers', config.idcustomer, 'customer',  action);
	};

	public buildCircuit(config: ICacheConfig, action: string = ''){
		if(config.idsite)
			this.build('generatecircuits', config.idsite, 'site', action);
		else if (config.idcustomer)
			this.build('generatecircuits', config.idcustomer, 'customer', action);
	};

	public buildPlaylogic(config: ICacheConfig, action: string = ''){
		if (config.idcustomer)
			this.build('generateplaylogics', config.idcustomer,  'customer', action);
		else if (config.idplayer)
			this.build('generateplaylogics', config.idplayer,  'player', action);
		else if (config.idarea)
			this.build('generateplaylogics', config.idarea, 'area', action);
	};

	public buildContent(config: ICacheConfig, action: string = ''){
		if(config.idsite)
			this.build('generatecontents', config.idsite, 'site',  action);
		else 
			this.build('generatecontents', config.idcustomer, 'customer', action);
	};

 	private build(ws: string, id = null, to: string = '', action: string = ''){
            this.callWebservice(ws, id, to).subscribe((result: IResult) => {
				if(result.success) {
					if(to && id){
						this.events.publish(`${to}:notification`, id, action);
					}
				} else {            
					this.debug.addError('WS.buildPlaylist', result.data);
					return null;
				}
			}, error => {
				this.debug.addError('WS.buildPlaylist', error.message);  
				return null;          
			});  
    } 

	private callWebservice(ws: string,  id: number = null, to: string = '' ){	
		if(!id && !to)
			return this.http.get<IResult>(`${CONFIG.ws_url}/private/${ws}`);
		else
			return this.http.get<IResult>(`${CONFIG.ws_url}/private/${ws}/${to}/${id}`);
	}	
	
	public sendMailAlertGeneratePlaylist(idcontent): Observable<IResult>{
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/sendalertemailplaylist/${idcontent}`)
	}
}