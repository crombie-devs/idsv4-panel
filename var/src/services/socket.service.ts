import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from "rxjs";
import io from 'socket.io-client';

import { CONFIG } from '../app/app.config';

export interface ISocketMessage {
	from: string,
	to: string,
	name: string,
	data?: any,
}

@Injectable()
export class SocketService {
	public connected: boolean = false;
	public id: string = null;
	private socket;
	private rooms: string[] = [];

	// Events
	public onConnect: BehaviorSubject<boolean>  = new BehaviorSubject(false);
	public onMessage: Subject<ISocketMessage>  = new Subject();

	constructor() {
		this.connect();
	}

	connect() {
		const socket = io(CONFIG.socket_url);
		socket.on('connect', () => {
			this.id = socket.id;
			this.rooms = [];
			this.connected = true;
			this.onConnect.next(this.connected);
		});
		socket.on('message', (message: ISocketMessage) => {
			this.onMessage.next(message);
		});
		socket.on('disconnect', (reason) => {
			this.connected = false;

			if (reason === 'io server disconnect')
				socket.connect();
			else if (reason === 'io client disconnect')
				socket.connect();
		});
		this.socket = socket;
	}

	public reconnect() {
		this.connected = false;
		this.socket.disconnect();
	}

	public join(room: string) {
		if(this.rooms.some((ro:string) => ro === room))
			return;

		this.socket.emit('join:room', room);
		this.rooms.push(room);
	}

	public leave(room: string) {
		this.socket.emit('leave:room', room);
		this.deleteRoom(room)
	}

	public send(data: ISocketMessage) {
		this.socket.emit('send:message', data);
	}

	public deleteRoom(room){
		const index = this.rooms.indexOf(room);
		if (index > -1) 
	  		this.rooms.splice(index, 1);
	}

}