import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { shareReplay, tap, catchError, map, concatMap } from 'rxjs/operators';
import { Observable, of, Subject, throwError } from 'rxjs';
import { DebugStore } from 'src/stores/debug.store';
import { environment } from 'src/environments/environment';
@Injectable({
    providedIn: 'root',
})

export class HttpService {
    alert: Subject<any> = new Subject();
    $alert = this.alert.asObservable();

    constructor(private http: HttpClient, private debug: DebugStore) { }

    private url(params: any[]): string {
        let param = '';
        if (params) {
            if (params.length > 0) {
                params.forEach((item) => {
                    param = param + '/' + item;
                });
            }
        }
        return param;
    }

    private debugStore(params: any, debugParams): void {
        if (params.success) {
            this.debug.addInfo(debugParams, 'successful');
        } else {
            this.debug.addInfo(debugParams, params.data);
        }
    }

    private debugError(error: any, debugParams): any {
        this.alert.next(error);
        this.debug.addInfo(debugParams, error);
    }

    private generateFormFile(values: any): FormData {
        const formData = new FormData();
        formData.append('_method', 'PUT');
        for (let key in values) {
            const val = values[key];

            if (values[key] instanceof File) {

                formData.append(key, val, val.name);
            } else {
                formData.append(key, val);
            }
        }
        return formData;
    }

    private generaFormArrayFiles(values: any): FormData {
        const formData = new FormData();
        formData.append('_method', 'PUT');
        for (let key in values) {
            const val: File = values[key];
            if (Array.isArray(val)) {
                val.forEach(f => {
                    if (f instanceof File) {
                        formData.append('files[]', f, f.name);
                    } else if (f instanceof Object) {
                        formData.append(`${key}[]`, JSON.stringify(f));
                    } else {
                        formData.append(`${key}[]`, f);
                    }
                });
            } else {
                if (values[key] instanceof File) {
                    formData.append(key, val, val.name);
                } else {
                    formData.append(key, val);
                }
            }
        }
        return formData;
    }



    private generaForm(values: any): FormData {
        const formData = new FormData();
        for (let key in values) {
            const val: File = values[key];
            if (Array.isArray(val)) {
                val.forEach(f => {
                    if (f instanceof File) {
                        formData.append('files[]', f, f.name);
                    }
                });
            } else {
                if (values[key] instanceof File) {
                    formData.append(key, val, val.name);
                } else {
                    formData.append(key, val);
                }
            }
        }
        return formData;
    }

    get(url: string, params: any[], debugParams: string): Observable<any> {
        const values = this.url(params);
        return this.http.get(environment.ws_url + '/' + url + values).pipe(
            tap((item: any) => this.debugStore(item, debugParams)),
            map((item) => item.data),
            shareReplay(),
            catchError((err) => this.debugError(err, debugParams)),
        );
    }
    update(url: string, values: any, debugParams: string): Observable<any> {
        return this.http.put(environment.ws_url + '/' + url, values).pipe(
            tap((item: any) => this.debugStore(item, debugParams)),
            map((item) => item.data),
            shareReplay(),
            catchError((err) => this.debugError(err, debugParams)),
        );
    }


    uploadCSV(url: string, csv: File, idmessage: string, datetime: any, debugParams): Observable<any> {
        const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' })
            .set('Accept', 'text/csv')
            .set('Accept', 'application/json');;

        let file = csv;
        let formData: FormData = new FormData();
        formData.append('idmessage', idmessage);
        formData.append('datetime', datetime);
        formData.append('csv', file, file.name);



        return this.http
            .post(environment.ws_url + '/' + url, formData, { headers: headers })
            .pipe(
                tap((item: any) => this.debugStore(item, debugParams)),
                map((item) => item.data),
                shareReplay(),
                catchError((err) => this.debugError(err, debugParams)),
            );

    }


    updateWithArrayFiles(
        url: string,
        values: any,
        debugParams: string
    ): Observable<any> {
        const formData = this.generaFormArrayFiles(values);
        const headers = new HttpHeaders({ enctype: 'multipart/form-data' });
        return this.http
            .post(environment.ws_url + '/' + url, formData, { headers: headers })
            .pipe(
                tap((item: any) => this.debugStore(item, debugParams)),
                map((item) => item.data),
                shareReplay(),
                catchError((err) => this.debugError(err, debugParams)),
            );
    }


    updateWithFiles(
        url: string,
        values: any,
        debugParams: string
    ): Observable<any> {
        const formData = this.generateFormFile(values);
        const headers = new HttpHeaders({ enctype: 'multipart/form-data' });
        return this.http
            .post(environment.ws_url + '/' + url, formData, { headers: headers })
            .pipe(
                tap((item: any) => this.debugStore(item, debugParams)),
                map((item) => item.data),
                shareReplay(),
                catchError((err) => this.debugError(err, debugParams)),
            );
    }
    create(url: string, values: any, debugParams: string): Observable<any> {
        let headers = new HttpHeaders();
        headers = headers.set('reset', 'reset');
        return this.http
            .post(environment.ws_url + '/' + url, values, { headers: headers })
            .pipe(
                tap((item: any) => this.debugStore(item, debugParams)),
                shareReplay(),
                map((item) => item.data),
                catchError((err) => this.debugError(err, debugParams)),
            );
    }
    createWithFiles(
        url: string,
        values: any,
        debugParams: string
    ): Observable<any> {
        const headers = new HttpHeaders({
            enctype: 'multipart/form-data',
            reset: url,
        });
        const formData = this.generaForm(values);
        return this.http
            .post(environment.ws_url + '/' + url, formData, { headers: headers })
            .pipe(
                tap((item: any) => this.debugStore(item, debugParams)),
                map((item) => item.data),
                shareReplay(),
                catchError((err) => this.debugError(err, debugParams)),
            );
    }

    delete(url: string, params: any, debugParams: string): Observable<any> {
        const values = this.url(params);
        return this.http.delete(environment.ws_url + '/' + url + values).pipe(
            tap((item: any) => this.debugStore(item, debugParams)),
            concatMap((item) => (item.success) ? of(item.data) : throwError(item.data)),
            shareReplay(),
            catchError((err) => this.debugError(err, debugParams)),
        );
    }
}
