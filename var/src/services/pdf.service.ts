import { Injectable } from '@angular/core';
import html2canvas from 'html2canvas';
import { jsPDF } from "jspdf";

@Injectable()
export class PdfService {
    constructor() { 
    }
    async generatePdf(data:any, name:string){
        /*let nodes = data.getElementsByTagName('mat-card');
        [...nodes].forEach(function(el){
            el.style.boxShadow = 'unset';
        });*/
        data.style.position = "absolute";
        data.style.paddingBottom = "40px";
        data.style.top = 0;
        html2canvas(data).then(canvas => {
            data.style.position = "initial";
            data.style.top = 0;
            data.style.paddingBottom = 0;
            const contentDataURL = canvas.toDataURL('image/jpeg');
            // Estas líneas son para testear que hay en el canvas
            /*canvas.toBlob(function(blob) {
                var newImg = document.createElement('img'),
                    url = URL.createObjectURL(blob);
              
                newImg.onload = function() {
                  // no longer need to read the blob so it's revoked
                  URL.revokeObjectURL(url);
                };
              
                newImg.src = url;
                document.body.appendChild(newImg);
              });*/
            const doc = new jsPDF('p', 'mm', 'a4');
            const imgProps = doc.getImageProperties(contentDataURL);
            var height = Math.floor(doc.internal.pageSize.getHeight() - 20);
            var width = Math.floor((imgProps.width * height) / imgProps.height);
            var left = (doc.internal.pageSize.getWidth() - width) / 2;
            doc.addImage(contentDataURL, 'JPEG', left, 0, width, height);
            doc.save(`${name}.pdf`);
            return true;
        })
    }
}