import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Md5 } from "md5-typescript";
import { sha256 } from 'js-sha256';

import { IResult } from 'src/models/iresult';
import { Observable } from 'rxjs';
import { CONFIG } from 'src/app/app.config';
import { User } from 'src/modules/user/models/user';
import { Site } from 'src/modules/site/models/site';
import { App } from 'src/modules/app/models/app';
import { Camera } from 'src/modules/camera/models/camera';
import { Customer } from 'src/modules/customer/models/customer';
import { TagCategory } from 'src/modules/tag/models/tagcategory';
import { IPlaylogic } from 'src/modules/playlogic/models/playlogic';
import { Player } from 'src/modules/player/models/player';
import { TranslateService } from '@ngx-translate/core';
@Injectable()
export class WSService {
	constructor(private http: HttpClient,
		private translate: TranslateService) {}
	private changeParamsIfDemo(params, people){
		if(CONFIG.demo && people){
			if(params.filter && params.filter.customer_id && params.filter.customer_id !== 31 && params.filter.customer_id !== 2) 
				params.filter.customer_id = 22;
			if(params.customer_id && params.customer_id !== 31 && params.customer_id !== 2)
				params.customer_id = 22;
			if(params.filter && params.filter.email)
				params.email = 'icreative@ladorian.com'
			if(params.email)
				params.email = 'icreative@ladorian.com'
		}
		if(CONFIG.demo && !people){
			if(params.site_id && params.customer_id !== 31){
				delete params.site_id;
			}
			if(params.idsite && params.idcustomer !== 31){
				delete params.idsite;
			}
			if(params.idcampaign && params.idcustomer !== 31){
				params.idcampaign = 23704;
			}
			if(params.idcontent && params.idcustomer !== 31){
				params.idcontent = 23704;
			}
			if(params.date_start && params.idcustomer !== 31)
				params.date_start = "2020-09-09";
			if(params.customer_id){
				params.customer_id = 31
			}
			if(params.idcustomer){
				params.idcustomer = 31
			}
		}
		return params;
	}
	private keyhashToTimestamp(timestamp: number): string {
		return Md5.init(timestamp + CONFIG.apikey);
	}
	
	private keyhashToTimestampSha256(timestamp: number): string {
		return sha256(timestamp + CONFIG.apikey);
	}

	public login(name: string, password:string, remember:boolean): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/public/login`, {name: name, password: password, remember: remember});
	}

	public recovery(data: any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/public/remember`, data);
	}

	public updatePassword(data: any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/public/update`, data);
	}

	public me(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/me`);
	}

	public users(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/users`);
	}

	public getUser(idUser: number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/user/${idUser}`);
	}

	public createUser(user: User): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/user`, user);
	}

	public updateUser(idUser: number, data: any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/user/${idUser}`, data);
	}

	public deleteUser(idUser: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/user/${idUser}`);
	}

	public roles(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/roles`);
	}
	
	public customers(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/customers`);
	}

	public customersToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/customer/toselect`);
	}

	public getCustomer(idCustomer: number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/customer/${idCustomer}`);
	}

	public createCustomer(customer: Customer): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/customer`, customer);
	}

	public updateCustomer(idCustomer: number, data: any): Observable<IResult> {
		const headers = new HttpHeaders();
		headers.append('Content-Type', 'text/html');
		

		return this.http.post<IResult>(`${CONFIG.ws_url}/private/customer/${idCustomer}`, data, { headers:headers });
	}

	
	public deleteCustomers(idcustomers: Customer[]): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/customer/delete`, {idcustomers});
	}

    public deleteCustomer(idcustomer: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/customer/${idcustomer}`);
	}

	public updateCustomerFormats(idCustomer:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/customer/${idCustomer}/formats`,data);
	}

	public deleteCustomerFormats(idCustomer:number, idFormat:number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/customer/${idCustomer}/format/${idFormat}`);
	}

	public apps(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/apps`);
	}

	public appsToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/apps/toselect`);
	}

	public getApp(idapp: number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/app/${idapp}`);
	}

	public createApp(app: App): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/app`, app);
	}

	public updateApp(idApp: number, data: any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/app/${idApp}`, data);
	}

	public deleteApp(idApp: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/app/${idApp}`);
	}

	public cameras(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/cameras`);
	}

	public getCamera(idcamera: number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/camera/${idcamera}`);
	}

	public createCamera(camera: Camera): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/camera`, camera);
	}

	public updateCamera(idCamera: number, data: any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/camera/${idCamera}`, data);
	}

	public deleteCamera(idCamera: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/camera/${idCamera}`);
	}

	public players(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/players`);
	}

	public getPlayer(idplayer: number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/player/${idplayer}`);
	}

	public createPlayer(player: Player): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/player`, player);
	}

	public updatePlayer(idPlayer: number, data: any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/player/${idPlayer}`, data);
	}

	public updatePlayerTags(idplayer:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/player/${idplayer}/tags`,data);
	}

	public updatePlayerPowers(idplayer:number, data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/player/${idplayer}/powers`,data);
	}

	public deletePlayer(idplayer: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/player/${idplayer}`);
	}

	public tags(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/tags`);
	}
	
	public tagsToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/tags/toselect`);
	}
	
	public tagCategories(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/tagcategories`);
	}

	public siteToSelectById(params:string): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/sites/toselect/${params}`);
	}

	public siteToSelect(): Observable<IResult> {
        console.log("WS SiteToSelect")
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/sites/toselect`);
	}

	public citiesToSelectById(idCountry:number,idProvince:number): Observable<IResult> {
		if(idCountry && idProvince)
			return this.http.get<IResult>(`${CONFIG.ws_url}/private/cities/toselect?idcountry=${idCountry}&idprovince=${idProvince}`);
		else if(idCountry)
			return this.http.get<IResult>(`${CONFIG.ws_url}/private/cities/toselect?idcountry=${idCountry}`);
		else if(idProvince)
			return this.http.get<IResult>(`${CONFIG.ws_url}/private/cities/toselect?idprovince=${idProvince}`);
		else
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/cities/toselect`);
	}

	public citiesToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/cities/toselect`);
	}

	public city(idcity: number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/city/${idcity}`);
	}

	public provincesToSelectById(idCountry:number): Observable<IResult> {
		if(idCountry)
			return this.http.get<IResult>(`${CONFIG.ws_url}/private/provinces/toselect/${idCountry}`);
		else
			return this.http.get<IResult>(`${CONFIG.ws_url}/private/provinces/toselect`);
	}

	public provincesToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/provinces/toselect`);
	}

	public countriesToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/countries/toselect`);
	}

	public totalEmissionsByCustomer(params): Observable<any> {
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestamp(timestamp);
		if(!params.site_id){
			return this.http.get<any>(`${CONFIG.analytics}/json/customer/count.${params.year}-${params.month}.json?keyhash=${keyhash}&timestamp=${timestamp}`);
		}
		else{
			return this.http.get<any>(`${CONFIG.analytics}/json/site/count.${params.year}-${params.month}.json?keyhash=${keyhash}&timestamp=${timestamp}`);
		}
	}
	// Llamadas al webservice de people
	public findPeople(params): Observable<IResult> {
		params = this.changeParamsIfDemo(params, true);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.post<IResult>(`${CONFIG.people}/ws/people/count/month/customer?keyhash=${keyhash}&timestamp=${timestamp}`, params);
	}

	public filterAndGroupPeople(params): Observable<IResult> {
		params = this.changeParamsIfDemo(params, true);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.post<IResult>(`${CONFIG.people}/ws/people/filterAndGroup?keyhash=${keyhash}&timestamp=${timestamp}`, params);
	} 

	public findPeopleChart(params): Observable<IResult> {
		params = this.changeParamsIfDemo(params, true);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.post<IResult>(`${CONFIG.people}/ws/people/count/month/cameraHour?keyhash=${keyhash}&timestamp=${timestamp}`, params);
	}

	public detailFacesByCamera(params: any, dashboard:boolean = false): Observable<IResult> {
		params = this.changeParamsIfDemo(params, true);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		if(!CONFIG.demo || !dashboard)
			return this.http.post<IResult>(`${CONFIG.people}/ws/people/filterAndGroup?keyhash=${keyhash}&timestamp=${timestamp}`, params);
		else
			return this.http.get<IResult>('assets/datos-afluencias-dashboard.json');
	}

	public getTotalFacesByCamera(params: any): Observable<IResult> {
		params = this.changeParamsIfDemo(params, true);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);	
		return this.http.post<IResult>(`${CONFIG.people}/ws/people/filterAndGroup?keyhash=${keyhash}&timestamp=${timestamp}`, params);
		
	}

	public analyticsAfluenciasSubmit(params: any): Observable<IResult>{
		params = this.changeParamsIfDemo(params, true);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.post<IResult>(`${CONFIG.people}/ws/people?keyhash=${keyhash}&timestamp=${timestamp}`, params);
	}
	// Llamadas a analitics
	public analyticsEmisionesCountByHour(params: any): Observable<IResult>{
		params = this.changeParamsIfDemo(params, false);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestamp(timestamp);
		return this.http.post<IResult>(`${CONFIG.analytics}/visualization/countByHour?keyhash=${keyhash}&timestamp=${timestamp}`, params);
	}

	public analyticsEmisionesCountByDay(params: any): Observable<IResult>{
		params = this.changeParamsIfDemo(params, false);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestamp(timestamp);
		return this.http.post<IResult>(`${CONFIG.analytics}/visualization/countByHour?keyhash=${keyhash}&timestamp=${timestamp}`, params);
	}

	public anayticsEmisionesFindAll(params: any): Observable<IResult>{
		params = this.changeParamsIfDemo(params, false);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestamp(timestamp);
		return this.http.post<IResult>(`${CONFIG.analytics}/visualization/findAll?keyhash=${keyhash}&timestamp=${timestamp}`, params);
	}

	public analyticsEmisionesCountByDateStr(params: any): Observable<IResult>{
		params = this.changeParamsIfDemo(params, false);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestamp(timestamp);
		return this.http.post<IResult>(`${CONFIG.analytics}/visualization/countByDateStr?keyhash=${keyhash}&timestamp=${timestamp}`, params);
	}

	public dashboardEmisionesCountByDate(params: any): Observable<any>{
		params = this.changeParamsIfDemo(params, false);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestamp(timestamp);
		return this.http.get<any>(`${CONFIG.analytics}/json/${params.cos}/countByDate.${params.date}.json?keyhash=${keyhash}&timestamp=${timestamp}`);
	}
	// Llamadas a emisiones
	public filterAndGroupEmission(params): Observable<IResult> {
		params = this.changeParamsIfDemo(params, false);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.post<IResult>(`${CONFIG.emissions}/ws/emission/filterAndGroup?keyhash=${keyhash}&timestamp=${timestamp}`, params);
	} 

	public emissionYears(params: any): Observable<IResult> {
		params = this.changeParamsIfDemo(params, false);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.post<IResult>(`${CONFIG.emissions}/ws/emission/years?keyhash=${keyhash}&timestamp=${timestamp}`, params);
	}

	public emissionMonths(params: any): Observable<IResult> {
		params = this.changeParamsIfDemo(params, false);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.post<IResult>(`${CONFIG.emissions}/ws/emission/months?keyhash=${keyhash}&timestamp=${timestamp}`, params);
	}

	public emissionSalesData(params: any): Observable<IResult>{
		if(CONFIG.demo && params.idcustomer !== 31)
			params = {
				dates:{
						minDate_:"2020-09-09",
						maxDate_:"2021-03-16",
						minDate_peant:"2019-09-09",
						maxDate_peant:"2020-03-16",
						minDate_campaign:"2020-09-09",
						maxDate_campaign:"2021-12-05"
					},
					idcustomer:31,
					idcontent:23704
				}
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/emission-sales/data`, params);
	}
	// Llamadas a tickets
	public getImportesByMonth(params): Observable<IResult> {
		params = this.changeParamsIfDemo(params, false);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.post<IResult>(`${CONFIG.tickets}/ws/tickets/amounts/byMonth?keyhash=${keyhash}&timestamp=${timestamp}`, params);
	}

	public getImportesByMonthByDay(params): Observable<IResult> {
		params = this.changeParamsIfDemo(params, false);
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.post<IResult>(`${CONFIG.tickets}/ws/tickets/amounts/byMonthByDay?keyhash=${keyhash}&timestamp=${timestamp}`, params);
	}

	public getProductsFromCustomer(){
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/products/toselect`);
	}
	public getProductsCategoriesContentsFromUser(){
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/categories/products/contents`);
	}
	public amountsByDateFromJson(date:string, type:string): Observable<any>{
		let timestamp: number = new Date().getTime();
		return this.http.get<any>(`${CONFIG.tickets}/json/${type}/count-amount-byDate.${date}.json?time=${timestamp}`);
	}

	public amountsByMonthFromJson(date:string, type:string): Observable<any>{
		let timestamp: number = new Date().getTime();
		return this.http.get<any>(`${CONFIG.tickets}/json/${type}/count-amount-byDate.${date}.json?time=${timestamp}`);
	}

	public getProductCategories(): Observable<IResult>{
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/products/categories/toselect`);
	}

	public playcircuitsToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/playcircuits/toselect`);
	}

	public playcircuits(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/playcircuits`);
	}

	public playcircuit(idCircuit:number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/playcircuit/${idCircuit}`);
	}

	public deletePlaycircuit(idCircuit:number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/playcircuit/${idCircuit}`);
	}

	public createPlaycircuit(data:any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/playcircuit`,data);
	}

	public updatePlaycircuit(idCircuit:number,data:any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/playcircuit/${idCircuit}`,data);
	}

	public updatePlaycircuitTags(idCircuit:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/playcircuit/${idCircuit}/tags`,data);
	}

	public updatePlaycircuitCountries(idCircuit:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/playcircuit/${idCircuit}/countries`,data);
	}

	public updatePlaycircuitProvinces(idCircuit:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/playcircuit/${idCircuit}/provinces`,data);
	}

	public updatePlaycircuitCities(idCircuit:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/playcircuit/${idCircuit}/cities`,data);
	}

	public updatePlaycircuitSites(idCircuit:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/playcircuit/${idCircuit}/sites`,data);
	}

	public updatePlaycircuitLangs(idCircuit:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/playcircuit/${idCircuit}/langs`,data);
	}

	public playareas(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/playareas`);
	}

	public contents(date?: string): Observable<IResult> {
		let param = (date) ? `?date=${date}` : '';
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/contents${param}`);
	}

	public contentsByMonth(month: string): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/contents-by-month/${month}`);
	}

	public contentsToSelect(): Observable<IResult> {
       return this.http.get<IResult>(`${CONFIG.ws_url}/private/contents/toselect`);
	}

	public contentsOrder(data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/contents/order`,{"contents": data});
	}

	public contentsOrders(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/contents/orders`);
	}

	public updateContent(idcontent:number, contentData:any, assets?: any, circuits?: any, areas?: any, tags?: any, powers?: any, products?: any, dates?: any, sites?: any): Observable<IResult> {
        let data: any = { form: contentData };
        if (assets && Array.isArray(assets))
            data.assets = assets;
        if (circuits && Array.isArray(circuits))
            data.circuits = circuits;
        if (areas && Array.isArray(areas))
            data.areas = areas;
        if (tags && Array.isArray(tags))
            data.tags = tags;
        if (powers && Array.isArray(powers))
            data.powers = powers;
        if (products && Array.isArray(products))
            data.products = products;
        if (dates && Array.isArray(dates))
            data.dates = dates;
        if (sites&& Array.isArray(sites))
            data.sites = sites;    
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/content/${idcontent}`,data);
	}
	
	public replaceContentOrderToArea(idarea: number, idcategory: number, data: any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/playarea/${idarea}/contents/${idcategory}`, {contents: data});
	}

	public replaceContentOrderToPlayer(idplayer: number, idcategory: number, data: any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/player/${idplayer}/contents/${idcategory}`, {contents: data});
	}

	public playareasToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/playareas/toselect`);
	}
	public playareasToShow(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/playareas/toshow`);
	}

	public playarea(idArea:number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/playarea/${idArea}`);
	}

	public deletePlayarea(idArea:number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/playarea/${idArea}`);
	}

	public createPlayarea(data:any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/playarea`,data);
	}

	public updatePlayarea(idArea:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/playarea/${idArea}`,data);
	}

	public createIndoorLocation(data:any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/indoorlocation`,data);
	}

	public updateIndoorLocation(id: number, data: any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/indoorlocation/${id}`,data);
	}

	public deleteIndoorLocation(id: number) {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/indoorlocation/${id}`);
	}

	public locations(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/indoorlocations`);
	}

	public location(idLocation:number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/indoorlocation/${idLocation}`);
	}

	public formats(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/formats`);
	}

	public getFormat(idformat:number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/format/${idformat}`);
	}

	public updateFormat(idformat:number, data): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/format/${idformat}`, data);
	}

	public deleteFormat(idFormat: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/format/${idFormat}`);
	}

	public updateFormatCustomers(idformat:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/format/${idformat}/customers`,data);
	}

	public formatsToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/formats/toselect`);
	}

	public createFormat(data:any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/format`,data);
	}

	public hideFormat(idcustomer: number, idformat: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/customer/${idcustomer}/format/${idformat}`);
	}

	public locationsToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/indoorlocations/toselect`);
	}

	//contents
	public createContent(contentData:any, assets?: any, circuits?: any, areas?: any, tags?: any, powers?: any, products?: any, dates?: any, sites?: any): Observable<IResult> {
		let data: any = { form: contentData };
        if (assets && assets.length > 0)
            data.assets = assets;
        if (circuits && circuits.length > 0)
            data.circuits = circuits;
        if (areas && areas.length > 0)
            data.areas = areas;
        if (tags && tags.length > 0)
            data.tags = tags;
        if (powers && powers.length > 0)
            data.powers = powers;
        if (products && products.length > 0)
            data.products = products;
        if (dates && dates.length > 0)
            data.dates = dates;
        if (sites && sites.length > 0)
            data.sites = sites;    
        return this.http.post<IResult>(`${CONFIG.ws_url}/private/content`,data);
	}

	public updateContentPlayareas(idContent:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/playareas`,data);
	}

	public updateContentPlaycircuits(idContent:number,data:any): Observable<IResult> {
		//return this.http.put<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/playcircuits`,data);
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/playciruits`,data);
	}

	public updateContentSites(idContent:number,data:any): Observable<IResult> {
		//return this.http.put<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/playcircuits`,data);
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/sites`,data);
	}

	public updateContentDates(idContent:number,data:any): Observable<IResult> {
		//return this.http.put<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/playcircuits`,data);
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/dates`,data);
	}

	public updateContentTags(idContent:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/tags`,data);
	}

	public updateContentProducts(idContent:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/products`,data);
	}

	public updateContentPowers(idContent:number, data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/powers`,data);
	}

	public updateContentAssets(idContent:number, data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/assets`,data);
	}
	
	//Delete
	public deleteContent(idContents:number[]): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/contents/delete`, {idContents});
	}


	public deleteContentPlayareas(idContent:number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/playareas`);
	}

	public deleteContentPlaycircuits(idContent:number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/playciruits`);
	}

	public deleteContentTags(idContent:number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/tags`);
	}

	public deleteContentProducts(idContent:number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/products`);
	}

	public deleteContentPowers(idContent:number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/powers`);
	}

	public deleteContentAssets(idContent:number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/content/${idContent}/assets`);
	}

	public contentCategoriesToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/content-categories/toselect`);
	}

	public contentCategories(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/content-categories`);
	}

	public contentCategory(idCategory:number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/content-category/${idCategory}`);
	}

	public deleteCategory(idCategory:number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/content-category/${idCategory}`);
	}

	public createCategory(data:any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/content-category`,data);
	}

	public updateCategory(idCategory:number,data:any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/content-category/${idCategory}`,data);
	}

	public contentTypesToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/content-types/toselect`);
	}

	public content(idcontent): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/content/${idcontent}`);
	}

	/* ASSETS */
	public assets(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/assets`);
	}

	public assetsToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/assets/toselect`);
	}

	public deleteAsset(id: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/asset/${id}`);
	}

	public deleteAssetForce(id: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/asset/${id}/force`);
	}

	public updateAsset(id: number, data: any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/asset/${id}`, data);
	}

	public createAsset(data: any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/asset`, data);
	}

	public assetTypesToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/asset-types/toselect`);
	}

	public contentsToEmission(params): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/contents/toemission`,params);
	}

	public playersToAnalytics(params: Object): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/players/toanalytics`, params);
	}

	public playersToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/players/toselect`);
	}

	public camerasToAnalytics(params: Object): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/cameras/toanalytics`, params);
	}

	/*public camerasToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/cameras/toselect`);
	}*/

	public updateCameraPowers(idcamera:number, data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/camera/${idcamera}/powers`,data);
	}

	public langs(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/langs`);
	}

	public langToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/langs/toselect`);
	}

	public tagCategoryToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/tagcategories/toselect`);
	}

	public tagById(idTag: number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/tag/${idTag}`);
	}

	public createTagCategory(TagCategory: any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/tagcategory`, TagCategory);
	}

	public updateTag(idTag: number, data: any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/tag/${idTag}`, data);
	}
	
	public updateTagCategory(idCategoryTag: number, data: any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/tagcategory/${idCategoryTag}`, data);
	}
	
	public createTag(Tag: any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/tag`, Tag);
	}
	
	public deleteTagCategory(idCategory: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/tagcategory/${idCategory}`);
	}
	
	public deleteTag(idTag: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/tag/${idTag}`);
	}
	
	public langsToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/langs/toselect`);
	}

	public playlogicToSelect() : Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/playlogics/toselect`);
	}

	public replacePlaylogicToArea(idarea: number, data: IPlaylogic[]): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/playarea/${idarea}/playlogics`, {playlogics: data});
	}

	public replacePlaylogicToPlayer(idplayer: number, data: IPlaylogic[]): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/player/${idplayer}/playlogics`, {playlogics: data});
	}

	public areaToSelect() : Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/playareas/toselect`);
	}

	public playerToSelect() : Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/players/toselect`);
	}

	public getProductsFromContent(idcontent){
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/content/${idcontent}/products`);
	}

	public getProductsFromCategory(params) : Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-products-from-category`, params);
	}

	public getSocialEconomicData(params) : Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-social-demographic-data`, params);
	}

	public getSocialEconomicAdminData(params): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-social-economic-admin-data`, params);
	}
	public getSocialSegmentationAdminData(params): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-segmentation-admin-data`, params);
	}
	public getProvincesOfCustomer() : Observable<IResult>{
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-provinces-from-customer`);
	}
	public getSitesByProvinces(params = null): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-sites-from-provinces`, params);
	}
	public getRatiusOfSites(params = null) : Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-ratius-from-sites`, params);
	}

	public getImpactSummary(data:any){
		let params = {
			idcustomer: data.customer,
			year: data.year,
			month: data.month
		}
		if(CONFIG.demo)
			params.idcustomer = 31;
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/impact/resumen`, params);
	}

	public updatePlaylogic(idplaylogic: number, data: any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/playlogic/${idplaylogic}`, data);
	}

	public createPlaylogic(data:any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/playlogic`,data);
	}

	public deletePlaylogic(idplaylogic: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/playlogic/${idplaylogic}`);
	}

	public getAssociationsCategories(): Observable<IResult>{
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/recomendations/associations/categories`);
	}

	public getAssociationsConditionalProbability(idcategory: number): Observable<IResult>{
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/recomendations/associations/conditional-probability/${idcategory}`);
	}

	public getRecomendationsCategories(month: number): Observable<IResult>{
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/categories/${month}`);
	}

	public getRecomendationsTrendsCategories(): Observable<IResult>{
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/trends-categories`);
	}

	public getPatronVentas(params:any): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-patron-ventas`, params)
	}

	public getfilasdiassemana(params:any): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-filas-dias-semana`, params)
	}

	public getTemperature(params:any): Observable<any>{
		return this.http.post<any>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-temperature`, params)
	}

	public getProductPrimeTime(params:Object): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/content/products/primetime`, params);
	}

	public getContentPrimeTime(params:Object): Observable<IResult>{
       return this.http.post<IResult>(`${CONFIG.ws_url}/private/content/content/primetime`, params);
	}

	public getCoordinatesFromAddres(address:string): Observable<any>{
		return this.http.get(`${CONFIG.gmaps_api}/geocode/json?address=${address}&language=${this.translate.currentLang}&key=${CONFIG.gmaps_apikey}`);
	}

	public getAutocompleteData(string:string): Observable<any>{
		return this.http.get(`${CONFIG.gmaps_api}/place/autocomplete/json?input=${string}&types=address&language=${this.translate.currentLang}&key=${CONFIG.gmaps_apikey}`);
	}

	public getTestPrimetime(data:any): Observable<any>{
		return this.http.post(`${CONFIG.ws_url}/private/algorithm/primetime`, data);
	}
	
	public getTestAsociaciones(data:any): Observable<any>{
		//return this.http.post(`${CONFIG.python_url}/asociaciones`, data);
		return this.http.post(`${CONFIG.ws_url}/private/algorithm/associations`, data);
	}

	public getTestSocioeconomico(data:any): Observable<any>{
		//return this.http.post(`${CONFIG.python_url}/asociaciones`, data);
		return this.http.post(`${CONFIG.ws_url}/private/algorithm/socioeconomico`, data);
	}

	public saveTranslation(lang:string, data: any): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/translation/${lang}/save`, data)
	}
	public getAgeVars(): Observable<IResult>{
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-age-vars`)
	}
	public getIncomeRanges(data): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-income-ranges`, data)
	}

	public getDemographicSegmentation(data): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-demographic-segmentation`, data)
	}
	public getEconomicSegmentation(data): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-economic-segmentation`, data)
	}

	public getAllProductTags(params): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/product/tags`, params)
	}

	public updateProductTags(params): Observable<IResult>{
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/product/${params.idproduct}/tags`, params.idtags)
	}

	/* public generarPlaylistByCustomer(idcustomer): Observable<IResult>{
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/generateplaylist/customer/${idcustomer}`)
	}

	public generarPlaylistBySite(idsite): Observable<IResult>{
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/generateplaylist/site/${idsite}`)
	}

	public generarPlaylistByPlayer(idplayer): Observable<IResult>{
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/generateplaylist/player/${idplayer}`)
	}

	public generarPlaylist(): Observable<IResult>{
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/generateplaylist`)
	}
 */
	public sendNotificacionUpdateByCustomer(idcustomer): Observable<IResult>{
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/sendNotificacion/update/customer/${idcustomer}`)
	}

	public sendNotificacionRefreshByCustomer(idcustomer): Observable<IResult>{
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/sendNotificacion/refresh/customer/${idcustomer}`)
	}

	public sendNotificacionRefreshBySite(idsite): Observable<IResult>{
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/sendNotificacion/update/site/${idsite}`)
	}

	public sendNotificacionUpdateBySite(idsite): Observable<IResult>{
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/sendNotificacion/refresh/site/${idsite}`)
	}

	public sendNotificacionByPlayer(idplayer): Observable<IResult>{
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/sendNotificacion/player/${idplayer}`)
	}

	public sendNotificacionByArea(idarea): Observable<IResult>{
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/sendNotificacion/area/${idarea}`)
	}	
	
	public sendMailAlertGeneratePlaylist(idcontent): Observable<IResult>{
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/sendalertemailplaylist/${idcontent}`)
	}

	public uploadTicketsFiles(customer, file): Observable<IResult>{
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.post<IResult>(`${CONFIG.tickets}/ws/import/upload/xlsx/${customer}?keyhash=${keyhash}&timestamp=${timestamp}`, file);
	}
	public uploadTicketsFilesToCore(customer, file): Observable<IResult>{
		let timestamp: number = new Date().getTime();
		let keyhash: string = this.keyhashToTimestampSha256(timestamp);
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/recomendations/upload/xlsx/${customer}`, file);
	}

	public updateRecomedationsData(params): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/recomendations/update`, params);
	}

	public templatesCustomers(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/templatescustomers`);
	}

	public templatesSectors(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/templatessectors`);
	}

	public updateTemplatesCustomer(id:number,data:any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/templatescustomer/replace/${id}`,data);
	}

	public templatesCustomersByIdcustomerMaster(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/templatescustomersbyidcustomermaster`);
	}

	public templates(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/templates`);
	}

	public deleteTemplate(idtemplate: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/template/${idtemplate}`);
	}
	public template(idtemplate: number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/template/${idtemplate}`);
	}
	public createTemplate(data:any): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/template`,data);
	}
	public updateTemplate(idtemplate: number, data: any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/template/${idtemplate}`, data);
	}

	public landingpages(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/landingpages`);
	}

	public landingpage(id: number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/landingpage/${id}`);
	}

	public getRecomendationsTrendsProvinces(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/recomendations/recomendations/get-trends-provinces`);
	}

	public getCamerasFromPusher(): Observable<IResult>{
		const params = {
			"publish_key": "pub-c-2478datb-aeba-425t-dt52-9d6b6c68id53",
			"subscribe_key": "sub-c-se83462d-cv32-89s6-a9dy-03vf4jdio3sk"	
		}
		return this.http.post<IResult>(`${CONFIG.socket_url}/private/cameras/room`, params);
	}

	public deleteHtmlTemplate(id: number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.template}/borrarplantilla.php?html=${id}`);
	}

	// Para BORRAR Cuando esté cambiado site.service
	public sites(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/sites`);
	}
	public getSite(idsite: number): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/site/${idsite}`);
	}
	public getSiteToSelect(): Observable<IResult> {
		return this.http.get<IResult>(`${CONFIG.ws_url}/private/sites/toselect`);
	}
	public createSite(site: Site): Observable<IResult> {
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/site`, site);
	}
	public updateSite(idSite: number, data: any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/site/${idSite}`, data);
	}
	public deleteSite(idSite: number): Observable<IResult> {
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/site/${idSite}`);
	}
	public updateSiteTags(idsite:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/site/${idsite}/tags`,data);
	}
	public updateSitePlaycircuits(idsite:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/site/${idsite}/playcircuits`,data);
	}
	public updateSiteHolidays(idsite:number,data:any): Observable<IResult> {
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/site/${idsite}/holidays`,data);
	}
	public createProduct(data): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/product`, data);
	}
	public editProduct(idproduct, data): Observable<IResult>{
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/product/${idproduct}`, data);
	}
	public deleteproduct(idproduct): Observable<IResult>{
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/product/${idproduct}`);
	}
	public createProductCategory(data): Observable<IResult>{
		return this.http.post<IResult>(`${CONFIG.ws_url}/private/product/category`, data);
	}
	public editProductCategory(idcategory, data): Observable<IResult>{
		return this.http.put<IResult>(`${CONFIG.ws_url}/private/product/category/${idcategory}`, data);
	}
	public deleteproductCategory(idcategory): Observable<IResult>{
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/product/category/${idcategory}`);
	}
	public detachproductFromContent(idproduct, idcontent): Observable<IResult>{
		return this.http.delete<IResult>(`${CONFIG.ws_url}/private/product/${idproduct}/content/${idcontent}`);
	}

	public testService():Observable<boolean> {
		return this.http.get<boolean>(`${CONFIG.ws_url}/public/test`);
	}
}
