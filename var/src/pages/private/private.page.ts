import { CONFIG, APP, VERSION } from 'src/app/app.config';
import { Component, Input, ViewChild } from '@angular/core';
import { DebugStore } from 'src/stores/debug.store';
import { AuthStore } from 'src/modules/auth/stores/auth.store';
import { NavbarStore } from 'src/modules/menu/stores/navbar.store';
import { Router, ActivatedRoute, NavigationEnd  } from '@angular/router';
import { MYLocalStorage } from 'src/services/localstorage.service';
import { WSService } from 'src/services/ws.service';
import { UserStore } from 'src/modules/user/stores/user.store';
import { MatSidenav } from '@angular/material/sidenav';
import { MatMenuTrigger } from '@angular/material/menu';
import { IUser, User } from 'src/modules/user/models/user';
import { INavbar, Navbar } from 'src/modules/menu/models/navbar';
import { IResult } from 'src/models/iresult';
import { CookieService } from 'ngx-cookie-service';
import { filter } from 'rxjs/operators';
import { DashboardService } from 'src/services/dashboard.service';
import { IDashboardMessage } from 'src/interfaces/IDashboardMessage';

@Component({
	providers: [NavbarStore],
	selector: 'private-page',
  	templateUrl: './private.page.html',
  	styleUrls: ['./private.page.scss']
})

export class PrivatePage {

	@ViewChild('sidenav') sidenav: MatSidenav;
	@ViewChild('MatMenuTrigger') trigger: MatMenuTrigger;
	@Input() company:string;
	
	public showSubmenu: boolean = false;
	public showSubSubMenu: boolean = false;
	public panelOpenState = false;
	public status: boolean = false;
	public me: any;
	public navbar: Navbar = {
		sites:0,
		cameras:0,
		players:0
	};
	public titleBreadcrumbs: string;
	public sitesCount: number = 1;
	public playersCount: number = 1;
	public camerasCount: number = 0;
	public config = CONFIG;
	public step = 0;
	public app = APP;
	public version = VERSION;

	public isShown: boolean = false;
	public messages: IDashboardMessage[] = [];

    constructor(
		private store: AuthStore, 
		private nbstore: NavbarStore, 
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private localStorage: MYLocalStorage,
		private ws: WSService,
		private debug: DebugStore,
		private ustore: UserStore,
		private cookieService: CookieService,
		private dashboardService: DashboardService
	) {

		this.titleBreadcrumbs = this.activatedRoute.snapshot.routeConfig.children[0].path;
		this.ustore.onChange.subscribe((user: User) => {
			this.me = user;
			
			this.dashboardService.getLastMessages().subscribe( 
				response => {
					if (response['success'] && response['data']) {
						this.messages = response['data']['entities'];

						this.messages = this.messages.filter(item => { return item.customer_id === this.me.customer.idcustomer || item.customer_id === 0; });
					}
				},
				error => {
					console.error(error);
				}
			);

		});

		this.nbstore.onChange.subscribe((navbar: Navbar) => {
			if(navbar)
				this.navbar = navbar;
		});

		router.events.pipe(
			filter(event => event instanceof NavigationEnd)  
		).subscribe((event: NavigationEnd) => {

			let urlParts = event.url.split('/');
			let name = urlParts[2];
			let page_size = this.cookieService.get('page-size');
			let sort_active = this.cookieService.get(name+'-sort-active');
			let sort_direction = this.cookieService.get(name+'-sort-direction');
			let filter = this.cookieService.get(name+'-filter');
			this.cookieService.deleteAll();

			if (page_size)
				this.cookieService.set('page-size', page_size);
			if (sort_active)
				this.cookieService.set(name+'-sort-active', sort_active);
			if (sort_direction)	
				this.cookieService.set(name+'-sort-direction', sort_direction);
			if (filter) 
				this.cookieService.set(name+'-filter', filter);			
		});

	}

	public ngOnInit(): void {

		this.ws.me().subscribe((result: IResult) => {
			if(result.success) {
				this.debug.addInfo('WS.Login', 'successful');
				this.saveUser(result.data.user);
			} else {            
				this.debug.addError('WS.Register', result.data);
				return null;			
			}
		}, error => {
			this.debug.addError('WS.Register', error.message);  
			return null;          
		});
	}

	public toggleShow() {
		this.isShown = !this.isShown;
	}

	public logout() {  
		this.ustore.delete();
		this.store.clear();  
		this.localStorage.clear();
		this.router.navigate(['/login']);
		let page_size = this.cookieService.get('page-size');
		this.cookieService.deleteAll();
		this.cookieService.set('page-size', page_size);
	}	

	public setStep(index: number) {
		this.step = index;
	}

	public nextStep() {
		this.step++;
	}

	public prevStep() {
		this.step--;
	}

	private saveUser(data: IUser) {
		this.ustore.set(data);				
	}

}
