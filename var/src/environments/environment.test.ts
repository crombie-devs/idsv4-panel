export const environment = {
    production: true,
    demo:false,
  
    apikey: 'ALRIDKJCS1SYADSKJDFS',
    ws_url: 'https://testv4.ladorianids.es',
    lang_url: 'https://testv4.ladorianids.es',
    socket_url: 'https://pusher.ladorianids.com',
    people: 'https://people.ladorianids.es',
    emissions: 'https://emissions.ladorianids.es',
    tickets: 'https://tickets.ladorianids.es',
    analytics: 'https://analytics.ladorianids.com',
    preview: 'https://testv4.ladorianids.es/ids-front/preview',
    preview_text: 'https://testv4.ladorianids.es/ids-front/preview-text',
    streaming_url: 'rtmp://streaming.ladorianids.es:1935/live',
    gmaps_api: 'https://maps.googleapis.com/maps/api',
    gmaps_apikey: 'AIzaSyBUiScH6TLvE-MAdHX_5Rib83pkr7rDcd0',
    //python_url: 'http://localhost:5000',
    python_url: 'http://82.223.69.121:5000',
    template: 'https://testv4.ladorianids.es/plantillascontenidov4',
    support_url: 'https://testv4.ladorianids.es/support',
    ids_front: 'https://testv4.ladorianids.es',
    localstorage: false
};