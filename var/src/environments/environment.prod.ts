export const environment = {
  production: true,
  demo:false,
  
  apikey: 'ALRIDKJCS1SYADSKJDFS',
  ws_url: 'https://ids.ladorianids.es',
  lang_url: 'https://ids.ladorianids.es',
  socket_url: 'https://pusher.ladorianids.com',
  people: 'https://people.ladorianids.es',
  emissions: 'https://emissions.ladorianids.es',
  tickets: 'https://tickets.ladorianids.es',
  analytics: 'https://analytics.ladorianids.com',
  preview: 'https://ids.ladorianids.es/ids-front/preview',
  preview_text: 'https://ids.ladorianids.es/ids-front/preview-text',
  ids_front:'https://ids.ladorianids.es',
  streaming_url: 'rtmp://streaming.ladorianids.es:1935/live',
  gmaps_api: 'https://maps.googleapis.com/maps/api',
  gmaps_apikey: 'AIzaSyBUiScH6TLvE-MAdHX_5Rib83pkr7rDcd0',
  python_url: 'http://82.223.69.121:5000',
  support_url: 'https://ids.ladorianids.es/support',
  template: 'https://ids.ladorianids.es/plantillascontenidov4',
  localstorage: false
};
