// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    demo: false,
    apikey: 'ALRIDKJCS1SYADSKJDFS',
    //ws_url: 'http://api.ladorianids.v4',
    ws_url: 'http://localhost',
    //ws_url: 'http://192.168.1.106:8081',
    //ws_url: 'https://dev4.ladorianids.es',
    //ws_url: 'http://localhost:8000',
    //ws_url: 'https://testv4.ladorianids.es',
    ws_language: 'http://localhost',
    socket_url: 'https://pusher.ladorianids.com',
    people: 'https://people.ladorianids.es',
    emissions: 'https://emissions.ladorianids.es',
    tickets: 'https://tickets.ladorianids.es',
    // tickets: 'http://localhost:3000',
    analytics: 'https://analytics.ladorianids.com',
    preview: 'https://testv4.ladorianids.es/ids-front/preview',
    preview_text: 'https://testv4.ladorianids.es/ids-front/preview-text',
    /* preview: 'http://localhost:4200/preview',
    preview_text: 'http://localhost:4200/preview-text', */
    ids_front: 'https://dev4.ladorianids.es',
    streaming_url: 'rtmp://streaming.ladorianids.es:1935/live',
    gmaps_api: 'https://maps.googleapis.com/maps/api',
    gmaps_apikey: 'AIzaSyBUiScH6TLvE-MAdHX_5Rib83pkr7rDcd0',
    python_url: 'https://82.223.69.121:5000',
    support_url: 'https://ids.ladorianids.es/support',
    lang_url: 'http://localhost',
    //template: 'http://localhost:8888'
    // template: 'https://ids.ladorianids.es/plantillascontenidov4'
    // template: 'http://localhost:8888'
    template: 'https://dev4.ladorianids.es/plantillascontenidov4',
    localstorage: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
