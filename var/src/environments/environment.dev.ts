export const environment = {
    production: false,
    demo: false,

    apikey: 'ALRIDKJCS1SYADSKJDFS',
    ws_url: 'http://35.205.85.234',
    lang_url: 'http://35.205.85.234',
    socket_url: 'https://pusher.ladorianids.com',
    people: 'https://people.ladorianids.es',
    emissions: 'https://emissions.ladorianids.es',
    tickets: 'https://tickets.ladorianids.es',
    analytics: 'https://analytics.ladorianids.com',
    preview: 'https://dev4.ladorianids.es/ids-front/preview',
    preview_text: 'https://dev4.ladorianids.es/ids-front/preview-text',
    ids_front: 'https://dev4.ladorianids.es',
    streaming_url: 'rtmp://streaming.ladorianids.es:1935/live',
    gmaps_api: 'https://maps.googleapis.com/maps/api',
    gmaps_apikey: 'AIzaSyBUiScH6TLvE-MAdHX_5Rib83pkr7rDcd0',
    python_url: 'https://82.223.69.121:5000',
    support_url: 'https://dev4.ladorianids.es/support',
    template: 'https://dev4.ladorianids.es/plantillascontenidov4',
    localstorage: false
};
