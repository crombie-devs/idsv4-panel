export class Hour {

  count: number;
  hour: number;

  constructor(obj?: any) {
    if(!obj)
      return;

    this.count = obj.count;
    this.hour = obj.hour;   

  }
 
}