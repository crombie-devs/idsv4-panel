export interface IMessage {
    date: Date;
	title: string;
	description: string;
}

export class Message {

    public date: Date;
	public title: string;
	public description: string;
    
    constructor(msg?: IMessage) {
        if(!msg){ return; }

        this.date = msg.date;
        this.title = msg.title;
        this.description = msg.description;
    }

}