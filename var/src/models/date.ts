export interface IContentDate {
    date_on: string;
    date_off: string;
}
  
export class ContentDate implements IContentDate {
  date_on: string;
  date_off: string;
  
    constructor(obj?: IContentDate) {
      if(!obj)
        return;
  
      this.date_on = obj.date_on;
      this.date_off = obj.date_off; 
  
    }
  
}