export const LOG_TYPE = {INFO: 'info', IMPORTANT:'important', ALERT: 'alert', ERROR: 'error'};

import moment from 'moment';


export class Log {
	level: string;
	title: string;
	description: string;
	date: Date;

	constructor(level: string, title:string, description: string) {
		this.level = level;
		this.title = title;
		this.description = description;
		this.date = new Date();
	}

	public dateToString(): string {
		return moment(this.date).format('HH:mm:ss.SSS DD/MM/YYYY'); 
	}

	public equal(log: Log): boolean {
		return (this.level === log.level && this.title === this.title && this.description === this.description);
	}
}