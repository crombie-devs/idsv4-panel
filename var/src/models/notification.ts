import { IMessage, Message } from './message';

export enum Level {ERROR = 0, WARNING = 1, INFO = 2, UNKNOWN = 3};

export interface INotification {
	from: string;
	date: Date;
	event: string;
	data: IMessage | any;
}

export class Notification {
    public from: string;
	public date: Date;
	public level: Level;
    public message: Message;
    
    constructor(not?: Notification) {
        if(!not){ return; }

        this.from = not.from;
        this.date = not.date;
        this.level = not.level;
        this.message = new Message(not.message);
    }

    public initJson(not: INotification) {
        this.from = not.from;
        this.date = not.date;
        this.level = this.eventToLevel(not.event);
        this.message = new Message(not.data);
    }

    private eventToLevel(event: string): Level {
        let level: Level = Level.UNKNOWN;
        if(event === 'log.error')
            level = Level.ERROR;
        else if(event === 'log.alert')
            level = Level.WARNING;
        else if(event === 'log.important')
            level = Level.INFO;

        return level;
    }

    public levelName(): string {
        let name = 'unknown';
        if(this.level === Level.ERROR)
            name = 'error';
        else if(this.level === Level.WARNING)
            name = 'warning';
        else if(this.level === Level.INFO)
            name = 'info';

        return name;
    }

    public levelIcon(): string {
        let name = 'unknown';
        if(this.level === Level.ERROR)
            name = 'report';
        else if(this.level === Level.WARNING)
            name = 'warning';
        else if(this.level === Level.INFO)
            name = 'info';

        return name;
    }

}