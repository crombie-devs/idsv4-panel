export interface IResult {
    success: boolean;
    data: any;
    message: string |  null;
    count: number;
}