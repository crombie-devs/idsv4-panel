import { Message } from './message';

export enum Level {ERROR = 0, WARNING = 1, INFO = 2, UNKNOWN = 3};

export interface IStreamMessage {
	from: string;
    name: string;
    to: string;
	data: any;
}

export class StreamMessage {
    public from: string;
    public name: string;
    public to: string;
    public data: any;
    
    constructor(mess?: StreamMessage) {
        if(!mess){ return; }

        this.from = mess.from;
        this.name = mess.name;
        this.to = mess.to;
        this.data = new Message(mess.data);
    }

    public initJson(mess: IStreamMessage) {
        this.from = mess.from;
        this.name = mess.name;
        this.to = mess.to;
        this.data = new Message(mess.data);
    }

}