import { Pipe, PipeTransform } from '@angular/core';

   
   @Pipe({
     name: 'currencyCustom'
   })
   export class CurrencyCustomPipe  implements PipeTransform {
  
     transform(value: any): any {
       if(value) {
        const parts = value.split(".");

        if(parts[0].length > 3) {
 
         const numeros = parts[0].split("");
         let n = 0;
         let salida = "";
 
         for(let a = 0 ; a < numeros.length; a ++) {
            
             
             salida = salida + numeros[a];
             if(n === 3) {
 
                 salida =   salida +  "." ;
                 n = 0
             }
    
          n++
         }
 
         
        return salida + "," +parts[1];
 
 
        }  else {
 
              return parts[0] + ',' +parts[1];
 
        }
       } 
      



     }
     
   }