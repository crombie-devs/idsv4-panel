import { SafePipe } from "./safe.pipe";
import { ReversePipe } from "./reverse.pipe";
import { CustomDatePipe } from "./custom-date.pipe";
import { SpanishDecimalPipe } from './custom-number.pipe';
import { FilterByNamePipe } from "./filterByName.pipe";
import { FilterAssetByNamePipe } from "./filterAssetByName.pipe"
import { CurrencyCustomPipe } from "./currency.pipe";

export const PIPES = [
    SafePipe,
    ReversePipe,
    CustomDatePipe,
    FilterByNamePipe,
    SpanishDecimalPipe,
    FilterAssetByNamePipe,
    CurrencyCustomPipe
]