import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterByNamePipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultFilter = [];
   
    
      for(const data of value){
       
      if(data.name.toLowerCase().indexOf(arg.trim().toLowerCase()) > -1){
         resultFilter.push(data);
      };
    };
    return resultFilter;
    
    
  }

}