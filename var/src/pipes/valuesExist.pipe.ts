import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';

@Pipe({ name: 'valuesExist' })
export class ValuesExistPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  transform(value, type ,data) {
     
     
    if(data[type]) {
      
        if(data.hasOwnProperty(type) && data[type]['name'] != null)
        {  
           if(data[type].hasOwnProperty('name'))  
              return data[type].name;  
        } 
 }
    return 'not_defined';

  }
} 