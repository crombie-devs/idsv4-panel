import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'messageStatus' })
export class MessageStatusPipe implements PipeTransform {
  constructor() {}
  transform(status: number) {
    let typeStatus = ["Pendiente", "En proceso", "Enviado"]  
    return typeStatus[status];
  }
} 