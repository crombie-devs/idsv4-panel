import { NgModule } from '@angular/core';
import { SafePipe } from "./safe.pipe";
import { ReversePipe } from "./reverse.pipe";
import { CustomDatePipe } from "./custom-date.pipe";
import { SpanishDecimalPipe } from './custom-number.pipe';
import { FilterByNamePipe } from "./filterByName.pipe";
import { FilterAssetByNamePipe } from "./filterAssetByName.pipe"
import { CurrencyCustomPipe } from "./currency.pipe";
import { DatediffPipe } from './dateDiff.pipe';
import { ValuesExistPipe } from './valuesExist.pipe';
import { WhatFilePipe } from './what-file.pipe';
import { MessageStatusPipe } from './messageStatus.pipe';

@NgModule({
  imports:[],
  declarations: [
    SafePipe,
    ReversePipe,
    CustomDatePipe,
    FilterByNamePipe,
    SpanishDecimalPipe,
    FilterAssetByNamePipe,
    CurrencyCustomPipe,
    DatediffPipe,
    ValuesExistPipe,
    WhatFilePipe,
    MessageStatusPipe 
  ],
  exports:[
    SafePipe,
    ReversePipe,
    CustomDatePipe,
    FilterByNamePipe,
    SpanishDecimalPipe,
    FilterAssetByNamePipe,
    CurrencyCustomPipe,
    DatediffPipe,
    ValuesExistPipe,
    WhatFilePipe,
    MessageStatusPipe 
  ],
})
export class PipesModule { }

