import { Pipe, PipeTransform } from '@angular/core';@Pipe({
  name: 'whatFile'
})
export class WhatFilePipe implements PipeTransform {

  transform(url: any): any {
    const arr = url.split('.');
    const ext = arr[arr.length - 1];
    const extensions = ['jpg', 'jpeg', 'gif', 'png', 'avif', 'PNG', 'GIF', 'JPEG', 'JPG'];
    
    if(extensions.includes(ext)) {
     return true;
    }
    return false; 
  }
}