import { Pipe, PipeTransform } from '@angular/core';
import { IAsset } from 'src/modules/asset/models/asset';

@Pipe({ name: 'filterasset' })
export class FilterAssetByNamePipe implements PipeTransform {

  transform(assets: IAsset[], term: string): IAsset[] {
    if(!term || term.length <= 0){ return assets; }
    return assets.filter(a => a.name.toLowerCase().includes(term.toLowerCase()));
   }
}